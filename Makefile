# Executables (local)
DOCKER_COMP = docker compose

# Docker containers
PHP_CONT = $(DOCKER_COMP) exec webapp
NPM_CONT  = $(DOCKER_COMP) exec webapp
BEHAT_CONT  = $(DOCKER_COMP) exec behat

# Executables
PHP      = $(PHP_CONT) php
COMPOSER = $(PHP_CONT) composer
CAKEPHP  = $(PHP_CONT) app/Console/cake
PHINX = $(PHP_CONT) vendors/bin/phinx
BEHAT  = $(BEHAT_CONT) bin/behat
NPM  = $(NPM_CONT) npm

# Misc
.DEFAULT_GOAL = help
.PHONY        = help build up start down logs sh composer vendor cake cc phinx behat

## —— 🐳 The webdelib Docker Makefile 🐳 ——————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

## —— Docker 🐳 ————————————————————————————————————————————————————————————————
build: ## Builds the Docker images
	@$(DOCKER_COMP) build --pull --no-cache

up: ## Start the docker hub in detached mode (no logs)
	@$(DOCKER_COMP) up --detach

start: build up ## Build and start the containers

down: ## Stop the docker hub
	@$(DOCKER_COMP) down --remove-orphans

logs: ## Show live logs
	@$(DOCKER_COMP) logs --tail=0 --follow

sh: ## Connect to the PHP container
	@$(PHP_CONT) sh

exec_sh: ## Execute script sh to the PHP container
	@$(eval c ?=)
	@$(PHP_CONT) sh $(c)

## —— Composer 🧙 ——————————————————————————————————————————————————————————————
composer: ## Run composer, pass the parameter "c=" to run a given command, example: make composer c='req symfony/orm-pack'
	@$(eval c ?=)
	@$(COMPOSER) $(c)

vendor: ## Install vendors according to the current composer.lock file
vendor: c=install --prefer-dist --no-dev --no-progress --no-scripts --no-interaction
vendor: composer

vendor-dev: ## Install vendors according to the current composer.lock file
vendor-dev: c=install
vendor-dev: composer

## —— CakePHP ———————————————————————————————————————————————————————————————
cake: ## List all CakePHP commands or pass the parameter "c=" to run a given command, example: make cake c=about
	@$(eval c ?=)
	@$(CAKEPHP) $(c)

cc: c=c:c ## Clear the cache
cc: cake

## —— Phinx ———————————————————————————————————————————————————————————————
phinx: ## List all Phinx commands or pass the parameter "c=" to run a given command, example: make cake c=about
	@$(eval c ?=)
	@$(PHINX) $(c)

migration: phinx

## —— Behat ———————————————————————————————————————————————————————————————
behat: ## List all Behat commands or pass the parameter "c=" to run a given command, example: make behat c=about
	@$(eval c ?=)
	@$(BEHAT) $(c)

cc: behat

behat_composer: ## Connect to the Node container
	@$(eval c ?=)
	@$(BEHAT_CONT) composer $(c)

## —— Npm ———————————————————————————————————————————————————————————————
npm: ## List all Npm commands or pass the parameter "npm=" to run a given command, example: make npm c=about
	@$(eval c ?=)
	@$(NPM) $(c)

npm_sh: ## Connect to the Node container
	@$(NPM_CONT) sh


#test-clean:
#	@rm -Rf build/testcollabora
#
#docker-checkstyle: docker-dir-build-test
#ifeq ($(DIR_BUILD_EXIST), true)
#	@rm -Rf build/test/phpcs
#endif
#	@mkdir build/test/phpcs
#ifneq (,$(findstring $(APP_ENV), dev test))
#	@vendors/bin/phpcs --config-set installed_paths vendors/cakephp/cakephp-codesniffer
#endif
#	vendors/bin/phpcs --standard=phpcs.xml --report-full=build/test/phpcs/full.txt --report-summary=build/test/phpcs/summary.txt app/Locale -v
#
#	#@vendors/bin/phpmd app/Model text codesize --reportfile test/reportfile-phpmd.xml --suffixes php,test/phpmd.xml
#	vendors/bin/phpmd app/Model text codesize phpmd.xml
#
#docker-phpunit: docker-dir-build-test
#	app/Console/cake install --no-question
#ifeq ($(DIR_BUILD_EXIST), true)
#	@rm -Rf build/test/phpunit
#endif
#	@mkdir build/test/phpunit
#	@echo $(MKFLAGS)
#	@mkdir build/test/phpunit/coverage
#ifdef MKFLAGS
#	app/Console/cake test app $(MKFLAGS) --configuration ./phpunit.xml --stderr
#else
#	app/Console/cake test app AllTests --configuration ./phpunit.xml --stderr
#endif
#
#behat:
#	make -C vendors/webdelib/behat behat
#behat-wip:
#	make -C vendors/webdelib/behat behat-wip
#behat-clean:
#	make -C vendors/webdelib/behat behat-clean
#behat-clean-app-conf:
#	make -C vendors/webdelib/behat behat-clean-app-conf
#
#lint:
#	docker-compose -f docker-compose.yml exec webapp make docker-lint
#docker-lint:
#	find -name "*.php" -print0 | xargs -0 -n1 -P8 php -l >> /dev/null 2>&1
#
#clean:
#	docker compose -f docker-compose.yml exec webapp make docker-clean
#
#docker-clean:
#	# Suppression des dossiers de build
#	@rm -Rf ./build/
#	# Suppression des sessions de connexion
#	@rm -f ./app/tmp/sessions/sess_*
#	# Suppression du cache
#	@rm -f ./app/tmp/cache/models/webdelib_*
#	@rm -f ./app/tmp/cache/persistent/webdelib_*
#	@rm -f ./app/tmp/cache/views/webdelib_*
#
#docker-permissions:
#	find . -type d -exec chmod 700 {} \;
#	find . -type f -exec chmod 644 {} \;
#	setfacl -R -b . && \
#	setfacl -R -m u:${HTTPDUSER}:r-x . && \
#	setfacl -R -m u:${HTTPDUSER}:rwx ./app/webroot/files && \
#	setfacl -R -m u:${HTTPDUSER}:rwx ./app/Config && \
#	setfacl -R -m u:${HTTPDUSER}:rwx ./app/tmp && \
#	setfacl -R -d -m u:${HTTPDUSER}:rwx ./app/tmp && \
#	setfacl -R -m u:${HTTPDUSER}:rwx ./app_check/Config && \
#	setfacl -R -m u:${HTTPDUSER}:rwx ./app_check/tmp && \
#	chmod +x ./app/Console/cake
#
#docker-permissions-dev:
#	chmod +x ./app/Console/cake
#	chmod +x ./node_modules/.bin/bower
#	chmod +x ./node_modules/.bin/grunt