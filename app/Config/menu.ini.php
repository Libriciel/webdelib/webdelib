<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$navbar = [
    'Mes projets' => [
        'title' => __('Projets que j\'ai créés ou qui sont dans mes circuits d\'élaboration et de validation'),
        'subMenu' => [
            [
                'html' => 'link',
                'libelle' => __('Créer un projet'),
                'check' => ['Projets', 'create'],
                'title' => __('Créer un nouveau projet'),
                'icon' => 'plus',
                'url' => [
                    'admin' => false,
                    'plugin' => null,
                    'controller' => 'projets',
                    'action' => 'add'
                ]
            ],
            [
                'html' => 'link',
                'libelle' => __('En cours de rédaction'),
                'icon' => 'pause',
                'check' => ['Projets', 'read'],
                'title' => __('Projets que j\'ai créés'),
                'url' => [
                    'admin' => false,
                    'plugin' => null,
                    'controller' => 'projets',
                    'action' => 'mesProjetsRedaction'
                ]
            ],
            [
                'html' => 'link',
                'libelle' => __('En cours de validation'),
                'icon' => 'road',
                'check' => ['Projets/mesProjetsValidation', 'read'],
                'title' => __('Projets que j\'ai validés ou créés qui sont en cour de validation'),
                'url' => [
                    'admin' => false,
                    'plugin' => null,
                    'controller' => 'projets',
                    'action' => 'mesProjetsValidation'
                ]
            ],
            [
                'html' => 'link',
                'libelle' => __('À traiter'),
                'icon' => 'play',
                'check' => ['Projets/mesProjetsATraiter','read'],
                'title' => __('Projets qui sont dans mes circuits de validation'),
                'url' => [
                    'admin' => false,
                    'plugin' => null,
                    'controller' => 'projets',
                    'action' => 'mesProjetsATraiter'
                ]
            ],
            [
                'html' => 'link',
                'libelle' => __('Validés'),
                'check' => ['Projets/mesProjetsValides','read'],
                'title' => __('Projets qui sont dans mes circuits de validation ou que j\'ai créés'),
                'icon' => 'check-square',
                'url' => [
                    'admin' => false,
                    'plugin' => null,
                    'controller' => 'projets',
                    'action' => 'mesProjetsValides'
                ]
            ],
            ['html' => 'divider'],
            [
                'html' => 'link',
                'libelle' => __('Mon service'),
                'check' => ['Projets/projetsMonService','read'],
                'title' => __('Projets rédigés par mon service'),
                'icon' => 'list',
                'url' => [
                    'admin' => false,
                    'plugin' => null,
                    'controller' => 'projets',
                    'action' => 'projetsMonService'
                ]
            ]
        ]
    ],
    'Tous les projets' => [
        'title' => __('Projets de tous les rédacteurs'),
        'subMenu' => [
            [
                'html' => 'subMenu',
                'content' => [
                    'Délibérations' => [
                        'title' => __('Projets de type délibération de tous les rédacteurs'),
                        'subMenu' => [
                            [
                                'html' => 'link',
                                'libelle' => __('À attribuer'),
                                'check' => ['Deliberations/tousLesProjetsSansSeance', 'read'],
                                'title' => __('Projets non associés à une séance'),
                                'icon' => 'list',
                                'url' => [
                                    'admin' => false,
                                    'plugin' => null,
                                    'controller' => 'deliberations',
                                    'action' => 'tousLesProjetsSansSeance'
                                ]
                            ],
                            [
                                'html' => 'link',
                                'libelle' => __('À valider'),
                                'check' => ['Deliberations/tousLesProjetsValidation', 'read'],
                                'title' => __('Projets en cours d\'élaboration et de validation'),
                                'icon' => 'road',
                                'url' => [
                                    'admin' => false,
                                    'plugin' => null,
                                    'controller' => 'deliberations',
                                    'action' => 'tousLesProjetsValidation'
                                ]
                            ],
                            [
                                'html' => 'link',
                                'libelle' => __('À faire voter'),
                                'check' => ['Deliberations/tousLesProjetsAFaireVoter', 'read'],
                                'title' => __('Projets validés associés à une séance'),
                                'icon' => 'list',
                                'url' => [
                                    'admin' => false,
                                    'plugin' => null,
                                    'controller' => 'deliberations',
                                    'action' => 'tousLesProjetsAFaireVoter'
                                ]
                            ],
                        ]
                    ]
                ]
            ],
            [
                'html' => 'subMenu',
                'content' => [
                    'Autres actes...' => [
                        'title' => __('Projets de type autres actes de tous les rédacteurs'),
                        'subMenu' => [
                            [
                                'html' => 'link',
                                'libelle' => __('À valider'),
                                'check' => ['AutresActes/autresActesAValider', 'read'],
                                'title' => __('Projets à valider'),
                                'icon' => 'road',
                                'url' => [
                                    'admin' => false,
                                    'plugin' => null,
                                    'controller' => 'autres_actes',
                                    'action' => 'autresActesAValider'
                                ]
                            ],
                            [
                                'html' => 'link',
                                'libelle' => __('Validés'),
                                'check' => ['AutresActes/autresActesValides', 'read'],
                                'title' => __('Projets validés'),
                                'icon' => 'check-square',
                                'url' => [
                                    'admin' => false,
                                    'plugin' => null,
                                    'controller' => 'autres_actes',
                                    'action' => 'autresActesValides'
                                ]
                            ],
                            ['html' => 'divider'],
                            [
                                'html' => 'link',
                                'libelle' => __('À télétransmettre'),
                                'check' => ['AutresActes/autresActesAEnvoyer', 'read'],
                                'title' => __('Projets à télétransmettre au TdT'),
                                'icon' => 'share-square',
                                'url' => [
                                    'admin' => false,
                                    'plugin' => null,
                                    'controller' => 'autres_actes',
                                    'action' => 'autresActesAEnvoyer'
                                ]
                            ],
                            [
                                'html' => 'link',
                                'libelle' => __('Télétransmis'),
                                'check' => ['AutresActes/autresActesEnvoyes', 'read'],
                                'title' => __('Projets télétransmis au TdT'),
                                'icon' => 'university',
                                'url' => [
                                    'admin' => false,
                                    'plugin' => null,
                                    'controller' => 'autres_actes',
                                    'action' => 'autresActesEnvoyes'
                                ]
                            ],
                            ['html' => 'divider'],
                            [
                                'html' => 'link',
                                'libelle' => __('Non Télétransmissibles'),
                                'check' => ['AutresActes/nonTransmis', 'read'],
                                'title' => __('Projets non transmissibles au TdT'),
                                'icon' => 'hdd',
                                'url' => [
                                    'admin' => false,
                                    'plugin' => null,
                                    'controller' => 'autres_actes',
                                    'action' => 'nonTransmis'
                                ]
                            ],
                            ['html' => 'divider'],
                            [
                                'html' => 'link',
                                'libelle' => __('Publication'),
                                'check' => ['sendToPublish','read'],
                                'title' => __('Envoi des autres actes pour publication'),
                                'icon' => 'globe',
                                'url' => [
                                    'admin' => false,
                                    'plugin' => null,
                                    'controller' => 'publier',
                                    'action' => 'sendToPublish'
                                ]
                            ],
                            ['html' => 'divider'],
                            [
                                'html' => 'link',
                                'libelle' => __('Versement vers le SAE'),
                                'check' => ['sendToSaeAutresActes','read'],
                                'title' => __('Envoi des autres actes au SAE'),
                                'icon' => 'archive',
                                'url' => [
                                    'admin' => false,
                                    'plugin' => null,
                                    'controller' => 'VerserSae',
                                    'action' => 'sendToSaeAutresActes'
                                ]
                            ],
                        ]
                    ],
                ]
            ],
            ['html' => 'divider'],
            [
                'html' => 'link',
                'libelle' => __('Tableau de bord'),
                'check' => ['TableauDeBord','read'],
                'title' => __('Tableau de bord des projets'),
                'icon' => 'tachometer-alt',
                'url' => ['admin' => false, 'plugin' => null, 'controller' => 'tableau_de_bord', 'action' => 'index']
            ],
        ]
    ],
    'Séances' => [
        'title' => __('Gestion des Séances en cours'),
        'subMenu' => [
            [
                'html' => 'link',
                'libelle' => __('Créer une séance'),
                'check' => ['Seances', 'create'],
                'title' => __('Créer une nouvelle séance'),
                'icon' => 'plus',
                'url' => [
                    'admin' => false,
                    'prefix' => null,
                    'plugin' => null,
                    'controller' => 'seances',
                    'action' => 'add'
                ]
            ],
            [
                'html' => 'link',
                'libelle' => __('À traiter'),
                'check' => ['Seances', 'read'],
                'title' => __('Liste des séances à traiter'),
                'icon' => 'unlock-alt',
                'url' => [
                    'admin' => false,
                    'prefix' => null,
                    'plugin' => null,
                    'controller' => 'seances',
                    'action' => 'index'
                ]
            ],
            [
                'html' => 'link',
                'libelle' => __('Calendrier'),
                'check' => ['calendrier', 'read'],
                'title' => __('Calendrier des séances'),
                'icon' => 'calendar-alt',
                'url' => ['admin' => false, 'plugin' => null, 'controller' => 'seances', 'action' => 'calendrier']
            ],
        ]
    ],
    'Post-séances' => [
        'title' => '', //FIX
        'subMenu' => [
            [
                'html' => 'link',
                'libelle' => __('Éditions'),
                'check' => ['Postseances', 'read'],
                'title' => __('Éditions des séances passées'),
                'icon' => 'lock',
                'url' => ['admin' => false, 'plugin' => null, 'controller' => 'postseances', 'action' => 'index']
            ],
            ['html' => 'divider'],
            [
                'html' => 'link',
                'libelle' => __('Signatures'),
                'check' => ['Postseances/signature','read'],
                'title' => __('Signature des délibérations'),
                'icon' => 'signature',
                'iconType' => 'ls',
                'url' => ['admin' => false, 'plugin' => null, 'controller' => 'postseances', 'action' => 'signature']
            ],
            ['html' => 'divider'],
            [
                'html' => 'link',
                'libelle' => __('À télétransmettre'),
                'check' => ['Postseances/toSend','read'],
                'title' => __('Envoi des délibérations au contrôle de légalité'),
                'icon' => 'share-square',
                'url' => ['admin' => false, 'plugin' => null, 'controller' => 'postseances', 'action' => 'toSend']
            ],
            [
                'html' => 'link',
                'libelle' => __('Télétransmises'),
                'check' => ['Postseances/transmit','read'],
                'title' => __('Délibérations télétransmises au contrôle de légalité'),
                'icon' => 'university',
                'url' => ['admin' => false, 'plugin' => null, 'controller' => 'postseances', 'action' => 'transmit']
            ],
            ['html' => 'divider'],
            [
                'html' => 'link',
                'libelle' => __('Publication'),
                'check' => ['sendToPublishDeliberation','read'],
                'title' => __('Envoi des délibérations pour publication'),
                'icon' => 'globe',
                'url' => [
                    'admin' => false,
                    'plugin' => null,
                    'controller' => 'publier',
                    'action' => 'sendToPublishDeliberation'
                ]
            ],
            ['html' => 'divider'],
            [
                'html' => 'link',
                'libelle' => __('Versement vers le SAE'),
                'check' => ['sendToSaeDeliberation','read'],
                'title' => __('Envoi des délibérations au SAE'),
                'icon' => 'archive',
                'url' => [
                    'admin' => false,
                    'plugin' => null,
                    'controller' => 'VerserSae',
                    'action' => 'sendToSaeDeliberation'
                ]
            ],
        ]
    ],
    'Administration' => [
        'title' => __('Administration de l\'application'), //FIX
        'subMenu' => [
            [
                'html' => 'subMenu',
                'content' => [
                    'Générale' => [
                        'title' => __('Administration Générale'),
                        'subMenu' => [
                            [
                                'html' => 'link',
                                'libelle' => __('Collectivité'),
                                'check' => ['Collectivites', 'read'],
                                'title' => __('Informations sur la collectivité'),
                                'icon' => 'building',
                                'manager' => false,
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => null,
                                    'controller' => 'collectivites',
                                    'action' => 'index'
                                ]
                            ],
                            [
                                'html' => 'link',
                                'libelle' => __('Thèmes'),
                                'check' => ['Themes', 'read'],
                                'title' => __('Gestion des thèmes'),
                                'icon' => 'theme',
                                'iconType' => 'ls',
                                'manager' => false,
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => null,
                                    'controller' => 'themes',
                                    'action' => 'index'
                                ]
                            ],
                            [
                                'html' => 'link',
                                'libelle' => __('Modèles d\'édition'),
                                'check' => ['ModelOdtValidator/Modeltemplates', 'read'],
                                'title' => __('Gestion des modèles'),
                                'icon' => 'modele',
                                'iconType' => 'ls',
                                'manager' => false,
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => 'model_odt_validator',
                                    'controller' => 'modeltemplates',
                                    'action' => 'index'
                                ]
                            ],
                            [
                                'html' => 'link',
                                'libelle' => __('Séquences'),
                                'check' => ['Sequences', 'read'],
                                'title' => __('Séquences des compteurs'),
                                'icon' => 'list-ol',
                                'manager' => false,
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => null,
                                    'controller' => 'sequences',
                                    'action' => 'index'
                                ]
                            ],
                            [
                                'html' => 'link',
                                'libelle' => __('Compteurs'),
                                'check' => ['Compteurs', 'read'],
                                'title' => __('Compteurs des types d\'actes'),
                                'icon' => 'tachometer-alt',
                                'manager' => false,
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => null,
                                    'controller' => 'compteurs',
                                    'action' => 'index'
                                ]
                            ],
                            [
                                'html' => 'link',
                                'libelle' => __('Types d\'actes'),
                                'check' => ['Typeactes', 'read'],
                                'title' => __('Gestion des types d\'acte'),
                                'icon' => 'book',
                                'manager' => false,
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => null,
                                    'controller' => 'typeactes',
                                    'action' => 'index'
                                ]
                            ],
                            [
                                'html' => 'link',
                                'libelle' => __('Types de séances'),
                                'check' => ['Typeseances', 'read'],
                                'title' => __('Gestion des types de séance'),
                                'icon' => 'tag',
                                'manager' => false,
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => null,
                                    'controller' => 'typeseances',
                                    'action' => 'index'
                                ]
                            ],
                            [
                                'html' => 'link',
                                'libelle' => __('Gestion vue synthétique'),
                                'check' => ['Collectivites', 'update'],
                                'title' => __('Gestion de la vue synthétique'),
                                'icon' => 'table',
                                'manager' => false,
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => null,
                                    'controller' => 'collectivites',
                                    'action' => 'edit9Cases'
                                ]
                            ],
                        ]
                    ],
                    'Utilisateurs' => [
                        'title' => __('Administration des utilisateurs'),
                        'subMenu' => [
                            [
                                'html' => 'link',
                                'libelle' => __('Utilisateurs'),
                                'check' => ['Users', 'read'],
                                'title' => __('Gestion des utilisateurs'),
                                'icon' => 'user',
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => null,
                                    'controller' => 'users',
                                    'action' => 'index'
                                ]
                            ],
                            [
                                'html' => 'link',
                                'libelle' => __('Profils'),
                                'check' => ['Profils', 'read'],
                                'title' => __('Gestion des profils'),
                                'icon' => 'users',
                                'manager' => false,
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => null,
                                    'controller' => 'profils',
                                    'action' => 'index'
                                ]
                            ],
                            [
                                'html' => 'link',
                                'libelle' => __('Services'),
                                'check' => ['Services', 'read'],
                                'title' => __('Gestion des services'),
                                'icon' => 'sitemap',
                                'manager' => true,
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => null,
                                    'controller' => 'services',
                                    'action' => 'index'
                                ]
                            ],
                            [
                                'html' => 'link',
                                'libelle' => __('Circuits'),
                                'check' => ['Cakeflow/Circuits', 'read'],
                                'title' => __('Informations sur la collectivité'),
                                'icon' => 'road',
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => 'cakeflow',
                                    'controller' => 'circuits',
                                    'action' => 'index'
                                ]
                            ],
                        ]
                    ],
                    'Acteurs' => [
                        'title' => __('Administration des acteurs'),
                        'subMenu' => [
                            [
                                'html' => 'link',
                                'libelle' => __('Acteurs'),
                                'check' => ['Acteurs','read'],
                                'title' => __('Gestion des acteurs'),
                                'icon' => 'user',
                                'manager' => false,
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => null,
                                    'controller' => 'acteurs',
                                    'action' => 'index'
                                ]
                            ],
                            [
                                'html' => 'link',
                                'libelle' => __('Types'),
                                'check' => ['Typeacteurs','read'],
                                'title' => __('Types des Acteurs'),
                                'icon' => 'users',
                                'manager' => false,
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => null,
                                    'controller' => 'typeacteurs',
                                    'action' => 'index'
                                ]
                            ],
                        ]
                    ],
                    'Informations sup.' => [
                        'title' => __('Administration des informations suplémentaires'),
                        'subMenu' => [
                            [
                                'html' => 'link',
                                'libelle' => __('Projet'),
                                'check' => ['Infosupdefs','read'],
                                'title' => __('Gestion des informations suplémentaires de projet'),
                                'icon' => 'list',
                                'manager' => false,
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => null,
                                    'controller' => 'infosupdefs',
                                    'action' => 'index',
                                    'Deliberation'
                                ]
                            ],
                            [
                                'html' => 'link',
                                'libelle' => __('Séance'),
                                'check' => ['Infosupdefs','read'],
                                'title' => __('Gestion des informations suplémentaires de séance'),
                                'icon' => 'list',
                                'manager' => false,
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => null,
                                    'controller' => 'infosupdefs',
                                    'action' => 'index',
                                    'Seance'
                                ]
                            ],
                        ]
                    ],
                    'Maintenance' => [
                        'title' => __('Maintenance de l\'application'),
                        'subMenu' => [
                            [
                                'html' => 'link',
                                'libelle' => __('Connecteurs'),
                                'check' => ['Connecteurs','read'],
                                'title' => __('Gestion des connecteurs de l\'application'),
                                'icon' => 'plug',
                                'manager' => false,
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => null,
                                    'controller' => 'connecteurs',
                                    'action' => 'index'
                                ]
                            ],
                            [
                                'html' => 'link',
                                'libelle' => __('Test du système'),
                                'check' => ['Verifications','read'],
                                'title' => __('Test du système de l\'application'),
                                'icon' => 'check',
                                'manager' => false,
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => null,
                                    'controller' => 'verifications',
                                    'action' => 'index'
                                ]
                            ],
                            [
                                'html' => 'link',
                                'libelle' => __('Tâches automatiques'),
                                'check' => ['Crons','read'],
                                'title' => __('Gestion des tâches automatiques'),
                                'icon' => 'clock',
                                'manager' => false,
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => null,
                                    'controller' => 'crons',
                                    'action' => 'index'
                                ]
                            ],
                            [
                                'html' => 'link',
                                'libelle' => __('Historiques'),
                                'check' => ['Historiques','read'],
                                'title' => __('Historiques des modifications de projets'),
                                'icon' => 'history',
                                'manager' => false,
                                'url' => [
                                    'admin' => true,
                                    'prefix' => 'admin',
                                    'plugin' => null,
                                    'controller' => 'historiques',
                                    'action' => 'index'
                                ]
                            ],
                        ]
                    ],
                ]
            ],
        ]
    ],
];
