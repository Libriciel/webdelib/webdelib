<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
Router::connect('/', ['controller' => 'dossiers', 'action' => 'index']);

/**
 * Ajout de la route pour la réponse REST à Allo.
 */
Router::connect('/api/rest/allo/version', ['controller' => 'allos', 'action' => 'version']);

Router::connect('/api/rest/wopi/files/:id',
    ['controller' => 'wopi', 'action' => 'getFileInfo', "[method]" => "GET"],
    ['pass' => ['id']]
);
Router::connect('/api/rest/wopi/files/:id',
    ['controller' => 'wopi', 'action' => 'getFileInfo', "[method]" => "POST"],
    ['pass' => ['id']]
);
Router::connect('/api/rest/wopi/files/:id/contents',
    ['controller' => 'wopi', 'action' => 'getFile', "[method]" => "GET"],
    ['pass' => ['id']]
);
Router::connect('/api/rest/wopi/files/:id/contents',
    ['controller' => 'wopi', 'action' => 'putFile', "[method]" => "POST"],
    ['pass' => ['id']]
);
Router::connect('/api/rest/wopi/files/:id/contents',
    ['controller' => 'wopi', 'action' => 'putFile', "[method]" => "PUT"],
    ['pass' => ['id']]
);

//Router::connect('/check/**', array('plugin' => 'check', 'controller' => 'verifications', 'action' => 'index'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';

