<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * Database configuration class.
 *
 * You can specify multiple configurations for production, development and testing.
 *
 * datasource => The name of a supported datasource; valid options are as follows:
 *  Database/Mysql - MySQL 4 & 5,
 *  Database/Sqlite - SQLite (PHP5 only),
 *  Database/Postgres - PostgreSQL 7 and higher,
 *  Database/Sqlserver - Microsoft SQL Server 2005 and higher
 *
 * You can add custom database datasources (or override existing datasources) by adding the
 * appropriate file to app/Model/Datasource/Database. Datasources should be named 'MyDatasource.php',
 *
 *
 * persistent => true / false
 * Determines whether or not the database should use a persistent connection
 *
 * host =>
 * the host you connect to the database. To add a socket or port number, use 'port' => #
 *
 * prefix =>
 * Uses the given prefix for all the tables in this database. This setting can be overridden
 * on a per-table basis with the Model::$tablePrefix property.
 *
 * schema =>
 * For Postgres/Sqlserver specifies which schema you would like to use the tables in.
 * Postgres defaults to 'public'. For Sqlserver, it defaults to empty and use
 * the connected user's default schema (typically 'dbo').
 *
 * encoding =>
 * For MySQL, Postgres specifies the character encoding to use when connecting to the
 * database. Uses database default not specified.
 *
 * unix_socket =>
 * For MySQL to connect via socket specify the `unix_socket` parameter instead of `host` and `port`
 *
 * settings =>
 * Array of key/value pairs, on connection it executes SET statements for each pair
 * For MySQL : http://dev.mysql.com/doc/refman/5.6/en/set-statement.html
 * For Postgres : http://www.postgresql.org/docs/9.2/static/sql-set.html
 * For Sql Server : http://msdn.microsoft.com/en-us/library/ms190356.aspx
 *
 * flags =>
 * A key/value array of driver specific connection options.
 */

class DATABASE_CONFIG
{
    /**
     * [public description]
     * @var [type]
     */
    public $default = [
         'datasource' => 'Database/Postgres',
         'persistent' => false,
         'host' => 'wd-postgres',
         'login' =>  'webdelib',
         'password' => 'webdelib',
         'database' => 'webdelib',
         'port'     => '5432',
         'schema'     => 'public',
         'prefix' => '',
         'settings' => ['bytea_output' => 'escape', 'datestyle' => 'ISO, DMY']
     ];
    /**
     * [public description]
     * @var [type]
     */
    public $test = [
        'datasource' => 'Database/Postgres',
        'persistent' => false,
        'host' => 'webdelib-postgres-test',
        'login' => 'webdelib',
        'password' =>'webdelib',
        'database' => 'webdelib_testunit',
        'port'     => '5432',
        'schema'     => 'public',
        'prefix' => '',
        'settings' => ['bytea_output' => 'escape', 'datestyle' => 'ISO, DMY']
    ];

    public function __construct()
    {
        Configure::load('fqdn_databases');
        $fqdnDataBases = array_unique(Configure::read('fqdn_config'));

        foreach ($fqdnDataBases as $database) {
            $this->{$database} = [
                'datasource' => 'Database/Postgres',
                'persistent' => false,
                'host' => env('POSTGRES_HOST'),
                'login' =>  env('POSTGRES_USER'),
                'password' => env('POSTGRES_PASSWORD'),
                'database' => env('POSTGRES_DB'),
                'port'     => '5432',
                'schema'     => $database,
                'prefix' => '',
                'settings' => ['bytea_output' => 'escape', 'datestyle' => 'ISO, DMY']
            ];
        }

        if (!empty(env('POSTGRES_HOST'))) {
            $this->default['host'] = env('POSTGRES_HOST');
        }
        if (!empty(env('POSTGRES_HOST_TEST'))) {
            $this->test['host'] = env('POSTGRES_HOST_TEST');
        }
        if (!empty(env('POSTGRES_DB'))) {
            $this->default['database'] = env('POSTGRES_DB');
        }
        if (!empty(env('POSTGRES_DB_TEST'))) {
            $this->test['database'] = env('POSTGRES_DB_TEST');
        }
        if (!empty(env('POSTGRES_USER'))) {
            $this->default['login'] = env('POSTGRES_USER');
            $this->test['login'] = env('POSTGRES_USER');
        }
        if (!empty(env('POSTGRES_PASSWORD'))) {
            $this->default['password'] = env('POSTGRES_PASSWORD');
            $this->test['password'] = env('POSTGRES_PASSWORD');
        }
        if (!empty(env('POSTGRES_PORT'))) {
            $this->default['port'] = env('POSTGRES_PORT');
            $this->test['port'] = env('POSTGRES_PORT');
        }
        if (!empty(env('POSTGRES_SCHEMA'))) {
            $this->default['schema'] = env('POSTGRES_SCHEMA');
            $this->test['schema'] = env('POSTGRES_SCHEMA');
        }
    }
}
