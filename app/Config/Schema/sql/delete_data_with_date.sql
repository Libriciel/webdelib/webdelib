CREATE OR REPLACE FUNCTION delete_projects_with_date(delete_date date)
RETURNS void AS $$
DECLARE
    deliberationIds RECORD;
    seanceIds RECORD;
    traitementIds RECORD;
BEGIN
    SELECT id INTO deliberationIds FROM deliberations WHERE date_acte <= delete_date;
    SELECT id INTO seanceIds FROM seances WHERE date <= delete_date;
    SELECT id INTO traitementIds FROM wkf_traitements WHERE target_id IN (SELECT id FROM deliberations WHERE date_acte <= delete_date);
    ---- Circuits
    DELETE FROM wkf_visas WHERE traitement_id = traitementIds.id;
    DELETE FROM wkf_traitements WHERE target_id = deliberationIds.id;
    ---- Deliberations
    DELETE FROM deliberations_seances WHERE deliberation_id = deliberationIds.id;
    DELETE FROM votes WHERE delib_id = deliberationIds.id;
    DELETE FROM listepresences WHERE delib_id = deliberationIds.id;
    DELETE FROM listepresences WHERE delib_id = deliberationIds.id;
    DELETE FROM commentaires WHERE delib_id = deliberationIds.id;
    DELETE FROM historiques WHERE delib_id = deliberationIds.id;
    DELETE FROM tdt_messages WHERE delib_id = deliberationIds.id;
    DELETE FROM infosups WHERE model='Deliberation' AND foreign_key = deliberationIds.id;
    DELETE FROM deliberations_typeseances WHERE deliberation_id = deliberationIds.id;
    DELETE FROM annexes WHERE foreign_key = deliberationIds.id;
    DELETE FROM annexes_versions WHERE foreign_key = deliberationIds.id;
    DELETE FROM deliberations_versions WHERE foreign_key = deliberationIds.id;
    DELETE FROM deliberations WHERE id = deliberationIds.id;
    ----- Seances
    DELETE FROM infosups WHERE model='Seance' AND foreign_key = seanceIds.id;
    DELETE FROM deliberations_seances WHERE seance_id = seanceIds.id;
    DELETE FROM seances WHERE id = seanceIds.id;
END;
$$ LANGUAGE plpgsql;


