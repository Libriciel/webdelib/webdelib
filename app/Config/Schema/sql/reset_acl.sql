TRUNCATE TABLE acos, aros, aros_acos;
ALTER TABLE acos ALTER id RESTART WITH 1;
ALTER TABLE aros ALTER id RESTART WITH 1;
ALTER TABLE aros_acos ALTER id RESTART WITH 1;