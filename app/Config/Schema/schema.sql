--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;
SET default_tablespace = '';
SET default_with_oids = false;
SET bytea_output = 'escape';

--
-- Name: acteurs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE acteurs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: acteurs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE acteurs (
    id integer DEFAULT nextval('acteurs_id_seq'::regclass) NOT NULL,
    typeacteur_id integer DEFAULT 0 NOT NULL,
    nom character varying(50) DEFAULT ''::character varying NOT NULL,
    prenom character varying(50) DEFAULT ''::character varying NOT NULL,
    salutation character varying(50) NOT NULL,
    titre character varying(250),
    "position" integer NOT NULL,
    date_naissance date,
    adresse1 character varying(100) NOT NULL,
    adresse2 character varying(100) NOT NULL,
    cp character varying(20) NOT NULL,
    ville character varying(100) NOT NULL,
    email character varying(100) NOT NULL,
    telfixe character varying(20),
    telmobile character varying(20),
    suppleant_id integer,
    note character varying(255) NOT NULL,
    actif boolean DEFAULT true NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL,
    notif_insertion boolean DEFAULT false
);


--
-- Name: acteurs_seances; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE acteurs_seances (
    id integer NOT NULL,
    acteur_id integer NOT NULL,
    seance_id integer NOT NULL,
    mail_id integer NOT NULL,
    date_envoi timestamp without time zone NOT NULL,
    date_reception timestamp without time zone,
    model character varying(20) NOT NULL
);


--
-- Name: acteurs_seances_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE acteurs_seances_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: acteurs_seances_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE acteurs_seances_id_seq OWNED BY acteurs_seances.id;


--
-- Name: acteurs_services_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE acteurs_services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: acteurs_services; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE acteurs_services (
    id integer DEFAULT nextval('acteurs_services_id_seq'::regclass) NOT NULL,
    acteur_id integer DEFAULT 0 NOT NULL,
    service_id integer DEFAULT 0 NOT NULL
);


--
-- Name: annexes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE annexes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: annexes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE annexes (
    id integer DEFAULT nextval('annexes_id_seq'::regclass) NOT NULL,
    model character varying(255) NOT NULL,
    foreign_key integer NOT NULL,
    joindre_ctrl_legalite boolean DEFAULT false NOT NULL,
    joindre_fusion boolean DEFAULT false NOT NULL,
    titre character varying(200) NOT NULL,
    filename character varying(100) NOT NULL,
    filetype character varying(255) NOT NULL,
    size integer NOT NULL,
    data bytea NOT NULL,
    data_pdf bytea,
    created timestamp without time zone,
    modified timestamp without time zone,
    edition_data bytea,
    edition_data_typemime character varying,
    "position" smallint,
    tdt_data_pdf bytea
);


--
-- Name: circuits_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE circuits_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: circuits_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE circuits_users (
    id integer DEFAULT nextval('circuits_users_id_seq'::regclass) NOT NULL,
    circuit_id integer NOT NULL,
    user_id integer NOT NULL
);


--
-- Name: collectivites; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE collectivites (
    id integer NOT NULL,
    id_entity integer,
    nom character varying(30) NOT NULL,
    adresse character varying(255) NOT NULL,
    "CP" character varying(5) NOT NULL,
    ville character varying(255) NOT NULL,
    telephone character varying(20) NOT NULL,
    logo bytea,
    "templateProject" json
);


--
-- Name: commentaires_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE commentaires_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: commentaires; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE commentaires (
    id integer DEFAULT nextval('commentaires_id_seq'::regclass) NOT NULL,
    delib_id integer DEFAULT 0 NOT NULL,
    agent_id integer DEFAULT 0 NOT NULL,
    texte character varying(1000),
    pris_en_compte smallint DEFAULT 0 NOT NULL,
    commentaire_auto boolean,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: compteurs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE compteurs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: compteurs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE compteurs (
    id integer DEFAULT nextval('compteurs_id_seq'::regclass) NOT NULL,
    nom character varying(255) NOT NULL,
    commentaire character varying(255) NOT NULL,
    def_compteur character varying(255) NOT NULL,
    sequence_id integer NOT NULL,
    def_reinit character varying(255) NOT NULL,
    val_reinit character varying(255),
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: crons; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE crons (
    id integer NOT NULL,
    nom character varying(255) NOT NULL,
    description character varying(255) DEFAULT NULL::character varying,
    plugin character varying(255) DEFAULT ''::character varying,
    action character varying(255) NOT NULL,
    has_params boolean,
    params character varying(255) DEFAULT NULL::character varying,
    next_execution_time timestamp without time zone,
    execution_duration character varying(255),
    last_execution_start_time timestamp without time zone,
    last_execution_end_time timestamp without time zone,
    last_execution_report text,
    last_execution_status character varying(255),
    active boolean,
    created timestamp without time zone NOT NULL,
    created_user_id integer NOT NULL,
    modified timestamp without time zone NOT NULL,
    modified_user_id integer NOT NULL,
    model character varying DEFAULT 'CronJob'::character varying,
    lock boolean NOT NULL,
    run_all boolean DEFAULT true
);


--
-- Name: crons_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE crons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: crons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE crons_id_seq OWNED BY crons.id;


--
-- Name: deliberations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE deliberations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: deliberations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE deliberations (
    id integer DEFAULT nextval('deliberations_id_seq'::regclass) NOT NULL,
    typeacte_id integer DEFAULT 1 NOT NULL,
    circuit_id integer DEFAULT 0,
    theme_id integer DEFAULT 0 NOT NULL,
    service_id integer DEFAULT 0 NOT NULL,
    redacteur_id integer DEFAULT 0 NOT NULL,
    rapporteur_id integer DEFAULT 0,
    anterieure_id integer,
    is_multidelib boolean,
    parent_id integer,
    objet character varying(1000) NOT NULL,
    objet_delib character varying(1000),
    titre character varying(1000),
    num_delib character varying(15),
    num_pref character varying(255),
    pastell_id character varying(10),
    tdt_id character varying,
    "tdt_dateAR" character varying(100),
    texte_projet bytea,
    texte_projet_name character varying(75),
    texte_projet_type character varying(255),
    texte_projet_size integer,
    texte_synthese bytea,
    texte_synthese_name character varying(75),
    texte_synthese_type character varying(255),
    texte_synthese_size integer,
    deliberation bytea,
    deliberation_name character varying(75),
    deliberation_type character varying(255),
    deliberation_size integer,
    date_limite date,
    date_envoi timestamp without time zone,
    etat integer DEFAULT 0 NOT NULL,
    parapheur_etat smallint,
    parapheur_commentaire character varying(1000),
    sae_etat boolean,
    reporte boolean DEFAULT false NOT NULL,
    montant integer,
    debat bytea,
    debat_name character varying(255),
    debat_size integer,
    debat_type character varying(255),
    avis integer,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL,
    vote_nb_oui integer,
    vote_nb_non integer,
    vote_nb_abstention integer,
    vote_nb_retrait integer,
    vote_commentaire character varying(1000),
    delib_pdf bytea,
    signature bytea,
    signee boolean DEFAULT false NOT NULL,
    commission bytea,
    commission_size integer,
    commission_type character varying(255),
    commission_name character varying(255),
    date_acte timestamp without time zone,
    date_envoi_signature timestamp without time zone,
    parapheur_id character varying(50),
    tdt_data_pdf bytea,
    tdt_data_bordereau_pdf bytea,
    president_id integer,
    parapheur_cible character varying,
    parapheur_bordereau bytea,
    tdt_ar bytea,
    tdt_ar_date timestamp without time zone,
    vote_prendre_acte boolean,
    signature_type character varying(100),
    numero_depot integer DEFAULT 0 NOT NULL,
    texte_projet_active boolean DEFAULT false NOT NULL,
    texte_synthese_active boolean DEFAULT false NOT NULL,
    texte_acte_active boolean DEFAULT false NOT NULL
);


--
-- Name: deliberations_seances_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE deliberations_seances_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: deliberations_seances; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE deliberations_seances (
    id integer DEFAULT nextval('deliberations_seances_id_seq'::regclass) NOT NULL,
    deliberation_id integer NOT NULL,
    seance_id integer NOT NULL,
    "position" integer,
    avis boolean,
    commentaire character varying(1000)
);


--
-- Name: deliberations_typeseances; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE deliberations_typeseances (
    id integer NOT NULL,
    deliberation_id integer NOT NULL,
    typeseance_id integer NOT NULL
);


--
-- Name: deliberations_typeseances_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE deliberations_typeseances_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: deliberations_typeseances_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE deliberations_typeseances_id_seq OWNED BY deliberations_typeseances.id;


--
-- Name: deliberations_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE deliberations_users (
    id integer NOT NULL,
    deliberation_id integer NOT NULL,
    user_id integer NOT NULL
);


--
-- Name: deliberations_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE deliberations_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: deliberations_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE deliberations_users_id_seq OWNED BY deliberations_users.id;


--
-- Name: historiques_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE historiques_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: historiques; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE historiques (
    id integer DEFAULT nextval('historiques_id_seq'::regclass) NOT NULL,
    delib_id integer NOT NULL,
    user_id integer NOT NULL,
    circuit_id integer,
    commentaire text NOT NULL,
    modified timestamp without time zone NOT NULL,
    created timestamp without time zone NOT NULL
);


--
-- Name: infosupdefs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE infosupdefs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: infosupdefs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE infosupdefs (
    id integer DEFAULT nextval('infosupdefs_id_seq'::regclass) NOT NULL,
    model character varying(25) DEFAULT 'Deliberation'::character varying NOT NULL,
    nom character varying(255) NOT NULL,
    commentaire character varying(255) NOT NULL,
    ordre integer NOT NULL,
    code character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    val_initiale character varying(1000),
    recherche boolean DEFAULT false NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL,
    actif boolean,
    typemime character varying,
    joindre_fusion boolean DEFAULT false
);


--
-- Name: infosupdefs_profils; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE infosupdefs_profils (
    id integer NOT NULL,
    profil_id integer NOT NULL,
    infosupdef_id integer NOT NULL
);


--
-- Name: infosupdefs_profils_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE infosupdefs_profils_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: infosupdefs_profils_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE infosupdefs_profils_id_seq OWNED BY infosupdefs_profils.id;


--
-- Name: infosuplistedefs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE infosuplistedefs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: infosuplistedefs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE infosuplistedefs (
    id integer DEFAULT nextval('infosuplistedefs_id_seq'::regclass) NOT NULL,
    infosupdef_id integer NOT NULL,
    ordre integer NOT NULL,
    nom character varying(255) NOT NULL,
    actif boolean DEFAULT true NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: infosups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE infosups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: infosups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE infosups (
    id integer DEFAULT nextval('infosups_id_seq'::regclass) NOT NULL,
    model character varying(25) DEFAULT 'Deliberation'::character varying NOT NULL,
    foreign_key integer NOT NULL,
    infosupdef_id integer,
    text character varying(1000),
    date date,
    file_name character varying(255),
    file_size integer,
    file_type character varying(255),
    content bytea,
    edition_data bytea,
    edition_data_typemime character varying,
    created timestamp without time zone DEFAULT (now())::timestamp(0) without time zone,
    modified timestamp without time zone DEFAULT (now())::timestamp(0) without time zone
);


--
-- Name: listepresences_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE listepresences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: listepresences; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE listepresences (
    id integer DEFAULT nextval('listepresences_id_seq'::regclass) NOT NULL,
    delib_id integer NOT NULL,
    acteur_id integer NOT NULL,
    present boolean NOT NULL,
    mandataire integer,
    suppleant_id integer
);


--
-- Name: natures_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE natures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: natures; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE natures (
    id integer DEFAULT nextval('natures_id_seq'::regclass) NOT NULL,
    name character varying(100) NOT NULL,
    code character varying(3) NOT NULL,
    dua character varying(50),
    sortfinal character varying(50),
    communicabilite character varying(50)
);


--
-- Name: nomenclatures; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE nomenclatures (
    id integer NOT NULL,
    parent_id character varying,
    name character varying NOT NULL,
    libelle character varying NOT NULL,
    lft integer DEFAULT 0,
    rght integer DEFAULT 0,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: nomenclatures_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE nomenclatures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: nomenclatures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE nomenclatures_id_seq OWNED BY nomenclatures.id;


--
-- Name: profils_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE profils_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: profils; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE profils (
    id integer DEFAULT nextval('profils_id_seq'::regclass) NOT NULL,
    parent_id integer DEFAULT 0,
    name character varying(100) DEFAULT ''::character varying NOT NULL,
    actif boolean DEFAULT true NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL,
    role_id integer DEFAULT 1 NOT NULL
);


--
-- Name: roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE roles (
    id integer NOT NULL,
    name text NOT NULL
);


--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- Name: seances_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE seances_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: seances; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE seances (
    id integer DEFAULT nextval('seances_id_seq'::regclass) NOT NULL,
    type_id integer DEFAULT 0 NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL,
    date_convocation timestamp without time zone,
    date timestamp without time zone NOT NULL,
    traitee integer DEFAULT 0 NOT NULL,
    commentaire character varying(500),
    secretaire_id integer,
    president_id integer,
    debat_global bytea,
    debat_global_name character varying(75),
    debat_global_size integer,
    debat_global_type character varying(255),
    pv_figes smallint,
    pv_sommaire bytea,
    pv_complet bytea,
    numero_depot integer DEFAULT 0 NOT NULL,
    idelibre_id character varying
);


--
-- Name: sequences_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sequences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sequences; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE sequences (
    id integer DEFAULT nextval('sequences_id_seq'::regclass) NOT NULL,
    nom character varying(255) NOT NULL,
    commentaire character varying(255) NOT NULL,
    num_sequence integer NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: services_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: services; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE services (
    id integer DEFAULT nextval('services_id_seq'::regclass) NOT NULL,
    parent_id integer DEFAULT 0,
    "order" character varying(50) NOT NULL,
    name character varying(100) NOT NULL,
    circuit_defaut_id integer NOT NULL,
    actif boolean DEFAULT true NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL,
    lft integer DEFAULT 0,
    rght integer DEFAULT 0
);


--
-- Name: services_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE services_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: services_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE services_users (
    id integer DEFAULT nextval('services_users_id_seq'::regclass) NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    service_id integer DEFAULT 0 NOT NULL
);


--
-- Name: tdt_messages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tdt_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tdt_messages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE tdt_messages (
    id integer DEFAULT nextval('tdt_messages_id_seq'::regclass) NOT NULL,
    delib_id integer NOT NULL,
    tdt_id integer NOT NULL,
    tdt_type integer NOT NULL,
    tdt_etat integer,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL,
    date_message date,
    tdt_data bytea,
    parent_id integer
);


--
-- Name: themes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE themes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: themes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE themes (
    id integer DEFAULT nextval('themes_id_seq'::regclass) NOT NULL,
    parent_id integer DEFAULT 0,
    "order" character varying(50) NOT NULL,
    libelle character varying(500),
    actif boolean DEFAULT true NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL,
    lft integer DEFAULT 0,
    rght integer DEFAULT 0
);


--
-- Name: typeactes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE typeactes (
    id integer NOT NULL,
    name character varying(300) NOT NULL,
    modeleprojet_id integer NOT NULL,
    modelefinal_id integer NOT NULL,
    nature_id integer NOT NULL,
    compteur_id integer NOT NULL,
    created date NOT NULL,
    modified date NOT NULL,
    gabarit_projet bytea,
    gabarit_synthese bytea,
    gabarit_acte bytea,
    teletransmettre boolean DEFAULT true,
    gabarit_acte_name character varying,
    gabarit_projet_name character varying,
    gabarit_synthese_name character varying,
    gabarit_projet_active boolean DEFAULT false NOT NULL,
    gabarit_synthese_active boolean DEFAULT false NOT NULL,
    gabarit_acte_active boolean DEFAULT false NOT NULL
);


--
-- Name: typeactes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE typeactes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: typeactes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE typeactes_id_seq OWNED BY typeactes.id;


--
-- Name: typeacteurs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE typeacteurs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: typeacteurs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE typeacteurs (
    id integer DEFAULT nextval('typeacteurs_id_seq'::regclass) NOT NULL,
    nom character varying(255) NOT NULL,
    commentaire character varying(255) NOT NULL,
    elu boolean NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: typeseances_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE typeseances_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: typeseances; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE typeseances (
    id integer DEFAULT nextval('typeseances_id_seq'::regclass) NOT NULL,
    libelle character varying(300) NOT NULL,
    retard integer DEFAULT 0,
    action smallint NOT NULL,
    compteur_id integer NOT NULL,
    modelprojet_id integer NOT NULL,
    modeldeliberation_id integer NOT NULL,
    modelconvocation_id integer NOT NULL,
    modelordredujour_id integer NOT NULL,
    modelpvsommaire_id integer NOT NULL,
    modelpvdetaille_id integer NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL,
    color character varying(7)
);


--
-- Name: typeseances_acteurs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE typeseances_acteurs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: typeseances_acteurs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE typeseances_acteurs (
    id integer DEFAULT nextval('typeseances_acteurs_id_seq'::regclass) NOT NULL,
    typeseance_id integer NOT NULL,
    acteur_id integer NOT NULL
);


--
-- Name: typeseances_natures_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE typeseances_natures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: typeseances_typeactes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE typeseances_typeactes (
    id integer DEFAULT nextval('typeseances_natures_id_seq'::regclass) NOT NULL,
    typeseance_id integer NOT NULL,
    typeacte_id integer NOT NULL
);


--
-- Name: typeseances_typeacteurs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE typeseances_typeacteurs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: typeseances_typeacteurs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE typeseances_typeacteurs (
    id integer DEFAULT nextval('typeseances_typeacteurs_id_seq'::regclass) NOT NULL,
    typeseance_id integer NOT NULL,
    typeacteur_id integer NOT NULL
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    id integer DEFAULT nextval('users_id_seq'::regclass) NOT NULL,
    profil_id integer DEFAULT 0 NOT NULL,
    statut integer DEFAULT 0 NOT NULL,
    username character varying(50) DEFAULT ''::character varying NOT NULL,
    note character varying(300),
    circuit_defaut_id integer,
    password character varying(100) DEFAULT ''::character varying NOT NULL,
    nom character varying(50) DEFAULT ''::character varying NOT NULL,
    prenom character varying(50) DEFAULT ''::character varying NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    telfixe character varying(20),
    telmobile character varying(20),
    date_naissance date,
    accept_notif boolean DEFAULT false,
    mail_refus boolean DEFAULT false,
    mail_traitement boolean DEFAULT false,
    mail_insertion boolean DEFAULT false,
    "position" integer,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL,
    mail_modif_projet_cree boolean DEFAULT false,
    mail_modif_projet_valide boolean DEFAULT false,
    mail_retard_validation boolean DEFAULT false,
    theme character varying,
    active boolean DEFAULT true,
    mail_error_send_tdt_auto boolean DEFAULT false
);


--
-- Name: votes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE votes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: votes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE votes (
    id integer DEFAULT nextval('votes_id_seq'::regclass) NOT NULL,
    acteur_id integer DEFAULT 0 NOT NULL,
    delib_id integer DEFAULT 0 NOT NULL,
    resultat integer,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY acteurs_seances ALTER COLUMN id SET DEFAULT nextval('acteurs_seances_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY crons ALTER COLUMN id SET DEFAULT nextval('crons_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY deliberations_typeseances ALTER COLUMN id SET DEFAULT nextval('deliberations_typeseances_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY deliberations_users ALTER COLUMN id SET DEFAULT nextval('deliberations_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY infosupdefs_profils ALTER COLUMN id SET DEFAULT nextval('infosupdefs_profils_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY nomenclatures ALTER COLUMN id SET DEFAULT nextval('nomenclatures_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY typeactes ALTER COLUMN id SET DEFAULT nextval('typeactes_id_seq'::regclass);


--
-- Name: acteurs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY acteurs
    ADD CONSTRAINT acteurs_pkey PRIMARY KEY (id);


--
-- Name: acteurs_seances_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY acteurs_seances
    ADD CONSTRAINT acteurs_seances_pkey PRIMARY KEY (id);


--
-- Name: acteurs_services_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY acteurs_services
    ADD CONSTRAINT acteurs_services_pkey PRIMARY KEY (id);


--
-- Name: annexes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY annexes
    ADD CONSTRAINT annexes_pkey PRIMARY KEY (id);


--
-- Name: circuits_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY circuits_users
    ADD CONSTRAINT circuits_users_pkey PRIMARY KEY (id);


--
-- Name: collectivites_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY collectivites
    ADD CONSTRAINT collectivites_pkey PRIMARY KEY (id);


--
-- Name: commentaires_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY commentaires
    ADD CONSTRAINT commentaires_pkey PRIMARY KEY (id);


--
-- Name: compteurs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY compteurs
    ADD CONSTRAINT compteurs_pkey PRIMARY KEY (id);


--
-- Name: crons_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY crons
    ADD CONSTRAINT crons_pkey PRIMARY KEY (id);


--
-- Name: deliberations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY deliberations
    ADD CONSTRAINT deliberations_pkey PRIMARY KEY (id);


--
-- Name: deliberations_seances_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY deliberations_seances
    ADD CONSTRAINT deliberations_seances_pkey PRIMARY KEY (id);


--
-- Name: deliberations_typeseances_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY deliberations_typeseances
    ADD CONSTRAINT deliberations_typeseances_pkey PRIMARY KEY (id);


--
-- Name: deliberations_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY deliberations_users
    ADD CONSTRAINT deliberations_users_pkey PRIMARY KEY (id);


--
-- Name: historiques_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY historiques
    ADD CONSTRAINT historiques_pkey PRIMARY KEY (id);


--
-- Name: infosupdefs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY infosupdefs
    ADD CONSTRAINT infosupdefs_pkey PRIMARY KEY (id);


--
-- Name: infosuplistedefs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY infosuplistedefs
    ADD CONSTRAINT infosuplistedefs_pkey PRIMARY KEY (id);


--
-- Name: infosups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY infosups
    ADD CONSTRAINT infosups_pkey PRIMARY KEY (id);


--
-- Name: listepresences_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY listepresences
    ADD CONSTRAINT listepresences_pkey PRIMARY KEY (id);


--
-- Name: natures_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY natures
    ADD CONSTRAINT natures_pkey PRIMARY KEY (id);


--
-- Name: nomenclatures_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY nomenclatures
    ADD CONSTRAINT nomenclatures_pkey PRIMARY KEY (id);


--
-- Name: profils_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY profils
    ADD CONSTRAINT profils_pkey PRIMARY KEY (id);


--
-- Name: role_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: seances_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY seances
    ADD CONSTRAINT seances_pkey PRIMARY KEY (id);


--
-- Name: sequences_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sequences
    ADD CONSTRAINT sequences_pkey PRIMARY KEY (id);


--
-- Name: services_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY services
    ADD CONSTRAINT services_pkey PRIMARY KEY (id);


--
-- Name: tdt_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tdt_messages
    ADD CONSTRAINT tdt_messages_pkey PRIMARY KEY (id);


--
-- Name: themes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY themes
    ADD CONSTRAINT themes_pkey PRIMARY KEY (id);


--
-- Name: typeactes_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY typeactes
    ADD CONSTRAINT typeactes_id_key UNIQUE (id);


--
-- Name: typeacteurs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY typeacteurs
    ADD CONSTRAINT typeacteurs_pkey PRIMARY KEY (id);


--
-- Name: typeseances_acteurs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY typeseances_acteurs
    ADD CONSTRAINT typeseances_acteurs_pkey PRIMARY KEY (id);


--
-- Name: typeseances_natures_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY typeseances_typeactes
    ADD CONSTRAINT typeseances_natures_pkey PRIMARY KEY (id);


--
-- Name: typeseances_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY typeseances
    ADD CONSTRAINT typeseances_pkey PRIMARY KEY (id);


--
-- Name: typeseances_typeacteurs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY typeseances_typeacteurs
    ADD CONSTRAINT typeseances_typeacteurs_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users_services_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY services_users
    ADD CONSTRAINT users_services_pkey PRIMARY KEY (id);


--
-- Name: votes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY votes
    ADD CONSTRAINT votes_pkey PRIMARY KEY (id);


--
-- Name: INFOSUPDEF_ID_ORDRE; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "INFOSUPDEF_ID_ORDRE" ON infosuplistedefs USING btree (infosupdef_id, ordre);


--
-- Name: acteur_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX acteur_id ON votes USING btree (acteur_id);


--
-- Name: acteurs_actif; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX acteurs_actif ON acteurs USING btree (actif);


--
-- Name: acteursservices_acteur_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX acteursservices_acteur_id ON acteurs_services USING btree (acteur_id);


--
-- Name: acteursservices_service_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX acteursservices_service_id ON acteurs_services USING btree (service_id);


--
-- Name: annexes_joindre; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX annexes_joindre ON annexes USING btree (foreign_key, joindre_fusion);


--
-- Name: deliberation_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX deliberation_id ON votes USING btree (delib_id);


--
-- Name: deliberations_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX deliberations_id ON deliberations_seances USING btree (deliberation_id);


--
-- Name: deliberations_seances_seance; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX deliberations_seances_seance ON deliberations_seances USING btree (seance_id);


--
-- Name: elu; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX elu ON typeacteurs USING btree (elu);


--
-- Name: etat; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX etat ON deliberations USING btree (etat);


--
-- Name: foreign_key; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX foreign_key ON infosups USING btree (foreign_key);


--
-- Name: idx_services_users; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_services_users ON services_users USING btree (user_id);


--
-- Name: index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index ON acteurs USING btree (id);


--
-- Name: infosupdef_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX infosupdef_id ON infosups USING btree (infosupdef_id);


--
-- Name: model; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX model ON infosupdefs USING btree (model);


--
-- Name: model_foreign_key; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX model_foreign_key ON annexes USING btree (model, foreign_key);


--
-- Name: nature_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX nature_id ON deliberations USING btree (typeacte_id);


--
-- Name: parent; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX parent ON deliberations USING btree (parent_id);


--
-- Name: rapporteur_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX rapporteur_id ON deliberations USING btree (rapporteur_id);


--
-- Name: redacteur_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX redacteur_id ON deliberations USING btree (redacteur_id);


--
-- Name: seances_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX seances_id ON deliberations_seances USING btree (seance_id);


--
-- Name: seances_traitee; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX seances_traitee ON seances USING btree (traitee);


--
-- Name: service_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX service_id ON deliberations USING btree (service_id);


--
-- Name: suppleant_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX suppleant_id ON acteurs USING btree (suppleant_id);


--
-- Name: tdtmsg_; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tdtmsg_ ON tdt_messages USING btree (delib_id);


--
-- Name: text; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX text ON infosups USING btree (text);


--
-- Name: theme_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX theme_id ON deliberations USING btree (theme_id);


--
-- Name: themes_left; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX themes_left ON themes USING btree (lft);


--
-- Name: type_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX type_id ON seances USING btree (type_id);


--
-- Name: typeacteur; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX typeacteur ON acteurs USING btree (typeacteur_id);


--
-- Name: typeseance_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX typeseance_id ON typeseances_typeacteurs USING btree (typeseance_id, typeacteur_id);


--
-- Name: typeseancenature_nature; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX typeseancenature_nature ON typeseances_typeactes USING btree (typeacte_id);


--
-- Name: user; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "user" ON historiques USING btree (user_id);


--
-- Name: user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_id ON circuits_users USING btree (user_id);


--
-- Name: username; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX username ON users USING btree (username);


--
-- PostgreSQL database dump complete
--
