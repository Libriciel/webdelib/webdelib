<?php 
class AppSchema extends CakeSchema {

	public function before($event = array()) {
		return true;
	}

	public function after($event = array()) {
	}

	public $acos = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'model' => array('type' => 'string', 'null' => true, 'default' => null),
		'foreign_key' => array('type' => 'integer', 'null' => true, 'default' => null),
		'alias' => array('type' => 'string', 'null' => true, 'default' => null),
		'lft' => array('type' => 'integer', 'null' => true, 'default' => null),
		'rght' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $acteurs = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'typeacteur_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'nom' => array('type' => 'string', 'null' => false, 'length' => 50),
		'prenom' => array('type' => 'string', 'null' => false, 'length' => 50),
		'salutation' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
		'titre' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 250),
		'position' => array('type' => 'integer', 'null' => true, 'default' => null),
		'adresse1' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100),
		'adresse2' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100),
		'cp' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20),
		'ville' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100),
		'email' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100),
		'telfixe' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20),
		'telmobile' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20),
		'suppleant_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'note' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1500),
		'actif' => array('type' => 'boolean', 'null' => false, 'default' => true),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'notif_insertion' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'notif_projet_valide' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'acteurs_actif_idx' => array('unique' => false, 'column' => 'actif'),
			'acteurs_suppleant_id_idx' => array('unique' => false, 'column' => 'suppleant_id'),
			'acteurs_typeacteur_id_idx' => array('unique' => false, 'column' => 'typeacteur_id')
		),
		'tableParameters' => array()
	);

	public $acteurs_seances = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'acteur_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'seance_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'mail_id' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20),
		'date_envoi' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'date_reception' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'model' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $acteurs_services = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'acteur_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'service_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'acteurs_services_acteur_id_idx' => array('unique' => false, 'column' => 'acteur_id'),
			'acteurs_services_service_id_idx' => array('unique' => false, 'column' => 'service_id')
		),
		'tableParameters' => array()
	);

	public $annexes = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'model' => array('type' => 'string', 'null' => false, 'default' => null),
		'foreign_key' => array('type' => 'integer', 'null' => false, 'default' => null),
		'joindre_ctrl_legalite' => array('type' => 'boolean', 'null' => false, 'default' => false),
		'joindre_fusion' => array('type' => 'boolean', 'null' => false, 'default' => false),
		'titre' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 200),
		'filename' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100),
		'filetype' => array('type' => 'string', 'null' => false, 'default' => null),
		'size' => array('type' => 'integer', 'null' => false, 'default' => null),
		'data' => array('type' => 'binary', 'null' => false, 'default' => null),
		'data_pdf' => array('type' => 'binary', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'edition_data' => array('type' => 'binary', 'null' => true, 'default' => null),
		'edition_data_typemime' => array('type' => 'string', 'null' => true, 'default' => null),
		'position' => array('type' => 'smallinteger', 'null' => true, 'default' => null),
		'tdt_data_pdf' => array('type' => 'binary', 'null' => true, 'default' => null),
		'tdt_data_edition' => array('type' => 'binary', 'null' => true, 'default' => null),
		'typologiepiece_code' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 5),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'annexes_foreign_key_joindre_fusion_idx' => array('unique' => false, 'column' => array('foreign_key', 'joindre_fusion')),
			'annexes_model_foreign_key_idx' => array('unique' => false, 'column' => array('model', 'foreign_key'))
		),
		'tableParameters' => array()
	);

	public $annexes_versions = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'version_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'data' => array('type' => 'binary', 'null' => true, 'default' => null),
		'edition_data' => array('type' => 'binary', 'null' => true, 'default' => null),
		'data_pdf' => array('type' => 'binary', 'null' => true, 'default' => null),
		'data_annexe' => array('type' => 'text', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'foreign_key' => array('type' => 'integer', 'null' => false, 'default' => null),
		'user_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'model' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50),
		'tdt_data_edition' => array('type' => 'binary', 'null' => true, 'default' => null),
		'tdt_data_pdf' => array('type' => 'binary', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $aros = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'model' => array('type' => 'string', 'null' => true, 'default' => null),
		'foreign_key' => array('type' => 'integer', 'null' => true, 'default' => null),
		'alias' => array('type' => 'string', 'null' => true, 'default' => null),
		'lft' => array('type' => 'integer', 'null' => true, 'default' => null),
		'rght' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $aros_acos = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'aro_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'aco_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'_create' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 2),
		'_read' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 2),
		'_update' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 2),
		'_delete' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 2),
		'_manager' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 2),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $circuits_users = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'circuit_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'circuits_users_user_id_idx' => array('unique' => false, 'column' => 'user_id')
		),
		'tableParameters' => array()
	);

	public $collectivites = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'id_entity' => array('type' => 'integer', 'null' => true, 'default' => null),
		'nom' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 128),
		'adresse' => array('type' => 'string', 'null' => false, 'default' => null),
		'CP' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 5),
		'ville' => array('type' => 'string', 'null' => false, 'default' => null),
		'telephone' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20),
		'logo' => array('type' => 'binary', 'null' => true, 'default' => null),
		'templateProject' => array('type' => 'text', 'null' => true, 'default' => null),
		'background' => array('type' => 'string', 'null' => true, 'default' => '#42a1e8', 'length' => 25),
		'config_login' => array('type' => 'text', 'null' => true, 'default' => null),
		'siret' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 14),
		'code_ape' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100),
		'email' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100),
		'responsable_nom' => array('type' => 'string', 'null' => true, 'default' => null),
		'responsable_prenom' => array('type' => 'string', 'null' => true, 'default' => null),
		'responsable_fonction' => array('type' => 'string', 'null' => true, 'default' => null),
		'responsable_email' => array('type' => 'string', 'null' => true, 'default' => null),
		'dpo_nom' => array('type' => 'string', 'null' => true, 'default' => null),
		'dpo_prenom' => array('type' => 'string', 'null' => true, 'default' => null),
		'dpo_email' => array('type' => 'string', 'null' => true, 'default' => null),
		'saas' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'force' => array('type' => 'integer', 'null' => true, 'default' => '5'),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $commentaires = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'delib_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'agent_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'texte' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
		'pris_en_compte' => array('type' => 'smallinteger', 'null' => false, 'default' => '0'),
		'commentaire_auto' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $compteurs = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'nom' => array('type' => 'string', 'null' => false, 'default' => null),
		'commentaire' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1500),
		'def_compteur' => array('type' => 'string', 'null' => false, 'default' => null),
		'sequence_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'def_reinit' => array('type' => 'string', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $crons = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'nom' => array('type' => 'string', 'null' => false, 'default' => null),
		'description' => array('type' => 'string', 'null' => true, 'default' => null),
		'plugin' => array('type' => 'string', 'null' => true),
		'action' => array('type' => 'string', 'null' => false, 'default' => null),
		'has_params' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'params' => array('type' => 'string', 'null' => true, 'default' => null),
		'next_execution_time' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'execution_duration' => array('type' => 'string', 'null' => true, 'default' => null),
		'last_execution_start_time' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'last_execution_end_time' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'last_execution_report' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
		'last_execution_status' => array('type' => 'string', 'null' => true, 'default' => null),
		'active' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'created_user_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified_user_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'model' => array('type' => 'string', 'null' => true, 'default' => 'CronJob'),
		'lock' => array('type' => 'boolean', 'null' => false, 'default' => null),
		'run_all' => array('type' => 'boolean', 'null' => true, 'default' => true),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $deliberations = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'typeacte_id' => array('type' => 'integer', 'null' => false, 'default' => '1'),
		'circuit_id' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'theme_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'service_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'redacteur_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'rapporteur_id' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'anterieure_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'is_multidelib' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'objet' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1000),
		'objet_delib' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1000),
		'titre' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1000),
		'num_delib' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 15),
		'num_pref' => array('type' => 'string', 'null' => true, 'default' => null),
		'pastell_id' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10),
		'tdt_id' => array('type' => 'string', 'null' => true, 'default' => null),
		'tdt_dateAR' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100),
		'texte_projet' => array('type' => 'binary', 'null' => true, 'default' => null),
		'texte_projet_name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 75),
		'texte_projet_type' => array('type' => 'string', 'null' => true, 'default' => null),
		'texte_projet_size' => array('type' => 'integer', 'null' => true, 'default' => null),
		'texte_synthese' => array('type' => 'binary', 'null' => true, 'default' => null),
		'texte_synthese_name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 75),
		'texte_synthese_type' => array('type' => 'string', 'null' => true, 'default' => null),
		'texte_synthese_size' => array('type' => 'integer', 'null' => true, 'default' => null),
		'deliberation' => array('type' => 'binary', 'null' => true, 'default' => null),
		'deliberation_name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 75),
		'deliberation_type' => array('type' => 'string', 'null' => true, 'default' => null),
		'deliberation_size' => array('type' => 'integer', 'null' => true, 'default' => null),
		'date_limite' => array('type' => 'date', 'null' => true, 'default' => null),
		'date_envoi' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'etat' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'parapheur_etat' => array('type' => 'smallinteger', 'null' => true, 'default' => null),
		'parapheur_commentaire' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
		'sae_etat' => array('type' => 'integer', 'null' => true, 'default' => null),
		'reporte' => array('type' => 'boolean', 'null' => false, 'default' => false),
		'montant' => array('type' => 'integer', 'null' => true, 'default' => null),
		'debat' => array('type' => 'binary', 'null' => true, 'default' => null),
		'debat_name' => array('type' => 'string', 'null' => true, 'default' => null),
		'debat_size' => array('type' => 'integer', 'null' => true, 'default' => null),
		'debat_type' => array('type' => 'string', 'null' => true, 'default' => null),
		'avis' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'vote_nb_oui' => array('type' => 'integer', 'null' => true, 'default' => null),
		'vote_nb_non' => array('type' => 'integer', 'null' => true, 'default' => null),
		'vote_nb_abstention' => array('type' => 'integer', 'null' => true, 'default' => null),
		'vote_nb_retrait' => array('type' => 'integer', 'null' => true, 'default' => null),
		'vote_commentaire' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1500),
		'delib_pdf' => array('type' => 'binary', 'null' => true, 'default' => null),
		'signature' => array('type' => 'binary', 'null' => true, 'default' => null),
		'signee' => array('type' => 'boolean', 'null' => false, 'default' => false),
		'commission' => array('type' => 'binary', 'null' => true, 'default' => null),
		'commission_size' => array('type' => 'integer', 'null' => true, 'default' => null),
		'commission_type' => array('type' => 'string', 'null' => true, 'default' => null),
		'commission_name' => array('type' => 'string', 'null' => true, 'default' => null),
		'date_acte' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'date_envoi_signature' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'parapheur_id' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50),
		'tdt_data_pdf' => array('type' => 'binary', 'null' => true, 'default' => null),
		'tdt_data_bordereau_pdf' => array('type' => 'binary', 'null' => true, 'default' => null),
		'president_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'parapheur_cible' => array('type' => 'string', 'null' => true, 'default' => null),
		'parapheur_bordereau' => array('type' => 'binary', 'null' => true, 'default' => null),
		'tdt_ar' => array('type' => 'binary', 'null' => true, 'default' => null),
		'tdt_ar_date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'vote_prendre_acte' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'signature_type' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100),
		'numero_depot' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'texte_projet_active' => array('type' => 'boolean', 'null' => false, 'default' => false),
		'texte_synthese_active' => array('type' => 'boolean', 'null' => false, 'default' => false),
		'texte_acte_active' => array('type' => 'boolean', 'null' => false, 'default' => false),
		'tdt_data_edition' => array('type' => 'binary', 'null' => true, 'default' => null),
		'vote_resultat' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'tdt_message' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
		'tdt_status' => array('type' => 'integer', 'null' => true, 'default' => null),
		'tdt_document_papier' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'typologiepiece_code' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 5),
		'publier_etat' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'publier_date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'sae_date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'sae_commentaire' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1500),
		'sae_atr' => array('type' => 'binary', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'deliberations_etat_idx' => array('unique' => false, 'column' => 'etat'),
			'deliberations_parent_id_idx' => array('unique' => false, 'column' => 'parent_id'),
			'deliberations_rapporteur_id_idx' => array('unique' => false, 'column' => 'rapporteur_id'),
			'deliberations_redacteur_id_idx' => array('unique' => false, 'column' => 'redacteur_id'),
			'deliberations_service_id_idx' => array('unique' => false, 'column' => 'service_id'),
			'deliberations_theme_id_idx' => array('unique' => false, 'column' => 'theme_id'),
			'deliberations_typeacte_id_idx' => array('unique' => false, 'column' => 'typeacte_id')
		),
		'tableParameters' => array()
	);

	public $deliberations_seances = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'deliberation_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'seance_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'position' => array('type' => 'integer', 'null' => true, 'default' => null),
		'avis' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'commentaire' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1500),
		'reset' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'deliberations_seances_deliberation_id_idx' => array('unique' => false, 'column' => 'deliberation_id'),
			'deliberations_seances_seance_id_idx' => array('unique' => false, 'column' => 'seance_id')
		),
		'tableParameters' => array()
	);

	public $deliberations_typeseances = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'deliberation_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'typeseance_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $deliberations_users = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'deliberation_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $deliberations_versions = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'version_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'model' => array('type' => 'string', 'null' => false, 'default' => null),
		'foreign_key' => array('type' => 'integer', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'tdt_data_edition' => array('type' => 'binary', 'null' => true, 'default' => null),
		'tdt_ar' => array('type' => 'binary', 'null' => true, 'default' => null),
		'parapheur_bordereau' => array('type' => 'binary', 'null' => true, 'default' => null),
		'commission' => array('type' => 'binary', 'null' => true, 'default' => null),
		'tdt_data_bordereau' => array('type' => 'binary', 'null' => true, 'default' => null),
		'delib_pdf' => array('type' => 'binary', 'null' => true, 'default' => null),
		'debat' => array('type' => 'binary', 'null' => true, 'default' => null),
		'deliberation' => array('type' => 'binary', 'null' => true, 'default' => null),
		'texte_synthese' => array('type' => 'binary', 'null' => true, 'default' => null),
		'texte_projet' => array('type' => 'binary', 'null' => true, 'default' => null),
		'signature' => array('type' => 'binary', 'null' => true, 'default' => null),
		'tdt_data_pdf' => array('type' => 'binary', 'null' => true, 'default' => null),
		'data_deliberation' => array('type' => 'text', 'null' => true, 'default' => null),
		'user_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $historiques = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'delib_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'circuit_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'commentaire' => array('type' => 'text', 'null' => false, 'default' => null, 'length' => 1073741824),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'revision_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'historiques_user_id_idx' => array('unique' => false, 'column' => 'user_id')
		),
		'tableParameters' => array()
	);

	public $infosupdefs = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'model' => array('type' => 'string', 'null' => false, 'default' => 'Deliberation', 'length' => 25),
		'nom' => array('type' => 'string', 'null' => false, 'default' => null),
		'commentaire' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1500),
		'ordre' => array('type' => 'integer', 'null' => false, 'default' => null),
		'code' => array('type' => 'string', 'null' => false, 'default' => null),
		'type' => array('type' => 'string', 'null' => false, 'default' => null),
		'val_initiale' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1000),
		'recherche' => array('type' => 'boolean', 'null' => false, 'default' => false),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'actif' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'typemime' => array('type' => 'string', 'null' => true, 'default' => null),
		'joindre_fusion' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'reactcomponent' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'edit_post_sign' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'api' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 150),
		'api_type' => array('type' => 'string', 'null' => true, 'default' => null),
		'api_limit' => array('type' => 'string', 'null' => true, 'default' => null),
		'api_query' => array('type' => 'string', 'null' => true, 'default' => null),
		'correlation' => array('type' => 'string', 'null' => true, 'default' => null),
		'joindre_sae' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'infosupdefs_model_idx' => array('unique' => false, 'column' => 'model')
		),
		'tableParameters' => array()
	);

	public $infosupdefs_profils = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'profil_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'infosupdef_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $infosuplistedefs = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'infosupdef_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'ordre' => array('type' => 'integer', 'null' => false, 'default' => null),
		'nom' => array('type' => 'string', 'null' => false, 'default' => null),
		'actif' => array('type' => 'boolean', 'null' => false, 'default' => true),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'infosuplistedefs_infosupdef_id_ordre_idx' => array('unique' => false, 'column' => array('infosupdef_id', 'ordre'))
		),
		'tableParameters' => array()
	);

	public $infosups = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'model' => array('type' => 'string', 'null' => false, 'default' => 'Deliberation', 'length' => 25),
		'foreign_key' => array('type' => 'integer', 'null' => false, 'default' => null),
		'infosupdef_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'text' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1000),
		'date' => array('type' => 'date', 'null' => true, 'default' => null),
		'file_name' => array('type' => 'string', 'null' => true, 'default' => null),
		'file_size' => array('type' => 'integer', 'null' => true, 'default' => null),
		'file_type' => array('type' => 'string', 'null' => true, 'default' => null),
		'content' => array('type' => 'binary', 'null' => true, 'default' => null),
		'edition_data' => array('type' => 'binary', 'null' => true, 'default' => null),
		'edition_data_typemime' => array('type' => 'string', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'geojson' => array('type' => 'text', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'infosups_foreign_key_idx' => array('unique' => false, 'column' => 'foreign_key'),
			'infosups_infosupdef_id_idx' => array('unique' => false, 'column' => 'infosupdef_id')
		),
		'tableParameters' => array()
	);

	public $ldapm_groups = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'lft' => array('type' => 'integer', 'null' => true, 'default' => null),
		'rght' => array('type' => 'integer', 'null' => true, 'default' => null),
		'name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 500),
		'dn' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $ldapm_models_groups = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'ldapm_groups_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'model' => array('type' => 'string', 'null' => true, 'default' => null),
		'foreign_key' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $liste_presences_globales = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'seance_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'acteur_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'present' => array('type' => 'boolean', 'null' => false, 'default' => null),
		'mandataire' => array('type' => 'integer', 'null' => true, 'default' => null),
		'suppleant_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $listepresences = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'delib_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'acteur_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'present' => array('type' => 'boolean', 'null' => false, 'default' => null),
		'mandataire' => array('type' => 'integer', 'null' => true, 'default' => null),
		'suppleant_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $modelsections = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => true, 'default' => null),
		'description' => array('type' => 'string', 'null' => true, 'default' => null),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => '1'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $modeltemplates = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'modeltype_id' => array('type' => 'integer', 'null' => true, 'default' => '1'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null),
		'filename' => array('type' => 'string', 'null' => false, 'default' => null),
		'filesize' => array('type' => 'integer', 'null' => true, 'default' => null),
		'content' => array('type' => 'binary', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'file_output_format' => array('type' => 'string', 'null' => true, 'default' => null),
		'force' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $modeltypes = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => true, 'default' => null),
		'description' => array('type' => 'string', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $modelvalidations = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'modelvariable_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'modelsection_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'modeltype_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'min' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'max' => array('type' => 'integer', 'null' => true, 'default' => null),
		'actif' => array('type' => 'boolean', 'null' => true, 'default' => true),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $modelvariables = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => true, 'default' => null),
		'description' => array('type' => 'string', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $natures = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100),
		'code' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 3),
		'default' => array('type' => 'string', 'null' => true, 'default' => '99_AU', 'length' => 5),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $natures_typologiepieces = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'nature_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'typologiepiece_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $nomenclatures = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'name' => array('type' => 'string', 'null' => false, 'default' => null),
		'libelle' => array('type' => 'string', 'null' => false, 'default' => null),
		'lft' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'rght' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $phinxlog = array(
		'version' => array('type' => 'biginteger', 'null' => false, 'default' => null),
		'migration_name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100),
		'start_time' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'end_time' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'breakpoint' => array('type' => 'boolean', 'null' => false, 'default' => false),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'version')
		),
		'tableParameters' => array()
	);

	public $profils = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'name' => array('type' => 'string', 'null' => false, 'length' => 100),
		'actif' => array('type' => 'boolean', 'null' => false, 'default' => true),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'role_id' => array('type' => 'integer', 'null' => false, 'default' => '1'),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $roles = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'name' => array('type' => 'text', 'null' => false, 'default' => null, 'length' => 1073741824),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $seances = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'type_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'date_convocation' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'traitee' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'commentaire' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1500),
		'secretaire_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'president_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'debat_global' => array('type' => 'binary', 'null' => true, 'default' => null),
		'debat_global_name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 75),
		'debat_global_size' => array('type' => 'integer', 'null' => true, 'default' => null),
		'debat_global_type' => array('type' => 'string', 'null' => true, 'default' => null),
		'pv_figes' => array('type' => 'smallinteger', 'null' => true, 'default' => null),
		'pv_sommaire' => array('type' => 'binary', 'null' => true, 'default' => null),
		'pv_complet' => array('type' => 'binary', 'null' => true, 'default' => null),
		'numero_depot' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'idelibre_id' => array('type' => 'string', 'null' => true, 'default' => null),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'lft' => array('type' => 'integer', 'null' => true, 'default' => null),
		'rght' => array('type' => 'integer', 'null' => true, 'default' => null),
		'idelibre_votes_warning' => array('type' => 'text', 'null' => true, 'default' => null),
		'sae_numero_versement' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'sae_pastell_id' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10),
		'sae_etat' => array('type' => 'integer', 'null' => true, 'default' => null),
		'sae_date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'sae_commentaire' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1500),
		'sae_atr' => array('type' => 'binary', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'seances_lft_rght_idx' => array('unique' => false, 'column' => array('lft', 'rght')),
			'seances_traitee_idx' => array('unique' => false, 'column' => 'traitee'),
			'seances_type_id_idx' => array('unique' => false, 'column' => 'type_id')
		),
		'tableParameters' => array()
	);

	public $sequences = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'nom' => array('type' => 'string', 'null' => false, 'default' => null),
		'commentaire' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1500),
		'num_sequence' => array('type' => 'integer', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'debut_validite' => array('type' => 'date', 'null' => true, 'default' => null),
		'fin_validite' => array('type' => 'date', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $services = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'order' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100),
		'circuit_defaut_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'actif' => array('type' => 'boolean', 'null' => false, 'default' => true),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'lft' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'rght' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $services_users = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'service_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'services_users_user_id_idx' => array('unique' => false, 'column' => 'user_id')
		),
		'tableParameters' => array()
	);

	public $tdt_messages = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'delib_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'tdt_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'tdt_type' => array('type' => 'integer', 'null' => false, 'default' => null),
		'tdt_etat' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'date_message' => array('type' => 'date', 'null' => true, 'default' => null),
		'tdt_data' => array('type' => 'binary', 'null' => true, 'default' => null),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'tdt_messages_delib_id_idx' => array('unique' => false, 'column' => 'delib_id')
		),
		'tableParameters' => array()
	);

	public $themes = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'order' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
		'libelle' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 500),
		'actif' => array('type' => 'boolean', 'null' => false, 'default' => true),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'lft' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'rght' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $tokensmethods = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'controller' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 250),
		'action' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 250),
		'php_sid' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 32),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'params' => array('type' => 'string', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'tokensmethods_controller_action_params_idx' => array('unique' => true, 'column' => array('controller', 'action', 'params')),
			'tokensmethods_action_idx' => array('unique' => false, 'column' => 'action'),
			'tokensmethods_controller_idx' => array('unique' => false, 'column' => 'controller'),
			'tokensmethods_php_sid_idx' => array('unique' => false, 'column' => 'php_sid'),
			'tokensmethods_user_id_idx' => array('unique' => false, 'column' => 'user_id')
		),
		'tableParameters' => array()
	);

	public $typeactes = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 300),
		'modele_projet_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'modele_final_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'nature_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'compteur_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'created' => array('type' => 'date', 'null' => false, 'default' => null),
		'modified' => array('type' => 'date', 'null' => false, 'default' => null),
		'gabarit_projet' => array('type' => 'binary', 'null' => true, 'default' => null),
		'gabarit_synthese' => array('type' => 'binary', 'null' => true, 'default' => null),
		'gabarit_acte' => array('type' => 'binary', 'null' => true, 'default' => null),
		'teletransmettre' => array('type' => 'boolean', 'null' => false, 'default' => false),
		'gabarit_acte_name' => array('type' => 'string', 'null' => true, 'default' => null),
		'gabarit_projet_name' => array('type' => 'string', 'null' => true, 'default' => null),
		'gabarit_synthese_name' => array('type' => 'string', 'null' => true, 'default' => null),
		'gabarit_projet_active' => array('type' => 'boolean', 'null' => false, 'default' => false),
		'gabarit_synthese_active' => array('type' => 'boolean', 'null' => false, 'default' => false),
		'gabarit_acte_active' => array('type' => 'boolean', 'null' => false, 'default' => false),
		'modele_bordereau_projet_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'deliberant' => array('type' => 'boolean', 'null' => false, 'default' => false),
		'publier' => array('type' => 'boolean', 'null' => false, 'default' => false),
		'verser_sae' => array('type' => 'boolean', 'null' => false, 'default' => false),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $typeacteurs = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'nom' => array('type' => 'string', 'null' => false, 'default' => null),
		'commentaire' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1500),
		'elu' => array('type' => 'boolean', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'actif' => array('type' => 'boolean', 'null' => true, 'default' => true),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $typeseances = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'libelle' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 300),
		'retard' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'action' => array('type' => 'smallinteger', 'null' => false, 'default' => null),
		'compteur_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'modele_projet_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'modele_deliberation_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'modele_convocation_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'modele_ordredujour_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'modele_pvsommaire_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'modele_pvdetaille_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'color' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 7),
		'modele_journal_seance_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'lft' => array('type' => 'integer', 'null' => true, 'default' => null),
		'rght' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			'typeseances_lft_rght_idx' => array('unique' => false, 'column' => array('lft', 'rght'))
		),
		'tableParameters' => array()
	);

	public $typeseances_acteurs = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'typeseance_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'acteur_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $typeseances_typeactes = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'typeseance_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'typeacte_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'typeseances_typeactes_typeacte_id_idx' => array('unique' => false, 'column' => 'typeacte_id')
		),
		'tableParameters' => array()
	);

	public $typeseances_typeacteurs = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'typeseance_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'typeacteur_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'typeseances_typeacteurs_typeseance_id_typeacteur_id_idx' => array('unique' => false, 'column' => array('typeseance_id', 'typeacteur_id'))
		),
		'tableParameters' => array()
	);

	public $typologiepieces = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null),
		'code' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 5),
		'lft' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'rght' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $users = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'profil_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'statut' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'username' => array('type' => 'string', 'null' => false, 'length' => 50),
		'note' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1500),
		'circuit_defaut_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'password' => array('type' => 'string', 'null' => false, 'length' => 100),
		'nom' => array('type' => 'string', 'null' => false, 'length' => 50),
		'prenom' => array('type' => 'string', 'null' => false, 'length' => 50),
		'email' => array('type' => 'string', 'null' => false),
		'telfixe' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20),
		'telmobile' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20),
		'date_naissance' => array('type' => 'date', 'null' => true, 'default' => null),
		'accept_notif' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'mail_refus' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'mail_traitement' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'mail_insertion' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'position' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'mail_modif_projet_cree' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'mail_modif_projet_valide' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'mail_retard_validation' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'theme' => array('type' => 'string', 'null' => true, 'default' => null),
		'active' => array('type' => 'boolean', 'null' => true, 'default' => true),
		'mail_error_send_tdt_auto' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'mail_projet_valide' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'mail_projet_valide_valideur' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'mail_majtdtcourrier' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'service_defaut_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'preference_first_login_active' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'mail_error_sae' => array('type' => 'boolean', 'null' => true, 'default' => false),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'users_username_idx' => array('unique' => true, 'column' => 'username')
		),
		'tableParameters' => array()
	);

	public $versions = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'subject' => array('type' => 'string', 'null' => false, 'default' => null),
		'version' => array('type' => 'string', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => array('id', 'subject', 'version'))
		),
		'tableParameters' => array()
	);

	public $votes = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'acteur_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'delib_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'resultat' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'votes_acteur_id_idx' => array('unique' => false, 'column' => 'acteur_id'),
			'votes_delib_id_idx' => array('unique' => false, 'column' => 'delib_id')
		),
		'tableParameters' => array()
	);

	public $wkf_circuits = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'nom' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 250),
		'description' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
		'actif' => array('type' => 'boolean', 'null' => false, 'default' => true),
		'defaut' => array('type' => 'boolean', 'null' => false, 'default' => false),
		'created_user_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'modified_user_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'wkf_circuits_created_user_id_idx' => array('unique' => false, 'column' => 'created_user_id'),
			'wkf_circuits_modified_user_id_idx' => array('unique' => false, 'column' => 'modified_user_id')
		),
		'tableParameters' => array()
	);

	public $wkf_compositions = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'etape_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'type_validation' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1),
		'trigger_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created_user_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'modified_user_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'soustype' => array('type' => 'integer', 'null' => true, 'default' => null),
		'type_composition' => array('type' => 'string', 'null' => true, 'default' => 'USER', 'length' => 20),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'wkf_compositions_etape_id_idx' => array('unique' => false, 'column' => 'etape_id'),
			'wkf_compositions_trigger_id_idx' => array('unique' => false, 'column' => 'trigger_id')
		),
		'tableParameters' => array()
	);

	public $wkf_etapes = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'circuit_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'nom' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 250),
		'description' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
		'type' => array('type' => 'integer', 'null' => false, 'default' => null),
		'ordre' => array('type' => 'integer', 'null' => false, 'default' => null),
		'created_user_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'modified_user_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'soustype' => array('type' => 'integer', 'null' => true, 'default' => null),
		'cpt_retard' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'wkf_etapes_circuit_id_idx' => array('unique' => false, 'column' => 'circuit_id'),
			'wkf_etapes_nom_idx' => array('unique' => false, 'column' => 'nom')
		),
		'tableParameters' => array()
	);

	public $wkf_signatures = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'type_signature' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100),
		'signature' => array('type' => 'text', 'null' => false, 'default' => null, 'length' => 1073741824),
		'visa_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

	public $wkf_traitements = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'circuit_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'target_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'numero_traitement' => array('type' => 'integer', 'null' => false, 'default' => '1'),
		'treated_orig' => array('type' => 'smallinteger', 'null' => false, 'default' => '0'),
		'created_user_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'modified_user_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'treated' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'wkf_traitements_circuit_id_idx' => array('unique' => false, 'column' => 'circuit_id'),
			'wkf_traitements_target_id_idx' => array('unique' => false, 'column' => 'target_id'),
			'wkf_traitements_treated_orig_idx' => array('unique' => false, 'column' => 'treated_orig')
		),
		'tableParameters' => array()
	);

	public $wkf_visas = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'traitement_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'trigger_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'signature_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'etape_nom' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 250),
		'etape_type' => array('type' => 'integer', 'null' => false, 'default' => null),
		'action' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 2),
		'commentaire' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
		'date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'numero_traitement' => array('type' => 'integer', 'null' => false, 'default' => null),
		'type_validation' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1),
		'etape_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'date_retard' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'wkf_visas_traitement_id_idx' => array('unique' => false, 'column' => 'traitement_id'),
			'wkf_visas_trigger_id_idx' => array('unique' => false, 'column' => 'trigger_id')
		),
		'tableParameters' => array()
	);

}
