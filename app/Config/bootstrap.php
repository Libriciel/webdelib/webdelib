<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

setlocale(LC_ALL, ['fr_FR', 'fr_FR@euro', 'fr', 'fr_FR.UTF8']);

// Setup a 'default' cache configuration for use in the application.
Cache::config('default', ['engine' => 'File']);

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 *
 * App::build([
 *     'Model'                     => ['/path/to/models/', '/next/path/to/models/'],
 *     'Model/Behavior'            => ['/path/to/behaviors/', '/next/path/to/behaviors/'],
 *     'Model/Datasource'          => ['/path/to/datasources/', '/next/path/to/datasources/'],
 *     'Model/Datasource/Database' => ['/path/to/databases/', '/next/path/to/database/'],
 *     'Model/Datasource/Session'  => ['/path/to/sessions/', '/next/path/to/sessions/'],
 *     'Controller'                => ['/path/to/controllers/', '/next/path/to/controllers/'],
 *     'Controller/Component'      => ['/path/to/components/', '/next/path/to/components/'],
 *     'Controller/Component/Auth' => ['/path/to/auths/', '/next/path/to/auths/'],
 *     'Controller/Component/Acl'  => ['/path/to/acls/', '/next/path/to/acls/'],
 *     'View'                      => ['/path/to/views/', '/next/path/to/views/'],
 *     'View/Helper'               => ['/path/to/helpers/', '/next/path/to/helpers/'],
 *     'Console'                   => ['/path/to/consoles/', '/next/path/to/consoles/'],
 *     'Console/Command'           => ['/path/to/commands/', '/next/path/to/commands/'],
 *     'Console/Command/Task'      => ['/path/to/tasks/', '/next/path/to/tasks/'],
 *     'Lib'                       => ['/path/to/libs/', '/next/path/to/libs/'],
 *     'Locale'                    => ['/path/to/locales/', '/next/path/to/locales/'],
 *     'Vendor'                    => ['/path/to/vendors/', '/next/path/to/vendors/'],
 *     'Plugin'                    => ['/path/to/plugins/', '/next/path/to/plugins/'],
 * ]);
 *
 */
/**
 * Custom Inflector rules can be set to correctly pluralize or singularize table, model,
 * controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', ['rules' => [], 'irregular' => [], 'uninflected' => []));
 * Inflector::rules('plural', ['rules' => [], 'irregular' => [], 'uninflected' => []));
 *
 */
Inflector::rules('plural', [
    'irregular' => [
        'acteur_service' => 'acteurs_services',
        'aro_aco' => 'aros_acos',
        'typeseance_acteur' => 'typeseances_acteurs',
        'typeseance_typeacteur' => 'typeseances_typeacteurs',
        'user_circuit' => 'users_circuits',
        'deliberation_user' => 'deliberations_users',
        'infosupdef' => 'infosupdefs',
        'infosuplistedef' => 'infosuplistedefs',
        'tokenmethod' => 'tokensmethods',
        'liste_presence_globale' => 'liste_presences_globales',
]]);

/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. Make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); //Loads a single plugin named DebugKit
 *
 */
CakePlugin::loadAll([
    //['bootstrap' => true],
    //'DebugKit' => ['bootstrap' => false, 'routes' => false],
    'Appchecks' => ['bootstrap' => false, 'routes' => false],
    //'Database' => ['bootstrap' => false, 'routes' => false],
    'Bootstrap3' => ['bootstrap' => false, 'routes' => false]
        //'ConnectorManager' => ['bootstrap' => true, 'routes' => false],
        //'Rest' => ['bootstrap' => false, 'routes' => false]
]);

/**
 * You can attach event listeners to the request lifecycle as Dispatcher Filter. By default CakePHP bundles two filters:
 *
 * - AssetDispatcher filter will serve your asset files (css, images, js, etc) from your themes and plugins
 * - CacheDispatcher filter will read the Cache.check configure variable
 *  and try to serve cached content generated from controllers
 *
 * Feel free to remove or add filters as you see fit for your application. A few examples:
 *
 * Configure::write('Dispatcher.filters', [
 * 'MyCacheFilter',
 * // will use MyCacheFilter class from the Routing/Filter package in your app.
 * 'MyCacheFilter' => ['prefix' => 'my_cache_'],
 * //  will use MyCacheFilter class from the Routing/Filter package in your app with settings array.
 * 'MyPlugin.MyFilter',
 * // will use MyFilter class from the Routing/Filter package in MyPlugin plugin.
 * ['callable' => $aFunction, 'on' => 'before', 'priority' => 9],
 * // A valid PHP callback type to be called on beforeDispatch
 * ['callable' => $anotherMethod, 'on' => 'after'],
 * // A valid PHP callback type to be called on afterDispatch
 * ));
 */
Configure::write('Dispatcher.filters', [
    'AssetDispatcher',
    'CacheDispatcher'
]);

/**
 * Configures default file logging options
 */
App::uses('Docker', 'Lib/Log/Engine');

CakeLog::config('debug', [
    'engine' => 'Docker',
    'types' => ['debug'],
    'stream' => 'php://stdout',
]);
CakeLog::config('info', [
    'engine' => 'Docker',
    'types' => ['info'],
    'stream' => 'php://stdout',
]);
CakeLog::config('error', [
    'engine' => 'Docker',
    'types' => ['notice', 'warning', 'error', 'critical', 'alert', 'emergency'],
]);
CakeLog::config('connector', [
    'engine' => 'Docker',
    'size' => false,
    'types' => ['info', 'warning', 'error'],
    'scopes' => ['connector', 'idelibre', 'parapheur', 'pastell', 'tdt', 'cas', 'mailsec'],
    'stream' => 'php://stdout',
]);


// Load composer autoload
require_once ROOT . DS . 'vendors' . DS . 'autoload.php';

// Remove and re-prepend CakePHP's autoloader as composer thinks it is the most important.
// See https://github.com/composer/composer/commit/c80cb76b9b5082ecc3e5b53b1050f76bb27b127b
// See http://goo.gl/kKVJO7
spl_autoload_unregister(['App', 'load']);
spl_autoload_register(['App', 'load'], true, true);

Configure::write('I18n.preferApp', true);

//Configuration du plugin Cakeflow
Configure::write('Cakeflow.app', 'webdelib');

Configure::write('Cakeflow.user.model', 'User');
Configure::write('Cakeflow.user.idsession', 'user.User.id');
Configure::write('Cakeflow.user.fields', 'prenom,nom,username');
Configure::write('Cakeflow.user.format', '%s %s (%s)');

Configure::write('Cakeflow.trigger.model', 'User');
Configure::write('Cakeflow.trigger.fields', 'prenom,nom,username');
Configure::write('Cakeflow.trigger.format', '%s %s (%s)');
Configure::write('Cakeflow.trigger.title', 'Utilisateur');

Configure::write('Cakeflow.target.model', 'Deliberation');

Configure::write('Cakeflow.manage.parapheur', false);
Configure::write('Cakeflow.manage.default', false);
Configure::write('Cakeflow.actionsOutOfTreatment', 'ST,JS,IN');
Configure::write('Cakeflow.trigger.parapheur', -1);
Configure::write("Cakeflow.etapesType", [
  'simple'=> ['value' => 1, 'label' => __('Simple')],
  'concurrent'=> ['value' => 2, 'label' => __('Concurrent [OU]')],
  'collaboratif'=> ['value' => 3, 'label' => __('Collaboratif [ET]')]]);
Configure::write('Cakeflow.models_circuit_default', ['User', 'Service']);

//Configuration du plugin ModelOdtValidator
Configure::write('ModelOdtValidator.appContainer', 'WEBDELIB');
Configure::write('ModelOdtValidator.models', ['Typeacte', 'Typeseance']);
Configure::write('ModelOdtValidator.file.output.format.model', 'Template');
Configure::write('ModelOdtValidator.modelsType', [
    'Projet',
    'Deliberation',
    'Convocation',
    'Ordredujour',
    'PvSommaire',
    'PvDetaille',
    'Recherche',
    'Multiseance',
    'BordereauProjet',
    'JournalSeance',
]);
Configure::write('ModelOdtValidator.modelsSection', [
    'Document',
    'Projet',
    'Seances',
    'AvisProjet',
    'Annexes',
]);

//Configuration du plugin AuthManager
Configure::write('AuthManager.ignorePlugins', [
    'Database',
    'Localized',
    'FusionConv',
    'DebugKit',
    'LdapManager',
    'AuthManager',
    'Bootstrap3',
    //dev
    'ConnectorManager',
    'Rest',
    'BsHelpers',
    'Postgres'
]);
Configure::write('AuthManager.ignoreActions', ['versioningEnable', 'getFQDNName','getTenantName','getFqdnDataBases']);
Configure::write('AuthManager.ignoreControllers', [
    'CakeError',
    'Dossiers',
    'Infosuplistedefs',
    'Allos',
    'Annexes',
    'Commentaires',
    'Webdav',
    'Infosups',
    'Models',
    'Natures',
    'Cakeflow.Traitements',
    'ModelOdtValidator.Debug',
    'ModelOdtValidator.Modelsections',
    'ModelOdtValidator.Modelvariables',
    'ModelOdtValidator.Modelvalidations',
    'Pages',
    'Wopi',
    'Versions',
    'Debats',
    'VerserSaeSeances',
    'Duplicate',
    'TraitementParLot',
    'ExportGedSeances',
    'Notifications'
]);
Configure::write('AuthManager.models', [
    'Profil',
    'User',
    'Typeacte',
    'Typeseance',
    'Cakeflow.Circuit',
    'Service',
]);

Configure::write('AuthManager.aros', ['Profil', 'User']);
//Affichage des droits
Configure::write(
    'AuthManager.configCRUD',
    [
    'CRUD' => [
        'controllers' => ['noCrud' => true, 'auto-select' => true],
        'Preferences' => ['noCrud' => true],
        'changeUserMdp' => ['noCrud' => true],
        'changeFormatSortie' => ['noCrud' => true],
        //
        'Projets' => [],
        'mesProjetsValidation' => ['noCrud' => true],
        'mesProjetsATraiter' => ['noCrud' => true],
        'mesProjetsValides' => ['noCrud' => true],
        'projetsMonService' => ['noCrud' => true],
        'editerTous' => ['noCrud' => true],
        'supprimerTous' => ['noCrud' => true],
        'duplicate' => ['noCrud' => true],
        //
        'Deliberations' => ['noCrud' => true, 'auto-select' => true],
        'tousLesProjetsSansSeance' => ['noCrud' => true],
        'tousLesProjetsValidation' => ['noCrud' => true],
        'tousLesProjetsAFaireVoter' => ['noCrud' => true],
        //
        'AutresActes' => ['noCrud' => true, 'auto-select' => true],
        'autresActesAValider' => ['noCrud' => true],
        'autresActesValides' => ['noCrud' => true],
        'autresActesAEnvoyer' => ['noCrud' => true],
        'autresActesEnvoyes' => ['noCrud' => true],
        'nonTransmis' => ['noCrud' => true],
        'autresActesAbandon' => ['noCrud' => true],
        //
        'Actes' => ['noCrud' => true, 'auto-select' => true],
        'editPostSign' => ['noCrud' => true],
        //
        'Validations' => ['noCrud' => true, 'auto-select' => true],
        'attribuerCircuit' => ['noCrud' => true],
        'reattribuerTous' => ['noCrud' => true],
        'retour' => ['noCrud' => true],
        'rebond' => ['noCrud' => true],
        'goNext' => ['noCrud' => true],
        'validerEnUrgence' => ['noCrud' => true],
        //
        'TableauDeBord' => ['noCrud' => true],
        //
        'Search' => ['noCrud' => true, 'auto-select' => true],
        'tousLesProjetsRecherche' => ['noCrud' => true],
        //
        'Signatures' => ['noCrud' => true, 'auto-select' => true],
        'sendDeliberationsToSignature' => ['noCrud' => true],
        'deliberationCancelSignature' => ['noCrud' => true],
        'sendAutresActesToSignature' => ['noCrud' => true],
        'autresActesCancelSignature' => ['noCrud' => true],
        //
        'Teletransmettre' => ['noCrud' => true, 'auto-select' => true],
        'deliberationCancelSendToTDT' => ['noCrud' => true],
        'autresActesCancelSendToTDT' => ['noCrud' => true],
        //
        'ExportGed' => ['noCrud' => true, 'auto-select' => true],
        'sendToGedDeliberation' => ['noCrud' => true],
        'sendToGedAutreActe' => ['noCrud' => true],
        //
        'Publier' => ['noCrud' => true, 'auto-select' => true],
        'sendToPublishDeliberation' => ['noCrud' => true],
        'sendToPublish' => ['noCrud' => true],
        //
        'VerserSae' => ['noCrud' => true, 'auto-select' => true],
        'sendToSaeDeliberation' => ['noCrud' => true],
        'sendToSaeAutresActes' => ['noCrud' => true],
        //
        'Seances'=> [],
        'calendrier' => ['noCrud' => true],
        'ExportGedSeances' => ['noCrud' => true],
        //
        'Votes' => [],
        'sendSeanceVotesToIdelibre' => ['noCrud' => true],
        'importVotesIdelibre' => ['noCrud' => true],
        'downloadVotesElectroniqueJson' => ['noCrud' => true],
        //
        'Avis' => [],
        'sendSeanceAvisToIdelibre' => ['noCrud' => true],
        //
        'Postseances' => ['noCrud' => true],
        'toSend' => ['noCrud' => true],
        'transmit' => ['noCrud' => true],
        'sendToSae' => ['noCrud' => true],
        'signature' => ['noCrud' => true],
        'ExportGedPostSeances' => ['noCrud' => true],
        //
        'Collectivites' => ['groupName' => 'Administration','role' => ['admin']],
        'Themes' => ['groupName' => 'Administration','role' => ['admin']],
        //
        'ModelOdtValidator' => [
            'groupName' => 'Administration',
            'noCrud' => true,
            'role' => ['admin'],
            'auto-select' => true
        ],
        'Modeltemplates' => ['groupName' => 'Administration','role' => ['admin']],
        //
        'Sequences' => ['groupName' => 'Administration','role' => ['admin']],
        'Compteurs' => ['groupName' => 'Administration','role' => ['admin']],
        'Typeactes' => ['groupName' => 'Administration','role' => ['admin']],
        'Typeseances' => ['groupName' => 'Administration','role' => ['admin']],
        'edit9Cases' => ['groupName' => 'Administration','role' => ['admin']],
        //
        'Users' => ['groupName' => 'Administration','role' => ['admin', 'manager']],
        'updateDroits' => ['groupName' => 'Administration','noCrud' => true, 'role' => ['admin']],
        'Profils' => ['groupName' => 'Administration','role' => ['admin']],
        'Services' => ['groupName' => 'Administration','role' => ['admin', 'manager']],
        //
        'Cakeflow' => [
            'groupName' => 'Administration',
            'noCrud' => true,
            'role' => ['admin', 'manager'],
            'auto-select' => true
        ],
        'Circuits' => ['groupName' => 'Administration','role' => ['admin', 'manager']],
        'Etapes' => ['groupName' => 'Administration','role' => ['admin', 'manager']],
        'Compositions' => ['groupName' => 'Administration','role' => ['admin', 'manager']],
        //
        'Acteurs' => ['groupName' => 'Administration','role' => ['admin']],
        'Typeacteurs' => ['groupName' => 'Administration','role' => ['admin']],
        'Infosupdefs' => ['groupName' => 'Administration','noCrud' => true, 'role' => ['admin']],

        'Connecteurs' => ['groupName' => 'Administration', 'noCrud' => true, 'role' => ['admin']],
        'Verifications' => ['groupName' => 'Administration', 'noCrud' => true, 'role' => ['admin']],
        'Crons' => ['groupName' => 'Administration', 'noCrud' => true, 'role' => ['admin']],
        'Historiques' => ['groupName' => 'Administration', 'noCrud' => true, 'role' => ['admin']]
    //'ConnectorManager' => ['noCrud' => true, 'role' => ['admin']],
    //'Connectors' => ['role' => ['admin']],
    ],
    'options' => ['order' => true]
    ]
);

CakePlugin::load('AuthManager', ['bootstrap' => true, 'routes' => false]);
CakePlugin::load('Cakeflow', ['bootstrap' => true, 'routes' => false]);
CakePlugin::load('ModelOdtValidator', ['bootstrap' => true, 'routes' => false]);
CakePlugin::load('FusionConv', ['bootstrap' => true, 'routes' => false]);
CakePlugin::load('LdapManager', ['bootstrap' => true, 'routes' => false]);
CakePlugin::load('Localized');

Configure::write('app_check.versionCakePHPAttendue', '2.10.24');
Configure::write('app_check.versionPHPAttendu', '7.4');
Configure::write('app_check.versionAPACHEAttendue', '2.4');

//appVersion
$versionFile = file(ROOT . DS . 'VERSION.txt');
define('VERSION', trim(array_pop($versionFile)));

define('DATA', '/data/');
define('PDFSTAMP_LOGO', APP . '/Config/PDFStamp_logo.png');

Configure::write('debugJs', false);
Configure::write('Config.language', 'fra');
/* Configuration pour la manipulation des pdf */
Configure::write('PDFTK_EXEC', '/usr/bin/pdftk');
Configure::write('PDFINFO_EXEC', '/usr/bin/pdfinfo');
/* Configuration GhostScript */
// Chemin vers l'executable
Configure::write('GS_EXEC', '/usr/bin/gs');
// Résolution d'image (pixels) : Agit sur la rapidité de génération des annexes (Conversion pdf et doc en image)
Configure::write('GS_RESOLUTION', '150');