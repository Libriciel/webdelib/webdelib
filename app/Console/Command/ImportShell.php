<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * Console ImportShell
 *
 * @package app.Console.Command.Shell
 * @version v4.3
 */
class ImportShell extends AppShell
{
    public $uses = ['Infosupdef', 'Infosuplistedef'];

    /**
     * Function getOptionParser
     * Options d'exécution et validation des arguments
     * @return ConsoleOptionParser $parser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();
        $parser->description(__('Commandes d\'import de webdelib.'));

        $parser->addSubcommand('infoSupListe', [
            'help' => __('Import de donnée dans une information supplémentaire de type liste.'),
            'parser' => [
                'options' => [
                    'code' => [
                        'name' => 'code',
                        'required' => true,
                        'short' => 'c',
                        'help' => 'Code de l\'information supplémentaire à importer'
                    ],
                    'file' => [
                        'name' => 'file',
                        'required' => true,
                        'short' => 'f',
                        'help' => 'Fichier d\'import',
                    ],
                /* NON PRIS EN COMPTE CAR ORDRE ERONNEE contrainte
                  'desactive' => array(
                  'name' => 'desactive',
                  'required' => false,
                  'short' => 'd',
                  'help' => 'Désactive l\'ancienne liste actuelle',
                  ) */
                ]
            ]
        ]);

        return $parser;
    }

    /**
     * Function infoSupListe
     */
    public function infoSupListe()
    {
        $success = false;

        try {
            $this->Infosuplistedef->begin();
            $infosupdef = $this->Infosupdef->find('first', [
                'fields' => 'id',
                'conditions' => ['code' => $this->params['code']],
                'recursive' => -1,
            ]);

            if (empty($infosupdef)) {
                throw new Exception('Code invalide');
            }

            /* NON PRIS EN COMPTE CAR ORDRE ERONNEE contrainte
              if(!empty($this->params['desactive'])) {
              $this->Infosuplistedef->begin();
              $this->Infosuplistedef->updateAll(
              array('Infosuplistedef.actif' => false, 'Infosuplistedef.ordre' => 999),
              array('Infosuplistedef.infosupdef_id' => $infosupdef['Infosupdef']['id'])
              );
              $this->Infosuplistedef->commit();
              } */

            $values = file_get_contents($this->params['file']);

            if (($handle = fopen($this->params['file'], 'rb')) !== false) {
                $i = 0;
                while (($lines = fgetcsv($handle, 1000, ';')) !== false) {
                    if ($i === 0) {
                        foreach ($lines as $key => $val) {
                            $cols[$key] = trim($lines[$key]);
                        }
                    } else {
                        $fields = [];
                        $this->Infosuplistedef->begin();

                        foreach ($cols as $keyCol => $name) {
                            if (!empty($lines[$keyCol])) {
                                $fields[$name] = trim($lines[$keyCol]);
                            } else {
                                throw new CakeException('Fichier invalide');
                            }
                        }

                        $fields['infosupdef_id'] = $infosupdef['Infosupdef']['id'];
                        $fields['ordre'] = $i;
                        $this->Infosuplistedef->create();
                        $this->Infosuplistedef->save($fields);
                        $this->Infosuplistedef->commit();
                        $this->out(
                            '<info>'
                            . __(
                                'Import : %s',
                                implode($fields)
                            )
                            .'</info>',
                            1,
                            Shell::VERBOSE
                        );
                    }
                    $i++;
                }
            } else {
                throw new CakeException('Fichier invalide');
            }
            $this->Infosuplistedef->commit();
            $success = true;
        } catch (Exception $e) {
            $this->Infosuplistedef->rollback();
            $this->out(
                '<info>'
                . __(
                    'ERREUR : %s',
                    $e->getMessage()
                )
                .'</info>',
                1,
                Shell::VERBOSE
            );
        }
        if (empty($success)) {
            $this->footer('<error>Erreur : un problème est survenu durant l\'import !!</error>', Shell::VERBOSE);
        } else {
            $this->footer('<info>Import accomplis avec succès !</info>', Shell::VERBOSE);
        }
    }
}
