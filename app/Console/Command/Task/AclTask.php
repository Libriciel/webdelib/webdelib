<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppTask', 'Console/Command/Task');
App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('AclManagerComponent', 'AuthManager.Controller/Component');
App::uses('AclSync', 'AuthManager.Lib');

/**
 * Application Task for Cake
 * Utilitaire de commande pour les Patchs.
 *
 * @package     app.Console.Command.Task
 * @version 5.1.0
 */
class AclTask extends AppTask
{
    public $uses = [
        'User',
        'Profil',
        'Cakeflow.Circuit',
        'Aro',
        'Aco',
        'Permission'
    ];

    /**
     * @version 5.1.0
     */
    public function execute()
    {
    }

    /**
     * Function updateAcl
     *
     * Mise à jour des acl
     *
     * @version 5.0
     */
    public function updateAcl($reorder = true) // phpcs:ignore
    {
        try {
            $this->out(__('<info>Synchronize Acl</info> Please wait...'), 1, Shell::VERBOSE);
            $this->AclSync = new AclSync();
            $this->AclSync->dataSource = Configure::read('Config.database');
            $this->AclSync->startup();
            $this->AclSync->Shell = $this;
            $this->AclSync->aco_sync();
            if ($reorder) {
                $this->out(__('<info>Reorder Acl</info> Please wait...'), 1, Shell::VERBOSE);
                $this->orderAcl();
            }
        } catch (Exception $e) {
            $this->error(__('<info>Synchronize Acl</info>'), $e->getMessage() . $e->getTraceAsString());
            return false;
        }

        return true;
    }


    /**
     * Function creationAcl
     *
     * Création des acl
     */
    public function creationAcl() // phpcs:ignore
    {
        try {
            $this->out(__('<info>Created Acl (installation)</info> Please wait...'), 1);

            $this->updateAcl();

            $this->out(__('<success>Acl Created Complete (installation)</success>'));
        } catch (Exception $e) {
            $this->error($e->getMessage(), Shell::QUIET);
        }
    }

    /**
     * Création des permissions pour l'admin général
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function createAdminPermission()
    {
        $this->out(__('Created Admin Permission'), 1, Shell::QUIET);
        $collection = new ComponentCollection();
        $this->AclManager = new AclManagerComponent($collection);

        $this->Aro->recursive = -1;
        $aroUser = $this->Aro->findByForeignKeyAndModel(1, 'User');
        $aroProfil = $this->Aro->findByForeignKeyAndModel(1, 'Profil');

        $acos = $this->Aco->find('all', [
            'conditions' => ['model' => null],
            'order' => ['id' => 'ASC'],
            'recursive' => -1]);

        foreach ($acos as $aco) {
            $this->Permission->create();
            $permission['Permission']['aro_id'] = $aroUser['Aro']['id'];
            $permission['Permission']['aco_id'] = $aco['Aco']['id'];
            foreach ($this->Permission->getAcoKeys($this->Permission->schema()) as $permKeys) {
                $permission['Permission'][$permKeys] = 1;
            }

            $this->Permission->save($permission);
            $this->Permission->clear();

            // Pour le profil administrateur
            $this->Permission->create();
            $permission['Permission']['aro_id'] = $aroProfil['Aro']['id'];
            $this->Permission->save($permission);
            $this->Permission->clear();
        }
    }

    /**
     * Création des permissions pour l'admin général
     *
     * @param  string  $model [description]
     * @param  integer|string $allow [description]
     */
    public function createUsersPermission($model, $allow = -1)
    {
        $this->out(__('Created user permission node: %s', $model));

        $this->Aco->recursive = -1;
        $aco = $this->Aco->findByAlias($model);
        if (empty($aco)) {
            $this->err(__('Unable to find Aco for "%s"', $model));
            return;
        }

        if (is_string($allow)) {
            $acoCopy = $this->Aco->findByAlias($allow);
        }
        $this->Aro->recursive = -1;
        $aro_users = $this->Aro->findAllByModel('User');
        $linkExist = false;
        foreach ($aro_users as $aro_user) {
            $this->Permission->create();
            $permission['Permission']['aro_id'] = $aro_user['Aro']['id'];
            $permission['Permission']['aco_id'] = $aco['Aco']['id'];

            if (is_string($allow)) {
                $link = $this->Permission->getAclLink(
                    ['User' => ['id' => $aro_user['Aro']['foreign_key']]],
                    $allow,
                );

                if (!empty($link['link'])) {
                    $linkExist = true;
                    foreach ($this->Permission->getAcoKeys($this->Permission->schema()) as $permKeys) {
                        $permission['Permission'][$permKeys] = $link['link'][0]['Permission'][$permKeys];
                    }
                } else {
                    $allow = '-1';
                }
            }
            if ($linkExist === false) {
                foreach ($this->Permission->getAcoKeys($this->Permission->schema()) as $permKeys) {
                    $permission['Permission'][$permKeys] = $allow;
                }
            }

            $this->Permission->save($permission);
            $this->Permission->clear();
        }
    }

    /**
     * Création des permissions pour l'admin général
     *
     * @param  [type]  $model [description]
     * @param  integer $allow [description]
     *
     * @return [type]         [description]
     *
     * @version 5.1.0
     */
    public function migrateUsersPermission($model, $oldPermission, $newPermission, $allow = -1)
    {
        $this->Aco->recursive = -1;
        $aco = $this->Aco->findByAlias($model);

        $this->Aro->recursive = -1;
        $aro_users = $this->Aro->findAllByModel('User');

        foreach ($aro_users as $aro_user) {
            $this->Permission->create();
            $permission['Permission']['aro_id'] = $aro_user['Aro']['id'];
            $permission['Permission']['aco_id'] = $aco['Aco']['id'];
            foreach ($this->Permission->getAcoKeys($this->Permission->schema()) as $permKeys) {
                $permission['Permission'][$permKeys] = $allow;
            }

            $this->Permission->save($permission);
            $this->Permission->clear();
        }
    }

    public function createControlerAcl($name, $childrens)
    {
        $this->out(__('Created %s', $name));
        $this->Aco->create();
        $this->Aco->save([
            'parent_id' => 1,
            'alias' => $name,
        ]);
        $parentId = $this->Aco->getLastInsertID();

        foreach ($childrens as $children) {
            $this->out(__('migrate children %s to %s', $children, $name));
            $this->Aco->recursive = -1;
            $aco = $this->Aco->findByAlias($children);
            if (empty($aco)) {
                $this->err(__('<error>Unable to find Aco children for "%s"</error>', $children));
            }
            $this->Aco->id = $aco['Aco']['id'];
            $this->Aco->saveField('parent_id', $parentId);
            $this->Aco->clear();
        }
    }

    /**
     * Création des acls pour des modèles de l'application
     *
     * @param string $acl Nom de l'acl (Aro||Aco)
     * @param string $model CakeObject Model
     */
    public function createAcl($acl, $model)
    {
        $this->out(__('Created %s: %s', $acl, $model), 1, Shell::QUIET);

        $this->{$model} = ClassRegistry::init($model);
        $this->{$model}->Behaviors->load('DynamicDbConfig');

        $this->{$model}->recursive = -1;
        $values = $this->{$model}->find('all', [
            'order' => ['id' => 'ASC'],
            'recursive' => -1]);

        foreach ($values as $value) {
            $alias = '';
            if (array_key_exists('name', $value[$model])) {
                $alias = $value[$model]['name'];
            } elseif (array_key_exists('nom', $value[$model])) {
                $alias = $value[$model]['nom'];
            } elseif (array_key_exists('libelle', $value[$model])) {
                $alias = $value[$model]['libelle'];
            }

            $this->out(__('Created %s new: %s', $acl, $alias), 1, Shell::QUIET);

            $this->{$acl}->create();
            $this->{$acl}->save([
                'foreign_key' => $value[$model]['id'],
                'model' => $model,
                'alias' => $alias,
            ]);
            $this->{$acl}->clear();
        }

        foreach ($values as $value) {
            if (!empty($value[$model]['parent_id'])) {
                $this->out(__('Created Acl parent_id: %s', $value[$model]['parent_id']), 1, Shell::QUIET);

                $this->{$acl}->recursive = -1;
                $parent = $this->{$acl}->find('first', [
                    'conditions' => ['model' => $model, 'foreign_key' => $value[$model]['parent_id']],
                    'recursive' => -1]);

                $droit = $this->{$acl}->find('first', [
                    'conditions' => ['model' => $model, 'foreign_key' => $value[$model]['id']],
                    'recursive' => -1]);

                $this->{$acl}->id = $droit[$acl]['id'];
                $this->{$acl}->saveField('parent_id', $parent[$acl]['id']);
                $this->{$acl}->clear();
            }
        }
    }

    /**
     * Création des acls pour des modèles de l'application
     *
     * @param string $acl Nom de l'acl
     * @param string $model Modèle
     * @version 5.1.0
     * @since 4.3.0
     */
    public function migrateNameControlerAcl($newPermission, $oldPermission)
    {
        $this->out(__('Migrate Acl: %s => %s', $oldPermission, $newPermission));

        $aco = $this->Aco->find('first', [
                'conditions' => [
                    'alias' => $oldPermission,
                    'model' => null
                ],
                'recursive' => -1]);

        if (empty($aco)) {
            $this->err(__('<error>Unable to find old Aco for "%s"</error>', $oldPermission));
            return;
        }

        $this->Aco->id = $aco['Aco']['id'];
        $this->Aco->saveField('alias', $newPermission);
    }

    /**
     * Function orderAcl
     * @version 5.1.0
     * @since 4.3.0
     */
    public function orderAcl()
    {
        $this->out(__('<info>Reorder Acl</info> Please wait...'), 1, Shell::QUIET);
        //$this->Aco->reorder(['field' =>'parent_id','order' => 'ASC']);

        $configCRUD = Configure::read('AuthManager.configCRUD');
        if (isset($configCRUD['options']['order']) && $configCRUD['options']['order'] === true) {
            $this->orderAclByModel(null, $configCRUD['CRUD']);
        }

        //$this->Aro->reorder(['field' =>'parent_id','order' => 'ASC']);
    }

    /**
     * Function orderAclByModel
     * @version 5.1.0
     * @since 4.3.0
     */
    private function orderAclByModel($model, $schema)
    {
        $schema = array_reverse($schema, true);
        $nodes = $this->Aco->node('controllers');
        $childrens = $this->Aco->children($nodes[0]['Aco']['id']);
        $childrens_alias = Hash::extract($childrens, '{n}.Aco.alias');
        foreach ($schema as $key => $value) {
            $key_children = array_search($key, $childrens_alias, true);
            if ($key_children !== false) {
                $this->Aco->moveUp($childrens[$key_children]['Aco']['id'], true);
            }
        }
    }
}
