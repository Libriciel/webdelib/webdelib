<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppTask', 'Console/Command/Task');
App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('ConversionComponent', 'Controller/Component');
App::uses('AppTools', 'Lib');

/**
 * Application Task for Cake
 * @version 5.0.1
 * @since   4.3.0
 * @package app.Console.Command.Task
 */
class AnnexeToolsTask extends AppTask
{
    public $uses = ['Annexe', 'Deliberation'];

    public $tasks = ['Output'];

    /**
     * @version 4.3
     */
    public function execute()
    {
    }

    public function initialize()
    {
        parent::initialize();

        $collection = new ComponentCollection();
        $this->Conversion = new ConversionComponent($collection);
    }

    /**
     * Function conversion
     * @param int $delib_id
     * @param bool $refresh
     * @return string
     * @version 4.3.0
     */
    public function conversion($delib_id = null, $refresh = false)
    {
        $convert = Configure::read('FusionConv.app.fileOdt.convert');
        try {
            $DOC_TYPE = Configure::read('DOC_TYPE');
            $this->Deliberation = ClassRegistry::init('Deliberation');
            $this->Annexe = ClassRegistry::init('Annexe');

            //Ne pas convertir les odt suivant la configuration de l'application
            $typemimes_for_fusion = AppTools::getTypemimesFilesForFusion();
            $typemimes_for_fusion_list = Set::extract('{n}.typemime', $typemimes_for_fusion);
            if ($convert===false) {
                $key_for_fusion_odt = array_search(
                    'application/vnd.oasis.opendocument.text',
                    $typemimes_for_fusion,
                    true
                );
                if ($key_for_fusion_odt !== false) {
                    unset($typemimes_for_fusion_list[$key_for_fusion_odt]);
                }
            }

            //Ne pas convertir les pdf pour le controle de légalité
            $typemimes_ctrl_legalite = AppTools::getTypemimesFilesForCtrlLegalite();
            $typemimes_ctrl_legalite_list = Set::extract('{n}.typemime', $typemimes_ctrl_legalite);
            $key_ctrl_legalite_pdf = array_search('application/pdf', $typemimes_ctrl_legalite_list, true);
            //Ne pas prendre en compte les fichiers non convertibles
            $typemimes_ctrl_legalite_list = array_intersect($typemimes_ctrl_legalite_list, $typemimes_for_fusion_list);
            if ($key_ctrl_legalite_pdf !== false) {
                unset($typemimes_ctrl_legalite_list[$key_ctrl_legalite_pdf]);
            }

            $conditions = ['AND' => [['OR' => [
                 ['AND' => ['joindre_ctrl_legalite' => true, 'filetype' => $typemimes_ctrl_legalite_list]],
                    ['AND' => ['joindre_fusion' => 1, 'filetype' => $typemimes_for_fusion_list]]
                ]]]];

            if (!empty($delib_id)) {
                $delibs = $this->Deliberation->find('all', [
                    'recursive' => -1,
                    'fields' => ['id'],
                    'conditions' => ['parent_id' => $delib_id]
                ]);

                if (!empty($delibs)) {
                    $foreign_key = Hash::extract($delibs, '{n}.Deliberation.id');
                    $foreign_key[] = $delib_id;
                } else {
                    $foreign_key = $delib_id;
                }

                $conditions['foreign_key'] = $foreign_key;
            }

            if ($refresh) {
                $conditions['AND'][]['OR'] = ['joindre_ctrl_legalite' => true, 'joindre_fusion' => 1];
            } else {
                $conditions['AND'][]['OR'] = [
                        ['AND' => [
                            'joindre_ctrl_legalite' => true,
                            'data_pdf' => null,
                            'filetype <>' => 'application/pdf' ]
                        ],
                        ['AND' => ['joindre_fusion' => 1, 'edition_data' => null]]
                ];
            }

            $annexes = $this->Annexe->find('all', [
                'fields' => [
                    'id','foreign_key', 'data', 'edition_data', 'data_pdf', 'filename',
                    'filetype', 'joindre_ctrl_legalite', 'joindre_fusion'
                ],
                'conditions' => $conditions,
                'order' => 'modified DESC',
                'recursive' => -1
            ]);

            if (!empty($annexes)) {
                foreach ($annexes as $annexe) {
                    if (!empty($annexe['Annexe']['data'])) {
                        $newAnnexe = [];
                        $this->Annexe->id = $annexe['Annexe']['id'];
                        $this->out(
                            '<warning>'
                            . __(
                                'Conversion annexe id: %s (Projet %s)',
                                $annexe['Annexe']['id'],
                                $annexe['Annexe']['foreign_key']
                            ) . '</warning>',
                            1,
                            Shell::VERBOSE
                        );
                        if ($annexe['Annexe']['joindre_fusion']
                            && (empty($annexe['Annexe']['edition_data']) || $refresh)
                        ) {
                            if (empty($convert)) {
                                $newAnnexe['edition_data'] = $annexe['Annexe']['data'];
                                $newAnnexe['edition_data_typemime'] = 'application/vnd.oasis.opendocument.text';
                            } else {
                                $newAnnexe['edition_data'] = $this->Conversion->toOdt(
                                    $annexe['Annexe']['data'],
                                    $annexe['Annexe']['filetype']
                                );
                                $newAnnexe['edition_data_typemime'] = 'application/vnd.oasis.opendocument.text';
                            }
                        }

                        if (($annexe['Annexe']['joindre_ctrl_legalite']
                                && !empty($DOC_TYPE[$annexe['Annexe']['filetype']]['convertir']))
                                && (empty($annexe['Annexe']['data_pdf']) || $refresh)) {
                            $extension = $DOC_TYPE[$annexe['Annexe']['filetype']]['extension'];
                            if (is_array($extension)) {
                                $extension = $extension[0];
                            }
                            $newAnnexe['data_pdf'] = $this->Conversion->convertirFlux(
                                $annexe['Annexe']['data'],
                                $extension,
                                'pdf',
                                false
                            );
                        }

                        if (!empty($newAnnexe)) {
                            $this->Annexe->save($newAnnexe);
                        }
                        $this->Annexe->clear();
                    } else {
                        CakeLog::write(
                            'error',
                            'Conversion annexe "vide" (data) id:' . $annexe['Annexe']['id']
                        );
                    }
                }
            }
            if (empty($annexes)) {
                return Cron::MESSAGE_FIN_EXEC_SUCCES . __(' Aucune annexe à convertir');
            } else {
                return Cron::MESSAGE_FIN_EXEC_SUCCES . " Annexe convertie(s):\n\n" . count($annexes);
            }
        } catch (Exception $e) {
            return Cron::MESSAGE_FIN_EXEC_ERROR
                . ' ' . $e->getMessage() . ' id=' . (isset($this->Annexe->id) ? $this->Annexe->id : 'Inconnu');
        }
    }

    /**
     * Function pour trouver les annexes qui n'ont pas de postion et leur en affecter une.
     *
     * @version 5.0.1
     * @since 5.0.1
     */
    public function orderAnnexes()
    {
        //Récupération des identifiants qui n'ont pas de position
        $projets = $this->Annexe->find('all', [
                'fields' => ['DISTINCT foreign_key'],
                'conditions' => ['position' => null],
                'recursive' => -1
                ]);
        $this->Output->info(__('%s annexe(s) qui n\'ont pas de postion', count($projets)));
        foreach ($projets as $projet) {
            $annexes = $this->Annexe->find('all', [
                'fields' => ['id'],
                'conditions' => ['foreign_key' => $projet['Annexe']['foreign_key']],
                'order' => ['id' => 'ASC'],
                'recursive' => -1
             ]);

            $position = 0;
            foreach ($annexes as $annexe) {
                $position++;
                $this->Annexe->id = $annexe['Annexe']['id'];
                $this->Annexe->saveField('position', $position);
                $this->Annexe->clear();
            }

            $this->Output->info(
                __('identifiant n°%s : Modfication(s) effectuée(s)', $projet['Annexe']['foreign_key'])
            );
        }

        return true;
    }
}
