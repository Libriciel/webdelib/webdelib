<?php
/**
 *
 * WebDavTask file
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       web-delib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CakeTime', 'Utility');
App::uses('AppTools', 'Lib');
App::uses('AppTask', 'Console/Command/Task');

/**
 * Application Task for Cake
 *
 * Utilitaire de commande pour le WEBDAV.
 *
 * @package app.Console.Command.Task
 */
class WebDavTask extends AppTask
{
    public $uses = [];

    /**
     * @version 4.3
     */
    public function execute()
    {
    }

    /**
     * Purge les fichiers temporaire du WEBDAV
     * @param int $day_interval Intervale en jours des suppressions des répertoires temporaires
     * @version 4.3
     */
    public function purge($day_interval = 7)
    {
        $aDayNotPurge = [];
        try {
            for ($i = 1; $i <= $day_interval; $i++) {
                $aDayNotPurge[] = AppTools::addSubDurationToDate('now', 'P' . $i . 'D', 'Ymd', 'sub');
            }

            $oFolder = new Folder(TMP . 'files' . DS . 'webdav' . DS . $this->getTenantName());
            $aDirs = $oFolder->read(true, true);
            $aDeleteDirs = array_diff($aDirs[0], $aDayNotPurge);

            if (empty($aDeleteDirs)) {
                $this->out('<info>' . __('Rien à faire') . '</info>', 1, Shell::NORMAL);
            }

            foreach ($aDeleteDirs as $dir) {
                $this->out(
                    '<warning>'
                    . __('Suppression du dossier:') . '</warning>' . ' '
                    . TMP . 'files' . DS . 'webdav' . DS . $this->getTenantName() . DS . $dir,
                    1,
                    Shell::NORMAL
                );
                $oFolder->delete(TMP . 'files' . DS . 'webdav' . DS . $this->getTenantName() . DS . $dir);
            }

            return Cron::MESSAGE_FIN_EXEC_SUCCES;
        } catch (Exception $e) {
            $this->out('<error>' . __('Erreur:') . '</error>' . ' ' . $e->getMessage(), 1, Shell::NORMAL);
            return $e->getMessage() . Cron::MESSAGE_FIN_EXEC_ERROR;
        }
    }
}
