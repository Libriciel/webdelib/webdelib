<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('ConnectionManager', 'Model');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * SqlTask Task for Console Cake
 *
 * Tache de mise à jour
 * Permet de passer simplement des fichiers sql à la base
 * Avec prise en charge transactionnelle
 *
 * @package app.Console.Command.Shell
 *
 * @version 5.1.0
 * @since 4.2.0
 */
class SqlTask extends Shell
{
    /**
     * [protected description]
     * @var DboSource
     */
    protected $dataSource;

    /**
     * [public description]
     * @var string
     */
    public $connection = 'default';

    /**
     * Function execute
     *
     * Initialisation de l'objet ConnectionManager
     * (récupération de la connexion à la base de données à partir de database.php)
     *
     * @version 5.1.0
     * @since 4.2.0
     */
    public function initialize()
    {
        parent::initialize();

        $this->connection = Configure::read('Config.database');
        $this->dataSource = ConnectionManager::getDataSource($this->connection);

        if (!$this->dataSource->isConnected()) {
            $message = "Impossible d'établir une connexion à la base de données %s. '
            . 'Veuillez vérifier vos paramètres dans le fichier app/Config/database.php et réessayer.";
            $this->error(sprintf($message, $this->connection));
        }
    }

    /**
     * [run description]
     * @param  [type] $path chemin vers le fichier sql à exécuter
     * @return boolean       résultat de l'exécution du sql
     *
     * @version 5.1.0
     * @since 4.2.0
     */
    public function run($path)
    {
        $file = new File($path);

        if ($file->exists() === false) {
            $msgid = "Fichier SQL \"%s\" introuvable, veuillez vous assurer d'avoir la dernière version des sources";
            $message = sprintf(__($msgid), $path);
            throw new RuntimeException($message);
        }

        try {
            $line = null;
            //Lecture ligne par ligne (séparateur ;)
            $content = $file->read();
            $file->close();

            //Supprime les lignes de commentaire
            $content = preg_replace('/--.*/', '', $content);
            $lines = explode(';', $content);

            foreach ($lines as $line) {
                //Suppression des espaces en début ou fin de ligne
                $line = trim($line);
                //Saut des mots clés begin et commit (transaction déjà démarrée)
                // @fixme
                if (!empty($line) && !in_array(strtolower($line), ['begin', 'commit'], true)) {
                    // Exécute la ligne SQL
                    //@fixme: cache ?
                    if ($this->dataSource->rawQuery($line) === false) {
                        $this->error($line);
                    }
                }
            }
        } catch (Exception $e) {
            if (empty($line) === false) {
                $this->err(sprintf(__("<emergency>Requête en erreur :</emergency> %s"), $line));
            }
            $msgId = "Erreur SQL dans le fichier %s : %s";
            $message = sprintf(__($msgId), $this->shortPath($path), $e->getMessage());
            throw new RuntimeException($message);
        }

        return true;
    }

    /**
     * [begin description]
     * Démarrage de la transaction
     *
     * @return [type] [description]
     *
     * @version 5.1.0
     * @since 4.2.0
     */
    public function begin()
    {
        return $this->dataSource->begin();
    }

    /**
     * [commit description]
     * Fin de la transaction, tout s'est bien passé
     *
     * @return [type] [description]
     *
     * @version 5.1.0
     * @since 4.2.0
     */
    public function commit()
    {
        return $this->dataSource->commit();
    }

    /**
     * [rollback description]
     * Fin de la transaction, une erreur a été rencontrée
     *
     * @return [type] [description]
     *
     * @version 5.1.0
     * @since 4.2.0
     */
    public function rollback()
    {
        return $this->dataSource->rollback();
    }

    /**
     * Passage des scripts SQL.
     * @fixme: transactions ?
     *
     * @param array $scripts
     * @param bool $transaction
     * @return bool
     * @throws Exception
     * @throws RuntimeException
     */
    public function apply(array $scripts, $transaction = true)
    {
        $scripts = array_filter($scripts);
        $success = true;

        if (empty($scripts) === false) {
            if ($transaction === true) {
                $success = $this->begin() && $success;
            }

            try {
                foreach (array_filter($scripts) as $name => $script) {
                    $this->out("<warning>$name ...</warning>");
                    if ($this->run($script) !== true) {
                        throw new RuntimeException(
                            __(
                                'Erreur lors du lancement du fichier SQL de : "%s" (%s)',
                                $name,
                                $this->shortPath($script)
                            )
                        );
                    }
                }
            } catch (Exception $e) {
                if ($transaction === true) {
                    $this->rollback();
                }
                throw $e;
            }

            if ($transaction === true) {
                if ($success === true) {
                    $success = $this->commit() && $success;
                } else {
                    $success = $this->rollback() && $success;
                }
            }
        }

        return $success;
    }

    /**
     * @return array|PDO|null
     */
    public function getDataSource()
    {
        return $this->dataSource;
    }

    /**
     * [commit description]
     * Fin de la transaction, tout s'est bien passé
     *
     * @return [type] [description]
     *
     * @version 5.1.0
     * @since 4.2.0
     */
    public function execute($script)
    {
        return $this->dataSource->execute($script);
    }
}
