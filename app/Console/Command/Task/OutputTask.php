<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * Application OutputTask for Cake
 *
 * Utilitaire de commande de sortie pour les consoles.
 *
 * @package app.Console.Command.Task
 */
class OutputTask extends Shell
{
    public $uses = [];

    /**
     * @version 4.3
     */
    public function execute()
    {
    }

    /**
     * [info description]
     * @param  [type] $var [description]
     * @return [type]      [description]
     * @version 5.1.0
     */
    public function info($var, $level = Shell::NORMAL)
    {
        $this->out('<info>' . $var .'</info>', 1, $level);
    }

    /**
     * [info description]
     * @param  [type] $var [description]
     * @return [type]      [description]
     * @version 5.1.0
     */
    public function error($var, $level = Shell::NORMAL)
    {
        $this->out('<error>' . $var .'</error>', 1, $level);
    }

    /**
     * [info description]
     * @param  [type] $var [description]
     * @return [type]      [description]
     * @version 5.1.0
     */
    public function warning($var, $level = Shell::NORMAL)
    {
        $this->out('<warning>' . $var .'</warning>', 1, $level);
    }


    /**
     * Function header
     *
     * Affiche un message entouré de deux barres horizontales
     *
     * @param string $var message
     * @version 5.1
     */
    public function header($var)
    {
        $this->hr();
        $this->out(__('-> %s', $var));
        $this->hr();
    }
}
