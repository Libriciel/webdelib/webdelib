<?php
/**
 *
 * GedoooTask file
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       web-delib v4.2
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Application Task for Cake
 *
 * Tâches de vérification des textes en base de données par Gedooo
 * Créé le : 01/10/2013
 *
 * @package app.Console.Command.Task
 */
class GedoooTask extends Shell
{
    public $uses = ['Annexe', 'Deliberation', 'Collectivite', 'Seance', 'Deliberationseance'];

    /**
     * variable contenant le log de génération des annexes par Gedooo
     *
     * @var type string rapport de gedooo pour les annexes
     */
    private $rapport;

    /**
     * fichier de rapport d'exécution
     *
     * @var type File
     */
    private $logFile;

    /**
     * fichier modèle pour génération Gedooo
     *
     * @var type File
     */
    private $modelFile;

    /**
     * dossier pour textes générées par Gedooo
     *
     * @var type Folder
     */
    private $textesFolder;

    /**
     * identifiants des textes en erreur
     *
     * @var type array
     */
    private $textesInError;

    /**
     * Chemins fichiers & dossiers
     *
     * @version 4.2
     */
    public $logPath;
    public $textePath;
    public $modelPath;

    /**
     * Function execute
     *
     * Fonction principale pour l'exécution des tests
     *
     * @return void
     * @version 4.2
     * @deprecated 5.1
     */
    public function execute()
    {
        // Inclusion des librairies Gedooo
        include_once APP . 'Vendor' . DS . 'GEDOOo' . DS . 'phpgedooo' . DS . 'GDO_PartType.class';
        include_once APP . 'Vendor' . DS . 'GEDOOo' . DS . 'phpgedooo' . DS . 'GDO_ContentType.class';
        include_once APP . 'Vendor' . DS . 'GEDOOo' . DS . 'phpgedooo' . DS . 'GDO_FusionType.class';

        // Initialisations
        $this->textesInError = [];

        // Chemins fichiers & dossiers
        $this->textePath = APP . 'tmp' . DS . 'files' . DS . 'textes';
        $this->logPath = APP . "tmp" . DS . "logs" . DS . "gedooo.log";
        $this->modelPath = APP . WEBROOT_DIR . DS . 'files' . DS . 'model_annexe_test.odt';

        // Instanciations
        $this->textesFolder = new Folder($this->textePath, true);
        $this->logFile = new File($this->logPath, true, 0644);
        $this->modelFile = new File($this->modelPath, false);
    }

    /**
     * Function testTextes
     *
     * Test des annexes
     * Pour les annexes des délibérations de séances en cours et sans séance
     * Génère un fichier texte avec les colonnes suivantes :
     * # seance_id delib_id annex_id result
     *
     * @param string $mode Mode de test des annexes {'all','noseance','nontraitees'}
     * @param string $id
     * @param string $logPath Chemin du fichier de log personnalisable
     * @return string
     * @version 4.2
     * @deprecated 5.1
     */
    public function testTextes($mode = 'all', $id = 'null', $logPath = null)
    {
        if ($logPath != null) {
            $this->logPath = $logPath;
        }

        if ($mode != 'id') {
            // Charge les délibs et annexes depuis la base de données
            $delibsWithAnnexes = $this->getDelibsWithAnnexes();
        }

        // filtre les annexes (scope selon argument $mode)
        switch ($mode) {
            case 'nontraitees':
                $projets = $this->getProjetsSeanceEnCours($delibsWithAnnexes);
                break;
            case 'noseance':
                $projets = $this->getProjetsSansSeance($delibsWithAnnexes);
                break;
            case 'id':
                $projets = $this->getProjetsParSeanceId($id);
                break;
            default:
                $projets = array_merge(
                    $this->getProjetsSeanceEnCours($delibsWithAnnexes),
                    $this->getProjetsSansSeance($delibsWithAnnexes)
                );
                break;
        }

        $nbProjets = count($projets);
        $nbAnnexes = $this->countAnnexesForDelibs($projets);

        if (!empty($nbProjets) || !empty($nbAnnexes)) {
            $this->out("\n<important>Nombre de projets à tester : $nbProjets ...</important>");
            $this->out("<important>Nombre d'annexes à tester : $nbAnnexes ...</important>\n");
            $p = 0;
            //Pour chaque projet
            foreach ($projets as $projet) {
                $p++;
                $hasTexte = false;
                $this->out(
                    "<info>$p/$nbProjets -> Test du projet " . $projet['Deliberation']['id'] . ' "'
                    . $projet['Deliberation']['objet_delib'] . "\"...</info>\n",
                    0
                );

                // Envoi des textes à gedooo
                if (!empty($projet['Deliberation']['texte_projet'])) {
                    $hasTexte = true;
                    $this->out("* Texte de projet " . $projet['Deliberation']['texte_projet_name'] . ' ... ', 0);
                    $result = $this->sendToGedooo(
                        $projet['Deliberation']['texte_projet'],
                        $projet['Deliberation']['texte_projet_name'],
                        $projet['Deliberation']['id'],
                        'Deliberation',
                        'texte_projet'
                    );
                    // Log du résultat
                    $this->ajouterLigneAuRapport(
                        $projet['Deliberation']['id'],
                        $projet['Deliberation']['id'],
                        'Deliberation',
                        'texte_projet',
                        $result
                    );
                }
                if (!empty($projet['Deliberation']['texte_synthese'])) {
                    $hasTexte = true;
                    $this->out("* Texte de synthèse " . $projet['Deliberation']['texte_synthese_name'] . ' ... ', 0);
                    $result = $this->sendToGedooo(
                        $projet['Deliberation']['texte_synthese'],
                        $projet['Deliberation']['texte_synthese_name'],
                        $projet['Deliberation']['id'],
                        'Deliberation',
                        'texte_synthese'
                    );
                    // Log du résultat
                    $this->ajouterLigneAuRapport(
                        $projet['Deliberation']['id'],
                        $projet['Deliberation']['id'],
                        'Deliberation',
                        'texte_synthese',
                        $result
                    );
                }
                if (!empty($projet['Deliberation']['deliberation'])) {
                    $hasTexte = true;
                    $this->out("* Texte de délibération " . $projet['Deliberation']['deliberation_name'] . ' ... ', 0);
                    $result = $this->sendToGedooo(
                        $projet['Deliberation']['deliberation'],
                        $projet['Deliberation']['deliberation_name'],
                        $projet['Deliberation']['id'],
                        'Deliberation',
                        'deliberation'
                    );
                    // Log du résultat
                    $this->ajouterLigneAuRapport(
                        $projet['Deliberation']['id'],
                        $projet['Deliberation']['id'],
                        'Deliberation',
                        'deliberation',
                        $result
                    );
                }
                foreach ($projet['Annexe'] as $annexe) {
                    $hasTexte = true;
                    $this->out(
                        "* Annexe " . $annexe['filename'] . ' (id: ' . $annexe['id']
                        . ', délibération: ' . $projet['Deliberation']['id'] . ')... ',
                        0
                    );
                    // Envoi de l'annexe à gedooo
                    $result = $this->sendToGedooo(
                        $annexe['data'],
                        $annexe['filename'],
                        $annexe['id'],
                        'Annexe',
                        'data'
                    );
                    // Log du résultat
                    $this->ajouterLigneAuRapport(
                        $annexe['id'],
                        $projet['Deliberation']['id'],
                        'Annexe',
                        'data',
                        $result
                    );
                } // Fin foreach annexe
                if (!$hasTexte) {
                    $this->out("Aucun texte associé !\n", 0);
                }
            } // Fin foreach projet

            try {
                // Création du fichier de rapport
                $this->creerRapport();
            } catch (Exception $exc) {
                $this->out('<error>Erreur fichier journal : ' . $exc->getMessage() . '</error>');
            }
        } else {
            $this->out("\n<info>Aucun texte à tester !</info>\n");
        }

        $this->textesFolder->delete();

        return $this->textesInError;
    }

    /**
     * Function getDelibsWithAnnexes
     *
     * Effectue une requête afin de récupérer un tableau de délibération avec annexes et séances,
     * Ce tableau est utilisé par les fonctions getProjetsSeanceEnCours et getProjetsSansSeance
     *
     * @see getProjetsSeanceEnCours
     * @see getProjetsSansSeance
     * @return array Tableau de délibérations contenant Annexes et Séances
     * @version 4.2
     * @deprecated 5.1
     */
    private function getDelibsWithAnnexes()
    {
        $this->out("<info>Chargement des projets depuis la base de données...</info>");
        $time_start = microtime(true);

        $options = [
            'fields' => ['id', 'objet_delib', 'texte_projet', 'texte_projet_name', 'texte_synthese',
                'texte_synthese_name', 'deliberation', 'deliberation_name'],
            'contain' => ['Annexe' => ['fields' => ['id', 'filename', 'filetype', 'data'],
                'conditions' => [
                    'filetype' => ["application/vnd.oasis.opendocument.text", "application/pdf"]
                ]
            ],
            'Deliberationseance' => ['fields' => [], 'Seance' => ['fields' => ['id', 'traitee']]]
            ]
        ];

        $delibs = $this->Deliberation->find('all', $options);

        $time_end = microtime(true);
        $this->out("<time>Durée : " . round($time_end - $time_start, 2) . 's</time>', 1, Shell::VERBOSE);

        return $delibs;
    }

    /**
     * Function getProjetsSeanceEnCours
     *
     * Filtre parmis les enregistrements ceux qui sont attachés à une séance en cours
     *
     * @params $delibs les délibs à filtrer
     * @return array Les projets de séances en cours
     * @version 4.2
     * @deprecated 5.1
     */
    private function getProjetsSeanceEnCours($delibs)
    {
        $this->out("<info>Sélection des projets attachées à une séance à traiter...</info>");
        $time_start = microtime(true);

        $projets = [];
        //Pour chaque délib
        foreach ($delibs as $delib) {
            $controler = false;
            //Si la délib possède une séance
            if (!empty($delib['Deliberationseance'])) {
                // Parcourir les séances et si une d'elle n'est pas traitée (non vide) contrôller
                foreach ($delib['Deliberationseance'] as $deliberationseance) {
                    if (!empty($deliberationseance['Seance'])) {
                        foreach ($deliberationseance['Seance'] as $seance) {
                            if ($seance['traitee'] == 0) {
                                $controler = true;
                                break;
                            }
                        }
                    }
                    // Si une séance de la délib est non traitée, arreter le parcourt des séances de la délibération
                    if ($controler) {
                        break;
                    }
                }
                // Si la délib fait partie d'une séance non traitée, ajouter au tableau de projets à tester
                if ($controler) {
                    $projets[] = $delib;
                }
            }
        }

        $time_end = microtime(true);
        $this->out(
            "<time>Durée : " . round($time_end - $time_start, 2) . 's</time>',
            1,
            Shell::VERBOSE
        );

        return $projets;
    }

    /**
     * Function getProjetsSansSeance
     *
     * Filtre parmis les enregistrements ceux qui ne sont attachés à aucune séance
     *
     * @params $delibs tableau de délibérations
     * @return array annexes Les annexes des délibérations sans séance
     * @version 4.2
     * @deprecated 5.1
     */
    private function getProjetsSansSeance($delibs)
    {
        $this->out("<info>Sélection des projets sans séance...</info>");
        $time_start = microtime(true);

        $projets = [];
        //Pour chaque délib
        foreach ($delibs as $delib) {
            //Si le projet ne possède pas de séance
            if (empty($delib['Deliberationseance'])) {
                // Ajouter toutes les annexes au tableau d'annexes à tester
                $projets[] = $delib;
            }
        }

        $time_end = microtime(true);
        $this->out(
            '<time>Durée : ' . round($time_end - $time_start, 2) . 's</time>',
            1,
            Shell::VERBOSE
        );

        return $projets;
    }

    /**
     * Function getProjetsParSeanceId
     *
     * @params $seanceId id de la séance contenant les textes à tester
     * @return array projets
     * @version 4.2
     * @deprecated 5.1
     */
    private function getProjetsParSeanceId($seanceId)
    {
        $this->out("<info>Sélection des projets de la séance $seanceId...</info>");
        $time_start = microtime(true);

        $options = ['recursive' => -1, 'fields' => ['deliberation_id'], 'conditions' => ['seance_id' => $seanceId]];

        $delibseance = $this->Deliberationseance->find('all', $options);
        $delibsid = [];
        foreach ($delibseance as $delibid) {
            $delibsid[] = $delibid['Deliberationseance']['deliberation_id'];
        }

        $delibs = $this->Deliberation->find('all', [
            'conditions' => ['Deliberation.id' => $delibsid],
            'fields' => ['Deliberation.id', 'Deliberation.objet_delib', 'Deliberation.texte_projet',
                'Deliberation.texte_projet_name', 'Deliberation.texte_synthese', 'Deliberation.texte_synthese_name',
                'Deliberation.deliberation', 'Deliberation.deliberation_name'],
            'contain' => [
                'Annexe' => [
                    'fields' => ['Annexe.id', 'Annexe.filename', 'Annexe.filetype', 'Annexe.data'],
                    'conditions' => [
                        'filetype' => ["application/vnd.oasis.opendocument.text", "application/pdf"]]
                ]
            ]
        ]);

        $projets = [];
        foreach ($delibs as $delib) {
            $projets[] = $delib;
        }

        $time_end = microtime(true);
        $this->out(
            '<time>Durée : ' . round($time_end - $time_start, 2) . 's</time>',
            1,
            Shell::VERBOSE
        );

        return $projets;
    }

    /**
     * Function ajouterLigneAuRapport
     *
     * Ajoute une ligne avec les informations sur l'annexe à la variable $rapport
     *
     * @param integer $id           numéro de délib
     * @param string  $model        nom du model
     * @param string  $column       nom de la colonne dans la base de données
     * @param string  $retourGedooo Informations sur le passage de l'annexe dans gedooo et ses propriétés
     * @return void
     * @version 4.2
     * @deprecated 5.1
     */
    private function ajouterLigneAuRapport($id, $delibId, $model, $column, $retourGedooo)
    {
        $this->rapport .=
            date("d-m-Y H:i:s") . "\t"
            . $id . "\t"
            . $delibId . "\t"
            . $model . "\t"
            . $column . "\t"
            . $retourGedooo . "\n";
    }

    /**
     * Function creerRapport
     *
     * Envoyer le rapport de l'exécution au fichier de log
     *
     * @throws Exception si problème d'accès disque
     * @return string sortie standard
     * @version 4.2
     * @deprecated 5.1
     */
    private function creerRapport()
    {
        $this->out('<info>Création du rapport (' . $this->logPath . ')...</info>');
        $time_start = microtime(true);

        if ($this->logFile->writable()) {
            if ($this->logFile->open('w')) {
                $this->rapport = $this->logFile->prepare($this->rapport);
                $this->logFile->append($this->rapport);
                $this->logFile->close();
            } else {
                throw new Exception("Impossible d'ouvrir le fichier " . $this->logFile->path);
            }
        } else {
            throw new Exception("Impossible d'écrire dans le fichier " . $this->logFile->path);
        }

        $time_end = microtime(true);
        $this->out(
            '<time>Durée : ' . round($time_end - $time_start, 2) . 's</time>',
            1,
            Shell::VERBOSE
        );
    }

    /**
     * Function sendToGedooo
     *
     * Envoi d'un fichier à Gedooo pour tester le retour
     *
     * @param $content
     * @param $filename
     * @param $id
     * @param $modelName
     * @param $column
     * @return string retour {ok,code_erreur}
     * @version 4.2
     * @deprecated 5.1
     */
    private function sendToGedooo($content, $filename, $id, $modelName, $column)
    {
        $time_start = microtime(true);

        //Initialisations
        $mimetype = 'application/vnd.oasis.opendocument.text';

        // Partie principale du document
        $oMainPart = new GDO_PartType();
        $oMainPart->addElement(
            new GDO_ContentType('fichier', $filename, $mimetype, 'binary', $content)
        );
        // Model
        $oTemplate = new GDO_ContentType("", $this->modelFile->path, $mimetype, "binary", $this->modelFile->read());

        //Fusion
        $oFusion = new GDO_FusionType($oTemplate, $mimetype, $oMainPart);
        $oFusion->process();
        $newFile = new File($this->textesFolder->path . DS . $filename . '.odt', false);
        try {
            //Conversion et concaténation
            $oFusion->SendContentToFile($newFile->path);
            if (!$newFile->delete()) {
                throw new Exception("Problème lors de la suppression du fichier :\n" . $newFile->path);
            }
            $retourGedooo = "OK";
            $this->out('<info>' . $retourGedooo . '</info>');
        } catch (Exception $exc) {
            $retourGedooo = "KO";
            $this->out('<info>' . $retourGedooo . '</info>');
            $this->out("<warning>" . $exc->getMessage() . "</warning>");
            //$this->out("<warning>Trace :\n" . $exc->getTraceAsString() . "</warning>");
            $this->textesInError[] = ['id' => $id, 'model' => $modelName, 'filename' => $filename, 'column' => $column];
        }

        $time_end = microtime(true);
        $this->out(
            "<time>Durée : " . round($time_end - $time_start, 2) . 's</time>',
            1,
            Shell::VERBOSE
        );

        return $retourGedooo;
    }

    /**
     * Function countAnnexesForDelibs
     *
     * Compte les annexes contenu par les délibs
     *
     * @param Deliberation $delibs délibs avec annexes à compter
     * @return int
     * @version 4.2 / 4.3
     */
    public function countAnnexesForDelibs($delibs)
    {
        $nbAnnexes = 0;
        foreach ($delibs as $projet) {
            $nbAnnexes += count($projet['Annexe']);
        }
        return $nbAnnexes;
    }
}
