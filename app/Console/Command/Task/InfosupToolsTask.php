<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('ConversionComponent', 'Controller/Component');

/**
 * Application Task for Cake
 *
 * @version 4.3
 * @package app.Console.Command.Task
 */
class InfosupToolsTask extends Shell
{
    public $uses = ['Infosup', 'Infosupdef'];

    /**
     * @version 4.3
     */
    public function execute()
    {
    }

    /**
     * Function conversion
     *
     * @param int $id Identifiant
     * @param bool $refresh
     * @return string
     *
     * @throws Exception avec erreur 500
     *
     * @version 4.4
     */
    public function conversion($model, $id = null, $refresh = false)
    {
        try {
            $collection = new ComponentCollection();
            $this->Conversion = new ConversionComponent($collection);

            $DOC_TYPE = Configure::read('DOC_TYPE');
            $conditions = ['Infosup.content IS NOT NULL','Infosup.content != '=>''];

            // add is not empty
            if (!empty($id)) {
                $conditions['Infosup.id'] = $id;
            }

            if (!$refresh) {
                $conditions['OR'] = ['edition_data IS NULL', 'edition_data' => ''];
            }
            if (!empty($model)) {
                $conditions['Infosupdef.model'] = $model;
            }

            $infosups = $this->Infosup->find('all', [
                'fields' => ['Infosup.id', 'foreign_key', 'file_type', 'content', 'modified',
                    'infosupdef_id', 'Infosupdef.id', 'Infosupdef.type'],
                'conditions' => $conditions,
                'joins' => [
                    $this->Infosup->join('Infosupdef', [
                        'type' => 'INNER',
                        'conditions' => [
                            'type' => ['richText', 'file'],
                            'joindre_fusion' => true
                        ]
                    ]),
                    $this->Infosup->join($model, ['type' => 'INNER'])
                ],
                'order' => ['Infosup.modified' => 'ASC'],
                'recursive' => -1
            ]);

            if (!empty($infosups)) {
                foreach ($infosups as $infosup) {
                    $this->Infosup->id = $infosup['Infosup']['id'];
                    switch ($infosup['Infosupdef']['type']) {
                        case 'richText':
                            $newInfosup['edition_data'] = $this->Conversion->convertirFlux(
                                $infosup['Infosup']['content'],
                                'html',
                                'odt',
                                false
                            );
                            break;
                        case 'file':
                            $extension = $DOC_TYPE[$infosup['Infosup']['file_type']]['extension'];
                            $convertir = $DOC_TYPE[$infosup['Infosup']['file_type']]['convertir'];
                            $joindre_fusion = $DOC_TYPE[$infosup['Infosup']['file_type']]['joindre_fusion'];
                            if (empty($extension)) {
                                throw new Exception(__('Type inconnu !'), 500);
                            }
                            if ($joindre_fusion==false) {
                                return;
                            }
                            if (is_array($extension)) {
                                $extension = $extension[0];
                            }
                            if ($convertir) {
                                $newInfosup['edition_data'] = $this->Conversion->convertirFlux(
                                    $infosup['Infosup']['content'],
                                    $extension,
                                    'odt'
                                );
                            } else {
                                $newInfosup['edition_data'] = $this->Conversion->toOdt(
                                    $infosup['Infosup']['content'],
                                    $infosup['Infosup']['file_type'],
                                    'application/vnd.oasis.opendocument.text'
                                );
                            }
                            break;
                        default:
                            throw new Exception(__('Type inconnu !'), 500);
                    }

                    if (!empty($newInfosup)) {
                        $newInfosup['edition_data_typemime'] = 'application/vnd.oasis.opendocument.text';
                        $this->Infosup->save($newInfosup);
                    }
                }
                return Cron::MESSAGE_FIN_EXEC_SUCCES
                    . __('%s Infosup (%s) convertie(s)', count($infosups), $model);
            } else {
                return Cron::MESSAGE_FIN_EXEC_SUCCES
                    . __(' Aucune Infosup (%s) à convertir', $model);
            }
        } catch (Exception $e) {
            $message = Cron::MESSAGE_FIN_EXEC_ERROR . ' ' . $e->getMessage() . ' id=' . (isset($this->Infosup->id)
                    ? $this->Infosup->id : 'Inconnu');
            throw new Exception($message, 500);
        }
    }
}
