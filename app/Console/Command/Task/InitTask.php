<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * Application Task for Cake
 * @package app.Console.Command.Task
 */
class InitTask extends Shell
{
    public $uses = [];

    /**
     * Function main
     *
     * @throws JsonException
     */
    public function initializeData()
    {
        $this->out(__('<info>initialize Data</info> Please wait...'), 1, Shell::QUIET);
        $collectivite = ClassRegistry::init('Collectivite');
        $collectivite->Behaviors->load('DynamicDbConfig');
        $this->out(__('<info>initialize Collectivite Data</info> Please wait...'), 1, Shell::QUIET);
        $collectivite->create();
        $collectivite->save([
            'id_entity' => null,
            'nom' => 'webdelib',
            'adresse' => '836 Rue du Mas de Verchant',
            'CP' => '34000',
            'ville' => 'MONTPELLIER',
            'telephone' => '0467659644',
            'logo' => '',
            'templateProject' => null,
            'background' => '',
            'config_login' => null,
            'siret' => '49101169800033',
            'code_ape' => '',
            'email' => 'collectivite@webdelib.invalid',
            'responsable_nom' => '',
            'responsable_prenom' => '',
            'responsable_fonction' => '',
            'responsable_email' => '',
            'dpo_nom' => '',
            'dpo_prenom' => '',
            'dpo_email' => '',
            'saas' => false,
            'force' => 1
        ], false);
        $this->out(__('<info>initialize Service Data</info> Please wait...'), 1, Shell::QUIET);
        $service = ClassRegistry::init('Service');
        $service->Behaviors->load('DynamicDbConfig');
        $service->create();
        $service->save([
            'parent_id' => 0,
            'order' => '',
            'name' => "Informatique",
            'circuit_defaut_id' => 0,
            'actif' => true
        ], false);
        $this->out(__('<info>initialize Profil Data</info> Please wait...'), 1, Shell::QUIET);
        $profil = ClassRegistry::init('Profil');
        $profil->Behaviors->load('DynamicDbConfig');
        $profil->create();
        $profil->save([
            'parent_id' => 0,
            'name' => "Administrateur",
            'actif' => true,
            'role_id' => 2
        ], false);
        $this->out(__('<info>initialize User Data</info> Please wait...'), 1, Shell::QUIET);
        $user = ClassRegistry::init('User');
        $user->Behaviors->load('DynamicDbConfig');
        $user->create();
        $user->save([
            'profil_id' => 1,
            'statut' => 1,
            'username' => 'admin',
            'circuit_defaut_id' => 1,
            'password' => env('APP_ADMIN_PASSWORD'),
            'nom' => 'admin',
            'prenom' => 'admin',
            'email' => 'admin@webdelib.invalid',
            'telfixe' => '',
            'date_naissance' => null,
            'accept_notif' => false,
            'mail_refus' => false,
            'mail_traitement' => false,
            'mail_insertion' => false,
            'position' => null,
            'mail_modif_projet_cree' => false,
            'mail_modif_projet_valide' => false,
            'mail_retard_validation' => false,
            'theme' => null,
            'active' => true,
            'mail_error_send_tdt_auto' => false,
            'mail_projet_valide' => false,
            'mail_projet_valide_valideur' => false,
            'mail_majtdtcourrier' => false,
            'service_defaut_id' => null,
            'preference_first_login_active' => false,
            'mail_error_sae' => false,
        ], false);
        $this->out(__('<info>initialize ServiceUser Data</info> Please wait...'), 1, Shell::QUIET);

        $serviceUser = ClassRegistry::init('ServiceUser');
        $serviceUser->Behaviors->load('DynamicDbConfig');
        $serviceUser->create();
        $serviceUser->save([
            'id' => 1,
            'user_id' => 1,
            'service_id' => 1,
        ], false);
    }
}
