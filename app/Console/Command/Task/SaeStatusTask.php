<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('Sae', 'Lib');

/**
 * Sae Task
 *
 * @package app.Console.Command.Task
 */
class SaeStatusTask extends Shell
{
    public $uses = [
        'SaeVersement',
        'Seance',
        'Deliberation',
    ];

    /**
     * Function maSaeStatus
     * Mise à jour des Ar et des documents disponibles
     *
     * @version 5.1
     */
    public function updateStatesSeances($delib_id, $days): string
    {
        if (!Configure::read('USE_PASTELL')) {
            return 'TRAITEMENT_TERMINE_OK'. "\n" .'Le connecteur parapheur n\'est pas activé';
        }

        $seances = $this->Seance->find('all', [
            'fields' => ['id', 'sae_pastell_id'],
            'conditions' => [
                'sae_etat' => [SaeVersement::ERREUR, SaeVersement::VERSER],
                'sae_pastell_id <>' => null,
            ],
            'order' => ['Seance.date' => 'ASC'],
            'recursive' => -1
        ]);

        if (empty($seances)) {
            return 'TRAITEMENT_TERMINE_OK' . "\n" . 'Rien à faire';
        }

        $rapport = '';
        foreach ($seances as $seance) {
            try {
                $rapport .= $this->SaeVersement->updateSeance(
                    $seance['Seance']['id'],
                    $seance['Seance']['sae_pastell_id']
                );
            } catch (Exception $e) {
                if ($e->getCode() !== 500) {
                    $rapport .= __('Séance %s : Versement en erreur (%s)', $e->getCode()). "\n";
                    continue;
                }
                return 'TRAITEMENT_TERMINE_ERREUR' . $rapport;
            }
        }

        return 'TRAITEMENT_TERMINE_OK' . $rapport;
    }

    /**
     * Function maSaeStatus
     * Mise à jour des Ar et des documents disponibles
     *
     * @version 5.1
     */
    public function updateStatesActes($delib_id, $days): string
    {
        if (!Configure::read('USE_PASTELL')) {
            return 'TRAITEMENT_TERMINE_OK'. "\n" .'Le connecteur parapheur n\'est pas activé';
        }

        $actes = $this->Deliberation->find('all', [
            'fields' => ['id', 'pastell_id'],
            'conditions' => [
                'sae_etat' => [SaeVersement::ERREUR, SaeVersement::VERSER],
                'pastell_id <>' => null,
            ],
            'order' => ['Deliberation.date_acte' => 'ASC'],
            'recursive' => -1
        ]);

        if (empty($actes)) {
            return 'TRAITEMENT_TERMINE_OK' . "\n" . 'Rien à faire';
        }

        $rapport = '';
        foreach ($actes as $acte) {
            try {
                $rapport .= $this->SaeVersement->updateActe(
                    $acte['Deliberation']['id'],
                    $acte['Deliberation']['pastell_id']
                );
            } catch (Exception $e) {
                if ($e->getCode() !== 500) {
                    $rapport .= __('Acte %s : Versement en erreur (%s)', $e->getCode()). "\n";
                    continue;
                }
                return 'TRAITEMENT_TERMINE_ERREUR' . $rapport;
            }
        }

        return 'TRAITEMENT_TERMINE_OK' . $rapport;
    }
}
