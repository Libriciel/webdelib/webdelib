<?php
/**
 *
 * CakeflowTask file
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       webdelib v4.2
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */
App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('AppTask', 'Console/Command/Task');

/**
 * Application Task for Cake
 *
 * @version 4.2
 * @package app.Console.Command.Task
 */
class CakeflowTask extends AppTask
{
    public $uses = [
        'Cakeflow.Visa',
        'Cakeflow.Etape',
        'Cakeflow.Traitement',
        'Cakeflow.Circuit',
        'Cakeflow.Composition',
        'Deliberation',
        'User'
    ];

    public function execute()
    {
    }

    /**
     * Function majTraitementsParapheur
     */
    public function majTraitementsParapheur()
    {
        return $this->Traitement->majTraitementsParapheur();
    }

    /**
     * Function alerteRetard
     * @return string
     */
    public function alerteRetard()
    {
        try {
            $users = [];
            $traitements = $this->Traitement->find(
                'all',
                [
                    'contain' => [
                    'Visa' => [
                        'conditions' => [
                            'not' => ['Visa.date_retard' => null],
                            'Visa.date_retard <=' => date('Y-m-d H:i:s'),
                            'Visa.action' => 'RI'],
                        'fields' => [
                            'Visa.id', 'Visa.date_retard',
                            'Visa.numero_traitement',
                            'Visa.trigger_id',
                            'Visa.action']
                    ]
                    ],
                    'fields' => [
                        'Traitement.id',
                        'Traitement.numero_traitement',
                        'Traitement.target_id'
                    ],
                    'conditions' => ['treated' => false],
                    'recursive' => -1
                ]
            );
            $messages = [];
            foreach ($traitements as $traitement) {
                if (empty($traitement['Visa'])) {
                    continue;
                }

                $this->Deliberation->recursive = -1;
                $this->Deliberation->id = $traitement['Traitement']['target_id'];

                if (!$this->Deliberation->exists()) {
                    continue;
                }

                if ($this->Deliberation->field('etat') !== 1
                    || $this->Deliberation->field('signee')) {
                    continue;
                }

                foreach ($traitement['Visa'] as $visa) {
                    // Si le visa ne respecte pas ces conditions, on passe au suivant
                    if (empty($visa['date_retard'])
                        || $visa['date_retard'] > date(Cron::FORMAT_DATE)
                        || $visa['action'] != 'RI'
                        || $traitement['Traitement']['numero_traitement'] != $visa['numero_traitement']
                    ) {
                        continue;
                    }
                    $messages[$visa['trigger_id']][] = $traitement['Traitement']['target_id'];
                }
            }
            foreach ($messages as $user => $targets) {
                $blaze = $this->User->prenomNomLogin($user);
                if (!empty($blaze) && !in_array($blaze, $users, true)) {
                    $users[] = $blaze;
                }
                //FIX affichage des personnes notification non active !!!
                //Envoi notification
                foreach ($targets as $target) {
                    $this->User->notifier($target, $user, 'retard_validation');
                }
            }

            if (empty($users)) {
                return Cron::MESSAGE_FIN_EXEC_SUCCES
                    . " Aucun utilisateur en retard";
            } else {
                return Cron::MESSAGE_FIN_EXEC_SUCCES
                    . " Utilisateurs alertés :\n\n" . implode(",\n", $users);
            }
        } catch (Exception $e) {
            return Cron::MESSAGE_FIN_EXEC_ERROR . ' ' . $e->getMessage();
        }
    }
}
