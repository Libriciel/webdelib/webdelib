<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('ConversionComponent', 'Controller/Component');
App::uses('S2lowComponent', 'Controller/Component');
App::uses('Tdt', 'Lib');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Tdt Task
 * @package app.Console.Command.Task
 */
class TdtTask extends Shell
{
    public $uses = [
        'Deliberation',
        'Annexe',
        'TdtMessage',
        'Profil',
        'Nature',
        'Seance',
        'Acteurseance'
    ];

    public function execute()
    {
    }

    /**
     * Function recupDataMessageTdt
     * Récuperation les documents Tdt
     * @return bool
     */
    public function recupDataMessageTdt()
    {
        try {
            $collection = new ComponentCollection();
            $this->S2low = new S2lowComponent($collection);

            $Tdt = new Tdt();
            $messages = $this->TdtMessage->find('all', [
                'fields' => ['id', 'tdt_id', 'delib_id'],
                'contains' => 'Deliberation',
                'conditions' => ['tdt_data NOT' => null],
                'recursive' => -1,
            ]);
            foreach ($messages as $message) {
                $this->out('Récupération pour le message Tdt => ' . $message['TdtMessage']['id']);
                $this->TdtMessage->id = $message['TdtMessage']['id'];
                $resultatTdtMessage = $Tdt->getDocument($message['TdtMessage']['tdt_id']);
                $this->TdtMessage->saveField('tdt_data', $resultatTdtMessage);
                $this->out('Ok');
            }
            return true;
        } catch (Exception $e) {
            throw new Exception('Échec lors de la récupération du message Tdt => '
                . $message['TdtMessage']['id']);
        }
        return false;
    }

    /**
     * Function majDateAr
     * Update date Ar for actes
     */
    public function updateDateAr()
    {
        $rapport = '';
        $actes = $this->Deliberation->find('all', [
            'fields' => ['id',
                'tdt_id',
                'pastell_id',
                'tdt_ar_date',
                'num_delib'
            ],
            'conditions' => ['etat' => 5,
                'tdt_ar_date' => null
            ],
            'recursive' => -1
        ]);

        if (empty($actes)) {
            $rapport = 'Rien à faire';
        }
        foreach ($actes as $acte) {
            try {
                $ar = $this->updateAr($acte);
                if (!empty($ar)) {
                    $rapport .= "Délibération " . $acte['Deliberation']['num_delib'] . " : "
                        . __('reçue le %s', date('d/m/Y', strtotime($ar))) . ".\n";
                } else {
                    $rapport .= "Délibération " . $acte['Deliberation']['num_delib'] . " : "
                        . __('En attente de réception') . "\n";
                }
            } catch (Exception $e) {
                $rapport .= "Délibération " . $acte['Deliberation']['num_delib'] . " : "
                    . __('Erreur :') .$e->getMessage(). "\n";
            }
        }

        return $rapport;
    }

    /**
     * Function updateAr
     * Update date Ar for actes
     * @param Object $acte Deliberation
     * @return bool succès
     */
    private function updateAr($acte)
    {
        App::uses('Tdt', 'Lib');
        $Tdt = new Tdt();
        //date au 20xx-xx-xx
        $ar = $Tdt->getDateAr($acte);

        if ($ar) {
            $this->Deliberation->id = $acte['Deliberation']['id'];
            $this->Deliberation->saveField('tdt_ar_date', $ar);
            return $ar;
        } else {
            return false;
        }
    }

    /**
     * Function majTdtFiles
     * Update à jour des Ar et des documents disponibles
     */
    public function updateTdtFiles($delib_id, $days)
    {
        return $this->Deliberation->updateTdtFiles($delib_id, $days);
    }

    /**
     * Function majTdtStatus
     * Mise à jour des Ar et des documents disponibles
     */
    public function updateTdtStatus($delib_id, $days)
    {
        return $this->Deliberation->updateTdtStatus($delib_id, $days);
    }

    /**
     * Function majTdtMinisterialMails
     * Récupération des courriers de la préfecture
     */
    public function updateTdtMinisterialMails($delib_id, $days)
    {
        return $this->Deliberation->updateTdtMinisterialMails($delib_id, $days);
    }

    /**
     * Function sendAuto
     * Envoi des actes en automatique
     */
    public function sendAuto()
    {
        App::uses('Tdt', 'Lib');
        $tdt = new Tdt();
        $rapport = '';
        $actes = $this->Deliberation->find('all', [
            'fields' => ['id'],
            'joins' => [
                [
                    'table' => 'typeactes',
                    'alias' => 'Typeacte_Filter',
                    'type' => 'INNER',
                    'conditions' => [
                        'Deliberation.typeacte_id = Typeacte_Filter.id',
                        'Typeacte_Filter.teletransmettre' => true
                    ]
                ],
            ],
            'conditions' => [
                'etat' => 3,
                'signee' => true,
                'tdt_id' => null,
                'NOT' => ['date_acte' => null]
            ],
            'recursive' => -1,
        ]);

        $error = 0;
        if (empty($actes)) {
            $rapport = 'Rien à faire';
        }
        foreach ($actes as $acte) {
            $this->Deliberation->clear();
            try {
                $this->Deliberation->id = $acte['Deliberation']['id'];
                $this->Deliberation->recursive = -1;
                $acte = $this->Deliberation->read();
                $acte['acte_nature_code'] =
                    $this->Nature->getNatureCodeByTypeActe($acte['Deliberation']['typeacte_id']);
                $typologiepieceAnnexes = $this->Annexe->findAllByForeignKeyAndJoindreCtrlLegalite(
                    $acte['Deliberation']['id'],
                    true,
                    ['typologiepiece_code'],
                    ['position' => 'ASC'],
                    null,
                    null,
                    -1
                );
                $acte['annexe_typologiepiece_code'] = !empty($typologiepieceAnnexes) ? Hash::extract(
                    $typologiepieceAnnexes,
                    '{n}.Annexe.typologiepiece_code'
                ) : '';

                if (empty($acte['acte_nature_code'])) {
                    throw new Exception(
                        __(
                            '%s (%s) : Aucune nature disponible.',
                            $this->Deliberation->field('objet_delib'),
                            $this->Deliberation->field('num_delib')
                        )
                    );
                }
                if (empty($acte['acte_nature_code'])) {
                    throw new Exception(
                        __('Nature manquante pour envoyer l\'acte %s', $acte['Deliberation']['num_delib'])
                    );
                }

                if (empty($acte['Deliberation']['num_pref'])) {
                    throw new Exception(
                        __('Classification manquante pour envoyer l\'acte %s', $acte['Deliberation']['num_delib'])
                    );
                }
                if (!empty($typologiepieceAnnexes)) {
                    if (empty($acte['annexe_typologiepiece_code'])) {
                        throw new Exception(
                            __(
                                '%s (%s) : Un ou plusieur type de pièce n\'ont pas été sélectionné pour les annexes.',
                                $this->Deliberation->field('objet_delib'),
                                $this->Deliberation->field('num_delib')
                            )
                        );
                    }
                    if (count($typologiepieceAnnexes) !== count($acte['annexe_typologiepiece_code'])) {
                        throw new Exception(
                            __(
                                '%s (%s) : Un ou plusieur type de pièce n\'ont pas été sélectionné pour les annexes.',
                                $this->Deliberation->field('objet_delib'),
                                $this->Deliberation->field('num_delib')
                            )
                        );
                    }
                }

                $docPrincipal = $this->Deliberation->field('delib_pdf');
                $annexes = $this->Deliberation->getAnnexesToSend($acte['Deliberation']['id']);
                $tdt_id = $tdt->send($acte, $docPrincipal, $annexes);
                $message = __(
                    'Acte %s envoyé le %s (Numéro d\'enveloppe : %s)',
                    $acte['Deliberation']['num_delib'],
                    date('d/m/Y'),
                    $tdt_id
                );
                $this->out('<success>' . $message . '</success>', 1, Shell::VERBOSE);
                $rapport .= $message . "\n";
            } catch (Exception $e) {
                $this->out('<error>' . $e->getMessage() . '</error>', 1, Shell::VERBOSE);
                $rapport .= $e->getMessage() . "\n";
                $error++;
                continue;
            }
        }

        if (!empty($error)) {
            $this->Profil->notifierAdmin('error_send_tdt_auto', $rapport);
        }

        return $rapport;
    }

    /**
     * Function conversion
     * @param int $id Identifiant
     * @param bool $refresh
     * @return string
     */
    public function conversion($model, $id = null, $refresh = false)
    {
        try {
            $collection = new ComponentCollection();
            $this->Conversion = new ConversionComponent($collection);

            $conditions = [
              'tdt_data_pdf IS NOT NULL',
              'tdt_data_pdf <>' => '',
              'created >' => '2018-01-01 00:00:00'
            ];
            if (!empty($id)) {
                array_push($conditions, [$model . '.id' => $id]);
            }

            if (!$refresh) {
                array_push($conditions, 'tdt_data_edition IS NULL');
            }

            $datas = $this->{$model}->find('all', [
                'fields' => ['id', 'tdt_data_pdf','tdt_data_edition'],
                'conditions' => $conditions,
                'order' => ['modified' => 'DESC'],
                'limit' => 100,
                'recursive' => -1,
            ]);
            if (!empty($datas)) {
                foreach ($datas as $data) {
                    $this->{$model}->id = $data[$model]['id'];
                    if (!$this->{$model}->saveField(
                        'tdt_data_edition',
                        $this->Conversion->toOdt(
                            $data[$model]['tdt_data_pdf'],
                            'application/pdf',
                            'application/vnd.oasis.opendocument.text'
                        )
                    )
                    ) {
                        throw new Exception('Conversion ToOdt', 500);
                    }
                    $this->{$model}->clear();
                }
                return Cron::MESSAGE_FIN_EXEC_SUCCES
                    . __('%s Data TDT (%s) convertie(s)', count($datas), $model);
            } else {
                return Cron::MESSAGE_FIN_EXEC_SUCCES . __(' Aucune Data TDT (%s) à convertir', $model);
            }
        } catch (Exception $e) {
            $message = Cron::MESSAGE_FIN_EXEC_ERROR . ' ' . $e->getMessage() . ' id=' . (isset($this->{$model}->id)
                    ? $this->{$model}->id : 'Inconnu');
            throw new Exception($message, 500);
        }
    }
}
