<?php

class AppTask extends Shell
{
    public function initialize()
    {
        parent::initialize();

        if (is_array($this->uses)) {
            foreach ($this->uses as $modelClass) {
                list($plugin, $modelClass) = pluginSplit($modelClass);
                $this->{$modelClass}->Behaviors->load('DynamicDbConfig');
            }
        }
    }

    public function getTenantName()
    {
        return Configure::read('Config.database');
    }
}
