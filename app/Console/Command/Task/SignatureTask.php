<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('Signature', 'Lib');
//App::uses('S2lowComponent', 'Controller/Component');

/**
 * Application Task for Cake
 *
 * @version 4.3
 * @package app.Console.Command.Task
 */
class SignatureTask extends Shell
{
    public $uses = [];

    /**
     * @version 4.3
     */
    public function execute()
    {
    }

    /**
     * Function updateAll
     *
     * @version 4.3
     */
    public function updateAll()
    {
        if (!Configure::read('USE_PARAPHEUR')) {
            return 'TRAITEMENT_TERMINE_OK'. "\n" .'Le connecteur parapheur n\'est pas activé';
        }

        $signature = new Signature();

        return $signature->updateAll();
    }
}
