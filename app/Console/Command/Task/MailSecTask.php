<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('MethodCurl', 'Lib');
App::uses('ConnectorS2low', 'Lib');

/**
 * [MailSecTask description]
 *
 * @version 5.2.0
 */
class MailSecTask extends Shell
{
    /**
     * [private description]
     * @var [type]
     */
    protected $useMailSec;

    /**
     * [private description]
     * @var [type]
     */
    protected $connecteurType;

    /**
     * [public description]
     * @var [type]
     */
    public $uses = ['Cron','Acteurseance'];

    /**
     * [initialize description]
     * @return [type] [description]
     *
     * @version 5.2.0
     */
    public function initialize()
    {
        parent::initialize();

        $this->useMailSec = Configure::read('USE_MAILSEC');
        $this->connecteurType = Configure::read('MAILSEC');
    }

    /**
     * Function mise à jour des mail securisé
     *
     * @version 5.2.0
     */
    public function updateAll()
    {
        if (!Configure::read('USE_MAILSEC')) {
            return Cron::EXECUTION_STATUS_SUCCES . "\n" .'Le connecteur Mail sécurisé n\'est pas activé';
        }

        try {
            if ($this->useMailSec) {
                if ($this->connecteurType =='PASTELL') {
                    $rapport = $this->updateAllPastell();
                } else {
                    $rapport = $this->updateAllS2low();
                }
            }
            return Cron::MESSAGE_FIN_EXEC_SUCCES . $rapport;
        } catch (Exception $e) {
            return Cron::MESSAGE_FIN_EXEC_ERROR . $e->getMessage();
        }
    }

    /**
     * [updateAllS2low description]
     * @return [type] [description]
     *
     * @version 5.2.0
     */
    private function updateAllS2low()
    {
        return $this->saveDateReception($this->listMailSecSendS2lowInfoTdt($this->listMailSend()));
    }

    /**
     * [updateAllPastell description]
     * @return [type] [description]
     *
     * @version 5.2.0
     */
    private function updateAllPastell()
    {
        return $this->saveDateReception($this->listMailSecSendPastellInfoDocuments($this->listMailSend()));
    }

    /**
     * [saveDateReception description]
     * @return [type] [description]
     *
     * @version 5.2.0
     */
    private function saveDateReception($resultats)
    {
        $rapport = __('Reception de l\'accusé de :') . "\n";
        $save = false;

        foreach ($resultats as $mailId => $dateReception) {
            $save = true;
            $acteur = $this->Acteurseance->find(
                'first',
                [
                  'fields' => ['Acteur.salutation','Acteur.nom','Acteur.prenom'],
                  'joins' => [$this->Acteurseance->join('Acteur', ['type' => 'INNER'])],
                  'conditions' => ['Acteurseance.id' => $mailId],
                  'recursive' => -1
                ]
            );

            $rapport .= __(
                '%s %s %s (%s)',
                $acteur['Acteur']['salutation'],
                $acteur['Acteur']['nom'],
                $acteur['Acteur']['prenom'],
                $dateReception
            ) . "\n";

            $this->Acteurseance->id = $mailId;
            $this->Acteurseance->saveField('date_reception', $dateReception);
        }

        return $save ? $rapport : '';
    }

    /**
     * [listMailSend description]
     * @return [type] [description]
     *
     * @version 5.2.0
     */
    private function listMailSend()
    {
        return $this->Acteurseance->find('list', [
          'fields' => ['Acteurseance.id','Acteur.email','Acteurseance.mail_id'],
          'joins' => [
              $this->Acteurseance->join('Acteur', ['type' => 'INNER']),
              $this->Acteurseance->join('Seance', [
                'type' => 'INNER', 'conditions' => ['traitee' => 0]
              ])],
          'conditions' => [
            'mail_id <>' => null,
            'date_envoi <>' => null,
            'date_reception' => null
          ],
          'recursive' => -1
        ]);
    }

    /**
     * [listMailSecSendS2lowInfoTdt description]
     * @return [type] [description]
     *
     * @version 5.2.0
     */
    private function listMailSecSendS2lowInfoTdt($mails)
    {
        $resultats = [];
        $connectorS2low = new ConnectorS2low();

        foreach ($mails as $mailId => $mail) {
            $response = $connectorS2low->checkMailSec($mailId);
            $resultats += $this->parseS2lowReponseWithMails($response, $mail);
        }

        return $resultats;
    }

    /**
     * [listMailSecSendPastellInfoDocuments Récupère les informations de Pastell]
     * @param  [type] $mails [description]
     * @return [type]        [description]
     *
     * @version 5.2.0
     */
    private function listMailSecSendPastellInfoDocuments($mails)
    {
        $resultats = [];

        $options["CurlOptions"] = [
          CURLOPT_USERPWD => Configure::read('PASTELL_MAILSEC_LOGIN') . ':' . Configure::read('PASTELL_MAILSEC_PWD')
        ];

        $HttpRequest = new MethodCurl(Configure::read('PASTELL_MAILSEC_HOST') . '/api/', $options);

        foreach ($mails as $mailId => $mail) {
            $responses = $HttpRequest->doRequest(
                'journal.php?id_e=' .Configure::read('PASTELL_MAILSEC_IDE'),
                [
                  "CurlOptions" =>
                  [
                    CURLOPT_VERBOSE => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_SSL_VERIFYPEER => Configure::read('PASTELL_MAILSEC_SSL_ALLOW_SELF_SIGNED'),
                  ],
                  "GetParameters"=> [
                    'id_d' => $mailId
                  ]
                ]
            );

            $resultats += $this->parsePastellReponseWithMails($responses, $mail);
        }

        return $resultats;
    }

    /**
     * [parseReponse Parse la réponse de S2low]
     * @param  [type] $response [description]
     * @return [type]           [description]
     *
     * @version 5.2.0
     */
    private function parseS2lowReponseWithMails($response, $mail)
    {
        $resultats = [];
        $debut = strpos($response, 'mailTo:t:');
        $tmp = substr($response, $debut + strlen('mailTo:t:'), strlen($response));
        $fin = strpos($tmp, '==message==');
        $date = trim(substr($tmp, 0, $fin));
        if ($debut !== false) {
            $resultats[key($mail)] = $date;
        }

        return $resultats;
    }

    /**
     * [parseReponse Parse la réponse de Pastell]
     * @param  [type] $response [description]
     * @return [type]           [description]
     *
     * @version 5.2.0
     */
    private function parsePastellReponseWithMails($responses, $mails)
    {
        $responses = json_decode($responses, true);

        $resultats = [];
        foreach ($mails as $key => $mail) {
            if (Hash::check($responses, '{*}[action=Consulté][message=/' . $mail . '/]')) {
                $date = Hash::extract($responses, '{*}[action=Consulté][message=/' . $mail . '/].date');
                $resultats[$key] = end($date);
            }
        }

        return $resultats;
    }
}
