<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('ComponentCollection', 'Controller');
App::uses('Controller', 'Controller');
App::uses('CronsComponent', 'Controller/Component');
App::uses('CakeTime', 'Utility');

/**
 * Console CronShell
 *
 * @package app.Console.Command.CronShell
 * @version v4.2
 */

class CronShell extends AppShell
{
    public $uses = ['Cron'];
    public $tasks = [
        'Tdt',
        'Cakeflow',
        'Signature',
        'AnnexeTools',
        'InfosupTools',
        'WebDav',
        'MailSec',
        'SaeStatus'
    ];

    public function main()
    {
        if ($this->param('tenant')) {
            $this->runPending();
            return;
        }
        foreach ($this->getFqdnDatabases() as $fqdn => $database) {
            if ($database !== 'default') {
                $this->setDbMultiBase($database);
            }
            $this->runPending();
        }
    }

    /**
     * Function getOptionParser
     *
     * Options d'exécution et validation des arguments
     *
     * @return Parser $parser
     * @version 4.3
     */
    public function getOptionParser()
    {
        $plugin = [
            'short' => 'p',
            'help' => __('Plugin to process'),
        ];
        $parser = parent::getOptionParser();

        $parser
            ->addSubcommand('sendTdtAuto', [
                'help' => __('Envoi automatique des actes vers le Tdt.'),
            ])->addSubcommand('majTdtMailSec', [
                'help' => __('Mise à jour des mails sécurisés'),
            ])->addSubcommand('majParapheurSignature', [
                'help' => __('Mise à jour des signatures.'),
            ])->addSubcommand('majParapheurDelegation', [
                'help' => __('Mise à jour des délégations de circuit.'),
            ])->addSubcommand('conversionFiles', [
                'help' => __('Conversion des fichiers.'),
            ])->addSubcommand('runPending', [
                'help' => __('Exécute toutes les tâches planifiée en attente.'),
            ])->addSubcommand('runAll', [
                'help' => __('Lance toutes les tâches automatiques.'),
                'parser' => [
                    'arguments' => [
                        'type' => [
                            'required' => true,
                            'help' => __('Le type d\'arbre à récupérer'),
                            'choices' => ['aco', 'aro']
                        ]
                    ]
                ]
            ])->addSubcommand('runId', [
                'help' => __('Exécute une tâche définie par son identifiant.'),
                'parser' => [
                    'arguments' => [
                        'id' => [
                            // 'name' => 'id',
                            'required' => true,
                            //'short' => 'i',
                            'help' => __('Id de la tâche'),
                        ]
                    ]
                ]
            ])->addSubcommand('updateTdtFiles', [
                'help' => __('Récupération des documents retours de télétransmissions'),
                    'parser' => [
                        'arguments' => [
                            'delib_id' => [
                                'required' => false,
                                'help' => __('Id de l\'acte à récupérer'),
                            ],
                            'days' => [
                                'required' => false,
                                'help' => __('Nombre de jours'),
                            ],
                        ]
                    ]
            ])->addSubcommand('updateTdtStatus', [
                'help' => __('Récupèration des statuts de télétransmission (erreurs et annulations)'),
                    'parser' => [
                        'arguments' => [
                          'id' => [
                              'name' => 'id',
                              'required' => false,
                              'short' => 'i',
                              'help' => __('Id de l\'acte à récupérer')
                          ],
                          'days' => [
                              'name' => 'days',
                              'required' => false,
                              'short' => 'd',
                              'help' => __('Nombre de jours')
                          ],
                        ]
                    ]
            ])->addSubcommand('updateTdtMinisterialMails', [
                'help' => __('Mise à jour des échanges sur les dossiers envoyés en TdT.'),
                'parser' => [
                    'arguments' => [
                        'id' => [
                            'required' => false,
                            'help' => __('Id de l\'acte à récupérer'),
                        ],
                        'days' => [
                            'required' => false,
                            'help' => __('Nombre de jours'),
                        ],
                    ]
                ]
            ])->addSubcommand('updateSaeStatus', [
                'help' => __('Récupèration des statuts de versement (erreurs et annulations)'),
                'parser' => [
                    'arguments' => [
                        'id' => [
                            'name' => 'id',
                            'required' => false,
                            'short' => 'i',
                            'help' => __('Id de l\'acte à récupérer')
                        ],
                        'days' => [
                            'name' => 'days',
                            'required' => false,
                            'short' => 'd',
                            'help' => __('Nombre de jours')
                        ],
                    ]
                ]
            ]);

        return  $parser;
    }

    /**
     * Function sendTdtAuto
     *
     * Envoi des actes automatiquements
     *
     * @version 4.4
     */
    public function sendTdtAuto()
    {
        try {
            $rapport = $this->Tdt->sendAuto();
            return Cron::MESSAGE_FIN_EXEC_SUCCES . ' ' . $rapport;
        } catch (Exception $e) {
            return Cron::MESSAGE_FIN_EXEC_ERROR . ' ' . $e->getMessage();
        }
    }

    /**
     * Function majTdtFile
     *
     * Met à jour les ar et bordereau des dossiers envoyés en tdt
     *
     * @version 5.1
     */
    public function updateTdtFiles($delib_id = null, $days = null)
    {
        try {
            // Met à jour les ar et bordereau des dossiers envoyés en tdt
            $rapport = $this->Tdt->updateDateAr();
            sleep(15);
            $rapport = $this->Tdt->updateTdtFiles($delib_id, $days);
            return Cron::MESSAGE_FIN_EXEC_SUCCES . "\n" . $rapport;
        } catch (Exception $e) {
            return Cron::MESSAGE_FIN_EXEC_ERROR . "\n" . $e->getMessage();
        }
    }

    /**
     * Function majTdtStatus
     *
     * Met à jour les ar et bordereau des dossiers envoyés en tdt
     *
     * @version 5.1
     */
    public function updateTdtStatus()
    {
        try {
            $rapport = $this->Tdt->updateTdtStatus(
                (!empty($this->args['id']) ? $this->args['id'] : null),
                (!empty($this->args['days']) ? $this->args['days'] : null)
            );
            return Cron::MESSAGE_FIN_EXEC_SUCCES . "\n" . $rapport;
        } catch (Exception $e) {
            return Cron::MESSAGE_FIN_EXEC_ERROR . "\n" . $e->getMessage();
        }
    }

    /**
     * Function updateTdtMinisterialMails
     *
     * Met à jour les echanges (TdtMessages) des dossiers envoyés en tdt
     *
     * @version 5.1
     */
    public function updateTdtMinisterialMails($delib_id = null, $days = null)
    {
        try {
            $rapport = $this->Tdt->updateTdtMinisterialMails($delib_id, $days);
            return Cron::MESSAGE_FIN_EXEC_SUCCES . "\n" . $rapport;
        } catch (Exception $e) {
            return Cron::MESSAGE_FIN_EXEC_ERROR . $e->getMessage();
        }
    }

    public function updateSaeStatus()
    {
        try {
            $rapport = 'Seance(s) :' . "\n" . $this->SaeStatus->updateStatesSeances(
                (!empty($this->args['id']) ? $this->args['id'] : null),
                (!empty($this->args['days']) ? $this->args['days'] : null)
            ). "\n" ;
            $rapport .= 'Acte(s) :' . "\n" .$this->SaeStatus->updateStatesActes(
                (!empty($this->args['id']) ? $this->args['id'] : null),
                (!empty($this->args['days']) ? $this->args['days'] : null)
            );
            return Cron::MESSAGE_FIN_EXEC_SUCCES . "\n" . $rapport;
        } catch (Exception $e) {
            return Cron::MESSAGE_FIN_EXEC_ERROR . "\n" . $e->getMessage();
        }
    }



    /**
     * Function majTdtMailSec
     *
     * Mise à jour des projets en délégation dans le iparapheur connecté
     *
     * @return string
     * @version 4.3
     */
    public function majTdtMailSec()
    {
        return $this->MailSec->updateAll();
    }

    /**
     * Function runPending
     *
     * Exécute toutes les tâches planifiée en attente.
     * Vérifie si la date/time d'execution prévue est inférieur à la date/time du jour
     * OU si le délais entre 2 exécutions est dépassé
     *
     * @version 4.3
     */
    public function runPending()
    {
        $rapport = date(Cron::FORMAT_DATE) . "\n";
        // lecture des crons à exécuter
        $crons = $this->Cron->find('all', [
            'recursive' => -1,
            'fields' => ['id', 'nom'],
            'conditions' => [
                'OR' => [
                    'next_execution_time <= ' => date(Cron::FORMAT_DATE),
                    'next_execution_time' => null,
                ],
                'active' => true
            ],
            'order' => ['next_execution_time ASC']]);
        if (!empty($crons)) {
            // exécutions
            foreach ($crons as $cron) {
                $rapport .= $cron['Cron']['id'] . '-'
                    . $cron['Cron']['nom'] . " : \n"
                    . $this->runId($cron['Cron']['id'])
                    . "\n";
            }
        } else {
            $rapport .= __("Aucune tâche planifiée à exécuter");
        }
        return $this->info($rapport);
    }

    /**
     * Function runAll
     *
     * @version 4.3
     */
    public function runAll()
    {
        // lecture des crons à exécuter
        $crons = $this->Cron->find('all', [
            'fields' => ['id'],
            'conditions' => [
                'active' => true,
                'lock' => false,
                'run_all' => true
            ],
            'recursive' => -1,
            'order' => ['next_execution_time ASC']]);
        // exécutions
        if (!empty($crons)) {
            foreach ($crons as $cron) {
                $this->runId($cron['Cron']['id']);
            }
        }
    }

    /**
     * Function _runId
     *
     * fonction d'exécution d'un cron par son id (actif ou non)
     *
     * @param integer $id id du cron a exécuter
     * @return bool|string
     *
     * @version 5.1.4
     * @since 4.3
     */
    public function runId($id = null)
    {
        $id = !empty($this->args[0]) ? $this->args[0] : $id;
        try {
            // initialisation date
            $executionStartTime = date(Cron::FORMAT_DATE);

            // lecture du cron à exécuter
            $cron = $this->Cron->find('first', [
                'fields'    => [
                    'Cron.id',
                    'Cron.active',
                    'Cron.plugin',
                    'Cron.lock',
                    'Cron.action',
                    'Cron.last_execution_status',
                    'Cron.last_execution_end_time',
                    'Cron.last_execution_start_time',
                    'Cron.next_execution_time',
                    'Cron.execution_duration',
                    'Cron.last_execution_report'
                    ],
                'recursive'  => -1,
                'conditions' => [
                    'id' => $id
                    ]]);

            // Sortie si tâche non trouvée
            if (empty($cron)) {
                throw new Exception('Tâche cron non disponible', '404');
            }

            if ($cron['Cron']['active'] === false) {
                throw new Exception('Tâche cron non active', '401');
            }

            if ($cron['Cron']['lock'] === true && !$this->Cron->isExecutionTooLast(
                $cron['Cron']['last_execution_start_time'],
                $cron['Cron']['next_execution_time']
            )
            ) {
                throw new Exception('Tâche déjà en execution', '423');
            }

            $this->Cron->id = $id;
            //Verrouille la tâche pour éviter les exécutions parallèles
            $this->Cron->saveField('lock', true);
            $this->Cron->saveField('last_execution_start_time', $executionStartTime);

            if (!empty($cron['Cron']['has_params'])) {
                $output = $this->dispatchShell('cron '
                . (!empty($cron['Cron']['plugin']) ? ucfirst($cron['Cron']['plugin']) . '.'
                        : '')
                . ucfirst($cron['Cron']['action'])
                . explode(',', $cron['Cron']['params'])
                . ' -t ' . Configure::read('Config.tenantName') . ' ');
            } else {
                $output = $this->dispatchShell('cron '

                . (!empty($cron['Cron']['plugin']) ? ucfirst($cron['Cron']['plugin'])  . '.'
                        : '')
                . ucfirst($cron['Cron']['action'])
                . ' -t ' . Configure::read('Config.tenantName') . ' ');
            }
        } catch (Exception $e) {
            if ($e->getCode() === 404 || $e->getCode() === 401 || $e->getCode() === 423) {
                $this->error(__('%s %s', $e->getCode(), $e->getMessage()));
                return Cron::EXECUTION_STATUS_FAILED;
            }
            $output = Cron::MESSAGE_FIN_EXEC_ERROR . " Exception levée : \n" . $e->getMessage();
            $this->log($e->getMessage() . PHP_EOL . $e->getTraceAsString(), 'error');
        }

        // initialisation du rapport d'exécution
        $rappExecution = str_replace([Cron::MESSAGE_FIN_EXEC_SUCCES,
                                      Cron::MESSAGE_FIN_EXEC_WARNING,
                                      Cron::MESSAGE_FIN_EXEC_ERROR], '', $output);

        $saveCron = [];
        $saveCron['Cron']['lock'] = false;

        if (strpos($output, Cron::MESSAGE_FIN_EXEC_SUCCES) !== false) {
            $saveCron['Cron']['last_execution_status'] = Cron::EXECUTION_STATUS_SUCCES;
        } elseif (strpos($output, Cron::MESSAGE_FIN_EXEC_WARNING) !== false) {
            $saveCron['Cron']['last_execution_status'] = Cron::EXECUTION_STATUS_WARNING;
        } else {
            $saveCron['Cron']['last_execution_status'] = Cron::EXECUTION_STATUS_FAILED;
        }
        $saveCron['Cron']['next_execution_time'] = $this->Cron->calcNextExecutionTime(
            $cron['Cron']['execution_duration'],
            $cron['Cron']['next_execution_time']
        );

        $saveCron['Cron']['last_execution_end_time'] = date(Cron::FORMAT_DATE);
        $saveCron['Cron']['last_execution_report'] = $rappExecution;

        if ($this->Cron->save($saveCron)) {
            return $output;
        } else {
            return Cron::EXECUTION_STATUS_FAILED . "\n" . $output;
        }
    }

    /**
     * Function majParapheurSignature
     *
     * Mise à jour des dossiers en attente de signature parapheur
     *
     * @return string
     * @version 4.3
     */
    public function majParapheurSignature()
    {
        /** @noinspection PhpParamsInspection Lib.Signature::__call() */
        return $this->Signature->updateAll();
    }

    /**
     * Function majParapheurDelegation
     *
     * Mise à jour des projets en délégation dans le iparapheur connecté
     *
     * @return string
     * @version 4.2
     */
    public function majParapheurDelegation()
    {
        return $this->Cakeflow->majTraitementsParapheur();
    }

    /**
     * Function conversionFiles
     *
     * Mise à jour des projets en délégation dans le iparapheur connecté
     *
     * @return string
     * @version 4.4
     */
    public function conversionFiles()
    {
        return $this->AnnexeTools->conversion()
                ."\n". $this->InfosupTools->conversion('Deliberation')
                ."\n". $this->InfosupTools->conversion('Seance')
                ."\n". $this->Tdt->conversion('Deliberation')
                ."\n". $this->Tdt->conversion('Annexe');
    }

    /**
     * Function alerteCakeflowRetard
     *
     * Mise à jour des projets en délégation dans le iparapheur connecté
     *
     * @return string
     * @version 4.2
     */
    public function alerteCakeflowRetard()
    {
        return $this->Cakeflow->alerteRetard();
    }

    /**
     * Function syncLdap
     *
     * Mise à jour des projets en délégation dans le iparapheur connecté
     *
     * @return string
     * @version 4.3
     */
    public function syncLdap()
    {
        try {
            if (Configure::read('LdapManager.Ldap.use')) {
                if ($this->dispatchShell('LdapManager.ldap_manager group_update')) {
                    $output = $this->dispatchShell('LdapManager.ldap_manager user_update');
                    return Cron::MESSAGE_FIN_EXEC_SUCCES . $output;
                } else {
                    throw new Exception('Syncroisation des groupes echoués');
                }
            } else {
                return Cron::MESSAGE_FIN_EXEC_SUCCES . __('Connecteur LDAP non configuré');
            }
        } catch (Exception $e) {
            $output = Cron::MESSAGE_FIN_EXEC_ERROR . " Exception levée : \n" . $e->getMessage();
            $this->log($e->getMessage() . PHP_EOL . $e->getTraceAsString(), 'error');
        }

        return $output;
    }

    /**
     * Function purgeWebDav
     *
     * Mise à jour des projets en délégation dans le iparapheur connecté
     *
     * @return string
     * @version 4.3
     */
    public function purgeWebDav()
    {
        return $this->WebDav->purge();
    }
}
