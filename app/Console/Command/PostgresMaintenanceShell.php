<?php

    /**
     * Code source de la classe PostgresMaintenanceShell.
     *
     * PHP 5.3
     *
     * @package Postgres
     * @subpackage Console.Command
     * @license CeCiLL V2 (http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)
     */
    // @codeCoverageIgnoreStart
    App::uses('PostgresAbstractShell', 'Postgres.Console/Command');
    // @codeCoverageIgnoreEnd

    /**
     * La classe PostgresMaintenanceShell fournit des méthodes de maintenance de
     * base de données PostgreSQL.
     *
     * @see Pgsqlcake.MaintenanceShell (sera dépréciée par cette classe-ci)
     *
     * @package Postgres
     * @subpackage Console.Command
     */
class PostgresMaintenanceShell extends PostgresAbstractShell
{
    /**
     * Description courte du shell
     *
     * @var string
     */
    public $description = 'Shell de maintenance de base de données PostgreSQL';

    /**
     * Liste des sous-commandes et de leur description.
     *
     * @var array
     */
    public $commands = [
        'all' => [
            'help' => "Effectue toutes les opérations de maintenance ( reindex, sequence, vacuum )."
        ],
        'sequences' => [
            'help' => 'Mise à jour des compteurs des champs auto-incrémentés'
        ],
        'vacuum' => [
            'help' => 'Nettoyage de la base de données et mise à jour des statistiques du planificateur'
        ],
        'reindex' => [
            'help' => 'Reconstruction des indexes'
        ],
        'upgradeSerialToIdentity' => [
            'help' => 'Migration des serials vers identity'
        ]
    ];

    /**
     * Liste des options et de leur description.
     *
     * @var array
     */
    public $options = [
        'connection' => [
            'short' => 'c',
            'help' => 'Le nom de la connection à la base de données',
            'default' => 'default'
        ]
    ];

    /**
     * Reconstruction des indexes.
     */
    public function reindex()
    {
        $this->out(date('H:i:s')." - {$this->commands[__FUNCTION__]['help']} (reindex)");
        $sql = sprintf('REINDEX DATABASE "%s";', $this->Dbo->config['database']);
        $success = ($this->Dbo->query($sql) !== false);

        if ($this->command === __FUNCTION__) {
            $this->_stop($success ? self::SUCCESS : self::ERROR);
            return;
        }

        return $success;
    }

    /**
     * Mise à jour des compteurs des champs auto-incrémentés.
     */
    public function sequences()
    {
        $this->out(date('H:i:s')." - {$this->commands[__FUNCTION__]['help']} (sequences)");

        $success = ($this->Dbo->begin() !== false);

        $sql = "SELECT table_schema AS \"Model__schema\",
                        table_name AS \"Model__table\",
						column_name AS \"Model__column\",
						column_default AS \"Model__sequence\",
						is_identity AS \"Model__is_identity\"
						FROM information_schema.columns
						WHERE (column_default LIKE 'nextval(%::regclass)' OR is_identity = 'YES' )
						ORDER BY table_schema, table_name, column_name";

        foreach ($this->Dbo->query($sql) as $model) {
            if (!empty($model['Model']['sequence'])) {
                $sequence = preg_replace('/^nextval\(\'(.*)\'.*\)$/', '\1', $model['Model']['sequence']);
                $sql = "SELECT setval('{$sequence}', COALESCE(MAX({$model['Model']['column']}),0)+1, false)
                    FROM {$model['Model']['schema']}.{$model['Model']['table']};";
                $format = 'Mise à jour de la séquence SERIAL pour la table "%s"."%s"';
                $this->out(sprintf($format, $model['Model']['schema'], $model['Model']['table']), 1, static::VERBOSE);
                $success = $success && ($this->Dbo->query($sql) !== false);
            } else {
                $sql = "SELECT setval(pg_get_serial_sequence('{$model['Model']['schema']}.{$model['Model']['table']}',
                    '{$model['Model']['column']}'), (SELECT MAX({$model['Model']['column']}) FROM
                    {$model['Model']['schema']}.{$model['Model']['table']}));";
                $format = 'Mise à jour de la séquence IDENTITY pour la table "%s"."%s"';
                $this->out(sprintf($format, $model['Model']['schema'], $model['Model']['table']), 1, static::VERBOSE);
                $success = $success && ($this->Dbo->query($sql) !== false);
            }
        }

        if ($success) {
            $success = ($this->Dbo->commit() !== false) && $success;
        } else {
            $success = ($this->Dbo->rollback() !== false) && $success;
        }

        return $success;
    }

    /**
     * Nettoyage de la base de données et mise à jour des statistiques du planificateur
     *
     * @url http://docs.postgresqlfr.org/8.4/maintenance.html (pas FULL)
     */
    public function vacuum()
    {
        $this->out(date('H:i:s')." - {$this->commands[__FUNCTION__]['help']} (vacuum)");
        $sql = "VACUUM ANALYZE;";
        $success = ($this->Dbo->query($sql) !== false);

        if ($this->command === __FUNCTION__) {
            $this->_stop($success ? self::SUCCESS : self::ERROR);
            return;
        }

        return $success;
    }

    /**
     * Effectue toutes les opérations de maintenance ( reindex, sequence, vacuum ).
     */
    public function all()
    {
        $success = true;

        $commands = Hash::remove($this->commands, __FUNCTION__);
        foreach (array_keys($commands) as $command) {
            $success = $success && $this->{$command}();
        }

        $this->_stop($success ? self::SUCCESS : self::ERROR);
    }

    /**
     * Mise à jour des compteurs des champs auto-incrémentés.
     */
    public function upgradeSerialToIdentity()
    {
        $this->out(date('H:i:s')." - {$this->commands[__FUNCTION__]['help']} (upgradeSerialToIdentity)");

        $success = ($this->Dbo->begin() !== false);

        $sql = "SELECT 'ALTER TABLE '||table_schema||'.\"'||TABLE_NAME||'\" ALTER '||COLUMN_NAME||' DROP DEFAULT;
                '||replace('DROP SEQUENCE '''||substring(column_default, 9, length(column_default)-19), '''', '')||';
                ALTER TABLE  '||table_schema||'.\"'||TABLE_NAME||'\" ALTER '||COLUMN_NAME||' ADD GENERATED "
                . "BY DEFAULT AS IDENTITY;
                SELECT SETVAL(pg_get_serial_sequence('''||table_schema||'.'||TABLE_NAME||''', '''||COLUMN_NAME||'''),
                (SELECT COALESCE(MAX('||COLUMN_NAME||'), 0) + 1 FROM '||table_schema||'.'||TABLE_NAME||'), false);'
                FROM information_schema.columns
                WHERE column_default LIKE 'nextval%'";

        foreach ($this->Dbo->query($sql) as $table) {
            $success = $success && ($this->Dbo->query($table[0]['?column?']) !== false);
        }

        if ($success) {
            $success = ($this->Dbo->commit() !== false) && $success;
        } else {
            $success = ($this->Dbo->rollback() !== false) && $success;
        }

        return $success;
    }
}
