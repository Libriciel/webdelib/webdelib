<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Shell', 'Console');
App::uses('ConnectionManager', 'Model');
App::uses('Cache', 'Cache');
App::uses('TenantConsoleOptionParser', 'Lib/Console');

/**
 * Application Shell
 *
 * Add your application-wide methods in the class below, your shells
 * will inherit them.
 *
 * @package app.Console.Command
 */
class AppShell extends Shell
{
    /**
     * Default success code
     * @var int
     */
    public const CODE_SUCCESS = 0;

    /**
     * Should contain a version name
     * @var string
     */
    public $version;
    /**
     * @var int|mixed|Shell|string
     */
    /**
     * @var array|int[]|mixed|Shell
     */
    public $fqdnDataBases;
    /**
     * @var DATABASE_CONFIG|Shell
     */
    public DATABASE_CONFIG $dataBaseConfig;
    private string $fqdnName;
    private string $bddName;

    /**
     * [initialize description]
     * @return [type] [description]
     */
    public function initialize()
    {
        $this->initDbMultiBase();

        parent::initialize();

        // Création de styles perso
        $this->stdout->styles('time', ['text' => 'magenta']);
        $this->stdout->styles('important', ['text' => 'red', 'bold' => true]);

        if (!file_exists(ROOT . DS . 'VERSION.txt')) {
            $this->error('VERSION ' . __('Le fichier VERSION.txt est introuvable'));
        }

        $versionFile = file(ROOT . DS . 'VERSION.txt');
        $this->version = trim(array_pop($versionFile));
    }


    protected function isDispatched()
    {
        $backtrace = Hash::extract(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS), '{n}.function');
        return in_array('dispatchShell', $backtrace, true);
    }

    protected function stop($status = 0)
    {
        if ($this->isDispatched()) {
            if ($status !== static::CODE_SUCCESS) {
                $message = sprintf("", $this->name, $status);
                throw new RuntimeException($message);
            }
            return $status === static::CODE_SUCCESS;
        } else {
            return parent::_stop($status);
        }
    }

    /**
     * @throws JsonException
     */
    public function dispatchShell()
    {
        $args = func_get_args();
        if (is_string($args[0]) && count($args) === 1) {
            $args = $args[0];
        }
        $this->out(sprintf("<info>%s</info>", json_encode($args, JSON_THROW_ON_ERROR)), 1, static::VERBOSE);
        try {
            return parent::dispatchShell($args);
        } catch (Exception $e) {
            throw new CakeException(__('%s %s arguments: %s', $e->getCode(), $e->getMessage(), $args));
        }
    }

    public function startup()
    {
        if (!$this->isDispatched()) {
            $this->_welcome();
        }
    }

    /**
     * Options d'exécution et validation des arguments
     * @return ConsoleOptionParser $parser
     */
    public function getOptionParser()
    {
        return new TenantConsoleOptionParser();
    }

    /**
     * [info description]
     */
    public function info($message, $level = Shell::NORMAL)
    {
        $this->out('<info>' . __('[%s] %s', $this->getFQDNName(), $message) .'</info>', 1, $level);
    }

    /**
     * [info description]
     * @param  [type] $var [description]
     * @return [type]      [description]
     * @version 5.1.0
     */
    public function error($message, $level = Shell::NORMAL)
    {
        $this->out('<error>' . __('[%s] %s', $this->getFQDNName(), $message) .'</error>', 1, $level);
    }

    /**
     * [info description]
     * @param  [type] $var [description]
     * @return [type]      [description]
     * @version 5.1.0
     */
    public function warning($message, $level = Shell::NORMAL)
    {
        $this->out('<warning>' . __('[%s] %s', $this->getFQDNName(), $message) .'</warning>', 1, $level);
    }


    /**
     * Function header
     *
     * Affiche un message entouré de deux barres horizontales
     *
     * @param string $var message
     * @version 5.1
     */
    public function header($var)
    {
        $this->hr();
        $this->out(__('-> %s', $var));
        $this->hr();
    }

    /**
     * Function footer
     *
     * Affiche un message entouré de deux barres horizontales
     *
     * @param string $var message
     * @version 4.2
     */
    public function footer($var, $level = Shell::NORMAL)
    {
        if ($level === Shell::NORMAL) {
            $this->hr();
            $this->out($var);
            $this->hr();
        }
    }

    private function initDbMultiBase()
    {
        // Charger dynamiquement la connexion à la base de données
        try {
            Configure::load('fqdn_databases');
            $this->fqdnDataBases = Configure::read('fqdn_config');
            $tenantName = $this->getArgumentValue('--tenant');
            if (empty($tenantName)) {
                $tenantName = $this->getArgumentValue('-t');
            }
            if ($tenantName!==null) {
                if (!in_array($tenantName, $this->fqdnDataBases, true)) {
                    throw new Exception(
                        __(
                            'Aucun tenant pour le nom: %s',
                            $tenantName
                        )
                    );
                }
                $this->setDbMultiBase($tenantName);
            }
        } catch (Exception $e) {
            // Gérer les erreurs en cas de problème de connexion
            $this->log('Erreur lors du chargement de la base de données : ' . $e->getMessage());
        }
    }

    /**
     * @param $fqdnName
     * @return void
     * @throws Exception
     */
    protected function setDbMultiBase($tenantName, $cache = false): void
    {
        Cache::clear();
        Configure::write('Config.url', $this->getTenantNamebyFQDN($tenantName));
        Configure::write('Config.database', $tenantName);
        Configure::write('Config.tenantName', $tenantName);
        Configure::write('Acl.database', $tenantName);
        Configure::write('App.fullBaseUrl', 'https://' . $this->getFQDNName());
        // Créer une nouvelle connexion
        ConnectionManager::create($tenantName);
        // Récupérer la connexion nouvellement créée
        $connection = ConnectionManager::getDataSource($tenantName);
        $connection->cacheSources = $cache;
        require_once DATA . $tenantName. DS . 'config/webdelib.inc';
        require_once DATA . $tenantName . DS . 'config/formats.inc';
        require_once DATA . $tenantName . DS . 'config/pastell.inc';
        if (!$connection->isConnected()) {
            throw new CakeException("Connexion non réussie à PostgreSQL !");
        }
        if (is_array($this->uses)) {
            foreach ($this->uses as $modelClass) {
                $this->{$modelClass}->useDbConfig = $tenantName;
                $this->{$modelClass}->schemaName = $tenantName;
                $this->{$modelClass}->Behaviors->load('DynamicDbConfig');
            }
        }
    }

    private function getTenantNamebyFQDN($tenantName)
    {
        return array_search($tenantName, $this->getFqdnDataBases(), true);
    }

    public function getFqdnDataBases()
    {
        return array_unique($this->fqdnDataBases);
    }

    public function getFQDNName()
    {
        return Configure::read('Config.url');
    }

    public function getTenantName()
    {
        return  Configure::read('Config.database');
    }

    // Fonction pour extraire la valeur d'un paramètre avec un préfixe spécifique
    private function getArgumentValue($key)
    {
        $args = $_SERVER['argv'];
        foreach ($args as $index => $arg) {
            // Vérifie si l'argument correspond au paramètre recherché
            if ($arg === $key && isset($args[$index + 1])) {
                // Retourne l'argument qui suit le paramètre
                return $args[$index + 1];
            }
        }
        return null; // Retourne null si le paramètre n'est pas trouvé
    }
}
