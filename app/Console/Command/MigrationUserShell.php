<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * Console MigrationUserShell
 *
 * @package app.Console.Command.Shell
 * @version v4.3
 */
class MigrationUserShell extends AppShell
{
    public $uses = ['User', 'Aro', 'Aco', 'Permission'];
    public $tasks = ['Sql'];

    /**
     * Function main
     *
     * VIDE
     *
     * @version 4.3
     */
    public function main()
    {
    }

    /**
     * Function getOptionParser
     *
     * Options d'exécution et validation des arguments
     *
     * @return Parser $parser
     * @version 4.3
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();
        $parser->description(__('Script configuration des utilisateurs.'));

        $parser->addSubcommand('notification', [
            'help' => __('Gestion des notifications des utilisateurs'),
        ]);

        $parser->addSubcommand('typeacte', [
            'help' => __('Gestion des droits sur les types d\'acte des utilisateurs'),
        ]);

        $parser->addSubcommand('typeseance', [
            'help' => __('Gestion des droits sur les types de seance des utilisateurs'),
        ]);

        return $parser;
    }

    /**
     * Function AlerteMail
     *
     * @version 4.3
     */
    public function notification()
    {
        $result = strtolower($this->in(
            "Gestion des notifications de tous les utilisateurs:\n"
            . " [a] Ajouter UNE notification\n [m] Modifier toutes les notifications\n [q] Quitter\n "
            . "Que voulez-vous faire?",
            ['a', 'm', 'q']
        ));
        if ($result === 'a') {
            $this->ajoutAlerteMail();
        } elseif ($result === 'm') {
            $this->modifAlerteMail();
        }
        return $this->stop();
    }

    /**
     * Function modifAlerteMail
     *
     * @version 4.3
     */
    private function modifAlerteMail()
    {
        try {
            $this->User->begin();
            $promp = "Recevoir des notifications (email) pour tous les utilisateurs:\n "
                . "[d] Au détail pour chaque alerte\n "
                . "[n] Non pour toutes les alertes\n "
                . "[o] Oui pour toutes les alertes\n [q] Quitter\n"
                . "Que voulez-vous faire?";
            //Recevoir des alertes par email
            $alerte = strtolower($this->in($promp, ['d', 'n', 'o', 'q'], 'd'));
            if ($alerte === 'q') {
                return $this->stop();
            }
            if ($alerte === 'd') {
                $types = [
                    'mail_insertion' => 'd\'insertion des projets',
                    'mail_refus' => 'des projets refusés',
                    'mail_traitement' => 'des traitements de validation en attente',
                    'mail_modif_projet_cree' => 'des projets modifiées',
                    'mail_modif_projet_valide' => 'des projets visés qui sont modifiés',
                    'mail_retard_validation' => 'des porjets en retard de validation',
                    'mail_error_send_tdt_auto' =>
                        'd\'une erreur lors  de l\'envoi automatique des actes à télétransmettre',
                    'mail_projet_valide' => 'd\'un projet que j\'ai rédigé est validé',
                    'mail_projet_valide_valideur' => 'd\'un projet que j\'ai traité est validé',
                    'mail_majtdtcourrier' => 'd\'un acte télétransmis est en erreur ou a été annulé'
                ];
                foreach ($types as $key => $type) {
                    $promp = "Recevoir des alertes par email $type ($key):\n "
                        . "[o] Oui\n "
                        . "[n] Non\n "
                        . "[q] Quitter\n"
                        . "Que voulez-vous faire?";
                    $result = strtolower($this->in($promp, ['o', 'n', 'q'], 'o'));
                    if ($result === 'q') {
                        return $this->stop();
                    }
                    $data['User.' . $key] = $result === 'o' ? true : false;
                }
                $data['User.accept_notif'] = true;
                // La validation finale n'a lieu que si on arrive au bout du script
                $this->User->updateAll($data);
                $success = true;
            } elseif ($alerte === 'o') {
                $this->User->updateAll([
                    'User.accept_notif' => true,
                    'User.mail_refus' => true,
                    'User.mail_traitement' => true,
                    'User.mail_insertion' => true,
                    'User.mail_modif_projet_cree' => true,
                    'User.mail_modif_projet_valide' => true,
                    'User.mail_majtdtcourrier' => true,
                    'User.mail_projet_valide_valideur' => true,
                    'User.mail_projet_valide' => true,
                    'User.mail_error_send_tdt_auto' => true,
                    'User.mail_retard_validation' => true]);
                $success = true;
            } elseif ($alerte === 'n') {
                $this->User->updateAll([
                    'User.accept_notif' => false,
                    'User.mail_refus' => false,
                    'User.mail_traitement' => false,
                    'User.mail_insertion' => false,
                    'User.mail_modif_projet_cree' => false,
                    'User.mail_modif_projet_valide' => false,
                    'User.mail_majtdtcourrier' => false,
                    'User.mail_projet_valide_valideur' => false,
                    'User.mail_projet_valide' => false,
                    'User.mail_error_send_tdt_auto' => false,
                    'User.mail_retard_validation' => false]);
                $success = true;
            }

            $this->User->commit();
        } catch (Exception $e) {
            $success = false;
            $this->User->rollback();
            $this->out("ERREUR : " . $e->getMessage());
        }

        if (empty($success)) {
            $this->footer('<error>Erreur : un problème est survenu durant la modification !!</error>');
        } else {
            $this->footer('<important>modification accomplis avec succès !</important>');
        }
    }

    /**
     * Function ajoutAlerteMail
     *
     * @version 4.3
     */
    private function ajoutAlerteMail()
    {
        try {
            $this->User->begin();

            $promp = "Liste des codes de la notification à ajouter.\n "
                . "[0] Recevoir les notifications\n "
                . "[1] Un projet est inséré dans un circuit dont je suis valideur\n "
                . "[2] Un projet est à traiter dans ma bannette\n "
                . "[3] Un projet que j'ai créé ou validé a été refusé \n "
                . "[4] Un projet que j'ai créé est modifié\n "
                . "[5] Un projet que j'ai visé est modifié\n "
                . "[6] Un projet que je dois traiter est en retard\n "
                . "[7] Un projet que j'ai rédigé est validé\n "
                . "[8] Un projet que j'ai traité est validé\n "
                . "[9] Erreur lors  de l'envoi automatique des actes à télétransmettre\n "
                . "[10] Un acte télétransmis est en erreur ou a été annulé\n "
                . "[q] Quitter\nQuel type de notifcation voulez-vous traiter?";
            //Recevoir des alertes par email
            $result = strtolower($this->in($promp, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 'q'], 'q'));
            if ($result === 'q') {
                return $this->stop();
            }
            switch ($result) {
                case '1':
                    $data = ['User.mail_insertion' => true];
                    break;
                case '2':
                    $data = ['User.mail_traitement' => true];
                    break;
                case '3':
                    $data = ['User.mail_refus' => true];
                    break;
                case '4':
                    $data = ['User.mail_modif_projet_cree' => true];
                    break;
                case '5':
                    $data = ['User.mail_modif_projet_valide' => true];
                    break;
                case '6':
                    $data = ['User.mail_retard_validation' => true];
                    break;
                case '7':
                    $data = ['User.mail_projet_valide' => true];
                    break;
                case '8':
                    $data = ['User.mail_projet_valide_valideur' => true];
                    break;
                case '9':
                    $data = ['User.mail_error_send_tdt_auto' => true];
                    break;
                case '10':
                    $data = ['User.mail_majtdtcourrier' => true];
                    break;
                case '00':
                    $data = ['User.accept_notif' => true];
                    break;

                default:
                    throw new Exception('Code invalide');
            }

            $this->User->updateAll($data);
            $success = true;
            $this->User->commit();
        } catch (Exception $e) {
            $success = false;
            $this->User->rollback();
            $this->out("ERREUR : " . $e->getMessage());
        }

        if (empty($success)) {
            $this->footer('<error>Erreur : un problème est survenu durant l\'ajout !!</error>');
        } else {
            $this->footer('<info>Ajout accomplis avec succès !</info>');
        }
    }

    /**
     * Function typeacte
     *
     * @version 5.1.0
     * @since 4.3
     */
    public function typeacte()
    {
        $result = strtolower($this->in(
            "Gestion des droits sur les types d\'acte de tous les utilisateurs.\n"
                . " [a] Ajouter un type d'acte sur tous les utilisateurs\n "
            . "[s] Supprimer un type d'acte sur tous les utilisateurs\n [q] Quitter\nQue voulez-vous faire?",
            ['a', 's', 'q']
        ));
        if ($result === 'a') {
            $this->updateAcl(1, 'Typeacte', __('type d\'acte'));
        } elseif ($result === 's') {
            $this->updateAcl(-1, 'Typeacte', __('type d\'acte'));
        }
        return $this->stop();
    }

    /**
     * Function typeseance
     *
     * @version 5.1.0
     */
    public function typeseance()
    {
        $result = strtolower($this->in("Gestion des droits sur les types de séance de tous les utilisateurs.\n"
                . " [a] Ajouter un type de séance sur tous les utilisateurs\n [s] Supprimer un type de séance sur "
                ."tous les utilisateurs\n [q] Quitter\nQue voulez-vous faire?", ['a', 's', 'q']));
        if ($result === 'a') {
            $this->updateAcl(1, 'Typeseance', __('type de séance'));
        } elseif ($result === 's') {
            $this->updateAcl(-1, 'Typeseance', __('type de séance'));
        }
        return $this->stop();
    }

    /**
     * Function _AjoutAcl
     *
     * @version 5.1.0
     * @since 4.3
     */
    private function updateAcl($access, $modelName, $name)
    {
        $success = false;
        try {
            App::uses($modelName, 'Model');
            $model = ClassRegistry::init($modelName);

            $this->Permission->begin();
            $records = $model->find(
                'all',
                [
                'fields' => ['id', $model->displayField],
                'recursive' => -1]
            );
            $promp = __(
                'Liste des %s à %s :',
                $name,
                $access === -1 ? __('supprimer') : __('ajouter')
            ) . "\n ";

            foreach ($records as $record) {
                $promp .= '[' . $record[$modelName]['id'] . '] ' . $record[$modelName][$model->displayField] . "\n ";
                $options[] = $record[$modelName]['id'];
            }

            $promp .= __('[q] Quitter')
                . "\n"
                . __(
                    'Quel %s voulez-vous %s ?',
                    $name,
                    $access === -1 ? __('supprimer') : __('ajouter')
                )
                . "\n ";
            $options[] = 'q';
            $add_id = strtolower($this->in($promp, $options));

            if ($add_id === 'q') {
                return $this->stop();
            }

            $promp = __('Liste des droits disponibles ?') . "\n ";
            $promp .= __('[c] Création') . "\n ";
            $promp .= __('[r] Lecture') . "\n ";
            $promp .= __('[u] Mise à jour') . "\n ";
            $promp .= __('[d] Suppression') . "\n ";
            $promp .= __('[q] Quitter')
                ."\n"
                . __(
                    'Quels types de droits souhaitez vous %s ?',
                    $access === -1 ? __('supprimer') : __('ajouter')
                )
                . "\n";
            $crud_selected = strtolower($this->in($promp, [
              'c','r', 'u', 'd','crud','cru','cr','rud','ru','crd','rd','rud','ud','cud','cd', 'q']));

            if ($crud_selected === 'q') {
                return $this->stop();
            }
            $crud = [];
            foreach (str_split($crud_selected) as $selected) {
                if ($selected === 'c') {
                    array_push($crud, 'create');
                }
                if ($selected === 'r') {
                    array_push($crud, 'read');
                }
                if ($selected === 'u') {
                    array_push($crud, 'update');
                }
                if ($selected === 'd') {
                    array_push($crud, 'delete');
                }
            }

            $aros = $this->Aro->find('all', [
                'fields' => ['id', 'foreign_key'],
                'conditions' => ['model' => 'User'],
                'recursive' => -1]);

            foreach ($aros as $aro) {
                $this->Permission->allow(
                    ['User' => ['id' => $aro['Aro']['foreign_key']]],
                    ["$modelName" => ['id' => $add_id]],
                    $crud,
                    $access
                );
            }

            $this->Permission->commit();
            $success = true;
        } catch (Exception $e) {
            $this->Permission->rollback();
            $this->error($e->getMessage());
        }

        if (!empty($success)) {
            $this->footer(__('%s effectué avec succès !', $access === -1 ? __('Suppression') : __('Ajout')));
        }
    }
}
