<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Console MaintenanceShell
 *
 * @package app.Console.Command.Shell
 * @version 5.1.0
 * @since 4.2.0
 */
class MaintenanceShell extends AppShell
{
    public $tasks = [
        'Acl',
        'AnnexeTools',
        'Gedooo',
        'InfosupTools',
        'Sql',
        'Tdt'
    ];

    public $uses = ['Annexe', 'Deliberation', 'Seance', 'Cron', 'Infosup'];

    /**
     * Function main
     *
     * VIDE
     *
     * @version 4.2
     */
    public function main()
    {
//        $this->out('Script de patch de webdelib');
    }

    /**
     * Function getOptionParser
     *
     * Options d'exécution et validation des arguments
     *
     * @return Parser $parser
     * @version 4.2
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        $parser->addSubcommand('seance', [
            'help' => __('Test dans l\'outil de fusion les documents d\'acte des séances en cours.'),
            'parser' => [
                'options' => [
                    'id' => [
                        'name' => 'id',
                        'required' => false,
                        'short' => 'i',
                        'help' => 'Test les documents d\'une séance donnée par son id.'
                    ],
                    'group' => [
                        'name' => 'group',
                        'required' => false,
                        'short' => 'g',
                        'help' => 'Test les documents d\'un ensemble de séances.',
                        'choices' => ['all', 'nonaffectees', 'nontraitees'],
//                        'default' => 'all'
                    ]
                ]
            ]
        ])
        ->addSubcommand('convertDataToFusion', [
            'help' => __('Conversion des données (fichier et texte) pour la fusion documentaire '
                . 'des seances, actes ou projets.'),
            'parser' => [
                'arguments' => [
                    'model' => [
                        'name' => 'model',
                        'required' => true,
                        'short' => 'm',
                        'help' => 'Donnée de fusion à convertir (Seance ou Deliberation).'
                    ],
                    'id' => [
                        'name' => 'id',
                        'required' => true,
                        'short' => 'i',
                        'help' => 'Conversion des données (fichier et texte) d\'une seance, '
                            . 'd\'un acte ou projets par l\'identifiant.'
                    ],
                    'all' => [
                        'name' => 'all',
                        'required' => false,
                        'short' => 'a',
                        'help' => 'Conversion des données (fichier et texte) de toute les seances et '
                            . 'les projets de la base de donnée.',
                    ],
                    'force' => [
                        'name' => 'force',
                        'boolean' => true,
                        'required' => false,
                        'short' => 'f',
                        'help' => __('Force la conversion des données')
                    ],
                ]
            ]
        ])
        ->addSubcommand('recupDataMessageTdt', [
            'help' => __('Récupération des documents des messages Tdt.'),
            'parser' => [
                'arguments' => [
                    'all' => [
                        'name' => 'all',
                        'required' => false,
                        'short' => 'a',
                        'help' => 'Récupération de tous les documents des messages Tdt.',
                    ]
                ]
            ]
        ])
        ->addSubcommand('recupDataTdt', [
            'help' => __('Récupération des documents du Tdt.'),
            'parser' => [
                'arguments' => [
                    'id' => [
                        'name' => 'id',
                        'required' => true,
                        'short' => 'i',
                        'help' => 'Récupération des documents d\'un acte par l\'identifiant.'
                    ],
                    'force' => [
                        'name' => 'force',
                        'boolean' => true,
                        'required' => false,
                        'short' => 'f',
                        'help' => __('Force la récupération des données')
                    ],
                ]
            ]
        ])
        ->addSubcommand('recupStatutTdt', [
            'help' => __('Récupération du statut TDT d\'un acte.'),
            'parser' => [
                'arguments' => [
                    'id' => [
                        'name' => 'id',
                        'required' => true,
                        'short' => 'i',
                        'help' => 'Récupération du statut "annulé" d\'un acte par l\'identifiant.'
                    ],
                ]
            ]
        ])
        ->addSubcommand('resetAcl', [
            'help' => __('Remise à zéro des acl')
        ])
        ->addSubcommand('order_acl', [
            'help' => __('Ordonner les acl')
        ])
        ->addSubcommand('update_acl', [
            'help' => __('Mise à jour des acl')
        ])
        ->addSubcommand('order_annexes', [
            'help' => __('Ordonner les annexes')
        ])
        ->addSubcommand('order_model', [
            'help' => __('Ordonner un modele'),
            'parser' => [
                'arguments' => [
                    'modelName' => [
                        'name' => 'model',
                        'required' => false,
                        'short' => 'm',
                        'help' => __('Ordonner un modele'),
                    ]
                ]
            ]
        ])
        ->addSubcommand('reset_mov', [
            'help' => __('Remise à zéro des variables de modèle')
        ]);

        $parser->addOption(
            'no-question',
            [
                'boolean' => true,
                'help' => __('Pas de question avec saisie au clavier.'),
            ]
        );


        return $parser;
    }

    public function convertDataToFusion()
    {
        $time_start = microtime(true);
        try {
            if (isset($this->args[2]) && $this->args[2]==='all') {
                $this->conversionAnnexe(null, $this->args[3]);
                $this->conversionInfosup('Seance', null, $this->args[3]);
                $this->conversionInfosup('Deliberation', null, $this->args[3]);
                $this->conversionDataTdt('Deliberation', null, $this->args[3]);
                $this->conversionDataTdt('Annexe', null, $this->args[3]);
            } else {
                if (empty($this->args[1])) {
                    throw new Exception('Id manquant');
                }
                if (empty($this->args[0])) {
                    throw new Exception('Model manquant');
                }

                if ($this->args[0] === 'Seance') {
                    $this->conversionInfosup('Seance', !empty($this->args[1])
                        ? $this->args[1] : null);
                } elseif ($this->args[0] === 'Deliberation') {
                    $force = false;
                    if (isset($this->args[3])) {
                        $force = true;
                    }
                    $this->conversionAnnexe(!empty($this->args[1])
                        ? $this->args[1] : null, $force);
                    $this->conversionInfosup('Deliberation', !empty($this->args[1])
                        ? $this->args[1] : null);
                    $this->conversionDataTdt('Deliberation', !empty($this->args[1])
                        ? $this->args[1] : null, $force);
                    $this->conversionDataTdt('Annexe', !empty($this->args[1])
                        ? $this->args[1] : null, $force);
                } else {
                    throw new Exception('Model inconnu');
                }
            }
        } catch (Exception $e) {
            $this->error(__FUNCTION__ . ': ' . $e->getMessage());
        }

        $time_end = microtime(true);
        $this->out(
            '<info>'
            . __(
                'Temps pour la conversion : %s secondes',
                round($time_end - $time_start)
            )
            .'</info>',
            1,
            Shell::VERBOSE
        );
    }

    /**
    * Function conversionAnnexe
    *
    * Vérifie la compatibilité des textes en base avec Gedooo,
    * - textes de projets valides ?
    * - annexes en odt valide ?
    *
    * @version 5.1.0
    * @since 4.4.0
    * @param  [type] $id [description]
    * @return [type]     [description]
    */
    private function conversionAnnexe($id = null, $force = false)
    {
        if (!empty($id)) {
            $this->info(__("[%s] Délibération (identifiant : %s)", __FUNCTION__, $id));
            try {
                $this->AnnexeTools->conversion($id, $force);
            } catch (Exception $e) {
                $this->error(__('[%s] Conversion non effectuée (identifiant: %s).', __FUNCTION__, $id));
            }
        } else {
            $annexes = $this->Annexe->find('all', [
                'fields' => ['DISTINCT foreign_key', 'filetype'],
                'conditions' => ['OR' => ['joindre_fusion' => true, 'joindre_ctrl_legalite' => true]],
                'order' => ['foreign_key' => 'ASC'],
                'recursive' => -1
            ]);
            $i = 0;
            foreach ($annexes as $annexe) {
                $i++;
                $this->info(
                    __(
                        '[%s] Conversion Projet n°%s (%s/%s)...',
                        __FUNCTION__,
                        $annexe['Annexe']['foreign_key'],
                        $i,
                        count($annexes)
                    )
                );
                $return = $this->AnnexeTools->conversion($annexe['Annexe']['foreign_key'], $force);
                $this->info($return);
                $this->info(__('[%s] Conversion terminée id: ', __FUNCTION__, $annexe['Annexe']['foreign_key']));
            }
            $this->info(__('[%s] Conversion des annexes terminée => %s annexes converties', __FUNCTION__, $i));
        }
    }

    /**
     * Function conversionAnnexe
     *
     * Vérifie la compatibilité des textes en base avec Gedooo,
     * - textes de projets valides ?
     * - annexes en odt valide ?
     * @version 5.1.0
     * @since 4.4.0
     */
    private function conversionInfosup($model, $id = null, $force = false)
    {
        $conditions = ['Infosup.model' => $model];
        if (!empty($id)) {
            $this->out(
                '<info>'
                . __('%s identifiant : %s', $model, $id)
                . '</info>',
                1,
                Shell::VERBOSE
            );
            $conditions['foreign_key'] = $id;
        }

        $conditions_model = [];
        $infosups = $this->Infosup->find('all', [
            'fields' => ['Infosup.id', 'file_type', 'Infosupdef.code', 'Infosupdef.type'],
            'joins' => [
                $this->Infosup->join('Infosupdef', [
                    'type' => 'INNER',
                    'conditions' => [
                        'type' => ['richText', 'file'],
                        'joindre_fusion' => true
                    ]
                ]),
                $this->Infosup->join($model, ['type' => 'INNER'])
            ],
            'conditions' => $conditions,
            'order' => 'foreign_key ASC',
            'recursive' => -1
        ]);

        $i = 0;
        if (!empty($id)) {
            $this->info(
                __(
                    'Conversion des informations supplémentaires du modèle "%s" n°%s (%s/%s)...',
                    $model,
                    $id,
                    $i,
                    count($infosups)
                )
            );
        } else {
            $this->info(
                __(
                    'Conversion des informations supplémentaires du modèle "%s" (%s/%s)...',
                    $model,
                    $i,
                    count($infosups)
                )
            );
        }
        foreach ($infosups as $infosup) {
            $i++;
            try {
                $this->InfosupTools->conversion($model, $infosup['Infosup']['id'], $force);
            } catch (Exception $e) {
                $this->error(
                    __(
                        '[%s] Sauvegarde non effectuée (identifiant: %s, type: %s, code : %s).',
                        __FUNCTION__,
                        $infosup['Infosup']['id'],
                        $infosup['Infosupdef']['type'],
                        $infosup['Infosupdef']['code']
                    )
                );
            }
            $this->out(
                __(
                    'Sauvegarde terminée (type: %s, code : %s).',
                    $infosup['Infosupdef']['type'],
                    $infosup['Infosupdef']['code']
                )
            );
        }
        if (!empty($id)) {
            $this->out(
                '<info>'
                . __(
                    'Conversion des informations supplémentaires du modèle "%s" pour l\'identifiant n°%s terminée '
                    . '=> %s informations supplémentaires converties.',
                    $model,
                    $id,
                    $i
                ) . '</info>',
                1,
                Shell::VERBOSE
            );
        } else {
            $this->out(
                '<info>'
                . __(
                    'Conversion des informations supplémentaires du modèle "%s" terminée '
                    . '=> %s informations supplémentaires converties.',
                    $model,
                    $i
                )
                . '</info>',
                1,
                Shell::VERBOSE
            );
        }
    }

    /**
         * Function recupDataTdt
         *
         * Met à jour des documents du TDT
         *
         * @version 5.2
         * @since 5.0.1
         */
    public function recupDataTdt()
    {
        try {
            $acteId = null;

            if (!empty($this->params['id'])) {
                $acteId = $this->params['id'];
            }

            $promp = "Récupération des documents du TDT.\n "
                    . "Veuillez indiquer depuis combien de jour la récupération doit-elle s'effectuer "
                    . "(exemple : 100 pour 100 jours) : ";
            $nbr_jours = strtolower($this->in($promp));

            if (!is_numeric($nbr_jours)) {
                throw new Exception(__('Ce n\'est pas un chiffre !'));
            }
            $rapport = $this->Tdt->updateDateAr();
            $this->out("<info>" . $rapport. "</info>");
            $success = $this->Deliberation->updateTdtFiles($acteId, $nbr_jours);
        } catch (Exception $e) {
            $success = false;
            $this->out("<error>" . $e->getMessage() . "</error>");
        }

        if (empty($success)) {
            $this->footer('<warning>Erreur : Récupération non disponible !</warning>');
        } else {
            $this->footer('<important>Récupération terminée !</important>');
        }
    }

    /**
     * Function recupStatutTdt
     *
     * Met à jour le statut d'un acte envoyé au tdt
     *
     * @version 5.2
     */
    public function recupStatutTdt()
    {
        try {
            $delib_id = $this->params['id'];

            if (empty($this->params['id'])) {
                throw new Exception('Id null ou manquant');
            }

            $success = $this->Deliberation->updateTdtStatus($delib_id, null, false);
        } catch (Exception $e) {
            $success = false;
            $this->out("<error>" . $e->getMessage() . "</error>", 1, Shell::VERBOSE);
        }

        if (empty($success)) {
            $this->footer('<warning>Erreur : Récupération non disponible !</warning>');
        } else {
            $this->footer('<important>Récupération terminée !</important>');
        }
    }

    /**
     * Function de remise à zero des permissions utilisateurs
     * @since 4.3
     * @version 5.1
     *
     * @return type
     */
    public function resetAcl()
    {
        try {
            if (!$this->param('quiet')) {
                $promp = "Remise à zero des permissions utilisateurs.\n "
                        . "[o] Oui\n "
                        . "[n] Non\n "
                        . "Que voulez-vous faire?";
                $alerte = strtolower($this->in($promp, ['o', 'n', 't', 'q'], 'o'));
                if ($alerte === 'q') {
                    return $this->stop();
                }
                if ($alerte === 'n') {
                    return $this->stop();
                }
            }
            $this->out("\nModification de la base de données...");

            $sql_files = [];
            $sql_files['Web-delib patch reset acl'] =
                APP . 'Config' . DS . 'Schema' . DS . 'sql' . DS . 'reset_acl.sql';

            $success = $this->Sql->apply($sql_files);
            if ($success == true) {
                $this->Sql->commit();
            }

            $this->Acl->creationAcl();
        } catch (Exception $e) {
            $success = false;
            $this->Sql->rollback();
            $this->out("ERREUR : " . $e->getMessage());
        }

        if (empty($success)) {
            $this->footer('<error>Erreur : un problème est survenu durant la modification !!</error>');
        } else {
            $this->footer('<important>modfication accomplis avec succès !</important>');
        }
    }

    /**
     * Function de remise à zero des variables de modèle du plugin "ModelOdtValidator"
     * @since 4.3
     * @version 5.0.1
     *
     * @return type
     */
    public function reset_mov() // phpcs:ignore
    {
        try {
            $alerte = '';
            if ($this->params['quiet']!==true) {
                $promp = "Remise à zero des variables de modèle.\n "
                        . "[o] Oui\n "
                        . "[n] Non\n "
                        . "Que voulez-vous faire?";
                $alerte = strtolower($this->in($promp, ['o', 'n', 't', 'q'], 'o'));
                if ($alerte === 'q') {
                    return $this->stop();
                }
                if ($alerte === 'n') {
                    return $this->stop();
                }
            }
            if ($alerte === 'o' || $this->params['quiet']) {
                $this->out("\nModification de la base de données...");
                $sql_files = [];
                //ModelOdtValidator
                $sql_files['Plugin : ModelOdtValidator v1.1.2 drop'] =
                    ROOT . DS . 'plugins' . DS . 'ModelOdtValidator' . DS . 'Config' . DS
                    . 'Schema' . DS. 'sql' . DS . 'drop.sql';
                $sql_files['Plugin : ModelOdtValidator v1.1.2 install'] =
                    ROOT . DS . 'plugins' . DS . 'ModelOdtValidator' . DS . 'Config' . DS
                    . 'Schema' . DS. 'sql' . DS .'create.sql';
                $sql_files['Plugin : ModelOdtValidator install_data webdelib'] =
                    APP . 'Config' . DS . 'Schema' . DS. 'sql' . DS .'model_odt_validator_install.sql';

                $this->Sql->begin();
                $success = $this->Sql->apply($sql_files);
                if ($success == true) {
                    $this->Sql->commit();
                }
            }
        } catch (Exception $e) {
            $success = false;
            $this->Sql->rollback();
            $this->out("Erreur : " . $e->getMessage());
        }

        if (empty($success)) {
            $this->error('Modifications Annulées: ' . __('Une erreur est survenue durant les modifications !'));
        } else {
            $this->footer('<warning>'.__('Modfications accomplis avec succès !').'</warning>');
        }
    }

    /**
     * Function order_acl
     *
     * @version 4.3
     */
    public function order_acl() // phpcs:ignore
    {
        try {
            if (!$this->param('no-question')) {
                $promp = "Ordonner les Acl.\n "
                    . "[o] Oui\n "
                    . "[n] Non\n "
                    . "Que voulez-vous faire?";
                $alerte = strtolower($this->in($promp, ['o', 'n', 't', 'q'], 'o'));
            } else {
                $alerte = 'o';
            }
            if ($alerte === 'q') {
                return $this->stop();
            }
            if ($alerte === 'n') {
                return $this->stop();
            }
            if ($alerte === 'o') {
                $this->info(__('Ordonnancement des acl en cours...'));
                $success = true;
                $this->Acl->orderAcl();
            }
        } catch (Exception $e) {
            $success = false;
            $this->out("ERREUR : " . $e->getMessage());
        }

        if (empty($success)) {
            $this->footer('<error>Erreur : un problème est survenu durant la modification !!</error>');
        } else {
            $this->footer('<important>modfication accomplis avec succès !</important>');
        }
    }

    /**
     * Fonction update_acl
     *
     * @version 4.3
     */
    public function update_acl() // phpcs:ignore
    {
        try {
            if (!$this->param('no-question')) {
                $promp = "Mettre à jour les Acl.\n "
                    . "[o] Oui\n "
                    . "[n] Non\n "
                    . "Que voulez-vous faire?";
                $alerte = strtolower($this->in($promp, ['o', 'n', 't', 'q'], 'o'));
            } else {
                $alerte = 'o';
            }
            if ($alerte === 'q') {
                return $this->stop();
            }
            if ($alerte === 'n') {
                return $this->stop();
            }
            if ($alerte === 'o') {
                $this->info(__('Mise à jour des nouveaux droits'));
                $success = true;
                $this->Acl->updateAcl();
            }
        } catch (Exception $e) {
            $success = false;
            $this->out("ERREUR : " . $e->getMessage());
        }

        if (empty($success)) {
            $this->footer('<error>Erreur : un problème est survenu durant la modification !!</error>');
        } else {
            $this->footer('<important>modfication accomplis avec succès !</important>');
        }
    }

    /**
     * Function order_acl
     * @version 5.0.1
     * @since 5.1.0
     */
    public function order_annexes() // phpcs:ignore
    {
        try {
            $promp = "Ordonner les annexes de projet.\n "
                    . "[o] Oui\n "
                    . "[n] Non\n "
                    . "Que voulez-vous faire?";
            $alerte = strtolower($this->in($promp, ['o', 'n', 't', 'q'], 'o'));
            if ($alerte === 'q') {
                return $this->stop();
            }
            if ($alerte === 'n') {
                return $this->stop();
            }
            if ($alerte === 'o') {
                $this->out("\n" . __('Modification de la base de données...'));
                $success = true;
                $this->AnnexeTools->orderAnnexes();
            }
        } catch (Exception $e) {
            $success = false;
            $this->out("ERREUR : " . $e->getMessage());
        }

        if (empty($success)) {
            $this->footer(
                '<error>'
                . __('Erreur : un problème est survenu durant la modification !!')
                . '</error>'
            );
        } else {
            $this->footer(
                '<info>'
                . __('Modification accomplis avec succès !')
                . '</info>'
            );
        }
    }

    /**
     * Function order_model
     *
     * @version 5.1.0
     */
    public function order_model() // phpcs:ignore
    {
        $success = false;
        try {
            $modelName = ucfirst($this->params['modelName']);
            App::uses($modelName, 'Model');
            $model = ClassRegistry::init($modelName);

            if (!array_key_exists('Tree', $model->actsAs)) {
                throw new Exception(__('Modèle non compatible'));
            }

            $alerte = '';
            if ($this->params['quiet']!==true) {
                $promp = "Ordonner les informations du modèle : \"" . $model->name . "\".\n "
                      . "[o] Oui\n "
                      . "[n] Non\n "
                      . "Que voulez-vous faire?";
                $alerte = strtolower($this->in($promp, ['o', 'n', 't', 'q'], 'o'));
                if ($alerte === 'q') {
                    return $this->stop();
                }
                if ($alerte === 'n') {
                    return $this->stop();
                }
            }
            if ($alerte === 'o' || $this->params['quiet']) {
                $this->out("\n".__('Modification de la base de données...'));
                $success = true;
                $model->recover();
            }
        } catch (Exception $e) {
            $this->error($e->getMessage());
        }

        $this->info(__('Modifications effectuées avec succès !'));
    }

    /**
     * Function conversionDataTdt
     * @version 5.1.0
     * @param  string $model
     * @param  integer $id
     */
    private function conversionDataTdt($model, $id = null, $force = false)
    {
        $conditions = ['tdt_data_edition' => null];

        if (!empty($id)) {
            $this->out(
                '<info>'
                . __(
                    '[%s] Conversion des données "%s" (identifiant : %s)',
                    __FUNCTION__,
                    $model,
                    $id
                )
                . '</info>',
                1,
                Shell::VERBOSE
            );
            if ($model == 'Deliberation') {
                $conditions['id'] = $id;
            }
            if ($model == 'Annexe') {
                $conditions['foreign_key'] = $id;
            }
        }

        $datas = $this->{$model}->find('all', [
            'fields' => ['id'],
            'conditions' => $conditions,
            'order' => 'modified ASC',
            'recursive' => -1
        ]);

        if (count($datas)>0) {
            $i = 0;
            if (!empty($id)) {
                $this->out(
                    '<info>'
                    . __(
                        '[%s] Conversion des données TDT "%s" n°%s (%s/%s)...',
                        __FUNCTION__,
                        $model,
                        $id,
                        $i,
                        count($datas)
                    )
                    . '</info>',
                    1,
                    Shell::VERBOSE
                );
            } else {
                $this->out(
                    '<info>'
                    . __(
                        '[%s] Conversion des données TDT du modèle "%s" (%s/%s)...',
                        __FUNCTION__,
                        $model,
                        $i,
                        count($datas)
                    )
                    . '</info>',
                    1,
                    Shell::VERBOSE
                );
            }
            foreach ($datas as $data) {
                $i++;
                try {
                    $this->Tdt->conversion($model, $data[$model]['id'], $force);
                } catch (Exception $e) {
                    $this->error(
                        __(
                            '[%s] Conversion non effectuée (identifiant: %s).',
                            __FUNCTION__,
                            $data[$model]['id']
                        )
                    );
                }
            }
            if (!empty($id)) {
                $this->out(
                    '<info>'
                    . __(
                        '[%s] Conversion des données TDT du modèle "%s" pour l\'identifiant n°%s terminée '.
                        '=> %s data converties.',
                        __FUNCTION__,
                        $model,
                        $id,
                        $i
                    ) . '</info>',
                    1,
                    Shell::VERBOSE
                );
            } else {
                $this->out(
                    '<info>'
                    . __(
                        '[%s] Conversion des données TDT du modèle "%s" terminée => %s data converties.',
                        __FUNCTION__,
                        $model,
                        $i
                    )
                    . '</info>',
                    1,
                    Shell::VERBOSE
                );
            }
        } else {
            $this->out(
                '<info>'
                . __(
                    '[%s] 0 Conversion des données TDT à effectuer pour le modèle "%s"',
                    __FUNCTION__,
                    $model
                )
                . '</info>',
                1,
                Shell::VERBOSE
            );
        }
    }
}
