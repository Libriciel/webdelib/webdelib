<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\NullOutput;
use Phinx\Console\PhinxApplication;

App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('AppTools', 'Lib');

/**
 * Console PatchShell
 *
 * @package app.Console.Command.Shell
 * @version 7.0.3
 * @since   4.2.0
 */
class PatchShell extends AppShell
{
    /**
     * [public tasks]
     * @var array
     */
    public $tasks = [
      'Acl',
      'Sql',
      'AnnexeTools'
    ];
    /**
     * [public uses]
     * @var array
     */
    public $uses = [
        'Version',
        'Typeacte',
        'Nature',
    ];

    /**
     * Contains a paths
     * @var [type]
     */
    private $patchPaths = [];

    private $commandPatchs = [
        'version703to800' => [
            'start' => '7.0.3',
            'fin' => '8.0.0'
        ],

    ];

    private PhinxApplication $appPhinx;

    /**
     * [initialize description]
     * @return [type] [description]
     *
     * @version 6.0.0
     */
    public function initialize()
    {
        parent::initialize();

        $this->commandPatchs += $this->caculateListPatch();

        $this->patchPaths = [
            'app' => APP . 'Config' . DS . 'Schema' . DS . 'patchs',
            'cakeflow' =>
                CakePlugin::path('Cakeflow') . 'Config' . DS . 'Schema' . DS . 'sql' . DS . 'patchs',
            'ldap_manager' =>
                CakePlugin::path('LdapManager') . 'Config' . DS . 'Schema' . DS . 'patchs',
            'model_odt_validator' =>
                CakePlugin::path('ModelOdtValidator') . 'Config' . DS . 'Schema'. DS . 'sql' . DS . 'patchs'
        ];

        $this->appPhinx = new PhinxApplication();
        $this->appPhinx->setAutoExit(false);
    }

    /**
     * Function main
     *
     * @version 6.0.3
     * @since   4.2.0
     */
    public function main()
    {
        $this->out('Script de patch de webdelib');
        $aide = __('Tapez "Console/cake patch -h" pour afficher l\'aide.');

        $command = 'version' . str_replace(['.','_'], ['',''], $this->command);

        if (array_key_exists($command, $this->commandPatchs)) {
            $commandPatchs = $this->caculateLastPatch($command);
            array_push($commandPatchs, $command);

            foreach ($commandPatchs as $commandPatch) {
                if ($this->patch($commandPatch) !== true) {
                    $this->stop(static::CODE_ERROR);
                }
            }

            $this->stop(static::CODE_SUCCESS);
        }

        $this->error(
            __(
                "La commande '%s' est inconnue, pour avoir la liste des noms de patch attendus.",
                $command
            )
            . "\n" . $aide
        );
        $this->stop(static::CODE_ERROR);
    }

    /**
     * Function getOptionParser
     * Options d'exécution et validation des arguments
     *
     * @return ConsoleOptionParser $parser
     *
     * @version 6.0.3
     * @since 4.3.1
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        $parser->description(__('Commandes de mise à jour de webdelib.'));

        foreach ($this->commandPatchs as $commandPatch) {
            $parser->addSubcommand(
                $commandPatch['start'] . '_to_' . $commandPatch['fin'],
                [
                    'help' => __(
                        'Patch de mise à jour de v%s vers v%s.',
                        $commandPatch['start'],
                        $commandPatch['fin']
                    ),
                ]
            );
        }

        $parser->addSubcommand(
            'install',
            [
                'short' => 'i',
                'help' => __('Patchs pour l\'installation de l\'application.'),
            ]
        );

        $parser->addSubcommand(
            'version',
            [
                'help' => __('Version de l\'application.'),
            ]
        );

        $parser->addOption(
            'no-question',
            [
                'boolean' => true,
                'help' => __('Pas d\'avertissement.'),
            ]
        );

        return $parser;
    }
    private function version703to800()
    {
        $this->info(__('Migration des alias'));

        //Migration des controleurs
        $this->Acl->migrateNameControlerAcl('Projets', 'Deliberations');
        $this->Acl->migrateNameControlerAcl('sendDeliberationsToSignature', 'sendToParapheur');
        $this->Acl->migrateNameControlerAcl('sendAutresActesToSignature', 'sendActesToSignature');
        $this->Acl->migrateNameControlerAcl('sendToGedAutreActe', 'ExportGedAutresActes');
        $this->Acl->migrateNameControlerAcl('sendSeanceVotesToIdelibre', 'sendToIdelibre');
        $this->Acl->migrateNameControlerAcl('sendToSaeDeliberation', 'sendToSae');

        $this->Acl->createControlerAcl('Preferences', [
            'changeUserMdp',
            'changeFormatSortie',
        ]);

        $this->Acl->createControlerAcl('Deliberations', [
            'tousLesProjetsSansSeance',
            'tousLesProjetsValidation',
            'tousLesProjetsAFaireVoter',
        ]);

        $this->Acl->createControlerAcl('AutresActes', [
            'nonTransmis',
            'autresActesAValider',
            'autresActesValides',
            'autresActesAEnvoyer',
            'autresActesEnvoyes',
            'autresActesAbandon',
        ]);


        $this->Acl->createControlerAcl('Teletransmettre', [
            'deliberationCancelSendToTDT',
            'autresActesCancelSendToTDT',
        ]);

        $this->Acl->createControlerAcl('Search', [
            'tousLesProjetsRecherche',
        ]);

        $this->Acl->createControlerAcl('Validations', [
            'reattribuerTous',
            'goNext',
            'retour',
            'rebond',
            'validerEnUrgence',
        ]);

        $this->Acl->createControlerAcl('Signatures', [
            'sendAutresActesToSignature',
            'sendDeliberationsToSignature',
            'deliberationCancelSignature',
            'autresActesCancelSignature',
        ]);

        $this->Acl->createControlerAcl('Votes', [
            'sendSeanceVotesToIdelibre',
        ]);

        $this->Acl->createControlerAcl('Publier', [
            'sendToPublishDeliberation',
            'sendToPublish',
        ]);

        $this->Acl->createControlerAcl('ExportGed', [
            'sendToGedAutreActe',
        ]);

        $this->Acl->createControlerAcl('VerserSae', [
            'sendToSaeDeliberation',
        ]);

        $this->info(__('Mise à jour des nouveaux droits...'));
        $this->Acl->updateAcl();

        $this->info(__('Ajout des nouveaux groupes de droits aux permissions utilisateurs...'));
        //Initialisation des groupes
        $this->Acl->createUsersPermission('Preferences', 1);
        $this->Acl->createUsersPermission('AutresActes');
        $this->Acl->createUsersPermission('Actes');
        $this->Acl->createUsersPermission('Validations');
        $this->Acl->createUsersPermission('Search');
        $this->Acl->createUsersPermission('Signatures');
        $this->Acl->createUsersPermission('Teletransmettre');
        $this->Acl->createUsersPermission('ExportGed');
        $this->Acl->createUsersPermission('Signatures');
        $this->Acl->createUsersPermission('VerserSae');
        $this->Acl->createUsersPermission('Cakeflow');

        $this->Acl->createUsersPermission('attribuerCircuit', 1);
        $this->Acl->createUsersPermission('sendToGedDeliberation', 'sendToGedAutreActe');
        $this->Acl->createUsersPermission('sendToSaeAutresActes', 'sendToSaeDeliberation');

        $this->Acl->createUsersPermission('Votes', 'Seances');
        $this->Acl->createUsersPermission('Avis', 'Seances');
        $this->Acl->createUsersPermission('sendSeanceAvisToIdelibre', 'sendSeanceVotesToIdelibre');

        $this->Acl->createUsersPermission('sendToSae');
        //$this->Acl->createUsersPermission('importVotesIdelibre');
        $this->Acl->createUsersPermission('downloadVotesElectroniqueJson', 'Seances');

        $this->Acl->createUsersPermission('ExportGedPostSeances', 'sendToGedDeliberation');

        $this->Acl->createUsersPermission('Verifications', 'Connecteurs');

        $this->info(__('Ajouts des nouvelles clefs de configuration dans webdelib.inc...'));

        $this->configureKeyAdd('SAE', 'SAE_PASTELL_DOSSIER_TYPE_SEANCE', 'ls-dossier-seance');

        $this->configureKeyAdd('FusionConv.app.fileOdt.convert', 'App.convert.tool', 'pdftk-imagick');
        $this->configureKeyAdd('App.convert.tool', 'App.convert.resolution', '150');
        $this->configureKeyAdd('App.convert.resolution', 'App.convert.url', 'http://wd-lspdf2odt');

        $this->info(__('Suppression des clefs de configuration obsolètes dans webdelib.inc...'));

        $this->configureKeysRemove([
            'PDFStamp.logo','AuthManager.Cas.cert_path',
            'S2LOW_PEM', 'S2LOW_SSLKEY', 'S2LOW_CACERT', 'S2LOW_CLASSIFICATION',
            'App.fullBaseUrl',
            'debug', 'debugJs',
            'FusionConv.cloudooo_port',
            'PDFTK_EXEC', 'PDFINFO_EXEC','GS_EXEC', 'GS_RESOLUTION',
            'PASTELL_CACERT','PASTELL_CLIENTCERT','PASTELL_CERTPWD',
            'IPARAPHEUR_CACERT','IPARAPHEUR_CLIENTCERT','IPARAPHEUR_CERTPWD',
            'IDELIBRE_USE_CERT','IDELIBRE_CAPATH','IDELIBRE_CA','IDELIBRE_KEY','IDELIBRE_CERT','IDELIBRE_CERTPWD']);

        $this->info(__('Modifications des uris des services dans webdelib.inc...'));
        $this->configureKeyEdit('FusionConv.Gedooo.wsdl', 'http://flow:8080/ODFgedooo');
        $this->configureKeyEdit('FusionConv.cloudooo_host', 'http://wd-cloudooo:8011');
        $this->configureKeyEdit('PDFStamp.host', 'http://wd-pdf-stamp:8080');
        $this->configureKeyEdit('portainer.host', 'http://wd-portaine:9000');

        $this->info(__('Transformation des chemins de fichier dans webdelib.inc...'));
        $this->configureKeyAdd(
            'PDFStamp.host',
            'PDFStamp.logo',
            'DATA . Configure::read(\'Config.tenantName\') . \'/config/cert_cas/client.pem\''
        );
        $this->configureKeyAdd(
            'AuthManager.Cas.uri',
            'AuthManager.Cas.cert_path',
            'DATA . Configure::read(\'Config.tenantName\') . \'/config/cert_cas/client.pem\''
        );

        $this->configureKeyAdd(
            'S2LOW_HOST',
            'S2LOW_PEM',
            'DATA . Configure::read(\'Config.tenantName\') . \'/config/cert_s2low/client.pem\''
        );
        $this->configureKeyAdd(
            'S2LOW_PEM',
            'S2LOW_SSLKEY',
            'DATA . Configure::read(\'Config.tenantName\') . \'/config/cert_s2low/key.pem\''
        );
        $this->configureKeyAdd(
            'S2LOW_SSLKEY',
            'S2LOW_CACERT',
            'DATA . Configure::read(\'Config.tenantName\') . \'/config/cert_s2low/ca.pem\''
        );
        $this->configureKeyAdd(
            'S2LOW_CACERT',
            'S2LOW_CLASSIFICATION',
            'DATA . Configure::read(\'Config.tenantName\') . \'/config/cert_s2low/classification.xml\''
        );

        return true;
    }

    /**
     * @param $commandPath
     * @return bool|void
     *
     * @version 6.0.3
     */
    public function patch($commandName, $incrementVersion = true)
    {
        $commandPath = $this->commandPatchs[$commandName];
        $versionStart = $commandPath['start'];
        $versionFin = $commandPath['fin'];

        $this->header(__('Mise à jour de webdelib v%s vers v%s', $versionStart, $versionFin));

//        $wrap = new Phinx\Wrapper\TextWrapper($this->appPhinx);
//        $this->appPhinx->run(new StringInput('migrate'), new NullOutput());
//        $this->info($wrap->getMigrate());
//        $success = $wrap->getExitCode()===0;

        $success = $success && $this->patchCommand($commandName);

        if ($success) {
            if ($incrementVersion) {
                //Mise en place la nouvelle version
                $this->info(__('Enregistrement de la version %s', $versionFin));
                $this->Version->create();
                $this->Version->save([
                    'subject' => 'webdelib',
                    'version' => $versionFin,
                ]);
            }

            $this->footer(__('Patch de la version %s vers la %s accompli avec succès !', $versionStart, $versionFin));

            return true;
        }

        $this->error(__('Un problème est survenu lors de l\'application du patch !'));
        return false;
    }

    /**
     * @version 6.0.3
     */
    public function version()
    {
        // Pas mise à jour automatique en cascade avant la version 6.0.3
        if (version_compare(VERSION, '6.0.2', '<')) {
            $this->info(__('Version inconnue antérieur à la v6.0.2'));
            $this->stop();
        }

        $this->info(__('Numéro de version de la base de donnée : %s', $this->lastVersion()));
        $this->info(__('Numéro de version de l\'application : %s', VERSION));
    }


    public function install()
    {
        $alerte = null;

        if (!$this->param('no-question')) {
            $promp = "Attention Voulez-vous mettre à jour une nouvelle installation:\n "
                . "[o] Oui \n [n] Non\n"
                . "Que voulez-vous faire?";
            //Recevoir des alertes par email
            $alerte = strtolower($this->in($promp, ['o', 'n'], 'n'));
            if ($alerte === 'n') {
                return $this->stop();
            }
        }

        if ($alerte === 'o' || $this->param('no-question')) {
            foreach ($this->commandPatchs as $commandPatchKey => $commandPatch) {
                if ($this->patch($commandPatchKey, false)!==true) {
                    $this->stop(static::CODE_SUCCESS);
                }
            }
            $this->stop(static::CODE_SUCCESS);
        }


        $this->stop(static::CODE_SUCCESS);
    }

    /**
     * @param $searchKey
     * @param $newKey
     * @param $value
     * @return bool
     *
     * @version 6.0.2
     */
    private function addComment($searchKey, $comment, $eol = 0)
    {
        $lines = file(DATA . Configure::read('Config.tenantName')
            . DS . 'config' . DS . 'webdelib.inc');
        $numberLines = count($lines);
        $searchKeyFound = false;
        for ($i = 0; $i <= $numberLines; $i++) {
            // Vérification si la ligne comporte la clef recherchée
            if ($searchKeyFound xor (strpos($lines[$i], $searchKey) !== false)) {
                $searchKeyFound = true;
                if (strpos($lines[$i], ');') !== false) {
                    $newLine = $i;
                    break;
                }
            }
        }
        $newlines = null;
        for ($i = 0; $i < $eol; $i++) {
            $newlines[]= PHP_EOL;
        }
        $newlines[]= __("/* %s */", $comment);
        $newlines[]= PHP_EOL;

        $aNewFileContent = array_merge(
            array_slice($lines, 0, $newLine +1),
            $newlines,
            array_slice($lines, $newLine +1, $numberLines)
        );

        if (file_put_contents(DATA . Configure::read('Config.tenantName')
            . DS . 'config' . DS . 'webdelib.inc', implode('', $aNewFileContent))) {
            return true;
        }

        return false;
    }

    /**
     *
     */
    private function saveConfigureBack()
    {
        $file = new File(DATA . Configure::read('Config.tenantName')
            . DS . 'config' . DS . 'webdelib.inc');

        if ($file->exists()) {
            $file->copy(DATA . Configure::read('Config.tenantName')
                . DS . 'config' . DS . 'webdelib.inc' . time() . '.back');
        }
    }

    /**
     * @param $commandPath
     * @return array|string[]
     *
     */
    private function pathSqlFiles($commandPath)
    {
        $pathSqlFiles = [];
        $pathFile = $this->patchPaths['app'] . DS . $commandPath['start'] . '_to_' . $commandPath['fin'] .'.sql';

        $file = new File($pathFile);

        if ($file->exists()) {
            $pathSqlFiles = [
                'Application : patch v' . $commandPath['fin'] => $pathFile,
            ];
        }
        $file->close();

        if (isset($commandPath['sql_files'])) {
            foreach ($commandPath['sql_files'] as $sqlFileKey => $sqlFile) {
                $pathFile = $this->patchPaths[$sqlFileKey] . DS . $sqlFile['file'];

                $file = new File($pathFile);

                if ($file->exists()) {
                    $pathSqlFiles += [
                        'Application : ' . $sqlFile['name'] => $pathFile,
                    ];
                }
                $file->close();
            }
        }

        return $pathSqlFiles;
    }


    /**
     * @param $commandName
     *
     * @version 6.0.3
     */
    private function patchCommand($commandName)
    {
        try {
            $this->info(__('Passage des patchs des commandes de mise à jour...'));

            if (method_exists($this, $commandName)) {
                return $this->$commandName();
            }
        } catch (Exception $e) {
            $this->error(__('Erreur lors du passage de la commande de patch: %s', $e->getMessage()));
            return false;
        }

        return true;
    }

    /**
     * @param $commandName
     * @return array
     *
     * @version 6.0.3
     */
    private function caculateLastPatch($commandName)
    {
        $commandLastPatchs = [];
        $commandFilterPatchs = [];

        $versionStart = $this->commandPatchs[$commandName]['start'];
        $version = $this->commandPatchs[$commandName]['fin'];
        // Pas mise à jour automatique en cascade avant la version 6.0.3
        if (version_compare($version, '6.0.2', '<')) {
            return $commandLastPatchs;
        }

        foreach ($this->commandPatchs as $commandPatchKey => $commandPatch) {
            if (version_compare($commandPatch['fin'], '6.0.2', '>')
                && version_compare($commandPatch['fin'], VERSION, '<')) {
                $commandFilterPatchs[$commandPatchKey] = $commandPatch;
            }
        }

        foreach ($commandFilterPatchs as $commandFilterPatchKey => $commandFilterPatch) {
            if (!$this->checkVersion($commandFilterPatch['fin'])
                && version_compare($commandFilterPatch['fin'], $version, '<=')
                && version_compare($commandFilterPatch['fin'], $this->lastVersion(), '>')
            ) {
                $commandLastPatchs[] = $commandFilterPatchKey;
            }
        }

        return $commandLastPatchs;
    }

    /**
     * @return array
     *
     * @version 6.0.4
     */
    private function caculateListPatch()
    {
        $versionPatch = substr(VERSION, 4, 1);
        $versionMinor = substr(VERSION, 2, -2);
        $versionMajor = substr(VERSION, 0, -4);
        $commandPatchs = [];
        for ($i=4; $i<=$versionPatch; $i++) {
            $versionPatchOld = $i-1;
            $commandPatchs += [
                'version60'. $versionPatchOld .'to60' . $i => [
                    'start' => '6.0.' . $versionPatchOld,
                    'fin' => '6.0.'. $i
                ]
            ];
        }

        return $commandPatchs;
    }

    /**
     * @param $commandName
     * @return string
     *
     * @version 6.0.3
     */
    private function caculateCommand($version)
    {
        $versionCurrent = explode('.', $version);
        return sprintf(
            'version%s%s%sto%s%s%s',
            $versionCurrent[0],
            $versionCurrent[1],
            $versionCurrent[2]-1,
            $versionCurrent[0],
            $versionCurrent[1],
            $versionCurrent[2]
        );
    }

    /**
     * @param $version
     * @return bool
     *
     * @version 6.0.3
     * @since 6.0.2
     */
    private function checkVersion($version)
    {
        $versionsCreated = $this->Version->find('count', [
            'conditions' => [
                'subject' => ['webdelib','web-delib'],
                'version' => $version,
            ],

        ]);

        return $versionsCreated > 0;
    }

    private function lastVersion()
    {
        $version = $this->Version->find('first', [
            'fields' => ['version'],
            'conditions' => [
                'Version.subject' => ['webdelib','web-delib'],
            ],
            'order' => ['Version.created' => 'desc', 'Version.version' => 'desc'],
        ]);

        return $version['Version']['version'] ?: null;
    }

    /**
     * @param $searchKey
     * @param $newKey
     * @param $value
     * @return bool
     *
     * @version 6.0.2
     */
    private function configureKeyAdd($searchKey, $newKey, $value)
    {
        $this->info(__('Ajout du Configure::write "%s"', $newKey));

        $lines = file(
            DATA . Configure::read('Config.tenantName')
            . DS . 'config' . DS . 'webdelib.inc'
        );
        $numberLines = count($lines);
        $searchKeyFound = false;
        for ($i = 0; $i <= $numberLines; $i++) {
            // Vérification si la ligne comporte la clef recherchée
            if ($searchKeyFound xor (strpos($lines[$i], $searchKey) !== false)) {
                $searchKeyFound = true;
                if (strpos($lines[$i], ');') !== false) {
                    $newLine = $i;
                    break;
                }
            }
        }

        $newlines = [];
        if (is_bool($value)) {
            $newlines = ["Configure::write('$newKey', " . ($value === true ? 'true' : 'false') . ");", PHP_EOL];
        } elseif (is_array($value)) {
            $newlines = ["Configure::write('$newKey', " . AppTools::varExportConfig($value, true) . ");", PHP_EOL];
        } elseif (strpos($value, 'DATA .') !== false) {
            $newlines = ["Configure::write('$newKey', $value);", PHP_EOL];
        } else {
            $newlines = ["Configure::write('$newKey', '" . $value . "');", PHP_EOL];
        }

        $aNewFileContent = array_merge(
            array_slice($lines, 0, $newLine +1),
            $newlines,
            array_slice($lines, $newLine +1, $numberLines)
        );

        if (file_put_contents(DATA . Configure::read('Config.tenantName')
            . DS . 'config' . DS . 'webdelib.inc', implode('', $aNewFileContent))) {
            return true;
        }

        return false;
    }

    private function configureKeyEdit($param, $new_value)
    {
        $file = new File(DATA . $this->getTenantName() . DS .'config' . DS . 'webdelib.inc', true);
        $content = $file->read();

        if (is_bool(Configure::read($param))) {
            $valeur = Configure::read($param) === true ? 'true' : 'false';
        } else {
            $valeur = Configure::read($param);
        }

        $count = 0;
        if (is_array($new_value)) {
            $pattern = '/Configure::write\(\'' . str_replace('.', '\.', $param) . '\'\,[^);]*\);/i';
            $replacement = "Configure::write('$param', " . AppTools::varExportConfig($new_value, true) . ");";

            $content = preg_replace($pattern, $replacement, $content, -1, $count);
        }


        if ($count === 0) {
            $host_b = "Configure::write('$param', " . var_export((string)$valeur, true) . ");";
            $host_a = "Configure::write('$param', " . var_export((string)$new_value, true) . ");";

            $content = str_replace($host_b, $host_a, $content, $count);
        }

        if ($count === 0) {
            $host_b = "Configure::write('$param', " . $valeur . ");";
            $host_a = "Configure::write('$param', $new_value);";

            $content = str_replace($host_b, $host_a, $content, $count);
        }

        if ($count>1) {
            $this->error(__('Erreur doublon dans la configuration'));
        }

        $file->write($content);
        $file->close();

        return true;
    }

    private function configureKeysRemove(array $keys): void
    {
        foreach ($keys as $key) {
            $this->configureKeyRemove($key);
        }
    }

    /**
     * @param $searchKey
     * @param $newKey
     * @param $value
     * @return bool
     *
     * @version 6.0.2
     */
    private function configureKeyRemove($searchKey)
    {
        $lines = file(DATA . Configure::read('Config.tenantName')
            . DS . 'config' . DS . 'webdelib.inc');
        $numberLines = count($lines);
        $searchKeyFound = false;
        for ($i = 0; $i <= $numberLines; $i++) {
            // Vérification si la ligne comporte la clef recherchée
            if ($searchKeyFound xor (strpos($lines[$i], $searchKey) !== false)) {
                $searchKeyFound = true;
                if (strpos($lines[$i], ');') !== false) {
                    $newLine = $i;
                    break;
                }
            }
        }

        $aNewFileContent = array_merge(
            array_slice($lines, 0, $newLine),
            array_slice($lines, $newLine + 1, $numberLines)
        );

        if (file_put_contents(DATA . Configure::read('Config.tenantName')
            . DS . 'config' . DS . 'webdelib.inc', implode('', $aNewFileContent))) {
            return true;
        }

        return false;
    }
}
