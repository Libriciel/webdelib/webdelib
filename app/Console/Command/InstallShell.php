<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

use Phinx\Console\PhinxApplication;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;

App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('ConsoleOutput', 'Console');

/**
 * Console InstallShell
 *
 * @package app.Console.Command.Shell
 * @version 6.0.3
 * @since   4.2.0
 */
class InstallShell extends AppShell
{
    /**
     * public tasks
     * @var array
     */
    public $tasks = [
      'Acl',
      'Init',
      'Sql'
    ];

    /**
     * [public uses]
     * @var array
     */
    public $uses = [
        'Version',
    ];

    /**
     * Contains a file list for SQL purge
     * @var [type]
     */
    private $sqlFilesDelete = [];
    /**
     * @var true
     */
    private bool $runInstall = false;

    public function initialize()
    {
        parent::initialize();

        $paths = [
            'app' => APP . 'Config' . DS . 'Schema',
        ];

        $this->sqlFilesDelete = [
            'webdelib delete_data' => $paths['app'] . DS . 'sql' . DS . 'delete_data_projet_seance.sql'
        ];

        $this->sqlFilesDeleteWithDate = [
            'webdelib delete_data_with_date' => $paths['app'] . DS . 'sql' . DS . 'delete_data_with_date.sql'
        ];
    }

    /**
     * main
     * Installation of the application
     */
    public function main()
    {
        try {
            foreach ($this->getFqdnDatabases() as $tenant) {
                $this->setRunInstall(false);
                $this->initSchema($tenant);
                // Connexion à la base de donnée
                $this->setDbMultiBase($tenant, false);
                $this->info(__('Vérification du schéma (tenant: %s)', $tenant));
                // Lancement de la migration de basse de donnée en installation ou path
                $this->runMigration();
                // Attente pour prendre en compte l'execution correcte du commit de base de donnée
                sleep(2);
                // Reconnexion à la base de donnée pour prendre en compte les
                // nouvelles tables (cacheSources cache à vérifier)
                $config = ConnectionManager::enumConnectionObjects()[$tenant];
                ConnectionManager::create($tenant, $config);
                if ($this->runInstall) {
                    // Installation des données d'initialisations
                    $this->runInstallData();
                } else {
                    // Exécution des modifications de version
                    $this->runPatch();
                }
            }
        } catch (RuntimeException $e) {
            $this->error(
                __('Erreur lors de l\'execution : %s', $e->getMessage())
            );
            $this->error(
                __('Trace : %s', $e->getTraceAsString())
            );
        }
        $this->stop(static::CODE_SUCCESS);
    }

    /**
     * main Installation of the application
     */
    public function canaryMigration()
    {
        try {
            foreach ($this->getFqdnDatabases() as $fqdn => $tenant) {
                $this->setDbMultiBase($tenant, false);
                // Connexion à la base de donnée
                $this->info(
                    __(
                        'Migration de la base de donnée en version v%s (tenant: %s)',
                        VERSION,
                        $tenant
                    )
                );
                $wrap = $this->executeCommandPhinxWithDatabaseConfig('migrate', $tenant);
                $wrap->getMigrate();
                $success = $wrap->getExitCode()===0;

                if (!$success) {
                    throw new CakeException(
                        __('Migration error !')
                    );
                }
                // Attente pour prendre en compte l'execution correcte du commit de base de donnée
                sleep(2);
            }
        } catch (CakeException|RuntimeException $e) {
            $this->error(
                __('Erreur lors de l\'execution : %s', $e->getMessage())
            );
            $this->error(
                __('Trace : %s', $e->getTraceAsString())
            );
        }
        $this->stop(static::CODE_SUCCESS);
    }

    public function setRunInstall($runInstall)
    {
        $this->runInstall = $runInstall;
    }

    public function runInstall()
    {
        // On vérifie que les tables existent
        $this->info(__('Ajout de la structure du schéma (tenant: %s)', $this->getTenantName()));
        $this->setRunInstall(true);
        $this->executeMigration();
        $this->info(__('La structure du schéma a été ajouté avec succès (tenant: %s)', $this->getTenantName()));
    }

    public function runMigration()
    {
        // On vérifie que les tables existent
        if (!$this->checkTableVersion()) {
            $this->runInstall();
        }
        $this->executeMigration();
    }

    private function executeMigration()
    {
        try {
            $this->info(
                __(
                    'Migration du schéma en version v%s (tenant: %s)',
                    VERSION,
                    $this->getTenantName()
                )
            );
            $wrap = $this->executeCommandPhinxWithDatabaseConfig('migrate', $this->getTenantName());
            $wrap->getMigrate();
            $success = $wrap->getExitCode()===0;

            if (!$success) {
                throw new CakeException(
                    __('Migration error !')
                );
            }
        } catch (CakeException $e) {
            throw new CakeException($e->getMessage());
        }
    }

    public function runInstallData()
    {
        try {
            $this->info(__('webdelib v%s installation des données de démarrage', $this->version));
            $this->info(__('Data installation (base: %s)... Please wait...', $this->getTenantName()), static::QUIET);

            $this->Acl->creationAcl();

            $this->Init->initializeData();

            //Mise en place des droits pour l'admin principal
            $this->Acl->createAdminPermission();

            $this->info(__('Enregistrement de la version %s...', VERSION));

            $this->Version->Behaviors->load('DynamicDbConfig');
            $this->Version->create();
            $this->Version->save([
                'subject' => 'webdelib',
                'version' => VERSION,
                'created' => date('Y-m-d H:i:s'),
            ]);

            $this->footer(
                __(
                    'Nouvelle installation du tenant accomplie avec succès (tenant: %s) !',
                    $this->getTenantName()
                )
            );
        } catch (Exception $e) {
            $this->error($e->getMessage() . PHP_EOL . $e->getTraceAsString(), static::QUIET);
        }
    }

    public function runPatch()
    {
        try {
            if ($this->lastVersion()===VERSION) {
                $this->info(__('Tenant déjà en dernière version (%s)', VERSION), static::QUIET);
                return;
            }
            $this->info(__('webdelib v%s command patch install', $this->version));
            $this->dispatchShell('patch install --no-question --tenant ' . $this->getTenantName());
        } catch (Exception $e) {
            $this->error($e->getMessage() . PHP_EOL . $e->getTraceAsString(), static::QUIET);
        }
    }

    /**
     * Function de purge des projets et des séances
     *
     * @version 5.1.1
     */
    public function deleteProjetSeance()
    {
        $promp = __('[q] Quitter')
            ."\n"
            . __('Voulez-vous vraiment supprimer les données de projet et de séance ?')
            . "\n";
        $selected = strtolower($this->in($promp, [
        'yes', 'y', 'q', __('exit'), __('quitter')]));

        if ($selected === 'q') {
            $this->footer(__('Suppression interrompu !'));
            return $this->stop(static::CODE_ERROR);
        }
        $promp = __('[q] Quitter')
            ."\n"
            . __('Avez-vous bien pris connaissance que l\'action que vous allez effectuer est irréversible ?')
            . "\n";
        $selected = strtolower($this->in($promp, [
        'yes', 'y', 'q', __('exit'), __('quitter')]));

        if ($selected === 'q') {
            $this->footer(__('Suppression interrompu !'));
            return $this->stop(static::CODE_ERROR);
        }
        $this->Sql->begin();
        if ($this->Sql->apply($this->sqlFilesDelete)) {
            $this->Sql->commit();
            $this->footer(
                __('Suppression des données de webdelib v%s accomplie avec succès !', $this->version)
            );
            $this->stop(static::CODE_SUCCESS);
        } else {
            $this->Sql->rollback();
            throw new CakeException(
                __('Suppression des données de webdelib v%s en error !', $this->version)
            );
        }
    }

    /**
     * Function de purge des projets et des séances
     */
    public function deleteProjetsWithDate()
    {
        $promp = __('[q] Quitter')
            ."\n"
            . __('Voulez-vous vraiment supprimer les données de projet antérieur à une date ?')
            . "\n";
        $selected = strtolower($this->in($promp, [
            'yes', 'y', 'q', __('exit'), __('quitter')]));

        if ($selected === 'q') {
            $this->footer(__('Suppression interrompu !'));
            $this->stop(static::CODE_ERROR);
        }

        $this->Sql->execute(file_get_contents(current($this->sqlFilesDeleteWithDate)));

        $promp = __('[q] Quitter')
            ."\n"
            . __('Veuillez renseigner une date au format anglais 2018-01-01 ?')
            . "\n";
        $date = trim($this->in($promp));

        $promp = __('[q] Quitter')
            ."\n"
            . __(
                'Avez-vous bien pris connaissance que l\'action que vous allez effectuer est '
                . 'irréversible pour les données antérieurs à la date du %s ?',
                $date
            )
            . "\n";
        $selected = strtolower($this->in($promp, [
            'yes', 'y', 'q', __('exit'), __('quitter')]));

        if ($selected === 'q') {
            $this->footer(__('Suppression interrompu !'));
            $this->stop(static::CODE_ERROR);
        }
        $this->Sql->begin();
        if ($this->Sql->execute(__('SELECT delete_projects_with_date(\'%s\');', $date))) {
            $this->Sql->commit();
            $this->footer(
                __('Suppression des données de webdelib v%s accomplie avec succès !', $this->version)
            );
            $this->stop(static::CODE_SUCCESS);
        } else {
            $this->Sql->rollback();
            throw new CakeException(
                __('Suppression des données de webdelib v%s en error !', $this->version)
            );
        }
    }

    /**
     * Options d'exécution et validation des arguments
     * @return ConsoleOptionParser $parser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();
        $parser->description(__('Installateur webdelib'));

        $parser->addSubcommand('deleteProjetSeance', [
            'help' => __('Suppression des données de projet et de séance.'),
        ]);

        $parser->addSubcommand('deleteProjetsWithDate', [
            'help' => __('Suppression des données de projet par rapport à une date.'),
        ]);

        $parser->addSubcommand('canaryMigration', [
            'help' => __('Migration de la base de donnée canary'),
        ]);

        $parser->addSubcommand('runInstall', [
            'help' => __('Migration: installation manuelle de la base de donnée du tenant'),
        ]);

        $parser->addSubcommand('runMigration', [
            'help' => __('Migration: Mise à jour manuelle de la base de donnée du tenant'),
        ]);

        $parser->addSubcommand('runPatch', [
            'help' => __('Patch: Mise à jour manuelle du tenant'),
        ]);

        $parser->addOption(
            'no-question',
            [
                'boolean' => true,
                'help' => __('Pas de question avec saisie au clavier.'),
            ]
        );

        return $parser;
    }

    private function executeCommandPhinxWithDatabaseConfig($command, $database)
    {
        try {
            $appPhinx = new PhinxApplication();
            $appPhinx->setAutoExit(false);

            // Préparer les options de la commande Phinx migrate
            $input = new ArrayInput([
                'command' => $command,
                '-e' => $database,
            ]);

            $appPhinx->run($input, new ConsoleOutput());
        } catch (Exception $e) {
            throw new CakeException(
                $e->getMessage()
            );
        }

        return new Phinx\Wrapper\TextWrapper($appPhinx);
    }

    public function lastVersion()
    {
        if ($this->runInstall === true) {
            return VERSION;
        }

        try {
            if (!$this->checkTableVersion()) {
                return false;
            }
            $ds = ConnectionManager::getDataSource($this->getTenantName());
            $version = $ds->fetchAll(
                "SELECT * FROM versions WHERE subject='webdelib' "
                ."OR subject='web-delib' ORDER BY created desc, version desc"
            );
        } catch (Exception $e) {
            $this->error('Erreur de lecture de la table version (lastVersion) : ' . $e->getMessage());
        }
        if (empty($version[0][0]['version'])) {
            $this->error('Erreur lors de la récupération de la version');
            $this->stop();
        }
        return $version[0][0]['version'];
    }

    public function checkTableVersion()
    {
        try {
            $ds = ConnectionManager::getDataSource($this->getTenantName());
            $tables = $ds->listSources();
            $tableName = $ds->fullTableName('versions', false, false);

            if (in_array($tableName, $tables, true)) {
                return true;
            }
        } catch (Exception $e) {
            $this->error('Erreur de lecture de la table version (checkTableVersion) : ' . $e->getMessage());
        }

        return false;
    }

    private function initSchema($schemaName)
    {
        $ds = ConnectionManager::getDataSource('default');
        $exists = $ds->fetchAll(
            "SELECT schema_name FROM information_schema.schemata WHERE schema_name = '$schemaName'",
            false
        );
        // Requête SQL pour vérifier si le schéma existe
        if (empty($exists)) {
            $sql = sprintf('CREATE SCHEMA "%s";', $schemaName);
            try {
                $ds->execute($sql);
                $this->info(__('Le schéma "%s" a été créé avec succès.', $schemaName));
            } catch (\Exception $e) {
                $this->error('Erreur lors de la création du schéma : ' . $e->getMessage());
            }
        }
    }
}
