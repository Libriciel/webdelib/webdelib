<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

Configure::write('USE_S2LOW', true);
Configure::write('S2LOW_HOST', 'https://s2low.formations.adullact.org/');
Configure::write('S2LOW_PEM', APP . 'Config/cert_s2low/client.pem');
Configure::write('S2LOW_SSLKEY', APP . 'Config/cert_s2low/key.pem');
Configure::write('S2LOW_CACERT', APP . 'Config/cert_s2low/ca.pem');
Configure::write('S2LOW_CERTPWD', 'julien.legall@libriciel.coop');
Configure::write('S2LOW_USEPROXY', false);
Configure::write('S2LOW_PROXYHOST', '');
Configure::write('USE_MAILSEC', false);
Configure::write('S2LOW_MAILSECPWD', '');
Configure::write('S2LOW_USELOGIN', true);
Configure::write('S2LOW_LOGIN', 'user1cle');
Configure::write('S2LOW_PWD', 'user1cle');
