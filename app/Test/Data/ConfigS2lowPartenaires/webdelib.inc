<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

Configure::write('debug', 0);
/* URL de Webdelib (pour mails) */
Configure::write('App.fullBaseUrl', 'https://webdelib.local');
Configure::write('Config.language', 'fra');

/* Configuration session default 600 */
Configure::write('Session.timeout', 600);
Configure::write('Cakeflow.optimisation', true);
Configure::write('Cakeflow.validation_finale', false);

/* Options d'affichage */
Configure::write('AFFICHE_HIERARCHIE_SERVICE', true);
Configure::write('AFFICHE_CONVOCS_ANONYME', true);
Configure::write('CONVOCS_MODIFIABLES', false);
Configure::write('NOT_UNIQUE_CIRCUIT', true);
Configure::write('DELIBERATIONS_MULTIPLES', false); //Mode délibérations multiples
Configure::write('INIT_SEQ', false); //Initialiser le numéro de séquence
Configure::write('LIMIT', 5); //limitation du nombre de projets par bannette
Configure::write('search.FusionConv.limit', 100); //limitation du nombre de projets généré par recherche

/* Configuration du Webdav */
Configure::write('Webdav.protocole', 'vnd.sun.star.webdavs');
Configure::write('Webdav.port', '');

/* Configuration pour la conversion de format des documents
 * Configuration FusionConv
 * SOAP : http://127.0.0.1:8880/ODFgedooo/OfficeService?wsdl
 * REST : http://127.0.0.1:8880/ODFgedooo
 */
Configure::write('FusionConv.Gedooo.wsdl', 'http://192.168.2.35:8880/ODFgedooo-1.0.1/OfficeService?wsdl');
Configure::write('FusionConv.method', 'GedoooCloudooo');
Configure::write('FusionConv.cloudooo_host', '192.168.2.35');
Configure::write('FusionConv.app.fileOdt.convert', true); //Convertion des odt en image background
Configure::write('FusionConv.maintenance.ssh.host', '');
Configure::write('FusionConv.maintenance.ssh.port', '');
Configure::write('FusionConv.maintenance.ssh.username', 'admin');
Configure::write('FusionConv.maintenance.ssh.password', 'admin');

/* Configuration pour la manipulation des pdf */
Configure::write('PDFTK_EXEC', '/usr/bin/pdftk');
Configure::write('PDFINFO_EXEC', '/usr/bin/pdfinfo');

/* Configuration GhostScript */
// Chemin vers l'executable
Configure::write('GS_EXEC', '/usr/bin/gs');
// Résolution d'image (pixels) : Agit sur la rapidité de génération des annexes (Conversion pdf et doc en image)
Configure::write('GS_RESOLUTION', '150');

/* Configuration mail */
Configure::write('MAIL_FROM', 'pouet@pouet.fr');
Configure::write('SMTP_USE', false);
Configure::write('SMTP_PORT', '');
Configure::write('SMTP_TIMEOUT', '');
Configure::write('SMTP_HOST', '');
Configure::write('SMTP_USERNAME', '');
Configure::write('SMTP_PASSWORD', '');
Configure::write('SMTP_CLIENT', '');

/**
 * Parapheur (Signature électronique, délégation)
 */
/* Activer le parapheur */
Configure::write('USE_PARAPHEUR', false);
/* Service utilisé pour le parapheur ('PASTELL', 'IPARAPHEUR', ...) */
Configure::write('PARAPHEUR', 'IPARAPHEUR');

/**
 * TDT (Tiers de Télétransmission)
 */
/* Activer le TDT (tiers de télétransmission) */
Configure::write('USE_TDT', true);
/* Service utilisé pour le TDT ('PASTELL', 'S2LOW', ...) */
Configure::write('TDT', 'S2LOW');

/**
 * SAE (Service d'archivage électronique)
 */
/* Activer le SAE */
Configure::write('USE_SAE', false);
/* Service utilisé pour le TDT ('PASTELL', 'ASALAE', ...) */
Configure::write('SAE', 'ASALAE');

/**
 * GED (Gestion électronique documentaire)
 */
/* Activer la GED */
Configure::write('USE_GED', true);
/* Service utilisé pour la GED ('PASTELL' ou 'CMIS') */
Configure::write('GED', 'CMIS');
Configure::write('GED_XML_VERSION', '3');

/**
 * Configuration des connecteurs
 */
/* Configuration de Pastell */
Configure::write('USE_PASTELL', false);
Configure::write('PASTELL_HOST', '');
Configure::write('PASTELL_TYPE', '');
Configure::write('PASTELL_PARAPHEUR_TYPE', 'Actes');
Configure::write('PASTELL_LOGIN', '');
Configure::write('PASTELL_PWD', '');
Configure::write('PASTELL_CACERT', APP . 'Config/cert_pastell/ac.pem');
Configure::write('PASTELL_CLIENTCERT', APP . 'Config/cert_pastell/cert.pem');
Configure::write('PASTELL_CERTPWD', '');

/* Configuration Webservice du iparapheur */
Configure::write('USE_IPARAPHEUR', false);
Configure::write('IPARAPHEUR_HOST', 'https://secure-parapheur.test.adullact.org/');
Configure::write('IPARAPHEUR_LOGIN', 'webdelib');
Configure::write('IPARAPHEUR_PWD', 'webdelib');
Configure::write('IPARAPHEUR_TYPE', 'ACTES');
Configure::write('IPARAPHEUR_VISIBILITY', 'PUBLIC');
Configure::write('IPARAPHEUR_URI', 'ws-iparapheur');
Configure::write('IPARAPHEUR_CACERT', APP . 'Config/cert_parapheur/ca.pem');
Configure::write('IPARAPHEUR_CLIENTCERT', APP . 'Config/cert_parapheur/cert.pem');
Configure::write('IPARAPHEUR_CERTPWD', '');

/* Configuration S2LOW */
Configure::write('USE_S2LOW', true);
Configure::write('S2LOW_HOST', 'https://s2low.partenaires.libriciel.fr');
Configure::write('S2LOW_PEM', APP . 'Config/cert_s2low/client.pem');
Configure::write('S2LOW_SSLKEY', APP . 'Config/cert_s2low/key.pem');
Configure::write('S2LOW_CACERT', APP . 'Config/cert_s2low/ca.pem');
Configure::write('S2LOW_CERTPWD', 'libriciel');
Configure::write('S2LOW_USEPROXY', false);
Configure::write('S2LOW_PROXYHOST', '');
Configure::write('USE_MAILSEC', false);
Configure::write('S2LOW_MAILSECPWD', '');
Configure::write('S2LOW_USELOGIN', true);
Configure::write('S2LOW_LOGIN', 'user1cle');
Configure::write('S2LOW_PWD', 'user1cle');

/* Configuration CONNECTEUR CMIS GED */
Configure::write('CMIS_HOST', '');
Configure::write('CMIS_LOGIN', 'admin');
Configure::write('CMIS_PWD', 'admin');
Configure::write('CMIS_REPO', '');

/* Configuration pour Idélibre */
Configure::write('USE_IDELIBRE', false);
Configure::write('IDELIBRE_HOST', '');
Configure::write('IDELIBRE_LOGIN', '');
Configure::write('IDELIBRE_PWD', '');
Configure::write('IDELIBRE_CONN', ''); //Collectivité
Configure::write('IDELIBRE_USEPROXY', false);
Configure::write('IDELIBRE_PROXYHOST', '');
//Certificats pour Idélibre
Configure::write('IDELIBRE_USE_CERT', false);
Configure::write('IDELIBRE_CAPATH', APP . 'Config/cert_idelibre/');
Configure::write('IDELIBRE_CA', APP . 'Config/cert_idelibre/ca.pem');
Configure::write('IDELIBRE_KEY', APP . 'Config/cert_idelibre/key.pem');
Configure::write('IDELIBRE_CERT', APP . 'Config/cert_idelibre/cert.pem');
Configure::write('IDELIBRE_CERTPWD', '');

/* Configuration ASAL@AE */
Configure::write('USE_ASALAE', false);
Configure::write('ASALAE_HOST', '');
Configure::write('ASALAE_LOGIN', '');
Configure::write('ASALAE_PWD', '');
Configure::write('ASALAE_WSDL', '');
Configure::write('ASALAE_SIREN_ARCHIVE', '');
Configure::write('ASALAE_NUMERO_AGREMENT', '');

/**
 * Configuration des annuaires (OpenLDAP ou Active Directory)
 */
Configure::write('LdapManager.Ldap.use', false);
Configure::write('LdapManager.Ldap.type', '');
Configure::write('LdapManager.Ldap.host', '');
Configure::write('LdapManager.Ldap.host_fall_over', '');
Configure::write('LdapManager.Ldap.port', '');
Configure::write('LdapManager.Ldap.basedn', '');
Configure::write('LdapManager.Ldap.login', '');
Configure::write('LdapManager.Ldap.password', '');
Configure::write('LdapManager.Ldap.tls', false);
Configure::write('LdapManager.Ldap.version', 3);
Configure::write('LdapManager.Ldap.account_suffix', '');
Configure::write('LdapManager.Ldap.fields', [
  'User' =>
  [
    'username' => '',
    'note' => '',
    'nom' => '',
    'prenom' => '',
    'email' => '',
    'telfixe' => '',
    'telmobile' => '',
    'active' => '',
  ],
]);

Configure::write('AuthManager.Authentification.use', false);
Configure::write('AuthManager.Authentification.type', '');

Configure::write('AuthManager.Cas.host', '');
Configure::write('AuthManager.Cas.port', '');
Configure::write('AuthManager.Cas.uri', '');
Configure::write('AuthManager.Cas.cert_path', APP . 'Config/cert_cas/client.pem');
