<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppTestFixture', 'Test/Fixture');

/**
 * Classe AroFixture
 * Fixture 7.*
 * @package app.Test.Fixture
 */
class AroFixture extends AppTestFixture
{
    public $import = ['table' => 'aros', 'records' => false];

    /**
     * records property
     *
     * @var array
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'parent_id' => null,
                'model' => 'Profil',
                'foreign_key' => '1',
                'alias' => 'Administrateur',
                'lft' => 1,
                'rght' => 4,
            ],
            [
                'id' => 2,
                'parent_id' => null,
                'model' => 'Profil',
                'foreign_key' => '2',
                'alias' => 'Utilisateur',
                'lft' => 5,
                'rght' => 8,
            ],
            [
                'id' => 3,
                'parent_id' => null,
                'model' => 'Profil',
                'foreign_key' => '3',
                'alias' => 'Administrateur fonctionnel',
                'lft' => 9,
                'rght' => 12,
            ],
            [
                'id' => 4,
                'parent_id' => 1,
                'model' => 'User',
                'foreign_key' => 1,
                'alias' => '1',
                'lft' => 2,
                'rght' => 3,
            ],
            [
                'id' => 5,
                'parent_id' => 2,
                'model' => 'User',
                'foreign_key' => 2,
                'alias' => '2',
                'lft' => 6,
                'rght' => 7,
            ],
            [
                'id' => 6,
                'parent_id' => 3,
                'model' => 'User',
                'foreign_key' => 3,
                'alias' => '3',
                'lft' => 10,
                'rght' => 11,
            ],
        ];
        parent::init();
    }
}
