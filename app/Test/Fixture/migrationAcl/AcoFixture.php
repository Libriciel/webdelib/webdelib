<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppTestFixture', 'Test/Fixture');

/**
 * Classe AcoFixture
 * Fixture 7.*
 * @package app.Test.Fixture
 */
class AcoFixture extends AppTestFixture
{
    public $import = ['table' => 'acos', 'records' => false];

    /**
     * records property
     *
     * @var array
     */
    public function init()
    {
        $this->records = [
            ['id' => 1, 'parent_id' => null, 'model' => null, 'foreign_key' => null, 'alias' => 'controllers', 'lft' => 1, 'rght' => 136],
            ['id' => 2, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Actes', 'lft' => 82, 'rght' => 83],
            ['id' => 3, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Acteurs', 'lft' => 124, 'rght' => 125],
            ['id' => 4, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Collectivites', 'lft' => 88, 'rght' => 89],
            ['id' => 5, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Compteurs', 'lft' => 100, 'rght' => 101],
            ['id' => 6, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Connecteurs', 'lft' => 130, 'rght' => 131],
            ['id' => 7, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Crons', 'lft' => 132, 'rght' => 133],
            ['id' => 8, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Deliberations', 'lft' => 2, 'rght' => 63],
            ['id' => 9, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'retour', 'lft' => 39, 'rght' => 40],
            ['id' => 10, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'duplicate', 'lft' => 29, 'rght' => 30],
            ['id' => 11, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'mesProjetsATraiter', 'lft' => 5, 'rght' => 6],
            ['id' => 12, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'mesProjetsValidation', 'lft' => 3, 'rght' => 4],
            ['id' => 13, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'mesProjetsValides', 'lft' => 7, 'rght' => 8],
            ['id' => 14, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'projetsMonService', 'lft' => 9, 'rght' => 10],
            ['id' => 15, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'tousLesProjetsValidation', 'lft' => 13, 'rght' => 14],
            ['id' => 16, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'tousLesProjetsSansSeance', 'lft' => 11, 'rght' => 12],
            ['id' => 17, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'tousLesProjetsAFaireVoter', 'lft' => 15, 'rght' => 16],
            ['id' => 18, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'validerEnUrgence', 'lft' => 43, 'rght' => 44],
            ['id' => 19, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'sendToParapheur', 'lft' => 47, 'rght' => 48],
            ['id' => 20, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'goNext', 'lft' => 37, 'rght' => 38],
            ['id' => 21, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'rebond', 'lft' => 41, 'rght' => 42],
            ['id' => 22, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'autresActesAValider', 'lft' => 17, 'rght' => 18],
            ['id' => 23, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'sendActesToSignature', 'lft' => 45, 'rght' => 46],
            ['id' => 24, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'autresActesValides', 'lft' => 19, 'rght' => 20],
            ['id' => 25, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'autresActesAEnvoyer', 'lft' => 21, 'rght' => 22],
            ['id' => 26, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'nonTransmis', 'lft' => 25, 'rght' => 26],
            ['id' => 27, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'autresActesEnvoyes', 'lft' => 23, 'rght' => 24],
            ['id' => 28, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'autresActesCancelSignature', 'lft' => 57, 'rght' => 58],
            ['id' => 29, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'deliberationCancelSignature', 'lft' => 49, 'rght' => 50],
            ['id' => 30, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'autresActesCancelSendToTDT', 'lft' => 59, 'rght' => 60],
            ['id' => 31, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'deliberationCancelSendToTDT', 'lft' => 51, 'rght' => 52],
            ['id' => 32, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'autresActesAbandon', 'lft' => 55, 'rght' => 56],
            ['id' => 33, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'tousLesProjetsRecherche', 'lft' => 27, 'rght' => 28],
            ['id' => 34, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'reattribuerTous', 'lft' => 31, 'rght' => 32],
            ['id' => 35, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'editerTous', 'lft' => 33, 'rght' => 34],
            ['id' => 36, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'supprimerTous', 'lft' => 35, 'rght' => 36],
            ['id' => 37, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'ExportGedAutresActes', 'lft' => 86, 'rght' => 87],
            ['id' => 38, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'ExportGedSeances', 'lft' => 84, 'rght' => 85],
            ['id' => 39, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Historiques', 'lft' => 134, 'rght' => 135],
            ['id' => 40, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Infosupdefs', 'lft' => 128, 'rght' => 129],
            ['id' => 41, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Postseances', 'lft' => 72, 'rght' => 81],
            ['id' => 42, 'parent_id' => 41, 'model' => null, 'foreign_key' => null, 'alias' => 'sendToSae', 'lft' => 77, 'rght' => 78],
            ['id' => 43, 'parent_id' => 41, 'model' => null, 'foreign_key' => null, 'alias' => 'toSend', 'lft' => 73, 'rght' => 74],
            ['id' => 44, 'parent_id' => 41, 'model' => null, 'foreign_key' => null, 'alias' => 'transmit', 'lft' => 75, 'rght' => 76],
            ['id' => 45, 'parent_id' => 41, 'model' => null, 'foreign_key' => null, 'alias' => 'signature', 'lft' => 79, 'rght' => 80],
            ['id' => 46, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Profils', 'lft' => 112, 'rght' => 113],
            ['id' => 47, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Seances', 'lft' => 64, 'rght' => 69],
            ['id' => 48, 'parent_id' => 47, 'model' => null, 'foreign_key' => null, 'alias' => 'calendrier', 'lft' => 65, 'rght' => 66],
            ['id' => 49, 'parent_id' => 47, 'model' => null, 'foreign_key' => null, 'alias' => 'sendToIdelibre', 'lft' => 67, 'rght' => 68],
            ['id' => 50, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Sequences', 'lft' => 98, 'rght' => 99],
            ['id' => 51, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Services', 'lft' => 114, 'rght' => 115],
            ['id' => 52, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'TableauDeBord', 'lft' => 70, 'rght' => 71],
            ['id' => 53, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Themes', 'lft' => 90, 'rght' => 91],
            ['id' => 54, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Typeactes', 'lft' => 102, 'rght' => 103],
            ['id' => 55, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Typeacteurs', 'lft' => 126, 'rght' => 127],
            ['id' => 56, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Typeseances', 'lft' => 104, 'rght' => 105],
            ['id' => 57, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Users', 'lft' => 106, 'rght' => 111],
            ['id' => 58, 'parent_id' => 57, 'model' => null, 'foreign_key' => null, 'alias' => 'changeUserMdp', 'lft' => 107, 'rght' => 108],
            ['id' => 59, 'parent_id' => 57, 'model' => null, 'foreign_key' => null, 'alias' => 'changeFormatSortie', 'lft' => 109, 'rght' => 110],
            ['id' => 60, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Cakeflow', 'lft' => 116, 'rght' => 123],
            ['id' => 61, 'parent_id' => 60, 'model' => null, 'foreign_key' => null, 'alias' => 'Circuits', 'lft' => 117, 'rght' => 118],
            ['id' => 62, 'parent_id' => 60, 'model' => null, 'foreign_key' => null, 'alias' => 'Compositions', 'lft' => 121, 'rght' => 122],
            ['id' => 63, 'parent_id' => 60, 'model' => null, 'foreign_key' => null, 'alias' => 'Etapes', 'lft' => 119, 'rght' => 120],
            ['id' => 64, 'parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'ModelOdtValidator', 'lft' => 92, 'rght' => 97],
            ['id' => 65, 'parent_id' => 64, 'model' => null, 'foreign_key' => null, 'alias' => 'Modeltemplates', 'lft' => 93, 'rght' => 94],
            ['id' => 66, 'parent_id' => 64, 'model' => null, 'foreign_key' => null, 'alias' => 'Modelvalidations', 'lft' => 95, 'rght' => 96],
            ['id' => 68, 'parent_id' => null, 'model' => 'Service', 'foreign_key' => 1, 'alias' => 'Informatique', 'lft' => 137, 'rght' => 138],
            ['id' => 69, 'parent_id' => null, 'model' => 'Typeacte', 'foreign_key' => 2, 'alias' => 'délibération', 'lft' => 139, 'rght' => 140],
            ['id' => 70, 'parent_id' => null, 'model' => 'Typeseance', 'foreign_key' => 1, 'alias' => 'Conseil Municipalé  tr', 'lft' => 141, 'rght' => 142],
            ['id' => 71, 'parent_id' => null, 'model' => 'Service', 'foreign_key' => 2, 'alias' => 'Service des assemblées ', 'lft' => 143, 'rght' => 144],
            ['id' => 72, 'parent_id' => null, 'model' => 'Circuit', 'foreign_key' => 1, 'alias' => 'Circuit de délibération', 'lft' => 145, 'rght' => 146],
            ['id' => 73, 'parent_id' => null, 'model' => 'Circuit', 'foreign_key' => 2, 'alias' => 'Circuit arrêté', 'lft' => 147, 'rght' => 148],
            ['id' => 74, 'parent_id' => null, 'model' => 'Typeacte', 'foreign_key' => 3, 'alias' => 'Arrété', 'lft' => 149, 'rght' => 150],
            ['id' => 76, 'parent_id' => null, 'model' => 'Typeacte', 'foreign_key' => 4, 'alias' => 'Décision', 'lft' => 151, 'rght' => 152],
            ['id' => 77, 'parent_id' => null, 'model' => 'Typeseance', 'foreign_key' => 3, 'alias' => 'Commission Finance', 'lft' => 153, 'rght' => 154],
            ['id' => 78, 'parent_id' => null, 'model' => 'Typeacte', 'foreign_key' => 5, 'alias' => 'Arrêté Transmissible', 'lft' => 155, 'rght' => 156],
            ['id' => 79, 'parent_id' => null, 'model' => 'Service', 'foreign_key' => 3, 'alias' => 'DGS', 'lft' => 157, 'rght' => 158],
            ['id' => 80, 'parent_id' => null, 'model' => 'Service', 'foreign_key' => 4, 'alias' => 'Service des assemblées ', 'lft' => 159, 'rght' => 160],
            ['id' => 81, 'parent_id' => null, 'model' => 'Service', 'foreign_key' => 5, 'alias' => 'Service des assemblées ', 'lft' => 161, 'rght' => 162],
            ['id' => 82, 'parent_id' => null, 'model' => 'Circuit', 'foreign_key' => 3, 'alias' => 'Délibération réinsère', 'lft' => 163, 'rght' => 164],
            ['id' => 83, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'sendToPublish', 'lft' => 61, 'rght' => 62],
            ['id' => 84, 'parent_id' => 8, 'model' => null, 'foreign_key' => null, 'alias' => 'sendToPublishDeliberation', 'lft' => 53, 'rght' => 54],
            ['id' => 85, 'parent_id' => null, 'model' => 'Typeacte', 'foreign_key' => 6, 'alias' => 'Délib 2', 'lft' => 165, 'rght' => 166],
            ['id' => 86, 'parent_id' => null, 'model' => 'Typeseance', 'foreign_key' => 4, 'alias' => 'bureau', 'lft' => 167, 'rght' => 168],
            ['id' => 87, 'parent_id' => null, 'model' => 'Service', 'foreign_key' => 6, 'alias' => 'Service des assemblées 2', 'lft' => 169, 'rght' => 170],
            ['id' => 88, 'parent_id' => null, 'model' => 'Circuit', 'foreign_key' => 4, 'alias' => 'Délibération réinsère - copie', 'lft' => 171, 'rght' => 172],
            ['id' => 89, 'parent_id' => null, 'model' => 'Typeacte', 'foreign_key' => 7, 'alias' => 'note', 'lft' => 173, 'rght' => 174],
            ['id' => 90, 'parent_id' => null, 'model' => 'Typeacte', 'foreign_key' => 8, 'alias' => 'test 2', 'lft' => 175, 'rght' => 176],
            ['id' => 91, 'parent_id' => null, 'model' => 'Service', 'foreign_key' => 7, 'alias' => 'test', 'lft' => 177, 'rght' => 180],
            ['id' => 92, 'parent_id' => null, 'model' => 'Service', 'foreign_key' => 8, 'alias' => 'test service assemblé', 'lft' => 181, 'rght' => 182],
            ['id' => 93, 'parent_id' => 103, 'model' => 'Service', 'foreign_key' => 9, 'alias' => 'Groupement ressources humaines', 'lft' => 184, 'rght' => 201],
            ['id' => 94, 'parent_id' => 93, 'model' => 'Service', 'foreign_key' => 10, 'alias' => 'Administrateur SIRH', 'lft' => 185, 'rght' => 186],
            ['id' => 95, 'parent_id' => 93, 'model' => 'Service', 'foreign_key' => 11, 'alias' => 'Direction', 'lft' => 187, 'rght' => 188],
            ['id' => 96, 'parent_id' => 93, 'model' => 'Service', 'foreign_key' => 12, 'alias' => 'Assitante GRH', 'lft' => 189, 'rght' => 190],
            ['id' => 97, 'parent_id' => 93, 'model' => 'Service', 'foreign_key' => 13, 'alias' => 'Chargée de missions transverses ', 'lft' => 191, 'rght' => 192],
            ['id' => 98, 'parent_id' => 93, 'model' => 'Service', 'foreign_key' => 14, 'alias' => 'Service GPEEC', 'lft' => 193, 'rght' => 194],
            ['id' => 99, 'parent_id' => 93, 'model' => 'Service', 'foreign_key' => 15, 'alias' => 'Service carrière ', 'lft' => 195, 'rght' => 196],
            ['id' => 100, 'parent_id' => 93, 'model' => 'Service', 'foreign_key' => 16, 'alias' => 'Service gestion sociale des personnels ', 'lft' => 197, 'rght' => 198],
            ['id' => 101, 'parent_id' => 93, 'model' => 'Service', 'foreign_key' => 17, 'alias' => 'Service logement', 'lft' => 199, 'rght' => 200],
            ['id' => 102, 'parent_id' => 91, 'model' => 'Service', 'foreign_key' => 18, 'alias' => 'Service rémunération', 'lft' => 178, 'rght' => 179],
            ['id' => 103, 'parent_id' => null, 'model' => 'Service', 'foreign_key' => 19, 'alias' => 'Direction administrative et financière modification', 'lft' => 183, 'rght' => 202],
        ];
        parent::init();
    }
}
