<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppTestFixture', 'Test/Fixture');

/**
 * Classe CompteurFixture
 *
 *
 * @package app.Test.Fixture
 *
 * @version 5.1.1
 * @since 4.4
 */
class CronFixture extends AppTestFixture
{
    public $import = ['connection' => 'webdelib_db1', 'table' => 'crons', 'records' => false];
    /**
     * fields property
     *
     * @var array
     */
    public $fields = [
        'id' => ['type' => 'integer', 'key' => 'primary'],
        'nom' => ['type' => 'string', 'null' => false],
        'description' => ['type' => 'string', 'null' => true],
        'plugin' => ['type' => 'string', 'null' => false],
        'action' => ['type' => 'string', 'null' => true],
        'has_params' => ['type' => 'boolean', 'null' => false],
        'params' => ['type' => 'string', 'null' => false],
        'next_execution_time' => 'datetime',
        'execution_duration' => ['type' => 'string', 'null' => false],
        'last_execution_start_time' => 'datetime',
        'last_execution_end_time' => 'datetime',
        'last_execution_report' => ['type' => 'string', 'null' => false],
        'last_execution_status' => ['type' => 'string', 'null' => false],
        'active' => ['type' => 'boolean', 'null' => false],
        'created_user_id' => ['type' => 'integer', 'null' => false],
        'modified_user_id' => ['type' => 'integer', 'null' => false],
        'model' => ['type' => 'string', 'null' => false],
        'lock' => ['type' => 'boolean', 'null' => false],
        'run_all' => ['type' => 'boolean', 'null' => false],
        'created' => 'datetime',
        'modified' => 'datetime'
    ];

    public function init()
    {
        $this->records = [
              [
                'id' => 5,
                'nom' => 'TDT : Mise à jour des accusés de réception',
                'description' => 'Vérifie la réception par la prefecture des dossiers envoyés via le TDT et dans le cas échéant, enregistre la date de l\'accusé de réception et le bordereau',
                'plugin' => '',
                'action' => 'majTdtAr',
                'has_params' => false,
                'params' => '',
                'next_execution_time' => null,
                'execution_duration' => 'PT5M',
                'last_execution_start_time' => null,
                'last_execution_end_time' => null,
                'last_execution_report' => '',
                'last_execution_status' => 'SUCCES',
                'active' => true,
                'created_user_id' => 1,
                'modified_user_id' => 1,
                'model' => 'Cron',
                'lock' => false,
                'run_all' => false,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
              ],
              [
               'id' => 7,
               'nom' => 'CONVERSION : conversion des fichiers',
               'description' => 'Conversion des fichiers dans différents formats pour la fusion ou autre télétransmission',
               'plugin' => '',
               'action' => 'conversionFiles',
               'has_params' => false,
               'params' => '',
               'next_execution_time' => null,
               'execution_duration' => 'PT15M',
               'last_execution_start_time' => null,
               'last_execution_end_time' => null,
               'last_execution_report' => '',
               'last_execution_status' => 'SUCCES',
               'active' => true,
               'created_user_id' => 1,
               'modified_user_id' => 1,
               'model' => 'Cron',
               'lock' => false,
               'run_all' => false,
               'created' => date('Y-m-d H:i:s'),
               'modified' => date('Y-m-d H:i:s')
             ],
         ];

        parent::init();
    }
}
