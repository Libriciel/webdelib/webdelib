<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppTestFixture', 'Test/Fixture');

/**
 * Classe AnnexeFixture
 *
 * @version 4.4
 * @package app.Test.Fixture
 */
class AnnexeFixture extends AppTestFixture
{
    /**
     * [public description]
     * @var [type]
     */
    public $import = ['connection' => 'webdelib_db1', 'table' => 'annexes', 'records' => false];

    /**
     * Définition des enregistrements.
     *
     * @var array
     */
    public function init()
    {
        $this->records = [
          [
              'id' => 1,
              'model' => 'Projet',
              'foreign_key' => 1,
              'position' => 1,
              'joindre_ctrl_legalite' => true,
              'titre' => 'Annexe 1',
              'filename' => 'AnnexeFixture_1_1.pdf',
              'filetype' => 'application/pdf',
              'size' => 501044,
              'data' => '',
              'data_pdf' => null,
              'created' => date('Y-m-d H:i:s'),
              'modified' => date('Y-m-d H:i:s'),
              'joindre_fusion' => true,
              'edition_data' => null,
              'edition_data_typemime' => null
          ],
          [
              'id' => 2,
              'model' => 'Projet',
              'foreign_key' => 1,
              'position' => 2,
              'joindre_ctrl_legalite' => true,
              'titre' => 'Annexe 2',
              'filename' => 'AnnexeFixture_1_2.pdf',
              'filetype' => 'application/pdf',
              'size' => 501044,
              'data' => '',
              'data_pdf' => null,
              'created' => date('Y-m-d H:i:s'),
              'modified' => date('Y-m-d H:i:s'),
              'joindre_fusion' => false,
              'edition_data' => null,
              'edition_data_typemime' => null
          ],
          [
              'id' => 3,
              'model' => 'Projet',
              'foreign_key' => 1,
              'position' => 3,
              'joindre_ctrl_legalite' => true,
              'titre' => 'Annexe 3',
              'filename' => 'AnnexeFixture_1_3.pdf',
              'filetype' => 'application/pdf',
              'size' => 501044,
              'data' => '',
              'data_pdf' => null,
              'created' => date('Y-m-d H:i:s'),
              'modified' => date('Y-m-d H:i:s'),
              'joindre_fusion' => false,
              'edition_data' => null,
              'edition_data_typemime' => null
          ],
          [
              'id' => 4,
              'model' => 'Projet',
              'foreign_key' => 1,
              'position' => 4,
              'joindre_ctrl_legalite' => true,
              'titre' => 'Annexe 4',
              'filename' => 'AnnexeFixture_1_4.pdf',
              'filetype' => 'application/pdf',
              'size' => 501044,
              'data' => '',
              'data_pdf' => null,
              'created' => date('Y-m-d H:i:s'),
              'modified' => date('Y-m-d H:i:s'),
              'joindre_fusion' => false,
              'edition_data' => null,
              'edition_data_typemime' => null
          ]
        ];

        parent::init();
    }
}
