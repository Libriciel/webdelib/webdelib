<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * Classe ModeltypeFixture
 *
 * @version 4.4
 * @package app.Test.Fixture.Plugin.ModelOdtValidator
 */
class ModeltypeFixture extends AppTestFixture
{

    /**
     * On importe la définition de la table et les enregistrements.
     *
     * @var array
     */
    public $import = ['connection' => 'webdelib_db1', 'table' => 'modeltypes', 'records' => false];

    /**
     * fields property
     *
     * @var array
     */
    public $fields = [
        'id' => ['type' => 'integer', 'key' => 'primary'],
        'name' => ['type' => 'string', 'null' => true],
        'description' => ['type' => 'string', 'null' => true],
        'created' => 'datetime',
        'modified' => 'datetime',
    ];

    /**
     * records property
     *
     * @var array
     */
    public function init()
    {
        // FIX
        $this->records = [
            [
                'id' => 1,
                'name' => 'Toutes éditions',
                'description' => 'Toutes éditions',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 2,
                'name' => 'Projet',
                'description' => 'Projet',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 3,
                'name' => 'Délibération',
                'description' => 'Délibération',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 4,
                'name' => 'Convocation',
                'description' => 'Convocation',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 5,
                'name' => 'Ordre du jour',
                'description' => 'Ordre du jour',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 6,
                'name' => 'PV sommaire',
                'description' => 'PV sommaire',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 7,
                'name' => 'PV détaillé',
                'description' => 'PV détaillé',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 8,
                'name' => 'Recherche',
                'description' => 'Recherche',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 9,
                'name' => 'Multi-séance',
                'description' => 'Multi-séance',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 10,
                'name' => 'Bordereau de projet',
                'description' => 'Bordereau de projet',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 11,
                'name' => 'Journal de séance',
                'description' => 'Journal de séance',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
        ];

        parent::init();
    }
}
