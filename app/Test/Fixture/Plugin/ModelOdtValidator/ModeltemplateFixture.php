<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * Classe ModeltemplateFixture
 *
 * @version 4.4
 * @package app.Test.Fixture.Plugin.ModelOdtValidator
 */
class ModeltemplateFixture extends AppTestFixture
{

    /**
     * On importe la définition de la table et les enregistrements.
     *
     * @var array
     */
    public $import = ['connection' => 'webdelib_db1', 'table' => 'modeltemplates', 'records' => false];

    /**
     * fields property
     *
     * @var array
     */
    public $fields = [
        'id' => ['type' => 'integer', 'key' => 'primary'],
        'modeltype_id' => ['type' => 'integer', 'null' => true],
        'name' => ['type' => 'string', 'null' => true],
        'filename' => ['type' => 'string', 'null' => true],
        'filesize' => ['type' => 'integer', 'null' => true],
        'content' => ['type' => 'binary', 'null' => true],
        'created' => 'datetime',
        'modified' => 'datetime',
        'file_output_format' => ['type' => 'string', 'null' => true],
        'force' => ['type' => 'boolean', 'null' => true],
    ];

    /**
     * records property
     *
     * @var array
     */
    public function init()
    {
        // FIX
        $this->records = [
            [
                'id' => 1,
                'modeltype_id' => 1,
                'name' => 'Toutes éditions',
                'filename' => 'toutes_edition.odt',
                'filesize' => '123456',
                'content' => '',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'file_output_format' => null,
                'force' => null,
            ],
            [
                'id' => 2,
                'modeltype_id' => 2,
                'name' => 'Modele de Projet',
                'filename' => 'projet.odt',
                'filesize' => '123456',
                'content' => '',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'file_output_format' => null,
                'force' => null,
            ],
            [
                'id' => 3,
                'modeltype_id' => 3,
                'name' => 'Modele de Délibération',
                'filename' => 'deliberation.odt',
                'filesize' => '123456',
                'content' => '',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'file_output_format' => null,
                'force' => null,
            ],
        ];

        parent::init();
    }
}
