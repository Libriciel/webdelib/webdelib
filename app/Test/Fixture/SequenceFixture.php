<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppTestFixture', 'Test/Fixture');

/**
 * Classe SequenceFixture
 *
 * @version 4.4
 * @package app.Test.Fixture
 */
class SequenceFixture extends AppTestFixture
{
    public $import = ['connection' => 'webdelib_db1', 'table' => 'sequences', 'records' => true];
}
