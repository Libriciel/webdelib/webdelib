<?php

App::uses('CakeTestFixture', 'TestSuite/Fixture');

class AppTestFixture extends CakeTestFixture
{

    /**
    * Base de données par défaut utilisée pour les tests
    *
    * @var string
    */
    //public $useDbConfig = 'test_my_other_db';  // Ta configuration de base de données de test par défaut

    public function __construct()
    {
        if ($this->name === null) {
            if (preg_match('/^(.*)Fixture$/', get_class($this), $matches)) {
                $this->name = $matches[1];
            } else {
                $this->name = get_class($this);
            }
        }
        $connection = 'test';
        if (!empty($this->useDbConfig)) {
            $connection = $this->useDbConfig;
            if (strpos($connection, 'test') !== 0) {
                $message = __d(
                    'cake_dev',
                    'Invalid datasource name "%s" for "%s" fixture. Fixture datasource names must begin with "test".',
                    $connection,
                    $this->name
                );
                throw new CakeException($message);
            }
        }
        $this->Schema = new CakeSchema(array('name' => 'TestSuite', 'connection' => $connection));
        $this->init();
    }

    /**
    * Constructeur de la fixture.
    */
    public function init()
    {
        if (isset($this->import) && (is_string($this->import) || is_array($this->import))) {
            $import = array_merge(
                ['connection' => 'webdelib_db1', 'records' => false],
                is_array($this->import) ? $this->import : ['model' => $this->import]
            );
            $this->Schema->connection = $import['connection'];

            if (isset($import['model'])) {
                list($plugin, $modelClass) = pluginSplit($import['model'], true);
                App::uses($modelClass, $plugin . 'Model');
                if (!class_exists($modelClass)) {
                    throw new MissingModelException(['class' => $modelClass]);
                }
                $model = new $modelClass(null, null, $import['connection']);
               // $model->useDbConfig = $import['connection'];
               // $model->setDataSource($import['connection']);
                $db = $model->getDataSource();
                if (empty($model->tablePrefix)) {
                    $model->tablePrefix = $db->config['prefix'];
                }
                $this->fields = $model->schema(true);
                $this->fields[$model->primaryKey]['key'] = 'primary';
                $this->table = $db->fullTableName($model, false, false);
                $this->primaryKey = $model->primaryKey;
                ClassRegistry::config(['ds' => 'test']);
                ClassRegistry::flush();
            } elseif (isset($import['table'])) {
                $model = new Model(null, $import['table'], $import['connection']);
                $db = ConnectionManager::getDataSource($import['connection']);
                $db->cacheSources = false;
                $model->useDbConfig = $import['connection'];
                $model->name = Inflector::camelize(Inflector::singularize($import['table']));
                $model->table = $import['table'];
                $model->tablePrefix = $db->config['prefix'];
                $this->fields = $model->schema(true);
                $this->primaryKey = $model->primaryKey;
                ClassRegistry::flush();
            }
            $binaryFieldNames = [];
            foreach ($this->fields as $fieldName => $field) {
                if ($field['type']==='binary') {
                    $binaryFieldNames[] = $fieldName;
                }
            }
            if (!empty($db->config['prefix']) && strpos($this->table, $db->config['prefix']) === 0) {
                $this->table = str_replace($db->config['prefix'], '', $this->table);
            }

            if (isset($import['records']) && $import['records'] !== false && isset($model) && isset($db)) {
                $this->records = [];
                $query = [
                    'fields' => $db->fields($model, null, array_keys($this->fields)),
                    'table' => $db->fullTableName($model),
                    'alias' => $model->alias,
                    'conditions' => array(),
                    'order' => null,
                    'limit' => null,
                    'group' => null
                ];

                $records = $db->fetchAll($db->buildStatement($query, $model), false, $model->alias);

                foreach ($records as &$record) {
                    foreach ($record as $modelName => &$modelRecord) {
                        foreach ($modelRecord as $fieldName => &$field) {
                            if (in_array($fieldName, $binaryFieldNames, true)) {
                                $field = '';
                            }
                        }
                    }
                }
                if ($records !== false && !empty($records)) {
                    $this->records = Hash::extract($records, '{n}.' . $model->alias);
                }
            }
        }

        if (!isset($this->table)) {
            $this->table = Inflector::underscore(Inflector::pluralize($this->name));
            //FIX
            if ($this->name === 'DeliberationUser') {
                $this->table = 'deliberations_users';
            }
            if ($this->name === 'DeliberationSeance') {
                $this->table = 'deliberations_seances';
            }
            if ($this->name === 'ActeurService') {
                $this->table = 'acteurs_services';
            }
            if ($this->name === 'NatureTypologiepiece') {
                $this->table = 'natures_typologiepieces';
            }
            if ($this->name === 'ListepresenceGlobale') {
                $this->table = 'liste_presences_globales';
            }
            //$this->table = Inflector::camelize(Inflector::singularize($import['table']));
            //$this->table = Inflector::underscore(Inflector::pluralize($this->name));
        }

        if (!isset($this->primaryKey) && isset($this->fields['id'])) {
            $this->primaryKey = 'id';
        }
    }
}
