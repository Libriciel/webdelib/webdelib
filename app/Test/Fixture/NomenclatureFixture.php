<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppTestFixture', 'Test/Fixture');

/**
 * Classe NomenclatureFixture
 *
 * @version 4.4
 * @package app.Test.Fixture
 */
class NomenclatureFixture extends AppTestFixture
{

    /**
     * On importe la définition de la table et les enregistrements.
     *
     * @var array
     */
    public $import = ['connection' => 'webdelib_db1', 'table' => 'nomenclatures', 'records' => false];

    /**
     * records property
     *
     * @var array
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'parent_id' => null,
                'name' => '1',
                'libelle' => '1 Commande Publique',
                'lft' => 1,
                'rght' => 3,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 2,
                'parent_id' => 1,
                'name' => '1.1',
                'libelle' => '1.1 Marchés publics',
                'lft' => 2,
                'rght' => 3,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 3,
                'parent_id' => 2,
                'name' => '1.1.1',
                'libelle' => '1.1.1 Délégation',
                'lft' => 3,
                'rght' => 3,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]

        ];
        parent::init();
    }
}
