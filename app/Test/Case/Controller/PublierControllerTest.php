<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * Classe DeliberationsControllerTest.
 *
 * @version 4.3
 * @package app.Test.Case.Controller
 */
class PublierControllerTest extends ControllerTestCase
{

    /**
     * Fixtures associated with this test case
     *
     * @version 4.3
     * @var array
     */
    public $fixtures = [
//        //ACL
//        'app.Aro',
//        'app.Aco',
//        'app.AroAco',
        //Génération
        'app.Deliberation',
        'app.DeliberationSeance',
        'app.Seance',
        'app.Typeseance',
        'app.DeliberationTypeseance',
        'app.Typeacte',
        'app.Collectivite',
        'app.Theme',
        'app.Nomenclature',
        'app.Listepresence',
        'app.Vote',
        'app.Annexe',
        'app.Historique',
        'app.User',
    ];

    public function setUp()
    {
        parent::setUp();
        // Simuler le HTTP_HOST
        $_SERVER['HTTP_HOST'] = 'localhost';
        Configure::write('Config.database', 'test');

        Configure::write('PUBLICATION_PREF_ID', 'PREF34');
        Configure::write('PUBLICATION_PERMALIEN', 'https://www.libriciel.fr');

        $Publier = $this->generate('Publier', [
            'components' => [
                'Acl',
                'Publication',
                'PDFStamp',
            ]
        ]);
        $Publier->Acl->method('check')->willReturn(true);
    }

    /**
     * Méthode exécutée après chaque test.
     * @return void
     */
    public function tearDown()
    {
        unset($_SERVER['HTTP_HOST']);
        parent::tearDown();
    }


    /**
     * Test de l'ajout dans un circuit avec l'optimisation d'activé.
     *
     * @return void
     * @throws JsonException
     * @version 4.3
     */
    public function testDownloadJson()
    {
        $result = $this->testAction('/publier/download/8');

        $file = new File(TMP . 'files' . DS . 'download.zip', true);
        $file->write($result);
        $resultFileMime = $file->mime();

        $this->assertFileExists($file->pwd(), $file->pwd());
        $this->assertEquals('application/zip', $resultFileMime, var_export($resultFileMime, true));

        $zip = new ZipArchive;
        if (!$zip->open($file->pwd())) {
            $this->fail('the file zip does not open');
        }

        //Bug lib zip
        $jsonContents = $zip->getFromName('DEL02.json');

        $jsonExpected = [
            "COLL_NOM" => "webdelib",
            "COLL_SIRET" => "49101169800033",
            "DELIB_ID" => "DEL02",
            "DELIB_DATE" => date('Y-m-d'),
            "DELIB_MATIERE_CODE" => "1.1",
            "DELIB_MATIERE_NOM" => "commande publique/marchés publics",
            "DELIB_OBJET" => chr(34) . "Délibération séance passée position 2" . chr(34),
            "BUDGET_ANNEE" => "",
            "BUDGET_NOM" => "",
            "PREF_ID" => "PREF34",
            "PREF_DATE" => "",
            "VOTE_EFFECTIF" => 0,
            "VOTE_REEL" => 0,
            "VOTE_POUR" => 0,
            "VOTE_CONTRE" => 0,
            "VOTE_ABSTENTION" => 0,
            "DELIB_URL" => "https://www.libriciel.fr/DEL02.pdf"
        ];

        $this->assertEquals(
            json_decode($jsonContents, true, 512, JSON_THROW_ON_ERROR),
            $jsonExpected,
            var_export($jsonContents, true)
        );

        $zip->close();
        $file->close();
    }

    /**
     * Test de l'ajout dans un circuit avec l'optimisation d'activé.
     *
     * @version 4.3
     * @return void
     */
    public function testDownloadCsv()
    {

        $result = $this->testAction('/publier/download/8');

        $file = new File(TMP . 'files' . DS . 'download.zip', true);
        $file->write($result);
        $resultFileMime = $file->mime();

        $this->assertFileExists($file->pwd(), $file->pwd());
        $this->assertEquals('application/zip', $resultFileMime, var_export($resultFileMime, true));

        $zip = new ZipArchive;
        if (!$zip->open($file->pwd())) {
            $this->fail('the file zip does not open');
        }

        $csvContents = $zip->getFromName('DEL02.csv');
        $csvExpected = "COLL_NOM,COLL_SIRET,DELIB_ID,DELIB_DATE,DELIB_MATIERE_CODE,DELIB_MATIERE_NOM,DELIB_OBJET,"
            . "BUDGET_ANNEE,BUDGET_NOM,PREF_ID,PREF_DATE,VOTE_EFFECTIF,VOTE_REEL,VOTE_POUR,"
            ."VOTE_CONTRE,VOTE_ABSTENTION,DELIB_URL"
            .CHR(13). CHR(10)
            . "webdelib,49101169800033,DEL02,".date('Y-m-d').",1.1,commande publique/marchés publics,"
            . chr(34) . "Délibération séance passée position 2" . chr(34)
            .",,,PREF34,,0,0,0,0,0,https://www.libriciel.fr/DEL02.pdf";
        $this->assertEquals($csvContents, $csvExpected, var_export($csvExpected, true));

        $zip->close();
        $file->close();
    }
}
