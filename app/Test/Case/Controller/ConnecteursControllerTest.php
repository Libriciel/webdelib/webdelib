<?php
App::uses('AppController', 'Controller');
App::uses('ConnecteursController', 'Controller');
App::uses('ControllerTrait', 'Test/Case/Controller');

/**
 * vendors/bin/cake -app app test app/Test/Case/Controller/ConnecteursControllerTest.php
 */
class ConnecteursControllerTest extends ControllerTestCase
{
    use ControllerTrait;

    public function setUp()
    {
        parent::setUp();
        // Simuler le HTTP_HOST
        $_SERVER['HTTP_HOST'] = 'localhost';
        Configure::write('Config.database', 'test');
        $this->Collectivite = ClassRegistry::init('Collectivite');
    }

    /**
     * Méthode exécutée après chaque test.
     * @return void
     */
    public function tearDown()
    {
        unset($_SERVER['HTTP_HOST']);
        parent::tearDown();
    }

    protected function getConfig()
    {
        $config = <<<'EOD'
/* Configuration pour Tests */
Configure::write('CONNECTEURS_CONTROLLER_TEST_HOST', '');
Configure::write('CONNECTEURS_CONTROLLER_TEST_LOGIN', '');
Configure::write('CONNECTEURS_CONTROLLER_TEST_PWD', '');
Configure::write('CONNECTEURS_CONTROLLER_TEST_USERNAME', '');
Configure::write('CONNECTEURS_CONTROLLER_TEST_PASSWORD', '\\$foo');
Configure::write('CONNECTEURS_CONTROLLER_TEST_PROXYHOST', '');
Configure::write('CONNECTEURS_CONTROLLER_TEST_X_AUTH_TOKEN', '');
EOD;
        eval($config);

        return $config;
    }

    /**
     * @dataProvider dataTestReplaceValue
     */
    public function testReplaceValue($param, $new_value, array $expectedOriginal, array $expectedChanged)
    {
        $controller = $this->generate('Connecteurs');
        $config = $this->getConfig();

        $method = new ReflectionMethod('ConnecteursController', 'replaceValue');
        $method->setAccessible(true);
        $result = $method->invoke($controller, $config, $param, $new_value);

        $original = array_diff(explode("\n", $config), explode("\n", $result));
        $changed = array_diff(explode("\n", $result), explode("\n", $config));

        $this->assertEquals($expectedOriginal, $original);
        $this->assertEquals($expectedChanged, $changed);
    }

    public function dataTestReplaceValue()
    {
        return [
            [
                'CONNECTEURS_CONTROLLER_TEST_PWD',
                'foobarbaz',
                [3 => "Configure::write('CONNECTEURS_CONTROLLER_TEST_PWD', '');"],
                [3 => "Configure::write('CONNECTEURS_CONTROLLER_TEST_PWD', 'foobarbaz');"],
            ],
            [
                'CONNECTEURS_CONTROLLER_TEST_PWD',
                '/&à}$-foo"\'\\`bar',
                [3 => "Configure::write('CONNECTEURS_CONTROLLER_TEST_PWD', '');"],
                [3 => "Configure::write('CONNECTEURS_CONTROLLER_TEST_PWD', '/&à}$-foo\"\'\\\\`bar');"],
            ],
            [
                'CONNECTEURS_CONTROLLER_TEST_PWD',
                'foo#!\$bar%;?\\`~bazàé&',
                [3 => "Configure::write('CONNECTEURS_CONTROLLER_TEST_PWD', '');"],
                [3 => "Configure::write('CONNECTEURS_CONTROLLER_TEST_PWD', 'foo#!\\\\\$bar%;?\\\\`~bazàé&');"],
            ],
            [
                'CONNECTEURS_CONTROLLER_TEST_PASSWORD',
                'foobarbaz',
                [5 => "Configure::write('CONNECTEURS_CONTROLLER_TEST_PASSWORD', '\\\\\$foo');"],
                [5 => "Configure::write('CONNECTEURS_CONTROLLER_TEST_PASSWORD', 'foobarbaz');"],
            ],
        ];
    }
}
