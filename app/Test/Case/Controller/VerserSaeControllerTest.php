<?php
App::uses('VerserSaeController', 'Controller');

/**
 * VerserSaeController Test Case
 */
class VerserSaeControllerTest extends ControllerTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [];

    /**
     * testSendToSaeAutresActes method
     *
     * @return void
     */
    public function testSendToSaeAutresActes()
    {
        $this->markTestIncomplete('testSendToSaeAutresActes not implemented.');
    }

    /**
     * testSendToSaeDeliberation method
     *
     * @return void
     */
    public function testSendToSaeDeliberation()
    {
        $this->markTestIncomplete('testSendToSaeDeliberation not implemented.');
    }

    /**
     * testVerser method
     *
     * @return void
     */
    public function testVerser()
    {
        $this->markTestIncomplete('testVerser not implemented.');
    }
}
