<?php
App::uses('ExportGedController', 'Controller');
App::uses('', 'Cakeflow.Model');
App::uses('PDFStampComponent', 'Controller/Component');
App::uses('ControllerTrait', 'Test/Case/Controller');

require_once(APP . DS . 'Vendor' . DS . 'cmis_repository_wrapper.php');
/**
 * ExportGedController Test Case
 */
class ExportGedControllerTest extends ControllerTestCase
{
    use ControllerTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.acteur',
        'app.typeacteur',
        'app.acteur_service',
        //Deliberation
        'app.typeacte',
        'app.deliberation',
        'app.deliberation_seance',
        'app.seance',
        'app.typeseance',
        'app.deliberation_typeseance',
        'app.collectivite',
        'app.theme',
        'app.nature',
        'app.nature_typologiepiece',
        'app.typologiepiece',
        'app.nomenclature',
        'app.listepresence',
        'app.vote',
        'app.annexe',
        'app.historique',
        'app.user',
        'app.service',
        'app.infosupdef',
        'app.infosup',
        'app.tdt_message',
        //Workflow
        'app.circuit_user',
        'app.circuit',
        'app.etape',
        'app.composition',
        'app.visa',
        'app.traitement',
        'app.token_method'
    ];

    public function setUp()
    {
        parent::setUp();
        // Simuler le HTTP_HOST
        $_SERVER['HTTP_HOST'] = 'localhost';
        Configure::write('Config.database', 'test');

        Configure::write('USE_GED', true);
        CakeLog::disable('stdout');
        CakeLog::disable('stderr');
    }

    public function tearDown()
    {
        unset($_SERVER['HTTP_HOST']);
        parent::tearDown();
    }

//    public function testSendToGedAutreActePastell()
//    {
//        Configure::write('GED', 'PASTELL');
//        $data = [
//            'Deliberation' => [
//                'id' => 1,
//                'send' => true
//            ]
//        ];
//        $this->testAction(
//            '/ExportGed/sendToGedAutreActe/1',
//            ['data' => $data, 'method' => 'post']
//        );
//    }

    public function testSendToGedAutreActeCMIS()
    {
        $this->markTestSkipped('must be revisited.');
        Configure::write('GED', 'CMIS');
        Configure::write('GED_XML_VERSION', 4);

        $clientMock = $this->createMock(CMISService::class);
        $clientMock->method('getFolderTree')->willReturn(null);
        $clientMock->method('createFolder')->willReturn(uniqid('folder', false));

        $ExportGed = $this->generate('ExportGed', [
            'components' => [
                'Flash' => ['set'],
                'Acl',
                'ExportGedCMIS' => [
                    'CMISService', 'getClient', 'send', 'getFolderId', 'deletetoGed', 'fileFusionProjet', 'PDFStamp'
                ],
                'ProjetTools',
            ]
        ]);
        $pDFStampComponent = $this->createMock(PDFStampComponent::class);
        $pDFStampComponent
            ->method('stamp')
            ->willReturn(file_get_contents(TESTS . 'Data' . DS .'AnnexFixture.pdf'));
        $ExportGed->ExportGedCMIS->PDFStamp = $pDFStampComponent;

        $ExportGed->ExportGedCMIS
            ->method('getClient')
            ->willReturn($clientMock);
        $ExportGed->ExportGedCMIS
            ->method('send');
        $ExportGed->ExportGedCMIS
            ->method('getFolderId')
            ->willReturn('1');
        $ExportGed->ExportGedCMIS
            ->method('deletetoGed')
            ->willReturn(true);
        $ExportGed->ExportGedCMIS
            ->method('fileFusionProjet')
            ->willReturn(file_get_contents(TESTS . 'Data' . DS .'AnnexFixture.pdf'));

        $this->assertFlash($ExportGed, [
            'Le dossier "ARRETE10" a été ajouté (Depot n° 1)',
            'Envoi(s) effectué(s) avec succès'
        ]);

        $data = [
            'Deliberation' => [
                10 => [
                    'send' => 1
                ]
            ]
        ];
        $this->testAction(
            '/ExportGed/sendToGedAutreActe/1',
            ['data' => $data, 'method' => 'post']
        );
    }
}
