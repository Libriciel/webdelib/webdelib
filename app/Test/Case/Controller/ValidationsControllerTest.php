<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Deliberation', 'Model');
App::uses('Traitement', 'Cakeflow.Model');
App::uses('Visa', 'Cakeflow.Model');
App::uses('DeliberationsController', 'Controller');
App::uses('ComponentCollection', 'Controller');
App::uses('', 'Cakeflow.Model');

/**
 * Classe DeliberationsControllerTest.
 *
 * @version 4.3
 * @package app.Test.Case.Controller
 */
class ValidationsControllerTest extends CakeTestCase
{

    /**
     * Fixtures associated with this test case
     *
     * @version 4.3
     * @var array
     */
    public $fixtures = [
        //Génération
        'app.Deliberation',
        'app.DeliberationSeance',
        'app.Seance',
        'app.Typeseance',
        'app.DeliberationTypeseance',
        'app.Typeacte',
        'app.Collectivite',
        'app.Theme',
//        'app.Infosup',
        'app.Infosupdef',
        'app.Listepresence',
        'app.Vote',
        'app.Annexe',
        'app.Plugin/ModelOdtValidator/Modeltemplate',
        'app.Plugin/ModelOdtValidator/Modeltype',
        //Duplication de projet
        'app.Historique',
        'app.User',
        //Workflow
        'app.CircuitUser',
        'app.circuit',
        'app.etape',
        'app.composition',
        'app.visa',
        'app.traitement',
//        'app.Plugin/ConnectorManager/Connector',
//        'app.Plugin/ConnectorManager/ConnectorJob',
    ];
    public $Deliberation;
    public $Deliberationseance;
    public $Circuit;
    //public $collection;
    //public $DeliberationsController;
    public $Traitement;
    public $Visa;
    public $Modeltemplate;

    public function setUp()
    {
        parent::setUp();
        // Simuler le HTTP_HOST
        $_SERVER['HTTP_HOST'] = 'localhost';
        Configure::write('Config.database', 'test');

        $this->Deliberation = ClassRegistry::init('Deliberation');
        $this->Deliberationseance = ClassRegistry::init('Deliberationseance');
        $this->Circuit = ClassRegistry::init('Cakeflow.Circuit');
        //$this->collection = new ComponentCollection();
        //$this->DeliberationsController = new DeliberationsController($collection);
        $this->Traitement = ClassRegistry::init('Cakeflow.Traitement');
        $this->Visa = ClassRegistry::init('Cakeflow.Visa');

        $this->Modeltemplate = ClassRegistry::init('Modeltemplate');
        $this->Modeltemplate->id = 1;

        $aData['content'] = file_get_contents(ROOT . DS . 'plugins/ModelOdtValidator/Test/Data/Modeltemplate1.odt');
        $this->Modeltemplate->save($aData, false);
    }

    /**
     * Méthode exécutée avant chaque test.
     *
     * @version 4.3
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($_SERVER['HTTP_HOST']);
        unset($this->Deliberation);
        unset($this->Deliberationseance);
        unset($this->Circuit);
        //unset($this->collection);
        //unset($this->DeliberationsController);
        unset($this->Traitement);
        unset($this->Visa);
    }

    /**
     * Test de l'ajout dans un circuit avec l'optimisation activé.
     *
     * @version 4.3
     * @return void
     */
    public function testAddIntoCircuitByOptimisation()
    {
        $this->markTestSkipped('must be revisited.');
        $id = 2;
        $userConnecte = 1;
        $circuit = 1;
        $numeroTraitement = 1;

        $traitementId = $this->Circuit->insertDansCircuit($circuit, $id, $userConnecte);

        $options = [
            'insertion' => [
                '0' => [
                    'Etape' => [
                        'etape_nom' => 'Rédacteur',
                        'etape_type' => 1
                    ],
                    'Visa' => [
                        '0' => [
                            'trigger_id' => $userConnecte,
                            'type_validation' => 'V'
                        ]
                    ],
                ]
            ],
            'optimisation' => true
        ];

        $traitementTermine = $this->Traitement->execute('IN', $userConnecte, $id, $options);

        $visas = $this->Visa->find('all', [
            'fields' => ['numero_traitement', 'action'],
            'conditions' => [
                'traitement_id' => $traitementId,
                'numero_traitement <=' => $numeroTraitement
            ],
            'order' => ['numero_traitement' => 'ASC','action' => 'ASC'],
            'recursive' => -1
        ]);
    }

    /**
     * Méthode exécutée avant chaque test.
     *
     * @version 4.3
     * @return void
     */
    /*
      public function test_delegToParapheurDocument(){
      $this->Deliberation->id = 1;
      $result = $this->Deliberation->_delegToParapheurDocument();

      $expected = array(
      'docPrincipale' => $result['docPrincipale'], //FIX remplacer par le fichier sortie dans Data
      'annexes' => array()
      );

      $this->assertEquals($result, $expected, var_export($result, true));
      }
     */

    /**
     * Test de l'ajout dans un circuit avec l'optimisation d'activé.
     *
     * @version 4.3
     * @return void
     */
//    public function testaddIntoCircuitFirstRedacColl()
//    {
//        $projetId = 2;
//        $userId = 4;
//        $serviceId = 4;
//        //Circuit id: 7 - Circuit avec étape rédacteur et première étape du circuit de type collaboratif
//        $visas = $this->executeCircuitByParam(7, $this->Deliberation->duplicate($projetId, $userId, $serviceId), $userId, true, 4);
//        $this->assertEquals($visas, array(
//            array('Visa' => array('numero_traitement' => 1, 'action' => 'IN')),
//            array('Visa' => array('numero_traitement' => 2, 'action' => 'OK')),
//            array('Visa' => array('numero_traitement' => 3, 'action' => 'OK')),
//            array('Visa' => array('numero_traitement' => 3, 'action' => 'RI')),
//            array('Visa' => array('numero_traitement' => 4, 'action' => 'RI')),
//            array('Visa' => array('numero_traitement' => 4, 'action' => 'RI'))), var_export($visas, true));
//    }

//    /**
//     * Test de l'ajout dans un circuit avec l'optimisation d'activé.
//     *
//     * @version 4.3
//     * @return void
//     */
//    public function testAddIntoCircuitWithOptimisation()
//    {
//        $projetId = 2;
//        $userId = 4;
//        $serviceId = 4;
//        //Circuit id: 2 - Circuit avec étape rédacteur en première étape du circuit de type simple
//        $projetTestId = $this->Deliberation->duplicate($projetId, $userId, $serviceId);
//
//        $visas = $this->executeCircuitByParam(2, $projetTestId, $userId, true, 4);
//        $excepted = [
//            ['Visa' => ['numero_traitement' => 1, 'action' => 'IN']],
//            ['Visa' => ['numero_traitement' => 2, 'action' => 'OK']],
//            ['Visa' => ['numero_traitement' => 3, 'action' => 'OK']],
//            ['Visa' => ['numero_traitement' => 4, 'action' => 'RI']]
//        ];
//        $this->assertEquals($excepted, $visas, var_export($visas, true));

//        //Circuit id: 5 - Circuit avec étape rédacteur en première étape du circuit de type concurrent
//        $visas = $this->executeCircuitByParam(5, $this->Deliberation->duplicate($projetId, $userId, $serviceId), $userId, true, 4);
//        $this->assertEquals($visas, [
//            ['Visa' => ['numero_traitement' => 1, 'action' => 'IN']],
//            ['Visa' => ['numero_traitement' => 2, 'action' => 'OK']],
//            ['Visa' => ['numero_traitement' => 3, 'action' => 'OK']],
//            ['Visa' => ['numero_traitement' => 3, 'action' => 'RI']],
//            ['Visa' => ['numero_traitement' => 4, 'action' => 'RI']],
//            ['Visa' => ['numero_traitement' => 4, 'action' => 'RI']]], var_export($visas, true));
//
//        //Circuit id: 4 - Circuitsans étape rédacteur en première étape du circuit de type simple
//        $visas = $this->executeCircuitByParam(4, $this->Deliberation->duplicate($projetId, $userId, $serviceId), $userId, true, 3);
//        $this->assertEquals($visas, [
//            ['Visa' => ['numero_traitement' => 1, 'action' => 'IN']],
//            ['Visa' => ['numero_traitement' => 2, 'action' => 'OK']],
//            ['Visa' => ['numero_traitement' => 3, 'action' => 'RI']]], var_export($visas, true));
//
//        //Circuit id: 6 - Circuit sans étape rédacteur et première étape du circuit de type concurrent
//        $visas = $this->executeCircuitByParam(6, $this->Deliberation->duplicate($projetId, $userId, $serviceId), $userId, true, 2);
//        $this->assertEquals($visas, [
//            ['Visa' => ['numero_traitement' => 1, 'action' => 'IN']],
//            ['Visa' => ['numero_traitement' => 2, 'action' => 'OK']],
//            ['Visa' => ['numero_traitement' => 2, 'action' => 'RI']]], var_export($visas, true));
//
//        //Circuit id: 7 - Circuit avec étape rédacteur et première étape du circuit de type collaboratif
//        $visas = $this->executeCircuitByParam(7, $this->Deliberation->duplicate($projetId, $userId, $serviceId), $userId, true, 4);
//        $this->assertEquals($visas, [
//            ['Visa' => ['numero_traitement' => 1, 'action' => 'IN']],
//            ['Visa' => ['numero_traitement' => 2, 'action' => 'OK']],
//            ['Visa' => ['numero_traitement' => 3, 'action' => 'OK']],
//            ['Visa' => ['numero_traitement' => 3, 'action' => 'RI']],
//            ['Visa' => ['numero_traitement' => 4, 'action' => 'RI']],
//            ['Visa' => ['numero_traitement' => 4, 'action' => 'RI']]], var_export($visas, true));
//
//        //Circuit id: 8 - Circuit sans étape rédacteur et première étape du circuit de type collaboratif
//        $visas = $this->executeCircuitByParam(8, $this->Deliberation->duplicate($projetId, 3, $serviceId), $userId, true, 3);
//        $this->assertEquals($visas, [
//            ['Visa' => ['numero_traitement' => 1, 'action' => 'IN']],
//            ['Visa' => ['numero_traitement' => 2, 'action' => 'OK']],
//            ['Visa' => ['numero_traitement' => 2, 'action' => 'RI']],
//            ['Visa' => ['numero_traitement' => 3, 'action' => 'RI']],
//            ['Visa' => ['numero_traitement' => 3, 'action' => 'RI']]], var_export($visas, true));
//    }

    /**
     * Test de l'ajout dans un circuit sans l'optimisation d'activé.
     *
     * @version 4.3
     * @return void
     */
//    public function testAddIntoCircuitWithoutOptimisation()
//    {
//        $projetId = 2;
//        $userId = 4;
//        $serviceId = 4;
//        //Circuit id: 2 - Circuit avec étape rédacteur en première étape du circuit de type simple
//        $visas = $this->executeCircuitByParam(2, $this->Deliberation->duplicate($projetId, $userId, $serviceId), $userId, false, 3);
//        $this->assertEquals($visas, [
//            ['Visa' => ['numero_traitement' => 1, 'action' => 'IN']],
//            ['Visa' => ['numero_traitement' => 2, 'action' => 'RI']],
//            ['Visa' => ['numero_traitement' => 3, 'action' => 'RI']]], var_export($visas, true));
//
//        //Circuit id: 5 - Circuit avec étape rédacteur en première étape du circuit de type concurrent
//        $visas = $this->executeCircuitByParam(5, $this->Deliberation->duplicate($projetId, $userId, $serviceId), $userId, false, 2);
//        $this->assertEquals($visas, [
//            ['Visa' => ['numero_traitement' => 1, 'action' => 'IN']],
//            ['Visa' => ['numero_traitement' => 2, 'action' => 'RI']]], var_export($visas, true));
//
//        //Circuit id: 4 - Circuitsans étape rédacteur en première étape du circuit de type simple
//        $visas = $this->executeCircuitByParam($serviceId, $this->Deliberation->duplicate($projetId, $userId, $serviceId), $userId, false, 2);
//        $this->assertEquals($visas, [
//            ['Visa' => ['numero_traitement' => 1, 'action' => 'IN']],
//            ['Visa' => ['numero_traitement' => 2, 'action' => 'RI']]], var_export($visas, true));
//
//        //Circuit id: 6 - Circuit sans étape rédacteur et première étape du circuit de type concurrent
//        $visas = $this->executeCircuitByParam(6, $this->Deliberation->duplicate($projetId, $userId, $serviceId), $userId, false, 3);
//        $this->assertEquals($visas, [
//            ['Visa' => ['numero_traitement' => 1, 'action' => 'IN']],
//            ['Visa' => ['numero_traitement' => 2, 'action' => 'RI']],
//            ['Visa' => ['numero_traitement' => 2, 'action' => 'RI']],
//            ['Visa' => ['numero_traitement' => 3, 'action' => 'RI']],
//            ['Visa' => ['numero_traitement' => 3, 'action' => 'RI']]], var_export($visas, true));
//
//        //Circuit id: 7 - Circuit sans étape rédacteur et première étape du circuit de type collaboratif
//        $visas = $this->executeCircuitByParam(7, $this->Deliberation->duplicate($projetId, $userId, $serviceId), $userId, false, 3);
//        $this->assertEquals($visas, [
//            ['Visa' => ['numero_traitement' => 1, 'action' => 'IN']],
//            ['Visa' => ['numero_traitement' => 2, 'action' => 'RI']],
//            ['Visa' => ['numero_traitement' => 3, 'action' => 'RI']],
//            ['Visa' => ['numero_traitement' => 3, 'action' => 'RI']]], var_export($visas, true));
//
//        //Circuit id: 8 - Circuit sans étape rédacteur et première étape du circuit de type collaboratif
//        $visas = $this->executeCircuitByParam(8, $this->Deliberation->duplicate($projetId, $userId, $serviceId), $userId, false, 3);
//        $this->assertEquals($visas, [
//            ['Visa' => ['numero_traitement' => 1, 'action' => 'IN']],
//            ['Visa' => ['numero_traitement' => 2, 'action' => 'RI']],
//            ['Visa' => ['numero_traitement' => 2, 'action' => 'RI']],
//            ['Visa' => ['numero_traitement' => 3, 'action' => 'RI']],
//            ['Visa' => ['numero_traitement' => 3, 'action' => 'RI']]], var_export($visas, true));
//    }

    /**
     * Test de l'ajout dans un circuit sans l'optimisation d'activé.
     *
     * @version 4.3
     * @return array Visas créer
     *
     * @param int $circuitId Identifiant du circuit
     * @param int $projetId Identifiant du projet
     * @param int $userConnecte Identifiant de l'utilisateur
     */
    private function executeCircuitByParam($circuitId, $projetId, $userConnecte, $optimisation, $numeroTraitement)
    {
        $options = [
            'insertion' => [
                '0' => [
                    'Etape' => [
                        'etape_nom' => 'Rédacteur',
                        'etape_type' => 1
                    ],
                    'Visa' => [
                        '0' => [
                            'trigger_id' => $userConnecte,
                            'type_validation' => 'V'
                        ]
                    ],
                ]
            ],
            'optimisation' => $optimisation
        ];

        $traitementId = $this->Circuit->insertDansCircuit($circuitId, $projetId, $userConnecte);
        $this->Traitement->execute('IN', $userConnecte, $projetId, $options);

        return $this->Visa->find('all', [
            'fields' => ['numero_traitement', 'action'],
            'conditions' => [
                'traitement_id' => $traitementId,
                'numero_traitement <=' => $numeroTraitement
            ],
            'order' => ['numero_traitement' => 'ASC','action' => 'ASC'],
            'recursive' => -1
        ]);
    }
}
