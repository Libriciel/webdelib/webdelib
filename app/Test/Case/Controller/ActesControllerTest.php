<?php
App::uses('ActesController', 'Controller');
App::uses('CakeflowAppModel', 'Cakeflow.Model');
App::uses('ControllerTrait', 'Test/Case/Controller');

/**
 * ActesController Test Case
 */
class ActesControllerTest extends ControllerTestCase
{
    use ControllerTrait;
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.acteur',
        'app.typeacteur',
        'app.acteur_service',
        //Deliberation
        'app.typeacte',
        'app.deliberation',
        'app.deliberation_seance',
        'app.seance',
        'app.typeseance',
        'app.deliberation_typeseance',
        'app.collectivite',
        'app.theme',
        'app.nature',
        'app.nature_typologiepiece',
        'app.typologiepiece',
        'app.nomenclature',
        'app.listepresence',
        'app.vote',
        'app.annexe',
        'app.historique',
        'app.user',
        'app.service',
        'app.infosupdef',
        'app.infosuplistedef',
        'app.infosupdef_profil',
        'app.infosup',
        'app.tdt_message',
        //Workflow
        'app.deliberation_user',
        'app.circuit_user',
        'app.circuit',
        'app.etape',
        'app.composition',
        'app.visa',
        'app.traitement',
        'app.token_method'
    ];

    public function setUp()
    {
        parent::setUp();
        // Simuler le HTTP_HOST
        $_SERVER['HTTP_HOST'] = 'localhost';
        Configure::write('Config.database', 'test');
        $this->Deliberation = ClassRegistry::init('Deliberation');
    }

    /**
     * Méthode exécutée après chaque test.
     * @return void
     */
    public function tearDown()
    {
        unset($_SERVER['HTTP_HOST']);
        parent::tearDown();
    }

    /**
     * testEditPostSign method
     *
     * @return void
     */
    public function testEditPostSign()
    {
        $controllerActes = $this->generate('Actes', [
            'methods' => [
                'isAuthorized'
            ],
            'components' => [
                'Acl',
                'SeanceTools',
            ]
        ]);

        $result = $this->testAction(
            '/actes/editPostSign/1',
            ['method' => 'get']
        );
    }

    /**
     * testDownload method
     *
     * @return void
     */
    public function testDownload(): void
    {
        $result = $this->testAction(
            '/actes/download/1',
            ['return' => 'contents']
        );
    }

    /**
     * testDownloadBordereau method
     *
     * @return void
     */
    public function testDownloadBordereau()
    {
        $result = $this->testAction(
            '/actes/download/1',
            ['return' => 'contents']
        );
    }

    /**
     * testDownloadSignature method
     *
     * @return void
     */
    public function testDownloadSignature()
    {
        $result = $this->testAction(
            '/actes/download/1',
            ['return' => 'contents']
        );
    }

    /**
     * testDownloadTdtTampon method
     *
     * @return void
     */
    public function testDownloadTdtTampon()
    {
        $result = $this->testAction(
            '/actes/download/1',
            ['return' => 'contents']
        );
    }

    /**
     * testDownloadTdtMessage method
     *
     * @return void
     */
    public function testDownloadTdtMessage()
    {
        $result = $this->testAction(
            '/actes/download/1',
            ['return' => 'contents']
        );
    }

    /**
     * testGetTampon method
     *
     * @return void
     */
    public function testGetTampon()
    {
        $result = $this->testAction(
            '/actes/download/1',
            ['return' => 'contents']
        );
    }

    /**
     * testGetBordereau method
     *
     * @return void
     */
    public function testGetBordereau()
    {
        $result = $this->testAction(
            '/actes/download/1',
            ['return' => 'contents']
        );
    }

    /**
     * testGetBordereauTdt method
     *
     * @return void
     */
    public function testGetBordereauTdt()
    {
        $result = $this->testAction(
            '/actes/download/1',
            ['return' => 'contents']
        );
    }

    /**
     * testGenerationNumerotation method
     *
     * @return void
     */
    public function testGenerationNumerotationGenerate()
    {
        $controllerActes = $this->generate('Actes', [
            'methods' => [
                'isAuthorized',
                'versioningEnable'
            ],
            'models' => [
                'Deliberation'=> ['genereNumeroActe'],
            ],
            'components' => [
                'Flash',
            ]
        ]);

        $controllerActes->Deliberation
            ->method('genereNumeroActe')
            ->willReturn('DEL03');

        $this->assertFlash($controllerActes, [
            'Acte n° 7 : numéro généré correctement\n',
        ]);

        $data = [
            'genereNumActe' => true,
            'Deliberation' => [
                7 => [
                    'is_checked' => 1,
                    'num_pref' => '1.1',
                ]]
        ];
        $result = $this->testAction(
            '/actes/generationNumerotation',
            [
                'data' => $data,
                'method' => 'post'
            ]
        );
    }

    /**
     * testGenerationNumerotation method
     *
     * @return void
     */
    public function testGenerationNumerotationNumExist()
    {
        $controllerActes = $this->generate('Actes', [
            'methods' => [
                'isAuthorized',
                'versioningEnable'
            ],
            'models' => [
                'Deliberation'=> ['genereNumeroActe'],
            ],
            'components' => [
                'Flash',
            ]
        ]);

        $controllerActes->Deliberation
            ->method('genereNumeroActe')
            ->willReturn(false);

        $this->assertFlash($controllerActes, [
            'Acte n° 8 : le numéro existe déjà\n',
        ]);

        $data = [
            'genereNumActe' => true,
            'Deliberation' => [
                8 => [
                    'is_checked' => 1,
                    'num_pref' => '1.1',
                ]]
        ];

        $result = $this->testAction(
            '/actes/generationNumerotation',
            [
                'data' => $data,
                'method' => 'post'
            ]
        );
    }

    /**
     * testGenerationNumerotation method
     *
     * @return void
     */
    public function testGenerationNumerotationNoAction()
    {
        $controllerActes = $this->generate('Actes', [
            'methods' => [
                'isAuthorized',
                'versioningEnable'
            ],
            'models' => [
                'Deliberation'=> ['genereNumeroActe'],
            ],
            'components' => [
                'Flash',
            ]
        ]);

        $this->assertFlash($controllerActes, [
            'Veuillez sélectionner une action',
        ]);

        $result = $this->testAction(
            '/actes/generationNumerotation',
            [
                'data' => [],
                'method' => 'post'
            ]
        );
    }
}
