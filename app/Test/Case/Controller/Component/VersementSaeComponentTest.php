<?php
App::uses('Controller', 'Controller');
App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('VersementSaeComponent', 'Controller/Component');
App::uses('AppTools', 'Lib');
class VersementSaeControllerTest extends Controller
{
}

/**
 * VersementSaeComponent Test Case
 */
class VersementSaeComponentTest extends CakeTestCase
{
    /**
     * Fixtures associated with this test case
     *
     * @version 4.3
     * @var array
     */
    public $fixtures = [
        'app.theme'
        ];
/**
 * setUp method
 *
 * @return void
 */
    public function setUp()
    {
        $this->markTestSkipped('must be revisited.');
        parent::setUp();
        $Collection = new ComponentCollection();
        $this->VersementSae = new VersementSaeComponent($Collection);
        $CakeRequest = new CakeRequest();
        $CakeResponse = new CakeResponse();
        $this->Controller = new VersementSaeControllerTest($CakeRequest, $CakeResponse);
        $this->VersementSae->initialize($this->Controller);

        $this->sae = $this->getMockBuilder(Sae::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->VersementSae->setSae($this->sae);
        $this->VersementSae->startup($this->Controller);
    }

/**
 * tearDown method
 *
 * @return void
 */
    public function tearDown()
    {
        unset($this->VersementSae);

        parent::tearDown();
    }

/**
 * testVerserService method
 *
 * @return void
 */
    public function testVerserService()
    {
        $folderSend = new Folder(
            AppTools::newTmpDir(TMP . 'files' . DS . 'sae' . DS),
            true
        );
        $this->VersementSae->verserService($folderSend);
        $this->assertEquals($folderSend->pwd(), $this->VersementSae->getFolderSend()->pwd());
    }

/**
 * testSetZip method
 *
 * @return void
 */
    public function testSetZip()
    {
        $zip = new ZipArchive();
        $this->VersementSae->setZip($zip);
        $this->assertInstanceOf('ZipArchive', $this->VersementSae->getZip());
    }

/**
 * testCreateElement method
 *
 * @return void
 */
    public function testCreateElement()
    {
        $expectedXml = '<?xml version="1.0" encoding="utf-8"?><delib idActe="12"/>';
        $dom = new DOMDocument('1.0', 'utf-8');
        $elementXml = $this->VersementSae->createElement($dom, 'delib', null, ['idActe' => 12]);
        $dom->appendChild($elementXml);
        $actuelXml = $dom->saveXML();
        $this->assertXmlStringEqualsXmlString($expectedXml, $actuelXml);
    }

/**
 * testCreateElementInfosupsByZip method
 *
 * @return void
 */
    public function testCreateElementInfosupsByZip()
    {
        $this->markTestIncomplete('testCreateElementInfosupsByZip not implemented.');
    }

/**
 * testGetDeliberation method
 *
 * @return void
 */
    public function testGetDeliberation()
    {
        $this->markTestIncomplete('testGetDeliberation not implemented.');
    }

/**
 * testAddFileToZip method
 *
 * @return void
 */
    public function testAddFileToZip()
    {
        $this->markTestIncomplete('testAddFileToZip not implemented.');
    }

/**
 * testSend method
 *
 * @return void
 */
    public function testSend()
    {
        $this->markTestIncomplete('testSend not implemented.');
    }

/**
 * testSendActe method
 *
 * @return void
 */
    public function testSendActe()
    {
        $this->markTestIncomplete('testSendActe not implemented.');
    }

/**
 * testCreateElementThemesForActe method
 *
 * @return void
 */
    public function testCreateElementThemesForActe()
    {
        $expectedXml = '<?xml version="1.0" encoding="utf-8"?><delib idActe="12">
            <themeProjet><niveau1>ADMINISTRATION GENERALE</niveau1></themeProjet></delib>';
        $dom = new DOMDocument('1.0', 'utf-8');
        $doc = $this->VersementSae->createElement($dom, 'delib', null, ['idActe' => 12]);
        $this->VersementSae->createElementThemesForActe($dom, $doc, 1);
        $dom->appendChild($doc);
        $actuelXml = $dom->saveXML();
        $this->assertXmlStringEqualsXmlString($expectedXml, $actuelXml);
    }
}
