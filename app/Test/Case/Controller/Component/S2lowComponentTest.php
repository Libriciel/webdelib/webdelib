<?php

/**
 * PatchShellTest file
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       webdelib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */
App::uses('Controller', 'Controller');
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('ComponentCollection', 'Controller');
App::uses('S2lowComponent', 'Controller/Component');
/**
 * Classe TdtTest.
 *
 * @version 4.5
 * @package app.Test.Image
 */

// Un faux controller pour tester against
class S2lowControllerTest extends Controller
{
}

/**
 *
 */
class S2lowComponentTest extends CakeTestCase
{
    
    public $S2lowComponent = null;
    public $Controller = null;
    
    public function setUp()
    {
        $this->markTestSkipped(
            'The mocks is not available.'
        );

        parent::setUp();
        // Configurer notre component et faire semblant de tester le controller
        $Collection = new ComponentCollection();
        $this->S2lowComponent = new S2lowComponent($Collection);
        $CakeRequest = new CakeRequest();
        $CakeResponse = new CakeResponse();
        $this->Controller = new S2lowControllerTest($CakeRequest, $CakeResponse);
        $this->S2lowComponent->startup($this->Controller);
    }
    public function configCertif()
    {
        
        
        $repertoire='/home/jlegall/public_html/webdelib/app/Test/Data/ConfigS2lowFormation/';
        
        Configure::write('S2LOW_PEM', $repertoire.'client.pem');
        Configure::write('S2LOW_CERTPWD', $repertoire.'key.pem');
        Configure::write('S2LOW_SSLKEY', $repertoire.'ca.pem');
        Configure::write('S2LOW_HOST'.'https://s2low.formations.adullact.org/');
        Configure::write('S2LOW_CERTPWD', 'julien.legall@libriciel.coop');
        Configure::write('S2LOW_USELOGIN', false);
        Configure::write('S2LOW_LOGIN', 'user1cle');
        Configure::write('S2LOW_PWD', 'user1cle');
    }
    
    public function configCertifUser()
    {
        
        
        $repertoire='/home/jlegall/public_html/webdelib/app/Test/Data/ConfigS2lowPartenaires/';
        
        Configure::write('S2LOW_PEM', $repertoire.'client.pem');
        Configure::write('S2LOW_CERTPWD', $repertoire.'key.pem');
        Configure::write('S2LOW_SSLKEY', $repertoire.'ca.pem');
        Configure::write('S2LOW_HOST'.'https://s2low.partenaires.libriciel.fr/');
        Configure::write('S2LOW_CERTPWD', 'libriciel');
        Configure::write('S2LOW_USELOGIN', true);
        Configure::write('S2LOW_LOGIN', 'user1cle');
        Configure::write('S2LOW_PWD', 'user1cle');
    }
    
    public function testloginCertificat()
    {
        $this->configCertif();
        $reponse=$this->S2lowComponent->testLogin();
        $this->assertEquals("OK", $reponse, var_export($reponse, true));
    }
    
    public function testClassificationCertificat()
    {
        $this->configCertif();
        $reponse=$this->S2lowComponent->getClassification();

        $recherche='actes:RetourClassification';
        
        $pos = strpos($reponse, $recherche);
        $this->assertFalse(empty($pos), var_export($reponse, true));
    }
    
    public function testloginCertifUser()
    {
        $this->configCertifUser();
        $response = $this->S2lowComponent->testLogin();
        $this->assertEquals("OK", $response, var_export($response, true));
    }
    
    public function testClassificationCertifUser()
    {
        $this->configCertifUser();
        $reponse=$this->S2lowComponent->getClassification();

        $recherche='actes:RetourClassification';
        
        $pos = strpos($reponse, $recherche);
        $this->assertFalse(empty($pos), var_export($reponse, true));
    }
    
    public function testgetFluxRetourCertif()
    {
        $this->configCertif();
        $reponse=$this->S2lowComponent->getFluxRetour(678945612);
        $this->assertEquals("OK", $reponse, var_export($reponse, true));
    }
}
