<?php

App::uses('Controller', 'Controller');
App::uses('CakeRequest', 'Network');
App::uses('HttpSocketResponse', 'Network/Http');
App::uses('ComponentCollection', 'Controller');
App::uses('ConversionComponent', 'Controller/Component');
App::uses('CakeResponse', 'Network');
App::uses('File', 'Utility');

// Un faux controller pour tester against
class ConversionControllerTest extends Controller
{
}
/**
 * Classe ConversionComponentTest.
 *
 * @package app.Test.Case.Model
 */
class ConversionComponentTest extends CakeTestCase
{

    public $ConversionComponent = null;
    public $Controller = null;

    public function setUp()
    {
        $this->markTestSkipped('must be revisited.');
        Configure::write('FusionConv.cloudooo_host', '');

        parent::setUp();
        // Configurer notre component et faire semblant de tester le controller
        $Collection = new ComponentCollection();
        $this->ConversionComponent = new ConversionComponent($Collection);
        $CakeRequest = new CakeRequest();
        $CakeResponse = new CakeResponse();
        $this->Controller = new ConversionControllerTest($CakeRequest, $CakeResponse);
        $this->ConversionComponent->initialize($this->Controller);
        $this->ConversionComponent->startup($this->Controller);
    }

    /**
     * Méthode exécutée avant chaque test.
     *
     * @version 4.3
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->ConversionComponent, $this->Controller);
    }

    /**
     * Méthode exécutée après chaque test.
     *
     * @version 4.3
     * @return void
     */
    public function testConvertirFlux()
    {
        $xMLRPC2Client = $this->getMockBuilder(stdClass::class)
            ->setMethods(['convertFile'])
            ->getMock();
        $xMLRPC2Client
            ->method('convertFile')
            ->willReturn(base64_encode(file_get_contents(TESTS . 'Data' . DS .'AnnexFixture.pdf')));

        $this->ConversionComponent->setClient($xMLRPC2Client);

        $fileOdt = new File(TESTS . 'Data' . DS .'OdtVide.odt');
        $fileOdt->close();
        $result = $this->ConversionComponent->convertirFlux($fileOdt->read(), 'odt', 'pdf');

        $filePdf = new File(AppTools::newTmpDir(TMP . 'files' . DS . 'conversion' . DS) . 'result.pdf', true);
        $filePdf->append($result);
        $expected = $filePdf->mime();
        $filePdf->close();

        $this->assertEquals($expected, 'application/pdf');
    }

    public function testConvertirFluxException()
    {
        $xMLRPC2Client = $this->getMockBuilder(stdClass::class)
            ->setMethods(['convertFile'])
            ->getMock();
        $xMLRPC2Client
            ->method('convertFile')
            ->will(self::throwException(new XML_RPC2_FaultException('SocketException', 200)));

        $this->ConversionComponent->setClient($xMLRPC2Client);

        $fileOdt = new File(TESTS . 'Data' . DS .'OdtVide.odt');
        $fileOdt->close();
        $result = $this->ConversionComponent->convertirFlux($fileOdt->read(), 'odt', 'pdf');
        $this->assertFalse($result);
    }

    /**
     * Méthode exécutée après chaque test.
     *
     * @return void
     */
    /* public function testconvertirFluxHTML(){

      $result=$this->ConversionComponent->convertirFlux('<b>test</b>', 'html');
      $file=new File(TMP.DS.'files'.DS.'checkConversionHtml.odt',TRUE);
      $file->append($result);
      $expected = $file->mime();
      $file->close();

      $this->assertEquals($expected, 'application/html');
      } */
}
