<?php

/**
 * DeliberationsControllerTest file   
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       webdelib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */
App::uses('Controller', 'Controller');
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('ComponentCollection', 'Controller');
App::uses('PastellComponent', 'Controller/Component');

/**
 * Classe DeliberationsControllerTest.
 *
 * @version 4.3
 * @package app.Test.Case.Controller
 */
class PastellComponentTest extends CakeTestCase
{

    public $PastellComponent = null;
    public $Controller = null;

    public function setUp()
    {
        $this->markTestSkipped(
            'The mocks is not available.'
        );

        parent::setUp();
        // Configurer notre component et faire semblant de tester le controller
        $Collection = new ComponentCollection();
        $this->PastellComponent = new PastellComponent($Collection);
        $CakeRequest = new CakeRequest();
        $CakeResponse = new CakeResponse();
        $this->Controller = new TestPastellController($CakeRequest, $CakeResponse);
        $this->PastellComponent->startup($this->Controller);
    }

    /**
     * Méthode exécutée avant chaque test.
     * 
     * @version 4.3
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->PastellComponent);
        unset($this->Controller);
    }

    /**
     * Test GetVersion()
     * 
     * @version 4.3
     * @return void
     */
    public function testGetVersion()
    {
        return ($this->PastellComponent->getVersion());
    }

    /**
     * Test GetDocumentTypes()
     * 
     * @version 4.3
     * @return void
     */
    public function testGetDocumentTypes()
    {
        //return ($this->PastellComponent->getDocumentTypes());
    }

    /**
     * Test GetDocumentTypeInfos()
     * 
     * @version 4.3
     * @return void
     */
    public function testGetDocumentTypeInfos()
    {
        //$this->PastellComponent->getDocumentTypeInfos('actes-generique');
        //return ($this->PastellComponent->getDocumentTypeInfos('actes-generique'));
    }

    /**
     * Test GetDocumentTypeActions()
     * 
     * @version 4.3
     * @return void
     */
    public function testGetDocumentTypeActions()
    {
        //$this->PastellComponent->getDocumentTypeActions('actes-generique');
        //return ($this->PastellComponent->getDocumentTypeActions('actes-generique'));
    }

    /**
     * Test ListEntities()
     * 
     * @version 4.3
     * @return void
     */
    public function testListEntities()
    {
        //debug ($this->PastellComponent->listEntities());
        $this->PastellComponent->listEntities(true);
        //return ($this->PastellComponent->listEntities(true));
    }

    /**
     * Test ListDocuments()
     * 
     * @version 4.3
     * @return void
     */
    public function testListDocuments()
    {
        //debug ($this->PastellComponent->listDocuments(48,'actes-generique'));
        //$this->PastellComponent->listDocuments(3, 'actes-generique');
        //return ($this->PastellComponent->listDocuments(3, 'actes-generique'));
    }

    /**
     * Test RechercheDocument()
     * 
     * @version 4.3
     * @return void
     */
    public function testRechercheDocument()
    {
//        $options = array(
//            'id_e' => 3,
//            'type' => 'actes-generique',
//            'search' => 'test sans sources'
//        );
//        $this->PastellComponent->rechercheDocument($options);
        //return ($this->PastellComponent->rechercheDocument($options));
    }

    /**
     * Test GetInfosField()
     * 
     * @version 4.3
     * @return void
     */
    public function testGetInfosField()
    {
        $this->PastellComponent->getInfosField('3', 'E43dLMG', 'classification');
        //return ($this->PastellComponent->getInfosField('3', 'E43dLMG', 'classification'));
    }

    /**
     * Test DetailDocument()
     * 
     * @version 4.3
     * @return void
     */
    public function testDetailDocument()
    {
        $this->PastellComponent->detailDocument(3, 'USQI7k1');
        //return ($this->PastellComponent->detailDocument(3, 'USQI7k1'));
    }

    /**
     * FIXME
     * Test DetailDocuments()
     * 
     * @version 4.3
     * @return void
     */
    public function testDetailDocuments()
    {
        //debug ($this->PastellComponent->detailDocuments(48,array('id_d: elRx6ID', 'id_d: dfsfsdf')));
        //$this->PastellComponent->detailDocuments(48, array('elRx6ID'));
        //return ($this->PastellComponent->detailDocuments(48,array('elRx6ID')));
    }

    /**
     * OK en v1.3
     * Test CreateDocument()
     * 
     * @version 4.3
     * @return void
     */
    public function testCreateDocument()
    {
        $this->PastellComponent->createDocument(48, 'actes-generique');
        //return ($this->PastellComponent->createDocument(48, 'actes-generique'));
    }

    /**
     * FIXME
     * Test GetInfosField()
     * 
     * @return void
     */
    /* public function testGetInfosField(){
      //debug ($this->PastellComponent->getInfosField(3,'L4iaPx6', null));
      $this->PastellComponent->getInfosField(48,'actes-generique');
      //return ($this->PastellComponent->getInfosField(48,'actes-generique'));
      } */
}

// Un faux controller pour tester against
class TestPastellController extends Controller
{

    public $paginate = null;

}
