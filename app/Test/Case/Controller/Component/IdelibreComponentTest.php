<?php

App::uses('Controller', 'Controller');
App::uses('CakeRequest', 'Network');
App::uses('HttpSocket', 'Network/Http');
App::uses('HttpSocketResponse', 'Network/Http');
App::uses('ComponentCollection', 'Controller');
App::uses('IdelibreComponent', 'Controller/Component');
App::uses('CakeResponse', 'Network');

// Un faux controller pour tester against
class IdelibreControllerTest extends Controller
{
}

class IdelibreComponentTest extends CakeTestCase
{
    /**
     * Fixtures associated with this test case
     *
     * @version 4.3
     * @var array
     */
    public $fixtures = [
        //Génération
        'app.Deliberation',
        'app.DeliberationSeance',
        'app.Seance',
        'app.Typeseance',
        'app.DeliberationTypeseance',
        'app.Typeacte',
        'app.Collectivite',
        'app.Theme',
        'app.Nomenclature',
        'app.Listepresence',
        'app.Vote',
        'app.Annexe',
        'app.Historique',
        'app.User',
    ];

    public $IdelibreComponent = null;
    public $Controller = null;

    public function setUp()
    {
        $this->markTestSkipped('must be revisited.');
        parent::setUp();
        // Configurer notre component et faire semblant de tester le controller
        $Collection = new ComponentCollection();
        $this->Idelibre = new IdelibreComponent($Collection);
        $CakeRequest = new CakeRequest();
        $CakeResponse = new CakeResponse();
        $this->Controller = new IdelibreControllerTest($CakeRequest, $CakeResponse);
        $this->Idelibre->initialize($this->Controller);
        $this->Idelibre->startup($this->Controller);
        $this->Listepresence = ClassRegistry::init('listepresence');
        $this->Votes = ClassRegistry::init('Vote');
        $this->Seance = ClassRegistry::init('Seance');
    }

    /**
     * Méthode exécutée avant chaque test.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->IdelibreComponent, $this->Controller);
    }
    /**
     * @throws JsonException
     */
    public function testPing(): void
    {
        $this->Idelibre->setHttpSocket($this->prepareHttpRequest(
            $this->returnValue(json_encode(['message' => 'success'], JSON_THROW_ON_ERROR))
        ));

        $result = $this->Idelibre->ping();
        $this->assertTrue($result);
    }


    public function testPingError(): void
    {
        $httpSocket = $this->createMock(HttpSocket::class);
        $httpSocket
            ->expects($this->any())
            ->method('get')
            ->will($this->throwException(new SocketException('SocketException')));
        $this->Idelibre->setHttpSocket($httpSocket);

        $result = $this->Idelibre->ping();
        $this->assertFalse($result);
    }

    public function testProcessingSaveImportResultsSeanceNotExistError500(): void
    {
        $this->expectExceptionCode(500);
        $this->expectExceptionMessage('Pas de délibérations disponible dans la séance');

        $result = $this->Idelibre->processingSaveImportResults(999);
    }

    public function testProcessingSaveImportResultsError404(): void
    {
        $httpSocket = $this->createMock(HttpSocket::class);
        $httpSocket
            ->expects($this->any())
            ->method('get')
            ->will($this->throwException(new SocketException('SocketException')));
        $this->Idelibre->setHttpSocket($httpSocket);

        $result = $this->Idelibre->processingSaveImportResults(1);

        $this->assertFalse($result);

        $seance = $this->Seance->find('first', [
            'conditions' => ['id' => 1],
            'recursive' => -1
        ]);
        $result = $seance['Seance']['idelibre_votes_warning'];
        $except = json_encode(
            [[
                'error' => 500,
                'errorMessage' => 'Erreur de récupération de l\'identifiant de structure depuis idelibre'
                    .' (code: 500, message: SocketException)'
            ]],
            JSON_THROW_ON_ERROR
        );

        $this->assertEquals($except, $result, var_export($result, true));
    }


        /**
     * @throws JsonException
     */
    public function testProcessingSaveImportResults()
    {
        $this->Idelibre->setHttpSocket($this->prepareHttpRequest(
            $this->returnValue(json_encode(['message' => 'success'], JSON_THROW_ON_ERROR))
        ));

        $result = $this->Idelibre->ping();
        $this->assertTrue($result);

//            ->will($this->throwException(new CakeException('test')));
//
//$Controller->flash('this should work', '/flash');
//        $this->IdelibreComponent  = $this->getMock(
//            'IdelibreComponent',
//            ['sittingResults'],
//            ['1'],
//            '',
//            false
//        );
//        $this->IdelibreComponent
//            ->expects($this->once())
//            ->method('sittingResults')
//            ->with('1')
//            ->will($this->throwException(new CakeException('test')));

//        $testComponent = $this->createMock(IdelibreComponent::class);

//        $testComponent
//            ->expects($this->once())
//            ->method('sittingResults')
//            ->with('1')
//            ->will($this->throwException(new CakeException('test')));
//
//        $result = $this->IdelibreComponent->processingSaveImportResults(1);
//
//        $this->assertTrue($result);

//        $testLookup = $this->getMockBuilder('Lookup')
//            ->getMock();
//
//        $testLookup
//            ->expects($this->any())
//            ->method('get')
//            ->will($this->returnValue('abc'));

//        $this->getMockBuilder();
////        e `getMockBuilder()` or `createMock()` in new unit tests.
//        $this->createMock('App\Controller\Component\AwsComponent');
//        $this->getMockBuilder()
    }

    private function prepareHttpRequest($value)
    {

        $httpSocket = $this->createMock(HttpSocket::class);
        $httpSocketResponse = $this->createMock(HttpSocketResponse::class, ['body','isOk']);
        $httpSocketResponse
            ->expects($this->any())
            ->method('body')
            ->will($value);
        $httpSocketResponse
            ->expects($this->any())
            ->method('isOk')
            ->will($this->returnValue(true));

        $httpSocket
            ->expects($this->any())
            ->method('get')
            ->will($this->returnValue($httpSocketResponse));

        return $httpSocket;


//            ->will($this->throwException(new CakeException('test')));
//
    }
}
