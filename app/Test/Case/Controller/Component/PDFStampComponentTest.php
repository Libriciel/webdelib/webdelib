<?php
App::uses('ComponentCollection', 'Controller');
App::uses('PDFStampComponent', 'Controller/Component');
App::uses('HttpSocket', 'Network/Http');

/**
 * PDFStampComponent Test Case
 */
class PDFStampComponentTest extends CakeTestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        $this->markTestSkipped('must be revisited.');
        parent::setUp();

        $Collection = new ComponentCollection();
        $this->PDFStamp = new PDFStampComponent($Collection);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PDFStamp);

        parent::tearDown();
    }

    /**
     * testStamp method
     *
     * @return void
     */
    public function testStamp()
    {
        $httpSocketResponseMock = $this->getMockBuilder('HttpSocketResponse')
            ->disableOriginalConstructor()
            ->setMethods(['isOk', 'code'])
            ->getMock();

        $httpSocketResponseMock
            ->method('isOk')
            ->willReturn(true);

        $httpSocketMock = $this->getMockBuilder('HttpSocket')
            ->disableOriginalConstructor()
            ->setMethods(['post'])
            ->getMock();

        $httpSocketMock
            ->method('post')
            ->willReturn($httpSocketResponseMock);

        $this->PDFStamp->setHttpSocket($httpSocketMock);

        $fileStream = file_get_contents(TESTS . 'Data' . DS . 'AnnexFixture.pdf');
        $dataAR = [
            'tdt_ar' => '<actes:ARActe actes:IDActe="12345"><actes:DateReception>'
                .date('Y-m-d', strtotime("-1 day"))
                .'/actes:DateReception></actes:ARActe>',
            'publier_date' => date('Y-m-d', strtotime("-1 day")),
        ];

        $response = $this->PDFStamp->stamp($fileStream, $dataAR);

        $this->assertInstanceOf('HttpSocketResponse', $response);
        $this->assertTrue($response->isOk());
    }
}
