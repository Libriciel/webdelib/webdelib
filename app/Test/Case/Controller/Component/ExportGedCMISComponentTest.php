<?php
App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('ExportGedCMISComponent', 'Controller/Component');

/**
 * ExportGedCMISComponent Test Case
 */
class ExportGedCMISComponentTest extends CakeTestCase
{

/**
 * Fixtures
 *
 * @var array
 */
    public $fixtures = [];

/**
 * setUp method
 *
 * @return void
 */
    public function setUp()
    {
        $this->markTestSkipped('must be revisited.');
        parent::setUp();
        $Collection = new ComponentCollection();
        $this->ExportGedCMIS = new ExportGedCMISComponent($Collection);
    }

/**
 * tearDown method
 *
 * @return void
 */
    public function tearDown()
    {
        unset($this->ExportGedCMIS);

        parent::tearDown();
    }

/**
 * testService method
 *
 * @return void
 */
    public function testService()
    {
        $this->markTestIncomplete('testService not implemented.');
    }

/**
 * testCreateElement method
 *
 * @return void
 */
    public function testCreateElement()
    {
        $this->markTestIncomplete('testCreateElement not implemented.');
    }

/**
 * testCreateElementInfosupsByZip method
 *
 * @return void
 */
    public function testCreateElementInfosupsByZip()
    {
        $this->markTestIncomplete('testCreateElementInfosupsByZip not implemented.');
    }

/**
 * testCreateElementInfosups method
 *
 * @return void
 */
    public function testCreateElementInfosups()
    {
        $this->markTestIncomplete('testCreateElementInfosups not implemented.');
    }

/**
 * testDeletetoGed method
 *
 * @return void
 */
    public function testDeletetoGed()
    {
        $this->markTestIncomplete('testDeletetoGed not implemented.');
    }

/**
 * testGetTdtMessageForGed method
 *
 * @return void
 */
    public function testGetTdtMessageForGed()
    {
        $this->markTestIncomplete('testGetTdtMessageForGed not implemented.');
    }

/**
 * testInflectorNameForFolder method
 *
 * @return void
 */
    public function testInflectorNameForFolder()
    {
        $this->markTestIncomplete('testInflectorNameForFolder not implemented.');
    }

/**
 * testInflectorPluralizeForFolder method
 *
 * @return void
 */
    public function testInflectorPluralizeForFolder()
    {
        $this->markTestIncomplete('testInflectorPluralizeForFolder not implemented.');
    }

    /**
     * testCreateElementPvComplet method
     *
     * @return void
     */
    public function testCreateElementPvComplet()
    {
        $this->markTestIncomplete('testCreateElementPvComplet not implemented.');
    }

    /**
     * testCreateElementPvSommaire method
     *
     * @return void
     */
    public function testCreateElementPvSommaire()
    {
        $this->markTestIncomplete('testCreateElementPvSommaire not implemented.');
    }

    /**
     * testCreateElementOdj method
     *
     * @return void
     */
    public function testCreateElementOdj()
    {
        $this->markTestIncomplete('testCreateElementOdj not implemented.');
    }

    /**
     * testCreateElementConvocation method
     *
     * @return void
     */
    public function testCreateElementConvocation()
    {
        $this->markTestIncomplete('testCreateElementConvocation not implemented.');
    }

    /**
     * testGetActeCmis method
     *
     * @return void
     */
    public function testGetActeCmis()
    {
        $this->markTestIncomplete('testGetActeCmis not implemented.');
    }

/**
 * testCreateElementCommissionsForActe method
 *
 * @return void
 */
    public function testCreateElementCommissionsForActe()
    {
        $this->markTestIncomplete('testCreateElementCommissionsForActe not implemented.');
    }

/**
 * testCreateElementFile method
 *
 * @return void
 */
    public function testCreateElementFile()
    {
        $this->markTestIncomplete('testCreateElementFile not implemented.');
    }

/**
 * testGetClient method
 *
 * @return void
 */
    public function testGetClient()
    {
        $this->markTestIncomplete('testGetClient not implemented.');
    }

/**
 * testGetFolderId method
 *
 * @return void
 */
    public function testGetFolderId()
    {
        $this->markTestIncomplete('testGetFolderId not implemented.');
    }

/**
 * testSend method
 *
 * @return void
 */
    public function testSend()
    {
        $this->markTestIncomplete('testSend not implemented.');
    }

/**
 * testFileFusionProjet method
 *
 * @return void
 */
    public function testFileFusionProjet()
    {
        $this->markTestIncomplete('testFileFusionProjet not implemented.');
    }

/**
 * testFileFusionSeance method
 *
 * @return void
 */
    public function testFileFusionSeance()
    {
        $this->markTestIncomplete('testFileFusionSeance not implemented.');
    }
}
