<?php
App::uses('ExportGedSeancesController', 'Controller');
App::uses('', 'Cakeflow.Model');

require_once(APP . DS . 'Vendor' . DS . 'cmis_repository_wrapper.php');
/**
 * ExportGedSeancesController Test Case
 */
class ExportGedSeancesControllerTest extends ControllerTestCase {

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.acteur',
        'app.typeacteur',
        'app.acteur_service',
        //Deliberation
        'app.typeacte',
        'app.deliberation',
        'app.deliberation_seance',
        'app.seance',
        'app.typeseance',
        'app.deliberation_typeseance',
        'app.collectivite',
        'app.theme',
        'app.nature',
        'app.nature_typologiepiece',
        'app.typologiepiece',
        'app.nomenclature',
        'app.listepresence',
        'app.vote',
        'app.annexe',
        'app.historique',
        'app.user',
        'app.service',
        'app.infosupdef',
        'app.infosup',
        'app.tdt_message',
        //Workflow
        'app.circuit_user',
        'app.circuit',
        'app.etape',
        'app.composition',
        'app.visa',
        'app.traitement',
        'app.token_method'
    ];

    public function setUp()
    {
        parent::setUp();
        // Simuler le HTTP_HOST
        $_SERVER['HTTP_HOST'] = 'localhost';
        Configure::write('Config.database', 'test');

        Configure::write('USE_GED', true);
    }

    /**
     * Méthode exécutée après chaque test.
     * @return void
     */
    public function tearDown()
    {
        unset($_SERVER['HTTP_HOST']);
        parent::tearDown();
    }



    /**
     * testSendToGed method
     *
     * @return void
     */
    public function testSendToGedAutreActeCMIS()
    {
        $this->markTestSkipped('must be revisited.');
        Configure::write('GED', 'CMIS');
        Configure::write('GED_XML_VERSION', 4);

        $clientMock = $this->createMock(CMISService::class);
        $clientMock->method('getFolderTree')->willReturn(null);
        $clientMock->method('createFolder')->willReturn(uniqid('folder', false));

        $ExportGedSeances = $this->generate('ExportGedSeances', [
            'components' => [
                'Progress' => ['at'],
                'Acl',
                'ExportGedCMIS' => [
                    'CMISService', 'getClient', 'send', 'getFolderId', 'deletetoGed', 'fileFusionSeance', 'fileFusionProjet'
                ],
                'SeanceTools',
            ]
        ]);
        $ExportGedSeances->ExportGedCMIS
            ->method('getClient')
            ->willReturn($clientMock);
        $ExportGedSeances->ExportGedCMIS
            ->method('send');
        $ExportGedSeances->ExportGedCMIS
            ->method('getFolderId')
            ->willReturn('1');
        $ExportGedSeances->ExportGedCMIS
            ->method('deletetoGed')
            ->willReturn(true);
        $ExportGedSeances->ExportGedCMIS
            ->method('fileFusionSeance')
            ->willReturn(file_get_contents(TESTS . 'Data' . DS .'AnnexFixture.pdf'));
        $ExportGedSeances->ExportGedCMIS
            ->method('fileFusionProjet')
            ->willReturn(file_get_contents(TESTS . 'Data' . DS .'AnnexFixture.pdf'));
        $ExportGedSeances->Progress
            ->method('at')
            ->with(self::anything(), self::callback(function ($string): bool {
                self::assertContains(
                    $string,
                    [
                        'Création du répertoire d\'export de séance dans la ged...',
                        'Création du répertoire de séance...',
                        'Recherche de la séance en base de données...',
                        'Création des dossiers...',
                        'Création du fichier XML...',
                        'Génération de la convocation...',
                        'Génération de l\'ordre du jour...',
                        'Ajout du PV sommaire...',
                        'Ajout du PV complet...',
                        'Ajout des informations supplémentaires de séance...',
                        'Ajout des délibérations...',
                        'Le dossier "Conseil_Municipal_dimanche_16_juin_2024_a_20_h_00"'
                        .' est préparé pour l\'envoi (Dépôt n°1)',
                        'Envoi des fichiers'
                    ]
                );

                return true;
            }));

        $data = [
            'Deliberation' => [
                10 => [
                    'send' => 1
                ]
            ]
        ];
        $this->testAction(
            '/ExportGedSeances/sendToGed/4/token',
            ['data' => $data, 'method' => 'post']
        );
    }
}
