<?php
App::uses('VotesController', 'Controller');
App::uses('Traitement', 'Cakeflow.Model');
App::uses('Visa', 'Cakeflow.Model');
App::uses('CakeflowAppModel', 'Cakeflow.Model');
App::uses('ControllerTrait', 'Test/Case/Controller');
App::uses('VoteService', 'Lib/Service');

/**
 * VotesController Test Case
 */
class VotesControllerTest extends ControllerTestCase
{
    use ControllerTrait;
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        //Deliberation
        'app.Acteur',
        'app.Typeacteur',
        'app.Deliberation',
        'app.DeliberationSeance',
        'app.Seance',
        'app.Typeacte',
        'app.Typeseance',
        'app.DeliberationTypeseance',
        'app.Typeacte',
        'app.Collectivite',
        'app.Theme',
        'app.Nature',
        'app.Nomenclature',
        'app.Listepresence',
        'app.ListepresenceGlobale',
        'app.Vote',
        'app.Annexe',
        'app.Historique',
        'app.User',
        'app.Service',
        //Workflow
        'app.circuit_user',
        'app.circuit',
        'app.etape',
        'app.composition',
        'app.visa',
        'app.traitement',
        'app.token_method'
    ];

    public function setUp()
    {
        parent::setUp();
        // Simuler le HTTP_HOST
        $_SERVER['HTTP_HOST'] = 'localhost';
        Configure::write('Config.database', 'test');
        Configure::write('Config.tenantName', 'test');
        $this->Deliberation = ClassRegistry::init('Deliberation');
        $this->VoteService = new VoteService();
        $this->VoteService->ListePresenceGlobale->useDbConfig = 'test';
    }

    public function tearDown()
    {
        unset($_SERVER['HTTP_HOST']);
        parent::tearDown();
    }

    /**
     * testIndex method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestSkipped('must be revisited.');
        $Votes = $this->generate('Votes', [
            'methods' => [
                'isAuthorized',
                'beforeFilter',
                //'afficherListePresentsGlobale'
            ],
            'models' => [
//                'Aro' => ''
            ],
            'components' => [
                'Acl',
                'SeanceTools',
            ]
        ]);

        $Votes->Acl->method('check')->willReturn(true);
        $Votes->SeanceTools
            ->expects($this->once())
            ->method('checkTypeSeance');

        $Votes->VoteService = $this->VoteService;

        $this->testAction('/votes/index/1');
        $this->assertInternalType('array', $this->vars['seance']);
        $this->assertNotEmpty($this->vars['seance']);
    }
//
///**
// * testEdit method
// *
// * @return void
// */
//    public function testEdit()
//    {
//        $this->markTestIncomplete('testEdit not implemented.');
//    }
//
///**
// * testDelete method
// *
// * @return void
// */
//    public function testDelete()
//    {
//        $Votes = $this->generate('Votes', [
//            'methods' => [
//                'isAuthorized',
//                'beforeFilter'
//            ],
//            'components' => [
//                'Acl',
//                'SeanceTools',
//                'Flash'
//            ]
//        ]);
//
//        $Votes->Acl->method('check')->willReturn(true);
//        $Votes->SeanceTools
//            ->expects($this->once())
//            ->method('checkTypeSeance');
//        $Votes->Flash
//            ->method('set');
//
//        $result = $this->testAction(
//            '/votes/delete/4/7',
//            ['return' => 'contents']
//        );
//
//        $this->Deliberation->id = 1;
//        $deliberation = $this->Deliberation->find('first', [
//            'conditions' => ['id' => 7],
//            'recursive' => -1
//        ]);
//        $this->assertEquals(
//            2,
//            $deliberation['Deliberation']['etat'],
//            var_export($deliberation['Deliberation']['etat'], true)
//        );
//    }
//
///**
// * testListerPresents method
// *
// * @return void
// */
//    public function testListerPresents()
//    {
//        $this->markTestIncomplete('testListerPresents not implemented.');
//    }
//
///**
// * testCopyFromPrevious method
// *
// * @return void
// */
//    public function testCopyFromPrevious()
//    {
//        $this->markTestIncomplete('testCopyFromPrevious not implemented.');
//    }
//
///**
// * testDownloadVotesElectroniqueJson method
// *
// * @return void
// */
//    public function testDownloadVotesElectroniqueJson()
//    {
//        $this->markTestIncomplete('testDownloadVotesElectroniqueJson not implemented.');
//    }
//
///**
// * testSendSeanceVotesToIdelibre method
// *
// * @return void
// */
//    public function testSendSeanceVotesToIdelibre()
//    {
//        $this->markTestIncomplete('testSendSeanceVotesToIdelibre not implemented.');
//    }
//
///**
// * testImportVotesIdelibre method
// *
// * @return void
// */
//    public function testImportVotesIdelibre()
//    {
//        $this->markTestIncomplete('testImportVotesIdelibre not implemented.');
//    }
}
