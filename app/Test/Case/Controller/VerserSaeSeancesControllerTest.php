<?php
App::uses('VerserSaeSeancesController', 'Controller');

/**
 * VerserSaeSeancesController Test Case
 */
class VerserSaeSeancesControllerTest extends ControllerTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.seance',
        'app.typeseance',
        'app.acteur',
        'app.deliberation',
        'app.seance',
        'app.deliberation_seance',
        'app.service',
        'app.theme',
        'app.typeacte',
        'app.nature',
        'app.nature_typologiepiece',
        'app.typologiepiece',
        'app.user',
        'app.Plugin/ModelOdtValidator/Modeltemplate',
        'app.Plugin/ModelOdtValidator/Modeltype',
        'app.annexe'
    ];

    /**
     * Méthode exécutée avant chaque test.
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        // Simuler le HTTP_HOST
        $_SERVER['HTTP_HOST'] = 'localhost';
        Configure::write('Config.database', 'test');
    }

    /**
     * Méthode exécutée après chaque test.
     * @return void
     */
    public function tearDown()
    {
        unset($_SERVER['HTTP_HOST']);
        parent::tearDown();
    }

    /**
     * testSendToSae method
     *
     * @return void
     */
    public function testSendToSae()
    {
        $this->markTestSkipped('must be revisited.');
        $verserSaeSeances = $this->generate('VerserSaeSeances', [
            'methods' => [
                'isAuthorized',
                'beforeRender',
                'beforeFilter',
            ],
            'models' => [
                'Seance'=> ['fusion'],
                'Deliberation'=> ['fusion'],
                'Infosup',
            ],
            'components' => [
                'RequestHandler',
                'Progress',
                'Conversion',
                'VersementSae' => ['send'],
                'Progress' => ['at','start','stop','redirect', 'error'],
            ]
        ]);

        $sae = $this->createMock(Sae::class);
        $verserSaeSeances->VersementSae->setSae($sae);
        $verserSaeSeances->VersementSae
            ->method('send')
            ->willReturn('10caracmax');

        $result = $this->testAction('/VerserSaeSeances/sendToSae/5/token', [
            'method' => 'GET',
            'return' => 'view'
        ]);
    }

    /**
     * testSendToSae method
     *
     * @return void
     */
    public function testSendToSaeError413()
    {
        $this->markTestSkipped('must be revisited.');
        $verserSaeSeances = $this->generate(
            'VerserSaeSeances',
            [
                'methods' => [
                    'isAuthorized',
                    'beforeRender',
                    'beforeFilter',
                    ],
                'models' => [
                    'Seance'=> ['fusion'],
                    'Deliberation'=> ['fusion'],
                    'Infosup',
                ],
                'components' => [
                    'RequestHandler',
                    'Progress',
                    'Conversion',
                    'VersementSae' => ['send'],
                    'Progress' => ['at','start','stop','redirect', 'error'],
                ]
            ]
        );

        $sae = $this->createMock(Sae::class);
        $verserSaeSeances->VersementSae->setSae($sae);

        $verserSaeSeances->VersementSae
            ->method('send')
            ->will(self::throwException(new CakeException('Request Entity Too Large', 413)));

        $verserSaeSeances->Progress
            ->method('error')
            ->with(self::callback(function (Exception $exception): bool {
                self::assertEquals('413', $exception->getCode());

                return true;
            }));

        $result = $this->testAction('/VerserSaeSeances/sendToSae/5/token', [
            'method' => 'GET',
            'return' => 'view'
        ]);
    }
}
