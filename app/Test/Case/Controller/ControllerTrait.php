<?php

trait ControllerTrait
{
    public function assertFlash(PHPUnit_Framework_MockObject_MockObject $controller, $messages):
    PHPUnit_Framework_MockObject_MockObject
    {
        $controller->Flash
            ->method('set')
            ->with(self::callback(static function ($string) use ($messages) : bool {
                self::assertContains(
                    $string,
                    $messages
                );

                return true;
            }));

        return $controller->Flash;
    }
}
