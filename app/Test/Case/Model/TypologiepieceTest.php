<?php
App::uses('Typologiepiece', 'Model');

/**
 * Typologiepiece Test Case
 */
class TypologiepieceTest extends CakeTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.typologiepiece',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        Configure::write('Config.database', 'test');
        $this->Typologiepiece = ClassRegistry::init('Typologiepiece');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Typologiepiece);

        parent::tearDown();
    }

    /**
     * testGetTypologieNamebyCode method
     *
     * @return void
     */
    public function testGetTypologieNamebyCode()
    {
        $this->markTestIncomplete('testGetTypologieNamebyCode not implemented.');
    }

    /**
     * testGetTypologiePieceByTypeActe method
     *
     * @return void
     */
    public function testGetTypologiePieceByTypeActe()
    {
        $this->markTestIncomplete('testGetTypologiePieceByTypeActe not implemented.');
    }

    /**
     * testGetTypologiePieceIdByTypeActe method
     *
     * @return void
     */
    public function testGetTypologiePieceIdByTypeActe()
    {
        $this->markTestIncomplete('testGetTypologiePieceIdByTypeActe not implemented.');
    }

    /**
     * testSaveTypesPJNatureActe method
     *
     * @return void
     */
    public function testSaveTypesPJNatureActe()
    {
        $this->markTestIncomplete('testSaveTypesPJNatureActe not implemented.');
    }
}
