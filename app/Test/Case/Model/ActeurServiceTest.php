<?php
App::uses('ActeurService', 'Model');

/**
 * ActeurService Test Case
 */
class ActeurServiceTest extends CakeTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.acteur_service',
        'app.acteur',
        'app.typeacteur',
        'app.service'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        Configure::write('Config.database', 'test');
        $this->ActeurService = ClassRegistry::init('ActeurService');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ActeurService);

        parent::tearDown();
    }

    public function testFindNatureTyplogiepiece()
    {
        $acteurService = $this->ActeurService->find('first', [
            'contain' => ['Acteur.id','Service.id'],
            'conditions' => ['acteur_id' => 1],
            'recursive' => -1
        ]);
        $expected = [
            'id' => 1,
            'acteur_id' => 1,
            'service_id' => 1
        ];
        $this->assertEquals($expected, $acteurService['ActeurService'], var_export($acteurService, true));
    }
}
