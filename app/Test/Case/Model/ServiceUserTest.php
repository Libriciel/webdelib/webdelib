<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('ServiceUser', 'Model');
App::uses('CakeTime', 'Utility');

/**
 * Classe ServiceUserTest.
 * @version 4.3
 * @package app.Test.Case.Model
 *
 */
class ServiceUserTest extends CakeTestCase
{

    /**
     * Fixtures associated with this test case
     *
     * @version 4.3
     * @var array
     */
    public $fixtures = [
      /*  'app.Service',
        'app.ServiceUser',
        'app.User',
        'app.Aro',
        'app.Aco',
        'app.AroAco'*/
    ];

    public function setUp()
    {
        parent::setUp();
        Configure::write('Config.database', 'test');
        $this->ServiceUser = ClassRegistry::init('ServiceUser');
    }

    /**
     * Méthode exécutée avant chaque test.
     *
     * @version 4.3
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->ServiceUser);
    }

    /**
     * Méthode exécutée après chaque test.
     *
     * @version 4.3
     * @return void
     */
    /* public function testFusionSeance() {
      $this->ServiceUser->fusion(4, 1);
      $result = $this->ServiceUser->find('all', array(
      'fields' => array('ServiceUser.user_id', 'ServiceUser.service_id'),
      'conditions' => array('OR' => array('user_id' => 4, 'service_id' => 1)),
      'recursive' => -1,
      'order' => 'id ASC')
      );

      $expected = array(
      array('ServiceUser' => array('user_id' => 1, 'service_id' => 1)),
      array('ServiceUser' => array('user_id' => 7, 'service_id' => 1)),
      array('ServiceUser' => array('user_id' => 5, 'service_id' => 1)),
      array('ServiceUser' => array('user_id' => 6, 'service_id' => 1))
      );

      $this->assertEquals($result, $expected, var_export($this->ServiceUser->validationErrors, true));
      } */

    /**
     * Méthode exécutée après chaque test.
     *
     * @version 4.3
     * @return void
     */
    public function testFusionSeanceEgal()
    {
        //false, 'Impossible de fusionner le même service'
        // $this->expectException('Exception', 'Impossible de fusionner le même service');
        //
        // $this->ServiceUser->fusion(1, 1);
        $this->assertTrue(true);
    }

    /**
     * Méthode exécutée après chaque test.
     *
     * @version 4.3
     * @return void
     */
    public function testFusionSeanceParent()
    {
        //false, 'Impossible de fusionner le même service'
        // $this->expectException('Exception', 'Impossible de fusionner ce service : il possède au moins un service');
        //
        // $this->ServiceUser->fusion(1, 2);
        $this->assertTrue(true);
    }

    /**
     * Méthode exécutée après chaque test.
     *
     * @version 4.3
     * @return void
     */
    public function testFusionSeanceNotExist()
    {
        //false, 'Impossible de fusionner le même service'
        // $this->expectException('Exception', 'Invalide ids pour fusionner le service');
        //
        // $this->ServiceUser->fusion(999, 1);
        $this->assertTrue(true);
    }
}
