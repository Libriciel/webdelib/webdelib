<?php
App::uses('Deliberation', 'Model');

/**
 * Deliberation Test Case
 */
class DeliberationTest extends CakeTestCase
{

/**
 * Fixtures
 *
 * @var array
 */
    public $fixtures = [
        'app.Deliberation',
        'app.DeliberationUser',
//        'app.deliberation_typeseance',
        'app.deliberation_seance',
//        'app.service',
        'app.user',
//        'app.profil',
//        'app.role',
//        'app.infosupdef',
//        'app.infosup',
        'app.seance',
        'app.typeseance',
        'app.compteur',
        'app.sequence',
        'app.Plugin/ModelOdtValidator/Modeltemplate',
        'app.Plugin/ModelOdtValidator/Modeltype',
//        'app.typeseance_typeacte',
        'app.typeacte',
        'app.nature',
//        'app.typologiepiece',
//        'app.nature_typologiepiece',
//        'app.typeacteur',
//        'app.acteur',
//        'app.acteur_service',
//        'app.typeseance_typeacteur',
//        'app.typeseance_acteur',
//        'app.infosuplistedef',
//        'app.infosupdef_profil',
//        'app.aro',
//        'app.aco',
        'app.historique',
//        'app.composition',
//        'app.etape',
//        'app.circuit',
//        'app.traitement',
//        'app.visa',
//        'app.service_user',
//        'app.circuit_user',
//        'app.service_user',
//        'app.theme',
//        'app.tdt_message',
//        'app.annexe',
//        'app.commentaire',
//        'app.listepresence',
//        'app.vote',
    ];

/**
 * setUp method
 *
 * @return void
 */
    public function setUp()
    {
        parent::setUp();
        Configure::write('Config.database', 'test');
        $this->Deliberation = ClassRegistry::init('Deliberation');
    }

/**
 * tearDown method
 *
 * @return void
 */
    public function tearDown()
    {
        unset($this->Deliberation);

        parent::tearDown();
    }

/**
 * testEstModifiable method
 *
 * @return void
 */
    public function testEstModifiable()
    {
        $canEditTrue = $this->Deliberation->estModifiable(1, 1, true);
        $this->assertTrue($canEditTrue);

        $canEditTrue = $this->Deliberation->estModifiable(1, 1, false);
        $this->assertTrue($canEditTrue);
    }

/**
 * testGetCurrentPosition method
 *
 * @return void
 */
    public function testGetCurrentPosition()
    {
        $this->markTestIncomplete('testGetCurrentPosition not implemented.');
    }

/**
 * testGetCurrentSeances method
 *
 * @return void
 */
    public function testGetCurrentSeances()
    {
        $this->markTestIncomplete('testGetCurrentSeances not implemented.');
    }

/**
 * testIsFirstDelib method
 *
 * @return void
 */
    public function testIsFirstDelib()
    {
        $this->markTestIncomplete('testIsFirstDelib not implemented.');
    }

/**
 * testChangeDateAR method
 *
 * @return void
 */
    public function testChangeDateAR()
    {
        $this->markTestIncomplete('testChangeDateAR not implemented.');
    }

/**
 * testGetModelId method
 *
 * @return void
 */
    public function testGetModelId()
    {
        $this->markTestIncomplete('testGetModelId not implemented.');
    }

/**
 * testGetModelForSeance method
 *
 * @return void
 */
    public function testGetModelForSeance()
    {
        $this->markTestIncomplete('testGetModelForSeance not implemented.');
    }

/**
 * testRefusDossier method
 *
 * @return void
 */
    public function testRefusDossier()
    {
        $this->markTestIncomplete('testRefusDossier not implemented.');
    }

/**
 * testCanSaveTypeacte method
 *
 * @return void
 */
    public function testCanSaveTypeacte()
    {
        $this->markTestIncomplete('testCanSaveTypeacte not implemented.');
    }

/**
 * testMajDelibRatt method
 *
 * @return void
 */
    public function testMajDelibRatt()
    {
        $this->markTestIncomplete('testMajDelibRatt not implemented.');
    }

/**
 * testReOrdonnePositionSeance method
 *
 * @return void
 */
    public function testReOrdonnePositionSeance()
    {
        $this->markTestIncomplete('testReOrdonnePositionSeance not implemented.');
    }

/**
 * testSaveDelibRattachees method
 *
 * @return void
 */
    public function testSaveDelibRattachees()
    {
        $this->markTestIncomplete('testSaveDelibRattachees not implemented.');
    }

/**
 * testSaveInfoDelibRattachees method
 *
 * @return void
 */
    public function testSaveInfoDelibRattachees()
    {
        $this->markTestIncomplete('testSaveInfoDelibRattachees not implemented.');
    }

/**
 * testSaveDebat method
 *
 * @return void
 */
    public function testSaveDebat()
    {
        $this->markTestIncomplete('testSaveDebat not implemented.');
    }

/**
 * testSupprimer method
 *
 * @return void
 */
    public function testSupprimer()
    {
        $this->markTestIncomplete('testSupprimer not implemented.');
    }

/**
 * testGetSeancesid method
 *
 * @return void
 */
    public function testGetSeancesid()
    {
        $this->markTestIncomplete('testGetSeancesid not implemented.');
    }

/**
 * testGetSeanceDeliberanteId method
 *
 * @return void
 */
    public function testGetSeanceDeliberanteId()
    {
        $this->markTestIncomplete('testGetSeanceDeliberanteId not implemented.');
    }

/**
 * testGetNbSeances method
 *
 * @return void
 */
    public function testGetNbSeances()
    {
        $this->markTestIncomplete('testGetNbSeances not implemented.');
    }

/**
 * testGetSeancesFromArray method
 *
 * @return void
 */
    public function testGetSeancesFromArray()
    {
        $this->markTestIncomplete('testGetSeancesFromArray not implemented.');
    }

/**
 * testGetTypeseancesFromArray method
 *
 * @return void
 */
    public function testGetTypeseancesFromArray()
    {
        $this->markTestIncomplete('testGetTypeseancesFromArray not implemented.');
    }

/**
 * testGetPosition method
 *
 * @return void
 */
    public function testGetPosition()
    {
        $this->markTestIncomplete('testGetPosition not implemented.');
    }

/**
 * testAfficherListePresents method
 *
 * @return void
 */
    public function testAfficherListePresents()
    {
        $this->markTestIncomplete('testAfficherListePresents not implemented.');
    }

/**
 * testBuildFirstList method
 *
 * @return void
 */
    public function testBuildFirstList()
    {
        $this->markTestIncomplete('testBuildFirstList not implemented.');
    }

/**
 * testCopyFromPreviousList method
 *
 * @return void
 */
    public function testCopyFromPreviousList()
    {
        $this->markTestIncomplete('testCopyFromPreviousList not implemented.');
    }

/**
 * testGetMultidelibs method
 *
 * @return void
 */
    public function testGetMultidelibs()
    {
        $this->markTestIncomplete('testGetMultidelibs not implemented.');
    }

/**
 * testGetMultidelibParent method
 *
 * @return void
 */
    public function testGetMultidelibParent()
    {
        $this->markTestIncomplete('testGetMultidelibParent not implemented.');
    }

/**
 * testCopyPositionsDelibs method
 *
 * @return void
 */
    public function testCopyPositionsDelibs()
    {
        $this->markTestIncomplete('testCopyPositionsDelibs not implemented.');
    }

/**
 * testIsDeliberant method
 *
 * @return void
 */
    public function testIsDeliberant()
    {
        $this->markTestIncomplete('testIsDeliberant not implemented.');
    }

/**
 * testReportePositionToCommissions method
 *
 * @return void
 */
    public function testReportePositionToCommissions()
    {
        $this->markTestIncomplete('testReportePositionToCommissions not implemented.');
    }

/**
 * testSetHistorique method
 *
 * @return void
 */
    public function testSetHistorique()
    {
        $this->markTestIncomplete('testSetHistorique not implemented.');
    }

/**
 * testSetCommentaire method
 *
 * @return void
 */
    public function testSetCommentaire()
    {
        $this->markTestIncomplete('testSetCommentaire not implemented.');
    }

/**
 * testMajActesParapheur method
 *
 * @return void
 */
    public function testMajActesParapheur()
    {
        $this->markTestIncomplete('testMajActesParapheur not implemented.');
    }

/**
 * testMajActeParapheur method
 *
 * @return void
 */
    public function testMajActeParapheur()
    {
        $this->markTestIncomplete('testMajActeParapheur not implemented.');
    }

/**
 * testMajSignaturesPastell method
 *
 * @return void
 */
    public function testMajSignaturesPastell()
    {
        $this->markTestIncomplete('testMajSignaturesPastell not implemented.');
    }

/**
 * testMajSignaturePastell method
 *
 * @return void
 */
    public function testMajSignaturePastell()
    {
        $this->markTestIncomplete('testMajSignaturePastell not implemented.');
    }

/**
 * testMajPdfTamponne method
 *
 * @return void
 */
    public function testMajPdfTamponne()
    {
        $this->markTestIncomplete('testMajPdfTamponne not implemented.');
    }

/**
 * testUpdateTdtFiles method
 *
 * @return void
 */
    public function testUpdateTdtFiles()
    {
        $this->markTestIncomplete('testUpdateTdtFiles not implemented.');
    }

/**
 * testUpdateTdtStatus method
 *
 * @return void
 */
    public function testUpdateTdtStatus()
    {
        $this->markTestIncomplete('testUpdateTdtStatus not implemented.');
    }

/**
 * testMajAR method
 *
 * @return void
 */
    public function testMajAR()
    {
        $this->markTestIncomplete('testMajAR not implemented.');
    }

/**
 * testMajPdf method
 *
 * @return void
 */
    public function testMajPdf()
    {
        $this->markTestIncomplete('testMajPdf not implemented.');
    }

/**
 * testUpdateTdtMinisterialMails method
 *
 * @return void
 */
    public function testUpdateTdtMinisterialMails()
    {
        $this->markTestIncomplete('testUpdateTdtMinisterialMails not implemented.');
    }

/**
 * testMajEchangesTdt method
 *
 * @return void
 */
    public function testMajEchangesTdt()
    {
        $this->markTestIncomplete('testMajEchangesTdt not implemented.');
    }

/**
 * testChercherVersionSuivante method
 *
 * @return void
 */
    public function testChercherVersionSuivante()
    {
        $this->markTestIncomplete('testChercherVersionSuivante not implemented.');
    }

/**
 * testGetModelTemplateId method
 *
 * @return void
 */
    public function testGetModelTemplateId()
    {
        $this->markTestIncomplete('testGetModelTemplateId not implemented.');
    }

/**
 * testPrepareTextesFiles method
 *
 * @return void
 */
    public function testPrepareTextesFiles()
    {
        $this->markTestIncomplete('testPrepareTextesFiles not implemented.');
    }

/**
 * testParentNode method
 *
 * @return void
 */
    public function testParentNode()
    {
        $this->markTestIncomplete('testParentNode not implemented.');
    }

/**
 * testBeforeFusion method
 *
 * @return void
 */
    public function testBeforeFusion()
    {
        $this->markTestIncomplete('testBeforeFusion not implemented.');
    }

/**
 * testSetVariablesFusion method
 *
 * @return void
 */
    public function testSetVariablesFusion()
    {
        $this->markTestIncomplete('testSetVariablesFusion not implemented.');
    }

/**
 * testIsVoted method
 *
 * @return void
 */
    public function testIsVoted()
    {
        $this->markTestIncomplete('testIsVoted not implemented.');
    }

/**
 * testGetVoteMethod method
 *
 * @return void
 */
    public function testGetVoteMethod()
    {
        $this->markTestIncomplete('testGetVoteMethod not implemented.');
    }

/**
 * testGetDocumentsForDelegation method
 *
 * @return void
 */
    public function testGetDocumentsForDelegation()
    {
        $this->markTestIncomplete('testGetDocumentsForDelegation not implemented.');
    }

/**
 * testGetDocument method
 *
 * @return void
 */
    public function testGetDocument()
    {
        $this->markTestIncomplete('testGetDocument not implemented.');
    }

/**
 * testGetAnnexesToSend method
 *
 * @return void
 */
    public function testGetAnnexesToSend()
    {
        $this->markTestIncomplete('testGetAnnexesToSend not implemented.');
    }

/**
 * testSetVariablesFusionDeliberations method
 *
 * @return void
 */
    public function testSetVariablesFusionDeliberations()
    {
        $this->markTestIncomplete('testSetVariablesFusionDeliberations not implemented.');
    }

/**
 * testSignatureManuscrite method
 *
 * @return void
 */
    public function testSignatureManuscrite()
    {
        $deliberation = $this->getMockForModel('Deliberation',
            [
                'versioningEnable',
                'getDocument',
                'lastVersion'
            ]
        );
        $deliberation
            ->method('versioningEnable')
            ->willReturn(true);
        $deliberation
            ->method('getDocument')
            ->willReturn(file_get_contents(TESTS . 'Data' . DS .'ActFixture.pdf'));
        $deliberation
            ->method('lastVersion')
            ->willReturn(true);


        $actual = $deliberation->signatureManuscrite(11, 1, CakeTime::format(time(), '%Y-%m-%d 00:00:00'));
        $this->assertTrue($actual);

        $deliberation = $this->Deliberation->find('first', [
            'fields' => ['date_acte'],
            'conditions' => ['id' => 11],
            'recursive' => -1
        ]);
        $expected = CakeTime::format(time(), 'Y-m-d');
        $this->assertEquals(
            $expected,
            CakeTime::format($deliberation['Deliberation']['date_acte'], 'Y-m-d'),
            var_export($deliberation['Deliberation']['date_acte'], true)
        );
    }

/**
 * testGenereNumeroActe method
 *
 * @return void
 */
    public function testGenereNumeroActe()
    {
        $deliberation = $this->getMockForModel('Deliberation', ['versioningEnable', 'getDocument','lastVersion']);
        $deliberation
            ->method('versioningEnable')
            ->willReturn(true);
        $deliberation
            ->method('getDocument')
            ->willReturn(file_get_contents(TESTS . 'Data' . DS .'ActFixture.pdf'));
        $deliberation
            ->method('lastVersion')
            ->willReturn(true);


        $actual = $deliberation->genereNumeroActe(7, 1, true);
        $this->assertTrue($actual);

        $deliberation = $this->Deliberation->find('first', [
            'fields' => ['num_delib'],
            'conditions' => ['id' => 7],
            'recursive' => -1
        ]);
        $expected =  __('C1_%s_101', date('Y', strtotime("-1 day")));
        $this->assertEquals(
            $expected,
            $deliberation['Deliberation']['num_delib'],
            var_export($deliberation['Deliberation']['num_delib'], true)
        );
    }

/**
 * testEnvoyerAuParapheur method
 *
 * @return void
 */
    public function testEnvoyerAuParapheur()
    {
        $this->markTestIncomplete('testEnvoyerAuParapheur not implemented.');
    }

/**
 * testFusion method
 *
 * @return void
 */
    public function testFusion()
    {
        $this->markTestIncomplete('testFusion not implemented.');
    }

/**
 * testFusionToFile method
 *
 * @return void
 */
    public function testFusionToFile()
    {
        $this->markTestIncomplete('testFusionToFile not implemented.');
    }

/**
 * testResetVote method
 *
 * @return void
 */
    public function testResetVote()
    {
        $this->markTestIncomplete('testResetVote not implemented.');
    }

/**
 * testDuplicate method
 *
 * @return void
 */
    public function testDuplicate()
    {
        $this->markTestIncomplete('testDuplicate not implemented.');
    }

/**
 * testSaveSendTdt method
 *
 * @return void
 */
    public function testSaveSendTdt()
    {
        $this->markTestIncomplete('testSaveSendTdt not implemented.');
    }

/**
 * testDelegationCakeflow method
 *
 * @return void
 */
    public function testDelegationCakeflow()
    {
        $this->markTestIncomplete('testDelegationCakeflow not implemented.');
    }

/**
 * testDelegationlinkCakeflow method
 *
 * @return void
 */
    public function testDelegationlinkCakeflow()
    {
        $this->markTestIncomplete('testDelegationlinkCakeflow not implemented.');
    }

/**
 * testComputeDateRetard method
 *
 * @return void
 */
    public function testComputeDateRetard()
    {
        $this->markTestIncomplete('testComputeDateRetard not implemented.');
    }

/**
 * testCheckPresenceTextes method
 *
 * @return void
 */
    public function testCheckPresenceTextes()
    {
        $this->markTestIncomplete('testCheckPresenceTextes not implemented.');
    }

/**
 * testCancelSignature method
 *
 * @return void
 */
    public function testCancelSignature()
    {
        $this->markTestIncomplete('testCancelSignature not implemented.');
    }

/**
 * testIsCancelableForTdt method
 *
 * @return void
 */
    public function testIsCancelableForTdt()
    {
        $this->markTestIncomplete('testIsCancelableForTdt not implemented.');
    }

/**
 * testResetSendTDT method
 *
 * @return void
 */
    public function testResetSendTDT()
    {
        $this->markTestIncomplete('testResetSendTDT not implemented.');
    }

    /**
     * testIsNew method
     *
     * @return void
     */
    public function testIsNewNotValidate()
    {
        $deliberation = $this->getMockForModel('Deliberation', ['versioningEnable']);
        $deliberation->validator()->add('num_delib', [
            'change' => [
                'rule' => ['isNew'],
                'message' => __("Le numéro d'acte doit être modifié.")
            ],
        ]);
        $deliberation->id = 8;
        $actual = $deliberation->save([
            'num_delib' => 'DEL02'
        ]);
        $this->assertFalse($actual);
    }

    /**
     * testIsNew method
     *
     * @return void
     */
    public function testIsNewValidate()
    {
        $deliberation = $this->getMockForModel('Deliberation', ['versioningEnable']);
        $deliberation->validator()->add('num_delib', [
            'change' => [
                'rule' => ['isNew'],
                'message' => __("Le numéro d'acte doit être modifié.")
            ],
        ]);
        $deliberation->id = 8;
        $actual = $deliberation->save([
            'num_delib' => 'DEL02A'
        ]);
        $this->assertEquals(
            'DEL02A',
            $actual['Deliberation']['num_delib'],
            var_export($actual['Deliberation']['num_delib'], true)
        );
    }

/**
 * testIsReadyForTDT method
 *
 * @return void
 */
    public function testIsReadyForTDT()
    {
        $this->markTestIncomplete('testIsReadyForTDT not implemented.');
    }

/**
 * testSendManually method
 *
 * @return void
 */
    public function testSendManually()
    {
        $this->markTestIncomplete('testSendManually not implemented.');
    }

/**
 * testIsSendManually method
 *
 * @return void
 */
    public function testIsSendManually()
    {
        $this->markTestIncomplete('testIsSendManually not implemented.');
    }

/**
 * testIsLocked method
 *
 * @return void
 */
    public function testIsLocked()
    {
        $this->markTestIncomplete('testIsLocked not implemented.');
    }

/**
 * testAbandon method
 *
 * @return void
 */
    public function testAbandon()
    {
        $this->markTestIncomplete('testAbandon not implemented.');
    }

/**
 * testLogicalValuesForOrder method
 *
 * @return void
 */
    public function testLogicalValuesForOrder()
    {
        $this->markTestIncomplete('testLogicalValuesForOrder not implemented.');
    }

/**
 * testIsParamTdtEmptyByActe method
 *
 * @return void
 */
    public function testIsParamTdtEmptyByActe()
    {
        $this->markTestIncomplete('testIsParamTdtEmptyByActe not implemented.');
    }

/**
 * testFileOutputFormatValues method
 *
 * @return void
 */
    public function testFileOutputFormatValues()
    {
        $this->markTestIncomplete('testFileOutputFormatValues not implemented.');
    }
}
