<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Acteur', 'Model');

/**
 * Classe AnnexeTest
 *
 * @package app.Test.Case.Model
 */
class ActeurTest extends CakeTestCase
{

    /**
     * Fixtures associated with this test case
     */
    public $fixtures = [
        'app.typeacteur',
        'app.acteur',
        'app.service',
        'app.acteur_service',
    ];

    public function setUp()
    {
        parent::setUp();
        Configure::write('Config.database', 'test');
        $this->Acteur = ClassRegistry::init('Acteur');
    }

    /**
     * Méthode exécutée avant chaque test.
     *
     * @version 4.3
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->Acteur);
    }

    public function testSelectActeurEluIdParDelegationId(): void
    {
        $actual = $this->Acteur->selectActeurEluIdParDelegationId(1);
        $this->assertEquals(1, $actual);
    }
}
