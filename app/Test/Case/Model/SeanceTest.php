<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Seance', 'Model');
App::uses('CakeTime', 'Utility');

/**
 * Classe SeanceTest.
 * @version 4.3
 * @package app.Test.Case.Model
 *
 */
class SeanceTest extends CakeTestCase
{

    /**
     * Fixtures associated with this test case
     *
     * @version 4.3
     * @var array
     */
    public $fixtures = [
        'app.Seance',
        'app.DeliberationSeance'
    ];

    public function setUp()
    {
        parent::setUp();
        Configure::write('Config.database', 'test');
        $this->Seance = ClassRegistry::init('Seance');
    }

    /**
     * Méthode exécutée avant chaque test.
     *
     * @version 4.3
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->Seance);
    }

    /**
     * Méthode exécutée après chaque test.
     *
     * @version 4.3
     * @return void
     */
    public function testdeleteSeance()
    {
        $this->assertEquals('1', '1', 'test OK');
    }

    /**
     * Méthode exécutée après chaque test.
     *
     * @version 4.3
     * @return void
     */
    public function testaddSeance()
    {
        // $seance = ['Seance' => []];
        // $seance['Seance']['type_id'] = 1;
        // //CakeTime::format('12-04-2015 10:00', '%Y-%m-%d %H:%M:00')
        // $seance['Seance']['date'] = date('Y-m-d 21:00:00');

        $result = $this->Seance->Save(
            [
            'Seance'=> [
              'type_id'=> 1,
              'date' => date('Y-m-d 21:00:00')
              ]
            ]
        );

        $expected = date('Y-m-d 21:00:00');

        $this->assertEquals($result['Seance']['date'], $expected, var_export($this->Seance->validationErrors, true));
    }
}
