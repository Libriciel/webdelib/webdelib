Te  <?php

/**
 * AnnexeTest file
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       webdelib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */
//App::uses('Compteur', 'Model');
App::uses('Sequence', 'Model');
App::uses('CakeTime', 'Utility');

/**
 * Classe CompteurTest
 *
 * @version 4.3
 * @package app.Test.Case.Model
 */
class SequenceTest extends CakeTestCase
{
    /**
     * Fixtures associated with this test case
     *
     * @version 4.3
     * @var array
     */
    public $fixtures = [
        'app.Sequence'
    ];

    /**
     * Méthode exécutée avant chaque test.
     * @version 4.3
     * @access public
     */
    public function setUp()
    {
        parent::setUp();
        Configure::write('Config.database', 'test');
        $this->Sequence = ClassRegistry::init('Sequence');
        //$this->Sequence = $this->getMockForModel('Sequence', ['find','save']);
        //$this->Sequence->Compteur = $this->getMockForModel('Compteur', ['find','save','updateReinit']);
    }

    /**
     * Méthode exécutée avant chaque test.
     * @version 4.3
     * @access public
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->Sequence);
    }

    // public function testResetSequenceEtCompteurs()
    // {
    //     $sequenceEntree=['Sequence'=>['id'=>1,
    //     'nom'=>'test',
    //     'commentaire'=>'test',
    //     'num_sequence'=>10,
    //     'created'=>'1999-01-08',
    //     'modified'=>'1999-01-08']];
    //
    //     $sequenceSortie = $sequenceEntree;
    //     $sequenceSortie['Sequence']['num_sequence']=1;
    //
    //     $compteur=['Compteur'=>['id'=>1,
    //     'nom'=>'test',
    //     'commentaire'=>'test',
    //     'def_compteur'=>'#JJ#_#MM#_#AAAA#_#00#',
    //     'sequence_id'=>1,
    //     'def_reinit'=>'#AAAA#',
    //     'val_reinit'=>'2017',
    //     'created'=>'1999-01-08',
    //     'modified'=>'1999-01-08']];
    //
    //     $date = strtotime('10-11-2018');
    //
    //     $this->Sequence->expects($this->once())
    //             ->method('save')
    //             ->with($sequenceSortie);
    //
    //     $this->Sequence->Compteur->expects($this->once())
    //             ->method('find')
    //             ->with('all', [
    //            'conditions' => ['sequence_id' => 1]
    //         ])
    //             ->will($this->returnValue([$compteur]));
    //
    //     $this->Sequence->Compteur->expects($this->once())
    //             ->method('updateReinit')
    //             ->with($compteur, $date);
    //
    //     $this->Sequence->resetSequenceEtCompteurs($sequenceEntree, $date);
    // }

    public function testIncrement()
    {
        $sequence=$this->Sequence->findById(1, ['id', 'num_sequence']);
        $this->Sequence->increment($sequence['Sequence']['id'], $sequence['Sequence']['num_sequence']);
        $this->Sequence->clear();

        $sequence=$this->Sequence->findById(1, ['id', 'num_sequence']);

        $this->assertEquals(101, $sequence['Sequence']['num_sequence'], var_export($sequence, true));
    }
}
