<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Compteur', 'Model');
App::uses('CakeTime', 'Utility');

/**
 * Classe CompteurTest
 *
 * @version 5.1.2
 * @since 4.3
 *
 * @package app.Test.Case.Model
 */
class CompteurTest extends CakeTestCase
{

    /**
     * Fixtures associated with this test case
     * @version 4.3
     * @access public
     * @var array
     */
    public $fixtures = [
        'app.Compteur',
        'app.Sequence',
        'app.Typeseance'
    ];

    /**
     * Méthode exécutée avant chaque test.
     * @version 4.3
     * @access public
     */
    public function setUp()
    {
        parent::setUp();
        Configure::write('Config.database', 'test');
        $this->Compteur = ClassRegistry::init('Compteur');
        $this->Sequence = ClassRegistry::init('Sequence');
    }

    /**
     * Méthode exécutée avant chaque test.
     * @version 4.3
     * @access public
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->Compteur);
        unset($this->Sequence);
    }

    /**
     * [compteurModifiedById description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    private function compteurModifiedById($id)
    {
        $this->recursive = -1;
        $compteur = $this->Compteur->findById($id, 'modified');

        return CakeTime::format($compteur['Compteur']['modified'], '%Y-%m-%d');
    }

    /**
     * [sequenceValidById description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    private function sequenceValidById($id)
    {
        $this->recursive = -1;
        $compteur = $this->Sequence->findById($id, ['debut_validite','fin_validite']);

        return [
          CakeTime::format($compteur['Sequence']['debut_validite'], '%Y-%m-%d'),
          CakeTime::format($compteur['Sequence']['fin_validite'], '%Y-%m-%d')
        ];
    }

    /**
     * [testGenereCompteurAnneeSansReinitialiser Compteur avec réinitialisation à l\'année à ne pas réinitialiser]
     * @return [type] [description]
     *
     * @version 5.1.2
     */
    public function testGenereCompteurAnneeSansReinitialiser(): void
    {
        $result = $this->Compteur->genereCompteur(1, date('Y-m-d', strtotime('+1 day')));
        $expected = sprintf('C1_%s_101', date('Y', strtotime('+1 day')));
        $this->assertEquals($result, $expected, var_export($result, true));

        $result = $this->Compteur->genereCompteur(1, date('Y-m-d', strtotime('+1 day')));
        $expected = sprintf('C1_%s_102', date('Y', strtotime('+1 day')));
        $this->assertEquals($result, $expected, var_export($result, true));
    }

    /**
     * [testGenereCompteurMoisSansReinitialiser Compteur avec réinitialisation au mois à ne pas réinitialiser]
     * @return [type] [description]
     *
     * @version 5.1.2
     */
    public function testGenereCompteurMoisSansReinitialiser()
    {
        $result = $this->Compteur->genereCompteur(2, date('Y-m-d'));
        $expected = sprintf(
            'C2_%s_%s_101',
            date('Y'),
            date('m')
        );
        $this->assertEquals($result, $expected, var_export($result, true));

        $result = $this->Compteur->genereCompteur(2, date('Y-m-d'));
        $expected = sprintf(
            'C2_%s_%s_102',
            date('Y'),
            date('m')
        );
        $this->assertEquals($result, $expected, var_export($result, true));
    }



    /**
     * [testGenereCompteurJourSansReinitialiser Compteur avec réinitialisation au jour à ne pas réinitialiser]
     * @return [type] [description]
     *
     * @version 5.1.2
     */
    public function testGenereCompteurJourSansReinitialiser()
    {
        $result = $this->Compteur->genereCompteur(3, date('Y-m-d'));
        $expected = sprintf('C3_%s_%s_%s_101', date('Y'), date('m'), date('d'));
        $this->assertEquals($result, $expected, var_export($result, true));

        $result = $this->Compteur->genereCompteur(3, date('Y-m-d'));
        $expected = sprintf('C3_%s_%s_%s_102', date('Y'), date('m'), date('d'));
        $this->assertEquals($result, $expected, var_export($result, true));
    }

    /**
     * [testGenereCompteurJourAvecReinitialiser Compteur avec réinitialisation à l\'année à réinitialiser]
     * @return [type] [description]
     *
     * @version 5.1.2
     */
    public function testGenereCompteurAnneeAvecReinitialiser()
    {
        $result = $this->Compteur->genereCompteur(4, date('Y-m-d'));
        $expected = sprintf('C4_%s_001', date('Y'));
        $this->assertEquals($result, $expected, var_export($result, true));

        $result = $this->Compteur->genereCompteur(4, date('Y-m-d'));
        $expected = sprintf('C4_%s_002', date('Y'));
        $this->assertEquals($result, $expected, var_export($result, true));
    }

    /**
     * [testGenereCompteurJourAvecReinitialiser Compteur avec réinitialisation au mois à réinitialiser]
     * @return [type] [description]
     *
     * @version 5.1.2
     */
    public function testGenereCompteurMoisAvecReinitialiser()
    {
        $result = $this->Compteur->genereCompteur(5, date('Y-m-d'));
        $expected = sprintf('C5_%s_%s_001', date('Y'), date('m'));
        $this->assertEquals($result, $expected, var_export($result, true));

        $result = $this->Compteur->genereCompteur(5, date('Y-m-d'));
        $expected = sprintf('C5_%s_%s_002', date('Y'), date('m'));
        $this->assertEquals($result, $expected, var_export($result, true));
    }

    /**
     * [testGenereCompteurJourAvecReinitialiser Compteur avec réinitialisation au jour à réinitialiser]
     * @return [type] [description]
     *
     * @version 5.1.2
     */
    public function testGenereCompteurJourAvecReinitialiser()
    {
        $result = $this->Compteur->genereCompteur(6, date('Y-m-d'));
        $expected = sprintf('C6_%s_%s_%s_001', date('Y'), date('m'), date('d'));
        $this->assertEquals($result, $expected, var_export($result, true));

        $result = $this->Compteur->genereCompteur(6, date('Y-m-d'));
        $expected = sprintf('C6_%s_%s_%s_002', date('Y'), date('m'), date('d'));
        $this->assertEquals($result, $expected, var_export($result, true));
    }

    /**
     * [testGenereCompteurJourAvecReinitialiser
     * Compteur avec réinitialisation à l'année déjà réinitialisé par un autre compteur]
     * @return [type] [description]
     *
     * @version 5.1.2
     */
    public function testGenereCompteurAnneDejaReinitialiserViaAutreCompteur()
    {
        $result = $this->Compteur->genereCompteur(7, date('Y-m-d'));
        $expected = sprintf('C7_%s_001', date('Y'));
        $this->assertEquals($result, $expected, var_export($result, true));

        $result = $this->Compteur->genereCompteur(7, date('Y-m-d'));
        $expected = sprintf('C7_%s_002', date('Y'));
        $this->assertEquals($result, $expected, var_export($result, true));
    }

    /**
     * [testGenereCompteurJourAvecReinitialiser
     * Compteur avec réinitialisation au jour déjà réinitialisé par un autre compteur]
     * @return [type] [description]
     *
     * @version 5.1.2
     */
    public function testGenereCompteurMoisDejaReinitialiserViaAutreCompteur()
    {
        $result = $this->Compteur->genereCompteur(8, date('Y-m-d'));
        $expected = sprintf('C8_%s_%s_001', date('Y'), date('m'));
        $this->assertEquals($result, $expected, var_export($result, true));

        $this->assertEquals($this->compteurModifiedById(8), date('Y-m-d'));

        $result = $this->Compteur->genereCompteur(8, date('Y-m-d'));
        $expected = sprintf('C8_%s_%s_002', date('Y'), date('m'));
        $this->assertEquals($result, $expected, var_export($result, true));
    }

    /**
     * [testGenereCompteurJourAvecReinitialiser
     * Compteur avec réinitialisation au jour déjà réinitialisé par un autre compteur]
     * @return [type] [description]
     *
     * @version 5.1.2
     */
    public function testGenereCompteurJourDejaReinitialiserViaAutreCompteur()
    {
        $result = $this->Compteur->genereCompteur(9, date('Y-m-d'));
        $expected = sprintf('C9_%s_%s_%s_001', date('Y'), date('m'), date('d'));
        $this->assertEquals($result, $expected, var_export($result, true));

        $this->assertEquals($this->compteurModifiedById(9), date('Y-m-d'));

        $result = $this->Compteur->genereCompteur(9, date('Y-m-d', strtotime('+1 day')));
        $expected = sprintf('C9_%s_001', date('Y_m_d', strtotime('+1 day')));
        $this->assertEquals($result, $expected, var_export($result, true));
    }


    /**
     * Cas au limite, enregistrement inexistant -> Exception Méthode exécutée avant chaque test.
     *
     */
    public function testGenereCompteurAnneePrecedenteSansReinitialiserNotFoundException(): void
    {
        $compteur = $this->Compteur->find('first', [
            'contain' => ['Sequence.debut_validite'],
            'conditions' => ['Compteur.id' => 1],
            'recursive' => -1
        ]);
        $this->expectExceptionMessage(
            sprintf(
                'Dernière réinitialisation : %s, date demandée %s',
                $compteur['Sequence']['debut_validite'],
                date('Y-m-d', strtotime('-1 year')),
            )
        );
        $this->Compteur->genereCompteur(1, date('Y-m-d', strtotime('-1 year')));
    }

    // /**
    //   * Cas au limite, enregistrement inexistant -> Exception Méthode exécutée avant chaque test.
    //   * @version 4.3
    //   * @access public
    //   * @expectedException NotFoundException
    // * @expectedExceptionMessage Dernière réinitialisation
    //   */
    // public function test_genereCompteur_mois_puismoisprecedent()
    // {
    //     $result = $this->Compteur->genereCompteur(6, '2015-08-03');
    //     $result = $this->Compteur->genereCompteur(6, '2015-07-03');
    // }
    //
    // /**
    //   * Cas au limite, enregistrement inexistant -> Exception Méthode exécutée avant chaque test.
    //   * @version 4.3
    //   * @access public
    //   * @expectedException NotFoundException
    //   * @expectedExceptionMessage Dernière réinitialisation
    //   */
    // public function test_genereCompteur_jour_puisjourprecedent()
    // {
    //     $result = $this->Compteur->genereCompteur(2, '2015-02-03');
    //     $result = $this->Compteur->genereCompteur(2, '2014-02-02');
    // }

    /**
     * [testGenereCompteurInexistant
     * Cas au limite, enregistrement inexistant -> Exception Méthode exécutée avant chaque test]
     */
    public function testGenereCompteurInexistant()
    {

        $this->expectException(NotFoundException::class);
        $this->expectExceptionMessage(__("ID:\"%s\" de compteur invalide", 600));
        // On passe un id null donc exeption car l'id n'éxiste pas
        $result = $this->Compteur->genereCompteur(600);
    }

    /**
     * [testGenereCompteurIdInexistant
     * Cas au limite, enregistrement inexistant -> Exception Méthode exécutée avant chaque test]
     */
    public function testGenereCompteurIdInexistant()
    {
        $this->expectExceptionMessage("ID:\"1500000\" de compteur invalide");
        //On passe l'id = 1500000, mais il existe pas
        $result = $this->Compteur->genereCompteur(1500000, date('Y-m-d'));
    }


    public function testGenereCompteurAvecMemeSequenceSansReinitialiser()
    {
        $result = $this->Compteur->genereCompteur(10, date('Y-m-d'));
        $expected = sprintf('C10_%s_1001', date('Y'));
        $this->assertEquals($result, $expected, var_export($result, true));

        $result = $this->Compteur->genereCompteur(11, date('Y-m-d'));
        $expected = sprintf('C11_%s_%s_1002', date('Y'), date('m'));
        $this->assertEquals($result, $expected, var_export($result, true));

        $result = $this->Compteur->genereCompteur(12, date('Y-m-d'));
        $expected = sprintf('C12_%s_%s_%s_1003', date('Y'), date('m'), date('d'));
        $this->assertEquals($result, $expected, var_export($result, true));
    }
}
