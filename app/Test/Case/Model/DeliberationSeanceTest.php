<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Deliberationseance', 'Model');
App::uses('Deliberation', 'Model');

/**
 * Classe DeliberationseanceTest
 *
 * @version 4.3
 * @package app.Test.Case.Model
 */
class DeliberationseanceTest extends CakeTestCase
{

    /**
     * Fixtures associated with this test case
     *
     * @version 4.3
     * @var array
     */
    public $fixtures = [
      //  'app.DeliberationSeance',
        // 'app.Deliberation',
        // 'app.Seance',
        // 'app.Historique',
        // 'app.User',
        // 'app.DeliberationUser',
    ];

    public function setUp()
    {
        parent::setUp();
        //$this->Deliberationseance = ClassRegistry::init('Deliberationseance');
        // $this->Deliberation = ClassRegistry::init('Deliberation');
    }

    /**
     * Méthode exécutée avant chaque test.
     *
     * @version 4.3
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        // unset($this->Deliberationseance);
        // unset($this->Deliberation);
    }

    /**
     * Méthode exécutée après chaque test.
     *
     * @version 4.3
     * @return void
     */
    // public function testaddDeliberationseance()
    // {
    //     $this->Deliberationseance->addDeliberationseance(3, 1);
    //     $this->Deliberationseance->addDeliberationseance(6, 1);
    //     $result = $this->Deliberationseance->find(
    //         'all',
    //         [
    //         'conditions' => ['Deliberationseance.seance_id' => 1],
    //         'fields' => ['Deliberationseance.id', 'Deliberationseance.position'],
    //         'order' => ['Deliberationseance.position ASC']]
    //     );
    //
    //     $expected = [
    //         ['Deliberationseance' => ['id' => 1, 'position' => '1']],
    //         ['Deliberationseance' => ['id' => 2, 'position' => '2']],
    //         ['Deliberationseance' => ['id' => 3, 'position' => '3']],
    //         ['Deliberationseance' => ['id' => 4, 'position' => '4']],
    //         ['Deliberationseance' => ['id' => 5, 'position' => '5']],
    //         ['Deliberationseance' => ['id' => 6, 'position' => '6']]
    //     ];
    //
    //     $this->assertEquals($expected, $result, var_export($result, true));
    // }

    /**
     * Méthode exécutée après chaque test.
     *
     * @version 4.3
     * @return void
     */
    // public function testdeleteDeliberationseance()
    // {
    //     $this->Deliberationseance->addDeliberationseance(3, 1);
    //     $this->Deliberationseance->addDeliberationseance(6, 1);
    //     $this->Deliberationseance->deleteDeliberationseance(3, 1);
    //     $result = $this->Deliberationseance->find(
    //         'all',
    //         [
    //         'conditions' => ['Deliberationseance.seance_id' => 1],
    //         'fields' => ['Deliberationseance.id', 'Deliberationseance.position'],
    //         'order' => ['Deliberationseance.position ASC']]
    //     );
    //
    //     $expected = [
    //         ['Deliberationseance' => ['id' => 1, 'position' => '1']],
    //         ['Deliberationseance' => ['id' => 2, 'position' => '2']],
    //         ['Deliberationseance' => ['id' => 6, 'position' => '3']]
    //     ];
    //
    //     $this->assertEquals($expected, $result, var_export($result, true));
    // }

    /**
     * Méthode exécutée après chaque test.
     *
     * @version 4.3
     * @return void
     */
    // public function testaddFalseDeliberationseance()
    // {
    //     //On crée un problème sur une délibération mal positionné
    //     $deliberationseance['position'] = 10;
    //     $deliberationseance['deliberation_id'] = 7;
    //     $deliberationseance['seance_id'] = 1;
    //     $this->Deliberationseance->create($deliberationseance);
    //     $this->Deliberationseance->save();
    //
    //     $deliberation['id'] = 7;
    //     $deliberation['etat'] = 1;
    //     $deliberation['objet'] = 'Test';
    //     $deliberation['objet_delib'] = 'Test';
    //     $deliberation['parent_id'] = null;
    //     $this->Deliberation->create($deliberation);
    //     $this->Deliberation->save();
    //
    //     $this->Deliberationseance->addDeliberationseance(3, 1);
    //     $this->Deliberationseance->addDeliberationseance(6, 1);
    //     $result = $this->Deliberationseance->find(
    //         'all',
    //         [
    //         'conditions' => ['Deliberationseance.seance_id' => 1],
    //         'fields' => ['Deliberationseance.id', 'Deliberationseance.position'],
    //         'order' => ['Deliberationseance.position ASC']]
    //     );
    //
    //     $expected = [
    //         ['Deliberationseance' => ['id' => 1, 'position' => '1']],
    //         ['Deliberationseance' => ['id' => 2, 'position' => '2']],
    //         ['Deliberationseance' => ['id' => 3, 'position' => '3']],
    //         ['Deliberationseance' => ['id' => 4, 'position' => '4']],
    //         ['Deliberationseance' => ['id' => 5, 'position' => '5']],
    //         ['Deliberationseance' => ['id' => 6, 'position' => '6']],
    //         ['Deliberationseance' => ['id' => 7, 'position' => '7']],
    //     ];
    //
    //     $this->assertEquals($expected, $result, var_export($result, true));
    // }
    //
    /**
     * testIsNewMessage method
     *
     * @return void
     */
    public function test()
    {
        $this->assertTrue(true);
    }
}
