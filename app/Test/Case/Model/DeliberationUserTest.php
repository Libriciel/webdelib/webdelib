<?php
App::uses('DeliberationUser', 'Model');

/**
 * DeliberationUser Test Case
 */
class DeliberationUserTest extends CakeTestCase
{

/**
 * Fixtures
 *
 * @var array
 */
    public $fixtures = [
        'app.deliberation_user',
        'app.user',
    ];

/**
 * setUp method
 *
 * @return void
 */
    public function setUp()
    {
        parent::setUp();
        Configure::write('Config.database', 'test');
        $this->DeliberationUser = ClassRegistry::init('DeliberationUser');
    }

/**
 * tearDown method
 *
 * @return void
 */
    public function tearDown()
    {
        unset($this->DeliberationUser);

        parent::tearDown();
    }

/**
 * testSaveDeliberationUser method
 *
 * @return void
 */
    public function testSaveDeliberationUser()
    {
        $data = [
                'Deliberation' =>  ['id' => 1],
                'DeliberationUser' => [2,3]
            ];

        $this->DeliberationUser->saveDeliberationUser($data);

        $deliberationUserList = $this->DeliberationUser->find('list', [
            'fields' => ['user_id'],
            'conditions' => ['deliberation_id' => 1],
            'recursive' => -1
        ]);
        $expected = [18=>2,19=>3];
        $this->assertEquals($expected, $deliberationUserList, var_export($deliberationUserList, true));
    }
}
