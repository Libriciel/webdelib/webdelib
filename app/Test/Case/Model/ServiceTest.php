<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Service', 'Model');
App::uses('CakeTime', 'Utility');

/**
 * Classe ServiceTest.
 * @version 4.3
 * @package app.Test.Case.Model
 *
 */
class ServiceTest extends CakeTestCase
{

    /**
     * Fixtures associated with this test case
     *
     * @var array
     */
    public $fixtures = [
        /*'app.Service',
        'app.ServiceUser',
        'app.Profil',
        'app.Historique',
        'app.CircuitUser',
        //'app.ActeurService',
        'plugin.Cakeflow.Circuit',
        'plugin.Cakeflow.Traitement',
        'plugin.Cakeflow.Visa',
        'plugin.Cakeflow.Etape',
        'plugin.Cakeflow.Composition',
        'app.User'*/
    ];
    public $autoFixtures = false;

    public function setUp()
    {
        parent::setUp();
        Configure::write('Config.database', 'test');
        $this->Service = ClassRegistry::init('Service');
    }

    /**
     * Méthode exécutée avant chaque test.
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Service);
    }

    /**
     * testIsNewMessage method
     *
     * @return void
     */
    public function test()
    {
        $this->assertTrue(true);
    }
}
