<?php
App::uses('NatureTypologiepiece', 'Model');

/**
 * NatureTyplogiepiece Test Case
 */
class NatureTypologiepieceTest extends CakeTestCase
{
    /**
     * Fixtures associated with this test case
     *
     * @var $fixtures array
     */
    public $fixtures = [
        'app.nature_typologiepiece',
        'app.typologiepiece',
        'app.nature'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        Configure::write('Config.database', 'test');
        $this->NatureTypologiepiece = ClassRegistry::init('NatureTypologiepiece');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NatureTypologiepiece);

        parent::tearDown();
    }

    public function testFindNatureTyplogiepiece()
    {
            $natureTyplogiepiece = $this->NatureTypologiepiece->find('first', [
                'contain' => ['Nature.id','Typologiepiece.id'],
                'conditions' => ['nature_id' => 1],
                'recursive' => -1
            ]);
            $this->assertEmpty($natureTyplogiepiece);
    }
}
