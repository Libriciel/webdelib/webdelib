<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('User', 'Model');
App::uses('CakeTime', 'Utility');
App::uses('File', 'Utility');

/**
 * Classe UserTest.
 * @version 4.3
 * @package app.Test.Case.Model
 *
 */
class UserTest extends CakeTestCase
{

    /**
     * Fixtures associated with this test case
     *
     * @version 4.3
     * @var array
     */
    public $fixtures = [
      /*  'app.User',
        'app.Deliberation',
        'app.Commentaire',
        'app.DeliberationSeance',
        'app.Seance',
        'app.Typeseance',
        'app.Theme',
        'app.Acteur'*/
    ];

    public function setUp()
    {
        parent::setUp();
        Configure::write('Config.database', 'test');
        $this->User = ClassRegistry::init('User');
    }

    /**
     * Méthode exécutée avant chaque test.
     *
     * @version 4.3
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->User);
    }

    /**
     * Méthode exécutée après chaque test.
     *
     * @version 4.3
     * @return void
     */
    public function testDeleteUser()
    {
        $this->assertEquals('1', '1', 'test OK');
    }

    /**
     * Méthode exécutée après chaque test.
     *
     * @version 4.3
     * @return void
     */
    // public function testNotifierUserIsertion()
    // {
    //     $mail = $this->User->notifier(1, 1, 'insertion');
    //     $file_mail = new File(TESTS . DS . 'Data' . DS . 'mails' . DS . 'insertion.txt');
    //     $this->assertEquals($file_mail->read(), $mail['message'], 'Mail d\'insertion en erreur');
    //     $file_mail->close();
    //
    // }

    /**
     * Méthode exécutée après chaque test.
     *
     * @version 4.3
     * @return void
     */
    public function testNotifierUserTraitement()
    {
//        $this->User->notifier(1, 1, 'traitement');
//        $this->User->notifier(1, 1, 'refus');
//        $this->User->notifier(1, 1, 'modif_projet_cree');
//        $this->User->notifier(1, 1, 'modif_projet_valide');
//        $this->User->notifier(1, 1, 'retard_validation');

        $this->assertEquals('1', '1', 'test OK');
    }
}
