<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppTools', 'Lib');

/**
 * [CronTest description]
 * @version 5.1.2
 * @package app.Test.Case.Model
 * @since 4.3.0
 */
class CronTest extends CakeTestCase
{
    /**
     * [FORMAT_DATE description]
     * @var string
     */
    const FORMAT_DATE = 'Y-m-d H:i:00';

    /**
     * [public description]
     * @var [type]
     */
    public $fixtures = [
        'app.Cron'
    ];

    /**
     * [setUp description]
     */
    public function setUp()
    {
        parent::setUp();
        Configure::write('Config.database', 'test');
        $this->Cron = ClassRegistry::init('Cron');
    }

    /**
     * [tearDown description]
     * @return [type] [description]
     */
    public function tearDown()
    {
        unset($this->Cron);

        parent::tearDown();
    }

    /**
     * [testcalcNextExecutionTimeNew description]
     * @return [type] [description]
     */
    public function testcalcNextExecutionTimeNew()
    {
        //$now = date(self::FORMAT_DATE);
        // lecture du cron à exécuter
        $cron = $this->Cron->find('first', [
              'conditions' => ['id' => 5],
              'recursive' => -1,
            ]);

        // initialisations
        $now = date('Y-m-d H:i:s');
        $nowTimestamp = CakeTime::fromString($now);
        $nextExecutionResultTime = date('Y-m-d 00:00:00');
        do {
            $nextExecutionResult = $nextExecutionResultTime = $nextDateDuration = AppTools::addSubDurationToDate($nextExecutionResultTime, 'PT5M', self::FORMAT_DATE);
            // La prochaine échéance devient la prochaine exécution
            $nextExecuteTimestamp = CakeTime::fromString($nextDateDuration);
        } while ($nowTimestamp >= $nextExecuteTimestamp);

        $nextExecution = $this->Cron->calcNextExecutionTime($cron['Cron']['execution_duration'], $cron['Cron']['next_execution_time']);

        $this->assertEqual($nextExecution, $nextExecutionResult, var_export($nextExecutionResult, true));
    }

    /**
     * [testcalcNextExecutionTime description]
     * @return [type] [description]
     */
    public function testcalcNextExecutionTimeLast()
    {
        // lecture du cron à exécuter
        $cron = $this->Cron->find('first', [
              'conditions' => ['id' => 5],
              'recursive' => -1,
            ]);

        $cron['Cron']['next_execution_time'] = AppTools::addSubDurationToDate($cron['Cron']['next_execution_time'], 'PT20M', self::FORMAT_DATE, 'sub');
        $nextExecutionResult = AppTools::addSubDurationToDate(date(self::FORMAT_DATE), 'PT5M', self::FORMAT_DATE);
        $nextExecution = $this->Cron->calcNextExecutionTime($cron['Cron']['execution_duration'], $cron['Cron']['next_execution_time']);

        $this->assertEqual($nextExecution, $nextExecutionResult, var_export($nextExecutionResult, true));
    }

    /**
     * [testcalcNextExecutionTime description]
     * @return [type] [description]
     */
    public function testcalcNextExecutionTimeFutur()
    {
        // lecture du cron à exécuter
        $cron = $this->Cron->find('first', [
              'conditions' => ['id' => 5],
              'recursive' => -1,
            ]);

        $cron['Cron']['next_execution_time'] = AppTools::addSubDurationToDate($cron['Cron']['next_execution_time'], 'PT20M', self::FORMAT_DATE);
        $nextExecutionResult = AppTools::addSubDurationToDate(date(self::FORMAT_DATE), 'PT5M', self::FORMAT_DATE);
        $nextExecution = $this->Cron->calcNextExecutionTime($cron['Cron']['execution_duration'], $cron['Cron']['next_execution_time']);

        $this->assertLessThan(CakeTime::fromString($nextExecution), CakeTime::fromString($nextExecutionResult), var_export($nextExecutionResult, true));
    }
}
