<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('TdtMessage', 'Model');

/**
 * Classe TdtMessageTest.
 * @version 4.3
 * @package app.Test.Case.Model
 *
 */
class TdtMessageTest extends CakeTestCase
{

    /**
     * Fixtures associated with this test case
     *
     * @version 4.3
     * @var array
     */
    public $fixtures = [
        /*'app.tdt_message',
        'app.deliberation',
        'app.service',
        'app.user',
        'app.profil',
        'app.infosupdef',
        'app.infosup',
        'app.infosuplistedef',
        'app.infosupdefs_profil',
        'app.historique',
        'app.composition',
        'app.etape',
        'app.circuit',
        'app.traitement',
        'app.visa',
        'app.signature',
        'app.users_service',
        'app.circuits_user',
        'app.acteur',
        'app.typeacteur',
        'app.acteurs_service',
        'app.theme',
        'app.typeacte',
        'app.compteur',
        'app.sequence',
        'app.typeseance',
        'app.model',
        'app.typeseances_typeacte',
        'app.typeseances_typeacteur',
        'app.typeseances_acteur',
        'app.nature',
        'app.annex',
        'app.commentaire',
        'app.listepresence',
        'app.vote',
        'app.deliberationseance',
        'app.seance',
        'app.deliberations_seance',
        'app.deliberationtypeseance',
        'app.deliberations_typeseance'*/
    ];

    public function setUp()
    {
        parent::setUp();
        Configure::write('Config.database', 'test');
        $this->TdtMessage = ClassRegistry::init('TdtMessage');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TdtMessage);

        parent::tearDown();
    }

    /**
     * testIsNewMessage method
     *
     * @return void
     */
    public function testIsNewMessage()
    {
        $this->assertTrue(true);
    }
}
