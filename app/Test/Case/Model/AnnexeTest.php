<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Annexe', 'Model');

/**
 * Classe AnnexeTest
 *
 * @version 4.3
 * @package app.Test.Case.Model
 */
class AnnexeTest extends CakeTestCase
{

    /**
     * Fixtures associated with this test case
     *
     * @version 4.3
     * @var array
     */
    public $fixtures = [
        'app.Annexe'
    ];

    public function setUp()
    {
        parent::setUp();
        Configure::write('Config.database', 'test');
        Configure::write('Config.tenant', 'test');
        require_once DATA . 'webdelib_db1' . DS . 'config/formats.inc';

        $this->Annexe = ClassRegistry::init('Annexe');
        $this->Annexe->recursive = -1;

        // Ajout des fichiers
        $db = $this->Annexe->getDataSource();
        $this->Annexe->updateAll(
            [
              // DboSource::value()
              'data' => $db->value(file_get_contents(TESTS . 'Data' . DS . 'AnnexFixture.pdf'), 'binary')
            ]
        );
    }

    /**
     * Méthode exécutée avant chaque test.
     *
     * @version 4.3
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->Annexe);
    }


    /**
     * [testReorderAnnexe description]
     * @return [type] [description]
     */
    // public function testReorderAnnexeBeforeFirst()
    // {
    //     $this->Annexe->id = 4;
    //     $this->Annexe->saveField('position', 1, ['callbacks'=>false]);
    //     $this->Annexe->saveField('modified', $this->dateAddDurationToUpdate(), ['callbacks'=>false]);
    //
    //     $this->Annexe->reorderAnnexe(1, 4);
    //
    //     $annexe=$this->Annexe->findAllByForeignKey(1, ['titre', 'position'], ['position'=>'ASC']);
    //     $annexe = Set::flatten($annexe);
    //
    //     $annexeExpected = [
    //       '0.Annexe.titre' => 'Annexe 4',
    //       '0.Annexe.position' => 1,
    //       '1.Annexe.titre' => 'Annexe 1',
    //       '1.Annexe.position' => 2,
    //       '2.Annexe.titre' => 'Annexe 2',
    //       '2.Annexe.position' => 3,
    //       '3.Annexe.titre' => 'Annexe 3',
    //       '3.Annexe.position' => 4,
    //     ];
    //
    //     $this->assertEqual($annexe, $annexeExpected, var_export($annexe, true));
    // }

    /**
     * Méthode exécutée après chaque test.
     *
     * @version 4.3
     * @return void
     */
    public function testSaveFilenameOK()
    {
        $this->markTestSkipped('must be revisited.');
        $newAnnexe = [
          'model' => 'Projet',
          'foreign_key' => 1,
          'joindre_ctrl_legalite' => true,
          'titre' => 'Annexe 5',
          'position' => 5,
          'filename' => 'AnnexeFixture_1_5.pdf',
          'filetype' => 'application/pdf',
          'size' => 501044,
          'data' => file_get_contents(TESTS . 'Data' . DS .'AnnexFixture.pdf'),
          'data' => '',
          'data_pdf' => null,
          'joindre_fusion' => 0,
          'edition_data' => null,
          'edition_data_typemime' => null
        ];

        $this->Annexe->create();
        $annexe = $this->Annexe->save($newAnnexe);
        $newAnnexeId = $this->Annexe->id;

        $this->assertEqual($newAnnexeId, 5, var_export($this->Annexe->validationErrors, true));

        $annexe=$this->Annexe->findAllByForeignKey(1, ['titre', 'position'], ['position'=>'ASC']);
        $annexe = Set::flatten($annexe);

        $annexeExpected = [
          '0.Annexe.titre' => 'Annexe 1',
          '0.Annexe.position' => 1,
          '1.Annexe.titre' => 'Annexe 2',
          '1.Annexe.position' => 2,
          '2.Annexe.titre' => 'Annexe 3',
          '2.Annexe.position' => 3,
          '3.Annexe.titre' => 'Annexe 4',
          '3.Annexe.position' => 4,
          '4.Annexe.titre' => 'Annexe 5',
          '4.Annexe.position' => 5,
        ];

        $this->assertEqual($annexe, $annexeExpected, var_export($annexe, true));
    }
    //
    // private function dateAddDurationToUpdate()
    // {
    //     return AppTools::addSubDurationToDate(date('Y-m-d H:i:s'), 'PT1M', 'Y-m-d H:i:s');
    // }
}
