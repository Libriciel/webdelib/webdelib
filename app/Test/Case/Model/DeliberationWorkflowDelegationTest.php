<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Deliberation', 'Model');
App::uses('Traitement', 'Cakeflow.Model');
App::uses('Circuit', 'Cakeflow.Model');

//FIX
App::uses('', 'Cakeflow.Model');
//App::uses('ConnectorManagerAppModel', 'ConnectorManager.Model');


//App::uses('ComponentCollection', 'Controller');
//App::uses('AppController', 'Controller');
//App::uses('DeliberationsController', 'Controller');

/**
 * Classe DeliberationWorkflowDelegationTest
 *
 * @version 4.3
 * @package app.Test.Case.Model
 */
class DeliberationWorkflowDelegationTest extends CakeTestCase
{

    /**
     * Fixtures associated with this test case
     *
     * @version 4.4.0
     * @var $fixtures array
     */
    public $fixtures = [
        //Génération
        'app.User',
        'app.Deliberation',
        'app.Historique',
        'app.DeliberationSeance',
        'app.Seance',
        'app.Typeseance',
        'app.DeliberationTypeseance',
        'app.Typeacte',
        'app.Collectivite',
        'app.Theme',
        'app.Infosupdef',
        'app.Listepresence',
        'app.Vote',
        'app.Annexe',
        'app.Plugin/ModelOdtValidator/Modeltemplate',
        'app.Plugin/ModelOdtValidator/Modeltype',
        //Workflow
        'app.CircuitUser',
        'app.circuit',
        'app.etape',
        'app.composition',
        'app.visa',
        'app.traitement',
//        'app.Plugin/ConnectorManager/Connector',
//        'app.Plugin/ConnectorManager/ConnectorJob',


    ];
    public $Deliberation;
    public $Circuit;
    public $Traitement;

    //public $autoFixtures = false;

    public function setUp()
    {
        $this->markTestSkipped('must be revisited.');
        parent::setUp();

         $this->Deliberation = ClassRegistry::init('Deliberation');
         //$this->Collectivite = ClassRegistry::init('Collectivite');
         $this->Circuit = ClassRegistry::init('Cakeflow.Circuit');
         $this->Traitement = ClassRegistry::init('Cakeflow.Traitement');
         $this->Etape = ClassRegistry::init('Cakeflow.Etape');
         $this->Visa = ClassRegistry::init('Cakeflow.Visa');
         $this->Modeltemplate = ClassRegistry::init('ModelOdtValidator.Modeltemplate');
         //$this->ConnectorJob = ClassRegistry::init('ConnectorManager.ConnectorJob');

         $modeltemplates = $this->Modeltemplate->find('all');
         foreach ($modeltemplates as  $modeltemplate) {
             $this->Modeltemplate->id = $modeltemplate['Modeltemplate']['id'];
             $this->Modeltemplate->save(['Modeltemplate' => [
                 'content' => file_get_contents(TESTS . DS . 'Data' . DS . 'OdtVide.odt')
                ]
             ], false);
             $this->Modeltemplate->clear();
         }
    }

    /**
     * Méthode exécutée avant chaque test.
     *
     * @version 4.4.0
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
         unset($this->Deliberation);
         unset($this->Circuit);
         //unset($this->collection);
         unset($this->Traitement);
    }

    /**
     * Méthode exécutée avant chaque test.
     *
     * @version 4.4.0
     * @return void
     */
     public function testaddIntoCircuit()
     {
         $projet_id = 3;
         $user_connecte = 1;
         $circuit_id = 2;

         $insertion = $this->Circuit->insertDansCircuit($circuit_id, $projet_id, $user_connecte);

         $options = [
             'insertion' => [
                 '0' => [
                     'Etape' => [
                         'etape_nom' => 'Rédacteur',
                         'etape_type' => 1
                     ],
                     'Visa' => [
                         '0' => [
                             'trigger_id' => $user_connecte,
                             'type_validation' => 'V'
                         ]
                     ],
                 ]
             ],
             'optimisation' => true
         ];

         $traitementTermine = $this->Traitement->execute('IN', $user_connecte, $projet_id, $options);

//         $ConnectorJob = $this->ConnectorJob->find('first', [
//            'fields' => ['args'],
//            'conditions' => ['id' => 1]
//         ]);
//         $json = json_decode($ConnectorJob['ConnectorJob']['args'], true);

         $this->assertFalse($traitementTermine);
     }

    /**
    //  * Méthode exécutée avant chaque test.
    //  *
    //  * @version 4.3
    //  * @return void
    //  */
//     public function testDelegToConnector()
//     {
//         $this->Deliberation->id = 1;
//         $result = $this->Deliberation->_delegToParapheurDocument();
//
//         $expected = [
//         'docPrincipale' => $result['docPrincipale'], //FIX remplacer par le fichier sortie dans Data
//         'annexes' => []
//         ];
//
//         $this->assertEquals($result, $expected, var_export($result, true));
//     }

//    function testDelegationPastell()
//    {
//        $visa = $this->Visa->find('first', [
//            'conditions' => ['Visa.id' => 2],
//        ]);
//
//        $etape = $this->Etape->find('first', [
//            'contain' => 'Composition',
//            'conditions' => ['Etape.id' => 2],
//        ]);
//
//        $data = [];
//        $data['Visa']=$visa;
//        $data['Etape']=$etape;
//        $composition = $data['Etape']['Composition'][0];
//
//
//        $result = $this->Deliberation->delegationCakeflow(1, $data);
//
//        $this->assertTrue($result);
//
//        $projet = $this->Deliberation->find('first', [
//            'conditions' => ['id' => 1],
//            'recursive'=> -1,
//        ]);
//
//        $this->assertEquals([
//            'parapheur_cible' => 'PASTELL',
//            'parapheur_etat' => 1,
//        ],[
//            'parapheur_cible' => $projet['Deliberation']['parapheur_cible'],
//            'parapheur_etat' => $projet['Deliberation']['parapheur_etat'],
//        ]);
//
//    }
}
