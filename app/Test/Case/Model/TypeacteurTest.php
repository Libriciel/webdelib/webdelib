<?php
App::uses('Typeacteur', 'Model');

/**
 * Typeacteur Test Case
 */
class TypeacteurTest extends CakeTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.typeacteur',
        'app.acteur',
        'app.service',
        'app.acteur_service',
    ];

/**
 * setUp method
 *
 * @return void
 */
    public function setUp()
    {
        parent::setUp();
        Configure::write('Config.database', 'test');
        $this->Typeacteur = ClassRegistry::init('Typeacteur');
    }

/**
 * tearDown method
 *
 * @return void
 */
    public function tearDown()
    {
        unset($this->Typeacteur);

        parent::tearDown();
    }

/**
 * testLibelleElu method
 *
 * @return void
 */
    public function testLibelleElu()
    {
        $actual = $this->Typeacteur->libelleElu(1, true);
        $this->assertEquals('Elu', $actual);
    }

    public function testLibelleNonElu()
    {
        $actual = $this->Typeacteur->libelleElu(0, true);
        $this->assertEquals('Non élu', $actual);
    }

    public function testLibelleEluMinuscule()
    {
        $actual = $this->Typeacteur->libelleElu(1);
        $this->assertEquals('élu', $actual);
    }

    public function testLibelleNonEluMinuscule()
    {
        $actual = $this->Typeacteur->libelleElu(0);
        $this->assertEquals('non élu', $actual);
    }

/**
 * testIsDeletable method
 *
 * @return void
 */
    public function testIsDeletableFalse()
    {
        $actual = $this->Typeacteur->isDeletable(1);
        $this->assertFalse($actual);
    }

    public function testIsDeletableTrue()
    {
        $actual = $this->Typeacteur->isDeletable(6);
        $this->assertTrue($actual);
    }
}
