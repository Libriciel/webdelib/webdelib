<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AnnexeToolsTask', 'Console/Command/Task');
App::uses('ConversionComponent', 'Controller/Component');
App::uses('Cron', 'Model');

/**
 * Classe AnnexeToolsTaskTest.
 *
 * @version 4.3
 * @package app.Test.Console.Command.Task
 */
class AnnexeToolsTaskTest extends CakeTestCase
{

    /**
     * Fixtures associated with this test case
     *
     * @version 4.3
     * @var array
     */
    public $fixtures = [
        'app.Deliberation',
        'app.Annexe',
    ];

    public function setUp()
    {
        $this->markTestSkipped('must be revisited.');
        parent::setUp();

        $this->Annexe = ClassRegistry::init('Annexe');

        $this->AnnexeTools = new AnnexeToolsTask();

        $this->Annexe->id = 1;
        $data = [
            'size' => strlen(file_get_contents(APP . 'Test/Data/AnnexFixture.pdf')),
            'data' => file_get_contents(APP . 'Test/Data/AnnexFixture.pdf'),
        ];
        $this->Annexe->save($data, false);
    }

    /**
     * tearDown method
     *
     * @version 4.3
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->Annexe);
    }

    /**
     * testIsNewMessage method
     *
     * @version 4.3
     * @return void
     */
    public function testConvertionAnnexeEmpty(): void
    {
        $excepted = file_get_contents(APP . 'Test/Data/OdtVide.odt');
        $this->AnnexeTools->Conversion = $this->createMock('ConversionComponent');
        $this->AnnexeTools->Conversion
            ->method('toOdt')
            ->willReturn($excepted);

        $this->AnnexeTools->conversion(1);

        $annexe = $this->Annexe->find(
            'first',
            [
                'fields' => ['id', 'edition_data'],
                'conditions' => [
                    'edition_data IS NOT NULL',
                    'id' => 1
                ],
                'recursive' => -1
            ]
        );
        $result = $annexe['Annexe']['edition_data'];

        $this->assertEquals($excepted, $result);
    }
}
