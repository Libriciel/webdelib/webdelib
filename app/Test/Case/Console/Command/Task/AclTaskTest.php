<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AclTask', 'Console/Command/Task');
App::uses('Shell', 'Console/Shell');

/**
 * Classe AclTaskTest.
 *
 * @package app.Test.Console.Command.Task
 */
class AclTaskTest extends CakeTestCase
{

    /**
     * Fixtures associated with this test case
     *
     * @version 4.3
     * @var array
     */
    public $fixtures = [
        'app.migrationAcl/aro',
        'app.migrationAcl/aco',
        'app.migrationAcl/aro_aco',
        'app.user'
    ];

    public function setUp()
    {
        $this->markTestSkipped('must be revisited.');
        parent::setUp();
        $this->AclTask = new AclTask();
        $this->AclTask->useDbConfig = 'test';
        $this->AclTask->Aco->useDbConfig = 'test';
        $this->AclTask->Aro->useDbConfig = 'test';
        $this->AclTask->Permission->useDbConfig = 'test';
        $this->Aco = ClassRegistry::init('Aco');
        $this->Permission = ClassRegistry::init('Permission');
    }

    /**
     * tearDown method
     *
     * @version 4.3
     * @return void
     */
    public function tearDown()
    {
        unset($this->AclTask);
        unset($this->Aco);
        unset($this->Permission);
        parent::tearDown();
    }

    /**
     * @return void
     */
    public function testMigrateNameControlerAcl(): void
    {
        $this->AclTask->migrateNameControlerAcl('Projets', 'Deliberations');

        $acoNew = $this->Aco->find('first', [
            'conditions' => [
                'id' => 8,
            ],
            'recursive' => -1]);

        $this->assertEquals([ 'Aco' => [
            'id' => 8,
            'parent_id' => 1,
            'model' => null,
            'foreign_key' => null,
            'alias' => 'Projets',
            'lft' => 2,
            'rght' => 63,
        ],
        ], $acoNew, var_export($acoNew, true));
    }

    /**
     * @return void
     */
    public function testCreateUsersPermissionNew(): void
    {

        $this->AclTask->updateAcl(false);
        $this->AclTask->createUsersPermission('attribuerCircuit', 1);

        $aroId = 4;
        $acoId = $this->getAroIdByName('attribuerCircuit');
        $permissionNew = $this->Permission->find('first', [
            'conditions' => [
                'aro_id' => $aroId,
                'aco_id' => $acoId,
            ],
            'recursive' => -1]);

        $this->assertEquals([ 'Permission' => [
            'id' => $this->getAcoAroId($acoId, $aroId),
            'aco_id' => $acoId,
            'aro_id' => $aroId,
            '_create' => '1',
            '_read' => '1',
            '_update' => '1',
            '_delete' => '1',
            '_manager' => '1',
        ],
        ], $permissionNew, var_export($permissionNew, true));
    }

    /**
     * @return void
     */
    public function testCreateUsersPermissionMigrateCrud(): void
    {
        $this->AclTask->updateAcl(false);
        $this->AclTask->createUsersPermission('Votes', 'Seances');

        $aroId = 4;
        $acoId = $this->getAroIdByName('Votes');
        $permissionNew = $this->Permission->find('first', [
            'conditions' => [
                'aro_id' => $aroId,
                'aco_id' => $acoId,
            ],
            'recursive' => -1]);

        $this->assertEquals([ 'Permission' => [
            'id' => $this->getAcoAroId($acoId, $aroId),
            'aco_id' => $acoId,
            'aro_id' => $aroId,
            '_create' => '1',
            '_read' => '1',
            '_update' => '1',
            '_delete' => '1',
            '_manager' => '1',
        ],
        ], $permissionNew, var_export($permissionNew, true));
    }


    private function getAroIdByName($name)
    {
        $acoNew = $this->Aco->find('first', [
            'conditions' => [
                'model' => null,
                'alias' => $name
            ],
            'recursive' => -1]);

        return $acoNew['Aco']['id'];
    }

    private function getAcoAroId($AcoId, $AroId)
    {
        $permissionNew = $this->Permission->find('first', [
            'conditions' => [
                'aro_id' => $AroId,
                'aco_id' => $AcoId,
            ],
            'recursive' => -1]);

        return $permissionNew['Permission']['id'];
    }
}
