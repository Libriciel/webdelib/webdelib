<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppShell', 'Console/Command');
App::uses('Infosup', 'Model');
App::uses('Infosupdef', 'Model');
App::uses('Infosuplistedef', 'Model');
App::uses('ImportShell', 'Console/Command');
App::uses('AppTools', 'Lib');
App::uses('File', 'Utility');

/**
 * Classe ImportShellTest.
 *
 * @version 4.3
 * @package app.Test.Console.Command
 */
class ImportShellTest extends CakeTestCase
{
    public $Infosup;
    public $Infosupdef;
    public $Infosuplistedef;
    public $ImportShell;

    /**
     * Fixtures associated with this test case
     *
     * @version 4.3
     * @var array
     */
    public $fixtures = [
        'app.infosup',
        'app.infosupdef',
        'app.infosuplistedef'
    ];

    public function setUp()
    {
        parent::setUp();
        Configure::write('Config.database', 'test');
        $this->ImportShell = new ImportShell();
        $this->Infosup = ClassRegistry::init('Infosup');
        $this->Infosupdef = ClassRegistry::init('Infosupdef');
        $this->Infosuplistedef = ClassRegistry::init('Infosuplistedef');
    }

    /**
     * Méthode exécutée avant chaque test.
     *
     * @version 4.3
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->ImportShell);
        unset($this->Infosup);
        unset($this->Infosupdef);
        unset($this->Infosuplistedef);
    }

    /**
     * Test Import Informations supllémentaires liste()
     *
     * @version 4.3
     * @return void
     */
    public function testinfoSupListe1collone()
    {
        $this->ImportShell->params = [
            'tenant' => 'test',
            'code' => 'axes',
            'file' => TESTS . 'Data' . DS . 'Shell' . '/import_liste1.csv',
            'desactive' => 'true'
        ];
        $this->ImportShell->infoSupListe();

        $result = $this->Infosuplistedef->find(
            'all',
            [
            'fields' => ['nom','ordre'],
            'conditions' => ['actif' => true, 'infosupdef_id' => 1],
            'recursive' => -1,
            'order' => ['ordre ASC']]
        );

        $expected = [
            ['Infosuplistedef' => ['nom' => 'MONTPELLIER', 'ordre' => 1]],
            ['Infosuplistedef' => ['nom' => 'GRABELS', 'ordre' => 2]]
        ];

        $this->assertEquals($expected, $result, var_export($result, true));
    }

    /**
     * Test Import Informations supllémentaires liste()
     *
     * @version 4.3
     * @return void
     */
    public function testinfoSupListe2collone()
    {
        $this->ImportShell->params = [
            'tenant' => 'test',
            'code' => 'axes',
            'file' => TESTS . 'Data' . DS . 'Shell' . '/import_liste2.csv',
            'desactive' => 'true'
        ];

        $this->ImportShell->infoSupListe();

        $result = $this->Infosuplistedef->find(
            'all',
            [
            'fields' => ['nom','actif','ordre'],
            'conditions' => ['actif' => true, 'infosupdef_id' => 1],
            'recursive' => -1,
            'order' => ['ordre ASC']]
        );

        $expected = [
            ['Infosuplistedef' => ['nom' => 'MONTPELLIER', 'actif' => true, 'ordre' => 1]],
            ['Infosuplistedef' => ['nom' => 'GRABELS', 'actif' => true, 'ordre' => 2]],
        ];

        $this->assertEquals($expected, $result, var_export($result, true));
    }
}
