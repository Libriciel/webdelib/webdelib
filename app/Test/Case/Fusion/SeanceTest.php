<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Builder', 'FusionConv.Utility');
App::uses('File', 'Utility');
App::uses('Folder', 'Utility');
App::uses('AppTools', 'Lib');
App::uses('phpOdtApi', 'ModelOdtValidator.Lib');
App::uses('AppModel', 'Model');

App::uses('Seance', 'Model');

/**
 * TestUnit SeanceTest
 * Test fusion of documents
 *
 * @version 4.4
 * @package app.Test.Case.Model
 */
class FusionSeanceTest extends CakeTestCase
{
    /**
     * Fixtures associated with this test case
     *
     * @version 4.3
     * @var array
     */
    public $fixtures = [
      /*  //Génération
        'app.Deliberation',
        'app.DeliberationSeance',
        'app.Seance',
        'app.Typeseance',
        'app.DeliberationTypeseance',
        'app.Typeacte',
        'app.Collectivite',
        'app.Theme',
        'app.Infosupdef',
        'app.Listepresence',
        'app.Vote',
        'app.Annexe',

        'app.Historique',
        'app.DeliberationUser',
        'app.User',

        'app.Plugin/ModelOdtValidator/Modeltemplate',
        'app.Plugin/ModelOdtValidator/Modeltype',*/
    ];

    private $Gedooo;
    private $phpOdtApi;
    private $file;
    private $Modeltemplate;

    public function setUp()
    {
        parent::setUp();

        $this->Seance = ClassRegistry::init('Seance');
        //gedFusion
        $this->phpOdtApi = new phpOdtApi();

        $this->Modeltemplate = ClassRegistry::init('ModelOdtValidator.Modeltemplate');

        $this->Modeltemplate->useDbConfig = 'test_fixture';
        $modeltemplates = $this->Modeltemplate->find('all');
        $this->Modeltemplate->useDbConfig = 'test';
        foreach ($modeltemplates as  $modeltemplate) {
            $this->Modeltemplate->id = $modeltemplate['Modeltemplate']['id'];
            $this->Modeltemplate->save(['Modeltemplate' => [ 'content' => $modeltemplate['Modeltemplate']['content']]], false);
            $this->Modeltemplate->clear();
        }
    }

    /**
     * Méthode exécutée avant chaque test.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->Gedooo);
        unset($this->phpOdtApi);
        unset($this->Seance);
        unset($this->Modeltemplate);
        //$this->file->close();
        unset($this->file);
    }

    /**
     * Test updateAll()
     * @return void
     */
    public function test_multi_seance()
    {
        $tmpDir = new Folder(AppTools::newTmpDir(TMP . 'files' . DS . 'test'));
        // Fusion
        $this->Seance->Behaviors->load('OdtFusion', ['modelTemplateId' => 3, 'modelOptions' => ['modelTypeName' => 'multiseances']]);
        $filename = $this->Seance->fusionName();
        $this->Seance->odtFusion(['modelOptions' => ['seanceIds' => [2,4]]]);
        $content = $this->Seance->getOdtFusionResult('odt');
        $this->deleteOdtFusionResult();
        $this->phpOdtApi->loadFromOdtBin($content);
        $result = $this->phpOdtApi->getContent('jeudi 01 septembre 2016');
        $this->assertNotNull($result);
    }

    /**
     * Test updateAll()
     * @return void
     */
//    public function test_multi_seance()
//    {
//        $tmpDir = new Folder(AppTools::newTmpDir(TMP . 'files' . DS . 'test'));
//        // Fusion
//        $this->Seance->Behaviors->load('OdtFusion', array('modelTemplateId' => 3, 'modelOptions' => array('modelTypeName' => 'multiseances')));
//        $filename = $this->Seance->fusionName();
//        $this->Seance->odtFusion(array('modelOptions' => array('seanceIds' => array(2,4))));
//        $content = $this->Seance->getOdtFusionResult('odt');
//
//        //$this->deleteOdtFusionResult();
//
//        //Initialisation de la librairie->odtFusionResult->binary
////        $this->phpOdtApi->loadFromOdtBin($this->Seance->odtFusionResult->binary);
////        debug($this->phpOdtApi->getUserFieldValue('date_seances'));
//
//        $this->phpOdtApi->loadFromOdtBin($content);
//        //$this->phpOdtApi->getUserFields('date_seances');
//        $result = $this->phpOdtApi->getContent('jeudi 01 septembre 2016');
//        $this->assertNotNull($result);
//        //debug($this->phpOdtApi->content);
//        //$this->assertEquals( 'jeudi 1 septembre 2016', $result, var_export( $result, true));
//    }
}
