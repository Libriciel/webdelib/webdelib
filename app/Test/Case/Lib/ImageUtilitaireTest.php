<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('ImageUtilitaire', 'Lib');

/**
 * Classe TdtTest.
 *
 * @version 4.5
 * @package app.Test.Image
 */
class ImageUtilitaireTest extends PHPUnit_Framework_TestCase
{
    public function testCalculXY()
    {
        $imageUtil = new ImageUtilitaire();
        $this->assertEquals($imageUtil->boundingRectangle(50, 100, 10000, 10000), [50,100]);
        $this->assertEquals($imageUtil->boundingRectangle(50, 100, 10, 60), [10,20]);
        $this->assertEquals($imageUtil->boundingRectangle(50, 100, 10, 10), [5,10]);
    }

    public function testCreationImageJpg()
    {
        $imageUtil = new ImageUtilitaire();
        $nameFileOrig = APP . 'Test/Data/ImageTest.jpg';
        $nameFile = TMP . 'testjpg.jpg';
        $wDest=80;
        $hdest=80;
        $imageUtil->resizeImageToFile($nameFileOrig, $nameFile, $wDest, $hdest);

        $imageSortieContent=ImageCreateFromJpeg(TMP . 'testjpg.jpg');
        $widthSortie=imagesx($imageSortieContent);
        $heigthSortie=imagesy($imageSortieContent);

        $this->assertEquals([$widthSortie,$heigthSortie], [80,69]);
    }

    public function testCreationImagePng()
    {
        $imageUtil = new ImageUtilitaire();
        $nameFileOrig = APP . 'Test/Data/ImageTest.png';
        $nameFile = TMP . 'testpng.jpg';
        $wDest=80;
        $hdest=80;
        $imageUtil->resizeImageToFile($nameFileOrig, $nameFile, $wDest, $hdest);

        $imageSortieContent=ImageCreateFromJpeg(TMP .'testpng.jpg');
        $widthSortie=imagesx($imageSortieContent);
        $heigthSortie=imagesy($imageSortieContent);

        $this->assertEquals([$widthSortie,$heigthSortie], [80,53]);
    }


    /**
     * Méthode exécutée avant chaque test.
     *
     * @return void
     */
/*    public function tearDown() {
        parent::tearDown();
        unset($this->Tdt);
        unset($this->Nomenclature);
    }*/

    /**
     * Test updateAll()
     * @return void
     */
    /*public function testGetClassification(){
        $retour = $this->Image->calculXY(10,60);
        $this->assertEquals($retour, [5,10]);

        $retour = $this->Image->calculXY(6,10);
        $this->assertEquals($retour, [5,10]);
    }*/
}
