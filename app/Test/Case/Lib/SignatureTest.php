<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Signature', 'Lib');

/**
 * Classe SignatureTest.
 *
 * @version 4.3
 * @package app.Test.Lib
 */
class SignatureTest extends CakeTestCase
{
    private $Signature;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Deliberation',
        'app.Collectivite',
        'app.Typeacte'
    ];

    public function setUp()
    {
        parent::setUp();
        $this->markTestSkipped('must be revisited.');

        $this->Deliberation = ClassRegistry::init('Deliberation');
        $this->Collectivite = ClassRegistry::init('Collectivite');
        Configure::write('IPARAPHEUR_TYPE', 'IPARAPHEUR');
        $this->Signature = new Signature();
    }

    /**
     * Méthode exécutée avant chaque test.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->Deliberation);
        unset($this->Collectivite);
        unset($this->Signature);
    }

    /**
     * Test listCircuits()
     * @return void
     */
    public function testListCircuits()
    {
        $expected = ["SIGNATURE-PDF","SIGNATURE-PDF-AUTOMATIQUE","SIGNATURE-PES"];
        $aIparapheur = $this->Signature->listCircuits();

        $this->assertEquals($expected, $aIparapheur, var_export(array_combine($aIparapheur, $expected), true)); //ca bug la
    }

    /**
     * Test updateAll()
     * @return void
     */
    public function testUpdateAll()
    {
        $retour = $this->Signature->updateAll();

        if (Configure::read('PARAPHEUR') == 'IPARAPHEUR') {
            assert($retour == 'TRAITEMENT_TERMINE_OK');
        }
    }

    /**
     * Test Send()
     * @return void
     */
    public function testSend()
    {
        $this->Deliberation->id = 1;
        $this->Deliberation->Behaviors->load('Containable');
        $target = $this->Deliberation->find('first', [
            'contain' => ['Typeacte.nature_id'],
            'conditions' => ['Deliberation.id' => $this->Deliberation->id]
        ]);
        $libelleSousType = 'Délibération';

        $aDelegToParapheurDocuments = [
            'docPrincipale' => file_get_contents(APP . 'Test/Data/AnnexFixture.pdf'),
            'annexes' => [file_get_contents(APP . 'Test/Data/AnnexFixture.pdf')]];

        $ret = $this->Signature->send($target, $libelleSousType, $aDelegToParapheurDocuments['docPrincipale'], $aDelegToParapheurDocuments['annexes']);
    }
}
