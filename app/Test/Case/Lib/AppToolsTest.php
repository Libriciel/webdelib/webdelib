<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppTools', 'Lib');

/**
 * Classe AppToolsTest.
 *
 * @package app.Test.Lib
 */
class AppToolsTest extends CakeTestCase
{
    private Faker\Generator $faker;

    public function setUp()
    {
        parent::setUp();

        $this->AppTools = new AppTools();
        $this->faker = Faker\Factory::create('fr_FR');
    }

    public function testReformatNameForPastellMaxNbChars()
    {
        $expected = $texte = $this->faker->realText(500, 1);
        $actual = $this->AppTools->reformatNameForPastell($texte);
        $this->assertEquals($expected, $actual, var_export($actual, true));
    }

    public function testReformatNameForPastell()
    {
        $expected = $texte = $this->faker->realText(600, 1);
        $actual = $this->AppTools->reformatNameForPastell($texte);
        $this->assertNotEquals($expected, $actual, var_export($actual, true));

        $this->assertLessThanOrEqual(
            500,
            mb_strlen($actual),
            var_export(mb_strlen($actual), true)
        );
    }

    public function testReformatNameForIparapheurMaxNbChars()
    {
        $expected = $texte = $this->faker->realText(250, 1);
        $actual = $this->AppTools->reformatNameForIparapheur($texte);
        $this->assertEquals($expected, $actual, var_export($actual, true));
    }

    public function testReformatNameForIparapheur()
    {
        $expected = $texte = $this->faker->realText(300, 1);
        $actual = $this->AppTools->reformatNameForIparapheur($texte);
        $this->assertNotEquals($expected, $actual, var_export($actual, true));

        $this->assertLessThanOrEqual(
            250,
            mb_strlen($actual),
            var_export(mb_strlen($actual), true)
        );
    }

    public function testReformatNameByLengthAnyNewlines()
    {
        $actual = $this->AppTools->reformatNameByLength(
            "Première ligne\nDeuxième ligne\r\nTroisième ligne\rQuatrième ligne",
            100
        );
        $this->assertEquals(
            'Première ligne Deuxième ligne Troisième ligne Quatrième ligne',
            $actual,
            var_export($actual, true)
        );
    }
}
