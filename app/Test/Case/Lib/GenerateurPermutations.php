<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

function permutations($objets, $longueurPermutation = null)
{
    $nombreObjets = count($objets);

    if ($longueurPermutation == null) {
        $longueurPermutation=$nombreObjets;
    }

    if ($longueurPermutation>$nombreObjets) {
        $objets=array_pad($objets, $longueurPermutation, "null");
        $nombreObjets = count($objets);
    }
    $indices = range(0, $nombreObjets-1);
    $cycles = range($nombreObjets, $nombreObjets - $longueurPermutation +1, -1);

    yield array_slice($objets, 0, $longueurPermutation);

    if ($nombreObjets<=0) {
        return;
    }

    while (true) {
        $exit_early = false;
        for ($i = $longueurPermutation;$i--;$i >=0) {
            $cycles[$i]-=1;
            if ($cycles[$i]==0) {
                // Pousse ce qui est à l'indice i vers la fin, bouge le reste en arrière
                if ($i < count($indices)) {
                    $removed = array_splice($indices, $i, 1);
                    array_push($indices, $removed[0]);
                }
                $cycles[$i] = $nombreObjets - $i;
            } else {
                $j = $cycles[$i];
                //permute indices $i & -$j.
                $i_val = $indices[$i];
                $neg_j_val = $indices[count($indices) - $j];
                $indices[$i] = $neg_j_val;
                $indices[count($indices)-$j] = $i_val;
                $result = [];
                $counter = 0;
                foreach ($indices as $indx) {
                    array_push($result, $objets[$indx]);
                    $counter++;
                    if ($counter == $longueurPermutation) {
                        break;
                    }
                }
                yield $result;
                $exit_early = true;
                break;
            }
        }
        if (!$exit_early) {
            break;
        }
    }
}

$result = iterator_to_array(permutations([1, 2, 3, 4], 3));
foreach ($result as $row) {
    implode(", ", $row) . "\n";
}

$result = iterator_to_array(permutations([1, 2, 3], 4));
foreach ($result as $row) {
    implode(", ", $row) . "\n";
}

$result = iterator_to_array(permutations([1, 2, 3], 5));
foreach ($result as $row) {
    implode(", ", $row) . "\n";
}
