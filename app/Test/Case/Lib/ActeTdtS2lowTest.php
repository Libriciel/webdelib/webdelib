<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Tdt', 'Lib/Tdt');
App::uses('ActeTdtS2low', 'Lib/Tdt');
App::uses('ConnectorS2low', 'Lib');

/**
 * Classe TdtTest.
 *
 * @version 4.5
 * @package app.Test.Image
 */
class ActeTdtS2lowTest extends PHPUnit_Framework_TestCase
{
    protected $repertoireTests = APP . 'Test/Data/S2low/';

    /**
     * [testGestionReponseVide description]
     * @return [type] [description]
     */
    public function testGestionReponseVide()
    {
       // $this->expectExceptionMessage("Retour Nul");
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Retour Vide');

        $json = '';
        $connectorS2low = $this->createMock('ConnectorS2low');
        $connectorS2low->method('getFluxRetour')->willReturn($json);

        $acte = new ActeTdtS2low("0", $connectorS2low);
    }

    public function testGestionRetourKO()
    {
        $this->expectExceptionMessage("Statut acte non OK");
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Identifiant de transaction inconnu');

        $json = file_get_contents($this->repertoireTests . 'actes_transac_get_status_KO');
        $connectorS2low = $this->createMock('ConnectorS2low');
        $connectorS2low->method('getFluxRetour')->willReturn($json);

        $acte = new ActeTdtS2low("0", $connectorS2low);
    }

    public function testGestionRetourOK1()
    {
        $json = file_get_contents($this->repertoireTests . "actes_transac_get_status_OK1");
        $connectorS2low = $this->createMock('ConnectorS2low');
        $connectorS2low->method('getFluxRetour')->willReturn($json);

        $acte = new ActeTdtS2low("0", $connectorS2low);
        $this->assertEquals($acte->getStatut(), "Acquittement reçu");

        $AR=file_get_contents($this->repertoireTests."AR_OK1");
        $this->assertEquals($acte->getARActe(), $AR);
    }

    public function testGestionRetourOK2()
    {
        $json=file_get_contents($this->repertoireTests."actes_transac_get_status_OK2");
        $connectorS2low = $this->createMock('ConnectorS2low');
        $connectorS2low
            ->method('getFluxRetour')
            ->willReturn($json);
        $acte = new ActeTdtS2low("0", $connectorS2low);
        $this->assertEquals($acte->getStatut(), "Acquittement reçu");
    }

    public function testGestionListe1()
    {
        $json=file_get_contents($this->repertoireTests."actes_transac_get_status_OK1");
        $listeFichiers=file_get_contents($this->repertoireTests."actes_transac_get_files_list_1");
        $connectorS2low = $this->createMock('ConnectorS2low');

        $connectorS2low->method('getFluxRetour')->willReturn($json);
        $connectorS2low->method('getActeDocuments')->willReturn($listeFichiers);
        $connectorS2low
            ->method('getActeDocumentTampon')
            ->with('5', true)->willReturn('Acte Tamponne');

        $acte = new ActeTdtS2low("0", $connectorS2low);

        $this->assertEquals($acte->getTampon(), 'Acte Tamponne');
    }
    public function testGestionAnnexes()
    {
        $json=file_get_contents($this->repertoireTests."actes_transac_get_status_OK1");
        $listeFichiers=file_get_contents($this->repertoireTests."actes_transac_get_files_list_1");
        $connectorS2low = $this->createMock('ConnectorS2low');
        $connectorS2low->method('getFluxRetour')->willReturn($json);
        $connectorS2low->method('getActeDocuments')->willReturn($listeFichiers);

        $connectorS2low
            ->method('getActeDocumentTampon')
            ->with('5', true)->willReturn('Acte Tamponne');

        $acte = new ActeTdtS2low("0", $connectorS2low);
        $details = $acte->getAnnexesDetails();

        $annexesAttendues = [
              'id' => '6',
               'name' => '22_AG-034-491011698-20171107-PEV_ESSAI_1-AI-1-1_2.pdf',
               'posted_filename' => 'compilation.pdf',
               'mimetype' => 'application/pdf',
               'size' => '3414581'];

        $this->assertEquals($details[0], $annexesAttendues);
    }

    public function testAttributCorrespondance()
    {
        $this->markTestSkipped('must be revisited.');

        $connectorS2low = $this->createMock('ConnectorS2low');

        $valeurs=['id'=>"1",
    'name'=>"name",
    'posted_filename'=>"posted_filename",
    'mimetype'=>"mimetype",
    'size'=>"size",
    'sign'=>"sign"];

        $fichier = new FichierActeTdt($connectorS2low, $valeurs);
        $this->assertEquals($fichier->correspondAttributs(['name'=>"name"]), true);
        $this->assertEquals($fichier->correspondAttributs(['name'=>"not_name"]), false);
        $this->assertEquals($fichier->correspondAttributs($valeurs), true);

        $valeursfausses = ['id'=>"1",
    'name'=>"name",
    'posted_filename'=>"posted_filename",
    'mimetype'=>"not_mimetype",
    'size'=>"size",
    'sign'=>"sign"];

        $this->assertEquals($fichier->correspondAttributs($valeursfausses), false);
    }

    public function testExceptionCorrespondance()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Attribut posted_falsename inconnu");
        $connectorS2low = $this->createMock('ConnectorS2low');

        $valeurs=['id'=>"1",
    'name'=>"name",
    'posted_filename'=>"posted_filename",
    'mimetype'=>"mimetype",
    'size'=>"size",
    'sign'=>"sign"];

        $fichier = new FichierActeTdt($connectorS2low, $valeurs);

        $valeursfausses = ['id'=>"1",
    'name'=>"name",
    'posted_falsename'=>"posted_filename",
    'mimetype'=>"not_mimetype",
    'size'=>"size",
    'sign'=>"sign"];

        $this->assertEquals($fichier->correspondAttributs($valeursfausses), false);
    }

    public function testGestionAnnexesTampons()
    {
        $json=file_get_contents($this->repertoireTests."actes_transac_get_status_OK1");
        $listeFichiers=file_get_contents($this->repertoireTests."actes_transac_get_files_list_1");
        $connectorS2low = $this->createMock('ConnectorS2low');
        $connectorS2low->method('getFluxRetour')->willReturn($json);
        $connectorS2low->method('getActeDocuments')->willReturn($listeFichiers);

        $connectorS2low->method('getActeDocumentTampon')->willReturn('Acte Tamponne');

        $acte = new ActeTdtS2low("0", $connectorS2low);
        $details = $acte->getAnnexesDetails();

        $annexesAttendues = [
               'id' => 6,
               'name' => '22_AG-034-491011698-20171107-PEV_ESSAI_1-AI-1-1_2.pdf',
               'posted_filename' => 'compilation.pdf',
               'mimetype' => 'application/pdf',
               'size' => '3414581'];

        $this->assertEquals($details[0], $annexesAttendues);
        //FIX
        //$annexes=$acte->findAnnexes(["size"=>'3414581']);
        //$this->assertEquals($annexes, ['Acte Tamponne']);
    }
}
