<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AnnexesMatcher', 'Lib/Tdt');

/**
 * Classe AnnexesMatcherTest
 *
 * @version 4.3
 * @package app.Test.Lib
 */
class AnnexesMatcherTest extends CakeTestCase
{
    public function arrays_are_similar($a, $b)
    {
        // Si le nombre d'éléments est différent, retourner
        if (count($a)!=count($b)) {
            return false;
        }
        // we know that the indexes, but maybe not values, match.
        // compare the values between the two arrays
        foreach ($a as $k => $v) {
            if (array_search($v, $b, true)===false) {
                throw new Exception(json_encode($v)."\n".json_encode($b));
            }
        }
        // we have identical indexes, and no unequal values
        return true;
    }
    public function testPermutationsEntreeNulle()
    {
        $entree = [];

        $sortie = [["nom"=>"Un"]];

        $annexesMatcher= new AnnexesMatcher($entree,$sortie);
        $permutations=$annexesMatcher->permutations($entree, count($sortie));

        foreach ($permutations as $p) {
            $permutationCalculee[]=$p;
        }

        $this->assertEquals([[null]], $permutationCalculee);
    }
    public function testPermutationsEntreeUnUn()
    {
        $entree = [["nom"=>"Un"]];

        $sortie = [["nom"=>"Un"]];

        $annexesMatcher= new AnnexesMatcher($entree,$sortie);
        $permutations=$annexesMatcher->permutations($entree, count($sortie));

        foreach ($permutations as $p) {
            $permutationCalculee[]=$p;
        }

        $this->assertEquals([[["nom"=>"Un"]]], $permutationCalculee);
    }
    public function testPermutationsEntreeDeuxUn()
    {
        $entree = [["nom"=>"Un"],["nom"=>"Deux"]];
        $sortie = [["nom"=>"Un"]];

        $annexesMatcher= new AnnexesMatcher($entree,$sortie);
        $permutations=$annexesMatcher->permutations($entree, count($sortie));

        foreach ($permutations as $p) {
            $permutationCalculee[]=$p;
        }
        $this->assertEquals([[["nom"=>"Un"]],[["nom"=>"Deux"]]], $permutationCalculee);
    }
    public function testPermutationsEntreeUnDeux()
    {
        $entree = [["nom"=>"Un"]];
        $sortie = [["nom"=>"Un"],["nom"=>"Deux"]];

        $annexesMatcher= new AnnexesMatcher($entree,$sortie);
        $permutations=$annexesMatcher->permutations($entree, count($sortie));

        foreach ($permutations as $p) {
            $permutationCalculee[]=$p;
        }

        $this->assertEquals([[["nom"=>"Un"],null],[null,["nom"=>"Un"]]], $permutationCalculee);
    }
    public function testPermutationsEntreeDeuxDeux()
    {
        $entree = [["nom"=>"Un"],["nom"=>"Deux"]];
        $sortie = [["nom"=>"Un"],["nom"=>"Deux"]];

        $annexesMatcher= new AnnexesMatcher($entree,$sortie);
        $permutations=$annexesMatcher->permutations($entree, count($sortie));

        foreach ($permutations as $p) {
            $permutationCalculee[]=$p;
        }

        $this->assertEquals([[["nom"=>"Un"],["nom"=>"Deux"]],[["nom"=>"Deux"],["nom"=>"Un"]]], $permutationCalculee);
    }

    public function testEntreeSortieIdentique()
    {
        $entree = [["id"=>1,"filename"=>"Machinchose",'size'=>5692],
            ["id"=>2,"filename"=>"Truc",'size'=>4684],
            ["id"=>3,"filename"=>"Bidule",'size'=>486464]];

        $sortie = [["id"=>2,"posted_filename"=>"Truc",'size'=>4684],
            ["id"=>1,"posted_filename"=>"Machinchose",'size'=>5692],
            ["id"=>3,"posted_filename"=>"Bidule",'size'=>486464]];

        $sortieAttendue = [["id_WD"=>1,"id_S2low"=>1],//,"nom_entree"=>"Machinchose",'taille_entree'=>5692,"nom_sortie"=>"Machinchose",'taille_sortie'=>5692],
            ["id_WD"=>2,"id_S2low"=>2],//"nom_entree"=>"Truc",'taille_entree'=>4684,"nom_sortie"=>"Truc",'taille_sortie'=>4684],
            ["id_WD"=>3,"id_S2low"=>3]];//,"nom_entree"=>"Bidule",'taille_entree'=>486464,"nom_sortie"=>"Bidule",'taille_sortie'=>486464]];
        $annexesMatcher= new AnnexesMatcher($entree, $sortie);
        $closestArrangement=$annexesMatcher->findClosestArrangement();
        $this->assertTrue($this->arrays_are_similar($sortieAttendue, $closestArrangement));
    }

    public function testEntreeSortieModifiees()
    {
        $entree = [["id"=>1,"filename"=>"Machinchose",'size'=>5692],
            ["id"=>2,"filename"=>"Truc",'size'=>4684],
            ["id"=>3,"filename"=>"Bidule",'size'=>486464]];

        $sortie = [["id"=>1,"posted_filename"=>"Trucmod",'size'=>4683],
            ["id"=>2,"posted_filename"=>"Machinchosemod",'size'=>5695],
            ["id"=>3,"posted_filename"=>"Bidulemod",'size'=>486468]];

        $sortieAttendue = [["id_WD"=>1,"id_S2low"=>2],//"nom_sortie"=>"Machinchosemod",'taille_sortie'=>5695,"nom_entree"=>"Machinchose",'taille_entree'=>5692],
            ["id_WD"=>2,"id_S2low"=>1],//"nom_sortie"=>"Trucmod",'taille_sortie'=>4683,"nom_entree"=>"Truc",'taille_entree'=>4684],
            ["id_WD"=>3,"id_S2low"=>3]];//,"nom_sortie"=>"Bidulemod",'taille_sortie'=>486468,"nom_entree"=>"Bidule",'taille_entree'=>486464]];

        $annexesMatcher= new AnnexesMatcher($entree, $sortie);
        $closestArrangement=$annexesMatcher->findClosestArrangement();

        $this->assertTrue($this->arrays_are_similar($sortieAttendue, $closestArrangement));
    }
    public function testEntreePlusPetite()
    {
        $entree = [["id"=>1,"filename"=>"Machinchose",'size'=>5692],
            ["id"=>2,"filename"=>"Truc",'size'=>4684]];

        $sortie = [["id"=>1,"posted_filename"=>"Truc",'size'=>4684],
            ["id"=>2,"posted_filename"=>"Machinchose",'size'=>5692],
            ["id"=>3,"posted_filename"=>"Bidule",'size'=>486464]];

        $sortieAttendue = [["id_WD"=>1,"id_S2low"=>2],//"nom_entree"=>"Machinchose",'taille_entree'=>5692,"nom_sortie"=>"Machinchose",'taille_sortie'=>5692],
            ["id_WD"=>2,"id_S2low"=>1]];//,"nom_entree"=>"Truc",'taille_entree'=>4684,"nom_sortie"=>"Truc",'taille_sortie'=>4684]];
        $annexesMatcher= new AnnexesMatcher($entree, $sortie);
        $closestArrangement=$annexesMatcher->findClosestArrangement();

        $this->assertTrue($this->arrays_are_similar($sortieAttendue, $closestArrangement));
    }
    public function testSortiePlusPetite()
    {
        $entree = [["id"=>1,"filename"=>"Machinchose",'size'=>5692],
            ["id"=>2,"filename"=>"Truc",'size'=>4684],
            ["id"=>3,"filename"=>"Bidule",'size'=>486464]];

        $sortie = [["id"=>1,"posted_filename"=>"Truc",'size'=>4684],
            ["id"=>2,"posted_filename"=>"Machinchose",'size'=>5692]];

        $sortieAttendue=[["id_WD"=>1,"id_S2low"=>2],//"nom_entree"=>"Machinchose",'taille_entree'=>5692,"nom_sortie"=>"Machinchose",'taille_sortie'=>5692],
            ["id_WD"=>2,"id_S2low"=>1]];//,"nom_entree"=>"Truc",'taille_entree'=>4684,"nom_sortie"=>"Truc",'taille_sortie'=>4684]];

        $annexesMatcher= new AnnexesMatcher($entree, $sortie);
        $closestArrangement=$annexesMatcher->findClosestArrangement();

        $this->assertTrue($this->arrays_are_similar($sortieAttendue, $closestArrangement));
    }
    
    public function testCharge(){
        $entree = [["id"=>1,"filename"=>"Machinchose",'size'=>5692],
            ["id"=>2,"filename"=>"Truc1",'size'=>4684],
            ["id"=>3,"filename"=>"Truc2",'size'=>4684],
            ["id"=>4,"filename"=>"Truc3",'size'=>4684],
            ["id"=>5,"filename"=>"Truc4",'size'=>4684],
            ["id"=>6,"filename"=>"Truc5",'size'=>4684],
            ["id"=>7,"filename"=>"Truc6",'size'=>4684],
            ["id"=>8,"filename"=>"Truc7",'size'=>4684],
            ["id"=>9,"filename"=>"Truc8",'size'=>4684],
            ["id"=>10,"filename"=>"Truc9",'size'=>4684],
            ["id"=>11,"filename"=>"Truc10",'size'=>4684],
            ["id"=>12,"filename"=>"Truc11",'size'=>4684],
            ["id"=>13,"filename"=>"Truc12",'size'=>4684],
            ["id"=>14,"filename"=>"Truc13",'size'=>4684],
            ["id"=>15,"filename"=>"Truc14",'size'=>4684],
            ["id"=>16,"filename"=>"Truc15",'size'=>4684],
            ["id"=>17,"filename"=>"Truc16",'size'=>4684],
            ["id"=>18,"filename"=>"Truc17",'size'=>4684],
            ["id"=>19,"filename"=>"Truc18",'size'=>4684],
            ["id"=>20,"filename"=>"Truc19",'size'=>4684],
            ["id"=>21,"filename"=>"Truc20",'size'=>4684],
            ["id"=>22,"filename"=>"Truc21",'size'=>4684],
            ["id"=>23,"filename"=>"Truc22",'size'=>4684],
            ["id"=>24,"filename"=>"Truc23",'size'=>4684],
            ["id"=>25,"filename"=>"Truc24",'size'=>4684],
            ["id"=>26,"filename"=>"Truc25",'size'=>4684],
            ["id"=>27,"filename"=>"Truc26",'size'=>4684],
            ["id"=>28,"filename"=>"Truc27",'size'=>4684],
            ["id"=>29,"filename"=>"Truc28",'size'=>4684],
            ["id"=>30,"filename"=>"Bidule",'size'=>486464]];
        
        $sortie = [["id"=>1,"posted_filename"=>"Machinchose",'size'=>5692],
            ["id"=>2,"posted_filename"=>"Truc1",'size'=>4684],
            ["id"=>3,"posted_filename"=>"Truc2",'size'=>4684],
            ["id"=>4,"posted_filename"=>"Truc3",'size'=>4684],
            ["id"=>5,"posted_filename"=>"Truc4",'size'=>4684],
            ["id"=>6,"posted_filename"=>"Truc5",'size'=>4684],
            ["id"=>7,"posted_filename"=>"Truc6",'size'=>4684],
            ["id"=>8,"posted_filename"=>"Truc7",'size'=>4684],
            ["id"=>9,"posted_filename"=>"Truc8",'size'=>4684],
            ["id"=>10,"posted_filename"=>"Truc9",'size'=>4684],
            ["id"=>11,"posted_filename"=>"Truc10",'size'=>4684],
            ["id"=>12,"posted_filename"=>"Truc11",'size'=>4684],
            ["id"=>13,"posted_filename"=>"Truc12",'size'=>4684],
            ["id"=>14,"posted_filename"=>"Truc13",'size'=>4684],
            ["id"=>15,"posted_filename"=>"Truc14",'size'=>4684],
            ["id"=>16,"posted_filename"=>"Truc15",'size'=>4684],
            ["id"=>17,"posted_filename"=>"Truc16",'size'=>4684],
            ["id"=>18,"posted_filename"=>"Truc17",'size'=>4684],
            ["id"=>19,"posted_filename"=>"Truc18",'size'=>4684],
            ["id"=>20,"posted_filename"=>"Truc19",'size'=>4684],
            ["id"=>21,"posted_filename"=>"Truc20",'size'=>4684],
            ["id"=>22,"posted_filename"=>"Truc21",'size'=>4684],
            ["id"=>23,"posted_filename"=>"Truc22",'size'=>4684],
            ["id"=>24,"posted_filename"=>"Truc23",'size'=>4684],
            ["id"=>25,"posted_filename"=>"Truc24",'size'=>4684],
            ["id"=>26,"posted_filename"=>"Truc25",'size'=>4684],
            ["id"=>27,"posted_filename"=>"Truc26",'size'=>4684],
            ["id"=>28,"posted_filename"=>"Truc27",'size'=>4684],
            ["id"=>29,"posted_filename"=>"Truc28",'size'=>4684],
            ["id"=>30,"posted_filename"=>"Bidule",'size'=>486464]];
        
        $sortieAttendue = [["id_WD"=>1,"id_S2low"=>1],
            ["id_WD"=>2,"id_S2low"=>2],
            ["id_WD"=>3,"id_S2low"=>3],
            ["id_WD"=>4,"id_S2low"=>4],
            ["id_WD"=>5,"id_S2low"=>5],
            ["id_WD"=>6,"id_S2low"=>6],
            ["id_WD"=>7,"id_S2low"=>7],
            ["id_WD"=>8,"id_S2low"=>8],
            ["id_WD"=>9,"id_S2low"=>9],
            ["id_WD"=>10,"id_S2low"=>10],
            ["id_WD"=>11,"id_S2low"=>11],
            ["id_WD"=>12,"id_S2low"=>12],
            ["id_WD"=>13,"id_S2low"=>13],
            ["id_WD"=>14,"id_S2low"=>14],
            ["id_WD"=>15,"id_S2low"=>15],
            ["id_WD"=>16,"id_S2low"=>16],
            ["id_WD"=>17,"id_S2low"=>17],
            ["id_WD"=>18,"id_S2low"=>18],
            ["id_WD"=>19,"id_S2low"=>19],
            ["id_WD"=>20,"id_S2low"=>20],
            ["id_WD"=>21,"id_S2low"=>21],
            ["id_WD"=>22,"id_S2low"=>22],
            ["id_WD"=>23,"id_S2low"=>23],
            ["id_WD"=>24,"id_S2low"=>24],
            ["id_WD"=>25,"id_S2low"=>25],
            ["id_WD"=>26,"id_S2low"=>26],
            ["id_WD"=>27,"id_S2low"=>27],
            ["id_WD"=>28,"id_S2low"=>28],
            ["id_WD"=>29,"id_S2low"=>29],
            ["id_WD"=>30,"id_S2low"=>30]];
        
        $annexesMatcher= new AnnexesMatcher($entree,$sortie);
        $closestArrangement=$annexesMatcher->findClosestArrangement();
    
        $this->assertTrue($this->arrays_are_similar($sortieAttendue,$closestArrangement));
    }
    
    public function testCharge2(){
        $entree = [["id"=>1,"filename"=>"Machinchose",'size'=>5692],
            ["id"=>2,"filename"=>"Truc1",'size'=>4684],
            ["id"=>3,"filename"=>"Truc2",'size'=>4684],
            ["id"=>4,"filename"=>"Truc3",'size'=>4684],
            ["id"=>5,"filename"=>"Truc4",'size'=>4684],
            ["id"=>6,"filename"=>"Truc5",'size'=>4684],
            ["id"=>7,"filename"=>"Truc6",'size'=>4684],
            ["id"=>8,"filename"=>"Truc7",'size'=>4684],
            ["id"=>9,"filename"=>"Truc8",'size'=>4684],
            ["id"=>10,"filename"=>"Truc9",'size'=>4684],
            ["id"=>11,"filename"=>"Truc10",'size'=>4684],
            ["id"=>12,"filename"=>"Truc11",'size'=>4684],
            ["id"=>13,"filename"=>"Truc12",'size'=>4684],
            ["id"=>14,"filename"=>"Truc13",'size'=>4684],
            ["id"=>15,"filename"=>"Truc14",'size'=>4684],
            ["id"=>16,"filename"=>"Truc15",'size'=>4684],
            ["id"=>17,"filename"=>"Truc16",'size'=>4684],
            ["id"=>18,"filename"=>"Truc17",'size'=>4684],
            ["id"=>19,"filename"=>"Truc18",'size'=>4684],
            ["id"=>20,"filename"=>"Truc19",'size'=>4684],
            ["id"=>21,"filename"=>"Truc20",'size'=>4684],
            ["id"=>22,"filename"=>"Truc21",'size'=>4684],
            ["id"=>23,"filename"=>"Truc22",'size'=>4684],
            ["id"=>24,"filename"=>"Truc23",'size'=>4684],
            ["id"=>25,"filename"=>"Truc24",'size'=>4684],
            ["id"=>26,"filename"=>"Truc25",'size'=>4684],
            ["id"=>27,"filename"=>"Truc26",'size'=>4684],
            ["id"=>28,"filename"=>"Truc27",'size'=>4684],
            ["id"=>29,"filename"=>"Truc28",'size'=>4684],
            ["id"=>30,"filename"=>"Bidule",'size'=>486464]];
        
        $sortie = [["id"=>1,"posted_filename"=>"Machinchose",'size'=>5692],
            ["id"=>2,"posted_filename"=>"Truc1",'size'=>4684],
            ["id"=>3,"posted_filename"=>"Truc2",'size'=>4684],
            ["id"=>4,"posted_filename"=>"Truc3",'size'=>4684],
            ["id"=>5,"posted_filename"=>"Truc4",'size'=>4684],
            ["id"=>6,"posted_filename"=>"Truc5",'size'=>4684],
            ["id"=>7,"posted_filename"=>"Truc6",'size'=>4684],
            ["id"=>8,"posted_filename"=>"Truc7",'size'=>4684],
            ["id"=>9,"posted_filename"=>"Truc8",'size'=>4684],
            ["id"=>10,"posted_filename"=>"Truc9",'size'=>4684],
            ["id"=>11,"posted_filename"=>"Truc10",'size'=>4684],
            ["id"=>12,"posted_filename"=>"Truc111",'size'=>4684],
            ["id"=>13,"posted_filename"=>"Truc122",'size'=>4684],
            ["id"=>14,"posted_filename"=>"Truc13",'size'=>4684],
            ["id"=>15,"posted_filename"=>"Truc14",'size'=>4684],
            ["id"=>16,"posted_filename"=>"Truc15",'size'=>4684],
            ["id"=>17,"posted_filename"=>"Truc16",'size'=>4684],
            ["id"=>18,"posted_filename"=>"Truc17",'size'=>4684],
            ["id"=>19,"posted_filename"=>"Truc18",'size'=>4684],
            ["id"=>20,"posted_filename"=>"Truc19",'size'=>4684],
            ["id"=>21,"posted_filename"=>"Truc20",'size'=>4684],
            ["id"=>22,"posted_filename"=>"Truc21",'size'=>4684],
            ["id"=>23,"posted_filename"=>"Truc22",'size'=>4684],
            ["id"=>24,"posted_filename"=>"Truc23",'size'=>4684],
            ["id"=>25,"posted_filename"=>"Truc24",'size'=>4684],
            ["id"=>26,"posted_filename"=>"Truc25",'size'=>4684],
            ["id"=>27,"posted_filename"=>"Truc26",'size'=>4684],
            ["id"=>28,"posted_filename"=>"Truc27",'size'=>4684],
            ["id"=>29,"posted_filename"=>"Truc28",'size'=>4684],
            ["id"=>30,"posted_filename"=>"Bidule",'size'=>486464]];
        
        $sortieAttendue = [["id_WD"=>1,"id_S2low"=>1],
            ["id_WD"=>2,"id_S2low"=>2],
            ["id_WD"=>3,"id_S2low"=>3],
            ["id_WD"=>4,"id_S2low"=>4],
            ["id_WD"=>5,"id_S2low"=>5],
            ["id_WD"=>6,"id_S2low"=>6],
            ["id_WD"=>7,"id_S2low"=>7],
            ["id_WD"=>8,"id_S2low"=>8],
            ["id_WD"=>9,"id_S2low"=>9],
            ["id_WD"=>10,"id_S2low"=>10],
            ["id_WD"=>11,"id_S2low"=>11],
            ["id_WD"=>12,"id_S2low"=>12],
            ["id_WD"=>13,"id_S2low"=>13],
            ["id_WD"=>14,"id_S2low"=>14],
            ["id_WD"=>15,"id_S2low"=>15],
            ["id_WD"=>16,"id_S2low"=>16],
            ["id_WD"=>17,"id_S2low"=>17],
            ["id_WD"=>18,"id_S2low"=>18],
            ["id_WD"=>19,"id_S2low"=>19],
            ["id_WD"=>20,"id_S2low"=>20],
            ["id_WD"=>21,"id_S2low"=>21],
            ["id_WD"=>22,"id_S2low"=>22],
            ["id_WD"=>23,"id_S2low"=>23],
            ["id_WD"=>24,"id_S2low"=>24],
            ["id_WD"=>25,"id_S2low"=>25],
            ["id_WD"=>26,"id_S2low"=>26],
            ["id_WD"=>27,"id_S2low"=>27],
            ["id_WD"=>28,"id_S2low"=>28],
            ["id_WD"=>29,"id_S2low"=>29],
            ["id_WD"=>30,"id_S2low"=>30]];
        
        $annexesMatcher= new AnnexesMatcher($entree,$sortie);
        $closestArrangement=$annexesMatcher->findClosestArrangement();
    
        $this->assertTrue($this->arrays_are_similar($sortieAttendue,$closestArrangement));
    }
    
    function testConversion(){
        $entree = [["id"=>1,"filename"=>"Machinchose.odt",'size'=>5692],
            ["id"=>2,"filename"=>"Truc1.odt",'size'=>4684],
            ["id"=>3,"filename"=>"Truc2.odt",'size'=>4684],
            ["id"=>4,"filename"=>"Truc3.odt",'size'=>4684],
            ["id"=>5,"filename"=>"Truc4.odt",'size'=>4684],
            ["id"=>6,"filename"=>"Truc5.odt",'size'=>4684],
            ["id"=>7,"filename"=>"Truc6.odt",'size'=>4684],
            ["id"=>8,"filename"=>"Truc7.odt",'size'=>4684],
            ["id"=>9,"filename"=>"Truc8.odt",'size'=>4684],
            ["id"=>10,"filename"=>"Truc9.odt",'size'=>4684],
            ["id"=>11,"filename"=>"Truc10.odt",'size'=>4684],
            ["id"=>12,"filename"=>"Truc11.odt",'size'=>4684],
            ["id"=>13,"filename"=>"Truc12.odt",'size'=>4684],
            ["id"=>14,"filename"=>"Truc13.odt",'size'=>4684],
            ["id"=>15,"filename"=>"Truc14.odt",'size'=>4684],
            ["id"=>16,"filename"=>"Truc15.odt",'size'=>4684],
            ["id"=>17,"filename"=>"Truc16.odt",'size'=>4684],
            ["id"=>18,"filename"=>"Truc17.odt",'size'=>4684],
            ["id"=>19,"filename"=>"Truc18.odt",'size'=>4684],
            ["id"=>20,"filename"=>"Truc19.odt",'size'=>4684],
            ["id"=>21,"filename"=>"Truc20.odt",'size'=>4684],
            ["id"=>22,"filename"=>"Truc21.odt",'size'=>4684],
            ["id"=>23,"filename"=>"Truc22.odt",'size'=>4684],
            ["id"=>24,"filename"=>"Truc23.odt",'size'=>4684],
            ["id"=>25,"filename"=>"Truc24.odt",'size'=>4684],
            ["id"=>26,"filename"=>"Truc25.odt",'size'=>4684],
            ["id"=>27,"filename"=>"Truc26.odt",'size'=>4684],
            ["id"=>28,"filename"=>"Truc27.odt",'size'=>4684],
            ["id"=>29,"filename"=>"Truc28.odt",'size'=>4684],
            ["id"=>30,"filename"=>"Bidule.odt",'size'=>486464]];
        
        $sortie = [["id"=>1,"posted_filename"=>"Machinchose.pdf",'size'=>5692],
            ["id"=>2,"posted_filename"=>"Truc1.pdf",'size'=>4684],
            ["id"=>3,"posted_filename"=>"Truc2.pdf",'size'=>4684],
            ["id"=>4,"posted_filename"=>"Truc3.pdf",'size'=>4684],
            ["id"=>5,"posted_filename"=>"Truc4.pdf",'size'=>4684],
            ["id"=>6,"posted_filename"=>"Truc5.pdf",'size'=>4684],
            ["id"=>7,"posted_filename"=>"Truc6.pdf",'size'=>4684],
            ["id"=>8,"posted_filename"=>"Truc7.pdf",'size'=>4684],
            ["id"=>9,"posted_filename"=>"Truc8.pdf",'size'=>4684],
            ["id"=>10,"posted_filename"=>"Truc9.pdf",'size'=>4684],
            ["id"=>11,"posted_filename"=>"Truc10.pdf",'size'=>4684],
            ["id"=>12,"posted_filename"=>"Truc111.pdf",'size'=>4684],
            ["id"=>13,"posted_filename"=>"Truc122.pdf",'size'=>4684],
            ["id"=>14,"posted_filename"=>"Truc13.pdf",'size'=>4684],
            ["id"=>15,"posted_filename"=>"Truc14.pdf",'size'=>4684],
            ["id"=>16,"posted_filename"=>"Truc15.pdf",'size'=>4684],
            ["id"=>17,"posted_filename"=>"Truc16.pdf",'size'=>4684],
            ["id"=>18,"posted_filename"=>"Truc17.pdf",'size'=>4684],
            ["id"=>19,"posted_filename"=>"Truc18.pdf",'size'=>4684],
            ["id"=>20,"posted_filename"=>"Truc19.pdf",'size'=>4684],
            ["id"=>21,"posted_filename"=>"Truc20.pdf",'size'=>4684],
            ["id"=>22,"posted_filename"=>"Truc21.pdf",'size'=>4684],
            ["id"=>23,"posted_filename"=>"Truc22.pdf",'size'=>4684],
            ["id"=>24,"posted_filename"=>"Truc23.pdf",'size'=>4684],
            ["id"=>25,"posted_filename"=>"Truc24.pdf",'size'=>4684],
            ["id"=>26,"posted_filename"=>"Truc25.pdf",'size'=>4684],
            ["id"=>27,"posted_filename"=>"Truc26.pdf",'size'=>4684],
            ["id"=>28,"posted_filename"=>"Truc27.pdf",'size'=>4684],
            ["id"=>29,"posted_filename"=>"Truc28.pdf",'size'=>4684],
            ["id"=>30,"posted_filename"=>"Bidule.pdf",'size'=>486464]];
        
        $sortieAttendue = [["id_WD"=>1,"id_S2low"=>1],
            ["id_WD"=>2,"id_S2low"=>2],
            ["id_WD"=>3,"id_S2low"=>3],
            ["id_WD"=>4,"id_S2low"=>4],
            ["id_WD"=>5,"id_S2low"=>5],
            ["id_WD"=>6,"id_S2low"=>6],
            ["id_WD"=>7,"id_S2low"=>7],
            ["id_WD"=>8,"id_S2low"=>8],
            ["id_WD"=>9,"id_S2low"=>9],
            ["id_WD"=>10,"id_S2low"=>10],
            ["id_WD"=>11,"id_S2low"=>11],
            ["id_WD"=>12,"id_S2low"=>12],
            ["id_WD"=>13,"id_S2low"=>13],
            ["id_WD"=>14,"id_S2low"=>14],
            ["id_WD"=>15,"id_S2low"=>15],
            ["id_WD"=>16,"id_S2low"=>16],
            ["id_WD"=>17,"id_S2low"=>17],
            ["id_WD"=>18,"id_S2low"=>18],
            ["id_WD"=>19,"id_S2low"=>19],
            ["id_WD"=>20,"id_S2low"=>20],
            ["id_WD"=>21,"id_S2low"=>21],
            ["id_WD"=>22,"id_S2low"=>22],
            ["id_WD"=>23,"id_S2low"=>23],
            ["id_WD"=>24,"id_S2low"=>24],
            ["id_WD"=>25,"id_S2low"=>25],
            ["id_WD"=>26,"id_S2low"=>26],
            ["id_WD"=>27,"id_S2low"=>27],
            ["id_WD"=>28,"id_S2low"=>28],
            ["id_WD"=>29,"id_S2low"=>29],
            ["id_WD"=>30,"id_S2low"=>30]];
        
        $annexesMatcher= new AnnexesMatcher($entree,$sortie);
        $closestArrangement=$annexesMatcher->findClosestArrangement();
    
        $this->assertTrue($this->arrays_are_similar($sortieAttendue,$closestArrangement));
    }
    
  /**
     * [testGestionReponseVide description]
     * @expectedException Exception
     * @expectedExceptionMessage Nombre d'annexes au nom altéré trop important
     * @return [type] [description]
     */
    
    function testConversionImpossible(){
        $entree = [["id"=>1,"filename"=>"Machinchose.odt1",'size'=>5692],
            ["id"=>2,"filename"=>"Truc1.odt1",'size'=>4684],
            ["id"=>3,"filename"=>"Truc2.odt1",'size'=>4684],
            ["id"=>4,"filename"=>"Truc3.odt1",'size'=>4684],
            ["id"=>5,"filename"=>"Truc4.odt1",'size'=>4684],
            ["id"=>6,"filename"=>"Truc5.odt1",'size'=>4684],
            ["id"=>7,"filename"=>"Truc6.odt1",'size'=>4684],
            ["id"=>8,"filename"=>"Truc7.odt1",'size'=>4684],
            ["id"=>9,"filename"=>"Truc8.odt1",'size'=>4684],
            ["id"=>10,"filename"=>"Truc9.odt1",'size'=>4684],
            ["id"=>11,"filename"=>"Truc10.odt1",'size'=>4684],
            ["id"=>12,"filename"=>"Truc11.odt1",'size'=>4684],
            ["id"=>13,"filename"=>"Truc12.odt1",'size'=>4684],
            ["id"=>14,"filename"=>"Truc13.odt1",'size'=>4684],
            ["id"=>15,"filename"=>"Truc14.odt1",'size'=>4684],
            ["id"=>16,"filename"=>"Truc15.odt1",'size'=>4684],
            ["id"=>17,"filename"=>"Truc16.odt1",'size'=>4684],
            ["id"=>18,"filename"=>"Truc17.odt1",'size'=>4684],
            ["id"=>19,"filename"=>"Truc18.odt1",'size'=>4684],
            ["id"=>20,"filename"=>"Truc19.odt1",'size'=>4684],
            ["id"=>21,"filename"=>"Truc20.odt1",'size'=>4684],
            ["id"=>22,"filename"=>"Truc21.odt1",'size'=>4684],
            ["id"=>23,"filename"=>"Truc22.odt1",'size'=>4684],
            ["id"=>24,"filename"=>"Truc23.odt1",'size'=>4684],
            ["id"=>25,"filename"=>"Truc24.odt1",'size'=>4684],
            ["id"=>26,"filename"=>"Truc25.odt1",'size'=>4684],
            ["id"=>27,"filename"=>"Truc26.odt1",'size'=>4684],
            ["id"=>28,"filename"=>"Truc27.odt1",'size'=>4684],
            ["id"=>29,"filename"=>"Truc28.odt1",'size'=>4684],
            ["id"=>30,"filename"=>"Bidule.odt1",'size'=>486464]];
        
        $sortie = [["id"=>1,"posted_filename"=>"Machinchose.pdf",'size'=>5692],
            ["id"=>2,"posted_filename"=>"Truc1.pdf",'size'=>4684],
            ["id"=>3,"posted_filename"=>"Truc2.pdf",'size'=>4684],
            ["id"=>4,"posted_filename"=>"Truc3.pdf",'size'=>4684],
            ["id"=>5,"posted_filename"=>"Truc4.pdf",'size'=>4684],
            ["id"=>6,"posted_filename"=>"Truc5.pdf",'size'=>4684],
            ["id"=>7,"posted_filename"=>"Truc6.pdf",'size'=>4684],
            ["id"=>8,"posted_filename"=>"Truc7.pdf",'size'=>4684],
            ["id"=>9,"posted_filename"=>"Truc8.pdf",'size'=>4684],
            ["id"=>10,"posted_filename"=>"Truc9.pdf",'size'=>4684],
            ["id"=>11,"posted_filename"=>"Truc10.pdf",'size'=>4684],
            ["id"=>12,"posted_filename"=>"Truc111.pdf",'size'=>4684],
            ["id"=>13,"posted_filename"=>"Truc122.pdf",'size'=>4684],
            ["id"=>14,"posted_filename"=>"Truc13.pdf",'size'=>4684],
            ["id"=>15,"posted_filename"=>"Truc14.pdf",'size'=>4684],
            ["id"=>16,"posted_filename"=>"Truc15.pdf",'size'=>4684],
            ["id"=>17,"posted_filename"=>"Truc16.pdf",'size'=>4684],
            ["id"=>18,"posted_filename"=>"Truc17.pdf",'size'=>4684],
            ["id"=>19,"posted_filename"=>"Truc18.pdf",'size'=>4684],
            ["id"=>20,"posted_filename"=>"Truc19.pdf",'size'=>4684],
            ["id"=>21,"posted_filename"=>"Truc20.pdf",'size'=>4684],
            ["id"=>22,"posted_filename"=>"Truc21.pdf",'size'=>4684],
            ["id"=>23,"posted_filename"=>"Truc22.pdf",'size'=>4684],
            ["id"=>24,"posted_filename"=>"Truc23.pdf",'size'=>4684],
            ["id"=>25,"posted_filename"=>"Truc24.pdf",'size'=>4684],
            ["id"=>26,"posted_filename"=>"Truc25.pdf",'size'=>4684],
            ["id"=>27,"posted_filename"=>"Truc26.pdf",'size'=>4684],
            ["id"=>28,"posted_filename"=>"Truc27.pdf",'size'=>4684],
            ["id"=>29,"posted_filename"=>"Truc28.pdf",'size'=>4684],
            ["id"=>30,"posted_filename"=>"Bidule.pdf",'size'=>486464]];
        
        $sortieAttendue = [["id_WD"=>1,"id_S2low"=>1],
            ["id_WD"=>2,"id_S2low"=>2],
            ["id_WD"=>3,"id_S2low"=>3],
            ["id_WD"=>4,"id_S2low"=>4],
            ["id_WD"=>5,"id_S2low"=>5],
            ["id_WD"=>6,"id_S2low"=>6],
            ["id_WD"=>7,"id_S2low"=>7],
            ["id_WD"=>8,"id_S2low"=>8],
            ["id_WD"=>9,"id_S2low"=>9],
            ["id_WD"=>10,"id_S2low"=>10],
            ["id_WD"=>11,"id_S2low"=>11],
            ["id_WD"=>12,"id_S2low"=>12],
            ["id_WD"=>13,"id_S2low"=>13],
            ["id_WD"=>14,"id_S2low"=>14],
            ["id_WD"=>15,"id_S2low"=>15],
            ["id_WD"=>16,"id_S2low"=>16],
            ["id_WD"=>17,"id_S2low"=>17],
            ["id_WD"=>18,"id_S2low"=>18],
            ["id_WD"=>19,"id_S2low"=>19],
            ["id_WD"=>20,"id_S2low"=>20],
            ["id_WD"=>21,"id_S2low"=>21],
            ["id_WD"=>22,"id_S2low"=>22],
            ["id_WD"=>23,"id_S2low"=>23],
            ["id_WD"=>24,"id_S2low"=>24],
            ["id_WD"=>25,"id_S2low"=>25],
            ["id_WD"=>26,"id_S2low"=>26],
            ["id_WD"=>27,"id_S2low"=>27],
            ["id_WD"=>28,"id_S2low"=>28],
            ["id_WD"=>29,"id_S2low"=>29],
            ["id_WD"=>30,"id_S2low"=>30]];
        
        $annexesMatcher= new AnnexesMatcher($entree,$sortie);
        $closestArrangement=$annexesMatcher->findClosestArrangement();
    
    }
}
