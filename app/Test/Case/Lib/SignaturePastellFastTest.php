<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Signature', 'Lib');
App::uses('ComponentCollection', 'Controller');
App::uses('PastellComponent', 'Controller/Component');
/**
 * Classe SignatureTest.
 *
 * @version 4.3
 * @package app.Test.Lib
 */
class SignaturePastellFastTest extends CakeTestCase
{
    private $Signature;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.User',
        'app.Deliberation',
        'app.Historique',
        'app.Collectivite',
        'app.Typeacte'
    ];

    public function setUp()
    {
        parent::setUp();
        $this->markTestSkipped('must be revisited.');

        $this->Deliberation = ClassRegistry::init('Deliberation');
        $this->Collectivite = ClassRegistry::init('Collectivite');
        $this->Collectivite->id = 1;
        $this->idE = $aData['id_entity'] = getenv('PASTELL_IDE_FAST');
        $this->Collectivite->save($aData, false);
        Configure::write('IPARAPHEUR_TYPE', 'FAST');
        Configure::write('PASTELL_LOGIN', env('PASTELL_LOGIN_FAST'));
        Configure::write('PASTELL_PWD', env('PASTELL_PWD_FAST'));
        $this->Signature = new Signature();
        // Configurer notre component et faire semblant de tester le controller
        $Collection = new ComponentCollection();
        $this->PastellComponent = new PastellComponent($Collection);
    }

    /**
     * Méthode exécutée avant chaque test.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->Deliberation);
        unset($this->Collectivite);
        unset($this->Signature);
    }

    /**
     * Test listCircuits()
     * @return void
     */
    public function testListCircuits()
    {
        $expected = ["SIGNATURE-PDF","SIGNATURE-PDF-AUTOMATIQUE","SIGNATURE-PES"];
        $aPastell = $this->Signature->listCircuits();

        $this->assertEquals($expected, $aPastell, var_export(array_combine($expected, $aPastell), true)); //ca bug la
    }

    /**
     * Test updateAll()
     * @return void
     */
    public function testUpdateAll(): void
    {
        $this->markTestSkipped('must be revisited.');

        $retour = $this->Signature->updateAll();

        $this->assertEquals('TRAITEMENT_TERMINE_OK', trim($retour), var_export($retour, true));
    }
//
//    /**
//     * Test Send()
//     * @return void
//     */
//    public function testSend()
//    {
//        $this->Deliberation->id = 1;
//        $acte = $this->Deliberation->find('first', [
//            'contain' => ['Typeacte.nature_id'],
//            'conditions' => ['Deliberation.id' => $this->Deliberation->id],
//            'recursive' => -1
//        ]);
//        $acte['acte_nature_code'] = 1;
//        $acte['Deliberation']['typologiepiece_code']='99_DE';
//        $acte['Deliberation']['annexe_typologiepiece_code']=['40_AC'];
//
//        $files = [
//            'docPrincipale' => file_get_contents(APP . 'Test/Data/AnnexFixture.pdf'),
//            'annexes' => [
//                ['filename' => 'AnnexFixture.pdf',
//                'content' => file_get_contents(APP . 'Test/Data/AnnexFixture.pdf')]
//            ],
//        ];
//
//        $idD = $this->Signature->send(
//            $acte,
//            'SIGNATURE-PDF',
//            $files['docPrincipale'],
//            $files['annexes']
//        );
//
//        $this->PastellComponent->delete($this->idE, $idD);
//        //FIX suppression dans pastell du fichier
//    }
}
