<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Signature', 'Lib');
App::uses('ComponentCollection', 'Controller');
App::uses('PastellComponent', 'Controller/Component');
/**
 * Classe SignatureTest.
 *
 * @version 4.3
 * @package app.Test.Lib
 */
class VisaPastellParapheurTest extends CakeTestCase
{
    private $Signature;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.User',
        'app.Deliberation',
        'app.Historique',
        'app.Collectivite',
        'app.Typeacte'
    ];

    public function setUp()
    {
        $this->markTestSkipped('must be revisited.');
        parent::setUp();
        $this->Deliberation = ClassRegistry::init('Deliberation');
        $this->Collectivite = ClassRegistry::init('Collectivite');

        $this->Signature = new Signature();
        // Configurer notre component et faire semblant de tester le controller
        $Collection = new ComponentCollection();
        $this->PastellComponent = new PastellComponent($Collection);
    }

    /**
     * Méthode exécutée avant chaque test.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->Deliberation);
        unset($this->Collectivite);
        unset($this->Signature);
    }

    /**
     * Test listCircuits()
     * @return void
     */
//    public function testListVisas()
//    {
//        $expected = ["Courrier","Commande","Facture"];
//        $aPastell = $this->Signature->listCircuits();
//
//        $this->assertEquals($expected, $aPastell, var_export(array_combine($expected, $aPastell), true)); //ca bug la
//    }

    /**
     * Test updateAll()
     * @return void
     */
//    public function testUpdateAll(): void
//    {
//        $retour = $this->Signature->updateAll();
//
//        $this->assertEquals('TRAITEMENT_TERMINE_OK', trim($retour), var_export($retour, true));
//    }

    /**
     * Test Send()
     * @return void
     */
    public function testSend()
    {
        $this->Deliberation->id = 1;
        $acte = $this->Deliberation->find('first', [
            'contain' => ['Typeacte.nature_id'],
            'conditions' => ['Deliberation.id' => $this->Deliberation->id],
            'recursive' => -1
        ]);
        $acte['acte_nature_code'] = 1;
        $acte['Deliberation']['typologiepiece_code']='99_DE';
        $acte['Deliberation']['annexe_typologiepiece_code']=['40_AC'];

        $files = [
            'docPrincipale' => file_get_contents(APP . 'Test/Data/AnnexFixture.pdf'),
            'annexes' => [
                ['filename' => 'AnnexFixture.pdf',
                'content' => file_get_contents(APP . 'Test/Data/AnnexFixture.pdf')]
            ],
        ];

        $idD = $this->Signature->send(
            $acte,
            'SIGNATURE-PDF',
            $files['docPrincipale'],
            $files['annexes']
        );

        $info = $this->PastellComponent->detailDocument($this->idE, $idD);
        $journal = $this->PastellComponent->journal($this->idE, $idD);

        //Suppression dans pastell du dossier
        $this->PastellComponent->delete($this->idE, $idD);

        $this->assertEquals('recu-iparapheur', trim($info['last_action']['action']), var_export($info, true));
    }
}
