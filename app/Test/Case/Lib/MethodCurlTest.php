<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('MethodCurl', 'Lib');

/**
 * Classe TdtTest.
 *
 * @version 4.5
 * @package app.Test.Image
 */
class MethodCurlTest extends PHPUnit_Framework_TestCase
{
    public function testRequeteSimple()
    {
        $curl = new MethodCurl("urlBase", []);

        $OptionsWS = $curl->getoptionsWebService();

        $this->assertEquals(
            ["CurlOptions"=>[],
                             "GetParameters" => [],
                             "PostParameters"=>[]],
                             $OptionsWS,
                            var_export($OptionsWS, true)
        );

        $options = $curl->initOptionsRequete("/Chemin", []);
        $this->assertEquals("urlBase/Chemin", $options['CurlOptions'][CURLOPT_URL]);
        $this->assertFalse(isset($options[CURLOPT_POST]));
    }
    public function testRequeteParamsWS()
    {
        $curl = new MethodCurl("urlBase", ["CurlOptions"   =>["CurlOption1"=>"CurlOption1WebService"],
                                           "GetParameters" => ["GetParameter1"=>"GetParameter1WebService"],
                                            "PostParameters"=> ["PostParameter1"=>"PostParameter1WebService"]]);

        $OptionsWS = $curl->getoptionsWebService();

        $this->assertEquals(
            ["CurlOptions"=>["CurlOption1"=>"CurlOption1WebService"],
                             "GetParameters" => ["GetParameter1"=>"GetParameter1WebService"],
                             "PostParameters"=>["PostParameter1"=>"PostParameter1WebService"]],
                             $OptionsWS,
                            var_export($OptionsWS, true)
        );

        $options = $curl->initOptionsRequete("/Chemin", []);

        $this->assertEquals("urlBase/Chemin?GetParameter1=GetParameter1WebService", $options['CurlOptions'][CURLOPT_URL], var_export($options, true).var_export($OptionsWS, true));
    }

    public function testRequeteParamsRequete()
    {
        $curl = new MethodCurl("urlBase", []);

        $OptionsWS = $curl->getoptionsWebService();

        $this->assertEquals(
            ["CurlOptions"=>[],
                             "GetParameters" => [],
                             "PostParameters"=>[]],
                             $OptionsWS,
                            var_export($OptionsWS, true)
        );

        $optionsRequete = $curl->initOptionsRequete("/Chemin", ["CurlOptions"   =>["CurlOption1"=>"CurlOption1Requete"],
                                           "GetParameters" => ["GetParameter1"=>"GetParameter1Requete"],
                                            "PostParameters"=> ["PostParameter1"=>"PostParameter1Requete"]]);


        $this->assertEquals("urlBase/Chemin?GetParameter1=GetParameter1Requete", $optionsRequete['CurlOptions'][CURLOPT_URL], var_export($optionsRequete, true).var_export($OptionsWS, true));
    }

    public function testRequeteParamsCurl1()
    {
        $curl = new MethodCurl("urlBase", []);

        $OptionsWS = $curl->getoptionsWebService();

        $this->assertEquals(
            ["CurlOptions"=>[],
                             "GetParameters" => [],
                             "PostParameters"=>[]],
                             $OptionsWS,
                            var_export($OptionsWS, true)
        );

        $optionsRequete = $curl->initOptionsRequete("/Chemin", ["CurlOptions"   =>["CurlOption1"=>"CurlOption1Requete"],
                                           "GetParameters" => ["GetParameter1"=>"GetParameter1Requete"],
                                            "PostParameters"=> ["PostParameter1"=>"PostParameter1Requete"]]);


        $this->assertEquals("CurlOption1Requete", $optionsRequete['CurlOptions']["CurlOption1"], var_export($optionsRequete, true).var_export($OptionsWS, true));
    }

    public function testRequeteParamsCurl2()
    {
        $curl = new MethodCurl("urlBase", ["CurlOptions"=>["CurlOption2"=>"CurlOption2Requete"]]);

        $OptionsWS = $curl->getoptionsWebService();

        $this->assertEquals(
            ["CurlOptions"=>["CurlOption2"=>"CurlOption2Requete"],
                             "GetParameters" => [],
                             "PostParameters"=>[]],
                             $OptionsWS,
                            var_export($OptionsWS, true)
        );

        $optionsRequete = $curl->initOptionsRequete("/Chemin", ["CurlOptions"   =>["CurlOption1"=>"CurlOption1Requete"],
                                           "GetParameters" => ["GetParameter1"=>"GetParameter1Requete"],
                                            "PostParameters"=> ["PostParameter1"=>"PostParameter1Requete"]]);


        $this->assertEquals("CurlOption1Requete", $optionsRequete['CurlOptions']["CurlOption1"], var_export($optionsRequete, true).var_export($OptionsWS, true));
    }
    public function testRequeteParamsCurl3()
    {
        $options["CurlOptions"] = [
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSLCERT => Configure::read('S2LOW_PEM'),
                CURLOPT_SSLCERTPASSWD => Configure::read('S2LOW_CERTPWD'),
                CURLOPT_SSLKEY => Configure::read('S2LOW_SSLKEY')
            ];

        $fp = fopen('test', 'w');
        $curl = new MethodCurl("urlBase", $options);

        $OptionsWS = $curl->getoptionsWebService();

        /*$this->assertEquals(["CurlOptions"=>[],
                             "GetParameters" => [],
                             "PostParameters"=>[]],
                             $OptionsWS,
                            var_export($OptionsWS, true));*/

        $optionsRequete = $curl->initOptionsRequete("/Chemin", ["CurlOptions"   =>[CURLOPT_FOLLOWLOCATION => true,CURLOPT_FILE => $fp],
                                           "GetParameters" => ["GetParameter1"=>"GetParameter1Requete"],
                                            "PostParameters"=> ["PostParameter1"=>"PostParameter1Requete"]]);


        $this->assertEquals($fp, $optionsRequete['CurlOptions'][CURLOPT_FILE], "Options Requete : \n".var_export($optionsRequete['CurlOptions'][CURLOPT_FILE], true)."\n optionsWS : \n".var_export($OptionsWS, true));
        fclose($fp);
    }

    public function testRequeteCurl()
    {
        $options["CurlOptions"] = [
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_RETURNTRANSFER => true,
            ];

        $fp = fopen(TMP . 'tests/testWD2', 'w');
        $curl = new MethodCurl("http://ipv4.download.thinkbroadband.com", $options);

        $OptionsWS = $curl->getoptionsWebService();

        /*$this->assertEquals(["CurlOptions"=>[],
                             "GetParameters" => [],
                             "PostParameters"=>[]],
                             $OptionsWS,
                            var_export($OptionsWS, true));*/

        /*$optionsRequete = $curl->initOptionsRequete("/5MB.zip",["CurlOptions"   =>[CURLOPT_FOLLOWLOCATION => true,CURLOPT_FILE => $fp],
                                           "GetParameters" => ["GetParameter1"=>"GetParameter1Requete"],
                                            "PostParameters"=> ["PostParameter1"=>"PostParameter1Requete"]]);*/
        $curl->doRequest("/5MB.zip", ["CurlOptions"=>[CURLOPT_FOLLOWLOCATION => true,CURLOPT_FILE => $fp]]);

        //$this->assertEquals($fp, $optionsRequete['CurlOptions'][CURLOPT_FILE], "Options Requete : \n".var_dump($optionsRequete['CurlOptions'][CURLOPT_FILE])."\n optionsWS : \n".var_dump($OptionsWS));
    }

    /*    public function testRequetePostParamsRequete(){
            $curl = new MethodCurl("urlBase",[]);

            $options = $curl->initOptionsRequete("/Chemin",["PostParameters"=>["PostParameter1"=>"PostParameter1Requete"]]);
            $this->assertEquals(true,$options[CURLOPT_POST],var_export($options,true));
            $this->assertEquals("PostParameter1=PostParameter1Requete",$options[CURLOPT_POSTFIELDS],var_export($options[CURLOPT_POSTFIELDS],true));
        }

        public function testRequetePostParamsWS(){
            $curl = new MethodCurl("urlBase",["PostParameters"=>["PostParameter1"=>"PostParameter1WebService"]]);
            $options = $curl->initOptionsRequete("/Chemin",[]);
            $this->assertEquals(true,$options[CURLOPT_POST],var_export($options,true));
            $this->assertEquals("PostParameter1=PostParameter1WebService",$options[CURLOPT_POSTFIELDS],var_export($options[CURLOPT_POSTFIELDS],true));
        }

        public function testParamsEgauxWSRequete(){
            $curl = new MethodCurl("urlBase",["CurlOptions"   =>["CurlOption1"=>"CurlOption1WebService"],
                                               "GetParameters" => ["GetParameter1"=>"GetParameter1WebService"],
                                                "PostParameters"=> ["PostParameter1"=>"PostParameter1WebService"]]);

            $options = $curl->initOptionsRequete("/Chemin",["CurlOptions"   =>["CurlOption1"=>"CurlOption1Requete"],
                                               "GetParameters" => ["GetParameter1"=>"GetParameter1Requete"],
                                                "PostParameters"=> ["PostParameter1"=>"PostParameter1Requete"]]);

            $this->assertEquals(true,$options[CURLOPT_POST],var_export($options,true));
            $this->assertEquals("PostParameter1=PostParameter1Requete",$options[CURLOPT_POSTFIELDS],var_export($options[CURLOPT_POSTFIELDS],true));
        }

        public function testParamsDifferentsWSRequete(){
            $curl = new MethodCurl("urlBase",["CurlOptions"   =>["CurlOption1"=>"CurlOption1WebService"],
                                               "GetParameters" => ["GetParameter1"=>"GetParameter1WebService"],
                                                "PostParameters"=> ["PostParameter1"=>"PostParameter1WebService"]]);

            $options = $curl->initOptionsRequete("/Chemin",["CurlOptions"   =>["CurlOption2"=>"CurlOption1Requete"],
                                               "GetParameters" => ["GetParameter2"=>"GetParameter1Requete"],
                                                "PostParameters"=> ["PostParameter2"=>"PostParameter1Requete"]]);

            $this->assertEquals(true,$options[CURLOPT_POST],var_export($options,true));

            $recherche='PostParameter1=PostParameter1WebService';

            $pos = strpos($options[CURLOPT_POSTFIELDS], $recherche);
            $this->assertFalse(empty($pos), var_export($options[CURLOPT_POSTFIELDS],true));

            $recherche='PostParameter2=PostParameter1Requete';

            $pos = strpos($options[CURLOPT_POSTFIELDS], $recherche);
            $this->assertFalse(empty($pos), var_export($options[CURLOPT_POSTFIELDS],true));
        }        */
}
