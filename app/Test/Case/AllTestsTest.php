<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class AllTests extends PHPUnit_Framework_TestSuite
{

    /**
     * Test suite with all test case files.
     *
     * @return CakeTestSuite
     * @version 4.3
     */
    public static function suite()
    {
        CakeLog::disable('stdout');
        CakeLog::disable('stderr');

        $suite = new CakeTestSuite('All tests');
        $suite->addTestDirectoryRecursive(TESTS . DS . 'Case' . DS . 'Controller');
        $suite->addTestDirectoryRecursive(TESTS . DS . 'Case' . DS . 'Model');
        $suite->addTestDirectoryRecursive(TESTS . DS . 'Case' . DS . 'Lib');
        $suite->addTestDirectoryRecursive(TESTS . DS . 'Case' . DS . 'Console');

        return $suite;
    }
}
