<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('PastellComponent', 'Controller/Component');
App::uses('Deliberation', 'Model');
App::uses('Collectivite', 'Model');
App::uses('AppTools', 'Lib');

/**
 * [Connecteurlib description]
 * @version     v5.0.0
 */
class Connecteurlib
{
    /**
     * @var string Protocole de signature (pastell|iparapheur)
     */
    protected $connecteur;

    /**
     * @var bool Indique si le connecteur est activé
     */
    protected $active = true;

    /**
     * @var Composant PastellComponent
     */
    protected $Pastell;

    /**
     * @var string type pastell
     */
    protected $pastellType;

    /**
     * @var int|string $idE
     */
    protected $idE;

    /**
     * @var Model Deliberation
     */
    protected $Deliberation;

    /**
     * @var int|string collectivite
     */
    protected $Collectivite;

    /**
     * @var Model Deliberation
     */
    protected $collection;

    /**
     * @var array configuration
     */
    protected $config;

    /**
     *
     * @param type $connecteur
     */
    public function __construct($connecteur)
    {
        $this->collection = new ComponentCollection();

        if (!empty($connecteur)) {
            $this->connecteur = $connecteur;
        } else {
            $this->connecteur = null;
        }

        if ($this->connecteur === 'PASTELL' && Configure::read('USE_PASTELL')) {
            $this->Pastell = new PastellComponent($this->collection);
            //Enregistrement de la collectivité (pour pastell)
            $collectivite = ClassRegistry::init('Collectivite');
            $collectivite->id = 1;
            $this->idE = $collectivite->field('id_entity');

            $this->pastellType = Configure::read('PASTELL_TYPE');
            $this->pastellVisaType = Configure::read('PASTELL_VISA_TYPE');
            $pastellConfig = Configure::read('Pastell');

            if (isset($pastellConfig[$this->pastellType])) {
                $this->config = $pastellConfig[$this->pastellType];
            }
        }
        $this->Deliberation = ClassRegistry::init('Deliberation');
    }

    /**
     * Fonction appelée à chaque appel de fonction non connu
     * et redirige vers la bonne fonction selon le protocol
     *
     * @param string $name nom de la fonction appelée
     * @param array $arguments tableau d'arguments indexés
     * @return mixed
     * @throws Exception
     */
    public function __call($name, $arguments)
    {
        if (!empty($this->connecteur) && $this->active) {
            $suffix = ucfirst(strtolower($this->connecteur));
        } else {
            return null; //Retoun NULL pour les appels de fonction sans connecteur
        }

        if (method_exists($this, $name . $suffix)) {
            return call_user_func_array([$this, $name . $suffix], $arguments);
        } else {
            throw new Exception(
                sprintf(
                    'The required method "%s" does not exist for %s',
                    $name . $suffix,
                    get_class()
                )
            );
        }
    }

    /**
     *
     * @return type
     */
    public function getType()
    {
        return !empty($this->connecteur) ? $this->connecteur : false;
    }

    /**
     * @param array $acte
     * @param string|null $actePDF
     * @param array $annexes (content, filename)
     * @return bool|int false si echec sinon identifiant pastell
     */
    protected function createPastell($acte, $actePDF = null, $annexes = [])
    {
        try {
            $this->Deliberation->id = $acte['Deliberation']['id'];
            $idD = $this->Pastell->createDocument($this->idE, $this->pastellType);
            $this->Pastell->modifDocument($this->idE, $idD, $acte, $actePDF, $annexes);
            $this->Deliberation->save(['pastell_id' => $idD], false);
            return $idD;
        } catch (Exception $e) {
            if (!empty($idD)) {
                $this->Pastell->action($this->idE, $idD, $this->Pastell->configAction('supression'));
            }
            Cakelog::error($e->getMessage(), 'connector');

            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param array $acte
     * @param array $annexes (content, filename)
     * @return bool|int false si echec sinon identifiant pastell
     */
    protected function createVisaPastell($projetId)
    {
        try {
            return $this->Pastell->createDocument($this->idE, $this->pastellVisaType);
        } catch (Exception $e) {
            Cakelog::error($e->getMessage(), 'connector');
            return false;
        }
    }

    /**
     * @version 5.1.0
     * @param type $reponse
     *
     * @return booleans
     */
    public function saveClassification($reponse)
    {
        $sucess = true;
        //Passage d'un xml ISO-8859-1 vers utf8
        $reponse = str_replace('ISO-8859-1', 'UTF-8', $reponse);
        $xml = simplexml_load_string(utf8_encode($reponse));
        if ($xml === false && $sucess) {
            return false;
        }
        $dom_xml = dom_import_simplexml($xml);
        if ($xml === false && $sucess) {
            return false;
        }
        $dom = new DOMDocument('1.0', 'UTF-8');
        $dom_xml = $dom->importNode($dom_xml, true);
        $dom->appendChild($dom_xml);

        $folderTmp = new Folder(
            dirname(Configure::read('S2LOW_CLASSIFICATION')),
            true,
            0777
        );
        $file = new File($folderTmp->pwd() . DS . basename(Configure::read('S2LOW_CLASSIFICATION')), true);

        $file->delete();
        $file->create();
        $dom->saveXML();
        if ($file->writable()) {
            $success=true;
            $file->write($dom->saveXML());
        } else {
            $sucess = false;
        }
        $file->close();
        return $file->pwd();
    }

    public function configField($field)
    {
        return $this->config['field'][$field];
    }

    public function configAction($field)
    {
        return $this->config['action'][$field];
    }
}
