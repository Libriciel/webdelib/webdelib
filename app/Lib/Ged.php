<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppTools', 'Lib');
App::uses('ConnecteurLib', 'Lib');

/**
 * Interface contrôlant les actions de versment GED
 * Utilise les connecteurs/components PastellComponent
 *
 * @package App.Lib.Ged
 * @version 5.1.4
 * @since   4.3.0
 */
class Ged extends ConnecteurLib
{
    /**
     * Appelée lors de l'initialisation de la librairie
     * Charge le bon protocol de signature et initialise le composant correspondant
     */
    public function __construct()
    {
        //FIXME cas où vide => exception levée lors de la tache planifiée
        parent::__construct(Configure::read('GED'));

        if (Configure::read('USE_GED')) {
            if (!$this->getType()) {
                throw new Exception("Aucune GED désigné");
            }
            if (!Configure::read("USE_" . $this->getType())) {
                throw new Exception("Le connecteur GED désigné n'est pas activé : USE_" . $this->getType());
            }
            if ($this->getType() == 'CMIS') {
                //FIX
                //$this->Cmis = new CmisComponent($this->collection);
            }
        } else {
            $this->active = false;
        }
    }

    /**
     * Envoi le dossier dans pastell au SAE
     * @param $id_d
     * @param null $classification
     * @return array
     * @throws \Psr\Http\Client\ClientExceptionInterface
     * @throws Exception
     */
    public function sendPastell($acte, $document = null, $annexes = [])
    {
        if (empty($acte['Deliberation']['pastell_id'])) {
            $acte['Deliberation']['pastell_id'] = parent::createPastell($acte, $document, $annexes);
        }

        $this->Pastell->envoiGed($this->idE, $acte['Deliberation']['pastell_id']);

        return $this->Pastell->action(
            $this->idE,
            $acte['Deliberation']['pastell_id'],
            $this->Pastell->configAction('send-ged')
        );
    }
}
