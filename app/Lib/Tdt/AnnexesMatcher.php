<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [PastellComponent description]
 * @package app.Controller.Component
 * @version 5.1.4
 * @since 5.0.1
 */
class AnnexesMatcher
{
    /**
     * [private description]
     * @var [type]
     */
    private $annexesWDToMatch = [];
    /**
     * [private description]
     * @var [type]
     */
    private $annexesS2lowToMatch =[];
    /**
     * [private description]
     * @var [type]
     */
    private $egalitesStrictes = [];
    /**
     * [private description]
     * @var [type]
     */
    private $approximations = [];

    /**
     * [permutations description]
     * @param  [type] $objets              [description]
     * @param  [type] $longueurPermutation [description]
     * @return [type]                      [description]
     */
    public function permutations($objets, $longueurPermutation = null)
    {
        $nombreObjets = count($objets);

        if ($longueurPermutation == null) {
            $longueurPermutation=$nombreObjets;
        }

        if ($longueurPermutation>$nombreObjets) {
            $objets=array_pad($objets, $longueurPermutation, null);
            $nombreObjets = count($objets);
        }
        $indices = range(0, $nombreObjets-1);
        $cycles = range($nombreObjets, $nombreObjets - $longueurPermutation +1, -1);

        yield array_slice($objets, 0, $longueurPermutation);

        if ($nombreObjets<=0) {
            return;
        }

        while (true) {
            $exit_early = false;
            for ($i = $longueurPermutation; $i--; $i >=0) {
                $cycles[$i]-=1;
                if ($cycles[$i]==0) {
                    // Pousse ce qui est à l'indice i vers la fin, bouge le reste en arrière
                    if ($i < count($indices)) {
                        $removed = array_splice($indices, $i, 1);
                        array_push($indices, $removed[0]);
                    }
                    $cycles[$i] = $nombreObjets - $i;
                } else {
                    $j = $cycles[$i];
                    //permute indices $i & -$j.
                    $i_val = $indices[$i];
                    $neg_j_val = $indices[count($indices) - $j];
                    $indices[$i] = $neg_j_val;
                    $indices[count($indices)-$j] = $i_val;
                    $result = [];
                    $counter = 0;
                    foreach ($indices as $indx) {
                        array_push($result, $objets[$indx]);
                        $counter++;
                        if ($counter == $longueurPermutation) {
                            break;
                        }
                    }
                    yield $result;
                    $exit_early = true;
                    break;
                }
            }
            if (!$exit_early) {
                break;
            }
        }
    }

    public function __construct($annexesWD, $annexesS2low)
    {
        $this->annexesWDToMatch=$annexesWD;
        $this->annexesS2lowToMatch=$annexesS2low;
    }

    /**
     * [findEgalitesStrictes description]
     * @return [type] [description]
     */
    private function findEgalitesStrictes()
    {
        foreach ($this->annexesWDToMatch as $keyAnnexesWDToMatch => $annexesWDToMatch) {
            foreach ($this->annexesS2lowToMatch as $keyAnnexesS2lowToMatch => $annexesS2lowToMatch) {
                if ($annexesWDToMatch["filename"] == $annexesS2lowToMatch["posted_filename"]) {
                    $this->egalitesStrictes[] = [
                      "id_WD" => $annexesWDToMatch["id"],
                      "id_S2low"=> $annexesS2lowToMatch["id"]
                    ];
                    unset($this->annexesWDToMatch[$keyAnnexesWDToMatch]);
                    unset($this->annexesS2lowToMatch[$keyAnnexesS2lowToMatch]);
                }
            }
        }
    }

    /**
     * [findApproximations description]
     * @return [type] [description]
     */
    private function findApproximations()
    {
        $this->annexesWDToMatch=array_values($this->annexesWDToMatch);
        $this->annexesS2lowToMatch=array_values($this->annexesS2lowToMatch);

        $perms = $this->permutations($this->annexesWDToMatch, count($this->annexesS2lowToMatch));
        foreach ($perms as $p) {
            $distance =0;
            foreach ($p as $cleSortie => $arrangementWD) {
                if (!is_null($arrangementWD)) {
                    $distance += levenshtein(
                        $this->annexesS2lowToMatch[$cleSortie]["posted_filename"],
                        $arrangementWD["filename"]
                    )
                        + abs($this->annexesS2lowToMatch[$cleSortie]["size"]-$arrangementWD["size"]);
                }
            }
            if (!(isset($distanceMin))||(isset($distanceMin)&&($distance<$distanceMin))) {
                $distanceMin=$distance;
                $permChoisie=[];
                foreach ($p as $cleSortie => $arrangementWD) {
                    if (!is_null($arrangementWD)) {
                        $temp["id_WD"]=$arrangementWD["id"];
                        $temp["id_S2low"]=$this->annexesS2lowToMatch[$cleSortie]["id"];
                        $this->approximations[$cleSortie]=$temp;
                    }
                }
            }
        }
    }

    /**
     * [findConversions description]
     * @return [type] [description]
     */
    private function findConversions()
    {
        $nbAnnexeWD = count($this->annexesWDToMatch);
        $nbAnnexeS2low = count($this->annexesS2lowToMatch);
        for ($indiceWD = 0; $indiceWD < $nbAnnexeWD; ++$indiceWD) {
            for ($indiceS2low = 0; $indiceS2low < $nbAnnexeWD; ++$indiceS2low) {
                if (array_key_exists($indiceS2low, $this->annexesS2lowToMatch)
                    && array_key_exists($indiceWD, $this->annexesWDToMatch)
                ) {
                    $matches=[];
                    if (preg_match(
                        "/^(.*)(\.odt|\.doc|\.docx|\.xls|.xlsx)$/",
                        $this->annexesWDToMatch[$indiceWD]["filename"],
                        $matches
                    )
                    ) {
                        $nomFichier=$matches[1].".pdf";
                        if ($nomFichier === $this->annexesS2lowToMatch[$indiceS2low]["posted_filename"]) {
                            $this->egalitesStrictes[] = [
                                "id_WD" => $this->annexesWDToMatch[$indiceWD]["id"],
                                "id_S2low"=> $this->annexesS2lowToMatch[$indiceS2low]["id"]
                            ];
                            unset($this->annexesWDToMatch[$indiceWD]);
                            unset($this->annexesS2lowToMatch[$indiceS2low]);
                        }
                    }
                }
            }
        }
    }

    /**
     * [findClosestArrangement description]
     * @return [type] [description]
     */
    public function findClosestArrangement()
    {
        $this->findEgalitesStrictes();
        $this->findConversions();

        if (count($this->annexesWDToMatch)>10) {
            throw new Exception("Nombre d'annexes au nom altéré trop important");
        }

        if ((count($this->annexesWDToMatch) > 0) && (count($this->annexesS2lowToMatch) > 0)) {
            $this->findApproximations();
        }
        return array_merge($this->approximations, $this->egalitesStrictes);
    }
}
