<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::Uses('InterfaceActeTdt', 'Lib/Tdt');
App::Uses('FichierActeTdtPastell', 'Lib/Tdt');

class ActeTdtPastell implements InterfaceActeTdt
{
    protected $tdt;
    protected $idE;
    protected $id_d;
    protected $config;
    protected $statut = "";
    protected $aractesDispo;
    protected $acte_tamponneDispo;
    protected $acteTamponneExist;
    protected $bordereauDispo;
    protected $bordereauExist;
    protected $correspondance = [   //Correspondance entre les actions Pastell et les états WD
        'tdt-error' => 'Erreur',
        'send-tdt-erreur' => 'Erreur',
        'erreur-verif-tdt' => 'Erreur',
        'annulation-tdt' => 'Annulé',
        'annuler-tdt' => 'Annulé',
        'send-tdt' => 'Posté',
        'acquiter-tdt' => 'Acquittement reçu',
        'accepter-sae' => 'Acquittement reçu'
      ];
    protected $fichiersAnnexes = [];

    private function setIdE($idE)
    {
        if (empty($idE)) {
            throw new Exception("Erreur à l'initialisation de FichierActeTdt : idE vide");
        }
        $this->idE=$idE;
    }

    private function setIdD($id_d)
    {
        if (empty($id_d)) {
            throw new Exception("Erreur à l'initialisation de FichierActeTdt : id_d vide");
        }
        $this->id_d=$id_d;
    }

    public function __construct(PastellComponent $pastell, array $donnees)
    {
        $this->setIdE($donnees['idE']);
        $this->setIdD($donnees['id_d']);
        $this->config = $donnees['config'];
        $this->Pastell = $pastell;
        $retourTdt = $this->getDetailsDocument();
        $this->tdtId = !empty($retourTdt['data']['tedetis_transaction_id'])
            ? trim($retourTdt['data']['tedetis_transaction_id']) : null;
        $this->aractesDispo = !empty($retourTdt['data']['aractes']);
        $this->acte_tamponneDispo = !empty($retourTdt['data']['acte_tamponne']);
        $this->acteTamponneExist =  Configure::read('S2LOW_ACTE_TAMPON_EXIST');
        $this->bordereauDispo = !empty($retourTdt['data']['bordereau']);
        $this->bordereauExist = Configure::read('S2LOW_BORDEREAU_EXIST');

        $this->acteEnvoyeTdt = (
            isset($retourTdt['data']['envoi_tdt'])
            && ($retourTdt['data']['envoi_tdt']==="on" || $retourTdt['data']['envoi_tdt']==true)
        )
            ? true : false;
        $this->statutTdtDispo = in_array('verif-reponse-tdt', $retourTdt['action_possible'], true);

        $this->derniereAction = $retourTdt['last_action']['action'];
        if (!empty($retourTdt['data']['autre_document_attache'])) {
            $documentsAttaches = $retourTdt['data']['autre_document_attache'];
            $annexesTamponnees = $retourTdt['data']['annexes_tamponnees'] ?? null;
            $this->initAnnexesDetails($documentsAttaches, $annexesTamponnees);
        }
    }

    /**
     * [getTdtId description]
     * @return [type] [description]
     *
     * @version 5.1.4
     */
    public function getTdtId()
    {
        return $this->tdtId;
    }

    private function initAnnexesDetails($documentsAttaches, $annexesTamponnees)
    {
        if ($this->getAnnexesExist()) {
            $nbDocumentsAttaches = sizeof($documentsAttaches);
            for ($i=0; $i < $nbDocumentsAttaches; $i++) {
                $nomPastell = $documentsAttaches[$i];
                $nomAnnexeTamponnee = $annexesTamponnees[$i];
                $this->fichiersAnnexes[] = new FichierActeTdtPastell(
                    $this->Pastell,
                    $i,
                    $this->idE,
                    $this->id_d,
                    $nomPastell,
                    $nomAnnexeTamponnee
                );
            }
        }
    }

    private function getDetailsDocument()
    {
        $infos = $this->Pastell->detailDocument($this->idE, $this->id_d);
        return $infos;
    }

    public function getStatut()
    {
        if (empty($this->tdtId) && !$this->acteEnvoyeTdt) {
            throw new Exception('Acte non envoyé au TdT');
        }

        if (array_key_exists($this->derniereAction, $this->correspondance)) {
            return $this->correspondance[$this->derniereAction];
        }
        $retourPastell = $this->Pastell->journal($this->idE, $this->id_d);
        $retourPastellEtatsConnus = array_filter($retourPastell, function ($array) {
            return array_key_exists($array['action'], $this->correspondance);
        });
        if (empty($retourPastellEtatsConnus)) {
            throw new Exception('Aucun état connu récupéré');
        }
        // S'il y a deux dates identiques
        $datePlusRecente = max(array_column($retourPastellEtatsConnus, 'date'));
        $retourPastellFiltre = array_filter($retourPastell, function ($array) use ($datePlusRecente) {
            return $array['date']===$datePlusRecente;
        });
        return $this->correspondance[array_values($retourPastellFiltre)[0]['action']];
    }

    public function checkStatut($statut)
    {
        if ($this->statut==="") {
            $this->statut=$this->getStatut();
        }
        if ($statut===$this->statut) {
            return true;
        }
        return false;
    }

    public function getARActe()
    {
        if (!$this->aractesDispo) {
            throw new Exception("Acte Non disponible", 423);
        }
        return $this->Pastell->getFile($this->idE, $this->id_d, $this->config['aractes']);
    }

    public function getTampon()
    {
        if ($this->acteTamponneExist && !$this->acte_tamponneDispo) {
            throw new Exception("Acte Tamponne Non disponible");
        }
        return $this->acteTamponneExist ?
            $this->Pastell->getFile($this->idE, $this->id_d, $this->config['acte_tamponne'])
            : null;
    }

    public function getTamponExist()
    {
        return $this->acteTamponneExist;
    }

    public function getBordereau()
    {
        if (!$this->acte_tamponneDispo) {
            throw new Exception("Bordereau non disponible");
        }
        return $this->Pastell->getFile($this->idE, $this->id_d, $this->config['bordereau']);
    }

    public function getBordereauExist()
    {
        return $this->bordereauExist;
    }

    public function getAnnexesExist()
    {
        return $this->getTamponExist();
    }


    public function getAnnexesTampon()
    {
        echo "Non implémenté en Pastell";
        throw new Exception("Non implémenté en Pastell");
    }

    public function presenceErreurMinistere()
    {
        throw new Exception("Non implémenté en Pastell");
    }

    public function getAnnexesDetails()
    {
        $annexesDetails=[];
        foreach ($this->fichiersAnnexes as $fichierAnnexe) {
            $annexesDetails[]=$fichierAnnexe->returnAsArray();
        }
        return $annexesDetails;
    }

    public function findAnnexes(array $conditions)
    {
        if (!sizeof($conditions)==1 && array_keys($conditions) ==['id']) {
            throw new Exception(
                'Gestion des conditions complexes sur les annexes non implémentée en Pastell'
            );
        }
        $annexes = [];
        foreach ($this->fichiersAnnexes as $annexe) {
            if ($annexe->idCorrespond($conditions["id"])) {
                $annexes[]=$annexe;
            }
        }
        return $annexes;
    }

    /**
     *
     * @return string
     */
    public function getMessageErreur()
    {
        return '';
    }
}
