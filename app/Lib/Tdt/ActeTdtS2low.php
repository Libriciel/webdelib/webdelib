<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('ConnectorS2low', 'Lib');
App::uses('CakeLog', 'Log');
App::Uses('FichierActeTdt', 'Lib/Tdt');

class ActeTdtS2low
{
    protected $connecteurS2low;
    protected $tdt_id;
    protected $statut = "";
    protected $messageErreur = "";
    protected $aRActe = "";
    protected $RequeteListeFaite=false;
    protected $fichierMetierXML;
    protected $fichierActe;
    public $fichiersAnnexes = [];
    protected $fichierBordereau;
    protected $acteTamponneExist = true;
    protected $bordereauExist = true;

    protected $correspondance = [
        ConnectorS2low::RETOUR_ERREUR => 'Erreur',
        ConnectorS2low::RETOUR_ANNULE => 'Annulé',
        ConnectorS2low::RETOUR_POSTE => 'Posté',
        ConnectorS2low::RETOUR_ATTENTE  => 'En attente de transmission',
        ConnectorS2low::RETOUR_TRANSMIS  => 'Transmis',
        ConnectorS2low::RETOUR_ACQUITTEMENT => 'Acquittement reçu',
        ConnectorS2low::RETOUR_VALIDE => 'Validé',
        ConnectorS2low::RETOUR_REFUSE => 'Refusé',
        ConnectorS2low::RETOUR_DOC_RECU => 'Document reçu',
        ConnectorS2low::RETOUR_ACQUITTEMENT_ENVOYE => 'Acquittement envoyé',
        ConnectorS2low::RETOUR_DOC_ENVOYE => 'Document envoyé',
        ConnectorS2low::RETOUR_REFUS_ENVOI => 'Refus d\'envoi',
        ConnectorS2low::RETOUR_ACQUITTEMENT_DOC_RECU => 'Acquittement de document reçu',
        ConnectorS2low::RETOUR_ENVOYE_SAE => 'Envoyé au SAE',
        ConnectorS2low::RETOUR_ARCHIVE_SAE => 'Archivé par le SAE',
        ConnectorS2low::RETOUR_ERREUR_ARCHIVAGE => 'Erreur lors de l\'archivage',
        ConnectorS2low::RETOUR_RECU_SAE => 'Reçu par le SAE',
        ConnectorS2low::RETOUR_DETRUITE => 'Détruite',
        ConnectorS2low::RETOUR_ATTENTE_POSTEE => 'En attente d\'être postée',
        ConnectorS2low::RETOUR_ATTENTE_SIGNEE => 'En attente d\'être signée',
        ConnectorS2low::RETOUR_ATTENTE_TRANSMISSION => 'En attente de transmission au SAE',
        ConnectorS2low::RETOUR_ERREUR_ENVOI_SAE => 'Erreur lors de l\'envoi au SAE',
        ConnectorS2low::RETOUR_DOCUMENT_REFUSE => 'Document reçu (pas d\'AR)'
    ];

    /**
     *
     * @param type $tdt_id l'id tdt de l'acte
     * @param type $connecteurS2low
     */
    public function __construct($tdt_id, $connecteurS2low)
    {
        //CakeLog::debug(spl_object_hash($this)." Construct");
        $this->connecteurS2low=$connecteurS2low;
        $this->tdt_id=$tdt_id;
        $retourTdt = $this->connecteurS2low->getFluxRetour($tdt_id);
        $retourTdt=$this->traiteFluxRetour($retourTdt);
        $this->statut=$retourTdt['statut'];

        if (!empty($retourTdt['acquittement'])) {
            $this->aRActe=$retourTdt['acquittement'];
        }
        if (!empty($retourTdt['messageErreur'])) {
            $this->messageErreur= utf8_encode(implode("\n", $retourTdt['messageErreur']));
        }
        $this->connecteurS2low->getActeDocuments($tdt_id);
    }

    /**
     * [getTdtId description]
     * @return [type] [description]
     *
     * @version 5.1.4
     */
    public function getTdtId()
    {
        return $this->tdt_id;
    }

    /**
     *
     * @version 5.1.0
     * @param type $message
     * @return boolean
     */

    public function presenceErreurMinistere()
    {
        $messageMinistere=true;
        $pos = strpos($this->messageErreur, "par le MI");

        if (!($pos === false)) {
            $messageMinistere=false;
        }
        return $messageMinistere;
    }

    /**
       * @version 5.1.0
       * @param type $flux
       * @return array
       * @throws Exception
       */

    public function traiteFluxRetour($flux)
    {
        if (empty($flux)) {
            throw new Exception("Retour Vide");
        }
        $retour=explode("\n", $flux);
        if ($retour[0]!='OK') {
            throw new Exception("Identifiant de transaction inconnu");
        }
        $statut=$retour[1];

        if (! array_key_exists($statut, $this->correspondance)) {
            throw new Exception('L\'état '.$statut.' n\'existe pas dans le tableau de correspondance');
        }
        $tableauRetour['statut'] = $this->correspondance[$statut];
        //Acquittement reçu, récupérer le fichier d'acquittement
        if ($statut==ConnectorS2low::RETOUR_ACQUITTEMENT) {
            $tableauRetour['acquittement']=mb_substr($flux, strpos($flux, '<?xml version'), strlen($flux));
        }
        if ($statut== ConnectorS2low::RETOUR_ERREUR) {         //Transaction en erreur, récupérer le message d'erreur
            $tableauRetour['messageErreur']=array_slice($retour, 2);
        }
        return $tableauRetour;
    }

    /**
     *
     * @version 5.1.0
     * @return type
     * @throws Exception
     */
    public function getStatut()
    {
        if (empty($this->statut)) {
            $msg = 'L\'objet %s est au statut %s et ne possède pas d\'acquittement';
            throw new Exception(sprintf(__($msg), __CLASS__, $this->statut));
        }
        return $this->statut;
    }

    /**
     * @version 5.1.0
     * @param type $statut
     * @return boolean
     */
    public function checkStatut($statut)
    {
        if ($statut===$this->statut) {
            return true;
        }
        return false;
    }


    /**
     *
     * @return type
     * @version 5.1.0
     */
    public function getMessageErreur()
    {
        return $this->messageErreur;
    }

    /**
     *
     * @version 5.1.0
     * @return type
     * @throws Exception
     */
    public function getARActe()
    {
        if (!$this->statut==="Acquittement reçu") {
            throw new Exception('L\'objet ActeTdt est au statut '.$this->statut.' et ne possède pas d\'acquittement');
        }
        if (empty($this->aRActe)) {
            throw new Exception('Acquittement vide');
        }
        return $this->aRActe;
    }

    /**
     *
     * @throws Exception
     * @version 5.1.0
     */
    private function getListeDocuments()
    {
        if (!$this->RequeteListeFaite) {
            $listeDocuments=json_decode($this->connecteurS2low->getActeDocuments($this->tdt_id), true);
            //CakeLog::debug("count : ".strval(count($listeDocuments)));
            //CakeLog::debug(var_export($listeDocuments,true));
            if (count($listeDocuments)<2) {
                throw new Exception('Retour S2low incorrects : '.var_export($listeDocuments, false));
            }
            $this->fichierMetierXML= new FichierActeTdt($this->connecteurS2low, $listeDocuments[0]);
            $this->fichierActe=new FichierActeTdt($this->connecteurS2low, $listeDocuments[1]);
            for ($i=2; $i<count($listeDocuments); $i++) {
                //CakeLog::debug("Traitement annexe ".strval($i));
                $fichierActe= new FichierActeTdt($this->connecteurS2low, $listeDocuments[$i]);
                //CakeLog::debug(strval((array)$fichierActe));
                array_push($this->fichiersAnnexes, $fichierActe);
            }
            //CakeLog::debug(spl_object_hash($this)." 1er debug : ".var_export(count($this->fichiersAnnexes),true));
            $this->RequeteListeFaite=true;
        }
    }

    /**
     *
     * @return type
     * @version 5.1.0
     */

    public function getTampon()
    {
        $this->getListeDocuments();
        $fichierTamponne=$this->fichierActe->getFichierTamponne();
        return $fichierTamponne;
    }

    public function getTamponExist()
    {
        return $this->acteTamponneExist;
    }

    /**
     *
     * @return type
     * @version 5.1.0
     */
    public function getBordereau()
    {
        if (empty($this->fichierBordereau)) {
            $this->fichierBordereau=$this->connecteurS2low->getActeBordereau($this->tdt_id);
        }
        return $this->fichierBordereau;
    }

    public function getBordereauExist()
    {
        return $this->bordereauExist;
    }

    /**
     *
     * @return type
     * @version 5.1.0
     */
    public function getAnnexesDetails()
    {
        $this->getListeDocuments();
        //CakeLog::debug(spl_object_hash($this)." 2eme debug : ".var_export(count($this->fichiersAnnexes),true));
        foreach ($this->fichiersAnnexes as $annexe) {
            $annexeDetails=['id'=>$annexe->getid(),
                'name'=>$annexe->getname(),
                'posted_filename'=>$annexe->getPostedFilename(),
                'mimetype'=>$annexe->getmimetype(),
                'size'=>$annexe->getsize()];
            $annexesDetails[]=$annexeDetails;
        }
        return $annexesDetails;
    }

    /**
     *
     * @param array $conditions
     * @return type
     * @version 5.1.0
     */
    public function findAnnexes(array $conditions)
    {
        $annexes = [];
        $this->getListeDocuments();
        foreach ($this->fichiersAnnexes as $annexe) {
            if ($annexe->correspondAttributs($conditions)) {
                $annexes[]=$annexe;//->getfichiertamponne();
            } else {
            }
        }
        return $annexes;
    }
}
