<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

interface InterfaceActeTdt
{
    public function getArActe();
    public function getTampon();
    public function getTamponExist();
    public function getBordereau();
    public function getBordereauExist();
    public function getAnnexesTampon();
    public function getAnnexesDetails();
    public function presenceErreurMinistere();    // Non utilisé, remplacé par getStatut
    public function getStatut();                  // Non
    public function getMessageErreur();
}
