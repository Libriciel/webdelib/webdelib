<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class FichierActeTdtPastell
{
    protected $id;
    protected $idE;
    protected $id_d;
    protected $posted_filename;
    protected $nomAnnexeTamponnee;
    protected $fichier;
    protected $size;

    public function __construct(PastellComponent $pastell, $id, $idE, $id_d, $nomPastell, $nomAnnexeTamponnee)
    {
        $this->Pastell = $pastell;
        $this->id=$id;
        $this->idE = $idE;
        $this->id_d = $id_d;
        $this->fichier = $this->Pastell->getFile($this->idE, $this->id_d, 'annexes_tamponnees', $this->id);
        $this->size = strlen($this->fichier);
        $this->posted_filename=$nomPastell;
        $this->nomAnnexeTamponnee=$nomAnnexeTamponnee;
    }

    public function idCorrespond($id)
    {
        if ($this->id===$id) {
            return true;
        }
        return false;
    }

    public function returnAsArray()
    {
        return ['id'=>$this->id,
            'posted_filename'=>$this->posted_filename,
            'size'=>$this->size
        ];
    }

    public function getfichiertamponne()
    {
        return $this->fichier;
    }

    public function getfichiertamponneExist()
    {
        return Configure::read('S2LOW_ACTE_TAMPON_EXIST');
    }
}
