<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * Classe contenant les fichiers du tdt s2low
 * @version 5.1.0
 */
class FichierActeTdt
{
    protected $id='';
    protected $name='';
    protected $posted_filename='';
    protected $mimetype='';
    protected $size='';
    protected $sign='';
    protected $fichiertamponne;
    protected $connecteurS2low;

    /**
     * Constructeur
     * @version 5.1.0
     *
     * @param type $connecteurS2low
     * @param array $donnees
     */

    public function __construct($connecteurS2low, array $donnees)
    {
        $this->connecteurS2low=$connecteurS2low;
        $this->id=$donnees['id'];
        $this->name=$donnees['name'];
        $this->posted_filename=$donnees['posted_filename'];
        $this->mimetype=$donnees['mimetype'];
        $this->size=$donnees['size'];
        $this->sign=$donnees['sign'];
    }

    /**
     *
     * @version 5.1.0
     * @return type
     */
    public function getid()
    {
        return $this->id;
    }
    /**
     *
     * @version 5.1.0
     * @return type
     */
    public function getsign()
    {
        return $this->sign;
    }

    /**
     *
     * @version 5.1.0
     * @return type
     */
    public function getname()
    {
        return $this->name;
    }

    /**
     *
     * @version 5.1.0
     * @return type
     */
    public function getPostedFilename()
    {
        return $this->posted_filename;
    }

    /**
     *
     * @version 5.1.0
     * @return type
     */

    public function getmimetype()
    {
        return $this->mimetype;
    }

    /**
     *
     * @version 5.1.0
     * @return type
     */
    public function getsize()
    {
        return $this->size;
    }

    /*
     * Réception des fichiers tamponnées (avec Lazy Loading)
     * @version 5.1.0
     */
    public function getfichiertamponne()
    {
        if (empty($this->fichiertamponne)) {
            $this->fichiertamponne=$this->connecteurS2low->getActeDocumentTampon($this->id, true);
        }
        return $this->fichiertamponne;
    }

    public function getfichiertamponneExist()
    {
        return Configure::read('S2LOW_ACTE_TAMPON_EXIST');
    }

    /**
     * Vérifie si le fichier a les attributs contenus dans le tableau
     * @param array $donnees
     * @return boolean
     * @throws Exception
     */
    public function correspondAttributs(array $donnees)
    {
        $equality=true;
        foreach ($donnees as $key => $value) {
            $getter='get'.$key;
            if (method_exists($this, $getter)) {
                $valueObject=$this->$getter();
                if (!($valueObject===$value)) {
                    $equality=false;
                }
            } else {
                throw new Exception("Attribut ".$key." inconnu");
            }
        }
        return $equality;
    }
}
