<?php

trait ValidationTrait
{
    /**
     * @param type $id
     * @since 4.3
     * @access public
     * @version 5.0.1
     */
    private function refuseDossier($id, $comment = true)
    {
        $this->Deliberation->Behaviors->load(
            'Version',
            $this->Deliberation->actsAsVersionOptionsList['Version']
        );

        if ($comment === true) {
            $this->Commentaire->enregistre(
                $id,
                $this->Auth->user('id'),
                $this->request->data['Commentaire']['texte']
            );
        }

        $this->Historique->enregistre($id, $this->Auth->user('id'), 'Projet refusé');

        $nouvelId = $this->Deliberation->refusDossier($id);
        $this->Traitement->execute('KO', $this->Auth->user('id'), $id);

        $destinataires = $this->Traitement->whoIs($id, 'in', ['OK', 'IN']);
        foreach ($destinataires as $destinataire_id) {
            if ($destinataire_id > 0) {
                $this->User->notifier($nouvelId, $destinataire_id, 'refus');
            }
        }
    }

    /**
     * @param type $id
     * @since 4.3
     * @access private
     * @version 5.0.1
     */
    private function accepteDossier($id)
    {
        $this->Deliberation->Behaviors->load(
            'Version',
            $this->Deliberation->actsAsVersionOptionsList['Version']
        );

        if (!empty($this->request->data['Commentaire']['texte'])) {
            $this->Commentaire->enregistre($id, $this->Auth->user('id'), $this->request->data['Commentaire']['texte']);
        }
        $traitementTermine = $this->Traitement->execute('OK', $this->Auth->user('id'), $id);

        $this->Historique->enregistre(
            $id,
            $this->Auth->user('id'),
            __('Projet accepté %s', ($traitementTermine ? ' et validé' : ''))
        );

        if ($traitementTermine) {
            $this->Deliberation->id = $id;
            if ($this->Deliberation->saveField('etat', 2)) {
                $this->notifyValidate($id);
            }
        } else {
            $destinataires = $this->Traitement->whoIs($id, 'current', 'RI');
            foreach ($destinataires as $destinataire_id) {
                if ($destinataire_id > 0) {
                    $this->User->notifier($id, $destinataire_id, 'traitement');
                }
            }
        }
    }

    /**
     * Permet de valider un projet en cours de validation en court-circuitant le circuit de validation
     * Appelée depuis la vue deliberations/tous_les_projets
     *
     * @param int $id
     * @param boolean $redirect
     * @return CakeResponse
     */
    private function validerEnUrgenceLeTraitement($id, $redirect = true)
    {
        $this->Deliberation->Behaviors->load(
            'Version',
            $this->Deliberation->actsAsVersionOptionsList['Version']
        );

        $this->Deliberation->recursive = -1;
        $deliberation = $this->Deliberation->find('first', [
            'fields' => ['id', 'etat', 'parapheur_etat'],
            'conditions' => ['Deliberation.id' => $id],
            'recursive' => -1]);

        if (empty($deliberation)) {
            $this->Flash->set(
                __('ID invalide pour le projet de délibération.'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            return $redirect ? $this->redirect($this->previous) : null;
        }

        if (empty($this->data)) {
            $this->set('delib_id', $id);
            $this->render();
            return null;
        }

        if ($deliberation['Deliberation']['etat'] !== 1) {
            $this->Flash->set(
                __('Le projet doit être dans un circuit pour être validé.'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
        } elseif ($deliberation['Deliberation']['parapheur_etat'] === 1) {
            $this->Flash->set(
                __('Le projet est dans une étape parapheur, il ne peut être validé en urgence.'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
        } elseif (isset($this->request->data['Traitement']['Commentaire'])
            && empty($this->request->data['Traitement']['Commentaire'])
            && Configure::read('Cakeflow.commentaire.obligatoire')
        ) {
            $this->Flash->set(__('Le commentaire est vide.'), [
                'element' => 'growl',
                'params' => ['type' => 'danger']
            ]);
        } else {
            if (!empty($this->request->data['Traitement']['Commentaire'])) {
                $this->Commentaire->enregistre(
                    $id,
                    $this->Auth->user('id'),
                    $this->request->data['Traitement']['Commentaire']
                );
            }

            // initialisation du visa si utilisateur connecté est hors traitement
            $options = [
                'insertion' => [
                    '0' => [
                        'Etape' => [
                            'etape_id' => null,
                            'etape_nom' => __('Validation en urgence'),
                            'etape_type' => 1
                        ],
                        'Visa' => [
                            '0' => [
                                'trigger_id' => $this->Auth->user('id'),
                                'type_validation' => 'V'
                            ]
                        ]
                    ]
                ]
            ];
            $this->Traitement->execute('ST', $this->Auth->user('id'), $id, $options);
            $this->Deliberation->id = $id;
            $this->Deliberation->saveField('etat', 2);
            $this->Deliberation->saveField('parapheur_etat', 0);
            $this->notifyValidate($id);
            $this->Historique->enregistre($id, $this->Auth->user('id'), __('Validation en urgence'));
            $this->Flash->set(
                __(
                    'Le projet %s a été validé en urgence.',
                    $deliberation['Deliberation']['id']
                ),
                ['element' => 'growl']
            );
        }

        return $redirect ? $this->redirect($this->previous) : null;
    }

    /**
     * [notifyValid description]
     * @return [type] [description]
     *
     * @version 5.1
     */
    private function notifyValidate($projet_id)
    {
        $projet = $this->Deliberation->find('first', [
            'fields' => ['Deliberation.id', 'Deliberation.redacteur_id', 'Deliberation.rapporteur_id'],
            'conditions' => ['Deliberation.id' => $projet_id],
            'contain' => 'DeliberationUser.user_id',
            'recursive' => -1
        ]);
        // Array redactors
        $redacteurs[] = $projet['Deliberation']['redacteur_id'];
        if (!empty($projet['DeliberationUser'])) {
            $redacteurs = array_merge($redacteurs, Set::extract('/user_id', $projet['DeliberationUser']));
        }
        // Notify validator and redactor
        $destinataires = $this->Traitement->whoIs($projet_id, 'in', ['OK', 'IN']);
        foreach ($destinataires as $destinataire_id) {
            if ($destinataire_id > 0) {
                // Sending an editors notification if the project is validated
                if (in_array($destinataire_id, $redacteurs, true)) {
                    $this->User->notifier($projet_id, $destinataire_id, 'projet_valide');
                } else {
                    // Sending a notification to validators if the project is validated
                    $this->User->notifier($projet_id, $destinataire_id, 'projet_valide_valideur');
                }
            }
        }
        // Sending a report to the reporter if the project is validated
        $this->Acteur->notifier($projet_id, $projet['Deliberation']['rapporteur_id'], 'projet_valide');
    }
}
