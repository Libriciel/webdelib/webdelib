<?php

App::uses('ConsoleOptionParser', 'Console');
App::uses('ConsoleInputOption', 'Console');

class TenantConsoleOptionParser extends ConsoleOptionParser
{

    public function __construct($command = null, $defaultOptions = true)
    {
        parent::__construct($command, $defaultOptions);

        if ($defaultOptions) {
            $this->addTenantOption();
        }
    }

    // Ajoutez votre propre méthode personnalisée
    public function addTenantOption()
    {
        return $this->addOption($this->getConsoleInputOptionTenant());
    }

    // Vous pouvez également surcharger une méthode existante si nécessaire
    public function addSubcommand($name, $options = [])
    {
//        if (is_object($name) && $name instanceof ConsoleInputSubcommand) {
//        } else {
//            $command = new ConsoleInputSubcommand($options);
//        }
        if (isset($options['parser']['options'])) {
            $options['parser']['options'] = array_merge($options['parser']['options'], [
                'parser' => [
                    'options' => $this->getConsoleInputOptionTenantToArray()
                ]
            ]);
        } elseif (isset($options['parser'])) {
            $options['parser'] += [
                'options' => $this->getConsoleInputOptionTenantToArray()
            ];
        }

        parent::addSubcommand($name, $options);

        // Ajoutez des options ou des logiques supplémentaires ici
        return $this;
    }

    public function getConsoleInputOptionTenantToArray()
    {
        return [
            $this->getConsoleInputOptionTenant()->name() => [
                'short' => $this->getConsoleInputOptionTenant()->short(),
                'help' => __('Nom du tenant pour l\'exécution de la commande.'),
            ]
        ];
    }
    public function getConsoleInputOptionTenant(): ConsoleInputOption
    {
        return new ConsoleInputOption(
            'tenant',
            't',
            __('Nom du tenant pour l\'exécution de la commande.'),
        );
    }

    public function setOptionParser($OptionParser)
    {
        return call_user_func_array([$OptionParser, 'addOption'], $this->getOptionParserTenant());
    }

    public function getOptionParserSubcommandTenant(): array
    {
        return [
            'tenant' =>
                [
                    'string' => true,
                    'help' => __('Nom du tenant'),
                ]
        ];
    }
}
