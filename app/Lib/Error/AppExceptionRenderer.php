<?php

// FIXME: dans le webdelib.inc
Configure::write('User.roles.admin', 2);
App::uses('ExceptionRenderer', 'Error');


class AppExceptionRenderer extends ExceptionRenderer
{
    protected function setDisplayErrorDetails($error)
    {
        $user = (array)$this->controller->Auth->user();
        $displayErrorDetails = (
            (Configure::read('debug') > 0)
            || ((int)Configure::read('User.roles.admin') === (int)Hash::get($user, 'role'))
        );
        $exeptionClass = get_class($error);
        $exceptionMessage = $error->getMessage();
        $this->controller->set(compact('displayErrorDetails', 'exeptionClass', 'exceptionMessage'));
    }

    public function error400($error)
    {
        $this->setDisplayErrorDetails($error);
        parent::error400($error);
        // ...
    }

    public function error500($error)
    {
        $this->setDisplayErrorDetails($error);
        parent::error500($error);
    }
}
