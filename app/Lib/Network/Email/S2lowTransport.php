<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AbstractTransport', 'Network/Email');
App::uses('MethodCurl', 'Lib');

class S2lowTransport extends AbstractTransport
{
    /**
     * [protected description]
     * @var [type]
     */
    protected $HttpRequest;

    /**
     * [protected description]
     * @var [type]
     */
    protected $mailId;
    /**
     * The response of the last sent SMTP command.
     *
     * @var array
     */
    protected $lastResponse = [];

    /**
     * Returns the response of the last sent SMTP command.
     *
     * A response consists of one or more lines containing a response
     * code and an optional response message text:
     * ```
     * array(
     *     array(
     *         'code' => '250',
     *         'message' => 'mail.example.com'
     *     ),
     *     array(
     *         'code' => '250',
     *         'message' => 'PIPELINING'
     *     ),
     *     array(
     *         'code' => '250',
     *         'message' => '8BITMIME'
     *     ),
     *     // etc...
     * )
     * ```
     *
     * @return array
     */
    public function getLastResponse()
    {
        return $this->lastResponse;
    }

    /**
     * Set the configuration
     *
     * @param array $config Configuration options.
     * @return array Returns configs
     */
    public function config($config = null)
    {
        if ($config === null) {
            return $this->_config;
        }
        $default = [
            'layout' => null,
            'host' => 'localhost',
            'proxy_host' => '',
            'username' => null,
            'password' => null,
            'client' => 'curl',
            'emailFormat' => 'text',
            'mail_password' => null,
            'tls' => true,
            'ssl_cert' => null,
            'ssl_certpassword' =>  null,
            'ssl_key' => null

        ];
        $this->_config = array_merge($default, $this->_config, $config);
        return $this->_config;
    }

    /**
     * Send mail
     *
     * @param CakeEmail $email CakeEmail
     * @return array
     */
    public function send(CakeEmail $email)
    {
        $this->connect();
        $this->sendDocument($email);
        sleep(2);

        return $this->mailId;
    }

    /**
     * [connect description]
     * @return [type] [description]
     */
    private function connect()
    {
        $options["CurlOptions"] = [
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSLCERT => $this->_config['ssl_cert'],
            CURLOPT_SSLCERTPASSWD => $this->_config['ssl_certpassword'],
            CURLOPT_SSLKEY => $this->_config['ssl_key']
        ];

        if (!empty($this->_config['proxy_host'])) {
            $options["CurlOptions"][CURLOPT_PROXY] = $this->_config['proxy_host'];
        }

        if (!empty($this->_config['username']) && !empty($this->_config['password'])) {
            $options["CurlOptions"][CURLOPT_USERPWD] = $this->_config['username'] . ':' . $this->_config['password'];
        }

        $this->HttpRequest = new MethodCurl($this->_config['host'] . '/modules/mail/api/', $options);
    }

    /**
     * [parseReponse description]
     * @param  [type] $response [description]
     * @return [type]           [description]
     */
    private function parseReponse($response)
    {
        if (strpos($response, 'OK:') !== false) {
            $this->mailId = trim(substr($response, 3, strlen($response)));
        }
        if (!empty($this->mailId)) {
            return $this;
        }

        throw new \Exception("Error Processing Parse Request", 1);
    }

    /**
     * [sendDocument description]
     * @return [type] [description]
     */
    private function sendDocument(CakeEmail $email)
    {
        $subject = iconv_mime_decode($email->subject(), 0, "UTF8");
        $data = [
          'mailto' => key($email->to()),
          'objet' => $subject,
          'message' => $email->message('text'),
        ];
        if (!empty($this->_config['mail_password'])) {
            $data['password'] = $this->_config['mail_password'];
        }
        // Compatibilité Pastell iso
        $data = array_map('utf8_decode', $data);

        $data = $this->attachmentFile($email, $data);

        $response = $this->HttpRequest->doRequest(
            'send-mail.php',
            [
              "CurlOptions" =>
              [
                CURLOPT_VERBOSE => true,
                CURLOPT_RETURNTRANSFER => true
              ],
              "PostParameters" => $data
            ]
        );

        $this->lastResponse = array_merge($this->lastResponse, [$response]);

        if (empty($this->parseReponse($response)->mailId)) {
            throw new \Exception("Error Processing sendMail Request", 1);
        }
    }

    /**
     * [attachmentFile description]
     * @param  CakeEmail $email [description]
     * @return [type]           [description]
     */
    private function attachmentFile(CakeEmail $email, $data)
    {
        if (!empty($email->attachments())) {
            $attachments = null;
            foreach ($email->attachments() as $filename => $file) {
                $attachments = curl_file_create($file['file'], $file['mimetype'], $filename);
                break;
            };
            $data['uploadFile1'] = $attachments;
        }

        return $data;
    }
}
