<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AbstractTransport', 'Network/Email');
App::uses('MethodCurl', 'Lib');

class PastellTransport extends AbstractTransport
{
    /**
     * [protected description]
     * @var [type]
     */
    protected $HttpRequest;

    /**
     * [protected description]
     * @var [type]
     */
    protected $documentId;
    /**
     * The response of the last sent SMTP command.
     *
     * @var array
     */
    protected $lastResponse = [];

    /**
     * Returns the response of the last sent SMTP command.
     *
     * A response consists of one or more lines containing a response
     * code and an optional response message text:
     * ```
     * array(
     *     array(
     *         'code' => '250',
     *         'message' => 'mail.example.com'
     *     ),
     *     array(
     *         'code' => '250',
     *         'message' => 'PIPELINING'
     *     ),
     *     array(
     *         'code' => '250',
     *         'message' => '8BITMIME'
     *     ),
     *     // etc...
     * )
     * ```
     *
     * @return array
     */
    public function getLastResponse()
    {
        return $this->lastResponse;
    }

    /**
     * Set the configuration
     *
     * @param array $config Configuration options.
     * @return array Returns configs
     */
    public function config($config = null)
    {
        if ($config === null) {
            return $this->_config;
        }
        $default = [
            'layout' => null,
            'host' => 'localhost',
            'port' => 443,
            'timeout' => 30,
            'username' => null,
            'password' => null,
            'client' => 'curl',
            'emailFormat' => 'text',
            'tls' => false,
            'ssl_allow_self_signed' => false
        ];
        $this->_config = array_merge($default, $this->_config, $config);
        return $this->_config;
    }

    /**
     * Send mail
     *
     * @param CakeEmail $email CakeEmail
     * @return array
     */
    public function send(CakeEmail $email)
    {
        $this->connect();
        $this->createDocument();
        $this->updateDocument($email);
        $this->attachmentFile($email);
        $this->sendDocument();

        return $this->documentId;
    }

    /**
     * [connect description]
     * @return [type] [description]
     */
    private function connect()
    {
        $options["CurlOptions"] = [
          CURLOPT_USERPWD => $this->_config['username'] . ':' . $this->_config['password'],
          CURLOPT_SSL_VERIFYHOST => $this->_config['tls'],
          CURLOPT_SSL_VERIFYPEER => $this->_config['ssl_allow_self_signed'],
        ];
        $this->HttpRequest = new MethodCurl($this->_config['host'] . '/api/', $options);
    }

    /**
     * [parseResponse description]
     * @param  [type] $response [description]
     * @return object [type]           [description]
     * @throws Exception
     */
    private function parseResponse($response)
    {
        // /CakeLog::debug(var_export($response, true));
        $response = json_decode($response, true);
        if (!empty($response)) {
            return (object)$response;
        }

        throw new \Exception("Error Processing Parse Request", 1);
    }

    /**
     * [createDocument description]
     * @return [type] [description]
     */
    private function createDocument()
    {
        $response = $this->HttpRequest->doRequest(
            'create-document.php?id_e=' . $this->_config['pastell_ide'] . '&type=' . $this->_config['pastell_flux'],
            ["CurlOptions"=>[
              CURLOPT_RETURNTRANSFER => true
            ]]
        );

        $response = $this->parseResponse($response);

        if (empty($response->id_d)) {
            throw new \Exception("Error Processing createDocument Request", 1);
        }

        $this->documentId = $response->id_d;

        $this->lastResponse = array_merge($this->lastResponse, [$response]);
    }

    /**
     * [updateDocument description]
     * @param  CakeEmail $email [description]
     * @return [type]           [description]
     */
    private function updateDocument(CakeEmail $email)
    {
        $subject = iconv_mime_decode($email->subject(), 0, "UTF8");
        $data = [
            'titre'=> $subject,
            'objet' => $subject,
            'message' => $email->message('text'),
        ];

        $mail_from = Configure::read('MAIL_FROM');
        if (!filter_var($mail_from, FILTER_VALIDATE_EMAIL)) {
            $syntaxe = '#(.*)\s<([\w.-]+@[\w.-]+\.[a-zA-Z]{2,6})>#';
            $froms = [];
            if (preg_match($syntaxe, $mail_from, $froms)) {
                foreach ($froms as $from) {
                    if (filter_var($from, FILTER_VALIDATE_EMAIL)) {
                        if ($from == $froms[2]) {
                            $mail_from = $from;
                        }
                        break;
                    }
                }
            }
        }

        if (count($email->to())>1) {
            $data['to'] = $mail_from;
            $data['bcc'] = implode(array_keys($email->to()), ',');
        } else {
            $data['to'] = key($email->to());
        }

        // Compatibilité Pastell iso
        $data = array_map('utf8_decode', $data);

        $response = $this->HttpRequest->doRequest(
            'modif-document.php?id_e=' .$this->_config['pastell_ide']. '&id_d=' . $this->documentId,
            [
                "CurlOptions"=>[
                    CURLOPT_VERBOSE => true,
                    CURLOPT_RETURNTRANSFER => true
                ],
                "PostParameters"=> $data]
        );

        $this->lastResponse = array_merge($this->lastResponse, [$response]);

        if ($this->parseResponse($response)->result !=='ok') {
            throw new \Exception("Error Processing updateDocument Request", 1);
        }
    }

    /**
     * [attachmentFile description]
     * @param  CakeEmail $email [description]
     * @return [type]           [description]
     */
    private function attachmentFile(CakeEmail $email)
    {
        if (empty($email->attachments())) {
            return;
        }

        $attachments = null;
        foreach ($email->attachments() as $filename => $file) {
            $attachments = curl_file_create($file['file'], $file['mimetype'], $filename);
            break;
        };
        $data = [
          'document_attache' => $attachments
        ];

        $response = $this->HttpRequest->doRequest(
            'modif-document.php?id_e=' .$this->_config['pastell_ide']. '&id_d=' . $this->documentId,
            [
              "CurlOptions"=>
              [
                CURLOPT_VERBOSE=>true,
                CURLOPT_RETURNTRANSFER=>true
              ],
              "PostParameters"=> $data
            ],
            true
        );

        $this->lastResponse = array_merge($this->lastResponse, [$response]);

        if ($this->parseResponse($response)->result !=='ok') {
            throw new \Exception("Error Processing updateDocument Request", 1);
        }
    }

    /**
     * [sendDocument description]
     * @return [type] [description]
     */
    private function sendDocument()
    {
        $response = $this->HttpRequest->doRequest(
            'action.php?id_e=' . $this->_config['pastell_ide'] . '&id_d=' . $this->documentId . '&action=envoi',
            ["CurlOptions"=>[
              CURLOPT_RETURNTRANSFER => true
            ]]
        );

        if ($this->parseResponse($response)->result != 1) {
            throw new \Exception("Error Processing sendDocument(envoi) Request", 1);
        }
    }
}
