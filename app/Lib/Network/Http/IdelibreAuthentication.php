<?php

class IdelibreAuthentication
{

    /**
    * Authentication
    *
    * @param HttpSocket $http
    * @param array $authInfo
    * @return void
    */
    public static function authentication(HttpSocket $http, &$authInfo)
    {
        $http->request['header'][$authInfo['user']] = $authInfo['pass'];
    }
}
