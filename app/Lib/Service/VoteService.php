<?php

use Cake\ORM\TableRegistry;

class VoteService
{

    public function __construct()
    {
        $this->Votes = ClassRegistry::init('Votes');
        $this->Listepresence = ClassRegistry::init('Listepresence');
        $this->ListePresenceGlobale = ClassRegistry::init('ListePresenceGlobale');
        $this->Seance = ClassRegistry::init('Seance');
        $this->Deliberation = ClassRegistry::init('Deliberation');
        $this->Acteur = ClassRegistry::init('Acteur');
        $this->DeliberationSeance = ClassRegistry::init('DeliberationSeance');
    }

    /**
     * @access public
     * @param interger $delib_id
     * @param interger $seance_id
     * @return array
     */
    public function afficherListePresents($delib_id, $seance_id)
    {
        $presents = $this->Listepresence->find('all', [
            'contain' => ['Acteur', 'Acteur.Typeacteur'],
            'conditions' => ['Listepresence.delib_id' => $delib_id],
            'order' => ["Acteur.position ASC"],
            'recursive' => -1
        ]);
        if (empty($presents) && $this->isFirstDelib($delib_id, $seance_id)) {
            $presents = $this->buildFirstList($delib_id, $seance_id);
        }

        // Si la liste est vide, on recupere la liste des present lors de la derbiere deliberation.
        // Verifier que la liste precedente n'est pas vide...
        if (empty($presents)) {
            $presents = $this->copyFromPreviousList($delib_id, $seance_id);
        }

        if (!empty($presents)) {
            foreach ($presents as &$acteur) {
                if (!empty($acteur['Listepresence']['mandataire'])) {
                    $mandataire = $this->Seance->Typeseance->Acteur->read(
                        'nom, prenom',
                        $acteur['Listepresence']['mandataire']
                    );
                    //['data']
                    $acteur['Listepresence']['mandataire'] =
                        $mandataire['Acteur']['prenom'] . " " . $mandataire['Acteur']['nom'];
                } elseif (!empty($acteur['Listepresence']['suppleant_id'])) {
                    $suppleant = $this->Seance->Typeseance->Acteur->read(
                        'nom, prenom',
                        $acteur['Listepresence']['suppleant_id']
                    );
                    $acteur['Listepresence']['suppleant'] =
                        $suppleant['Acteur']['prenom'] . " " . $suppleant['Acteur']['nom'];
                }
            }
        }

        return $presents;
    }

    /**
     * @param type $delib_id
     * @param type $seance_id
     * @return type
     * @version 4.3
     * @access public
     */
    public function isFirstDelib($delib_id, $seance_id)
    {
        $position = $this->Deliberation->getPosition($delib_id, $seance_id);
        return ($position == 1);
    }

    /**
     * @param type $delib_id
     * @param type $seance_id
     * @return type
     * @version 4.3
     * @access protected
     */
    public function copyFromPreviousList($delib_id, $seance_id)
    {
        $position = $this->Deliberation->getPosition($delib_id, $seance_id);
        if ($position == 1) {
            return null;
        }
        $previousDelibId = $this->getDelibIdByPosition($seance_id, $position);
        $previousPresents = $this->Listepresence->find(
            'all',
            ['conditions' => ['Listepresence.delib_id' => $previousDelibId],
                'recursive' => -1]
        );

        foreach ($previousPresents as $present) {
            $this->Listepresence->create();
            $params['data']['Listepresence']['acteur_id'] = $present['Listepresence']['acteur_id'];
            $params['data']['Listepresence']['mandataire'] = $present['Listepresence']['mandataire'];
            $params['data']['Listepresence']['suppleant_id'] = $present['Listepresence']['suppleant_id'];
            $params['data']['Listepresence']['present'] = $present['Listepresence']['present'];
            $params['data']['Listepresence']['delib_id'] = $delib_id;
            $this->Listepresence->save($params['data']);
        }
        $liste = $this->Listepresence->find('all', ['conditions' => ['Listepresence.delib_id' => $delib_id],
            'order' => ["Acteur.position ASC"],
            'contain' => ['Acteur', 'Acteur.Typeacteur']]);
        if (!empty($liste)) {
            return $liste;
        }

        return $this->buildFirstList($delib_id, $seance_id);
    }

    /**
     * @access protected
     * @param type $delib_id
     * @param type $seance_id
     * @return type
     */
    public function buildFirstList($delib_id, $seance_id)
    {
        $seance = $this->Seance->find('first', ['conditions' => ['Seance.id' => $seance_id],
            'recursive' => -1,
            'fields' => ['Seance.type_id']]);
        $elus = $this->Seance->Typeseance->acteursConvoquesParTypeSeanceId($seance['Seance']['type_id'], true);
        foreach ($elus as $elu) {
            $this->Listepresence->create();
            $params['data']['Listepresence']['acteur_id'] = $elu['Acteur']['id'];
            $params['data']['Listepresence']['present'] = 1;
            $params['data']['Listepresence']['delib_id'] = $delib_id;
            $this->Listepresence->save($params['data']);
        }

        return $this->Listepresence->find('all', ['conditions' => ['Listepresence.delib_id' => $delib_id],
            'order' => ["Acteur.position ASC"],
            'contain' => ['Acteur', 'Acteur.Typeacteur']]);
    }

    /**
     * Récupération de la liste des présents
     *
     * @access private
     *
     * @param int $delib_id
     * @return array
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function getAttendanceList($id, $global = false)
    {
        $modelAttendance = 'Listepresence';
        $columnIdentity = 'delib_id';
        if ($global === true) {
            $modelAttendance = 'ListePresenceGlobale';
            $columnIdentity = 'seance_id';
        }
        $acteurs = $this->{$modelAttendance}->find('all', [
            'contain' => [
                'Acteur' => [
                    'fields' => ['id', 'nom', 'prenom','actif', 'suppleant_id']
                ]
            ],
            'conditions' => [$columnIdentity => $id],
            'order' => ['position' => 'ASC'],
            'recursive' => -1,
        ]);

        foreach ($acteurs as &$acteur) {
            if ($acteur['Acteur']['actif'] === false) {
                continue;
            }

            if (!empty($acteur['Acteur']['suppleant_id'])) {
                $suppleant = $this->Acteur->find('first', [
                    'conditions' => ['Acteur.id' => $acteur['Acteur']['suppleant_id'],
                        'Acteur.actif' => true],
                    'order' => ['nom' => 'ASC'],
                    'recursive' => -1,
                    'fields' => ['id', 'nom', 'prenom']]);
                $acteur['Suppleant'] = $suppleant['Acteur'] ?? '';
            }

            if (!empty($acteur[$modelAttendance]['mandataire'])) {
                $mandataire = $this->Acteur->find('first', [
                    'conditions' => ['Acteur.id' => $acteur[$modelAttendance]['mandataire'],
                        'Acteur.actif' => true],
                    'order' => ['nom' => 'ASC'],
                    'recursive' => -1,
                    'fields' => ['id', 'nom', 'prenom']]);
                $acteur['Mandataire'] = $mandataire['Acteur'];
            }
        }
        return $acteurs;
    }

    /**
     * @access public
     * @param interger $delib_id
     * @param interger $seance_id
     * @return array
     */
    public function afficherListePresentsGlobale($id)
    {
        $presents = $this->ListePresenceGlobale->find('all', [
            'contain' => [
                'Acteur.Typeacteur.id',
                'Acteur' => ['fields' => ['position','nom']],
            ],
            'conditions' => ['seance_id' => $id],
            'order' => [
                'Acteur.position' => 'ASC',
                'Acteur.nom' => 'ASC'
            ],
            'recursive' => -1
        ]);
        if (empty($presents)) {
            $presents = $this->buildListGlobale($id);
        }

        if (!empty($presents)) {
            foreach ($presents as &$acteur) {
                if (!empty($acteur['ListePresenceGlobale']['mandataire'])) {
                    $mandataire = $this->Seance->Typeseance->Acteur->read(
                        'nom, prenom',
                        $acteur['ListePresenceGlobale']['mandataire']
                    );
                    //['data']
                    $acteur['ListePresenceGlobale']['mandataire'] =
                        $mandataire['Acteur']['prenom'] . " " . $mandataire['Acteur']['nom'];
                } elseif (!empty($acteur['ListePresenceGlobale']['suppleant_id'])) {
                    $suppleant = $this->Seance->Typeseance->Acteur->read(
                        'nom, prenom',
                        $acteur['ListePresenceGlobale']['suppleant_id']
                    );
                    $acteur['ListePresenceGlobale']['suppleant'] =
                        $suppleant['Acteur']['prenom'] . " " . $suppleant['Acteur']['nom'];
                }
            }
        }

        return $presents;
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function buildListGlobale($id)
    {
        $seance = $this->Seance->find('first', [
            'conditions' => ['Seance.id' => $id],
            'recursive' => -1,
            'fields' => ['Seance.type_id']]);
        $elus = $this->Seance->Typeseance->acteursConvoquesParTypeSeanceId($seance['Seance']['type_id'], true);
        foreach ($elus as $elu) {
            $this->ListePresenceGlobale->create();
            $params['data']['ListePresenceGlobale']['acteur_id'] = $elu['Acteur']['id'];
            $params['data']['ListePresenceGlobale']['present'] = 1;
            $params['data']['ListePresenceGlobale']['seance_id'] = $id;
            $this->ListePresenceGlobale->save($params['data']);
        }

        return $this->ListePresenceGlobale->find('all', [
            'conditions' => ['seance_id' => $id],
            'order' => ['Acteur.position' => 'ASC'],
            'contain' => ['Acteur', 'Acteur.Typeacteur']]);
    }

    /**
     * @param type $seance_id
     * @param type $position
     * @return int
     * @version 4.3
     * @access protected
     */
    private function getDelibIdByPosition($seance_id, $position)
    {
        $delib = $this->DeliberationSeance->find(
            'first',
            ['conditions' => ['DeliberationSeance.position' => $position - 1,
                'Seance.id' => $seance_id],
                'fields' => ['Deliberation.id']]
        );

        if (isset($delib['Deliberation']['id'])) {
            return $delib['Deliberation']['id'];
        } else {
            return 0;
        }
    }
}
