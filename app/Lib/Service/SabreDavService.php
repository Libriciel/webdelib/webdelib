<?php

class SabreDavService
{
    public function __construct()
    {
    }

    /**
     *
     * @param string $fileDir
     * @return File
     */
    public function getFileDav($fileDir)
    {
        $this->file = new File(TMP . 'files' . DS . $fileDir);
        return $this->file;
    }
}
