<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('S2lowComponent', 'Controller/Component');
App::uses('TdtMessage', 'Model');
App::uses('AppTools', 'Lib');
App::uses('CakeTime', 'Utility');
App::uses('ConnecteurLib', 'Lib');
App::uses('ActeTdt', 'Lib');
App::uses('ActeTdtPastell', 'Lib/Tdt');
App::uses('ActeTdtS2low', 'Lib/Tdt');
App::uses('ConnecteurS2low', 'Lib');
App::uses('Nomenclature', 'Model');

/**
 * [Tdt description]
 */
class Tdt extends ConnecteurLib
{
    /**
     * @var Component s2lowComponent
     */
    public $S2low;

    public $ConnecteurS2low;

    /**
     * @var Model TdtMessage
     */
    private $TdtMessage;

    /**
     * @var array type_message => libelle_type_message
     */
    public $echanges = [
        '2' => 'courrier_simple',
        '3' => 'demande_piece_complementaire',
        '4' => 'lettre_observation',
        '5' => 'defere_tribunal_administratif'
    ];

    /**
     * Appelée lors de l'initialisation de la librairie
     * Charge le bon protocol de signature et initialise le composant correspondant
     *
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct(Configure::read('TDT'));

        if (!Configure::read('USE_TDT')) {
            throw new Exception("TDT désactivée");
        }
        if (!$this->getType()) {
            throw new Exception("Aucun TDT désigné");
        }
        if (Configure::read("USE_" . $this->getType())) {
            $this->connecteur = Configure::read('TDT');
        } else {
            throw new Exception(
                "Le connecteur tdt désigné n'est pas activé : USE_" . $this->getType()
            );
        }

        if ($this->getType() == 'S2LOW') {
            $this->S2low = new S2lowComponent($this->collection);
            $this->ConnecteurS2low = new ConnectorS2low();
        }

        $this->TdtMessage = ClassRegistry::init('TdtMessage');
        $this->Deliberation = ClassRegistry::init('Deliberation');
        $this->Seance = ClassRegistry::init('Seance');
    }

    /**
     * Envoi le dossier dans pastell au TdT
     *
     * @param type $acte
     * @param type $document
     * @param type $annexes
     * @return type
     */
    public function sendPastell($acte, $document = null, $annexes = [], $user_id = null)
    {
        $newPastell = false;
        try {
            //Gestion dans Pastell des signatures manuscrites parapheur_etat==0 OR IS NULL
            if (empty($acte['Deliberation']['parapheur_etat'])) {
                $acte[$this->configField('envoi_signature')] = false;
            }
            // Création de l'acte
            if (empty($acte['Deliberation']['pastell_id'])) {
                $acte['Deliberation']['pastell_id'] = parent::createPastell($acte, $document, $annexes);
                if (empty($acte['Deliberation']['pastell_id'])) {
                    throw new Exception('pastell_id est vide suite à le demande de création');
                }
                $newPastell=true;
            }

            // Modification des données de transmissions
            $this->Pastell->updateTdt($this->idE, $acte['Deliberation']['pastell_id'], $acte);

            // Envoi de la demande de télétransmission
            $this->Pastell->action(
                $this->idE,
                $acte['Deliberation']['pastell_id'],
                $this->configAction('send-tdt')
            );

            // Récupération de l'identifiant de télétransmission si celui-ci est disponible
            $infos = $this->Pastell->detailDocument($this->idE, $acte['Deliberation']['pastell_id']);
            if (empty($infos['data']['tedetis_transaction_id'])) {
                CakeLog::error(__('TdT tedetis_transaction_id manquant'), 'Pastell');
            }
            $tdtId = trim($infos['data']['tedetis_transaction_id']);
            $this->Deliberation->saveSendTdt($acte['Deliberation']['id'], $tdtId, $user_id);

            return $tdtId;
        } catch (Exception $e) {
            if ($newPastell) {
                $this->Pastell->action(
                    $this->idE,
                    $acte['Deliberation']['pastell_id'],
                    $this->configAction('supression')
                );
                $this->Deliberation->clear();
                $this->Deliberation->id = $acte['Deliberation']['id'];
                $this->Deliberation->saveField('pastell_id', '');
            }
            throw new Exception($e->getMessage());
        }

        return false;
    }

    /**
     * @return type
     * @throws Exception
     */
    public function sendS2low($acte, $document = null, $annexes = [], $user_id = null)
    {
        App::uses('Folder', 'Utility');
        App::uses('File', 'Utility');

        if (!Configure::read('USE_TDT')) {
            throw new Exception(
                __('Erreur : TDT désactivé. Pour activer ce service, veuillez contacter votre administrateur.')
            );
        }
        //$Tdt = new Tdt();

        try {
            if (empty($acte['Deliberation']['num_pref'])) {
                throw new Exception(
                    __('Erreur (%s) : Aucune classification sélectionnée.', $acte['Deliberation']['num_delib'])
                );
            }

            $typeacte = $this->Deliberation->Typeacte->find('first', [
                'fields' => [
                    'Typeacte.id'
                ],
                'contain' => [
                    'Nature' => ['fields' => 'Nature.code']
                ],
                'conditions' => [
                    'Typeacte.id' => $acte['Deliberation']['typeacte_id']
                ],
                'recursive' => -1,
            ]);
            switch ($typeacte['Nature']['code']) {
                case 'DE':
                    $nature_code = 1;
                    break;
                case 'AR':
                    $nature_code = 2;
                    break;
                case 'AI':
                    $nature_code = 3;
                    break;
                case 'CC':
                    $nature_code = 4;
                    break;
                case 'AU':
                    $nature_code = 6;
                    break;
                default:
                    throw new Exception(
                        __('Erreur (%s) : Aucune nature disponible.', $acte['Deliberation']['num_delib'])
                    );
            }

            $classifications = explode('.', $acte['Deliberation']['num_pref']);
            for ($position = 0; $position < 5; $position++) {
                if (!empty($classifications[$position])) {
                    ${'class'.($position+1)} = $classifications[$position];
                } else {
                    ${'class'.($position+1)} = '';
                }
            }

            if (empty($document)) {
                throw new Exception(__('Erreur (%s) : Fichier Vide.', $acte['Deliberation']['num_delib']));
            }
            $folder = new Folder(AppTools::newTmpDir(TMP . 'files' . DS . 'tdt' . DS), true, 0777);
            $errors = $folder->errors();
            if (!empty($errors)) {
                throw new Exception(
                    __('Erreur (%s) : %s.', $acte['Deliberation']['num_delib'], implode($errors))
                );
            }

            $file = new File($folder->pwd() . DS . 'D_' . $acte['Deliberation']['id'] . '.pdf', true);
            $file->write($document);
            $file->close();

            if (!empty($acte['Deliberation']['signature'])) {
                $fileSignature = new File($folder->pwd() . DS . 'signature_' . $acte['Deliberation']['id'] . '.zip');
                $fileSignature->write($acte['Deliberation']['signature']);

                $zip = new ZipArchive();
                $fileNameSignature = null;

                if ($zip->open($fileSignature->pwd()) === true) {
                    for ($i = 0; $i < $zip->numFiles; $i++) {
                        $entry = $zip->getNameIndex($i);

                        if (strrpos($entry, '- 1 -') !== false) {
                            $zip->extractTo($folder->pwd() . DS, [$entry]);
                            $zip->close();
                            $fileNameSignature = $entry;
                            break;
                        }
                    }
                    if (empty($fileNameSignature)) {
                        throw new Exception(
                            __(
                                'Erreur (%s) : Erreur lecture signature.',
                                $acte['Deliberation']['num_delib']
                            )
                        );
                    }
                }
                $fileSignature->close();
            }

            // Check $decision_date
            $seanceDeliberanteId = $this->Deliberation->getSeanceDeliberanteId($acte['Deliberation']['id']);
            if (!empty($seanceDeliberanteId)) {
                $this->Seance->id = $seanceDeliberanteId;
                $this->Seance->recursive = -1;
                $decision_date = CakeTime::i18nFormat($this->Seance->field('date'), '%Y-%m-%d');
            } else {
                $decision_date = CakeTime::i18nFormat($acte['Deliberation']['date_acte'], '%Y-%m-%d');
            }

            $acteTdt = [
                'api' => '1',
                'nature_code' => utf8_decode($nature_code),
                'type_acte' => utf8_decode($acte['Deliberation']['typologiepiece_code']),
                'document_papier' => utf8_decode($acte['Deliberation']['tdt_document_papier']),
                'classif1' => utf8_decode($class1),
                'classif2' => utf8_decode($class2),
                'classif3' => utf8_decode($class3),
                'classif4' => utf8_decode($class4),
                'classif5' => utf8_decode($class5),
                'number' => utf8_decode($acte['Deliberation']['num_delib']),
                'decision_date' => $decision_date,
                'subject' => AppTools::convertUTF8TForS2low(
                    substr($acte['Deliberation']['objet_delib'], 0, 499)
                ), //LIMITE s²low
                'document_papier' => utf8_decode(($acte['Deliberation']['tdt_document_papier'] ? '1' : '0')),
                'acte_pdf_file' => ['pwd'=> $file->pwd(),
                        'typemime'=>  'application/pdf',
                        'filename'=>  $file->name]
            ];

            if (!empty($fileNameSignature) && file_exists($folder->pwd() . DS . $fileNameSignature)) {
                $acteTdt['acte_pdf_file_sign'] = [
                        'pwd'=>  $folder->pwd() . DS . $fileNameSignature,
                        'typemime'=>  'application/zip',
                        'filename'=>  $fileNameSignature];
            }

            if (!empty($annexes)) {
                $acteTdt['acte_attachments_sign[' . count($annexes) . ']'] = "";
                foreach ($annexes as $key => $annexe) {
                    $annexe_dir_tmp = AppTools::newTmpDir($folder->pwd());
                    $fileAnnexe = new File($annexe_dir_tmp . DS . $annexe['filename']);
                    $fileAnnexe->append($annexe['content']);
                    $acteTdt["acte_attachments[$key]"] = [
                        'pwd'=>  $fileAnnexe->pwd(),
                        'typemime'=>  $annexe['mimetype'],
                        'filename'=>  $annexe['filename']];
                    $file->close();
                }
            }
            if (!empty($acte['annexe_typologiepiece_code'])) {
                foreach ($acte['annexe_typologiepiece_code'] as $key => $typologiepieceCode) {
                    $acteTdt["type_pj[$key]"] = $typologiepieceCode;
                }
            }
            $curl_return = utf8_encode($this->S2low->send($acteTdt));

            if (strpos($curl_return, 'OK') === false) {
                throw new Exception(
                    __(
                        'Erreur (%s) : %s.',
                        $acte['Deliberation']['num_delib'],
                        !empty($curl_return) ? $curl_return : 'S2low->send'
                    )
                );
            }
            CakeLog::info($curl_return, 'tdt');
            $tdt_id = substr($curl_return, 3, strlen($curl_return));
            $folder->delete();
            $this->Deliberation->saveSendTdt($acte['Deliberation']['id'], trim($tdt_id), $user_id);

            return trim($tdt_id);
        } catch (Exception $e) {
            if (isset($folder)) {
                $folder->delete(); //Purge du dernier dossier en cas d'erreur*
            }
            CakeLog::error($curl_return, 'tdt');
            throw new Exception($e->getMessage());
        }

        return false;
    }

    /**
     * @param int $id_d
     * @param bool $get_details récupérer le message et la date
     * @return array(
     * 'action' => 'rejet-iparapheur',
     * 'message' => 'Le document a été rejeté dans le parapheur : 23/01/2014 16:54:52 : [RejetSignataire] vu',
     * 'date' => '2014-01-23 16:55:07'
     * )
     */
    public function getLastActionPastell($id_d, $get_details = false)
    {
        $details = $this->Pastell->detailDocument($this->idE, $id_d);
        if ($get_details) {
            return $details['last_action'];
        }
        $last_action = $details['last_action']['action'];
        return $last_action;
    }

    /**
     * @param int $id_d
     * @return array
     */
    public function getDetailsPastell($id_d)
    {
        return $this->Pastell->detailDocument($this->idE, $id_d);
    }

    /**
     * @param int $id_d
     * @param bool $all
     * @return array|bool
     */
    public function getReponsesPastell($acte)
    {
        $id_d = $acte['Deliberation']['pastell_id'];
        if (empty($id_d)) {
            return false;
        }
        $tdt_messages = [];
        $infos = $this->Pastell->detailDocument($this->idE, $id_d);
        if (empty($infos['data'])) {
            return false;
        }

        foreach ($this->echanges as $type => $echange) {
            if (!empty($infos['data']['has_' . $echange])) {
                $demande = $this->TdtMessage->findByTdtId($infos['data'][$echange . '_id']);

                if (empty($demande)) {
                    $tdt_messages[] = [
                        'type' => $type,
                        'id' => $infos['data'][$echange . '_id'],
                        'data' => $this->Pastell->getFile($this->idE, $id_d, $echange),
                        'date_message' => $infos['data'][$echange . '_date']
                    ];
                }
                if (!empty($infos['data'][$echange . '_response_transaction_id'])) {
                    $reponse = $this->TdtMessage->findByTdtId($infos['data'][$echange . '_response_transaction_id']);
                    if (empty($reponse)) {
                        $tdt_messages[] = [
                            'type' => $type,
                            'id' => $infos['data'][$echange . '_response_transaction_id'],
                            'data' => $this->Pastell->getFile($this->idE, $id_d, 'reponse_' . $echange),
                            'date_message' => null // Pas de date retour de patell
                        ];
                    }
                }
            }
        }

        return $tdt_messages;
    }

    /**
     * @param type $acte
     * @return type
     */
    public function getReponsesS2low($acte)
    {
        $tdt_id = $acte['Deliberation']['tdt_id'];
        $tdt_messages = [];
        $result = $this->S2low->getNewFlux($tdt_id);
        if (!empty($result)) {
            $result_array = explode("\n", trim($result));
            if ($result_array[0] != 'KO') {
                foreach ($result_array as $line) {
                    if (!empty($result)) {
                        list($type, $status, $id) = explode("-", $line);
                        $tdt_messages[] = [
                            'type' => $type,
                            'status' => $status,
                            'id' => $id,
                            'data' => $this->S2low->getCourrier($id)
                        ];
                    }
                }
            }
        }
        return $tdt_messages;
    }

    /**
     *
     * @param type $acte
     * @return boolean
     */
    public function getDateArPastell($acte)
    {
        $id_d = $acte['Deliberation']['pastell_id'];
        $infos = $this->Pastell->detailDocument($this->idE, $id_d);
        if (!empty($infos['data']['date_ar'])) {
            // A vérifier que la date est au format date et non comme la fonction french
            return $infos['data']['date_ar'];
        } else {
            return false;
        }
    }

    /**
     * @param type $acte
     * @return boolean
     */
    public function getDateArS2low($acte)
    {
        $tdt_id = $acte['Deliberation']['tdt_id'];
        $flux = $this->S2low->getFluxRetour($tdt_id);
        $codeRetour = substr($flux, 3, 1);
        if ($codeRetour == 4) {
            $dateAR = $this->S2low->getDateAR($res = mb_substr(
                $flux,
                strpos($flux, '<actes:ARActe'),
                strlen($flux)
            ));
            return $dateAR;
        }

        return false;
    }

    /**
     *
     * @return type
     */
    public function getDateClassification()
    {
        $doc = new DOMDocument();
        if (!@$doc->load(Configure::read('S2LOW_CLASSIFICATION'))) {
            $this->updateClassification();
            $doc->load(Configure::read('S2LOW_CLASSIFICATION'));
        }

        return !empty($doc->getElementsByTagName('DateClassification')->item(0)->nodeValue)
            ? $doc->getElementsByTagName('DateClassification')->item(0)->nodeValue
            : null;
    }

    /**
     *
     * @return type
     */
    public function updateClassification()
    {
        try {
            $classification = $this->getClassification();
            App::uses('Nature', 'Model');
            $nature = ClassRegistry::init('Nature');
            $nature->saveNatureActes($classification['NaturesActes']);

            $nomenclature = ClassRegistry::init('Nomenclature');

            $nomenclature->deleteAll(['1 = 1'], false);

            foreach ($classification['Matieres'] as $key => $matiere) {
                $nomenclature->create();
                $data['Nomenclature']['parent_id'] = null;
                $data['Nomenclature']['name'] = $matiere['name'];
                $data['Nomenclature']['libelle'] = $matiere['libelle'];
                $nomenclature->save($data);

                $this->saveMatieres($matiere['children'], $nomenclature->getInsertID());
            }

            return true;
        } catch (Exception $e) {
            CakeLog::error($e->getMessage(), 'connector');
        }

        return false;
    }

    /**
     * Récupération de l'acte tamponé au format pdf
     *
     * @param type $acte
     * @return type
     */
    public function getTamponS2low($acte)
    {
        $tdt_id = $acte['Deliberation']['tdt_id'];
        $flux = $this->S2low->getActeTampon($tdt_id);
        $infoContent = AppTools::fileMime($flux, true, 'application/pdf');
        if (!empty($infoContent['mimetype']) && $infoContent['mimetype'] == 'application/pdf') {
            return $flux;
        }

        return false;
    }

    /**
     * Récupération des annexes tamponées au format pdf
     *
     * @param type $acte
     * @return type
     */
    public function getAnnexesTamponS2low($acte, $ordre)
    {
        $tdt_id = $acte['Deliberation']['tdt_id'];
        return $this->S2low->getActeAnnexesTampon($tdt_id, $ordre);
        /*$infoContent = AppTools::fileMime($flux, true, 'application/pdf');
        if (!empty($infoContent['mimetype']) && $infoContent['mimetype'] == 'application/pdf') {
            return $flux;
        }
        return false;*/
    }

    /**
     * Récupération de l'acte tamponé au format pdf
     *
     * @param type $acte
     * @return boolean
     * @deprecated
     */
    public function getTamponPastell($acte)
    {
        $id_d = $acte['Deliberation']['pastell_id'];
        if (empty($id_d)) {
            return false;
        }
        $infos = $this->Pastell->detailDocument($this->idE, $id_d);
        if (!empty($infos['data']['acte_tamponne'])) {
            return $this->Pastell->getFile($this->idE, $id_d, $this->config['field']['acte_tamponne']);
        }

        return false;
    }

    /**
     * Récupération du fichier bordereau
     *
     * @param type $acte
     * @return boolean
     * @deprecated
     */
    public function getBordereauPastell($acte)
    {
        $id_d = $acte['Deliberation']['pastell_id'];
        if (empty($id_d)) {
            return false;
        }
        $infos = $this->Pastell->detailDocument($this->idE, $id_d);
        if (!empty($infos['data']['bordereau'])) {
            return $this->Pastell->getFile($this->idE, $id_d, $this->config['field']['bordereau']);
        }

        return false;
    }

    /**
     * Récupération du fichier bordereau
     *
     * @param type $acte
     * @return type
     */
    public function getBordereauS2low($tdt_id)
    {
        return $this->S2low->getActeBordereau($tdt_id);
    }

    /** TODO FIXME
     * Récupération du fichier de message
     *
     * @param type $message_id
     * @return type
     */
    public function getDocumentS2low($message_id)
    {
        return $this->S2low->getDocument($message_id);
    }

    /**
     * Récupération du de l'ARacte
     *
     * @param type $acte
     * @return boolean
     */
    public function getArActeS2low($tdt_id)
    {
        return $this->S2low->getFluxRetour($tdt_id);
    }

    /**
     * @param type $tdt
     * @param type $acte
     * @return \ActeTdtS2low
     */
    public function createActeTdtS2low($acte, $connecteurS2low = null)
    {
        if (is_null($connecteurS2low)) {
            $connecteurS2low=$this->ConnecteurS2low;
        }
        $tdt_id = $acte['Deliberation']['tdt_id'];
        return new ActeTdtS2low($tdt_id, $connecteurS2low);
    }

    /**
     * @param type $tdt
     * @param type $acte
     * @return \ActeTdtPastell
     */
    public function createActeTdtPastell($acte, $connecteurPastell = null)
    {
        if (is_null($connecteurPastell)) {
            $connecteurPastell=$this->Pastell;
        }
        $donnees = [
                    'idE'=>$this->idE,
                    'id_d'=>$acte['Deliberation']['pastell_id'],
                    'config'=>$this->config['field']
                  ];
        return new ActeTdtPastell($connecteurPastell, $donnees);
    }

    /**
     * Récupération du de l'ARacte
     *
     * @param type $acte
     * @return boolean
     */
    public function getArActePastell($acte)
    {
        $id_d = $acte['Deliberation']['pastell_id'];
        if (!empty($id_d)) {
            $infos = $this->Pastell->detailDocument($this->idE, $id_d);
            if (!empty($infos['data']['aractes'])) {
                return $this->Pastell->getFile($this->idE, $id_d, $this->config['field']['aractes']);
            }
        }

        return false;
    }

    /**
     *
     * @return boolean|array
     * @throws Exception
     */
    public function getClassificationS2low()
    {
        try {
            $classification = $this->S2low->getClassification();
            $filePathClassification = $this->saveClassification($classification);
            $xmlObject = Xml::build($filePathClassification);
            $xmlArray = Xml::toArray($xmlObject);
        } catch (XmlException $e) {
            return false;
        } catch (Exception $e) {
            throw new Exception('Erreur de récupération de la classification.');
        }

        return [
          'NaturesActes' => $this->getNatures(
              $xmlArray['RetourClassification']['actes:NaturesActes']['actes:NatureActe']
          ),
          'Matieres' => $this->getMatieres(
              1,
              null,
              $xmlArray['RetourClassification']['actes:Matieres']['actes:Matiere1']
          )
        ];
    }

    /**
     *
     * @return boolean|array
     * @throws Exception
     */
    public function getClassificationPastell()
    {
        try {
            $classification = $this->Pastell->getClassification($this->idE);
            $filePathClassification = $this->saveClassification($classification);

            $xmlObject = Xml::build($filePathClassification);
            $xmlArray = Xml::toArray($xmlObject);
        } catch (XmlException $e) {
            return false;
        } catch (Exception $e) {
            throw new Exception('Erreur de récupération de la classification.');
        }

        return [
          'NaturesActes'=>$this->getNatures(
              $xmlArray['RetourClassification']['actes:NaturesActes']['actes:NatureActe']
          ),
          'Matieres'=>$this->getMatieres(
              1,
              null,
              $xmlArray['RetourClassification']['actes:Matieres']['actes:Matiere1']
          )
        ];
    }

    /**
     * [getNatures description]
     *
     * @param  [type] $natures [description]
     * @return [type]          [description]
     */
    private function getNatures($natures)
    {
        $liste = [];
        foreach ($natures as $nature) {
            $liste[] = [
            'name' => $nature['@actes:Libelle'],
            'code' => $nature['@actes:TypeAbrege'],
            'typesPJNatureActe' => $this->getTypePJNatureActes($nature['actes:TypePJNatureActe'])
            ];
        }

        return $liste;
    }

    /**
     * [getTypePJNatureActes description]
     *
     * @param  [type] $natures [description]
     * @return [type]          [description]
     */
    private function getTypePJNatureActes($typePJNatureActes)
    {
        $liste = [];
        foreach ($typePJNatureActes as $typePJNatureActe) {
            $liste[] = [
              'name' => $typePJNatureActe['@actes:Libelle'],
              'code' => $typePJNatureActe['@actes:CodeTypePJ']
            ];
        }

        return $liste;
    }

    /**
     *
     * @param type $start
     * @param type $key
     * @param type $aMatiere
     * @return type
     */
    private function getMatieres($start, $key, $aMatiere)
    {
        $liste = [];
        $i = 0;
        foreach ($aMatiere as $matiere) {
            $name = (!empty($key) ? $key . '.' : '') . $matiere['@actes:CodeMatiere'];
            $liste[$i] = [
                'name' => $name,
                'libelle' => $name . ' ' . $matiere['@actes:Libelle']
            ];

            $rangChild = $start + 1;
            if (isset($matiere['actes:Matiere' . $rangChild])) {
                $matiereOrdonne = $this->getMatieresOrdonne(
                    $matiere['actes:Matiere' . $rangChild],
                    'actes:Matiere' . $rangChild
                );
                $liste[$i]['children'] = $this->getMatieres($rangChild, $name, $matiereOrdonne);
            }
            $i++;
        }

        return $liste;
    }

    /**
     *
     * @param type $matiere
     * @param type $key
     * @return type
     */
    private function getMatieresOrdonne($matiere, $key)
    {
        if (count($matiere) == 1) {
            if (count($matiere[$key]) == 1) {
                return [$matiere[$key]];
            }
            return $matiere[$key];
        }
        if (isset($matiere['@actes:CodeMatiere'])) {
            return [$matiere];
        }

        return Hash::sort($matiere, '{n}.@actes:CodeMatiere', 'asc');
    }

    /**
     * @param type $matieres
     * @param type $parent
     */
    private function saveMatieres($matieres, $parent)
    {
        $nomenclature = ClassRegistry::init('Nomenclature');

        foreach ($matieres as $matiere) {
            $nomenclature->create();
            $data['Nomenclature']['parent_id'] = $parent;
            $data['Nomenclature']['name'] = $matiere['name'];
            $data['Nomenclature']['libelle'] = $matiere['libelle'];
            $nomenclature->save($data);

            if (!empty($matiere['children'])) {
                $this->saveMatieres($matiere['children'], $nomenclature->getInsertID());
            }
        }
    }
}
