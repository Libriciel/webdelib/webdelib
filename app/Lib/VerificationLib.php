<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

use phpgedooo_client\GDO_PartType;
use phpgedooo_client\GDO_FusionType;
use phpgedooo_client\GDO_ContentType;
use phpgedooo_client\GDO_FieldType;

use PastellClient\Client;
use PastellClient\Api\Version as PastellVersion;
use Http\Adapter\Guzzle6\Client as GuzzleClient;

class VerificationLib
{
    const SUCCESS = 'ok';
    const ERROR = 'ko';

    /**
     *
     * @return type
     */
    public static function configDataBase()
    {
        // fichier de conf database.php
        if (!file_exists(ROOT . DS . 'app' . DS . 'Config' . DS . 'database.php')) {
            return [["libelle" => "Fichier de configuration database.php non trouvé", "okko" => 'ko']];
        }
        return [["libelle" => "Fichier de configuration database.php trouvé", "okko" => 'ok']];
    }

    public static function infosDataBase()
    {
        $infos = [];
        $db = ConnectionManager::getDataSource(Configure::read('Config.tenantName'));

        // affichage des infos de connexion
        foreach ($db->config as $key => $valeur) {
            if ($key == 'password') {
                $valeur = '*******';
            }
            if (in_array($key, ['host', 'login', 'database', 'schema', 'port', 'datasource'], true)) {
                $infos[strval($key)] = strval($valeur); //array("libelle"=>$key . ' : ' . $valeur,"okko"=> 'info');
            }
        }
        return $infos;
    }

    public static function connectionDataBase()
    {
        $infos = [];
        $db = ConnectionManager::getDataSource(Configure::read('Config.tenantName'));

        if (!array_key_exists('port', $db->config)) {
            $db->config['port'] = '';
        }

        $conn = "host='{$db->config['host']}' port='{$db->config['port']}' dbname='{$db->config['database']}' ";
        $conn .= "user='{$db->config['login']}' password='{$db->config['password']}'";
        $link = pg_pconnect($conn);
        unset($conn);

        if (empty($link)) {
            $infos[] = ["libelle" => "Connexion à la base de données échouée", "okko" => 'ko'];
        } else {
            $infos[] = ["libelle" => "Connexion à la base de données réussie" , "okko" => 'ok'];
            // version de la base de données
            $results = pg_query($link, 'SELECT VERSION();');
            $result = pg_fetch_row($results);
            $infos['version'] = ["libelle" => 'Version de la base de données : ' . $result[0], "okko" => 'info'];
        }
        pg_close($link);

        return $infos;
    }

    public static function connectionDataBaseConfig()
    {
        $infos = [];
        $db = ConnectionManager::getDataSource(Configure::read('Config.tenantName'));
        switch ($db->config['datasource']) {
            case 'Database/Postgres':
                // version de la base de données
                $infos[] = ["libelle" => 'Client encodage (client_encoding) : ' . $db->getEncoding(),
                    "okko" => $db->getEncoding()=='UTF8'?'ok':'ko'];
                $result = $db->execute('SHOW bytea_output')->fetch();
                $infos[] = ["libelle" => 'Format de sortie (bytea_output) : ' . $result['bytea_output'],
                    "okko" => $result['bytea_output']=='escape'?'ok':'ko'];
                break;
        }
        return $infos;
    }

    /**
     *
     * @global type $appli_path
     * @global type $versionCakePHPAttendue
     * @global type $versionPHPAttendue
     * @global type $versionAPACHEAttendue
     */
    public static function versionWebdelib()
    {
        // affichage de la version de webdelib
        if (file_exists(ROOT . DS . 'app' . DS . 'Config' . DS . 'core.php')) {
            //include_once(APP . DS. 'Config' . DS. .'core.php');
            $versionFile = file(ROOT . DS . 'VERSION.txt');
            $version = trim(array_pop($versionFile));
            if (empty($version)) {
                return ['libelle' => 'Version de webdelib : déclaration de Version non trouvée', 'okko' => 'ko'];
             //self::d('Version de webdelib : déclaration de Version non trouvée', 'ko');
            } else {
                return ['libelle' => "Version de webdelib : " . $version, 'okko' => 'info'];
                //self::d("Version de webdelib : " . $version, 'info');
            }
        } else {
            return ['libelle' => 'Version de webdelib : fichier core.php non trouvé', 'okko' => 'ko'];
            //self::d('Version de webdelib : fichier core.php non trouvé', 'ko');
        }
    }

    public static function versionCakePhp($versionCakePHPAttendue)
    {
        if (file_exists(ROOT . DS . 'vendors' . DS . 'cakephp' . DS . 'cakephp'
            . DS . 'lib' . DS . 'Cake' . DS . 'VERSION.txt')) {
            $versionFile = file(ROOT . DS . 'vendors' . DS . 'cakephp' . DS . 'cakephp'
                . DS . 'lib' . DS . 'Cake' . DS . 'VERSION.txt');
            $fVer = trim(array_pop($versionFile));
            $okko = (version_compare($fVer, $versionCakePHPAttendue, '>=')) ? 'ok' : 'ko';
            //self::d("Version de CakePHP (attendue $versionCakePHPAttendue) : $fVer", $okko);
            return ['libelle' => "Version de CakePHP (minimum $versionCakePHPAttendue) : $fVer", 'okko' => $okko];
        } else {
            //self::d('Version de CakePHP : fichier de version de CakePHP non trouvé', 'ko');
            return ['libelle' => 'Version de CakePHP : fichier de version de CakePHP non trouvé', 'okko' => 'ko'];
        }
    }

    public static function versionPHP($versionPHPAttendue)
    {
        // version de PHP
        $phpVer = phpversion();
        $okko = (version_compare(phpversion(), $versionPHPAttendue, '>=')) ? 'ok' : 'ko';
        return ['libelle' => "Version de PHP (attendue $versionPHPAttendue) : $phpVer", 'okko' => $okko];
    }

    public static function versionApache($versionAPACHEAttendue)
    {
        // version de APACHE
        $apacheVer = apache_get_version();
        $okko = ($apacheVer >= $versionAPACHEAttendue) ? 'ok' : 'ko';
        return ['libelle' => "Version d'APACHE (attendue $versionAPACHEAttendue) : $apacheVer", 'okko' => $okko];
    }

    public static function versionOS()
    {
        // affichage de la version de l'os du serveur
        $result = [];
        if (DIRECTORY_SEPARATOR === '\\') {
            // windows system
            exec("ver", $result);
            if (!empty($result[0])) {
                $ver = $result[0];
            } elseif (!empty($result[1])) {
                $ver = $result[1];
            } else {
                $ver = 'WIN OS inconnu!';
            }
        } else {
            if (file_exists('/etc/issue.net')) {
                $ver = file_get_contents('/etc/issue.net');
            } elseif (file_exists('/etc/issue')) {
                $ver = file_get_contents('/etc/issue');
                $ver = str_replace(['\\n', '\\l'], '', $ver);
            } else {
                exec("lsb_release -d", $result);
                if (!empty($result)) {
                    $ver = $result[0];
                } else {
                    $ver = 'UNIX OS inconnu!';
                }
            }
        }
        return ['libelle' => "Version de l'OS du serveur : $ver", 'okko' => 'info'];
    }

    /**
     *
     */
    public static function email()
    {
        $okko="ok";
        if (env('APP_ENV')!=='dev') {
            $destinataire = Configure::read("MAIL_FROM");
            $ok = mail(
                'admin@webdelib.invalid',
                'Test de la fonction mail',
                'Ceci est un message de test de webdelib, veuillez ne pas répondre.'
            );
            $okko = $ok ? "ok" : "ko";
        }

        return ['libelle' => "Envoi de courrier électronique : $okko", 'okko' => $okko];
    }

    /**
     *
     * @param type $fichier_conf
     * @return boolean
     */
    public static function envoiSMTP()
    {
        // mailadministrateur
        $retour = [];
        $retour[] = ['libelle' => "Utilisation du SMTP : OUI", 'okko' => 'info'];
        $retour[] = ['libelle' => "Serveur du SMTP : " . Configure::read('SMTP_HOST'), 'okko' => 'info'];
        $retour[] = ['libelle' => "Port du serveur SMTP : " . Configure::read('SMTP_PORT'), 'okko' => 'info'];
        $retour[] = [
            'libelle' => "Utilisateur du serveur SMTP : " . Configure::read('SMTP_USERNAME'),
            'okko' => 'info'
        ];
        $smtp = Configure::read('SMTP_PASSWORD');
        if (!empty($smtp)) {
            $retour[] = ['libelle' => "Password du serveur SMTP : ********", 'okko' => 'info'];
        }
    }

    /**
     *
     * @param type $fichier_conf
     * @return boolean
     */
    public static function variableNecessaire($description, $variable)
    {
        // vérification des variables de configuration
        if (!Configure::check($variable)) {
            return [
                "libelle" => $description . ": déclaration de "
                    . $variable . " non trouvée dans le fichier de configuration",
                "okko" => 'ko'];
        }
        $valeur = Configure::read($variable);
        if (empty($valeur)) {
            //debug($variable." : ".$valeur);
            return [
                "libelle" => $description . " : " .
                    $variable . " non renseigné dans le fichier de configuration",
                "okko" => 'ko'];
        } else {
            return [
                "libelle" => $description . " : " . $valeur, "okko" => 'ok'];
        }
    }

    public static function typeConversion()
    {
        $convTypes = ['GedoooCloudooo' => 'Conversion Cloudooo'];
        $convType = Configure::read('FusionConv.method');

        if (!array_key_exists($convType, $convTypes)) {
            return ["libelle" => "Type d'outil de conversion : $convType n'est pas géré par webdelib", "okko" => 'ko'];
        } else {
            return ["libelle" => "Type d'outil de conversion : $convType ", "okko" => 'ok'];
        }
    }

    public static function afficheInfosCloudoo()
    {
        $cloudoooHost = Configure::read('FusionConv.cloudooo_host');

        return ["libelle" => "Url de CLOUDOOO : $cloudoooHost", "okko" => 'info'];
    }

    public static function conversion()
    {
        // exécutable du convertisseur
        $repModels = ROOT . DS . 'app' . DS . WEBROOT_DIR . DS . 'files' . DS;
        $modelFileName = 'empty.odt';
        require_once 'XML/RPC2/Client.php';
        // initialisations
        $options = [
           'uglyStructHack' => true
        ];

        $url = Configure::read('FusionConv.cloudooo_host');
        try {
            $client = XML_RPC2_Client::create($url, $options);
            $result = $client->convertFile(base64_encode($repModels . $modelFileName), 'odt', 'pdf', false, true);
            return ["libelle" => "Communication avec l'outil de conversion", "okko" => 'ok'];
        } catch (XML_RPC2_FaultException $e) {
            return ["libelle" => "Communication avec l'outil de conversion" . ' (Code erreur : ' . $e->getCode()
                . ')', "okko" => 'ko'];
        } catch (Exception $e) {
            return ["libelle" => "Communication avec l'outil de conversion" . ' (Code erreur : ' . $e->getCode()
                . ')', "okko" => 'ko'];
        }
    }

    public static function reponseGedooo()
    {
        try {
            $gedoooRest = Configure::read('FusionConv.Gedooo.wsdl');
            // Get cURL resource
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, [
               CURLOPT_RETURNTRANSFER => 1,
               CURLOPT_URL => $gedoooRest . "/rest/version"
            ]);
            // Send the request & save response to $resp
            $version = curl_exec($curl);
            // Close request to clear up some resources
            curl_close($curl);

            $params = [];
            if (!empty($version)) {
                return ["libelle" => "Gedooo répond", "okko" => 'ok'];
            }
            return ["libelle" => "Gedooo ne répond pas", "okko" => 'ok'];
        } catch (Exception $e) {
            return [
                "libelle" => "Version de l'outil d'édition : Erreur lors de la connexion au WSDL : " .
                $e->getCode() . $e->getMessage(), "okko" => 'ko'
            ];
        }
    }

    /**
     *
     * @global type $appli_path
     * @global type $fichier_conf
     * @return boolean
     */
    public static function versionGedooo()
    {
        // initialisations
        try {
            $gedoooRest = Configure::read('FusionConv.Gedooo.wsdl');
            // Get cURL resource
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, [
               CURLOPT_RETURNTRANSFER => 1,
               CURLOPT_URL => $gedoooRest . "/rest/version"
            ]);
            // Send the request & save response to $resp
            $version = curl_exec($curl);
            // Close request to clear up some resources
            curl_close($curl);

            $params = [];
            return ["libelle" => "Version de l'outil d'édition : " . $version, "okko" => 'ok'];
        } catch (Exception $e) {
            //Erreur lors de l'initialisation de la connexion : code 001
            return [
                "libelle" => "Version de l'outil d'édition : Erreur lors de la connexion au WSDL : "
                . $e->getCode() . $e->getMessage(), "okko" => 'ko'
            ];
        }
    }

    public static function fusionGedoo()
    {
        // test d'édition
        $tmpFile = "/tmp/FILE_RESULT.odt";
        @unlink($tmpFile);

        $fichierSource = ROOT . DS . 'plugins' . DS . 'FusionConv' . DS . 'Config' . DS . 'OdtVide.odt';
        $oTemplate = new GDO_ContentType(
            "",
            "modele.odt",
            "application/vnd.oasis.opendocument.text",
            "binary",
            file_get_contents($fichierSource)
        );

        $time_start = microtime(true);
        $oMainPart = new GDO_PartType();
        $oMainPart->addElement(new GDO_FieldType('ma_variable', 'OK', 'text'));
        $oFusion = new GDO_FusionType(
            Configure::read('FusionConv.Gedooo.wsdl'),
            $oTemplate,
            "application/vnd.oasis.opendocument.text",
            $oMainPart
        );
        try {
            $oFusion->process();
            $oFusion->sendContentToFile($tmpFile);
            $time_end = microtime(true);
            $time = round($time_end - $time_start, 2);
            if (file_exists($tmpFile)) {
                return ["libelle" => "Fusion réussie avec ODFGEDOOo en $time secondes", "okko" => 'ok'];
            } else {
                return ["libelle" => 'Fusion échouée avec ODFGEDOOo', "okko" => 'ko'];
            }
        } catch (Exception $e) {
            return ["libelle" => "Fusion échouée avec ODFGEDOOo : " . $e->getMessage(), "okko" => 'ko'];
        }
    }

    /**
     *
     */
    public static function presenceModelesOdt()
    {
        // initialisations
        $repModels = ROOT . DS . 'app' . DS . WEBROOT_DIR . DS . 'files' . DS;
        $modelFileName = 'empty.odt';
        if (file_exists($repModels . $modelFileName)) {
            $resultMessage = $repModels . $modelFileName;
            $okko = 'ok';
        } else {
            $resultMessage = "$repModels$modelFileName non trouvé";
            $okko = 'ko';
        }
        d($resultMessage, $okko);
    }

    /**
     *
     */
    public static function getClassification()
    {
        $resultats = [];
        $time_start = microtime(true);
        $url = Configure::read('S2LOW_HOST') .'/modules/actes/actes_classification_fetch.php';

        if (Configure::read('S2LOW_USELOGIN')) {
            $urlConnexion = Configure::read('S2LOW_HOST') .'/api/test-connexion.php';
            $chConnexion = curl_init();
            curl_setopt($chConnexion, CURLOPT_URL, $urlConnexion);
            if (Configure::read('S2LOW_USEPROXY')) {
                curl_setopt($chConnexion, CURLOPT_PROXY, Configure::read('S2LOW_PROXYHOST'));
            }
            curl_setopt($chConnexion, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($chConnexion, CURLOPT_CAPATH, str_replace(
                'app_check',
                'app',
                Configure::read('S2LOW_CAPATH')
            ));
            curl_setopt($chConnexion, CURLOPT_SSLCERT, str_replace(
                'app_check',
                'app',
                Configure::read('S2LOW_PEM')
            ));
            curl_setopt($chConnexion, CURLOPT_SSLCERTPASSWD, Configure::read('S2LOW_CERTPWD'));
            curl_setopt($chConnexion, CURLOPT_SSLKEY, str_replace(
                'app_check',
                'app',
                Configure::read('S2LOW_SSLKEY')
            ));
            curl_setopt($chConnexion, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($chConnexion, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($chConnexion, CURLOPT_USERPWD, Configure::read('S2LOW_LOGIN').':'
                .Configure::read('S2LOW_PWD'));
            $reponse = curl_exec($chConnexion);
            if (curl_errno($chConnexion)) {
                $resultats[] = ["libelle" => curl_error($chConnexion), "okko" => 'ko'];
                return $resultats;
            }
            if (!empty($reponse) && stripos($reponse, 'OK') === false) {
                $resultats[] = ["libelle" => 'Echec de connexion (login ou password incorrect)', "okko" => 'ko'];

                return $resultats;
            }
        }

        $data = ['api' => '1'];
        $url .= (stripos($url, '?')!==false ? '&' : '?') . http_build_query($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if (Configure::read('S2LOW_USEPROXY')) {
            curl_setopt($ch, CURLOPT_PROXY, Configure::read('S2LOW_PROXYHOST'));
        }
        if (Configure::read('S2LOW_USELOGIN')) {
            curl_setopt($chConnexion, CURLOPT_USERPWD, Configure::read('S2LOW_LOGIN').':'
                .Configure::read('S2LOW_PWD'));
        }
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CAPATH, str_replace('app_check', 'app', Configure::read('S2LOW_CAPATH')));
        curl_setopt($ch, CURLOPT_SSLCERT, str_replace('app_check', 'app', Configure::read('S2LOW_PEM')));
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, Configure::read('S2LOW_CERTPWD'));
        curl_setopt($ch, CURLOPT_SSLKEY, str_replace('app_check', 'app', Configure::read('S2LOW_SSLKEY')));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $reponse = curl_exec($ch);
        if (curl_errno($ch)) {
            $resultats[] = ["libelle" => curl_error($ch), "okko" => 'ko'];
            return $resultats;
        }
        curl_close($ch);
        $time_end = microtime(true);
        if (stripos($reponse, 'DateClassification') !== false) {
            $time = round($time_end - $time_start, 2);
            $resultats[] = ["libelle" => Configure::read('S2LOW_HOST'), "okko" => 'info'];
            $resultats[] = ["libelle" => "Fichier de classification S²low récupéré en $time secondes", "okko" => 'ok'];
        } else {
            if (stripos($reponse, 'KO') !== false) {
                $resultats[] = ["libelle" => utf8_encode($reponse), "okko" => 'ko'];
                return $resultats;
            }
            $resultats[] = ["libelle" => 'Echec de récupération du fichier de classification', "okko" => 'ko'];
        }

        return $resultats;
    }

    /**
     *
     * @return type
     */
    public static function getCircuitsParapheur()
    {
        $resultats = [];
        $time_start = microtime(true);

        //FIX
        App::uses('ComponentCollection', 'Controller');
        App::uses('Component', 'Controller');
        include(ROOT . DS . 'app' . DS . 'Controller' . DS . 'Component' . DS . 'IparapheurComponent.php');
        //App::uses('IparapheurComponent', 'Controller/Component');

        $collection = new ComponentCollection();
        $Parafwebservice = new IparapheurComponent($collection);
        $echo = $Parafwebservice->echoWebservice();

        $resultats[] = ["libelle" => $Parafwebservice->wsto, "okko" => 'info'];
        if (strpos($echo, 'coucou marie claude') == false) {
            $resultats[] = ["libelle" => 'Connexion au parapheur', "okko" => 'ko'];
            return $resultats;
        }
        $resultats[] = ["libelle" => 'Connexion au parapheur', "okko" => 'ok'];

        $typeCircuit = Configure::read('IPARAPHEUR_TYPE');
        $webservices = $Parafwebservice->getListeTypesWebservice();

        if (!in_array($typeCircuit, $webservices['typetechnique'], true)) {
            $resultats[] = [
                "libelle" => 'Le type technique ' . $typeCircuit . ' n\'est pas créé sur le parapheur',
                "okko" => 'ko'
            ];
            return $resultats;
        }
        $resultats[] = [
            "libelle" => 'Le type technique ' . $typeCircuit . ' est présent sur le parapheur',
            "okko" => 'ok'
        ];
        $circuits = $Parafwebservice->getListeSousTypesWebservice($typeCircuit);
        $time_end = microtime(true);
        $time = round($time_end - $time_start, 2);

        if (empty($circuits['soustype'])) {
            $resultats[] = ["libelle" => 'liste de circuit du iparapheur vide', "okko" => 'ko'];
            return $resultats;
        }
        $resultats[] = [
            "libelle" => count($circuits['soustype'])
                . " circuit(s) du iparapheur récupéré(s) en $time secondes : "
                . implode(', ', $circuits['soustype']), "okko" => 'ok'
        ];

        return $resultats;
    }

    /**
     *
     */
    public static function getPastellVersion()
    {
        try {
            $resultats = [];
            $resultats[] = ["libelle" => Configure::read('PASTELL_HOST'), "okko" => 'info'];

            $httpClient = GuzzleClient::createWithConfig(['verify' => false]);
            $client = Client::createWithHttpClient($httpClient);
            $client->setUrl(Configure::read('PASTELL_HOST'));
            $client->authenticate(Configure::read("PASTELL_LOGIN"), Configure::read("PASTELL_PWD"));
            $reponse = (new PastellVersion($client))->show();
        } catch (Exception $e) {
            $reponse = null;
        }

        if (is_array($reponse) and isset($reponse['version'])) {
            $resultats[] = ["libelle" => 'Version de pastell : ' . $reponse['version'], "okko" => 'ok'];
        } else {
            $resultats[] = ["libelle" => 'Impossible de communiquer avec Pastell : ', "okko" => 'ko'];
        }
        return $resultats;
    }

    public static function checkFichierIni($fichier, $key = null)
    {
        if (!file_exists(DATA . Configure::read('Config.tenantName')
            . DS . 'config' . DS . $fichier)) {
            $resultMessage = "$fichier non trouvé : renommer le fichier $fichier.default en $fichier";
            return ["okko" => 'ko', "libelle" => $resultMessage];
        }
        $resultMessage = $fichier;
        return ["okko" => 'ok', "libelle" => $resultMessage];
    }

    public static function checkBinaire($binaire, $key)
    {
        $output = exec('whereis -b ' . $binaire['executable']);
        if ($output == $binaire['executable'] . ":") {
            return ["okko" => 'ko', "libelle" => $key . ' (non trouvé)'];
        }
        unset($output);

        $commande = $binaire['executable'] . ' ' . $binaire['option_version'] . ' 2>&1';
        exec($commande, $output);
        $output = implode($output);
        if (strpos($output, $binaire['attendu']) === false) {
            return ["okko" => 'ko', "libelle" => $key . ' (mauvaise version)', 'message' => $output];
        }
        return ["okko" => 'ok', "libelle" => $key];
    }

    public static function apacheCheckModule($module, $key)
    {
        $modules = apache_get_modules();
        $okko = in_array($module, $modules, true) ? 'ok' : 'ko';
        return ["okko" => $okko, "libelle" => $module];
    }

    public static function phpCheckExtension($extension, $key)
    {
        $okko = extension_loaded($extension) ? 'ok' : 'ko';
        return ["okko" => $okko, "libelle" => $extension];
    }

    public static function phpCheckLibrairie($librairie, $key)
    {
        require_once $librairie['require'];
        $output = class_exists($librairie['class'], false);
        $okko = !empty($output) ? 'ok' : 'ko';
        return ["okko" => $okko, "libelle" => $key];
    }

    public static function checkConditions($liste, $function)
    {
        $resultatsTests = [];
        $message = '';

        foreach ($liste as $key => $val) {
            $message = '';
            $resultatTest = self::$function($val, $key);
            if (array_key_exists('message', $resultatTest)) {
                $message = $resultatTest["message"];
            }
            if (gettype($val) == "array") {
                $libelle = implode("_", $val);
            } else {
                $libelle = strval($val);
            }
            $resultatsTests[$libelle] = [
                'libelle' => $resultatTest["libelle"],
                'okko' => $resultatTest["okko"],
                'message' => $message
            ];
        }
        return $resultatsTests;
    }

    /**
     *
     */
    public static function checkGED()
    {
        $resultats = [];
        $urlGED = Configure::read("CMIS_HOST");
        $repoGED = Configure::read("CMIS_REPO");
        $resultats[] = ['libelle' => "URL de la GED : " . $urlGED, 'okko' => 'info'];
        $resultats[] = ['libelle' => "Dossier distant : " . $repoGED, 'okko' => 'info'];
        return $resultats;
    }

    /**
     *
     */
    public static function checkIdelibre()
    {
        $resultats = [];
        $url = Configure::read('IDELIBRE_HOST') . '/Api300/ping';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $reponse = trim(curl_exec($ch));

        if (curl_errno($ch)) {
            $resultats[] = ['libelle' => curl_error($ch), 'okko' => 'ko'];
            curl_close($ch);
            return $resultats;
        }
        curl_close($ch);

        if ($reponse != 'ping') {
            $resultats[] = ['libelle' => Configure::read('IDELIBRE_HOST') . ' ne répond pas', 'okko' => 'ko'];
            return $resultats;
        }
        $resultats[] = ['libelle' => 'Accès ok à ' . Configure::read('IDELIBRE_HOST'), 'okko' => 'ok'];

        return $resultats;
    }

    /**
     *
     */
    public static function checkIdelibreConnexion()
    {
        $resultats = [];
        $url = Configure::read('IDELIBRE_HOST') . '/Api300/check';
        $data = [
           'username' => Configure::read('IDELIBRE_LOGIN'),
           'password' => Configure::read('IDELIBRE_PWD'),
           'conn' => Configure::read('IDELIBRE_CONN'),
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        if (Configure::read('IDELIBRE_USEPROXY')) {
            curl_setopt($ch, CURLOPT_PROXY, Configure::read('IDELIBRE_PROXYHOST'));
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_VERBOSE, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $reponse = trim(curl_exec($ch));

        if (curl_errno($ch)) {
            $resultats[] = ['libelle' => curl_error($ch), 'okko' => 'ko'];
            curl_close($ch);
            return $resultats;
        }
        curl_close($ch);

        //self::d(var_export($reponse,true), 'info');
        //self::d(var_export(strpos($reponse, 'success'),true), 'info');
        if ($reponse != 'success') {
            $resultats[] = ['libelle' => 'Connexion à idelibre ko', 'okko' => 'ko'];
            return $resultats;
        }
        $resultats[] = ['libelle' => 'Connexion à idelibre ok', 'okko' => 'ok'];
        return $resultats;
    }

    /**
     *
     */
    public static function checkIdelibreVersion()
    {
        $resultats = [];
        $url = Configure::read('IDELIBRE_HOST') . '/Api300/version';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $reponse = trim(curl_exec($ch));

        if (curl_errno($ch)) {
            $resultats[] = ['libelle' => 'Version indisponible', 'okko' => 'ko'];
            curl_close($ch);
            return $resultats;
        }

        $info = curl_getinfo($ch);
        switch ($info['http_code']) {
            case '200':
                $resultats[] = ['libelle' => 'Version : ' . $reponse, 'okko' => 'ok'];
                break;
            default:
                $resultats[] = ['libelle' => 'Version indisponible', 'okko' => 'ko'];
                break;
        }

        curl_close($ch);

        return $resultats;
    }

    public static function checkLDAPUser()
    {
        try {
            $oLdap = ClassRegistry::init('LdapManager.LdapUser');
            $user = Configure::read('LdapManager.Ldap.login');
            $password = Configure::read('LdapManager.Ldap.password');
            if (!empty($oLdap)) {
                $retour = $oLdap->getDataSource()->auth($user, $password);
                if ($retour === true) {
                    return ["libelle" => "Authentifié avec " . $user, "okko" => "ok"];
                }
                /* $logs = file(LOGS.'error.log');
                  $last = $logs[count($logs)-1];
                  $message = trim(preg_replace('/^.*Error: (.*)$/', '\1', $last)); */
                return ["libelle" => "Impossible de s\'authentifier avec " . $user,
                    "okko" => "ko",
                    "message" => $retour
                ];
            }
            //debug($oLdap->getDataSource()->lastError());
            return ["libelle" => "Impossible d\'initialiser le LDAP", "okko" => "ko"];
        } catch (Exception $e) {
            CakeLog::error($e->getLine() . $e->getMessage());
        }
    }

    /**
     *
     */
    public static function checkLDAP()
    {
        try {
            $ldap_group = ClassRegistry::init('LdapManager.LdapGroup');
            // affichage des infos LDAP
            //Liste groups ldap
            $ldapGroups = $ldap_group->find(
                'count',
                [
                'fields' => [$ldap_group->getKeyWord('cn')],
                'conditions' => [
                   $ldap_group->getKeyWord('objectClass') => $ldap_group->getKeyWord('groupOfNames'),
                ],
                'targetDn'=> Configure::read("LdapManager.Ldap.filter"),
                'recursive' => -1,
                'order' => $ldap_group->getKeyWord('cn')
                   ]
            );

            $ldapUsers = $ldap_group->find(
                'count',
                [
                    'fields' => [$ldap_group->getKeyWord('cn')],
                    'conditions' => [
                        $ldap_group->getKeyWord('objectClass') => $ldap_group->getKeyWord('inetOrgPerson'),
                    ],
                    'targetDn'=> Configure::read("LdapManager.Ldap.filter"),
                    'recursive' => -1,
                    'order' => $ldap_group->getKeyWord('cn')
                ]
            );

            return [
                       ['libelle' => __('Nombre de groupes trouvés : %s', $ldapGroups > 0 ? $ldapGroups : '0') ,
                           'okko' => $ldapGroups > 0 ? 'ok' : 'ko'],
                       ['libelle' => __('Nombre d\'utilisateurs trouvés : %s', $ldapUsers > 0 ? $ldapUsers : '0'),
                           'okko' => $ldapUsers > 0 ? 'ok' : 'ko'],
                   ];
        } catch (Exception $e) {
            return ['libelle' => 'Connexion au serveur LDAP : échec (' . $e->getMessage() . " )", 'okko' => 'ko'];
        }
    }

    /**
     * Vérifie l'intégrité du schéma de la base de données
     *
     * @param type $plugin
     * @throws Exception
     */
    public static function checkVersionSchema()
    {
        $versionFile = file(ROOT . DS . 'VERSION.txt');
        $versionPHPAttendue = trim(array_pop($versionFile));
        if (empty($versionPHPAttendue)) {
            return ['libelle' => 'Version de webdelib : déclaration de Version non trouvée', 'okko' => 'ko'];
        }
        $infos = [];
        $db = ConnectionManager::getDataSource(Configure::read('Config.database'));

        // version de la base de données
        $result = $db
            ->execute('
                SELECT "version" FROM "versions" where subject=\'webdelib\' ORDER BY created DESC, version DESC
            ')
            ->fetch();
        $version = '';
        if (!empty($result['version'])) {
            $version = $result['version'];
        }
        return [[
            'libelle' => "Version de schéma de base de donnée (attendue $versionPHPAttendue) : $version",
            'okko' => $version === $versionPHPAttendue ? 'ok': 'ko'
        ]];
    }

    /**
     * Vérifie l'intégrité du schéma de la base de données
     *
     * @param type $plugin
     * @throws Exception
     */
    public static function checkSchema($plugin = '')
    {
        $command = ROOT . DS . 'app' . DS . 'Console'
            . DS . 'cake schema update ' . (!empty($plugin) ? '--plugin ' . $plugin : '')
            . ' --dry';
        try {
            $retour = exec($command, $message);
            //self::t('h5', "Test d'intégrité du schéma " . $plugin);
            if ($retour == 'Schema is up to date.') {
                return [["libelle" => 'Schéma de la base de données : Valide !', "okko" => 'ok']];
            } else {
                //Supprime les 12 premières et les deux dernières lignes du tableau
                //constituant le message de retour de l'exécution (infos inutiles)
                self::removeFromArray($message, 12, 2);
                throw new Exception(implode('<br />', $message));
            }
        } catch (Exception $e) {
            return [[
                "libelle" => 'Schéma de la base de données : Problème d\'intégrité !!',
                'Afficher/Masquer les différences..', "message" => $e->getMessage(),
                "okko" => 'ko'
            ]];
        }
    }

    /**
     * @param $array    le tableau à redimmensionner
     * @param $begin    nouvel indice de début de tableau (nombre d'éléments à supprimer en début du tableau)
     * @param $end      nouvel indice de fin de tableau (nombre d'éléments à supprimer en fin du tableau)
     */
    public static function removeFromArray(&$array, $begin, $end)
    {
        for ($i = 0; $i < $begin; $i++) {
            array_shift($array);
        }
        for ($i = 0; $i < $end; $i++) {
            array_pop($array);
        }
    }

    /**
     *
     */
    public static function permissionsRepertoires()
    {
        $permissions = [];
        $permissions[] = ["libelle" => 'Propriétaire du script courant : ' . get_current_user(), "okko" => 'info'];
        $dirConfig = ROOT . DS . 'app' . DS . 'Config';
        $dirCongifIsWritable = is_writable($dirConfig);
        $dirTmp = ROOT . DS . 'app' . DS . 'tmp';
        $dirTmpIsWritable = is_writable($dirTmp);
        $permissions[] = [
            "libelle" => __('Répertoire configuration de l\'application est accessible en écriture : (%s)', $dirConfig),
            "okko" => $dirCongifIsWritable == true ? 'ok' : 'ko'
        ];
        $permissions[] = [
            "libelle" => __('Répertoire temporaire de l\'application est accessible en écriture : (%s)', $dirTmp),
            "okko" => $dirTmpIsWritable == true ? 'ok' : 'ko'
        ];
        return $permissions;
        //self::t('h5', "CakePHP");
        //self::verifConsoleCakePhp();
    }

    /**
     *
     * @global type $appli_path
     */
    public static function consoleCakePhp()
    {
        // fichier exécutable de la console
        $consoleFileUri = ROOT . DS . 'app' . DS .  'Console' . DS . 'cake';

        if (file_exists($consoleFileUri)) {
            if (is_executable($consoleFileUri)) {
                return [[
                    "libelle" => "Console de CakePHP :"
                        ." le fichier exécutable de la console CakePHP '$consoleFileUri' a les droits en exécution",
                    "okko" => 'ok'
                ]];
            } else {
                return [[
                    "libelle" => "Console de CakePHP :"
                        ." le fichier exécutable de la console CakePHP '$consoleFileUri' n'a pas"
                        ." les droits en exécution",
                    "okko" => 'ko'
                ]];
            }
        } else {
            return [[
                "libelle" => 'Console de CakePHP : le fichier exécutable de la console CakePHP non trouvé',
                "okko" => 'ko'
            ]
            ];
        }
    }
}
