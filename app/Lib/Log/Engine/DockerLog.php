<?php

App::uses('ConsoleLog', 'Log/Engine');

class DockerLog extends ConsoleLog
{
    /**
     * Implements writing to console.
     *
     * @param string $type The type of log you are making.
     * @param string $message The message you want to log.
     * @return bool success of write.
     */
    public function write($type, $message)
    {
        $tenantName = Configure::read('Config.tenantName');
        $output = date('Y-m-d H:i:s')
            . ' [' . $tenantName . '] '
            . ucfirst($type)
            . ': ' . $message . "\n";
        return $this->_output->write(
            sprintf(
                '<%s>%s</%s>',
                $type,
                $output,
                $type
            ),
            false
        );
    }
}
