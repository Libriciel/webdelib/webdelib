<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [MethodCurl description]
 */
class MethodCurl
{
    protected string $baseUrl = '';
    protected array $optionsWebService = [
        "CurlOptions"=>[],
        "GetParameters" => [],
        "PostParameters"=>[]
    ];

    /**
     * @param string $baseUrl URL de base du Webservice
     * @param array $options Options dans un tableau contenant les clés "CurlOptions",
     * "GetParameters", "PostParameters".
     */
    public function __construct($baseUrl, array $options)
    {
        $this->baseUrl = $baseUrl;
        foreach (array_keys($this->optionsWebService) as $key) {
            if (!empty($options[$key])) {
                $this->optionsWebService[$key]=$options[$key];
            }
        }
    }

    /**
     * @return array Tableau contenant les clés "CurlOptions", "GetParameters", "PostParameters"
     */
    public function getoptionsWebService()
    {
        return $this->optionsWebService;
    }

    /**
     * @param array $array
     */

    public function setOptionsWebservice(array $array)
    {
        foreach ($array as $typeOption => $tableauOptions) {
            foreach ($tableauOptions as $cle => $valeur) {
                $this->optionsWebService[$typeOption][$cle]=$valeur;
            }
        }
    }

    /**
     *  Rajoute les paramètres spécifiques à la requête
     * @param string $url chemin de la requête, ser rajoute à $baseURL
     * @param array $optionsRequete Tableau des paramètrespécifiques à la requête, contenant
     * les clés "CurlOptions", "GetParameters", "PostParameters"
     * @return array ableau contenant les clés "CurlOptions", "GetParameters", "PostParameters"
     */
    public function initOptionsRequete($url, array $optionsRequete = [])
    {
        $options=$this->optionsWebService;
        $optionsConstruites=[];
        foreach (array_keys($options) as $key) {
            if (empty($optionsRequete[$key])===false) {
                if (is_array($optionsRequete[$key])) {
                    $options[$key]=$optionsRequete[$key]+$options[$key];
                } else {
                    $options[$key]=$optionsRequete[$key];
                }
            }
        }
        $curlOptions = $options["CurlOptions"];

        $getParametres =[];
        if (!empty($options["GetParameters"])) {
            $getParametres=$options["GetParameters"];
        }

        $url= $this->constructURL($url, $getParametres);
        $curlOptions[CURLOPT_URL]=$url;

        $optionsConstruites['CurlOptions']=$curlOptions;

        if (!empty($options["PostParameters"])) {
            $optionsConstruites['PostParameters'] = $options["PostParameters"];
        }
        return $optionsConstruites;
    }

    /**
     * Construit l'URL complètes en agrégeant l'URL de base, le chemin de la requête et les paramètres GET
     * @param type $chemin
     * @param type $parametres
     * @return type
     */
    protected function constructURL($chemin, $parametres = null)
    {
        $url =  $this->baseUrl. $chemin;
        if (!empty($parametres)) {
            $url .= (stripos($url, '?')!==false ? '&' : '?') . http_build_query($parametres);
        }
        return trim($url);
    }

    /**
     * Effectue la requête sur le webservice configuré avec les options contenues dans $options
     * @param string $url
     * @param array $options Tableau des paramètres spécifiques à la requête,
     * contenant les clés "CurlOptions", "GetParameters", "PostParameters"
     * @return type Résultat de la requête
     */
    public function doRequest($url, array $options = [], $fileTransfert = false)
    {
        $optionsRequete=$this->initOptionsRequete($url, $options);
        $ch = curl_init();
        if (!$ch) {
            throw new Exception("Couldn't initialize a cURL handle");
        }
        curl_setopt_array($ch, $optionsRequete['CurlOptions']);

        if ($fileTransfert) {
            $folder = new Folder(AppTools::newTmpDir(TMP . 'files' . DS . 'Curl'), true, 0777);
            $file = new File($folder->path . DS . 'WD_CURL_DOC', true, 0777);
            $fp = fopen($file->path, 'w');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FILE, $fp);
        }
        if (!empty($optionsRequete['PostParameters'])) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $optionsRequete['PostParameters']);
        }

        $retourCurl = curl_exec($ch);

        $erreurCurl='';
        if ($retourCurl == false) {
            $erreurCurl = 'Erreur Curl : ' . curl_error($ch);
        }
        if ($fileTransfert) {
            fclose($fp);
            $resultat = $file->read();
            $folder->delete();
        } else {
            $resultat = $retourCurl;
        }

        return $resultat;
    }
}
