<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('IparapheurComponent', 'Controller/Component');
App::uses('ConnecteurLib', 'Lib');
App::uses('AppTools', 'Lib');

/**
 * Interface controllant les actions de signature électronique
 * Utilise les connecteurs/components PastellComponent et IparapheurComponent.
 *
 *  @version 5.0.0
 */
class Signature extends ConnecteurLib
{
    /**
     * @var Composant IparapheurComponent
     */
    private $Iparapheur;

    /**
     * @var string type technique dans le parapheur
     */
    private $parapheurType;
    private $parapheurMode;

    /**
     * @var string visibilité du document parapheur
     */
    private $visibility;

    /**
     * @var Composant PastellComponent
     */
    protected $Pastell;

    /**
     * Appelée lors de l'initialisation de la librairie
     * Charge le bon protocol de signature et initialise le composant correspondant.
     */
    public function __construct()
    {
        parent::__construct(Configure::read('PARAPHEUR'));

        if (Configure::read('USE_PARAPHEUR')) {
            if (!$this->getType()) {
                throw new Exception('Aucun parapheur désigné');
            }
            if (!Configure::read('USE_'.$this->getType())) {
                throw new Exception(
                    "Le connecteur parapheur désigné n'est pas activé : USE_".$this->getType()
                );
            }
            if ('IPARAPHEUR' == $this->getType()) {
                $this->Iparapheur = new IparapheurComponent($this->collection);
                $this->visibility = Configure::read('IPARAPHEUR_VISIBILITY');
            }
            $this->parapheurType = Configure::read('IPARAPHEUR_TYPE');
            $this->parapheurMode = Configure::read('PASTELL_PARAPHEUR_MODE');
        } else {
            $this->active = false;
        }
    }

    /**
     * @param array      $delib
     * @param int|string $circuit_id
     * @param array      $annexes    (content, filename, mimetype)
     *
     * @return bool|int false si echec sinon identifiant iparapheur
     */
    public function sendIparapheur($delib, $circuit_id, $document, $annexes = [])
    {
        if (is_numeric($circuit_id)) {
            $circuits = $this->listCircuitsIparapheur();
            $libelleSousType = $circuits[$circuit_id];
        } else {
            $libelleSousType = $circuit_id;
        }
        $targetName = $delib['Deliberation']['objet_delib'];
        $date_limite = null;

        $ret = $this->Iparapheur->creerDossierWebservice(
            $targetName,
            $this->parapheurType,
            $libelleSousType,
            $this->visibility,
            $document,
            $annexes,
            $date_limite
        );

        if ('OK' == $ret['messageretour']['coderetour']) {
            return $ret['dossierID'];
        } else {
            CakeLog::error($ret['messageretour']['message'], 'parapheur');

            return false;
        }
    }

    /**
     * @param array      $acte
     * @param int|string $circuit_id
     * @param string     $document   contenu du fichier du document principal
     * @param array      $annexes    (content, filename)
     *
     * @return bool|int false si echec sinon identifiant pastell
     */
    public function sendPastell($acte, $circuitId, $actePDF, $annexes = [])
    {
        try {
            $acte[$this->Pastell->configField('envoi_signature')] = true;
            $idD = parent::createPastell($acte, $actePDF, $annexes);

            if (is_numeric($circuitId)) {
                $circuits = $this->Pastell->getInfosField(
                    $this->idE,
                    $idD,
                    $this->Pastell->configField('iparapheur_sous_type')
                );
                $sousType = $circuits[$circuitId];
            } else {
                $sousType = $circuitId;
            }
            $this->Pastell->selectCircuit($this->idE, $idD, $sousType);

            if ($this->Pastell->action($this->idE, $idD, $this->Pastell->configAction('send-iparapheur'))) {
                return $idD;
            }
        } catch (Exception $e) {
            if (!empty($idD)) {
                $this->Pastell->delete($this->idE, $idD);
            }
            throw new CakeException($e->getMessage());
        }

        return false;
    }

    /**
     * @param array      $acte
     * @param int|string $circuit_id
     * @param string     $document   contenu du fichier du document principal
     * @param array      $annexes    (content, filename)
     *
     * @return bool|int false si echec sinon identifiant pastell
     */
    public function sendVisaPastell($acte, $circuit_id, $document, $annexes = [])
    {
        try {
            $id_d = $this->createVisaPastell($acte['Deliberation']['id']);
            if ($id_d===false) {
                throw new Exception('Error sendVisaPastell::createVisaPastell');
            }

            // Modification des données de transmissions
            $this->Pastell->updateVisa($this->idE, $id_d, $acte, $document, $annexes = []);

            if (is_numeric($circuit_id)) {
                $circuits = $this->Pastell->getInfosField($this->idE, $id_d, 'iparapheur_sous_type');
                $sousType = $circuits[$circuit_id];
            } else {
                $sousType = $circuit_id;
            }
            $this->Pastell->selectCircuit($this->idE, $id_d, $sousType);

            if ($this->Pastell->action($this->idE, $id_d, 'orientation')) {
                return $id_d;
            } else {
                throw new Exception('Error sendVisaPastell::action');
            }
        } catch (Exception $e) {
            $this->Pastell->action($this->idE, $id_d, 'supression');
            Cakelog::error($e->getMessage(), 'connector');
            throw new Exception($e->getMessage());
        }

        return false;
    }

    /**
     * @param int  $id_d
     * @param bool $get_details
     *
     * @return array(
     *                'action' => 'rejet-iparapheur',
     *                'message' => 'Le document a été rejeté dans le parapheur :
     * 23/01/2014 16:54:52 : [RejetSignataire] vu',
     *                'date' => '2014-01-23 16:55:07'
     *                )
     */
    public function getLastActionPastell($id_d, $get_details = false)
    {
        $details = $this->Pastell->getLastAction($this->idE, $id_d);
        if ($get_details) {
            return $details['last_action'];
        }
        $last_action = $details['last_action']['action'];

        return $last_action;
    }

    /**
     * @param int $id_d
     *
     * @return string flux du fichier bordereau
     */
    public function getBordereauPastell($id_d)
    {
        if ('FAST' === $this->parapheurMode) {
            return null;
        }
        return $this->Pastell->getFile($this->idE, $id_d, $this->config['field']['document_signe']);
    }

    /**
     * @param int $id_d
     *
     * @return string flux du fichier signature (zip)
     */
    public function getSignaturePastell($id_d)
    {
        return $this->Pastell->getFile($this->idE, $id_d, $this->config['field']['signature']);
    }

    /**
     * @param int $id_d
     *
     * @return string flux du fichier signature (zip)
     */
    public function getDocumentPastell($id_d)
    {
        return $this->Pastell->getFile($this->idE, $id_d, $this->config['field']['arrete']);
    }

    /**
     * @param int $id_d
     *
     * @return string flux du fichier signé (pdf)
     */
    public function getDocumentSignePastell($id_d)
    {
        return $this->getSignaturePastell($id_d);
    }

    /**
     * @param int $id_d
     *
     * @return array
     */
    public function getDetailsPastell($id_d)
    {
        return $this->Pastell->detailDocument($this->idE, $id_d);
    }

    /**
     * @param int $id_d
     *
     * @return bool
     */
    public function isSignePastell($id_d)
    {
        $details = $this->Pastell->detailDocument($this->idE, $id_d);

        return !empty($details['data'][$this->config['field']['has_signature']]);
    }

    /**
     * @param int $id
     */
    public function deleteIparapheur($id)
    {
        $this->Iparapheur->effacerDossierRejeteWebservice($id);
    }

    /**
     * @param int $id
     */
    public function archiveIparapheur($id)
    {
        $this->Iparapheur->archiverDossierWebservice($id, 'EFFACER');
    }

    /**
     * @param int $id_d
     *
     * @return array
     */
    public function deletePastell($id_d)
    {
        return $this->Pastell->action($this->idE, $id_d, $this->config['action']['supression']);
    }

    /**
     * @see listCircuits()
     *
     * @return array
     *
     * @throws Exception
     */
    public function listCircuitsIparapheur()
    {
        try {
            $resp = $this->Iparapheur->getListeSousTypesWebservice($this->parapheurType);
            if (array_key_exists('soustype', $resp)) {
                return $resp['soustype'];
            } else {
                throw new Exception($resp['messageretour']['message']);
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @see listVisaCircuitsIparapheur()
     *
     * @return array
     */
    public function listVisaCircuitsIparapheur()
    {
        return $this->listCircuitsIparapheur();
    }

    /**
     * @see listCircuits()
     *
     * @return array
     */
    public function listCircuitsPastell()
    {
        return $this->Pastell->getCircuits($this->idE);
    }

    /**
     * @see listCircuits()
     *
     * @return array
     */
    public function listVisaCircuitsPastell()
    {
        return $this->Pastell->getVisaCircuits($this->idE);
    }



    /**
     * @return array
     */
    public function printCircuits()
    {
        $circuits = [];
        try {
            $circuits_parapheur = $this->listCircuits();
            if (!empty($circuits_parapheur)) {
                $circuits['Parapheur'] = $circuits_parapheur;
            }
        } catch (\Throwable $e) {
            $message = __('Une erreur s\'est produite lors de la récupération des circuits.');
            CakeLog::error($message);
            throw new Exception($message, 0, $e);
        }

        return $circuits;
    }

    /**
     * @see updateAll()
     *
     * @return array
     */
    public function updateAllIparapheur()
    {
        return $this->Deliberation->majActesParapheur();
    }

    /**
     * @see updateAll()
     *
     * @return array
     */
    public function updateAllPastell()
    {
        return $this->Deliberation->majSignaturesPastell();
    }

    /**
     * Retourne l'historique parapheur
     * @param int $documentId identifiant du dossier pastell
     * @return array
     *
     * @version 6.0.6
     */
    public function getParapheurHistorique($documentId)
    {
        try {
            $this->Pastell = new PastellComponent($this->collection);
            $ParapheurHistoryFile = $this->Pastell->execute(
                'v2/entite/' . $this->idE . '/document/' . $documentId .'/file/iparapheur_historique'
            );

            $filePathClassification = $this->saveXmlFile($ParapheurHistoryFile);

            $xmlObject = Xml::build($filePathClassification);
            $xmlArray = Xml::toArray($xmlObject);
        } catch (XmlException $e) {
            return false;
        } catch (Exception $e) {
            throw new Exception(
                'Erreur de récupération de l\'historique parapheur pour le document ' . $documentId . '.'
            );
        }


        return $xmlArray;
    }

    /**
     * Retourne la date de signature parapheur
     * @param int $documentId identifiant du dossier pastell
     * @return string
     * @version 7.0.0
     */
    public function getSignatureDateFromParapheurHistoric($pastellDocumentId)
    {
        $parapheurHistoric = $this->getParapheurHistorique($pastellDocumentId);

        if (!is_array($parapheurHistoric)) {
            return false;
        }

        if ('FAST' === $this->parapheurMode) {
            $simpleHistoric = Hash::extract(
                $parapheurHistoric,
                "iparapheur_historique.iparapheur_historique.{n}[stateName=/^Signé/]"
            );
            $keyOfDate = array_search('Signé', array_column($simpleHistoric, 'stateName'));

            return strtotime($simpleHistoric[$keyOfDate]['date']);
        }

        $simpleHistoric = Hash::extract(
            $parapheurHistoric,
            "iparapheur_historique.LogDossier.LogDossier.{n}[status=/^Signe/]"
        );
        $keyOfDate = array_search('Signe', array_column($simpleHistoric, 'status'));

        return $simpleHistoric[$keyOfDate]['timestamp'];
    }

    public function saveXmlFile($reponse, $fileName = "historique")
    {
        $sucess = true;
        $xml = simplexml_load_string($reponse);
        if ($xml === false && $sucess) {
            return false;
        }
        $dom_xml = dom_import_simplexml($xml);
        if ($xml === false && $sucess) {
            return false;
        }
        $dom = new DOMDocument('1.0', 'UTF-8');
        $dom_xml = $dom->importNode($dom_xml, true);
        $dom->appendChild($dom_xml);

        $folderTmp = new Folder(
            AppTools::newTmpDir(TMP . 'files' . DS . 'tdt' . DS),
            true,
            0777
        );
        $file = new File($folderTmp->pwd() . DS . "$fileName.xml", true);

        $file->delete();
        $file->create();
        $dom->saveXML();
        if ($file->writable()) {
            $success=true;
            $file->write($dom->saveXML());
        } else {
            $sucess = false;
        }
        $file->close();
        return $file->pwd();
    }
}
