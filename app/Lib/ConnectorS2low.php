<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('MethodCurl', 'Lib');

/**
 * [ConnectorS2low description]
 * @version 5.1.0
 */
class ConnectorS2low
{
    public const RETOUR_ERREUR = '-1';
    public const RETOUR_ANNULE = '0';
    public const RETOUR_POSTE = '1';
    public const RETOUR_ATTENTE = '2';
    public const RETOUR_TRANSMIS = '3';
    public const RETOUR_ACQUITTEMENT = '4';
    public const RETOUR_VALIDE = '5';
    public const RETOUR_REFUSE = '6';
    public const RETOUR_DOC_RECU = '7';
    public const RETOUR_ACQUITTEMENT_ENVOYE = '8';
    public const RETOUR_DOC_ENVOYE = '9';
    public const RETOUR_REFUS_ENVOI = '10';
    public const RETOUR_ACQUITTEMENT_DOC_RECU = '11';
    public const RETOUR_ENVOYE_SAE = '12';
    public const RETOUR_ARCHIVE_SAE = '13';
    public const RETOUR_ERREUR_ARCHIVAGE = '14';
    public const RETOUR_RECU_SAE = '15';
    public const RETOUR_DETRUITE = '16';
    public const RETOUR_ATTENTE_POSTEE = '17';
    public const RETOUR_ATTENTE_SIGNEE = '18';
    public const RETOUR_ATTENTE_TRANSMISSION = '19';
    public const RETOUR_ERREUR_ENVOI_SAE = '20';
    public const RETOUR_DOCUMENT_REFUSE = '21';

    protected $curl;
    /**
       *  "Lazy loading" de la classe WebdelibCurl.
       * Effectue la requête pour récupérer le nounce si l'authentification se fait par certificat + login
       * @version 5.1.4
       * @since 5.1.0
       * @return WebdelibCurl
       */
    public function curl()
    {
        if (null === $this->curl) {
            $options["CurlOptions"] = [
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSLCERT => Configure::read('S2LOW_PEM'),
                CURLOPT_SSLCERTPASSWD => Configure::read('S2LOW_CERTPWD'),
                CURLOPT_SSLKEY => Configure::read('S2LOW_SSLKEY')
            ];

            if (Configure::read('S2LOW_USEPROXY')) {
                $options["CurlOptions"][CURLOPT_PROXY] = Configure::read('S2LOW_PROXYHOST');
            }

            if (Configure::read('S2LOW_USELOGIN')) {
                $options["CurlOptions"][CURLOPT_USERPWD] =
                    Configure::read('S2LOW_LOGIN') . ':' . Configure::read('S2LOW_PWD');
            }

            $this->curl = new MethodCurl(Configure::read('S2LOW_HOST'), $options);
        }
        return $this->curl;
    }

    /**
     * @version 5.1.0
     * @param type $acte
     * @return type
     */
    public function send($acte)
    {
        //mise en forme de la variable $acte
        foreach ($acte as $key => &$champ) {
            if (is_array($champ) && !empty($champ['pwd'])) {
                $champ = curl_file_create($champ['pwd'], $champ['typemime'], $champ['filename']);
            }
        }
        //Préparation des paramètres de la requête
        $result=$this->curl()->doRequest(
            '/modules/actes/actes_transac_create.php',
            ["CurlOptions"=>[CURLOPT_VERBOSE=>true,CURLOPT_RETURNTRANSFER=>true],
                                 "PostParameters"=> $acte]
        );
        CakeLog::write('debug', 'RetourS2low : '.var_export($result, true));
        return $result;
    }


    /**
      * @version 5.1.4
      * @since 5.1.0
      * @return boolean
      */
    public function testLogin()
    {
        return $this->curl()->doRequest('/api/test-connexion.php');
    }
    /**
      * @version 5.1.4
      * @since 5.1.0
      * @return boolean
      */
    public function getClassification()
    {
        return $this->curl()->doRequest(
            '/modules/actes/actes_classification_fetch.php',
            ["GetParameters" =>['api' => '1']]
        );
    }
    /**
      * @version 5.1.4
      * @since 5.1.0
      * @param type $tdt_id
      * @return type
      */
    public function getFluxRetour($tdt_id)
    {
        return $this->curl()->doRequest(
            "/modules/actes/actes_transac_get_status.php",
            ["GetParameters" =>["transaction"=>$tdt_id],
            "CurlOptions" =>[CURLOPT_VERBOSE=>false]]
        );
    }

    public function getActeTampon($tdt_id)
    {
        return $this->getActeDocument($tdt_id, 'acte', true);
    }

    public function getActeAnnexesTampon($tdt_id, $ordre)
    {
        return $this->getActeDocument($tdt_id, 'annexe', true, $ordre);
    }

    public function getCourrier($tdt_id)
    {
        return $this->getActeDocument($tdt_id, 'courrier');
    }

    /**
      * @param type $file
      * @param type $tampon
      * @return type
      */
    public function getActeDocumentTampon($file, $tampon)
    {
        return $this->curl()->doRequest(
            '/modules/actes/actes_download_file.php',
            [
                'GetParameters' =>[
                    'file'=>$file,
                    'tampon'=>$tampon
                ],
                'CurlOptions' => [
                    CURLOPT_RETURNTRANSFER=>true,
                    CURLOPT_FOLLOWLOCATION=>true
                ]
            ]
        );
    }

    /**
     * @version 5.1.0
     * @param type $tdt_id
     * @return type
     */
    public function getActeBordereau($tdt_id)
    {
        $curl_return = $this->curl()->doRequest(
            "/modules/actes/actes_create_pdf.php",
            ["GetParameters" =>["trans_id"=>$tdt_id],
                                                    "CurlOptions"=>[CURLOPT_FOLLOWLOCATION => true]]
        );
        return $curl_return;
    }

    /**
     * @version 5.1.0
     * @param type $tdt_id
     * @return type
     */
    private function getActeDocument($tdt_id, $document = 'ArActe', $tampon = false, $ordre = null)
    {
        try {
            $json = $this->getActeDocuments($tdt_id);
            if ($json === false) {
                throw new Exception(__('getActeDocument'), 500);
            }
            $documents = json_decode($json, true);
            switch ($document) {
                case 'ArActe':
                    $file = $documents[0]['id'];
                    $tampon = $documents[0]['id'];
                    break;
                case 'acte':
                    $file = $documents[1]['id'];
                    break;
                case 'annexe':
                    $file = $documents[($ordre + 1)]['id'];
                    break;
                case 'courrier':
                    $file = $documents[0]['id'];
                    break;
                default:
                    throw new Exception(__('getActeDocument'), 500);
            }

            $curl_return = $this->curl()->doRequest(
                '/modules/actes/actes_download_file.php',
                ["GetParameters" =>['file'=>$file,'tampon'=>$tampon],
                    "CurlOptions"=>[CURLOPT_RETURNTRANSFER=>true,CURLOPT_FOLLOWLOCATION=>true]]
            );


            if ($curl_return === false) {
                throw new Exception(curl_error($curl));
            }
        } catch (Exception $e) {
            throw new Exception(__('getActeDocument'), 500);
        }

        return $curl_return;
    }

    /**
     * @version 5.1.0
     * @param type $tdt_id
     * @return type
     */
    public function getActeDocuments($tdt_id)
    {
        try {
            $curl_return = $this->curl()->doRequest(
                "/modules/actes/actes_transac_get_files_list.php?transaction=$tdt_id",
                ["CurlOptions"=>[CURLOPT_FOLLOWLOCATION=> true]]
            );
            if ($curl_return === false) {
                throw new Exception(curl_error($ch));
            }
        } catch (Exception $e) {
            return false;
        }

        return $curl_return;
    }

    /**
     * @version 5.1.0
     * @param type $mail_id
     * @return type
     */
    public function checkMailSec($mail_id)
    {
        $curl_return = $this->curl()->doRequest(
            "/modules/mail/api/detail-mail.php?id=$mail_id",
            ["CurlOptions"=>[CURLOPT_VERBOSE => false]]
        );

        return($curl_return);
    }

    /**
     * @version 5.1.0
     * @param type $tdt_id
     * @return type
     */
    public function getNewFlux($tdt_id)
    {
        return $this->curl()->doRequest("/modules/actes/actes_transac_get_document.php?id=$tdt_id");
    }

    /**
     * @param type $document_id
     * @return boolean
     * @throws Exception
     */
    public function getDocument($document_id)
    {
        try {
            $response = $this->curl()->doRequest(
                "/modules/actes/actes_transac_get_document.php?id=$document_id",
                ["CurlOptions"=>[CURLOPT_FOLLOWLOCATION => true],true]
            );

            if ($response === false) {
                throw new Exception("Réponse vide du connecteur");    //TODO  Modifier
                //$this->log(curl_error($ch), 's2low');
            }
            echo "document_id ".$document_id."\n";
        } catch (Exception $e) {
            return false;
        }
        return $response;
    }
}
