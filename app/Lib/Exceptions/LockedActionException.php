<?php
/**
 * Interface contrôlant les actions de versment GED
 * Utilise les connecteurs/components PastellComponent
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       webdelib v5.1
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 * @version     v5.1
 * @package     app.Lib.Exception
 *
 * @author      Mickael Pastor
 * @created     10 10 2017
 */
class LockedActionException extends CakeException
{
};
