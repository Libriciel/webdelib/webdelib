<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppTools', 'Lib');
App::uses('ConnecteurLib', 'Lib');
App::uses('File', 'Utility');

/**
 * Interface contrôlant les actions de versment SAE
 * Utilise les connecteurs/components PastellComponent et AsalaeComponent
 *
 * @package App.Lib.Sae
 * @version 5.1.4
 * @since 4.3.0
 */
class Sae extends ConnecteurLib
{
    /**
     * @var Component AsalaeComponent
     */
    private $Asalae;

    /**
     * Appelée lors de l'initialisation de la librairie
     * Charge le bon protocol de signature et initialise le composant correspondant
     */
    public function __construct()
    {
        parent::__construct(Configure::read('SAE'));

        if (Configure::read('USE_PASTELL')) {
            $this->connecteur = Configure::read('SAE');
            $this->pastellTypeSaeDossierSeance = Configure::read('SAE_PASTELL_DOSSIER_TYPE_SEANCE');
        }
    }

    /**
     * Envoi le dossier dans pastell au SAE
     * @param $id_d
     * @param null $classification
     * @return array
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function sendPastell($acte, $document = null, $annexes = [])
    {
        if (empty($acte['Deliberation']['pastell_id'])) {
            $acte['Deliberation']['pastell_id'] = parent::createPastell($acte, $document, $annexes);
        }

        $this->Pastell->envoiSae($this->idE, $acte['Deliberation']['pastell_id']);

        return $this->Pastell->action(
            $this->idE,
            $acte['Deliberation']['pastell_id'],
            $this->Pastell->configAction('send-archive')
        );
    }

    /**
     * Envoi le dossier dans pastell au SAE
     * @param $id_d
     * @param null $classification
     * @return array|bool|int
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function sendDossierSeance(File &$zip, File &$xml)
    {
        try {
            $pastellId = $this->createDossierSeance($zip, $xml);

            $this->Pastell->envoiSae($this->idE, $pastellId);

            $this->Pastell->action($this->idE, $pastellId, $this->Pastell->configAction('orientation'));
        } catch (Exception $e) {
            if (!empty($idD)) {
                $this->Pastell->delete($this->idE, $idD);
            }
            throw new CakeException($e->getMessage(), $e->getCode());
        }

        return $pastellId;
    }

    /**
     * @param array $acte
     * @param array $annexes (content, filename)
     * @return bool|int false si echec sinon identifiant pastell
     * @throws Exception
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    protected function createDossierSeance(File &$zip, File &$xml)
    {
        try {
            $idD = $this->Pastell->createDocument($this->idE, $this->pastellTypeSaeDossierSeance);
            $this->Pastell->modifDocumentSaeDossierSeance($this->idE, $idD, $zip, $xml);

            return $idD;
        } catch (Exception $e) {
            if (!empty($idD)) {
                $this->Pastell->action($this->idE, $idD, $this->Pastell->configAction('supression'));
            }
            Cakelog::error($e->getMessage(), 'connector');

            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param int $id_d
     * @return array
     */
    public function getDetailsPastell($id_d)
    {
        return $this->Pastell->detailDocument($this->idE, $id_d);
    }

    /**
     * @param int $id_d
     *
     * @return string flux du fichier atr
     */
    public function getATR($id_d)
    {
        return $this->Pastell->getFile($this->idE, $id_d, $this->Pastell->configField('reply_sae'));
    }
}
