<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * Librairie regroupant des fonctions hors MVC
 * @version 5.1.0
 * @since 4.3.0
 */
class AppTools
{
    /**
     * Ajout ou soustrait un délai (xs:duration) à une date
     * @param string $date date
     * @param string $duration délai sous la forme xs:duration
     * @param string $format format de sortie de la date
     * @param string $operateur 'add' pour l'ajout et 'sub' pour la soustraction du délai
     * @return string résultat formaté ou null en cas d'erreur
     */
    public static function addSubDurationToDate($date, $duration, $format = 'Y-m-d', $operateur = 'add')
    {
        // initialisation
        $ret = null;
        try {
            $thisDate = new DateTime($date);
            $thisDuration = new DateInterval($duration);
            if ($operateur == 'add') {
                $thisDate->add($thisDuration);
            } elseif ($operateur == 'sub') {
                $thisDate->sub($thisDuration);
            }
            $ret = $thisDate->format($format);
        } catch (Exception $e) {
            debug('Fonction webdelib::addSubDurationToDate : ' . $e->getMessage());
        }
        return $ret;
    }

    /**
     * formate une date issue de la base de donnée
     * @param string $dateBD date issue de la lecture d'un enregistrement en base de données
     * @param string $format format de sortie utilisée par la fonction date()
     * @return string date mise en forme
     */
    public static function timeFormat($dateBD, $format = 'Y-m-d')
    {
        if (empty($dateBD)) {
            return '';
        }
        $dateTime = strtotime($dateBD);
        return date($format, $dateTime);
    }

    /**
     * formate une xs:duration sous forme litérale
     * @param string $duration délai sous la forme xs:duration
     * @return string délai mise en forme litérale
     */
    public static function durationToString($duration)
    {
        // initialisation
        $format = [];
        if (empty($duration)) {
            return '';
        }
        $thisDuration = new DateInterval($duration);
        $annees = $thisDuration->y;
        $mois = $thisDuration->m;
        $jours = $thisDuration->d;
        $heures = $thisDuration->h;
        $minutes = $thisDuration->i;
        $secondes = $thisDuration->s;
        if (!empty($annees)) {
            $format[] = $annees > 1 ? '%y ans' : '%y an';
        }
        if (!empty($mois)) {
            $format[] = '%m mois';
        }
        if (!empty($jours)) {
            $format[] = $jours > 1 ? '%d jours' : '%d jour';
        }
        if (!empty($heures)) {
            $format[] = $heures > 1 ? '%h heures' : '%h heure';
        }
        if (!empty($minutes)) {
            $format[] = $minutes > 1 ? '%i minutes' : '%i minute';
        }
        if (!empty($secondes)) {
            $format[] = $secondes > 1 ? '%s secondes' : '%s seconde';
        }

        if (empty($format)) {
            return '';
        } else {
            return $thisDuration->format(implode(', ', $format));
        }
    }

    /**
     * transforme une xs:duration sous forme de tableau
     * @param string $duration délai sous la forme xs:duration
     * @return array délai sous forme de tableau array('year', 'month', 'day', 'hour', 'minute', 'seconde')
     */
    public static function durationToArray($duration)
    {
        // initialisation
        $ret = ['year' => 0, 'month' => 0, 'day' => 0, 'hour' => 0, 'minute' => 0, 'seconde' => 0];

        if (!empty($duration)) {
            $thisDuration = new DateInterval($duration);
            $ret['year'] = $thisDuration->y;
            $ret['month'] = $thisDuration->m;
            $ret['day'] = $thisDuration->d;
            $ret['hour'] = $thisDuration->h;
            $ret['minute'] = $thisDuration->i;
            $ret['seconde'] = $thisDuration->s;
        }

        return $ret;
    }

    /**
     * transforme une tableau (array('year', 'month', ...)) en xs:duration ('D1Y...')
     * @param array $duration délai sous forme de tableau array('year', 'month', 'day', 'hour', 'minute', 'seconde')
     * @return string délai sous la forme xs:duration
     */
    public static function arrayToDuration($duration)
    {
        // initialisation
        $ret = $periode = $temps = '';
        $defaut = ['year' => 0, 'month' => 0, 'day' => 0, 'hour' => 0, 'minute' => 0, 'seconde' => 0];

        if (empty($duration) || !is_array($duration)) {
            return '';
        }

        $duration = array_merge($defaut, $duration);
        if (!empty($duration['year'])) {
            $periode .= $duration['year'] . 'Y';
        }
        if (!empty($duration['month'])) {
            $periode .= $duration['month'] . 'M';
        }
        if (!empty($duration['day'])) {
            $periode .= $duration['day'] . 'D';
        }
        if (!empty($duration['hour'])) {
            $temps .= $duration['hour'] . 'H';
        }
        if (!empty($duration['minute'])) {
            $temps .= $duration['minute'] . 'M';
        }
        if (!empty($duration['seconde'])) {
            $temps .= $duration['seconde'] . 'S';
        }

        if (!empty($periode) || !empty($temps)) {
            $ret = 'P' . $periode;
            if (!empty($temps)) {
                $ret .= 'T' . $temps;
            }
        }

        return $ret;
    }

    /**
     * Retourne un répertoire temporaire disponible dans le dossier passé en parametre
     * @param string $patchDir
     * @return bool|string
     */
    public static function newTmpDir($patchDir)
    {
        App::uses('Folder', 'Utility');
        $folder = new Folder($patchDir, true, 0777);
        //Création du répertoire temporaire
        $folder = new Folder($folder->path . DS . sha1(mt_rand()), true, 0777);
        return $folder->path;
    }

    /**
     *
     * @param type $file
     * @return type
     */
    public static function getNameFile($file)
    {
        $info = pathinfo($file);
        return basename($file, '.' . $info['extension']);
    }

    /**
     *
     * @param string $file
     * @return string
     */
    public static function getExtensionFile($file)
    {
        $info = pathinfo($file);
        return !empty($info['extension']) ?  $info['extension'] : false;
    }

    /**
     * Retourne le type mime d'un flux passé en parametre
     * @param string $data chemin ou fichier sous format string
     * @return bool|string
     */
    public static function fileMime($data, $flux = false, $mimetype = null)
    {
        App::uses('Fido', 'ModelOdtValidator.Lib');
        App::uses('File', 'Utility');

        if (empty($flux)) {
            $file = new File($data, false);
            $allowed = Fido::analyzeFile($file->path);
            $file->close();
        } else {
            $typemimes = AppTools::getTypemimesFiles();
            $typemimes = Set::combine($typemimes, '{n}.typemime', ['%s', '{n}.extension']);
            $file = new File(AppTools::newTmpDir(TMP . 'files/test')
                . '/test_.' . $typemimes[$mimetype], true, 0777);
            $file->write($data);
            $allowed = Fido::analyzeFile($file->path);
            $file->delete();
        }

        if (empty($allowed['mimetype'])) {
            $puids = AppTools::getPUIDsFiles();
            //si pronom ne trouve pas le type mime on laisse php le faire
            $allowed['mimetype'] = !empty($puids[$allowed['puid']]["typemime"])
                ? $puids[$allowed['puid']]["typemime"] : $file->mime();
        }

        return $allowed;
    }

    /**
     * Retourne les extentions acceptées par l'application
     * @param string $data
     * @return bool|string
     */
    public static function getExtensionsFilesValid()
    {
        $extensions = [];

        foreach (Configure::read('DOC_TYPE') as $format) {
            if (!is_array($format['extension'])) {
                $extensions[] = $format['extension'];
            } else {
                foreach ($format['extension'] as $extension) {
                    $extensions[] = $extension;
                }
            }
        }

        return $extensions;
    }

    /**
     * Retourne les extentions acceptées par l'application
     * @param string $data
     * @return bool|int[]|string|string[]
     */
    public static function getTypemimesFilesValid()
    {
        return array_keys(Configure::read('DOC_TYPE'));
    }

    /**
     * Retourne les extentions acceptées par l'application
     * @param string $data
     * @return bool|string
     */
    public static function getTypemimesFilesForFusion()
    {
        $typemimes = [];
        foreach (Configure::read('DOC_TYPE') as $typemime => $format) {
            if ($format['joindre_fusion']) {
                $typemimes[] = [
                    'typemime' => $typemime,
                    'extension' => $format['extension'],
                    'formatname' => $format['formatname']
                ];
            }
        }

        return $typemimes;
    }

    /**
     * Retourne-les extentions acceptées par l'application
     * @param string $data
     * @return bool|string
     */
    public static function getTypemimesFilesForCtrlLegalite()
    {
        $typemimes = [];
        foreach (Configure::read('DOC_TYPE') as $typemime => $format) {
            if ($format['joindre_ctrl_legalite']) {
                $typemimes[] = [
                    'typemime' => $typemime,
                    'extension' => $format['extension'],
                    'formatname' => $format['formatname']
                ];
            }
        }

        return $typemimes;
    }

    /**
     * Retourne les extentions acceptées par l'application
     * @param string $data
     * @return bool|string
     */
    public static function getTypemimesFiles()
    {
        $typemimes = [];
        foreach (Configure::read('DOC_TYPE') as $typemime => $format) {
            $typemimes[] = [
                'typemime' => $typemime,
                'extension' => is_array($format['extension'])
                    ? implode(' ', $format['extension']) : $format['extension'],
                'formatname' => $format['formatname']
            ];
        }
        return $typemimes;
    }

    /**
     * Retourne-les extentions pour un typemime
     * @param $mimetype
     * @return bool|string
     */
    public static function getTypemimesFileByTypemime($mimetype)
    {
        $typemimes = self::getTypemimesFiles();
        $typemimes = Set::combine($typemimes, '{n}.typemime', ['%s', '{n}.extension']);

        return $typemimes[$mimetype];
    }


    /**
     * Retourne-les puids acceptées par l'application
     * @return array()
     */
    public static function getPUIDsFiles()
    {
        $puids = [];
        foreach (Configure::read('DOC_TYPE') as $typemime => $format) {
            foreach ($format['puid'] as $puid_key => $puid) {
                if ($puid['actif'] === true) {
                    $puids[$puid_key] = [
                        'typemime' => $typemime,
                        'extension' => is_array($format['extension'])
                            ? implode(' ', $format['extension']) : $format['extension'],
                        'formatname' => $format['formatname']
                    ];
                }
            }
        }
        return $puids;
    }

    /**
     * @param int|string $bytes
     * @param int $decimals
     * @return string
     */
    public static function humanFilesize($bytes, int $decimals = 2): string
    {
        if (empty($bytes)) {
            return 'Indisponible';
        }

        $sz = ['B', 'Ko', 'Mo', 'Go'];
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / (1024 ** $factor)) . @$sz[$factor];
    }

    /**
     *
     * @param type $_string
     * @return type
     */
    public static function xmlEntityEncode($_string)
    {
        //UTILISER  htmlentities() à partir de php 5.4.0
        // Set up XML translation table
        $_xml = [];
        $_xl8_iso = get_html_translation_table(HTML_ENTITIES, ENT_QUOTES);
        //Compatibilité php <5.3.3
        foreach ($_xl8_iso as $key => $value) {
            $_xl8[utf8_encode($key)] = utf8_encode($value);
        }
        while (list($_key, $_val) = each($_xl8)) {
            $_xml[$_key] = '&#' . AppTools::uniord($_key) . ';';
        }

        return strtr($_string, $_xml);
    }

    /**
     *
     * @param type $u
     * @return type
     */
    public static function uniord($u)
    {
        $k = mb_convert_encoding($u, 'UCS-2LE', 'UTF-8');
        $k1 = ord(substr($k, 0, 1));
        $k2 = ord(substr($k, 1, 1));

        return $k2 * 256 + $k1;
    }

    /**
     * Retourne la date en francais et en toute lettre du timestamp passé,
     * seule les dates a quatre chifres sont gérés.
     *
     * @param type $timestamp de la date voulue
     * @return string date en toute lettre
     */
    public static function dateLettres($timestamp)
    {
        $days = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
        $months = ['', 'janvier', 'février', 'mars', 'avril', 'mai', 'juin',
            'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'];
        $jour = $days[date('w', $timestamp)];
        $nbJour = date('d', $timestamp);
        // jour en toute lettre
        $nbJour = self::dizaines($nbJour);
        $mois = $months[date('n', $timestamp)];
        // année en toute lettre
        $nbAnnee = date('Y', $timestamp);
        if (substr($nbAnnee, 0, 1) == 0) {
            $annee = '';
        } elseif (substr($nbAnnee, 0, 1) == 1) {
            $annee = 'mille' . ' ';
        } else {
            $annee = self::unite(substr($nbAnnee, 0, 1)) . ' ' . 'mille' . ' ';
        }
        if (substr($nbAnnee, 1, 1) == 0) {
            $annee .= '';
        } elseif (substr($nbAnnee, 1, 1) == 1) {
            $annee .= 'cent' . ' ';
        } else {
            $annee .= self::unite(substr($nbAnnee, 1, 1)) . ' ' . 'cent' . ' ';
        }
        $annee .= self::dizaines(substr($nbAnnee, 2));
        return("L'an $annee, le $nbJour $mois ");
    }

    /**
     * change des chiffres précis en lettre
     *
     * @param string $data le chiffre à transformer
     * @return string|boolean soit le chiffre voulu en lettre soit false
     */
    private static function latinIrregulier($data)
    {
        switch ($data) {
            case 11:
                return "onze";
            case 12:
                return "douze";
            case 13:
                return "treize";
            case 14:
                return "quatorze";
            case 15:
                return "quinze";
            case 16:
                return "seize";
        }
        return false;
    }

    /**
     * Renvoie les unitées en lettre
     *
     * @param string $data le chiffre à transformer
     * @return string|boolean soit le chiffre voulu en lettre soit false
     */
    private static function unite($data)
    {
        switch ($data) {
            case 0:
                return '';
            case 1:
                return "un";
            case 2:
                return "deux";
            case 3:
                return "trois";
            case 4:
                return "quatre";
            case 5:
                return "cinq";
            case 6:
                return "six";
            case 7:
                return "sept";
            case 8:
                return "huit";
            case 9:
                return "neuf";
        }
        return false;
    }

    /**
     * Renvoie les dizaines en toutes lettres
     *
     * @param integer $data le chiffre à transformer
     * @return string|boolean soit le chiffre voulu en lettre soit false
     */
    private static function dizaines($data)
    {
        $ret = self::latinIrregulier($data);
        if (!$ret) {
            $et = '';
            if (substr($data, 1) == 1) {
                $et = 'et ';
            }
            switch (substr($data, 0, 1)) {
                case 0:
                    return '' . self::unite(substr($data, 1));
                case 1:
                    return "dix" . ' ' . self::unite(substr($data, 1));
                case 2:
                    return "vingt" . ' ' . $et . self::unite(substr($data, 1));
                case 3:
                    return "trente" . ' ' . $et . self::unite(substr($data, 1));
                case 4:
                    return "quarante" . ' ' . $et . self::unite(substr($data, 1));
                case 5:
                    return "cinquante" . ' ' . $et . self::unite(substr($data, 1));
                case 6:
                    return "soixante" . ' ' . $et . self::unite(substr($data, 1));
                case 7:
                    return "soixante" . ' ' . $et . self::dizaines('1' . substr($data, 1));
                case 8:
                    return "quatre vingt" . ' ' . self::unite(substr($data, 1));
                case 9:
                    return "quatre vingt" . ' ' . self::dizaines('1' . substr($data, 1));
            }
        } else {
            return $ret;
        }
        return false;
    }

    public function colors()
    {
        return ['Peach Echo' => ['hex' => '#F7786B'],
            'Serenity' => ['hex' => '#91A8D0'],
            'Snorkel Blue' => ['hex' => '#034F84'],
            'Limpet Shell' => ['hex' => '#98DDDE'],
            'Lilac Grey' => ['hex' => '#9896A4'],
            'Iced Coffee' => ['hex' => '#B18F6A'],
            'Fiesta' => ['hex' => '#DD4132'],
            'Buttercup' => ['hex' => '#FAE03C'],
            'Green Flash' => ['hex' => '#79C753'],
            'Marsala' => ['hex' => '#955251'],
            'Tangerine Tango' => ['hex' => '#DD4124'],
            'Honeysucle' => ['hex' => '#D65076'],
            'Turquise' => ['hex' => '#45B8AC'],
            'Mimosa' => ['hex' => '#EFC050'],
            'Blue Izis' => ['hex' => '#5B5EA6'],
            'Chili Pepper' => ['hex' => '#9B2335'],
            'Sand Dollar' => ['hex' => '#DFCFBE'],
            'Highway Brown' => ['hex' => '#633517'],
            'Highway Red' => ['hex' => '#a6001a'],
            'Highway Orange' => ['hex' => '#e06000'],
            'School Bus Yellow' => ['hex' => '#ee9600'],
            'Highway Yellow' => ['hex' => '#ffab00'],
            'Highway Green' => ['hex' => '#004d33'],
            'Highway Blue' => ['hex' => '#00477e'],
            'Tan' => ['hex' => '#b19b8b'],
            'Rosewood' => ['hex' => '#dcaf9a'],
            'Khaki 475' => ['hex' => '#7d776a'],
            'Army Blue 451' => ['hex' => '#3a435d'],
            'Gold' => ['hex' => '#967d46'],
            'Brown 383 Camo' => ['hex' => '#594d45'],
            'Dull Red, ANA 618' => ['hex' => '#874840'],
            'International Light Red' => ['hex' => '#bd4255'],
            'Navy Torpedo' => ['hex' => '#e85e31'],
            'Olive Drab CAMO' => ['hex' => '#595142'],
            'NASA Primer' => ['hex' => '#798175'],
            'Forest Service Green' => ['hex' => '#6fc7b0'],
            'ANA 609' => ['hex' => '#7284b7'],
            'Beige' => ['hex' => '#d0bca4'],
            'Black Camo' => ['hex' => '#373538'],
            'Ivory' => ['hex' => '#f5e4c8'],
            'Deep cream' => ['hex' => '#fdf07a'],
            'Terracotta' => ['hex' => '#a65341'],
            'Cherry' => ['hex' => '#c41c22'],
            'Dark violet' => ['hex' => '#6e4a75'],
            'Fontana' => ['hex' => '#a97681'],
            'Azalea' => ['hex' => '#f77d64'],
            'Caramel' => ['hex' => '#9d7c4d'],
            'Greengage' => ['hex' => '#8f9d38'],
            'Larkspur' => ['hex' => '#65a2c3'],
            'Regalia' => ['hex' => '#684a6b'],
            'Rose Quartz' => ['hex' => '#F7CAC9'],
        ];
    }

    public function colourBrightness($hex, $percent)
    {
        // Work out if hash given
        $hash = '';
        if (stristr($hex, '#')) {
            $hex = str_replace('#', '', $hex);
            $hash = '#';
        }
        /// HEX TO RGB
        $rgb = [
            hexdec(substr($hex, 0, 2)),
            hexdec(substr($hex, 2, 2)),
            hexdec(substr($hex, 4, 2))
        ];
        //// CALCULATE
        for ($i = 0; $i < 3; $i++) {
            // See if brighter or darker
            if ($percent > 0) {
                // Lighter
                $rgb[$i] = round($rgb[$i] * $percent) + round(255 * (1 - $percent));
            } else {
                // Darker
                $positivePercent = $percent - ($percent * 2);
                $rgb[$i] = round($rgb[$i] * $positivePercent) + round(0 * (1 - $positivePercent));
            }
            // In case rounding up causes us to go to 256
            if ($rgb[$i] > 255) {
                $rgb[$i] = 255;
            }
        }
        //// RBG to Hex
        $hex = '';
        for ($i = 0; $i < 3; $i++) {
            // Convert the decimal digit to hex
            $hexDigit = dechex($rgb[$i]);
            // Add a leading zero if necessary
            if (strlen($hexDigit) == 1) {
                $hexDigit = "0" . $hexDigit;
            }
            // Append to the hex string
            $hex .= $hexDigit;
        }

        return $hash . $hex;
    }

    public function convertToBytes($size)
    {
        // Remove the non-unit characters from the size.
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size);
        // Remove the non-numeric characters from the size.
        $size = preg_replace('/[^0-9\.]/', '', $size);
        if ($unit) {
            // Find the position of the unit in the ordered string which is the power of
            // magnitude to multiply a kilobyte by.
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        }

        return round($size);
    }

    /**
     *  Modification pour le nom de dossier
     *
     * @param string $objetDossier
     * @return string
     */
    public static function reformatNameForIparapheur($objetDossier): string
    {
        return self::reformatNameByLength($objetDossier, 250);
    }

    /**
     *  Modification pour le nom de dossier
     *
     * @param string $objetDossier
     * @return string
     */
    public static function reformatNameForPastell(string $objetDossier): string
    {
        return self::reformatNameByLength($objetDossier, 500);
    }

    /**
     *  Modification pour le nom de dossier
     *
     * @param string $texte
     * @return string
     */
    public static function reformatNameByLength(string $texte, $length): string
    {
        $texte = preg_replace('/\R/', ' ', $texte);

        if (mb_strlen($texte) > $length) {
            $length = mb_strrpos(mb_substr($texte, 0, $length, 'UTF-8'), ' ');
            $texte = mb_substr(
                $texte,
                0,
                $length,
                'UTF-8'
            );
        }

        return trim($texte);
    }

    /**
     *  Convertion UTF8 bers un encodage compatible avec S2low
     *
     * @param type $objetDossier
     * @return type
     */
    public static function convertUTF8TForS2low($str)
    {
        return iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $str);
    }

    /**
     *  Modification pour le nom de dossier
     *
     * @param int $comment
     * @param $length
     * @return string
     */
    public function truncateComment($comment, $length)
    {
        $comment = trim($comment);
        if (mb_strlen($comment) > $length) {
            $comment = __('%s...', mb_substr($comment, 0, $length, 'utf-8'));
        }
        return $comment;
    }

    /**
     * this is for searching a value inside a multidimontionnal array,
     * and then return the parent of the parent array that holds the value.
     * @param $needle
     * @param $haystack
     * @return bool|int|string
     */
    public static function recursiveArraySearch($needle, $haystack)
    {
        foreach ($haystack as $key => $value) {
            $current_key=$key;
            if ($needle===$value or (is_array($value) && self::recursiveArraySearch($needle, $value) !== false)) {
                return $current_key;
            }
        }
        return false;
    }

    /**
     * @param $expression
     * @param false $return
     * @return string|void
     */
    public function varExportConfig($expression, $return = false)
    {
        $export = var_export($expression, true);
        $export = preg_replace("/^([ ]*)(.*)/m", '$1$1$2', $export);
        $array = preg_split("/\r\n|\n|\r/", $export);
        $array = preg_replace(["/\s*array\s\($/", "/\)(,)?$/", "/\s=>\s$/"], [null, ']$1', ' => ['], $array);
        $export = join(PHP_EOL, array_filter(["["] + $array));
        if ((bool)$return) {
            return $export;
        } else {
            echo $export;
        }
    }

    public static function humanPHPSize($value)
    {
        return str_replace('M', ' Mo', $value);
    }

    public static function env($key, $default = null)
    {
        $value = env($key);
        return $value ?? $default;
    }
}
