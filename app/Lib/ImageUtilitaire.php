<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('File', 'Utility');

/**
 * [ImageUtilitaire description]
 */
class ImageUtilitaire
{
    /**
     *  Retourne  un array contenant la largeur et la hauteur optimales permettant de faire rentrer
     * sans déformation l'image originale dans les dimensions demandées, sans agrandissement.
     *
     * @param integer $width La largeur de l'image originale
     * @param integer $height La hauteur de l'image originale
     * @param integer $wDest La largeur maximale de l'image résultante
     * @param integer $hDest La hauteur maximale de l'image résultante
     * @return array En clé 0, la largeur optimale, en clé 1 la hauteur optimale
     */
    public static function boundingRectangle($width = 0, $height = 0, $wDest = 0, $hDest = 0)
    {
        $wNew=0;
        $hNew=0;
        if (($width==0)||($height==0)||($wDest==0)||($hDest==0)) {
            throw new Exception('Paramètres d\'entrée non nuls obligatoires');
        }
        if ($width<=$wDest && $height<=$hDest) { // Ne pas redimmensionner
            $wNew=$width;
            $hNew=$height;
        } elseif ($wDest>$width/$height*$hDest) { //Utiliser $height comme base
            $hNew=$hDest;
            $wNew=$hNew/$height * $width;///$hDest*$width;
        } else {                                            //Utiliser $Ymax comme base
            $wNew=$wDest;
            $hNew=$wDest/$width*$height;///$wDest*$height;
        }
        return([$wNew,$hNew]);
    }

    public static function resizeImageToFile($nameFileOrig, $nameFileDest, $wDest, $hdest)
    {
        if (is_null($nameFileOrig)) {
            throw new Exception('Chemin d\'origine vide');
        }
        $imageContent = imagecreatefromstring(file_get_contents($nameFileOrig));

        if ($imageContent==false) {
            throw new Exception('Format d\'image non reconnu');
        }

        $width=imagesx($imageContent);
        $heigth=imagesy($imageContent);


        list($wNew, $hNew) = ImageUtilitaire::boundingRectangle($width, $heigth, $wDest, $hdest);

        $newContent = imagecreatetruecolor($wNew, $hNew);

        imagecopyresampled($newContent, $imageContent, 0, 0, 0, 0, $wNew, $hNew, $width, $heigth);

        imagejpeg($newContent, $nameFileDest);
        return true;
    }
}
