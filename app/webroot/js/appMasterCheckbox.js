/**
 * Comportement des checkboxes / master checkbox
 * Select All / Deselect All
 * Initial state
 * @version 4.3
 */

define(["jquery"], function ($) {
    'use strict';

    // Main function
    $.fn.appMasterCheckbox = function (options) {
        // Ensure that only one scrollUp exists
        if (!$.data(document.body, 'appMasterCheckbox')) {
            $.data(document.body, 'appMasterCheckbox', true);
            $.fn.appMasterCheckbox.init(options);
        }
    };

    // Init
    $.fn.appMasterCheckbox.init = function (options) {

        var object = $.fn.appMasterCheckbox.settings = $.extend({}, $.fn.appMasterCheckbox.defaults, options),
                $self;

        $self = $("#" + object.id);
        //console.log("#" + object.Name);
        // masterCheckbox -> Checkbox
        // La master checkbox coche/décoche tout d'un coup
        $self.on('change', function () {
            //console.log($self);
            $("input[type=checkbox]").not(':disabled').prop('checked', $(this).prop('checked'));
            $('input[class*="masterCheckbox_checkbox"]').trigger("change"); //FIX
                    //.trigger("change")
        });
        //Checkbox -> masterCheckbox not("#" + object.id)
        $('input[class*="masterCheckbox_checkbox"]').on('change', function () {
            $self.prop('checked', $('input[class*="masterCheckbox_checkbox"]').not(':disabled').not(':checked').length === 0);
            // Si toutes les checkboxes (sauf masterCheckbox) sont cochées, cocher masterCheckbox
            if ($('input[class*="masterCheckbox_checkbox"]').not(':checked').length === 0) {
                $self.prop('checked', true);
            }
            if(!_.isEmpty($('select[class*="app-traitementParLot"]'))) {
              changeSelection();
            }
        });

        if(!_.isEmpty($('select[class*="app-traitementParLot"]'))) {
          changeSelection();
        }
    }

    function changeSelection() {
      //traitemement par lot
      var nbChecked = $('input[type=checkbox].masterCheckbox_checkbox:checked').length;
      //Apposer ou non la class disabled au bouton selon si des checkbox sont cochées (style)
      if (nbChecked > 0) {
          $('#select-circuit').show();
          $('#genereNumActe').removeClass('disabled');
          $('#genereNumActe').prop('disabled', false);
          $('#signatureManuscrite').removeClass('disabled');
          $('#signatureManuscrite').prop('disabled', false);
          $('#autresActesAbandon').prop('disabled', false);
          $('#sendToTdt').prop('disabled', false);
      } else {
          $('#select-circuit').hide();
          $('#genereNumActe').addClass('disabled');
          $('#genereNumActe').prop('disabled', true);
          $('#signatureManuscrite').addClass('disabled');
          $('#signatureManuscrite').prop('disabled', true);
          $('#autresActesAbandon').prop('disabled', true);
          $('#sendToTdt').prop('disabled', true);
      }
    }

    // Defaults
    $.fn.appMasterCheckbox.defaults = {
        id: 'masterCheckbox', // Element ID
    };

//    $.fn.appMasterCheckbox.selectAll = function () {
//        var object = $.fn.appMasterCheckbox.settings = $.extend({}, $.fn.appFilter.defaults, options),
//                $self;
//
//        $self = $("#" + object.Name).trigger('click');
//
//        return false;
//    }

    $.appMasterCheckbox = $.fn.appMasterCheckbox;
});
