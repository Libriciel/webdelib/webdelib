/**
 * Comportement des checkboxes / master checkbox
 * Select All / Deselect All
 * Initial state
 * @version 5.1.2
 * @since 5.1.1
 */

define(["jquery"], function ($) {
    'use strict';

    var downloadTimer;
    var btnHtml;

    // Main function
    $.fn.appTypologie = function (options) {
        // Ensure that only one scrollUp exists
        if (!$.data(document.body, 'appTypologie')) {
            $.data(document.body, 'appTypologie', true);
            $.fn.appTypologie.init(options);
        }
    };

    // Init
    $.fn.appTypologie.init = function (options) {

        var object = $.fn.appTypologie.settings = $.extend({}, $.fn.appTypologie.defaults, options),
                $self;

        $self = $("#" + object.id);

        $('.class-wd-tdt-typologieForm').on("click", function () {
          log($(this));

          var path = '/teletransmettre';
          if($(this).closest("form").attr('action').search(/teletransmettre/g)===-1){
            path = '/postseances';
          }

          modalInit($(this).attr("data-target"), path);
        });
    };

    function modalInit($dataTarget, path){
      var $bodyModal = $($dataTarget + ' div.modal-body');
      var projetId = $dataTarget.replace(/\D/g, '');
      $bodyModal.empty();
      $.ajax({
          type: 'POST',
          url: path + '/typologiePieceToTdtAjax',
          data: {
            'acte_id': projetId,
          },
          beforeSend: function () {
          },
          success: function (data) {
            $(data).appendTo($bodyModal);
            $bodyModal.find('.selectone').select2();
            $bodyModal.closest('div.modal-dialog').css('display', 'table');
            //checkTypologiePiece($bodyModal);
            modifyInfoToTdt(projetId, path);
          },
          complete: function(){
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
              log(XMLHttpRequest);
              log(errorThrown);
              log(textStatus);
          }
      });
    }

    function checkTypologiePiece ($bodyModal) {
        $bodyModal.find('select[id*="NumPref"]').on('change', function(){
         var $acteId = $bodyModal.find('input[data-acte-id]').attr('data-acte-id');
         var $typeacteId = $bodyModal.find('input[data-acte-typeacte-id]').attr('data-acte-typeacte-id');
         var $codeClassification = $bodyModal.find('select[data-acte-code-classification]').val();

         initTypologiePieces($bodyModal.find('select[data-typologiepiece-type="Principal"]'), 'Principale',$acteId, $typeacteId);
         initTypologiePieces($bodyModal.find('select[data-typologiepiece-type="Annexe"]'), 'Annexe',$acteId, $typeacteId, $codeClassification);
      });
    };


    /** Fonction d'annulation de suppression d'une annexe
     *
     * @param {type} annexe_id
     * @returns {undefined}
     */
    function initTypologiePieces($select, $type, $projetId, $typeacteId, $codeClassification, $selected) {
      log('[initTypologiePieces][type]' + $type);
      log('[initTypologiePieces][projetId]' + $projetId);
      log('[initTypologiePieces][codeClassification]' + $codeClassification);
      log('[initTypologiePieces][natureId]' + $typeacteId);
      $.ajax({
          type: 'POST',
          data: {
              'type': $type,
              'typeacte_id': $typeacteId,
              'num_pref': $codeClassification,
              'projet_id': $projetId,
              'typologiepiece_code': $selected
              },
          url: '/deliberations/getTypologiePiecesAjax',
          beforeSend: function () {
              $select.closest('div.form-group').hide();
              $select.find("option").remove();
              $select.removeAttr("disabled");
          },
          success: function (options) {
              if(!_.isEmpty(options)){
                  var newOption = new Option('', '', false, false);
                  $select.append(newOption).trigger('change');

                  $.each(options, function (key, option) {
                    var newOption = new Option(option.name, option.value, false, option.selected);
                    $select.append(newOption).trigger('change');
                  });

                  $select.closest('div.form-group').show();
              }
          },
          complete: function(){
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
              log(XMLHttpRequest);
              log(errorThrown);
              log(textStatus);
          }
      });
    };

    /**
     * [sendToTdt description]
     * @param  {[type]} projetId [description]
     * @return {[type]}          [description]
     */
    function modifyInfoToTdt(projetId, path) {
        $('#modifyBtn-tdt-' + projetId).on("click", function () {
          var data = { Deliberation: [] };

          var $tdtDocumentPapier = $('input[type="checkbox"][id="Deliberation['+projetId+']TdtDocumentPapier"]').prop('checked');
          log('$tdtDocumentPapier:' + $tdtDocumentPapier);
          var annexes = [];
          $('select[id*="Deliberation['+projetId+']Annexe"] option:selected').each(function () {
            annexes.push({
                'id': $(this).closest('select').attr('data-typologiepiece-annexe-id'),
                'typologiepiece_code': $(this).val()
            });
          });
          data.Deliberation.push({
              'id': projetId,
              'num_pref': $('select[id="Deliberation['+projetId+']NumPref"] option:selected').val(),
              'typologiepiece_code': $('select[id="Deliberation['+projetId+']TypologiepieceCode"] option:selected').val(),
              'tdt_document_papier': _.isUndefined($tdtDocumentPapier) ? false : $tdtDocumentPapier,
              'Annexe': annexes
          });
          log(data);

          $.ajax({
              type: "POST",
              url: path + '/editInfoSendToTdt',
              data: data,
              success: function(data) {
                // then reload the page.(3)
                window.location.hash = 'Deliberation' + projetId + 'Send';
                location.reload();
              },
              complete: function(){
              },
              error: function (XMLHttpRequest, textStatus, errorThrown) {
                  log(XMLHttpRequest);
                  log(errorThrown);
                  log(textStatus);
              }
          });
        });
    };


    function log(log){
      if ($.fn.appTypologie.settings.debug) {
        console.log(log);
      }
    }

    // Defaults
    $.fn.appTypologie.defaults = {
        id: 'appTypologie', // Element ID
        debug: false
    };

    $.appTypologie = $.fn.appTypologie;
});
