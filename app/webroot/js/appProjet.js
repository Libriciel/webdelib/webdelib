define(['jquery','select2'], function ($) {

    // Main function
    $.fn.appProjet = function (options) {
        // Ensure that only one scrollUp exists
        if (!$.data(document.body, 'appProjet')) {
            $.data(document.body, 'appProjet', true);
            $.fn.appProjet.init(options);
        }
    };

    $('#DeliberationObjet').textcounter({
        max                      : 499,
        countSpaces              : true,
        twoCharCarriageReturn    : true,
        countExtendedCharacters  : true,
        counterText              : "(max. %d / 499 caractères)",
        init                     : function (el) {
            $('.text-count-wrapper').attr('class','text-right');
        }
    });

    $('#DeliberationTitre').textcounter({
        max                      : 1000,
        countSpaces              : true,
        twoCharCarriageReturn    : true,
        countExtendedCharacters  : true,
        counterText              : "(max. %d / 1000 caractères)",
        init                     : function (el) {
            $('.text-count-wrapper').attr('class','text-right');
        }
    });

    /** déplace la position d'une annexe
     *
     * @param projet_id
     * @param annexe_id
     * @param {type} newPosition
     * @returns {undefined}
     */
    function deplacerAnnexePosition(projet_id, annexe_id, newPosition)
    {
        log('[deplacerAnnexePosition][projet_id]' + projet_id);
        log('[deplacerAnnexePosition][annexe_id]' + annexe_id);
        log('[deplacerAnnexePosition][newPosition]' + newPosition);

        var table = $('table[data-wd-projet-id=' + projet_id + '].wd-projet-annexe-table');
        var row = table.find('tr[data-wd-projet-annexe-id=' + annexe_id + ']');

        var position = 1;
        table.find('tbody tr:not([class*=annexeLine]):not([data-wd-projet-annexe-id=' + annexe_id + '])').each(function () {
            if ($(this).attr('data-wd-projet-position') == newPosition) {
                if (parseInt(row.attr('data-wd-projet-position')) > parseInt(newPosition)) {
                    $(this).before(row);
                    position++;
                } else {
                    $(this).after(row);
                }
            }
            $(this).attr('data-wd-projet-position', position);
            $(this).find('td div.wd-annexe-edit-position').html(position);

            $(this).position++;
        });
        row.attr('data-wd-projet-position', newPosition);
        $(this).find('td div.wd-annexe-edit-position').html(newPosition);

        reordonnerPosition(projet_id);
    }

    /** met à jour l'ordre d'affichage des annexes
     *
     * @param {type} ref
     * @returns {undefined}
     */
    function reordonnerPosition(projet_id)
    {
        log('[reordonnerPosition][projet_id]' + projet_id);
        var rows = $('table[data-wd-projet-id=' + projet_id + '].wd-projet-annexe-table tbody tr:not([class*=annexeLine])');
        rows.each(function (index) {
              ++index;
              log('[reordonnerPosition][index]' + index);
              //on met à jour les positions de sources et de destinations des text et selected
              $(this).find('.wd-projet-annexe-select').val(index).trigger("change");
              $(this).find('.wd-projet-annexe-select').prop("disabled", false);
              $(this).find('input[type=hidden][name*=foreign_key]').prop("disabled", false);

              $(this).find('.annexe-view:first').html(index);
              $(this).attr('data-wd-projet-position', index);
              // if ($(this).hasClass('annexeLine')) {
              //     $(this).attr('id', projetRef + 'ProjetId_' + projet_id + 'AnnexeLine_' + index );
              // }
              //on met à jour le bouton d'annnulation
              $(this).find('.annulerAjoutAnnexe')
                      .attr('data-wd-projet-annexe-id', index)
                      .attr('data-wd-projet-id', projet_id);
                    //  .attr('data-wd-projet-ref', projetRef);//annulerAjoutAnnexe' + index +
        });
    }

    /**
     * @returns {undefined}
     */
    function disableExitWarning()
    {
        $(window).unbind('beforeunload');
        setTimeout(function () {
            onUnloadEditForm();
        }, 3000); // 3000 millisecondes = 3 secondes
    }

    /** Gestion des sorties du formulaire
     *
     * @returns {undefined}
     */
    function onUnloadEditForm()
    {
        $(window).bind('beforeunload', function () {
            return "Attention !\n\n Si vous quittez cette page, vous allez perdre vos modifications.";
        });
    }

    function log(log)
    {
        if ($.fn.appProjet.settings.debug) {
            console.log(log);
        }
    }

    /**
     * Fonction d'ajout d'une nouvelle annexe : appelée à l'enregistrement de la modale
     * @returns {boolean}
     */
    function ajouterAnnexeLigne(projet_id, annexe_num)
    {

        var newModal = $('div[data-wd-projet-id=' + projet_id + '].wd-modal-annexe-add');
        var annexe_key = projet_id + '_' + annexe_num;
        //var ref = $('#annexeModal input#annexeProjetRef').val();
        var table = $('table[data-wd-projet-id=' + projet_id + '].wd-projet-annexe-table');
        // if(table.attr('data-wd-projet-id') == 'TemplateProjetId'){
        //   table.attr('data-wd-projet-id', projet_id);
        // }

        var newLine = table.find('tr.annexeLine:first').clone(true);

        //Récupération des valeurs
        var joindre_fusion = newModal.find("input[name*='fusion']:not([type=hidden])").prop('checked');
        log('ajouterAnnexeLigne[joindre_fusion]' + joindre_fusion);
        var joindre_ctrl = newModal.find("input[name*='ctrl']:not([type=hidden])").prop('checked');
        log('ajouterAnnexeLigne[joindre_ctrl]' + joindre_ctrl);
        var typologiepieceCodeLibelle = joindre_ctrl ? newModal.find("select[name*='typologiepiece_code'] option:selected").text() : '';
        log('ajouterAnnexeLigne[AnnexeAddTdtTypologiepieceCodeLibelle]' + typologiepieceCodeLibelle);

        //var line = $('<tr class="success ligneAnnexe" title="Annexe à ajouter" id="'+ ref + 'LigneAnnexe_' + annexe_num + '"></tr>');

        //Ajout de la ligne dans le tableau des annexes
        newLine.attr('data-wd-projet-annexe-id', annexe_num);

        newLine.find('td[id=AnnexeAddPosition]').html(annexe_num); //annexe-view
        newLine.find('td[id=AnnexeAddNameFile]').html(newModal.find('input#Annexe' + annexe_key + 'File').val().split("\\").pop());
        newLine.find('td[id=AnnexeAddTitle]').html(newModal.find('input#Annexe' + annexe_key + 'Titre').val());
        newLine.find('td[id=AnnexeAddTaille]').html(newModal.find('input#Annexe' + annexe_key + 'Titre').val());
        newLine.find('td[id=AnnexeAddTdt]').html(joindre_ctrl ? 'Oui' : 'Non');
        newLine.find('td[id=AnnexeAddTdtTypologiepieceCodeLibelle]').html(typologiepieceCodeLibelle);

        newLine.find('td[id=AnnexeAddFusion]').html(joindre_fusion ? 'Oui' : 'Non');

        newLine.find('div.btn-group #AnnexeAddUndo').attr('data-wd-projet-annexe-id', annexe_num);
        newLine.find('div.btn-group #AnnexeAddUndo').attr('data-wd-projet-id', projet_id);

        newLine.show();

        newLine.appendTo(table);

        //Déclencheurs des boutons //FIX
        newLine.find('div.btn-group #AnnexeAddUndo').click(function () {
            annulerAjoutAnnexe($(this));
        });

        //FIX
        table.slideDown('slow');

        // $('html, body').animate({
        //     scrollTop: table.offset().top // Prise en compte de la topbar
        // }, 'slow');

        reordonnerPosition(projet_id);

        return false;
    }

    /**
     * Annule l'ajout d'une annexe (supprime du tableau et du formulaire)
     * @param element
     * @returns {boolean}
     */
    function annulerAjoutAnnexe(btn_annuler)
    {

        var annexe_id = btn_annuler.attr('data-wd-projet-annexe-id');
        var projet_id = btn_annuler.attr('data-wd-projet-id');
        var annexe_key =  projet_id + '_' + annexe_id;

        //var id = , ref = $(element).attr('data-wd-projet-ref'), projet_id = $(element).attr('data-wd-projet-id');

        $('table[data-wd-projet-id='+projet_id+'] tr.annexeLine[data-wd-projet-annexe-id=' + annexe_id + ']').remove();
        $('div[data-wd-projet-id=' + projet_id + '].wd-annexe-add-data div[id=' + annexe_key + ']').remove();
        // $('#' + ref + 'LigneAnnexe_' + id).fadeOut(300, function () {
        //     /*if ($('#tableAnnexes' + ref + ' tbody tr:visible').length == 0)
        //      $('#tableAnnexes' + ref).hide();*/
        // }).remove();
        //
        // $('#' + ref + 'AnnexeModal_' + id).remove();

        reordonnerPosition(projet_id);

        return false;
    }

    /**
     * remet les champs à zéro
     */
    function modalAnnexeAdd(projet_id)
    {

        log('div[data-wd-projet-id=' + projet_id + '].wd-modal-annexe-add');
        //Numéro d'annexe
        var numAnnexe = $('table[data-wd-projet-id=' + projet_id +'].wd-projet-annexe-table tr').length - 1;
        var annexe_key = projet_id + '_' + numAnnexe;
        log('Annexe début du compteur (' + numAnnexe + ')');
        var newModal = $('div[data-wd-projet-id=' + projet_id + '].wd-modal-annexe-add').clone();
        newModal.appendTo($('.wd-annexe-add-data[data-wd-projet-id=' + projet_id + ']'));
        //var newModal = $('.wd-annexe-add-data[data-wd-projet-id=' + projet_id + '] div[data-wd-projet-id=' + projet_id + '].wd-modal-annexe-add');
        //Purge des events
        newModal.find('.wd-modal-btn-annexe-add-confirm').off('click');
        // Delete disabled
        newModal.find('input').removeAttr('disabled');
        //var annexTable = $(ref + 'table#wd-projet-annexe-table' + ' > tbody:last');
        newModal.find('.wd-modal-annexe-add-annexe-position').val(numAnnexe);

        //On clone le modal de base
        //$('#annexeModal').find("input[type=file]").filestyle('destroy');

        $.each(['id', 'name', 'for'], function (key, value) {
            newModal.find("[" + value + "*='TemplateAnnexeNum']").each(function () {
                $(this).attr(value, $(this).attr(value).replace('TemplateAnnexeNum', annexe_key));
            });
        });

        // newModal.find("[name*='[TemplateAnnexeNum]']").each(function () {
        //     $(this).attr('name', $(this).attr('name').replace('[TemplateAnnexeNum]', '[' + annexe_key + ']'));
        // });

        //newModal.appendTo('#' + ref + 'AnnexeAdd[data-wd-projet-id=' + projet_id + ']');
        //newModal.attr('numAnnexe', ref + 'AnnexeAddModal_' + annexe_key);
        //newModal.attr('data-wd-projet-id', projet_id);

        // .each(['id', 'name', 'aria-labelledby', 'aria-controls', 'data-target', 'data-wd-projet-id', 'projet_id'], function (key, value) {
        //   newDelibBloc.find("[" + value + "*='TemplateProjetId']").each(function () {
        //       $(this).attr(value, $(this).attr(value).replace('TemplateProjetId', iMultiDelibAAjouter));
        //   });
        // });

        //Change les attributs id, name et les labels avec le numéro courant
        // var attributs = ['id','name','for'];
        // newModal.find("input, label, div").each(function () {
        //     var element=$(this);
        //     for (var index in attributs) {
        //         if (element.attr(attributs[index]) !== undefined) {
        //             element.attr(attributs[index], element.attr(attributs[index]).replace('0', annexe_key));
        //         }
        //     }
        // });


        //Hack filestyle
        //newModal.find(".bootstrap-filestyle").remove();
        //newModal.find("input[type=file]:first").after('<input class="aa" name="aaaa" type="file" />');
        newFileStyle(newModal.find("input[type=file]:first"));

        newModal.find("input[name*='fusion']:first").closest('div.form-group').hide();
        newModal.find("input[name*='ctrl']:first").closest('div.form-group').hide();
        newModal.find("select[name*='typologiepiece_code']:first").closest('div.form-group').hide();

        //newModal.find('#btnAnnexeAddModal').attr('data-id', annexe_key).attr('data-ref', ref);
        newModal.find('.wd-modal-annexe-add-erreur').hide();
        newModal.find('.wd-modal-annexe-add-erreur').html('');

        newModal.modal('show');
        //var modal = $('#' + ref + 'AnnexeAddModal_'+ annexe_key);
        // Gestion des formats d'annexe (affichage checkboxes)
        newModal.find("input[type=file]:first").on('change', function () {

            var newModal = $(this).closest('div.wd-modal-annexe-add');
            //Purge de l'erreur
            newModal.find('.wd-modal-annexe-add-erreur').hide();
            newModal.find('.wd-modal-annexe-add-erreur').html('');

            var $selectTypologiePieces = newModal.find("select[name*='typologiepiece_code']:first");
            $selectTypologiePieces.select2();
            $selectTypologiePieces.val(null).trigger('change');

            if (newModal.find("input[type=file]:first").val() !== '') {
                var tmpArray = $(this).val().split('.');
                var extension = tmpArray[tmpArray.length - 1];
                extension = extension.toLowerCase();
                log('[newModal][input][change]: '+ extension)
                var re = new RegExp('^[a-zA-Z0-9-_.& ]{5,}$', 'i');
                //Fix path navigator
                var filename = newModal.find("input[type=file]:first").val().replace(/^.*\\/, "");
                var error_display = newModal.find('.wd-modal-annexe-add-erreur');
                if (re.test(filename)==false) {
                    error_display.html('<strong> Erreur : </strong> Seulement les lettres, les entiers, les espaces et les caractères spéciaux -_.& sont autorisés dans le nom du fichier. Minimum de 5 caractères.');
                    error_display.show();
                }
                if (filename.length > 45) {
                    error_display.html('<strong> Erreur : </strong> Le nom du fichier ne doit pas dépasser 45 caractères.');
                    error_display.show();
                }

                if ($.inArray(extension, window.app_data.extensions) === -1) {
                    error_display.html('<strong> Erreur : </strong> Les fichiers ' + extension + ' ne sont pas autorisés.');
                    error_display.show();
                }
                log('[newModal][input][change][extensionsFusion]: '+ $.inArray(extension, window.app_data.extensionsFusion))
                if ($.inArray(extension, window.app_data.extensionsFusion) === -1) {
                    newModal.find("input[type='checkbox'][name*='fusion']:first").prop('checked', false).closest('div.form-group').hide();
                } else {
                    newModal.find("input[type='checkbox'][name*='fusion']:first").prop('checked', true);
                    newModal.find("input[type='checkbox'][name*='fusion']:first").closest('div.form-group').show();
                    log('[newModal][input][change][extensionsCtrl][closest] '+ newModal.find("input[type='checkbox'][name*='fusion']:first").closest('div.form-group').html())
                }
                var inputJoindreCtrl = newModal.find("input[type='checkbox'][name*='ctrl']:first");
                var checkJoindreCtrl = inputJoindreCtrl.prop('checked');
                var selectTypologiePieces = newModal.find("select[name*='typologiepiece_code']:first");

                log('[newModal][input][change][extensionsCtrl]: '+ $.inArray(extension, window.app_data.extensionsCtrl))
                if ($.inArray(extension, window.app_data.extensionsCtrl) === -1) {
                    log('[newModal][input][change][extensionsCtrl]: hide')
                    inputJoindreCtrl.prop('checked', false).closest('div.form-group').hide();
                    inputJoindreCtrl.closest('div.form-group').hide();
                    selectTypologiePieces.val(null).trigger('change');
                    selectTypologiePieces.closest('div.form-group').hide();
                } else {
                    log('[newModal][input][change][extensionsCtrl]: show');
                    inputJoindreCtrl.closest('div.form-group').show();
                    if (checkJoindreCtrl && !_.isEmpty($("#DeliberationNumPref").val())) {
                        selectTypologiePieces.closest('div.form-group').show();
                    }
                }
            }
        });

        //une fois dans la popup on attend le clic de joindre au contrôle de légalité
        log('modalAnnexeAdd[data-wd-projet-id=' + projet_id + ']checkJoindreCtrl');
        newModal.find("input[name*='ctrl']:not([type='hidden'])").on('change', function () {
            var newModal = $(this).closest('div.wd-modal-annexe-add');

            var checkJoindreCtrl = $(this).prop('checked');
            var $selectTypologiePieces = newModal.find("select[name*='typologiepiece_code']:first");

            log('modalAnnexeAdd[checkJoindreCtrl] value: ' + checkJoindreCtrl);
            if (checkJoindreCtrl && !_.isEmpty($("#DeliberationNumPref").val())) {
                initTypologiePieces($selectTypologiePieces, 'Annexe', $('#DeliberationTypeacteId').val(), $('#DeliberationNumPref').val());
                $selectTypologiePieces.closest('div.form-group').show();
            } else {
                $selectTypologiePieces.val(null).trigger('change');
                $selectTypologiePieces.closest('div.form-group').hide();
            }
        });

        //une fois dans la popup on attend le clic de confirmation d'ajout de l'annexe
        newModal.find('.wd-modal-btn-annexe-add-confirm').on('click', function () {
            var newModal = $(this).closest('div.wd-modal-annexe-add');
            var numAnnexe = newModal.find('.wd-modal-annexe-add-annexe-position').val();
            var projetId = newModal.find('.wd-modal-annexe-add-projet-id').val();
            var keyAnnexe = projetId + '_' + numAnnexe;

            var error_display = newModal.find('.wd-modal-annexe-add-erreur');
            //Purge de l'erreur
            // Recherche
            if (newModal.find('input[type=file]:first').val() === '') {
                error_display.html('<strong> Erreur : </strong> Impossible d\'ajouter l\'annexe, aucun fichier n\'a été sélectionné !');
                error_display.show();
            }

            //Test sur le nom de fichier (>75 car.)
            log('input#Annexe' + keyAnnexe + 'Titre');
            var tmpArray = newModal.find('input#Annexe' + keyAnnexe + 'Titre').val().split('\\');
            var filename = tmpArray[tmpArray.length - 1];
            if (filename.length > 75) {
                error_display.html('<strong> Erreur : </strong>Le Titre ne doit pas dépasser 75 caractères. !');
            }

            if (newModal.find('.wd-modal-annexe-add-erreur').html() === '') {
                //on ajoute l'annexe à la liste des annexes rattachés
                ajouterAnnexeLigne(projet_id, numAnnexe);
                newModal.attr('data-wd-annexe-position', numAnnexe);
                newModal.attr('id', keyAnnexe);
                //$("<div/>").attr('id', keyAnnexe).appendTo($('div[data-wd-projet-id=' + projet_id + '].wd-annexe-add-data'));

                // $.each(['input'], function (key, value) {
                //   newModal.find(value).each(function () {
                //     $(this).appendTo($('#' + keyAnnexe));
                //   });
                // });
                log('input#Annexe#TypologiepieceCode' + $('select#Annexe' + keyAnnexe + 'TypologiepieceCode option:selected').val());
                if (!_.isEmpty($('select#Annexe' + keyAnnexe + 'TypologiepieceCode option:selected').val())) {
                    $('<input type="hidden" name="data[Annexe]['+ keyAnnexe +'][typologiepiece_code]" value="'+ $('select#Annexe' + keyAnnexe + 'TypologiepieceCode option:selected').val() +'" />').appendTo($('#' + keyAnnexe));
                } else {
                    $('<input type="hidden" name="data[Annexe]['+ keyAnnexe +'][typologiepiece_code]" value="" />').appendTo($('#' + keyAnnexe));
                }
                newModal.modal('hide');
                newModal.removeClass('wd-modal-annexe-add');
                //closeAnnexeModal(projet_id, numAnnexe);
                //log($('div[data-wd-projet-id=' + projet_id + '].wd-annexe-add-data').html());
            } else {
                error_display.show();
            }
        });

        //Suppression de la modal si évenement "closed"
        newModal.on('hidden.bs.modal', function (e) {
            var newModal = $(this).closest('div.modal');
            if (_.isEmpty(newModal.attr('data-wd-annexe-position'))) {
                log('suppression de la modal');
                newModal.remove();
            }
            //closeAnnexeModal( $(this).find('.wd-modal-annexe-add-projet-id').val(), $(this).find('.wd-modal-annexe-add-annexe-position').val())
        });
    }

    /**
     * Ferme la fenêtre modale d'ajout d'annexe
     */
    function closeAnnexeModal(projet_id, annexe_num)
    {

        var newModal = $('div.wd-annexe-add-data div[data-wd-projet-id=' + projet_id + '].wd-modal-annexe-add');

        //unbind event
        newModal.find("input[name*='ctrl']:not([type='hidden'])").off('change');
        newModal.find('.wd-modal-btn-annexe-add-confirm').off('click');

        var annexe_key = projet_id + '_' + annexe_num
        $.each(['id', 'name', 'for'], function (key, value) {
            newModal.find("[" + value + "*='" + annexe_key + "']").each(function () {
                $(this).attr(value, $(this).attr(value).replace(annexe_key, 'TemplateAnnexeNum'));
            });
        });

        newModal.find("input[type=checkbox]").each(function () {
            $(this).prop('checked', false)
        });
        newModal.find("input[type=text]").each(function () {
            $(this).val('');
        });

        newModal.find("input[type=file]:first").filestyle('clear');
        newModal.find('.wd-modal-annexe-add-annexe-position').val('');
        newModal.find('input').attr('disabled','disabled');
        // /newModal.find('select[class*="select2"]').select2("destroy");
        newModal.find('select').attr('disabled','disabled');

        newModal.modal('hide');
    }

    /** Fonction de suppression d'une annexe
     *
     * @param {type} annexe_id
     * @returns {undefined}
     */
    function supprimerAnnexe(projet_id, annexe_id)
    {
        var bloc = $('#editAnnexe' + annexe_id);
        bloc.addClass('danger').addClass('aSupprimer').attr('title', 'Annexe à supprimer');
        bloc.find('.annexe-edit-btn').hide();
        bloc.find('.annexe-delete-btn').show();

        var inputDelete =$(document.createElement('input')).attr({
            class: 'wd-annexe-delete',
            name: 'data[AnnexesASupprimer][' + annexe_id + ']',
            type: 'hidden',
            'data-wd-annexe-delete-id': annexe_id,
            value: annexe_id
          })
        $('div[data-wd-projet-id=' + projet_id + '].wd-annexe-delete-data').append(inputDelete);
        log('div[data-wd-projet-id=' + projet_id + '].wd-annexe-delete-data');
    }

    /** Fonction d'annulation de suppression d'une annexe
     *
     * @param {type} annexe_id
     * @returns {undefined}
     */
    function initTypologiePieces($select, $type, $natureId, $codeClassification, $selected)
    {
        log('[initTypologiePieces][type]' + $type);
        log('[initTypologiePieces][natureId]' + $natureId);
        $.ajax({
            type: 'POST',
            data: {
                'type': $type,
                'typeacte_id': $natureId,
                'num_pref': $codeClassification,
                'projet_id': $("#DeliberationId") ? $("#DeliberationId").val() : null,
                'typologiepiece_code': $selected
            },
            url: '/teletransmettre/getTypologiePiecesAjax',
            beforeSend: function () {
                $select.closest('div.form-group').hide();
                $select.html('');
                //$select.find("option").remove();
                $select.removeAttr("disabled");
                $select.trigger('change');
            },
            success: function (options) {
                if (!_.isEmpty(options)) {
                    var newOption = new Option('', '', false, false);
                    $select.append(newOption).trigger('change');

                    $.each(options, function (key, option) {
                        log('initTypologiePieces[option.name] value:' + option.name);
                        log('initTypologiePieces[option.value] value:' + option.value);
                        log('initTypologiePieces[option.selected] value:' + option.selected);
                        var newOption = new Option(option.name, option.value, false, option.selected);
                        $select.append(newOption).trigger('change');
                    });

                    $select.closest('div.form-group').show();
                }
            },
            complete: function () {
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                log(XMLHttpRequest);
                log(errorThrown);
                log(textStatus);
            }
        });
    }

    /** Fonction d'annulation de suppression d'une annexe
     *
     * @param {type} annexe_id
     * @returns {undefined}
     */
    function annulerSupprimerAnnexe(annexe_id)
    {
        var bloc = $('#editAnnexe' + annexe_id);
        bloc.removeClass('danger').removeClass('aSupprimer').removeAttr('title');
        bloc.find('.annexe-delete-btn').hide();
        bloc.find('.annexe-edit-btn').show();
        // Delete
        $('input[type=hidden][data-wd-annexe-delete-id=' + annexe_id+'].wd-annexe-delete').remove();
        log('input[type=hidden][data-wd-annexe-delete-id=' + annexe_id+'].wd-annexe-delete');
    }

    /** Fonction d'annulation de la modification de l'annexe
     *
     * @param {type} annexeId
     * @returns {undefined}
     */
    function annulerModifierAnnexe(projet_id, annexe_id)
    {
        var bloc = $('#editAnnexe' + annexe_id);

        bloc.find('#modifieAnnexeTitre' + annexe_id).val(bloc.find('#afficheAnnexeTitre' + annexe_id).attr('data-valeur-init'));
        bloc.find('#modifieAnnexeCtrl' + annexe_id).prop('checked', bloc.find('#afficheAnnexeCtrl' + annexe_id).attr('data-valeur-init'));
        bloc.find('#modifieAnnexeFusion' + annexe_id).prop('checked', bloc.find('#afficheAnnexeFusion' + annexe_id).attr('data-valeur-init'));

        if (bloc.find('#modifieAnnexeCtrl' + annexe_id).prop('checked')) {
            bloc.find('#modifieAnnexeTypologiepieceCode' + annexe_id).select2("destroy");
        }
        bloc.find('#modifieAnnexeTypologiepieceCode' + annexe_id).prop('disabled', 'disabled');


        // on remet la position par defaut de l'annexe en le switchant de la même maniere
        // qu'au changement de position
        // on prends l'id de l'annexe dont on souhaite changer la position
        // et celui de destination, ainsi que leur position initiale respective
        deplacerAnnexePosition(projet_id, annexe_id, $('#editAnnexe' + annexe_id).attr('data-wd-projet-position-init'));

        bloc.removeClass('warning').removeClass('aModifier').removeAttr('title');

        bloc.find('.annexe-edit').hide();
        bloc.find('.annexe-cancel-btn').hide();
        bloc.find('.annexe-view').show();
        bloc.find('.annexe-edit-btn').show();

    }

    /**
     *
     * @param {type} id
     * @returns {undefined}
     */
    function reset_html(id)
    {
        $('#' + id + ' input[type=file]').val(null);
        $('#' + id + ' a').remove();
    }

    //AFFICHE LE SELECTEUR DE FICHIER EN CAS DE SUPPRESSION
    function afficherFichierSelecteur(type)
    {
        $('.editer_fichier_' + type).hide();
        $('.choisir_fichier_' + type).show();
        $('.choisir_fichier_' + type + ' input:file').filestyle('disabled', true); //FIX disabled
        $('.choisir_fichier_' + type + ' input:file').filestyle('disabled', false);
    }

    /** Fonction de modification de l'annexe
     *
     * @param {type} annexe_id
     * @returns {undefined}
     */
    function modifierAnnexe(annexe_id)
    {

        var bloc = $('#editAnnexe' + annexe_id);

      //Activation conditionnelle des checkboxes fusion et ctrl_legalite selon extension annexe
        var fileext = bloc.find('.annexefilename').text().split('.').pop().toLowerCase();
        if ($.inArray(fileext, window.app_data.extensionsFusion) === -1) {
            bloc.find('#modifieAnnexeFusion' + annexe_id).prop('checked', false).prop('disabled', 'disabled');
            bloc.find('#modifieAnnexeFusion' + annexe_id + '_').prop('checked', false).prop('disabled', 'disabled');
        } else {
            bloc.find('#modifieAnnexeFusion' + annexe_id).prop('disabled', false);
            bloc.find('#modifieAnnexeFusion' + annexe_id + '_').prop('disabled', false);
        }
        if ($.inArray(fileext, window.app_data.extensionsCtrl) === -1) {
            bloc.find('#modifieAnnexeCtrl' + annexe_id).prop('checked', false).prop('disabled', 'disabled');
            bloc.find('#modifieAnnexeCtrl' + annexe_id + '_').prop('checked', false).prop('disabled', 'disabled');
        } else {
            bloc.find('#modifieAnnexeCtrl' + annexe_id).prop('disabled', false);
            bloc.find('#modifieAnnexeCtrl' + annexe_id + '_').prop('disabled', false);

            $selectTypologiePieces = bloc.find("select[name*='typologiepiece_code']:first");
            $selectTypologiePieces.select2();
            if (bloc.find('#modifieAnnexeCtrl' + annexe_id).prop('checked') && !_.isEmpty($("#DeliberationNumPref").val())) {
                $selectTypologiePieces.closest('div.form-group').show();
                initTypologiePieces($selectTypologiePieces, 'Annexe', $('#DeliberationTypeacteId').val(), $('#DeliberationNumPref').val(), $('#afficheTypologiepieceCode' + annexe_id).attr('data-valeurinit'))
            } else {
                $selectTypologiePieces.val(null).trigger('change');
              //$selectTypologiePieces.select2("destroy");
                $selectTypologiePieces.closest('div.form-group').hide();
            }

            bloc.find("input[id='" + 'modifieAnnexeCtrl' + annexe_id + "']:not([type='hidden'])").click(function () {
                var bloc = $(this).closest('tr.aModifier');
                var checkJoindreCtrl = $(this).prop('checked');
                $selectTypologiePieces = bloc.find("select[name*='typologiepiece_code']:first");
                $selectTypologiePieces.select2();
                if (checkJoindreCtrl && !_.isEmpty($("#DeliberationNumPref").val())) {
                    $selectTypologiePieces.closest('div.form-group').show();
                    initTypologiePieces($selectTypologiePieces, 'Annexe', $('#DeliberationTypeacteId').val(), $('#DeliberationNumPref').val())
                } else {
                    $selectTypologiePieces.val(null).trigger('change');
                  //$selectTypologiePieces.select2("destroy");
                    $selectTypologiePieces.closest('div.form-group').hide();
                }
            });
        }

        bloc.find('#modifieAnnexeTitre' + annexe_id).prop('disabled', false);
        bloc.find('#modifieAnnexePosition' + annexe_id).prop('disabled', false);

        bloc.find('.urlWebdavAnnexeHidden').show();
        bloc.find('.urlWebdavAnnexeEdit').show();


        bloc.addClass('warning').addClass('aModifier').attr('title', 'Annexe à modifier');
        bloc.find('.wd-projet-annexe-select').prop("disabled", false);
        bloc.find('input[type=hidden]').prop("disabled", false);

        bloc.find('.annexe-edit').each(function () {
            $(this).show();
        });

        bloc.find('.annexe-view').hide();
        bloc.find('.annexe-edit-btn').hide();
        bloc.find('.annexe-edit').show();
        bloc.find('.annexe-cancel-btn').show();
    }

    function newFileStyle(input_type_file)
    {
        $(input_type_file).each(function () {
            var options = {
                'input' : $(this).attr('data-input') === 'false' ? false : true,
                'icon' : $(this).attr('data-icon') === 'false' ? false : $(this).attr('data-icon'),
                'buttonBefore' : $(this).attr('data-buttonBefore') === 'true' ? true : false,
                'disabled' : $(this).attr('data-disabled') === 'true' ? true : false,
                'size' : $(this).attr('data-size'),
                'text' : $(this).attr('data-text'),
                'buttonName' : $(this).attr('data-buttonName'),
                /*'iconName' : $(this).attr('data-iconName'),*/
                'badge' : $(this).attr('data-badge') === 'false' ? false : true,
                'placeholder': $(this).attr('data-placeholder'),
                'btnClass': $(this).attr('data-btnClass'),
            };
            $(this).filestyle(options);
          // Hack filestyle buttonText
          // $this.parent().find('label[for="' + $this.attr('id') + '"] span').text($this.attr('data-buttonText'));
        });

    }

    // Fonction d'ajout d'une nouvelle deliberation : duplique le div ajouteMultiDelibTemplate et incrémente l'indexe
    function ajouterMultiDelib(iMultiDelibAAjouter)
    {

        log('ajouterMultiDelib[iMultiDelibAAjouter]'+ iMultiDelibAAjouter);
        //Clone le bloc template
        var newDelibBloc = $('.delibRattacheeAddTemplate[data-wd-projet-add-id=TemplateProjetId]').clone(true);

        newDelibBloc.attr('data-wd-projet-add-id', newDelibBloc.attr('data-wd-projet-add-id').replace('TemplateProjetId', iMultiDelibAAjouter));

        $.each(['id', 'name', 'aria-labelledby', 'aria-controls', 'data-target', 'data-wd-projet-id', 'projet_id', 'data-wd-projet-add-id'], function (key, value) {
            newDelibBloc.find("[" + value + "*='TemplateProjetId']").each(function () {
                $(this).attr(value, $(this).attr(value).replace('TemplateProjetId', iMultiDelibAAjouter));
            });
        });
        newDelibBloc.find('input[type=hidden].wd-modal-annexe-add-projet-id').val(iMultiDelibAAjouter);

        //Add newFileStyle
        newDelibBloc.find("input[type=file]:first").prop('disabled', false);
        newDelibBloc.find("input[type=file]:first").attr('data-disabled', 'false');
        newFileStyle(newDelibBloc.find("input[type=file]:first"));
        //Add newFileStyle clear
        newDelibBloc.find('.wd-filestyle-clear').attr('data-wd-filestyle-id', 'Multidelib' + iMultiDelibAAjouter + 'Deliberation');
        //active disabled
        newDelibBloc.find('textarea').prop('disabled', false);
        // Annexes
        // //Add newFileStyle clear
        newDelibBloc.find('#ajouteAnnexesRef').attr('id', 'ajouteAnnexesdelibRattachee' + iMultiDelibAAjouter);
        newDelibBloc.find('#tableAnnexesdelibRattacheeTemplate').attr('id', 'tableAnnexesdelibRattachee' + iMultiDelibAAjouter);
        newDelibBloc.find('#supprimeAnnexesdelibRattacheeTemplate').attr('id', 'supprimeAnnexesdelibRattachee' + iMultiDelibAAjouter);
        newDelibBloc.find('#ajouteAnnexesdelibRattacheeTemplate').attr('id', 'ajouteAnnexesdelibRattachee' + iMultiDelibAAjouter);
        newDelibBloc.find('#delibRattacheeTemplate').attr('id', 'delibRattachee' + iMultiDelibAAjouter);
        newDelibBloc.find('#btnAnnexeModal').attr('data-ref', 'delibRattachee' + iMultiDelibAAjouter);

        //if a default template exists, we hide the text selector
        if (newDelibBloc.find('.gabarit_acte_multidelib').val() == '1') {
            newDelibBloc.find('.selectTexteActeGabarit').hide();
            newDelibBloc.find('.editTexteActeGabarit').show();
            newDelibBloc.find('.selectTexteActeGabarit input').prop('disabled',true);
            newDelibBloc.find('.editTexteActeGabarit input').prop('disabled',false);
            // $newDelibBloc.find(' #Multidelib' + iMultiDelibAAjouter + 'Gabarit').prop('disabled',false);
            // $newDelibBloc.find(' #Multidelib' + iMultiDelibAAjouter + 'Deliberation').prop('disabled', true);
        } else {
            newDelibBloc.find('.selectTexteActeGabarit').show();
            newDelibBloc.find('.editTexteActeGabarit').hide();
            newDelibBloc.find('.selectTexteActeGabarit input').prop('disabled',false);
            newDelibBloc.find('.editTexteActeGabarit input').prop('disabled',true);
            // $newDelibBloc.find(' #Multidelib' + iMultiDelibAAjouter + 'Gabarit').prop('disabled',true);
            // $newDelibBloc.find(' #Multidelib' + iMultiDelibAAjouter + 'Deliberation').prop('disabled', false);
        }

        newDelibBloc.find('#annexeModalAddLinkdelibRattacheeTemplate')
            .attr('id', 'annexeModalAddLinkdelibRattachee' + iMultiDelibAAjouter)
            .attr('data-ref', 'delibRattachee' + iMultiDelibAAjouter);

        //Ajout au DOM
        $('#ajouteMultiDelib').append(newDelibBloc);
        //Effet d'apparition
        newDelibBloc.slideDown('slow');
        //Scroll jusqu'au nouveau bloc
        $('html, body').animate({
            scrollTop: newDelibBloc.offset().top
        }, 'slow');
    }

    /**
     * Supprimer le bloc de la nouvel delib rattachée
     * => avec animation
     * @param element
     */
    function annulerAjouterDelibRattachee(element)
    {
        $(element).closest('.panel-group').slideUp('slow', function () {
            $(this).remove();
        });
    }

    /**
     * Supprime le gabarit de la multidelib lors de la création
     * ou efface le champs file si gabarit vide
     * @param id
     */
    function supprimerGabaritMultidelibCreation(projet_id)
    {
        log('#Multidelib' + projet_id + 'GabaritBloc');
        var bloc = $('#Multidelib' + projet_id + 'GabaritBloc');
        bloc.find('.editTexteActeGabarit').hide();
        bloc.find('.selectTexteActeGabarit').show();

        bloc.find("input[type=file]").prop('disabled', false);
        bloc.find("input[type=file]").attr('data-disabled', 'false');
        newFileStyle(bloc.find("input[type=file]"));
        bloc.find('.selectTexteActe input').show();
    }


    /**
     * Supprime le gabarit de la multidelib ou efface le champs file si gabarit vide
     * @param id
     */
    function supprimerGabaritMultidelib(id)
    {
        $('#Multidelib' + id + 'GabaritBloc').hide();
        $('#Multidelib' + id + 'Gabarit').prop('disabled', true);
        $('input#Multidelib' + id + 'Deliberation').prop('disabled', false).show().val(null);
        $('#supprimerMultidelib' + id + 'Gabarit').attr('title', 'Réinitialiser la sélection');
    }

    // Fonction de modification d'une délibération rattachée
    function modifierDelibRattachee(projet_id)
    {
        var bloc = $('#delibRattachee' + projet_id);
        log('modifierDelibRattachee[projet_id] ' + projet_id);

        bloc.find('legend span.label').addClass('label-warning').text('Modification');

        bloc.find('#delibRattacheeDisplay' + projet_id).hide();
        bloc.find('#delibRattacheeForm' + projet_id).show();

        //On active les champs du formulaire
        bloc.find('#Multidelib' + projet_id + 'Id').prop('disabled', false);
        bloc.find('#Multidelib' + projet_id + 'ObjetDelib').prop('disabled', false);


        //si le systeme d'edition de fichier est affiché
        //on disable le champ de selection
        if (!_.isUndefined(bloc.find('.editTexteActe').html())) {
            log('modifierDelibRattachee[wd-projet-multi-texte-deliberation-file] ' + bloc.find('.wd-projet-multi-texte-deliberation-file').attr('disabled'));
            bloc.find('.wd-projet-multi-texte-deliberation-file').prop('disabled', false);
        } else {
            //bloc.find('#Multidelib' + projet_id + 'Deliberation').prop('disabled', false);
            bloc.find(".selectTexteActe input[type=file]").prop('disabled', false);
            bloc.find(".selectTexteActe input[type=file]").attr('data-disabled', 'false');
            log('modifierDelibRattachee[.selectTexteActe input[type=file]] ' + bloc.find(".selectTexteActe input[type=file]").attr('disabled'));
            newFileStyle(bloc.find(".selectTexteActe input[type=file]:first"));
            bloc.find('.selectTexteActe input').show();
        }

        //$('#Multidelib' + projet_id + 'ObjetDelib').val($('#Multidelib' + projet_id + 'libelle').text());

        bloc.find('.actions-multidelib').hide();

        //On affiche le panel header en succes
        bloc.addClass('panel-success');

        //Affichage des boutons
        bloc.find('.multiDeliberationModifier').hide();
        bloc.find('.multiDeliberationModifierAnnuler').show();
        bloc.find('.multiDeliberationSupprimerAnnuler').hide();

        //On affiche le formulaire de l'accordeon
        bloc.find('#collapse' + projet_id).addClass('in');

    }

    // Fonction d'annulation des modifications d'une délibération rattachée
    function annulerModifierDelibRattachee(projet_id)
    {
        //On recupere la deliberation ratachée selectionné
        var bloc = $('#delibRattachee' + projet_id);

        //Affichage des boutons
        bloc.find('.multiDeliberationModifier').show();
        bloc.find('.multiDeliberationModifierAnnuler').hide();

        //On disable tout les contenus*


        $.each(['input', 'textarea'], function (key, value) {
            bloc.find(value).each(function () {
                $(this).prop('disabled', true);
                log("annulerModifierDelibRattachee:"+ value +"[disabled]" + $(this));
            });
        });

        //On cache le formulaire de l'accordeon
        bloc.find('#collapse' + projet_id).removeClass('in');

        //On remet l'affichage du fond du header par defaut
        bloc.removeClass('panel-success');

        bloc.find('#delibRattacheeDisplay' + projet_id).show();
        bloc.find('#delibRattacheeForm' + projet_id).hide();

        bloc.find('legend span.label').removeClass('label-warning').text('Visualisation');

        //On desactive les champs du formulaire
        bloc.find('#Multidelib' + projet_id + 'Id').prop('disabled', true);
        bloc.find('#Multidelib' + projet_id + 'Deliberation').prop('disabled', true);
       // $bloc.find('#Multidelib' + projet_id + 'ObjetDelib').prop('disabled', true).val($('#Multidelib' + projet_id + 'libelle').text());

        var tabAnnexe = $('#tableAnnexesdelibRattachee' + projet_id);
        tabAnnexe.find('tr').each(function () {
            if ($(this).hasClass('aSupprimer')) {
                annulerSupprimerAnnexe($(this).attr('data-wd-projet-id'), $(this).attr('data-wd-projet-annexe-id'));
            }
            if ($(this).hasClass('aModifier')) {
                annulerModifierAnnexe($(this).attr('data-wd-projet-id'), $(this).attr('data-wd-projet-annexe-id'));
            }
        });

        bloc.find('.actions-multidelib').show();
    }

    // Fonction de suppression d'une délibération rattachée
    function supprimerDelibRattachee(projet_id)
    {
        $('#collapseHeading' + projet_id).addClass('danger');
        var bloc = $('#delibRattachee' + projet_id);
        bloc.addClass('panel-danger');
        bloc.find('legend span.label').addClass('label-important').text('Supprimer');
        bloc.find('#MultidelibASupprimer' + projet_id).prop('disabled', false);
        bloc.find('#delibRattacheeDisplay' + projet_id).slideUp('slow');
        bloc.find('.actions-multidelib').hide();
        bloc.find('.multiDeliberationSupprimerAnnuler').show();
        bloc.find('.multiDeliberationModifierAnnuler, .multiDeliberationModifier, .multiDeliberationSupprimer').hide();

        //On cache le formulaire de l'accordeon
        bloc.find('#collapse' + projet_id).removeClass('in');
    }

    // Fonction de d'annulation de suppression d'une annexe
    function annulerSupprimerDelibRattachee(id)
    {
        //On recupere la deliberation ratachée selectionné
        var $bloc = $('#delibRattachee' + id);

        //on affiche les boutons modifier et supprimer
        $bloc.find('.multiDeliberationModifier').show();
        $bloc.find('.multiDeliberationSupprimer').show();
        $bloc.find('.multiDeliberationModifierAnnuler').hide();
        $bloc.find('.multiDeliberationSupprimerAnnuler').hide();

        //On reset le panel header en default
        $bloc.removeClass('panel-success');
        $bloc.removeClass('panel-danger');

        //On desactive les champs du formulaire en attendant une modication
        $bloc.find('#Multidelib' + id + 'Id').prop('disabled', true);
        $bloc.find('#Multidelib' + id + 'Deliberation').prop('disabled', true);
        $bloc.find('#Multidelib' + id + 'ObjetDelib').prop('disabled', true);//.val($('#Multidelib' + id + 'libelle').text());
    }

    //Fonction permettant d'annuller la suppression
    //du fichier texte acte de la deliberation rattachée
    function annulerSupprimerTextDelibRattachee(id)
    {
        //on annule la suppression du fichier utilisé
        $('#delibRattachee' + id + ' .editTexteActe').show();
        $('#delibRattachee' + id + ' .selectTexteActe').hide();
        //on met le champ du fichier en disbaled pour eviter qu'a l'envoi le fichier soit ecrasé
        $('#Multidelib' + id + 'Deliberation').prop('disabled',true);

        //on met le champ du fichier en enabled pour qu'a l'envoi le fichier soit ecrasé
        $('#Multidelib' + id + 'Deliberation').prop('disabled',true);
        $('.annulerSupprimerDelibRattachee' + id).hide();
    }

      // Fonction de suppression du texte de délibération sous forme de fichier joint
    function supprimerTextDelibDelibRattachee(id)
    {
        $('#delibRattachee' + id + ' .editTexteActe').hide();
        var bloc = $('#delibRattachee' + id + ' .selectTexteActe');
        //on met le champ du fichier en enabled pour qu'a l'envoi le fichier soit ecrasé
        $('#Multidelib' + id + ' input').prop('disabled',false);
        $('.annulerSupprimerDelibRattachee' + id).show();
        //on le champs qui desactive la mise en place de gabarit par defaut dans la bdd
        $('#Multidelib' + id + 'Gabarit').prop('disabled',true);

        bloc.find("input[type=file]:first").prop('disabled', false);
        bloc.find("input[type=file]:first").attr('data-disabled', 'false');
        newFileStyle(bloc.find("input[type=file]:first"));

        bloc.show();
    }

        // Init
    $.fn.appProjet.init = function (options) {

        var object = $.fn.appProjet.settings = $.extend({}, $.fn.appProjet.defaults, options),
                $self;

        $self = $("#" + object.id);
        //Ne pas initialisé le code si l'objet Name n'existe pas dans le DOM
        if (!$self.is("*")) {
            return;
        }

        //Gestion des multidélibérations
        $('#listeTypeactesId').on("change", function () {
            if ($.inArray(parseInt($(this).val()), window.app_data.allowedMulti) === -1) {
                $('#DeliberationIsMultidelib').prop('checked', false).parent().hide();
            } else {
                $('#DeliberationIsMultidelib').parent().show();
            }
        });

        onUnloadEditForm();

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            if ( $('#DeliberationId').length === 0 ) {
                $('#appProjet').append('<input type="hidden" name="nameTab" value="' + $(e.target).attr('href').replace('#', '') + '" />');
                postsubmit();
                $('#appProjet').submit();
            }
        });
        $(".wd-boutonValider").on('click', function (evt) {
            postsubmit();
            evt.stopPropagation();
            $(window).unbind("beforeunload");
            $('#appProjet').submit();
        });

        function postsubmit()
        {
            $('#DeliberationTypeseance option').each(function () {
                $(this).removeAttr("disabled");
            });
            //Pour la sauvegarde des séances
            $('#DeliberationSeance option').each(function () {
                $(this).removeAttr("disabled");
            });
            $("#DeliberationTypeacteId").removeAttr('disabled');

            $('.wd-boutonValider').attr('disabled', 'disabled');
        }

        //Pour ne pas avoir la confirmation de sortie sur les liens dans le projet
        $("form#appProjet a").on('click', function () {
            disableExitWarning();
        });
        $("#boutonAnnuler").on('click', function () {
            disableExitWarning();
        });

        $('#DeliberationTypeacteId').on('select2:select', function (e) {
            $.ajax({
                type: 'POST',
                data: {
                    "projet_id": $("#DeliberationId") ? $("#DeliberationId").val() : null,
                    "typeacte_id": $(this).val()
                },
                url: '/deliberations/getTypeseancesParTypeacteAjax',
                beforeSend: function () {
                    $("#selectTypeseances").hide();
                    $("#selectTypeseances option").remove()
                    $("#selectDatesSeances").hide();
                    $("#selectDatesSeances option").remove()
                },
                success: function (options) {
                    if (!_.isEmpty(options)) {
                        var data = _.map(options, function (option) {
                            return {id: option.value,
                                text: option.name,
                                disabled: option.disabled,
                                selected: option.selected}
                        });

                        $('#DeliberationTypeseance').select2("destroy");
                        $('#DeliberationTypeseance').select2({
                            data: data
                        });
                        setTimeout(function () {
                            hideClear();
                        }, 0);
                    }
                },
                complete: function () {
//                    //Pour confimer des sélections
//                    $('#DeliberationTypeacteId option').each(function(){
//                        $(this).removeAttr('selected');
//                        log($(this).text());
//                    });
//                    log($("#DeliberationTypeacteId option[selected='selected']").text());
//                    $("#DeliberationTypeacteId option[value=" + $(this).val() + "]").attr('selected', 'selected');
//                    log($("#DeliberationTypeacteId option[selected='selected']").text());
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    log(XMLHttpRequest);
                    log(errorThrown);
                    log(textStatus);
                }
            });
            if (!_.isEmpty($("#DeliberationNumPref").val())) {
                log('[#DeliberationNumPref]' + $("#DeliberationNumPref").val());
                initTypologiePieces($("#DeliberationTypologiepieceCode"), 'Principale', $('#DeliberationTypeacteId').val(), $("#DeliberationNumPref").val());
            }
        });

        $('#DeliberationTypeseance').on('select2:select', function (e) {
            //Pour confimer des sélections
            _.map($(this).val(), function (value) {
                $("#DeliberationTypeseance option[value=" + value + "]").attr('selected', 'selected');
            });
            $('#selectTypeseances .select2-search__field').hide(); //Pour ne pas supprimer les selections
            $.ajax({
                type: 'POST',
                url: '/deliberations/getSeancesParTypeseanceAjax',
                data: {
                    "projet_id": $("#DeliberationId") ? $("#DeliberationId").val() : null,
                    "typeseances_ids": $(this).val()
                },
                beforeSend: function () {
                },
                success: function (options) {
                    if (!_.isEmpty(options)) {
                        var data = _.map(options, function (option) {
                            return {id: option.value,
                                text: option.name,
                                disabled: option.disabled,
                                selected: option.selected}
                        });
                        $('#DeliberationSeance').select2("destroy");
                        $('#DeliberationSeance').select2({
                            data: data
                        });
                        setTimeout(function () {
                            //FIX dans une prochaine amélioration de select2
                            _.map(options, function (option) {
                                $("#DeliberationSeance option[value=" + option.value + "]").attr('data-wd-typeseance-id', option['data-wd-typeseance-id']);
                            });
                            hideClear();
                        }, 0);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    log(XMLHttpRequest);
                    log(errorThrown);
                    log(textStatus);
                }
            });
        });
        $('#selectTypeseances .select2-search--inline, #selectDatesSeances .select2-search--inline').on('keydown', function (evt) {
            evt.stopPropagation();
            evt.isDefaultPrevented();
            evt.preventDefault();
        });

        //Suppression des types de séance
        $('#DeliberationTypeseance').on("select2:unselect", function (e) {
            //Rendre le type de séance sélectionnable
            e.params.data.disabled = false
            //FIX select de base mais pourquoi !
            $("#DeliberationTypeseance option[value=" + e.params.data.id + "]").removeAttr('selected');
            // Suppression des séances
            $("#DeliberationSeance option[data-wd-typeseance-id=" + e.params.data.id + "]:not(:selected)").remove();

            setTimeout(function () {
                hideClear();
            }, 0);
        });

        $('#DeliberationSeance').on("select2:select", function (e) {
            $('#selectDatesSeances .select2-search__field').hide(); //Pour ne pas supprimer les selections
            $('#DeliberationSeance .select2-search__field').hide();
            $("#DeliberationTypeseance option[value=" + $("#DeliberationSeance option[value=" + e.params.data.id+"]").attr('data-wd-typeseance-id') + "]").attr("disabled", "disabled");
        });


        $('#DeliberationSeance').on("select2:unselect", function (e) {
            log("select2:unselect[data][id]" + e.params.data.id);
            log("select2:unselect[data][disabled]" + e.params.data.disabled);

            //FIX select de base mais pourquoi !
            $("#DeliberationSeance option[value=" + e.params.data.id + "]").removeAttr("selected");
            var selectedTypeseanceId = $("#DeliberationSeance option[value=" + e.params.data.id+"]").attr('data-wd-typeseance-id');
            var nbrSeanceSelected = 0;
            $('#DeliberationSeance option').each(function () {
                if ($(this).attr('selected') && $(this).attr('data-wd-typeseance-id') === selectedTypeseanceId) {
                    nbrSeanceSelected++;
                }
            });
            log("select2:unselect[nbrSelectedTypeseanceId]" + nbrSeanceSelected);
            if (nbrSeanceSelected == 0) {
                //disabled type seance
                $("#DeliberationTypeseance option[value=" + selectedTypeseanceId + "]").removeAttr("disabled");
                //disabled seance action = 0
                $('#DeliberationSeance option[data-wd-typeseance-action=0]').each(function () {
                    $(this).removeAttr("disabled");
                });
            }
        });

        $('#DeliberationTypeseance').on("change", function (e) {
            log('#DeliberationTypeseance[change]');
            setTimeout(function () {
                log('#DeliberationTypeseance[hideClear]');
                hideClear();
            }, 0);
        });
        $('#DeliberationSeance').on("change", function (e) {
            log('#DeliberationSeance[change]');
            setTimeout(function () {
                log('#DeliberationSeance[hideClear]');
                hideClear();
            }, 0);
        });

        setTimeout(function () {
                hideClear();
        }, 0);

        function hideClear()
        {
            if ($('#DeliberationTypeseance option').size()>0) {
                $('#DeliberationTypeseance option').each(function () {
                    if ($(this).attr('disabled')=='disabled') {
                        $('#selectTypeseances li[title="'+ $(this).text() +'"] span.select2-selection__choice__remove').hide();
                    } else {
                        $('#selectTypeseances li[title="'+ $(this).text() +'"] span.select2-selection__choice__remove').show();
                    }
                });
                $("#selectTypeseances").show();
            } else {
                $("#selectTypeseances").hide();
            }
            //Si un type d'acte est sélectionné on ne peut plus changer le type d'acte
            if ($('#DeliberationTypeseance option[selected="selected"]').size()>0) {
                $("#DeliberationTypeacteId").attr('disabled', 'disabled').trigger("change");
            } else {
                $("#DeliberationTypeacteId").removeAttr('disabled').trigger("change");
            }

            if ($('#DeliberationSeance option').size()>0) {
                $('#DeliberationSeance option').each(function () {
                    if ($(this).attr('disabled')=='disabled') {
                        log('hideClear[hide]' + $(this).text());
                        $('#selectDatesSeances li[title="'+ $(this).text() +'"] span.select2-selection__choice__remove').hide();
                    } else {
                        log('hideClear[show]' + $(this).text());
                        $('#selectDatesSeances li[title="'+ $(this).text() +'"] span.select2-selection__choice__remove').show();
                    }
                });
                //reload a select2
                var select2Instance = $('#DeliberationSeance').data('select2');
                var resetOptions = select2Instance.options.options;
                $('#DeliberationSeance').select2('destroy').select2(resetOptions);

                $("#selectDatesSeances").show();
            } else {
                $("#selectDatesSeances").hide();
            }

        }

        $('.wd-projet-texte').on('click', function () {
            afficherFichierSelecteur($(this).attr('data-wd-projet-texte-type'));
        });

        $('.wd-projet-annexe-edit').on('click', function () {
            modifierAnnexe($(this).attr('data-wd-projet-annexe-id'));
        });

        $('.wd-projet-annexe-edit-exit').on('click', function () {
            annulerModifierAnnexe($(this).attr('data-wd-projet-id'), $(this).attr('data-wd-projet-annexe-id'));
        });

        $('.wd-projet-annexe-delete').on('click', function () {
            supprimerAnnexe($(this).attr('data-wd-projet-id'),$(this).attr('data-wd-projet-annexe-id'));
        });

        $('.wd-projet-annexe-delete-exit').on('click', function () {
            annulerSupprimerAnnexe($(this).attr('data-wd-projet-annexe-id'));
        });

    //AFFICHE l'onglet Annexe(s) ou Délibération rattachées selon la valeur de la checkbox multi-deliberation
        $('#DeliberationIsMultidelib').on("change", function () {
            var libelle = $('#DeliberationObjet[data-wd-libelle=deliberation]');
            var libelle_multi = $('#DeliberationObjet[data-wd-libelle=multi-deliberation]');
            if ( $(this).prop('readonly') === true) {
                $(this).prop('checked', true);
           //For edit
                if ($('#DeliberationId').is("*")) {
                      libelle.prop('disabled', true);
                      libelle_multi.prop('disabled', false);
                      libelle.parent().parent().hide();
                }
                $('.nav-tabs li a[role=tab][href$="#multidelib"]').parent().show();
                $('.nav-tabs li a[role=tab][href$="#annexes"]').parent().hide();
                return;
            }
            log('[#DeliberationIsMultidelib]' + $(this).prop('checked'))
            if ( $(this).prop('checked') === true) {
            //For edit
                if ($('#DeliberationId').is("*")) {
                    libelle.prop('disabled', true);
                    libelle_multi.prop('disabled', false);
                    libelle_multi.val(libelle.val());
                    libelle.parent().parent().hide();
                }
            //on affiche le dernier onglet( deliberations rattachées )
            //et on cache l'avant dernier ( annexe(s) )
                $('.nav-tabs li a[role=tab][href$="#multidelib"]').parent().show();
                $('.nav-tabs li a[role=tab][href$="#annexes"]').parent().hide();
            } else {
                if ($('#DeliberationId').is("*")) {
                        libelle.prop('disabled', false);
                        libelle_multi.prop('disabled', true);
                        libelle.val(libelle_multi.val());
                        libelle.parent().parent().show();
                }
                $('.nav-tabs li a[role=tab][href$="#multidelib"]').parent().hide();
                $('.nav-tabs li a[role=tab][href$="#annexes"]').parent().show();
            }
        });

        $("#DeliberationIsMultidelib").trigger("change");

        if ( !_.isEmpty($("#DeliberationTypeacteId option[selected='selected']").text())
        && !_.isEmpty($("#DeliberationNumPref").val())) {
            log('[#DeliberationIsMultidelib]' + $("#DeliberationTypeacteId option[selected='selected']").text());
            initTypologiePieces($("#DeliberationTypologiepieceCode"), 'Principale', $('#DeliberationTypeacteId').val(), $('#DeliberationNumPref').val())
        }

    /**
     * Action déclenchée lors du clic sur le bouton "Ajouter une annexe"
     * @param element reference du lien cliqué (gestion du multi-délib)
     */
        $('.wd-modal-btn-annexe-add').click(function (event) {
            // modal add new annexe
            modalAnnexeAdd($(this).attr('data-wd-projet-id'));
        });

    // modifier l'ordre des annnexes pour l'annexe principale et les déliberations rattachéees
        $('.wd-projet-annexe-select').on("select2:select", function () {
            //on prends l'id de l'annexe dont on souhaite changer la position
            //et celui de destination, ainsi que leur position initiale respective
            //on deplace la ligne du tableau
            ////$(this).closest('tr').attr('data-wd-projet-position'),
            deplacerAnnexePosition(
                $(this).attr('data-wd-projet-id'),
                $(this).attr('data-wd-projet-annexe-id'),
                $(this).val()
            );

        });

    // on ordonne la position des annexes de déliberation principale
    //reordonnerPosition('delibPrincipale');

        $(this).find("input[accept='application/vnd.oasis.opendocument.text']").change(function () {

            if ($(this).val() !== '') {
                var tmpArray = $(this).val().split('.');
                //Test sur l'extension (ODT ?)
                var extension = tmpArray[tmpArray.length - 1];
                if (extension.toLowerCase() !== 'odt') {
                    $.growl(
                        {
                            title: "<strong>Erreur: </strong>",
                            message: 'Format du document invalide. Seuls les fichiers au format ODT sont autorisés.'
                            },
                        {type: "danger"}
                    );
                    $(this).val(null);
                    return false;
                }

                //Test sur le nom de fichier (>75car)
                var tmpArray = $(this).val().split('\\');
                var filename = tmpArray[tmpArray.length - 1];
                if (filename.length > 75) {
                    $.growl(
                        {
                            title: "<strong>Erreur: y</strong>",
                            message: 'Le nom du fichier ne doit pas dépasser 75 caractères.'
                            },
                        {type: "danger"}
                    );
                    $(this).val(null);
                    return false;
                }
            }
        });

    // Supprimer le texte de délibération du projet
        $('.multiDeliberationSupprimerTextDeliberation').on('click', function () {
            supprimerTextDelibDelibRattachee($(this).attr('data-wd-projet-id'));
        });
    // Annuler les modifications du projet
        $('.multiDeliberationModifierAnnuler').on('click', function () {
            annulerModifierDelibRattachee($(this).attr('data-wd-projet-id'));
            annulerSupprimerTextDelibRattachee($(this).attr('data-wd-projet-id'));
        });
    // Modification le projet
        $('.multiDeliberationModifier').on('click', function () {
            modifierDelibRattachee($(this).attr('data-wd-projet-id'));
        });
    // Annuler la suppression du projet
        $('.multiDeliberationSupprimerAnnuler').on('click', function () {
            annulerSupprimerDelibRattachee($(this).attr('data-wd-projet-id'));
        });
    // Supprimer le projet
        $('.multiDeliberationSupprimer').on('click', function () {
            supprimerDelibRattachee($(this).attr('data-wd-projet-id'));
        });

        $('#ajouterMultiDelib').on('click', function (e) {
            log('#ajouterMultiDelib[object.iMultiDelibAAjouter]' + object.iMultiDelibAAjouter);
            ajouterMultiDelib(object.multiDelibId + object.iMultiDelibAAjouter);
            object.iMultiDelibAAjouter++;
        });

        $('#annulerAjouterDelibRattachee').on('click', function () {
            annulerAjouterDelibRattachee();
        });

        $('.wd-projet-multi-gabarit-delete').on('click', function () {
            supprimerGabaritMultidelibCreation($(this).attr('data-wd-projet-add-id'));
        });


        $("#DeliberationIsMultidelib").change(function () {
            if ($(this).prop("checked")) {
                $('#lienTab5').show();
                $('#htextedelib').hide();
                $('#lienTab3').hide();
                $('#delibPrincipaleAnnexeRatt').append($('#DelibPrincipaleAnnexes').detach());
                $('#texteDelibOngletDelib').append($('#texteDeliberation').detach());
                if (_.isUndefined($('#ajouteMultiDelib'))) {
                    $('#ajouteMultiDelib').innerHTML('');
                }
            } else {
                $('#lienTab3').show();
                $('#lienTab5').hide();
                $('#htextedelib').show();
                $('#DelibOngletAnnexes').append($('#DelibPrincipaleAnnexes').detach());
                $('#texteDelibOngletTextes').append($('#texteDeliberation').detach());
            }
        }).change();

        $("#DeliberationEditForm").submit(function () {
            $('#ajouteMultiDelib .libelle-multidelib').each(function () {
                if ($.trim($(this).val()) == '') {
                    $.jGrowl('Erreur : Veuillez renseigner les libellés des délibérations.');
                    return false;
                }
            });
            //on met le champ file de l'ajout de deliberations rattachées en disabled
            $('#MultidelibTemplateDeliberation').filestyle('disabled', true);
        });

    // Lorsque l'on selectionne une classification
        $("#DeliberationNumPref").change(function () {
            if (!_.isEmpty($('#DeliberationTypeacteId').val()) && !_.isEmpty($("#DeliberationNumPref").val())) {
                log('[#DeliberationNumPref]' + $('#DeliberationTypeacteId').val());
                initTypologiePieces($("#DeliberationTypologiepieceCode"), 'Principale', $('#DeliberationTypeacteId').val(), $('#DeliberationNumPref').val());
            }
        });
    //FIX
        $(".wd-modal-btn-annexe-add").attr("type", "button");
        $(".wd-modal-btn-annexe-add").removeAttr("data-toggle");
        $(".wd-modal-btn-annexe-add").removeAttr("data-target");
    }

     // Defaults
    $.fn.appProjet.defaults = {
        id: 'appProjet', // Element ID
        multiDelibId: 'Multidelib-',
        iMultiDelibAAjouter: 1,
        debug: false,
    };

    $.appProjet = $.fn.appProjet;
});
