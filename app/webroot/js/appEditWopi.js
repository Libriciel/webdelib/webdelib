/**
 * Comportement des checkboxes / master checkbox
 * Select All / Deselect All
 * Initial state
 * @version 4.3
 */

define(["jquery"], function ($) {
    'use strict';

    // Main function
    $.fn.appEditWopi = function (options) {
        // Ensure that only one scrollUp exists
        if (!$.data(document.body, 'appEditWopi')) {
            $.data(document.body, 'appEditWopi', true);
            $.fn.appEditWopi.init(options);
        }
    };

    // Init
    $.fn.appEditWopi.init = function (options) {

        var object = $.fn.appEditWopi.settings = $.extend({}, $.fn.appEditWopi.defaults, options), $self;

        $self = $('#' + object.id);

        /**
         * Action déclenchée lors du clic sur le bouton "Ajouter une annexe"
         * @param element reference du lien cliqué (gestion du multi-délib)
         */
        $('.wd-modal-edit-wopi').click(function (event) {
            log('wopi: '+$(this).attr('data-wd-edit-text-id'));
            // modal add new annexe
            modalEdit($(this));
        });
    };

    function modalEdit(edit){
        let id = edit.attr('data-wd-edit-text-id');
        var wopiSrc = edit.attr('data-wd-edit-text-wopi-src');

        var wopiClientUrl = edit.attr('data-wd-edit-text-wopi-client-url');
        if (!wopiClientUrl) {
            log('error: wopi client url not found');
            return;
        }

        var wopiUrl = wopiClientUrl + '?WOPISrc=' + wopiSrc;

        var formElem = document.getElementById("collabora-submit-form");
        if (!formElem) {
            log('error: submit form not found');
            return;
        }

        formElem.action = wopiUrl;
        formElem.submit();

        //$('#modalOffice').modal('show');
    }

    function log(log){
        if ($.fn.appEditWopi.settings.debug) {
            console.log(log);
        }
    }

    // Defaults
    $.fn.appEditWopi.defaults = {
        id: 'appEditWopi',
        debug: false
    };

    $.appEditWopi = $.fn.appEditWopi;
});
