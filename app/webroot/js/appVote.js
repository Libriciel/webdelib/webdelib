define(['jquery'], function ($) {
    'use strict';

    // Main function
    $.fn.appVote = function (options) {
        // Ensure that only one appVote xists
        if (!$.data(document.body, 'appVote')) {
            $.data(document.body, 'appVote', true);
            $.fn.appVote.init(options);
        }
    };

    // Init
    $.fn.appVote.init = function (options) {

        var object = $.fn.appVote.settings = $.extend({}, $.fn.appVote.defaults, options),
                $self;

        //Ne pas initialisé le code si l'objet Name n'existe pas dans le DOM
        if(!$("." + object.Name).is("*")){
            return;
        }

        var parentElement = $("#DeliberationListerPresencesGlobaleForm, #DeliberationListerPresentsForm");
        $(".app-selectPresent").select2({
            //width: "element",
            dropdownParent: parentElement,
            templateSelection: function (object) {
                // trim sur la sélection (affichage en arbre)
                return $.trim(object.text);
            }
        });

        vote();
        numbersOfVoters('#nombreVotants');
        numbersOfVoters('#NombreTotalVoix');


        // Affichage du type de saisie du vote
        // $("#VoteTypeVote").val(1);
        affichageTypeVote();
        //erase des checkbox du tableau des votes
        $('.class-wd-vote-erase').on('click', function () {
            $('#'+ $(this).attr('data-wd-vote-erase-id') +' input[type=radio]').removeAttr('checked');
        });

         // Intialisation du décompte des voix
         $( ".class-wd-vote-racc" ).on('change', function () {
             vote_global($(this), $(this).attr('data-wd-vote-type'));
         });

         // Intialisation du décompte des voix
         $( "#voteDetail" ).on('change', function () {
             vote();
         });

         // Listeneur sur le select onchange
         $( "#VoteTypeVote" ).change(function() {
             affichageTypeVote();
         });

         // Listeneur sur le nombre de votes onchange
         $( '#tableDetailVote input[type=number]').on('change', function () {
             numbersOfVoters('#NombreTotalVoix');
         });

         // Listeneur comptant le nombre de caractéres sur le textarea commentaire
        $('#DeliberationVoteCommentaire').textcounter({
            max                      : 1000,
            countSpaces              : true,
            twoCharCarriageReturn    : true,
            countExtendedCharacters  : true,
            counterText              : "(max. %d / 1000 caractères)",
            init                     : function(el){
                $('.text-count-wrapper').attr('class','text-center');
            }
        });

        //affiche ou cache la liste des mendataires selon si l'élu est present ou pas
        $( "#DeliberationListerPresencesGlobaleForm input:checkbox, #DeliberationListerPresentsForm input:checkbox" ).click(function() {
            if( $(this).is(":checked") === true )
            {
                $('#liste_Acteur' + $(this).attr('data-wd-acteur-id') + 'Present').prop("disabled", true);
                $('#liste_Acteur' + $(this).attr('data-wd-acteur-id') + 'Present').val(null).trigger("change");
            }
            else
            {
                $('#liste_Acteur' + $(this).attr('data-wd-acteur-id') + 'Present').prop("disabled", false);
            }
        });

    };

    function affichageTypeVote() {
        $('#voteDetail, #voteTotal, #voteResultat, #votePrendsActe').hide();
        switch ($("#VoteTypeVote").val()) {
            case '1':
                $('#voteDetail').show();
                break;
            case '2':
                $('#voteTotal').show();
                break;
            case '3':
                $('#voteResultat').show();
                break;
            case '4':
                $('#votePrendsActe').show();
                break;
        }
    }

    /**
     * Affiche le nombre de votants => Nombre de votes renseignés par l'agent.
     */
    function numbersOfVoters(idCell) {

        var numbersOfVoter = 0;

        if (idCell === "#nombreVotants" ) { // detail des voix
            numbersOfVoter =  parseInt($('#VoteRes3').val()) + // Nombre de votant oui
                parseInt($('#VoteRes2').val()) + // Nombre de votant non
                parseInt($('#VoteRes4').val()) + // Nombre d'abstention
                parseInt($('#VoteRes5').val());  // Nombre de présent qui ne participe pas au vote
        }else if(idCell === '#NombreTotalVoix' ) { // affichage Global du nombre de voix
            numbersOfVoter =
                parseInt($('#DeliberationVoteNbOui').val()) + // Nombre de votant oui
                parseInt($('#DeliberationVoteNbNon').val()) + // Nombre de votant non
                parseInt($('#DeliberationVoteNbAbstention').val())+ // Nombre qui s'abtiennent.
                parseInt($('#DeliberationVoteNbRetrait').val()) ;  // Nombre de présent qui ne participe pas au vote
        }
        if(isNaN(numbersOfVoter)){numbersOfVoter = '0';}

        $(idCell).html('Total des saisies : ' + numbersOfVoter + '/' + $('#tableDetailVote input[type=radio][name^=data][value="3"]').length);
    }

    function vote(){
        majTotauxVotes();
        numbersOfVoters('#nombreVotants');
    }

    /**
     * gestion des raccourcis pour le vote
     * @argument {objet} obj
     * @argument {objet} scope
     */
    function vote_global(obj, scope){
        var name = $(obj).attr('name');
        var valChecked = $('#tableDetailVote input[type=radio][name='+name+']:checked').val();

        if (scope === 'tous') {
            $('#tableDetailVote input[type=radio][name^=racc_typeacteur][value='+valChecked+']').prop('checked', true);
            $('#tableDetailVote input[type=radio][name^=data][value='+valChecked+']').prop('checked', true);
        } else {
            $('#tableDetailVote tr.'+scope+' input[type=radio][name^=data][value='+valChecked+']').prop('checked', true);
            $('#tableDetailVote input[type=radio][name=racc_tous]:checked').prop('checked', false);
        }
        majTotauxVotes();
    }

    /**
     * mise à jour des totaux des votes
     */
    function majTotauxVotes() {
        $('#VoteRes3').val($('#tableDetailVote input[type=radio][name^=data][value=3]:checked').length);
        $('#VoteRes2').val($('#tableDetailVote input[type=radio][name^=data][value=2]:checked').length);
        $('#VoteRes4').val($('#tableDetailVote input[type=radio][name^=data][value=4]:checked').length);
        $('#VoteRes5').val($('#tableDetailVote input[type=radio][name^=data][value=5]:checked').length);
    }

    // Defaults
    $.fn.appVote.defaults = {
        Name: 'appVote' // Element ID
    };


    $.appVote = $.fn.appVote;
});
