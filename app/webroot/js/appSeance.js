define(['jquery'], function ($) {
  'use strict';

  // Main function
  $.fn.appSeance = function (options) {
    // Ensure that only one appSeance exists
    if (!$.data(document.body, 'appSeance')) {
      $.data(document.body, 'appSeance', true);
      $.fn.appSeance.init(options);
    }
  };

  // Init
  $.fn.appSeance.init = function (options) {

    var object = $.fn.appSeance.settings = $.extend({}, $.fn.appSeance.defaults, options);
    var $self;

    $self = $('#' + object.id);

    //Ne pas initialisé le code si l'objet Name n'existe pas dans le DOM
    if (!$self.is('*')) {
      return;
    }

    //Lors d'action sur un select de class "select_rapporteur":
    $('.select_rapporteur').change(function () {
        document.location = '/seances/changeRapporteur/'
        + $(this).attr('data-seance-id') + '/' + $(this).attr('data-delib-id') + '/'
        + $(this).val();
      }
    );

    $('.wd-change-position').on('change', function () {
        document.location = '/seances/changePosition/'
        + $(this).attr('data-wd-seance-id') + '/' + $(this).val() + '/'
        + $(this).attr('data-wd-projet-id');
      }
    );

    //Lors d'action sur une checkbox :
     $('input[class*="masterCheckbox_checkbox"]').on('change', function () { selectionChange(); });

    $('#SeanceModelId').on('change', function () {
        selectionChange();
      }
    ).trigger('change');

    $('#signatureManuscrite').on('click', function () {
        $(this).closest('form').attr('data-waiter-title', 'Opération de signature en cours');
      }
    );
    $('#signatureParapheur').on('click', function () {
        $(this).closest('form').attr('data-waiter-title', 'Envoi du dossier au Parapheur');
      }
    );

    /**
     * Actions au chargement de la page
     */
    $('#genereNumActe').addClass('disabled');
    $('#genereNumActe').prop('disabled', true);
    $('#signatureManuscrite').addClass('disabled');
    $('#signatureManuscrite').prop('disabled', true);
    $('#autresActesAbandon').prop('disabled', true);
    $('input[type="checkbox"].masterCheckbox_checkbox').change(changeSelection);
    changeSelection();

  };

  function selectionChange() {
    var nbChecked = $('input[type=checkbox].masterCheckbox_checkbox:checked').length;

    //Apposer ou non la class disabled au bouton selon si des checkbox sont cochées (style)
    if (nbChecked > 0 && $('#SeanceModelId').val() != '') {
      $('#generer_multi_seance').removeClass('disabled');
      $('#generer_multi_seance').prop('disabled', false);
    } else {
      $('#generer_multi_seance').addClass('disabled');
      $('#generer_multi_seance').prop('disabled', true);
    }

    $('#nbSeancesChecked').text('(' + nbChecked + ')');
  }

  /**
   * Afficher/Masquer la sélection de circuit selon si la selection est vide ou non
   */
  function changeSelection() {

    $.each($('div[data-step]'), function () {
        $(this).remove();
      });

    var nbChecked = $('input[type=checkbox].masterCheckbox_checkbox:checked').length;
    $('#nbSeancesChecked').text('(' + nbChecked + ')');
    //Apposer ou non la class disabled au bouton selon si des checkbox sont cochées (style)
    if (nbChecked > 0) {
      $('#select-circuit').show();
      $('#genereNumActe').removeClass('disabled');
      $('#genereNumActe').prop('disabled', false);
      $('#signatureManuscrite').removeClass('disabled');
      $('#signatureManuscrite').prop('disabled', false);
    } else {
      $('#select-circuit').hide();
      $('#genereNumActe').addClass('disabled');
      $('#genereNumActe').prop('disabled', true);
      $('#signatureManuscrite').addClass('disabled');
      $('#signatureManuscrite').prop('disabled', true);
    }
  }

  // Defaults
  $.fn.appSeance.defaults = {
    id: 'appSeance',
  };

  $.appSeance = $.fn.appSeance;
});
