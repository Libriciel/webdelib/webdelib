// Importer le CSS de Bootstrap
import '@libriciel/ls-bootstrap-5/wd-bootstrap.css';

// Importer les scripts JavaScript de Bootstrap
import 'bootstrap';

import "@fortawesome/fontawesome-free/js/all.js";
import "@fortawesome/fontawesome-free/css/all.css";