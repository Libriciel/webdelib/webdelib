require(['domReady', 'appMain'], function (domReady) {
    //console.log( 'script main.js' + new Date().getTime() );
    domReady(function() {
        //console.log( 'appAttendable! fired at ' + new Date().getTime() );
        var downloadTimer;
        $(".waiter").appAttendable();

        //console.log( 'appFilter! fired at ' + new Date().getTime() );
        $("#boutonBasculeCriteres").appFilter();

        //console.log( 'appMasterCheckbox! fired at ' + new Date().getTime() );
        $.appMasterCheckbox();


        // filestyle
        // clear
        //nofilestyle
        $(".wd-nofilestyle").filestyle('destroy');
        $('.wd-filestyle-clear').on('click', function (e) {
          $attr = $(this).attr('data-wd-filestyle-id');
          $('#' + $attr).filestyle('clear');
        });
        $('#CollectiviteBackground').colorpicker();

        /**
        * Composant pour le retour en haut de page
        */
       $.scrollUp({
           scrollTitle: 'Remonter en haut de la page',
           scrollImg: true,
           scrollDistance: 300, // Distance from top/bottom before showing element (px)
           scrollSpeed: 500, // Speed back to top (ms)
           animation: 'fade', // Fade, slide, none
           animationInSpeed: 500, // Animation in speed (ms)
           animationOutSpeed: 500 // Animation out speed (ms)
       });

        $.fn.select2.defaults.set( "theme", "bootstrap" );
        $.fn.select2.defaults.set( "minimumResultsForSearch", 5);
        $.fn.select2.defaults.set('amdLanguageBase', 'select2/i18n/');
        //$.fn.select2.defaults.set('debug', true);

        $('.selectmultiple').select2({
            //dropdownAutoWidth: true,
            templateSelection: function (object) {
                // trim sur la sélection (affichage en arbre)
                return $.trim(object.text);
            }
        });

        $(".selectone").select2({
            //width: "element",
            templateSelection: function (object) {
                // trim sur la sélection (affichage en arbre)
                return $.trim(object.text);
            }
        });

        $('.dropdown-submenu > a').submenupicker();
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover({ html : true });


        $('.wd-autocomplete-geojson').autocomplete({
            serviceUrl: '/infosups/getGeoJsonFromApi',
            dataType: 'json',
            minChars: 5,
            params: {
                'model': '',
                'id': ''
            },
            onSearchStart: function (params) {
                $('#' + $(this).attr('wd-geojson')).val('');
                params.model = $(this).attr('data-wd-infosupdef-model');
                params.id = $(this).attr('data-wd-infosupdef-id');
                // console.log($(this).attr('data-wd-infosupdef-model'));
                // console.log($(this).attr('data-wd-infosupdef-id'));
                //$(this).prop( "disabled", true );

                if($(this).nextAll('.autocomplete-feedback').length === 0) {
                    $(this)
                        .after('<div class="autocomplete-feedback"><div class="loader"><span class="sr-only">Chargement...</span></div></div>')
                        .parent('.form-group')
                        .addClass('has-feedback');
                    $(this).addClass('autocomplete-is-typing');
                }
            },
            onSearchComplete: function (params) {
                //$(this).prop( "disabled", false );
                $(this).removeClass('autocomplete-is-typing');
                $(this).nextAll('.autocomplete-feedback').remove();
            },
            showNoSuggestionNotice:true,
            noSuggestionNotice: 'Aucun résultat',
            preventBadQueries: false,
            transformResult: function(response) {
                $(this).next('.autocomplete-feedback').remove();
                if(response.hasOwnProperty('label') === true) {
                    labelAddress = response.label;
                }
                if(response.hasOwnProperty('features') === true && Array.isArray(response.features) === true) {
                    return {
                        suggestions: $.map(response.features, function(dataItem) {
                            return { value: dataItem.properties[labelAddress], data: dataItem};
                        })
                    };
                } else {
                    return {
                        suggestions: { suggestions: [] }
                    };
                }
            },

            onSelect: function(suggestion) {
                $(this).next('.autocomplete-feedback').remove();
                $('#' + $(this).attr('wd-geojson')).val(JSON.stringify(suggestion.data));
            }
        });



        //Démarrage des modules
        $.appVote();
        $.appProjet();
        $.appSearch();
        $.appCompteur();
        $.appUser();
        $.appSeance();
        $.appConnecteur();
        $.appAbandon();
        $.appSendTdt();
        $.appTypologie();
        $.appEditWopi();
        $.appPassword();

        ////////////////////////////////////////////////////
        //filtre des dates ( tableau de bord + historique //
        ////////////////////////////////////////////////////
        /**
         * Créé par Florian Ajir, le 05/03/14.
         *
         * Instructions javascript à exécuter au lancement de toutes les pages
         *
         * A partir de la date passée, on va renvoyer une nouvelle date
         * en fonction de la combobox et de l'écart passé. L'utilisation
         * d'une variable date permet de gérer les fin de mois, jour, ...
         *
         * @param {type} idModel date de départ au format yyyy-mm-dd hh:mm:ss
         * @returns {String} retourne la date correctement formaté avec l'écarrt voulu
         */
        function modifierPlageDateTime(idModel) {
            //creation de la liste des id des champs de filtre a cibler
            var idDifDate = '#' + idModel.replace('#', 'DifDate');
            var idDateFin = '#' + idModel.replace('#', 'DateFin');
            var idDateDebut = '#' + idModel.replace('#', 'DateDebut');

            // si on n'a pas de date de fin, on inverse le signe de l'opérateur
            // pour remplir la date de fin selon la plage

            var operator = 1;
            if($(idDifDate).val().substring(0,1)=='-'){
                operator = -(operator);
            }

            var dateSource = new Date();
            var newDate = new Date();

            //selon le signe de l'opérateur on modifie la date de but ou de fin
            if( operator < 0 && $(idDateFin).val()){
                dateSource = $(idDateFin).datetimepicker('getDate');
                newDate = new Date(dateSource);
            }else if(operator > 0 && $(idDateDebut).val()){
                dateSource = $(idDateDebut).datetimepicker('getDate');
                newDate = new Date(dateSource);
            }

            switch (true) {
                //1 heure
                case new RegExp("hour").test($(idDifDate).val()):
                    newDate.setHours(dateSource.getHours() + operator);
                    break;

                //1 jour
                case new RegExp("day").test($(idDifDate).val()):
                    newDate.setDate(dateSource.getDate() + operator);
                    break;

                //1 mois
                case new RegExp("month").test($(idDifDate).val()):
                    newDate.setMonth(dateSource.getMonth() + operator);
                    break;

                //1 an
                case new RegExp("year").test($(idDifDate).val()):
                    newDate.setFullYear(dateSource.getFullYear() + operator);
                    break;

                //1 semaine
                case new RegExp("week").test($(idDifDate).val()):
                    newDate.setDate((dateSource.getDate() - 6) + operator);
                    break;
            }
            //selon le signe de l'opérateur, on modifie la date de début ou de fin
            if( operator > 0 ){
              $(idDateDebut).datetimepicker('setDate', dateSource);
              $(idDateFin).datetimepicker('setDate', newDate);
            }else{
                $(idDateDebut).datetimepicker('setDate', newDate);
                $(idDateFin).datetimepicker('setDate', dateSource);
            }
        }

        $('#CritereDifDate, #CritereDateDebut, #CritereDateFin').on('change', function () {
            // on génère la date de début avec le temps de la plage choisie dans la liste
            if($('#CritereDateFin').val()  && $('#CritereDifDate').val()){
                $('#CritereDateDebut').val($('#CritereDateFin').val());
                modifierPlageDateTime('Critere#');
            }
            // on ajoute à la date de début le temps de la plage
            else if ($('#CritereDateDebut').val() && $('#CritereDifDate').val()){
                modifierPlageDateTime('Critere#');
            }
            // si la plage a été choisie, alors on remplit la date de fin
            // pour effectuer le calcul de la date de début
            else if($('#CritereDifDate').val()){
                $("#CritereDateFin").datetimepicker("setDate", new Date());
                modifierPlageDateTime('Critere#');
            }

        });

        /////////////////////////////////////////////////
        // filtres des dates pour la recherche global ///
        /////////////////////////////////////////////////

        // filtre des dates projet
        $('#DeliberationDifDate, #DeliberationDateDebut, #DeliberationDateFin').on('change', function () {
            // on génère la date de début avec le temps de la plage choisie dans la liste
            if($('#DeliberationDateFin').val()  && $('#DeliberationDifDate').val()){
                $('#DeliberationDateDebut').val($('#DeliberationDateFin').val());
                modifierPlageDateTime('Deliberation#');
            }
            // on ajoute à la date de début le temps de la plage
            else if ($('#DeliberationDateDebut').val() && $('#DeliberationDifDate').val()){
                modifierPlageDateTime('Deliberation#');
            }
            // si la plage a été choisie, alors on remplit la date de fin
            // pour effectuer le calcul de la date de début
            else if($('#DeliberationDifDate').val()){
                $("#DeliberationDateFin").datetimepicker("setDate", new Date());
                modifierPlageDateTime('Deliberation#');
            }
        });


        // filtre des dates en préfecture
        $('#DeliberationDifDateAr, #DeliberationDateDebutAr, #DeliberationDateFinAr').on('change', function () {
            // on génère la date de début avec le temps de la plage choisie dans la liste
            if($('#DeliberationDateFinAr').val()  && $('#DeliberationDifDateAr').val()){
                $('#DeliberationDateDebutAr').val($('#DeliberationDateFinAr').val());
                modifierPlageDateTime('Deliberation#Ar');
            }
            // on ajoute à la date de début le temps de la plage
            else if ($('#DeliberationDateDebutAr').val() && $('#DeliberationDifDateAr').val()){
                modifierPlageDateTime('Deliberation#Ar');
            }
            // si la plage a été choisie, alors on remplit la date de fin
            // pour effectuer le calcul de la date de début
            else if($('#DeliberationDifDateAr').val()){
                $("#DeliberationDateFinAr").datetimepicker("setDate", new Date());
                modifierPlageDateTime('Deliberation#Ar');
            }
        });

        // filtre des dates en préfecture
        $('.searchInfosupInterval, .searchInfosupDateDebut, .searchInfosupDateFin').on('change', function () {
            var infosupdefId = $(this).attr('data-wd-infosups-id');
            // on génère la date de début avec le temps de la plage choisie dans la liste
            if($("#Infosup" + infosupdefId +'DateFin').val()  && $("#Infosup" + infosupdefId +'DifDate').val()){
                $("#Infosup" + infosupdefId +'DateDebut').val($("#Infosup" + infosupdefId +'DateFin').val());
                modifierPlageDateTime("Infosup" + infosupdefId +"#");
            }
            // on ajoute à la date de début le temps de la plage
            else if ($("#Infosup" + infosupdefId +'DateDebut').val() && $("#Infosup" + infosupdefId +'DifDate').val()){
                modifierPlageDateTime("Infosup" + infosupdefId +"#");
            }
            // si la plage a été choisie, alors on remplit la date de fin
            // pour effectuer le calcul de la date de début
            else if($("#Infosup" + infosupdefId +'DifDate').val()){
                $("#Infosup" + infosupdefId +'DateFin').datetimepicker("setDate", new Date());
                modifierPlageDateTime("Infosup" + infosupdefId +"#");
            }
        });

        $(".wd-infosup-file").on("click", function (){
            infoSupSupprimerFichier($(this).attr("data-wd-infosup-infosupdef-code"));
        });

        function infoSupSupprimerFichier(infoSupCode) {
            $(".choisir_fichier_"+infoSupCode).show();
            $("#infosup_hidden_"+infoSupCode).val(""); //FIX : sinon, le nom du fichier est tout de même posté...
            $(".editer_fichier_"+infoSupCode).hide();
            $(".choisir_fichier_"+ infoSupCode + " input:file").filestyle('disabled', true);
            $(".choisir_fichier_"+ infoSupCode + " input:file").filestyle('disabled', false);

        }

        //    GESTION DES ERREURS
        $('.error-message').each(function() {
            $(this).addClass('alert alert-danger');
        });

        //FIX avec commenaire votre data-charLeftValid-lengthmax
        // Listeneur comptant le nombre de caractéres sur le textarea commentaire
        $('.app-text-counter').each(function(){
            $(this).textcounter({
                max: 1000,
                countSpaces              : true,
                twoCharCarriageReturn    : true,
                countExtendedCharacters  : true,
                counterText : "(max. %d / 1000 caractères)",
            });
        });

    });


    $('.showTexteEnrichi').on('click', function () {
        if ($(this).attr('data-infosup-hide') == 'off') {
            $('#' + $(this).attr('data-infosup-id')).show();
            tinymce.baseURL = $("meta[name='base']").attr('href')+"/js/components/tinymce";
            tinymce.init({
                themes: "modern",
                readonly : true,
                mode : "specific_textareas",
                language_url : $("meta[name='base']").attr('href')+"/js/components/tinymce-i18n/langs/fr_FR.js",
                editor_selector: $(this).attr('data-infosup-id')});

            $(this).attr('data-infosup-hide', 'on');
            $(this).html($(this).attr('data-infosup-hide-on-texte'));
        } else {
            $('.' + $(this).attr('data-infosup-id')).tinymce().remove();
            $('#' + $(this).attr('data-infosup-id')).hide();
            $(this).attr('data-infosup-hide', 'off');
            $(this).html($(this).attr('data-infosup-hide-off-texte'));
        }

    var $select2 =  $(".selectMultipleOrderBySelect").select2();


    /**
     * select2_renderselections
     * @param  {jQuery Select2 object}
     * @return {null}
     */
    function select2_renderSelections($select2){
        var order      = $select2.data('preserved-order') || [];
        var $container = $select2.next('.select2-container');
        var $tags      = $container.find('li.select2-selection__choice');
        var $input     = $tags.last().next();

        // apply tag order
        order.forEach(function (val) {
            var $el = $tags.filter(function (i, tag) {
                return $(tag).data('data').id === val;
            });
            $input.before($el);
        });
    }


    /**
     * selectionHandler
     * @param  {Select2 Event Object}
     * @return {null}
     */
    function selectionHandler(e){
        var $select2  = $(this);
        var val       = e.params.data.id;
        var order     = $select2.data('preserved-order') || [];

        switch (e.type){
            case 'select2:select':
                order[ order.length ] = val;
                break;
            case 'select2:unselect':
                var found_index = order.indexOf(val);
                if (found_index >= 0 )
                    order.splice(found_index,1);
                break;
        }
        $select2.data('preserved-order', order); // store it for later
        select2_renderSelections($select2);
    }

    });
});
