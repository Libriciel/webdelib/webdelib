define(["jquery"], function ($) {



    // Main function
    $.fn.appSearch = function (options) {
        // Ensure that only one scrollUp exists
        if (!$.data(document.body, 'appSearch')) {
            $.data(document.body, 'appSearch', true);
            $.fn.appSearch.init(options);
        }
    };

    $.fn.appSearch.init = function (options) {

        var object = $.fn.appSearch.settings = $.extend({}, $.fn.appSearch.defaults, options),
            $self;
        $self = $("#" + object.id);
        //Ne pas initialisé le code si l'objet avec l'id appSearch'existe pas dans le DOM
        if(!$self.is("*")){
            return;
        }

        $(".class-wd-selectToken").select2({
            theme: "bootstrap",
            tags: true,
            tokenSeparators: [",", " ", ";"],
//            tokenizer: function(input, selection, callback) {
//                if (input.indexOf(',') < 0 && input.indexOf(' ') < 0 && input.indexOf(';') < 0)
//                    return;
//
//                var parts = input.split(/,| |\;/);
//                for (var i = 0; i < parts.length; i++) {
//                    var part = parts[i];
//                    part = part.trim();
//
//                    callback({id:part,text:part});
//                }
//            }
        });


        $('#DeliberationModel').on("select2:select", function (e) {
            if (e.params.data.id>0) {
                $('#submitSearchForm').html('<span class="fa fa-cog"></span> Générer le document');
                $('#submitSearchForm').attr('data-waiter-type', 'progress-bar');
                $('#submitSearchForm').removeAttr('type');
            } else {
                $('#submitSearchForm').html('<span class="fa fa-search"></span> Recherche en cours');
                $('#submitSearchForm').removeAttr('data-waiter-type');
                $('#submitSearchForm').attr('type', 'submit');
            }
        });

        $('#submitSearchForm').click(function () {
            if ($('#submitSearchForm').attr('data-waiter-type') != 'progress-bar') {
                return;
            }
            var form_attributes = $(this).closest("form").clone().get(0).attributes;
            var form_now = $(this).closest("form");

            $.each(form_attributes, function (index, attr) {
                if(!_.isUndefined(attr)) {
                    form_now.removeAttr(attr.name);
                }
            });

            form_now.
            attr('action', '/search/index').
            attr('method', 'post').
            attr('class', 'waiter form-horizontal').
            attr('autocomplete', 'off').
            attr('id', 'Deliberation');

            var find_waiter = new RegExp(/waiter/);
            if(!_.isNull(form_attributes['class']) && !find_waiter.test(form_attributes['class'].value.toLowerCase())){
                form_now.attr('class', form_attributes['class'].value + ' waiter');
            }else{
                form_now.appAttendable({});
            }

            form_now.attr('data-waiter-send', 'XHR').
            attr('data-waiter-title','Génération du document').
            attr('data-waiter-type', 'progress-bar').
            attr('data-waiter-callback', 'getProgress');

            form_now.on('progress_send', function (e) {
                $(this).off('progress_send');
                $.each(($(this).closest('form').clone().get(0).attributes), function( index, attr ) {
                    if(!_.isUndefined(attr)) {
                        form_now.removeAttr(attr.name);
                    }
                });
                $.each(form_attributes, function( index, attr ) {
                    if(!_.isUndefined(attr)) {
                        form_now.attr(attr.name, attr.value);
                    }
                });
            });
        });

        var $select2 =  $(".selectMultipleOrder").select2({
            theme: "bootstrap",
            escapeMarkup: function (markup) {
                return markup;
            }});

        // NOTE - you also have to massage the options and put the chosen persisted options in the correct order at top of the collection
        function preserveOrderOnSelect2Choice(e){
          var id = e.params.data.id;
          var option = $(e.target).children('[value="'+id+'"]');
          option.detach();
          $(e.target).append(option).change();
        }

        $select2.each(function(){
          $(this).on('select2:select', preserveOrderOnSelect2Choice);
        });

        /**
         * defaults: Cache order of the initial values
         * @type {object}
         */
        var defaults = $select2.select2('data');
        defaults.forEach(function (obj) {
            var order = $select2.data('preserved-order') || [];
            order[order.length] = obj.id;
            $select2.data('preserved-order', order);
        });

        /**
         * select2_renderselections
         * @param  {jQuery Select2 object}
         * @return {null}
         */
        function select2_renderSelections($select2){
            var orders      = $select2.data('preserved-order') || [];
            var $container = $select2.next('.select2-container');
            var $tags      = $container.find('li.select2-selection__choice');
            var $input     = $tags.last().next();


            // apply tag order
            if(orders.length>1){
              orders.forEach(function (order) {
                  var $el = $tags.filter(function (i, tag) {
                      return $(tag).data('data').id === order;
                  });
                  $input.before($el);
              });
            }
        }

        /**
         * selectionHandler
         * @param  {Select2 Event Object}
         * @return {null}
         */
        function selectionHandler(e){
            var $select2  = $(this);
            var val       = e.params.data.id;
            var order     = $select2.data('preserved-order') || [];

            log('selectionHandler:'+val);
            switch (e.type){
                case 'select2:select':
                    order[ order.length ] = val;
                    break;
                case 'select2:unselect':
                    var found_index = order.indexOf(val);
                    if (found_index >= 0 )
                        order.splice(found_index,1);
                    break;
            }
            $('#strOrder').val(order);
            $select2.data('preserved-order', order); // store it for later
            select2_renderSelections($select2);
        }

        $select2.on('select2:select select2:unselect', selectionHandler);
    };

    function log(log){
      if ($.fn.appSearch.settings.debug) {
        console.log(log);
      }
    }

     // Defaults
    $.fn.appSearch.defaults = {
        id: 'appSearch', // Element ID
        debug: false,
    };

    $.appSearch = $.fn.appSearch;
});
