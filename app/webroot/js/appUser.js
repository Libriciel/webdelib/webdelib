define(['jquery'], function($) {
  'use strict';

  // Main function
  $.fn.appUser = function(options) {
    // Ensure that only one appUser exists
    if (!$.data(document.body, 'appUser')) {
      $.data(document.body, 'appUser', true);
      $.fn.appUser.init(options);
    }
  };

  // Init
  $.fn.appUser.init = function(options) {

    var object = $.fn.appUser.settings = $.extend({}, $.fn.appUser.defaults, options),
      $self;

    $self = $("#" + object.id);
    //Ne pas initialisé le code si l'objet Name n'existe pas dans le DOM
    if (!$self.is("*")) {
      return;
    }

    //Tableau des notifications
    displayNotification();
    $("#UserAcceptNotif").change(function() {
      displayNotification();
    });

    $("#all_circuits").select2({
      matcher: hideSelected
    });

    onchangeCircuitDefault();
    $('#all_circuits').on('change', function() {
      onchangeCircuitDefault();
    });

    initJsTree();
  };

  function hideSelected(params, data) {

    // `params.term` should be the term that is used for searching
    // `data.text` is the text that is displayed for the data object
    if (data.text.toUpperCase().indexOf($.trim(params.term).toUpperCase()) > -1 ||  $.trim(params.term) === '') {
        var modifiedData = $.extend({}, data, true);
        if (modifiedData.element.selected == true) {
            return null;
        }
        // You can return modified objects from here
        // This includes matching the `children` how you want in nested data sets
        return modifiedData;
    }
    // Return `null` if the term should not be displayed
    return null;
  }

  function initJsTree() {

    var ctrlPressed = false;
    $(window).keydown(function(evt) {
      if (evt.which == 17) { // ctrl
        ctrlPressed = true;
      }
    }).keyup(function(evt) {
      if (evt.which == 17) { // ctrl
        ctrlPressed = false;
      }
    });

    $('#services').jstree({
      /* Initialisation de jstree sur la liste des services */
      "core": { // Paramétrage du coeur du plugin
        "animation": 0, // Pas d'animation (déplier)
        "themes": {
          "stripes": true
        } // Une ligne sur deux est grise (meilleure lisibilité)
      },
      "checkbox": { // Paramétrage du plugin checkbox
        "three_state": false, // Ne pas propager la séléction parent/enfants
      },
      "search": { // Paramétrage du plugin de recherche
        "fuzzy": false, // Indicates if the search should be fuzzy or not (should chnd3 match child node 3).
        "show_only_matches": true, // Masque les résultats ne correspondant pas
        "case_sensitive": false, // Sensibilité à la casse
        "close_opened_onclear": false // Ne pas déselectionner les résultats ne correspondant pas
      },
      "types": {
        "level0": {
          "icon": "fa fa-sitemap"
        },
        "default": {
          "icon": "fa fa-users"
        }
      },
      "contextmenu": {
        "items": contextMenu
      },
      "plugins": [
        "checkbox", // Affiche les checkboxes
        "wholerow", // Toute la ligne est surlignée
        "search", // Champs de recherche d'élément de la liste (filtre)
        "types", // Pour les icônes
        "contextmenu" // Menu clic droit
      ]
    });

    var services = $('#ServiceService').val().split(',');
    services.forEach(function(entry) {
      $('#services').jstree('select_node', 'Service_' + entry);
    });

    $('#services').on('changed.jstree', function(event, data) {
      /* Listener onChange qui fait la synchro jsTree/hiddenField */
      var i, ii, r = [];
      data.instance.open_node(data.node);
      for (i = 0; i < data.selected.length; i++) {
        var idnode = data.instance.get_node(data.selected[i]);

        r.push(idnode.data.id);
        //select all child nodes when parent selected ,
        if (ctrlPressed) {
          var child = data.instance.get_children_dom(idnode);
          for (ii = 0; ii < child.length; ii++) {
            data.instance.check_node(child[ii]);
          }
        }
      }

      if (data.action == 'deselect_node' && ctrlPressed) {
        var child = data.instance.get_children_dom(data.node.id);
        for (ii = 0; ii < child.length; ii++) {
          data.instance.uncheck_node(child[ii]);
        }
      }
      $('#ServiceService').val(r.join(','));
    });

    /* Recherche dans la liste jstree */
    $('#search_service_button').click(function() {
      $('#services').jstree(true).search($('#search_service').val());
    });
    /* Recherche dans la liste jstree */
    $('#search_service_erase_button').click(function() {
      $('#search_service').val('');
      $('#services').jstree(true).clear_search();
    });
    $('#search_service').keydown(function(event) {
      if (event.keyCode === 13) {
        event.preventDefault();
        $('#search_service_button').click();
        return false;
      }
    });
    $("#search_service_plier_button").click(function() {
      $('#services').jstree('close_all');
    });
    $("#search_service_deplier_button").click(function() {
      $('#services').jstree('open_all');
    });
    $("#search_service_cocher_button").click(function() {
      $('#services').jstree('select_all');
    });
    $("#search_service_decocher_button").click(function() {
      $('#services').jstree('deselect_all');
    });
    $("#search_service_ascenceur_button").click(function() {
      var overflow = $('#services').css('overflow');
      if (overflow === 'auto') {
        $('#services')
          .css('overflow', 'hidden')
          .css('max-height', 'none');
        $("#search_service_ascenceur_button").text('Activer défilement');
      } else {
        $('#services')
          .css('overflow', 'auto')
          .css('max-height', '350px');
        $("#search_service_ascenceur_button").text('Désactiver défilement');
      }
    });
  }


  function contextMenu(node) {
    // The default set of all items
    var items = {
      "closeSearch": {
        "label": "Effacer la recherche",
        "action": function() {
          $('#search_service').val('');
          $('#services').jstree(true).clear_search();
        },
        "icon": "fa fa-eraser",
        "separator_after": true
      },
      "selectAll": {
        "label": "Tout cocher",
        "action": function() {
          $('#services').jstree('select_all');
        },
        "icon": "fa fa-check-square"
      },
      "deselectAll": {
        "label": "Tout décocher",
        "action": function() {
          $('#services').jstree('deselect_all');
        },
        "icon": "fa fa-square-o",
        "separator_after": true
      },
      "openAll": {
        "label": "Tout déplier",
        "action": function() {
          $('#services').jstree('open_all');
        },
        "icon": "fa fa-plus-square-o"
      },
      "closeAll": {
        "label": "Tout replier",
        "action": function() {
          $('#services').jstree('close_all');
        },
        "icon": "fa fa-minus-square-o"
      }
    };
    if ($('#search_service').val().length === 0) {
      delete items.closeSearch;
    }
    return items;
  }

  function onchangeCircuitDefault() {
      var selected = $('#default_circuit').val();
      log('onchangeCircuitDefault');
      $('#default_circuit').empty();

    //  $('<option value=""></option>').prependTo('#default_circuit');

      $('#all_circuits').find("option:selected").each(function(_index, element) {
          if ($("#default_circuit option[value='"+ $(element).val() +"']").length < 1){
              $(element).clone().removeAttr('selected').removeAttr('data-select2-id').appendTo('#default_circuit');
          }
      });
      $('#all_circuits').find("option:not(:selected)").each(function(_index, element) {
          if ($("#default_circuit option[value='"+ $(element).val() +"']").length > 0){
              $("#default_circuit option[value='"+ $(element).val() +"']").remove();
          }
      });
      $('#default_circuit').val(selected);
    }

    function displayNotification() {

      var notif = $("#UserAcceptNotif:checked").val();

      if (notif == true) {
        $("#notificationTable").hide();
      } else {
        $("#notificationTable").show();
      }
    }

  function log(log){
    if ($.fn.appUser.settings.debug) {
      console.log(log);
    }
  }

  // Defaults
  $.fn.appUser.defaults = {
    id: 'appUser',
    debug: false,
  };
  $.appUser = $.fn.appUser;
});
