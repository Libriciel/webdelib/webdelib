/**
 * Comportement des checkboxes / master checkbox
 * Select All / Deselect All
 * Initial state
 * @version 4.3
 */

define(["jquery"], function ($) {
    'use strict';

    // Main function
    $.fn.appAbandon = function (options) {
        // Ensure that only one scrollUp exists
        if (!$.data(document.body, 'appAbandon')) {
            $.data(document.body, 'appAbandon', true);
            $.fn.appAbandon.init(options);
        }
    };

    // Init
    $.fn.appAbandon.init = function (options) {

        var object = $.fn.appAbandon.settings = $.extend({}, $.fn.appAbandon.defaults, options),
                $self;

        $self = $("#" + object.id);

        modalStepsStructure();
        $('form.appAbandon select.app-traitementParLot').on("change", function () {
          if(!_.isEmpty($('form.appAbandon select.app-traitementParLot option[value=autresActesAbandon]:selected').val())){
            $('form.appAbandon select.app-traitementParLot option[value=autresActesAbandon]:selected')
              $('form.appAbandon input[type="checkbox"].masterCheckbox_checkbox').change(changeSelection);
              changeSelection();
            }
        });

        $('#DeliberationCommentaire').textcounter({
            max                      : 1500,
            countSpaces              : true,
            twoCharCarriageReturn    : true,
            countExtendedCharacters  : true,
            counterText              : "(max. %d / 1500 caractères)",
            init                     : function(el){
                $('.text-count-wrapper').attr('class','text-center');
            }
        });

    }

    var abandon = function () {
        var data = { Deliberation: [] };
        $.each($("input[name='data[Deliberation][id]'][type='hidden']"), function () {
            data.Deliberation.push({
                id: $(this).val(),
                Commentaire: {
                    texte: $(this).closest('div').find('textarea').val(),
                },
            });
        });

        $.ajax({
            type: "POST",
            url: '/autres_actes/autresActesAbandon',
            data: data,
            success: function(data) {
                        location.reload(); // then reload the page.(3)
            },
        });
    };

    function modalStepsStructure() {
      var checkCommentLength = function () {
          $("textarea[name='data[Deliberation][commentaire]']").on('keyup',
              function () {
                if ($(this).val().length > 1) {
                  $('button[data-orientation="next"]').prop('disabled', false);
                }else {
                  $('button[data-orientation="next"]').prop('disabled', true);
                }
              }
          );
        };

      var out = '<div id="modalSteps" class="modal fade">'
          + '<div class="modal-dialog">'
          + '<div class="modal-content">'
          + '<div class="modal-header">'
          + '<h4 class="js-title-step">'
          + '</h4>'
          + '</div>';

      out += '<div class="modal-body" id="modal-abandon">';
      out += '</div>';            // End body

      out += '<div class="modal-footer">'
          + '<button type="button" class="btn btn-default js-btn-step pull-left" data-orientation="cancel" data-dismiss="modal"></button>'
          + '<button type="button" class="btn btn-warning js-btn-step" data-orientation="previous"></button>'
          + '<button type="button" class="btn btn-success js-btn-step" data-orientation="next"></button>'
          + '</div>';

      out += '</div>'
          + '</div>'
          + '</div>';

      $(out).appendTo('body');

      $('#modalSteps').modalSteps({
          btnCancelHtml: '<span class="fa fa-times"></span> Annuler ',
          btnPreviousHtml: '<span class="fa fa-arrow-left"></span> Retour',
          btnNextHtml: '<span class="fa fa-arrow-right"></span> Suivant',
          btnLastStepHtml: '<span class="fa fa-minus-circle"></span> Abandonner le(s) acte(s)   ',
          disableNextButton: true,
          callbacks: {
              '*': checkCommentLength,
            },
          completeCallback: abandon,
        });
    }

    /**
     * Afficher/Masquer la sélection de circuit selon si la selection est vide ou non
     */
    function changeSelection() {

        $.each($('div[data-step]'), function () {
            $(this).remove();
        });

        var index = 0;
        $.each($('input[type="checkbox"].masterCheckbox_checkbox:checked'), function () {
            var data = {parameters: {}};
            var identifiant = $(this).prop('name').replace(/\D/g, '');

            var objet = $(this).parent().next('td').next('td').next('td').next('td').text();
            if ($('div[data-step="' + index + '"]').length === 0) {
                data.parameters['Projet_' + identifiant] = {
                    name: 'projet',
                    value: {Deliberation: {id: identifiant}},
                };

                $.post(
                    '/autres_actes/abandonActe',
                    data,
                    function (data) {
                        var progressBarPercent = 100 * (++index / $('input[class="masterCheckbox_checkbox"]:checked').length);
                        var title = 'Renseigner la raison de l\'abandon du projet : ' + identifiant;

                        var modal = '<div class="row hide" data-step="'
                            + index + '" data-title="' + title + '">'
                            + '<div class="col-md-12">'
                            + '<div class="jumbotron text-center">'
                            + '<p>' + objet + '</p>' + data + '</div>'
                            + '  <div class="progress"> \n '
                            + '<div class="progress-bar" role="progressbar" style="width:'
                            + progressBarPercent + '%;" aria-valuenow="'
                            + progressBarPercent + '" aria-valuemin="0" aria-valuemax="100">'
                            + index + '/' + $('input[class="masterCheckbox_checkbox"]:checked').length
                            + '</div>\n'
                            + '</div></div></div>';
                        if ($('div[data-step="' + index + '"]').length === 0) {
                            $(modal).appendTo('#modal-abandon');
                        }
                    });
            }
        });
    }

    // Defaults
    $.fn.appAbandon.defaults = {
        id: 'masterCheckbox', // Element ID
    };

//    $.fn.appAbandon.selectAll = function () {
//        var object = $.fn.appAbandon.settings = $.extend({}, $.fn.appFilter.defaults, options),
//                $self;
//
//        $self = $("#" + object.Name).trigger('click');
//
//        return false;
//    }

    $.appAbandon = $.fn.appAbandon;
});
