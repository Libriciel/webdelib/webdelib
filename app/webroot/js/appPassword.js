/**
 * appPassword
 * @version 7.0
 */

define(["jquery"], function ($) {
    'use strict';

    // Main function
    $.fn.appPassword = function (options) {
        // Ensure that only one scrollUp exists
        if (!$.data(document.body, 'appPassword')) {
            $.data(document.body, 'appPassword', true);
            $.fn.appPassword.init(options);
        }
    };

    // Init
    $.fn.appPassword.init = function (options) {
        $('#password')
            .lsPasswordStrengthMeter($.fn.lsPasswordStrengthMeter.configure('3.4',  parseInt($("#password").attr('data-wd-min-entropie')) ))
            .lsPasswordToggler($.fn.lsPasswordToggler.configure('3.4'));
    };

    // Defaults
    $.fn.appPassword.defaults = {
    };

    $.appPassword = $.fn.appPassword;
});