define(['jquery'], function ($) {
    'use strict';

    // Main function
    $.fn.appFilter = function (options) {
        // Ensure that only one scrollUp exists
        if (!$.data(document.body, 'appFilter')) {
            $.data(document.body, 'appFilter', true);
            $.fn.appFilter.init(options);
        }
    };

    // Init
    $.fn.appFilter.init = function (options) {

        var object = $.fn.appFilter.settings = $.extend({}, $.fn.appFilter.defaults, options),
                $self;

        // To the top
        $('#' + object.Click).click(function (e) {
            e.preventDefault();
            
            $.fn.appFilter.slide(object);
        });
        
        $('#' + object.Click_destroy).click(function (e) {
            e.preventDefault();
            $.fn.appFilter.destroy(object);
        });

    }

    /**
     * slideDown or slideUp the panel of filter
     */
    $.fn.appFilter.slide = function (object) {
        var $self = $('#' + object.Name);
        if ($self.is(":hidden"))
        {
            $self.slideDown();
        } else {
            $self.slideUp();
        }
    }

    /**
     * Modify the Button filter if standard change
     */
    $.fn.appFilter.critereChange = function (element) {
        // Clonage du bouton appliquer filtre et insertion à droite du champ modifié
        var selector = ".input";
        if ($(element).closest('div').hasClass("date")) {
            selector = ".date";
        }
        if ($(element).closest(selector).find(".applyFilter").length === 0) {
            var btn = $('#filtreButton').clone();
            btn.removeAttr('id').addClass('minifiltre');
            $(element).closest(selector).append(btn);
        }
    }

    // Defaults
    $.fn.appFilter.defaults = {
        Name: 'appFilter', // Element ID
        Click: 'appFilter_slideUpAndDown',
        Click_destroy: 'appFilter_destroy',
    };

    /**
     * Destroy filter
     */
    $.fn.appFilter.destroy = function (object) {
        var $self = $('#' + object.Name),
                $form = $self.find("form");
        // reset the values
        $self.slideUp("slow", function () {
            $self.find("select").val('');
            $self.find("input").val('');
            $form.submit();
        });
    }

    $.appFilter = $.fn.appFilter;
});