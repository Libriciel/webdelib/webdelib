/**
 * appCompteur
 * @version 5.0
 */

define(["jquery"], function ($) {
    'use strict';

    // Main function
    $.fn.appCompteur = function (options) {
        // Ensure that only one scrollUp exists
        if (!$.data(document.body, 'appCompteur')) {
            $.data(document.body, 'appCompteur', true);
            $.fn.appCompteur.init(options);
        }
    };

    // Init
    $.fn.appCompteur.init = function (options) {

        var object = $.fn.appCompteur.settings = $.extend({}, $.fn.appCompteur.defaults, options),
                $self;

        $('#CompteurAideformatOptions').change(function(){
            InsertSelectedValueInToInput($(this))
        });
        $('#CompteurAideformatDateOptions').change(function(){
            InsertSelectedValueInToInput($(this))
        });
    }

    /**
     *  Insère la valeur de l'élément sélectionné du champ select 'select_element'
     * dans le champ input 'input_name' du formulaire 'form_name'
     * a l'endroit du curseur ou remplace le texte sélectionné
     * @param {type} e
     * @param select_element Objet champ select
     * @param form_name Nom du formulaire contenant le champ input concerné
     * @param input_name Nom du champ input
     * @returns {undefined}
     *
     */
    function InsertSelectedValueInToInput(e) {
        var input_element = document.getElementById(e.attr('data-compteur-def'));
        input_element.focus();

        /* pour Internet Explorer */
        if (typeof document.selection != 'undefined') {
            /* Insertion du code de formatage */
            var range = document.selection.createRange();
            var insText = range.text;
            range.text = e.val();
            /* Ajustement de la position du curseur */
            range.select();
        }
        /* pour navigateurs basés sur Gecko*/
        else if (typeof input_element.selectionStart != 'undefined')
        {
            /* cas particulier du critère de réinitialisation */
            if (e.attr('data-compteur-def')  === 'CompteurDefReinit') {
                input_element.value = e.val();

            /* Insertion du code de formatage */
            } else {
                var start = input_element.selectionStart;
                var end = input_element.selectionEnd;
                input_element.value = input_element.value.substr(0, start) + e.val() + input_element.value.substr(end);
                /* Ajustement de la position du curseur */
                var pos = start + e.val().length;
                input_element.selectionStart = pos;
                input_element.selectionEnd = pos;
            }
        };

        e.selectedIndex = 0;
    };

    // Defaults
    $.fn.appCompteur.defaults = {
    };

    $.appCompteur = $.fn.appCompteur;
});
