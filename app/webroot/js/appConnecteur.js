/**
 * appConnecteur
 * @version 5.1.0
 */

define(['jquery'], function ($) {
  'use strict';

  // Main function
  $.fn.appConnecteur = function (options) {
      // Ensure that only one scrollUp exists
      if (!$.data(document.body, 'appConnecteur')) {
        $.data(document.body, 'appConnecteur', true);
        $.fn.appConnecteur.init(options);
      }
    };

  // Init
  $.fn.appConnecteur.init = function (options) {

    var object = $.fn.appConnecteur.settings = $.extend({}, $.fn.appConnecteur.defaults, options);
    var $self;

      $("#show_hide_password button").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
          $('#show_hide_password input').attr('type', 'password');
          $('#show_hide_password span').addClass( "fa-eye-slash" );
          $('#show_hide_password span').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
          $('#show_hide_password input').attr('type', 'text');
          $('#show_hide_password span').removeClass( "fa-eye-slash" );
          $('#show_hide_password span').addClass( "fa-eye" );
        }
    });

    $('#btnSaveDataConnexion').on('click', function () {
      $.ajax({
        type: 'POST',
        data: {
          "host": $("#ConnecteurHost") ? $("#ConnecteurHost").val() : null,
          "login": $("#ConnecteurLogin") ? $("#ConnecteurLogin").val() : null,
          "pwd": $("#ConnecteurPwd") ? $("#ConnecteurPwd").val() : null,
        },
        url: '/connecteurs/saveDataConnexionPastell',
        beforeSend: function (xhr) {
          if( !_.isEmpty($("#ConnecteurHost").val())
              && !_.isEmpty($("#ConnecteurLogin").val())
              && !_.isEmpty($("#ConnecteurPwd").val())
          )
          {
            return true
          }
          $.growl({
                title: '<strong>Erreur: </strong>',
                message: 'Informations manquantes pour les informations de connexion',
              },
              { type: 'danger' }
          );
          return false
        },
        success: function (response) {
          if (!_.isEmpty(response.error)) {
              $.growl({
                    title: '<strong>Erreur: </strong>',
                    message: response.error,
                  },
                  { type: 'danger' }
              );
              return;
            }
            $.growl({
                  title: '<strong>Important: </strong>',
                  message: 'Informations de connexion enregistrées',
                },
                {type: 'success'});
            $('#btnRefreshEntities').attr('disabled', false);
        },
        complete: function () {
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          log(XMLHttpRequest);
          log(errorThrown);
          log(textStatus);
        }
      });
    });
    $('#btnRefreshEntities').on('click', function () {
      $.ajax({
        type: 'GET',
        url: '/connecteurs/listEntities',
        dataType: 'json',
        success: function (options) {
          if (!_.isEmpty(options)) {
            var data = _.map(options, function (element, number) {
              return {
                id: number,
                text: element
              }
            });

            $('#CollectiviteIdEntity').select2('destroy');
            $('#CollectiviteIdEntity').select2({
                data: data,
              });

            $.growl({
                title: '<strong>Important: </strong>',
                message: 'Récupération effectuée',
              },
              { type: 'success' }
              );
          }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          $.growl({
              title: '<strong>Erreur: </strong>',
              message: 'Erreur de récupération de la liste des collectivités, veuillez vérfier les informations de connexion',
            },
            { type: 'danger' }
        );

          //console.log(XMLHttpRequest);
          console.log(errorThrown);
          //console.log(textStatus);
        }
      });
    });

    $('#ConnecteurUsePastellTrue').on('change', function () {
        changeActivation($(this));
      });

    $('#ConnecteurUsePastellFalse').on('change', function () {
        changeActivation($(this));
      });

  };

  function changeActivation(element) {
    if ($(element).val() == 'true') {
      $('#config_content').show();
    } else {
      $('#config_content').hide();
    }
  }

  // Defaults
  $.fn.appConnecteur.defaults = {
  };

  $.appConnecteur = $.fn.appConnecteur;
});
