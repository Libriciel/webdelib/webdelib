/**
 * Comportement des checkboxes / master checkbox
 * Select All / Deselect All
 * Initial state
 * @version 4.3
 */

define(["jquery"], function ($) {
    'use strict';

    var btnHtml;

    // Main function
    $.fn.appSendTdt = function (options) {
        // Ensure that only one scrollUp exists
        if (!$.data(document.body, 'appSendTdt')) {
            $.data(document.body, 'appSendTdt', true);
            $.fn.appSendTdt.init(options);
        }
    };

    // Init
    $.fn.appSendTdt.init = function (options) {

        var object = $.fn.appSendTdt.settings = $.extend({}, $.fn.appSendTdt.defaults, options),
                $self;

        $self = $("#" + object.id);

        $('form.appSendTdt select.app-traitementParLot').on("change", function () {
          $('form.appSendTdt div[data-wd-action] div#sendToTdtAlert').remove();
          $('form.appSendTdt div[data-wd-action] br').remove();
          if(!_.isEmpty($('form.appSendTdt select.app-traitementParLot option[value=sendToTdt]:selected').val())){
              blockBtn();
            }
        });
    }

    /**
     * [blockBtn description]
     * @param  {[type]} source [description]
     * @return {[type]}        [description]
     */
    function blockBtn() {
      $('form.appSendTdt button#sendToTdt').prop('disabled', true);
      $('form.appSendTdt button#sendToTdt').attr('data-lock','data-lock');
      btnHtml = $('form.appSendTdt button#sendToTdt').html();
      $('form.appSendTdt button#sendToTdt').html('<i class="fa fa-spinner fa-spin"></i> Vérification...');
      $('form.appSendTdt div[data-wd-action]').append('<br /><br /><div id="sendToTdtAlert"></div>');

      CheckUnBlock();
    }

    function CheckUnBlock() {
      var unBlock = false;
      $('input[type="checkbox"].masterCheckbox_checkbox:checked').each(function () {

        var projet_id = $(this).prop('name').replace(/\D/g, '');
        log('CheckUnBlock[each][projet_id] value: ' + projet_id);
        log('CheckUnBlock[button][isUndefined] value: ' +
        _.isUndefined($(this).closest('tr').find('button[data-target=#modify-tdt-' + projet_id + '].btn-warning').html()));
        if (_.isUndefined($(this).closest('tr').find('button[data-target=#modify-tdt-' + projet_id + '].btn-warning').html())) {
          unBlock = true;
        } else {
          return unBlock = false;
        }

      });
      log('CheckUnBlock value: ' + unBlock);
      if(unBlock){
        $('form.appSendTdt div[data-wd-action] div#sendToTdtAlert').append(
          '<div class="alert alert-warning" role="alert">'
              +'Attention: Pensez à vérifier les informations nécessaires à la télétransmission parmi le ou les actes sélectionnés'
              +'</div>');
        unBlockBtn(false);
      } else {
        if($('input[type="checkbox"].masterCheckbox_checkbox:checked').length > 0)
        {
        $('form.appSendTdt div[data-wd-action] div#sendToTdtAlert').append(
          '<div class="alert alert-danger" role="alert">'
              +'Des informations nécessaires à la télétransmission sont manquantes parmi le ou les actes sélectionnés'
              +'</div>');
        } else {
          $('form.appSendTdt div[data-wd-action] div#sendToTdtAlert').append(
            '<div class="alert alert-danger" role="alert">'
                +'Aucun acte(s) sélectionné(s)'
                +'</div>');
        }
        unBlockBtn(true);
      }
    }

    /**
     * [unBlockBtn description]
     * @param  {[type]} source [description]
     * @return {[type]}        [description]
     */
    function unBlockBtn(disabled) {
      log('unBlockBtn[disabled] value:' + disabled);
      $('form.appSendTdt button#sendToTdt').html(btnHtml);
      if($('input[type=checkbox]:checked').length > 0 && disabled===false){
        log('unBlockBtn disabled');
        $('form.appSendTdt button#sendToTdt').removeAttr('disabled');
        $('form.appSendTdt button#sendToTdt').removeClass('disabled');

        $('form.appSendTdt button#sendToTdt').removeAttr('data-lock');
      }

    }

    function log(log){
      if ($.fn.appSendTdt.settings.debug) {
        console.log(log);
      }
    }

    // Defaults
    $.fn.appSendTdt.defaults = {
        id: 'appSendTdt', // Element ID
        debug: true
    };

    $.appSendTdt = $.fn.appSendTdt;
});
