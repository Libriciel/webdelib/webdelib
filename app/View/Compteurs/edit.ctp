<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Compteurs'), ['action' => 'index']);

if ($this->Html->value('Compteur.id')) {
    $this->Html->addCrumb(__('Modification d\'un compteur'));
    echo $this->Bs->tag('h3', __('Modifier le compteur: ' . $this->Html->value('Compteur.nom')));
    echo $this->BsForm->create(
        'Compteur',
        [
        'url' => [
            'prefix' => $this->request['prefix'],
            'action' => 'edit',
            $this->Html->value('Compteur.id')
        ],
        'type' => 'post']
    );
} else {
    $this->Html->addCrumb(__('Ajouter un compteur'));
    echo $this->Bs->tag('h3', __('Ajouter un compteur')) .
    $this->BsForm->create(
        'Compteur',
        [
        'url' => [
            'prefix' => $this->request['prefix'],
            'controller' => 'compteurs',
            'action' => 'add'
        ],
        'type' => 'post']
    );
}

echo $this->BsForm->input('Compteur.nom', [
    'label' => __('Nom') . ' <acronym title="obligatoire">(*)</acronym>']);

echo $this->BsForm->input('Compteur.commentaire', [
    'label' => __('Commentaire')]);

echo $this->BsForm->input('Compteur.def_compteur', [
    'label' => __('Définition du compteur') . ' <acronym title="obligatoire">(*)</acronym>',
    'maxlength' => '45']);

$this->BsForm->setLeft(3);
$this->BsForm->setRight(9);
echo $this->BsForm->select('aideformatOptions', $aideformatOptions, [
    'label' => false,
    'class' => 'selectone',
    'data-placeholder' => __('Cliquer ici pour ajouter une définition'),
    'empty' => true,
    'inline' => true,
    'data-compteur-def'=> 'CompteurDefCompteur',
    'autocomplete' => 'off',
    'escape' => false]);
$this->BsForm->setDefault();

echo $this->BsForm->input('Compteur.def_reinit', [
    'label' => __('Critère de réinitialisation'),
    'readonly' => $readOnlyDefReInit ? true : false
  ]);

$this->BsForm->setLeft(3);
$this->BsForm->setRight(9);
echo $this->BsForm->select('aideformatDateOptions', $aideformatDateOptions, [
    'label' => false,
    'class' => 'selectone',
    'data-placeholder' => __('Cliquer ici pour choisir un critère de réinitialisation'),
    'empty' => true,
    'data-compteur-def'=> 'CompteurDefReinit',
    'inline' => true,
    'autocomplete' => 'off',
    'escape' => false]);
$this->BsForm->setDefault();

echo $this->Bs->div('required');
echo $this->BsForm->input('Compteur.sequence_id', [
    'type' => 'select',
    'option' => $sequences,
    'label' => __('Séquence') . ' <acronym title="obligatoire">(*)</acronym>',
    'class' => 'selectone',
    'data-placeholder' => __('Sélectionner une séquence'),
    'data-allow-clear' => ($this->request->action = 'admin_add' ? true : false),
    'autocomplete' => 'off',
    'empty' => ($this->request->action = 'admin_add' ? true : false)])
. ($this->Form->isFieldError('Compteur.sequence_id') ?
        $this->Form->error('Compteur.sequence_id') : '');
echo $this->Bs->close();
echo $this->Form->hidden('Compteur.id');

echo $this->Html2->btnSaveCancel('', $previous, __('Enregistrer'), __('Enregistrer'));
echo $this->BsForm->end();
