<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Compteurs'));

echo $this->Bs->tag('h3', __('Liste des compteurs'));
if ($this->permissions->check('Compteurs', 'create')) {
    echo $this->Html2->btnAdd(__("Ajouter un compteur"), __("Ajouter"));
}

echo $this->Bs->table([['title' => __('Libellé')],
    ['title' => __('Commentaire')],
    ['title' => __('Définition')],
    ['title' => __('Critère de réinitialisation')],
    ['title' => __('Séquence')],
    ['title' => __('Actions'), 'class' => 'text-center'],
        ], ['hover', 'striped']);
foreach ($compteurs as $compteur) {
    echo $this->Bs->cell(
            $compteur['Compteur']['nom']
    );
    echo $this->Bs->cell($compteur['Compteur']['commentaire']);
    echo $this->Bs->cell($compteur['Compteur']['def_compteur']);
    echo $this->Bs->cell($compteur['Compteur']['def_reinit']);
    echo $this->Bs->cell($compteur['Sequence']['nom'] . ' : ' . $compteur['Sequence']['num_sequence']);

    $actions = $this->Bs->div('btn-group');
    if ($this->permissions->check('Compteurs', 'read')) {
        $actions.= $this->Bs->btn($this->Bs->icon('eye', null, ['stylePrefix' => 'far']), ['controller' => 'compteurs', 'action' => 'view', $compteur['Compteur']['id']], ['type' => 'default', 'escapeTitle' => false, 'title' => __('Visualiser')]);
    }
    if ($this->permissions->check('Compteurs', 'update')) {
        $actions.= $this->Bs->btn($this->Bs->icon('pencil-alt'), ['controller' => 'compteurs', 'action' => 'edit', $compteur['Compteur']['id']], ['type' => 'primary', 'escapeTitle' => false, 'title' => __('Modifier')]);
    }
    if ($this->permissions->check('Compteurs', 'delete')) {
        $actions.= $this->Bs->btn(
            $this->Bs->icon('trash'),
                ['controller' => 'compteurs', 'action' => 'delete', $compteur['Compteur']['id']],
                ['type' => 'danger',
                    'escapeTitle' => false,
                    'title' => __('Supprimer'),
                    'class' => !empty($compteur['Typeseance']) ? 'disabled' : '',
                    'confirm' => __('Êtes-vous sûr de vouloir supprimer %s ?', $compteur['Compteur']['nom']),
                ]
        );
    }
    $actions.=$this->Bs->close();

    echo $this->Bs->cell($actions, 'text-center');
}
echo $this->Bs->endTable();
