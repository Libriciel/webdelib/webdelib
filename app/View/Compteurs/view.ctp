<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Compteurs'), ['action' => 'index']);
$this->Html->addCrumb($compteur['Compteur']['nom']);

$panel_left = '<b>' . __('Nom') . ' : </b>' . $compteur['Compteur']['nom'] . '<br />' .
        '<b>' . __('Définition du compteur') . ' : </b>' . $compteur['Compteur']['def_compteur'] . '<br />' .
        '<b>' . __('Critère de réinitialisation de la séquence') . ' : </b>' . $compteur['Compteur']['def_reinit'] . '<br />' .
        '<b>' . __('Date de création') . ' : </b>' . $compteur['Compteur']['created'] . '<br />';
$panel_right = '<b>' . __('Commentaire') . ' : </b>' . $compteur['Compteur']['commentaire'] . '<br />' .
        '<b>' . __('Nom et numéro de la séquence') . ' : </b>' . $compteur['Sequence']['nom'] . ' : ' . $compteur['Sequence']['num_sequence'] . '<br />' .
        '<b>' . __('Dernière valeur calculée du critère de réinitialisation') . ' : </b>' . $compteur['Compteur']['val_reinit'] . '<br />' .
        '<b>' . __('Date de modification') . ' : </b>' . $compteur['Compteur']['modified'] . '<br />';

echo $this->Bs->tag('h3', __('Compteur')) .
 $this->Bs->panel(__('Fiche Compteur : ') . $compteur['Compteur']['nom']) .
 $this->Bs->row() .
 $this->Bs->col('xs6') . $panel_left .
 $this->Bs->close() .
 $this->Bs->col('xs6') . $panel_right .
 $this->Bs->close(2) .
 $this->Bs->endPanel() .
 $this->Bs->row() .
 $this->Bs->col('md4 of5') .
 $this->Bs->div('btn-group', null, ['id' => "actions_fiche"]) .
 $this->Html2->btnCancel() .
(
    $this->permissions->check('Compteurs', 'update')
        ? $this->Bs->btn($this->Bs->icon('pencil-alt') . ' ' . __('Modifier'), ['controller' => 'compteurs', 'action' => 'edit', $compteur['Compteur']['id']], [
        'type' => 'primary',
        'escapeTitle' => false,
        'title' => __('Modifier')])
        : ''
) .
 $this->Bs->close(6);
