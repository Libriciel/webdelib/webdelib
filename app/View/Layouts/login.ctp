<?php
$cakeDescription = __d('webdelib', 'webdelib');
?>
<!DOCTYPE html>
<html  lang="fr">
<head>
    <meta charset="utf-8">
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $this->fetch('title'); ?>
    </title>
    <?php
    echo $this->Html->meta(["name" => "base", "href" => $base_url]);
    echo $this->Html->meta(["name" => "viewport", "content" => "width=device-width,  initial-scale=1.0"]);
    echo $this->Html->meta(
        'wd-favicon.ico',
        'img/logo-webdelib/wd-favicon.ico',
        ['type' => 'icon']
    );
    echo $this->Html->script('webpack/dist/main.js');

    echo $this->Html->css('users');

    echo $this->Html->script('ls-elements');

    echo $this->fetch('css');
    echo $this->fetch('meta');
    ?>
</head>

<body>


<!-- Content -->
<?php
//echo $this->Session->flash();
echo $this->fetch('content');
?>

<!-- Footer Libriciel SCOP -->
    <ls-lib-footer
            class="ls-login-footer ls-footer-fixed-bottom"
            active="webdelib"
            application_name="<?php echo $cakeDescription . ' ' . VERSION . " / "; ?>">
    </ls-lib-footer>

<?php
echo $this->fetch('script'); //Affichage de la mise en mémoire tampon des script Js
?>
</body>
</html>
