<?php

/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
$cakeDescription = __d('webdelib', 'webdelib');
?>
<!DOCTYPE html>
<html  lang="fr">
<head>
    <meta charset="utf-8">
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $this->fetch('title'); ?>
    </title>
    <?php
    //META
    echo $this->Html->meta(["name" => "viewport", "content" => "width=device-width,  initial-scale=1.0"]);
    echo $this->Html->meta(
        'wd-favicon.ico',
        'img/logo-webdelib/wd-favicon.ico',
        ['type' => 'icon']
    );
    echo $this->Html->meta(["name" => "base", "href" => $base_url]);
    echo $this->Html->meta(['name' => 'robots', 'content' => 'noindex, nofollow']);

    //CSS
    if (empty($theme) || $theme=='Normal') {
        echo $this->Html->css('/js/components/bootstrap/dist/css/bootstrap.min');
    } else {
        echo $this->Html->css('/js/components/bootswatch/'.$theme.'/bootstrap.min');
    }

    echo $this->Html->css('/js/components/select2/dist/css/select2.min');
    echo $this->Html->css('/js/components/select2-bootstrap-theme/dist/select2-bootstrap.min');
    echo $this->Html->css('/js/components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker');
    echo $this->Html->css('/js/components/yamm3/yamm/yamm.css');

    echo $this->Html->css('/js/components/bootstrap-submenu/dist/css/bootstrap-submenu.min');
    echo $this->Html->css('/js/components/bootstrap-table/dist/bootstrap-table.min');
    echo $this->Html->css('/js/components/jstree/dist/themes/default/style.min');
    echo $this->Html->css('/js/components/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min');

    echo $this->Html->css('filtres');
    echo $this->Html->css('style');

    //Scripts JS
    if (Configure::read('debugJs') === true) {
        echo $this->Html->script('components/requirejs/require', ['data-main' => Router::url('/js/config.js')]);
    } else {
        echo $this->Html->script('require-v'.VERSION.'.min');
    }

    echo $this->fetch('css');
    echo $this->fetch('meta');
    ?>
</head>
<body role="document">
<?php echo $this->element('navbar'); ?>
<?php
App::uses('Debugger', 'Utility');
if (Configure::read('debug') > 0):
    Debugger::checkSecurityKeys();
endif;
?>
<noscript>
    <div class="alert alert-error text-center">
        <strong>Attention!</strong> Vous devez activer JavaScript dans votre navigateur pour pouvoir utiliser le service Webdelib
    </div>
</noscript>
<?php echo $this->fetch('filtre');?>
<!-- Contents -->
<div class="container-fluid" role="main">
<?php
  echo $this->Html->getCrumbList(['class'=>'breadcrumb'], __('Accueil'));
  echo $this->fetch('content');
  echo $this->Flash->render('flash', ['element' => 'Flash/growl']);
  echo $this->Flash->render('auth', ['element'=>'Flash/growl', 'params' => ['type' => 'danger']]);
  $db = ConnectionManager::getDataSource(Configure::read('Config.database'));
  echo $db->showLog();
?>
</div>
<!--Footer-->
<?php
  echo $this->element('footer');
  echo $this->element('waiter');
  echo $this->element('notification');
  echo $this->fetch('script'); //Affichage de la mise en mémoire tampon des script Js
?>
</body>
</html>
