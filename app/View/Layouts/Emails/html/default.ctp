<html lang="fr">
    <head>
        <title><?php echo $title_for_layout; ?></title>
    </head>
    <body>
        <?php echo $content_for_layout; ?>

        <p><?php echo __('Ce mail de notification a été envoyé par webdelib pour votre information. Merci de ne pas répondre.'); ?></p>
    </body>
</html>
