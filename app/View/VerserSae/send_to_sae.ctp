<?php

$nbProjets = isset($this->Paginator) ? $this->Paginator->counter('{:count}') : $nbProjets;
$titre = __('Versement vers le SAE') . ' (' . $nbProjets . ' ' . ($nbProjets > 1 ? 'projets' : 'projet') . ')';

if($this->action=='sendToSaeAutresActes'){
    $this->Html->addCrumb(__('Tous les projets'));
    $this->Html->addCrumb(__('Autres actes'));
} else {
    $this->Html->addCrumb(__('Post-séances'));
}

$this->Html->addCrumb(__('Versement vers le SAE'));

echo $this->element('filtre');

echo $this->Bs->tag('h3', $titre);

echo $this->BsForm->create('Deliberation', [
    'url' => ['controller' => 'verserSae', 'action' => 'sendToSae'],
    'class' => 'waiter form-inline',
    'data-waiter-title'=>__('Versement en cours'),
]);

$titles[] = ['title' => $this->BsForm->checkbox(null, [
    'id' => 'masterCheckbox',
    'label' => '',
    'inline' => true
])];
$titles[] = ['title' => 'Id.'];
$titles[] = ['title' =>  $this->Paginator->sort('num_delib', __('Numéro'))];
$titles[] = ['title' => $this->Paginator->sort('objet_delib', __('Libellé'))];
$titles[] = ['title' => __('Date de versement')];
$titles[] = ['title' => __('Versé')];
$titles[] = ['title' => __('Actions')];

echo $this->Bs->table($titles, ['hover', 'striped']);

foreach ($deliberations as $delib) {
    $options = [];
    if (!$delib['Deliberation']['sae_etat']) {
        $options['checked'] = true;
    } else {
        $options['disabled'] = true;
    }
    echo $this->Bs->cell($this->Form->checkbox('Deliberation.' . $delib['Deliberation']['id'] . '.is_checked', $options));


    echo $this->Bs->cell($this->Html->link($delib['Deliberation']['id'], [
        'controller' => 'projets',
        'action' => 'view', $delib['Deliberation']['id']]
    ));
    echo $this->Bs->cell($delib['Deliberation']['num_delib']);
    echo $this->Bs->cell($delib['Deliberation']['objet_delib']);
    echo $this->Bs->cell($delib['Deliberation']['sae_date'] ? CakeTime::i18nFormat(strtotime($delib['Deliberation']['sae_date']), '%d/%m/%Y') : '-');
    echo $this->Bs->cell($this->Sae->btnEtat($delib['Deliberation']['sae_etat'], $delib['Deliberation']['sae_commentaire']));

    echo $this->Bs->cell(
        $this->Bs->btn($this->Bs->icon('download'), [
            'controller' => 'actes',
            'action' => 'download', $delib['Deliberation']['id']], ['type' => 'default',
            'escapeTitle' => false,
            'title' => __('Télécharger le document:') . ' ' . $delib['Deliberation']['objet_delib']
        ])
    );
}
echo $this->Bs->endTable();

$this->BsForm->setLeft(0);
$this->BsForm->setRight(12);

$actions_possibles = [];
if(Configure::read('USE_SAE')){
    $actions_possibles['verser'] = __('Verser');
}
$actions_possibles['verser_manuellement'] = __('Déclarer un versement manuel');

echo $this->element('Projet/verser', [
        'traitement_lot' => true ,
        'actions_possibles'=> $actions_possibles
    ]).

    $this->Bs->close();

echo $this->BsForm->end();

echo $this->Html->tag('br');

//paginate
echo $this->element('paginator', ['paginator' => $this->Paginator]);