<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Insérer dans un circuit'));

$title = '<span class="label label-' . ($projet['Deliberation']['etat'] == -1 ? 'danger' : 'default') . '" ' . (!empty($projet['listeSeances'][0]['color']) ? 'style="background-color: ' . $projet['listeSeances'][0]['color'] . '"' : '') . '>' . $projet['Deliberation']['id'] . '</span>';

if (!empty($projet['Multidelib'])) {
    $listeIds = $projet['Deliberation']['id'];
    foreach ($projet['Multidelib'] as $delibRattachee) {
        $listeIds .= ', ' . $delibRattachee['id'];
    }
    $title .= ' ' . __('Projet multi-délibérations n°') . $projet['Deliberation']['id'];
}

if (!empty($projet['User'])) {
    $title.=' ' . $this->Bs->icon(
        'users',
        '',
        [
                'data-toggle' => 'tooltip',
                'data-placement' => 'right',
                'escape' => false,
                'title' => __('Multi-rédacteurs')
                    ]
    );
}


echo $this->Bs->div('panel panel-' . ($projet['Deliberation']['etat'] == -1 ? 'danger' : 'default'));
echo $this->Bs->div(
    'panel-heading',
    $this->Bs->row() .
    $this->Bs->col('xs8') .
        $this->Bs->div('media') .
        $this->Bs->div(null, $this->Bs->btn($this->Bs->icon(
            $this->ProjetUtil->etat_icon($projet['Deliberation']['iconeEtat']['image']),
            [   'lg',
                'iconType' =>  $this->ProjetUtil->etat_iconType($projet['Deliberation']['iconeEtat']['image'])]
        ), null, [
                    'tag' => 'button',
                    'type' => empty($projet['iconeEtat']['status']) ? $this->ProjetUtil->etat_icon_type($projet['Deliberation']['iconeEtat']['image']) : $projet['iconeEtat']['status'],
                    'size' => 'lg',
                    //'disabled'=>'disabled',
                    'data-toggle' => 'popover',
                    'data-content' => $this->ProjetUtil->etat_icon_help($projet['Deliberation']['iconeEtat']['image']),
                    'data-placement' => 'right',
                    'escapeTitle' => false,
                    'title' => $projet['Deliberation']['iconeEtat']['titre']
                ]), ['class' => 'media-left']) .
        $this->Bs->div('media-body') .
        $this->Bs->tag('h4', $projet['Deliberation']['objet'], ['class' => 'media-heading']) .
        $this->Bs->div(null, $title) .
        $this->Bs->close(2) .
        $this->Bs->close() .
        $this->Bs->col('xs4') .
        $this->Bs->div('text-right', $this->Bs->btn($projet['Typeacte']['name'], null, [
                    'tag' => 'button',
                    'type' => 'default',
        ])) .
        $this->Bs->close(2)
);
echo $this->Bs->div('panel-body');


echo $this->Bs->confirm($this->Bs->icon('comment-dots') . ' ' . __('Commenter'), ['controller' => 'commentaires', 'action' => 'add', $projet['Deliberation']['id']], ['type' => 'info',
    'escapeTitle' => false,
    'title' => __('Commenter le projet'),
    'texte' => $this->BsForm->create('Commentaire', [
            'url' => ['controller' => 'commentaires', 'action' => 'add', $projet['Deliberation']['id']]])
        . $this->Form->hidden('Commentaire.delib_id', ['value' => $projet['Deliberation']['id']])
        . $this->BsForm->input('Commentaire.texte', [
            'type' => 'textarea',
            'label' => __('Commentaire'),
            'class' => 'app-text-counter',
            'cols' => '3',
        ])
        . $this->BsForm->end()
,
    'header' => __('Nouveau commentaire (projet %s)', $projet['Deliberation']['id']),
    'style' => [
        'border-bottom-right-radius' => '4px',
        'border-top-right-radius' => '4px',
    ]
], ['form' => true]);
echo $this->Bs->close();

echo $this->element('projetInfo', ['projet' => $projet]);

echo $this->Html->tag('br /');

echo $this->BsForm->create('Deliberation', ['type' => 'post', 'url' => ['plugin' => null, 'controller' => 'validations', 'action' => 'attribuerCircuit', $projet['Deliberation']['id']]]);

// choisir un circuit
if (!empty($circuits)) {
    echo $this->BsForm->select('Deliberation.circuit_id', $circuits, [
        'label' => __('Choisir un circuit'),
        'autocomplete' => 'off',
        'default' => isset($userCircuitDefaultId) ? $userCircuitDefaultId : false,
        'data-placeholder' => __('Sélectionnez un circuit'),
        'empty' => ''
    ]);
} else {
    echo $this->Bs->div('well  text-center', __('Aucun circuit disponible'));
}

// données concernant le circuit selectionné
echo $this->Bs->row() .
    $this->Bs->col('xs3')
 . $this->Bs->close() .
 $this->Bs->col('xs9') . $this->Bs->div(null, (isset($attribuer_circuit_visu) ? $attribuer_circuit_visu : ''), ['id' => 'selectCircuit'])
 . $this->Bs->close(3);

echo $this->Bs->div('btn-group', null) .
    $this->Bs->btn($this->Bs->icon('times-circle') . ' ' . __('Annuler'), $previous, [
        'type' => 'default',
        'escapeTitle' => false,
        'title' => __('Annuler les modifications')]) .
     $this->Bs->btn($this->Bs->icon('pencil-alt') . ' ' . __('Modifier le projet'), [
       'controller' => 'projets', 'action' => 'edit', $projet['Deliberation']['id']], [
         'type' => 'primary',
         'option_type'=> 'button',
         'escape' => false,
         'title' => $this->Lock->lockTitle($projet['Deliberation'], __('Modifier le projet')),
         'tag'=> $this->Lock->lockButton($projet['Deliberation']),
         'style' => $this->Lock->lockStyle($projet['Deliberation']),
         'disabled'=> $this->Lock->lockDisabled($projet['Deliberation']),
         'confirm' => $this->Lock->lockConfirm($projet['Deliberation'])
     ]);

if (!empty($circuits)) {
    echo $this->Bs->btn($this->Bs->icon('road') . ' ' . __('Insérer le projet dans le circuit'), null, [
        'tag' => 'button',
        'type' => 'success',
        'id' => 'attribuer',
        'name' => 'attribuer',
        'escapeTitle' => false,
        'title' => __('Insérer le projet dans le circuit')]);
}

echo $this->Bs->close() . $this->Bs->div('br');



echo $this->BsForm->end();

echo $this->Html->scriptBlock('
    require(["domReady"], function (domReady) {
    domReady(function () {
    $("#DeliberationAttribuercircuitForm").addClass("waiter");

       if ($(".parapheur_error").length > 0){
           $("#attribuer").remove();
       }

    $("#DeliberationCircuitId").select2({
        allowClear: true
    }).on("select2-removed",
    function(e) {
        $("#selectCircuit").html(\'\');
    }).on("change",

    function(e) {

        if($("#DeliberationCircuitId").val() != \'\'){
            var ajaxUrl = \'' . $this->Html->url([
            'plugin' => 'cakeflow',
            'controller' => 'circuits',
            'action' => 'visuCircuit']) . '/\' + $("#DeliberationCircuitId").val();
            $.ajax({
                url: ajaxUrl,
                beforeSend: function () {
                    $("#selectCircuit").html(\'\');
                },
                success: function (result) {
                    $("#selectCircuit").html(result);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });
        }
    });
            });
});
');
