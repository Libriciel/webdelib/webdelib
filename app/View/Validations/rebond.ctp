<?php
echo $this->Html->tag('h3', __('Envoyer le projet à un ou plusieurs utilisateurs'));
$this->Html->addCrumb(__('Validations'), ['action' => 'traiter', $delib_id]);
$this->Html->addCrumb(__('Envoyer le projet à un utilisateur'));
$options = [
    'detour' => __('Envoyer (sans retour)') . ' ' . $this->Bs->icon('share'),
    'retour' => __('Aller-retour') . ' ' . $this->Bs->icon('retweet')];

if (configure::read('Cakeflow.validation_finale')) {
    $options['validation'] = __('Validation finale') . ' ' . $this->Bs->icon('legal');
}

$attributes = [
    'autocomplete' => 'off',
    'legend' => false];
echo $this->BsForm->create('Insert', ['url' => [
        'controller' => 'validations',
        'action' => 'rebond', $delib_id
    ], 'type' => 'post']
);
$affiche = $this->BsForm->radio('etape_choisie', [
    3 => __('Collaboratif [ET]'),
    2 => __('Concurrent [OU]'),
    1 => __('Simple')], $attributes);
$affiche .= $this->BsForm->select('users_id', $users, [
    'multiple' => true,
    'autocomplete' => 'off',
    'class' => 'selectmultiple',
    'disabled'=> 'disabled',
    'data-placeholder' => __('Sélectionner des utilisateurs')]);

$affiche = $this->Html2->div('panel-heading', __('Sélection du ou des destinataires'))
    . $this->Html2->div('panel-body', $affiche);
echo $this->Html2->div('panel panel-default', $affiche);

/* echo  $this->Html->tag('div', 'Titre', array('class' => 'panel-heading'));
  echo $this->Html->tag('div', $affiche, array('class' => 'panel-body')); */
$attributes = ['legend' => false, 'value' => 'retour'];
$radio = '';

if ($collaboratif === true) {
    $radio .= $this->BsForm->hidden('option', ['value' => 'retour']);
    $radio .= $this->BsForm->radio('option_disabled', $options, array_merge($attributes, ['disabled' => true]));
    $radio .= '<div class="spacer"></div>';
    $radio .= $this->Html->para('profil', __('Note : pour les étapes collaboratives (ET), l\'aller-retour est la seule possibilité.'), ['style' => 'float: left;text-align: left;']);
} else {
    $radio .= $this->BsForm->radio('option', $options, $attributes);
}
$radio = $this->Html2->div('panel-heading', __('Sélection du type d\'envoi'))
    . $this->Html2->div('panel-body', $radio);
echo $this->Html2->div('panel panel-default', $radio);

$commentaire= $this->Html2->div('panel-heading', __('Ajouter un commentaire')) .
    $this->Html2->div('panel-body', $this->BsForm->input('Commentaire', [
        'type' => 'textarea',
        'label' => __('Commentaire'),
        'class' => 'app-text-counter',
        'rows' => '2'
    ]));
echo $this->Html2->div('panel panel-default', $commentaire);

echo $this->Html2->btnSaveCancel('', ['action' => 'traiter', $delib_id]);

echo $this->BsForm->end();
?>
<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {

        $("#InsertEtapeChoisie3").on('change', function () {
            if($(this).is(':checked')){
                $('#InsertUsersId').select2.defaults.set("maximumSelectionLength", null);
                $('#InsertUsersId').select2();
                $("#InsertUsersId").val(null).trigger("change");
                $("#InsertUsersId").prop("disabled", false)
            }
        });

        $("#InsertEtapeChoisie2").on('change', function () {
            if($(this).is(':checked')){
                $('#InsertUsersId').select2.defaults.set("maximumSelectionLength", null);
                $('#InsertUsersId').select2();
                $("#InsertUsersId").val(null).trigger("change");
                $("#InsertUsersId").prop("disabled", false);
            }
        });

        $("#InsertEtapeChoisie1").on('change', function () {
            if($(this).is(':checked')){
                $('#InsertUsersId').select2.defaults.set('maximumSelectionLength', 1);
                $('#InsertUsersId').select2();
                $("#InsertUsersId").val(null).trigger("change");
                $("#InsertUsersId").prop("disabled", false)
            }
        });

//        $('#InsertUsersId').select2({
//            width: "100%"
//        });
    });
});
</script>
