<?php

$this->Html->addCrumb(__('Valider en urgence'));
echo $this->Bs->tag('h3', __('Valider en urgence'));

echo $this->BsForm->create('Traitement',
    [
        'url' => [
            'controller' => 'Validations', 'action' => 'validerEnUrgence', $delib_id
        ]
    ]
);

echo $this->BsForm->input('Commentaire', [
    'type' => 'textarea',
    'label' => __('Commentaire'),
    'rows' => '2',
    'maxlength' => '1000',
    'help'=> __('Votre commentaire ne peut dépasser 1500 caractères.'),
    'required' => true]
);

echo $this->Html2->btnSaveCancel('', $previous);
echo $this->BsForm->end();
// FIX scriptBlock
echo $this->Bs->scriptBlock("$('#TraitementGoNextForm').addClass('waiter')");
