<?php
echo $this->Html->tag('h3', __('Retourner le projet à une étape précédente'));
$this->Html->addCrumb(__('Validations'), ['action' => 'traiter', $delib_id]);
$this->Html->addCrumb(__('Retourner à'));

echo $this->BsForm->create(
  'Traitement',
    [
  'url' => [
    'controller' => 'Validations',
    'action' => 'retour', $delib_id]
  ]
);
echo $this->BsForm->input(
    'etape',
    [
      'label' => __('Étape du circuit'),
      'help' => __("A quelle étape voulez-vous renvoyer le projet ?")
    ]
);
echo $this->BsForm->input('Commentaire', [
     'type' => 'textarea',
     'label' => __('Commentaire'),
     'class' => 'app-text-counter',
     'rows' => '2'
 ]);

 echo $this->Html2->btnSaveCancel('', ['action' => 'traiter', $delib_id]);
 echo $this->BsForm->end();
?>
<script>
require(['domReady'], function (domReady) {
    domReady(function () {
        $("#TraitementEtape").select2({
            width: 'auto'
        });
    });
});
</script>
<style>
    label {
        float:none;
        text-align: left;
    }
</style>
