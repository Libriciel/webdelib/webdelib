<?php
$this->Html->addCrumb(__('Validation d\'un projet'));
echo $this->Bs->tag('h3', __('Validation d\'un projet'));

// Initialisation des boutons action de la vue
$linkBarre = $this->Bs->div('btn-toolbar', null, ['role' => 'toolbar']);
$linkBarre .= $this->Bs->div('btn-group'); //
$linkBarre .= $this->Html2->btnCancel($previous);
$linkBarre .= $this->Bs->btn($this->Bs->icon('cog') . ' ' . __('Générer'), [
        'controller' => 'projets', 'action' => 'genereFusionToClient',
    $projet['Deliberation']['id']], ['type' => 'default',
           'escapeTitle' => false,
           'data-waiter-send' => 'XHR',
           'data-waiter-callback' => 'getProgress',
           'data-waiter-type' => 'progress-bar',
           'data-waiter-title' => __('Générer le document du projet : %s', $projet['Deliberation']['id']),
           'class' => 'waiter',
           'title' => __('Générer le document du projet : %s', $projet['Deliberation']['id'])
       ]);
if ($this->permissions->check('Projets', 'update')) {
    $linkBarre .= $this->Bs->btn(
        $this->Bs->icon('pencil-alt') . ' ' . __('Modifier le projet'),
        [
              'controller' => 'projets',
              'action' => 'edit', $projet['Deliberation']['id']],
        [
              'type' => 'primary',
              'option_type'=> 'button',
              'escape' => false,
              'class' => 'btn-action-m',
              'title' => $this->Lock->lockTitle($projet['Deliberation'], __('Modifier le projet')),
              'style' => $this->Lock->lockStyle($projet['Deliberation']),
              'tag'=> $this->Lock->lockButton($projet['Deliberation']),
              'disabled'=> $this->Lock->lockDisabled($projet['Deliberation']),
              'confirm' => $this->Lock->lockConfirm($projet['Deliberation'])
            ]
    );
}
$linkBarre .= $this->Bs->close();
$linkBarre .= $this->Bs->div('btn-group');
$linkBarre .= $this->Bs->confirm($this->Bs->icon('comment-dots') . ' ' . __('Commenter'), [
        'controller' => 'commentaires',
    'action' => 'add', $projet['Deliberation']['id']], [
                'type' => 'info',
    'escapeTitle' => false,
    'title' => __('Commenter le projet'),
    'texte' => $this->BsForm->create('Commentaire', [
            'url' => ['controller' => 'commentaires', 'action' => 'add', $projet['Deliberation']['id']]])
        . $this->Form->hidden('Commentaire.delib_id', ['value' => $projet['Deliberation']['id']])
        . $this->Form->hidden('forceReturnAction', ['value' => 'traiter'])
    . $this->BsForm->input('Commentaire.texte', [
        'type' => 'textarea',
        'label' => __('Commentaire'),
        'class' => 'app-text-counter',
        'required' => true,
        'cols' => '3',
        ])
    . $this->BsForm->end()
    ,
    'header' => __('Nouveau commentaire (projet %s)', $projet['Deliberation']['id']),
    'style' => [
        'border-bottom-right-radius' => '4px',
        'border-top-right-radius' => '4px',
    ]
        ], ['form' => true]);
$linkBarre .= $this->Bs->close();

$linkBarre .= $this->Bs->div('btn-group');
if ($this->permissions->check('Validations/retour', 'read')) {
    $linkBarre .= $this->Bs->btn(
        $this->Bs->icon('fast-backward') . ' ' . __('Retourner à'),
        ['controller' => 'Validations', 'action' => 'retour', $projet['Deliberation']['id']],
        [
        'type' => 'primary',
        'escapeTitle' => false,
        'tag'=> $this->Lock->lockButton($projet['Deliberation']),
        'style' => $this->Lock->lockStyle($projet['Deliberation']),
        'disabled'=> $this->Lock->lockDisabled($projet['Deliberation']),
        'title' => $this->Lock->lockTitle($projet['Deliberation'], __('Retourner le projet à')),
    ]
    );
}
if ($this->permissions->check('Validations/rebond', 'read')) {
    $linkBarre .= $this->Bs->btn(
        $this->Bs->icon('fast-forward') . ' ' . __('Envoyer à'),
        ['controller' => 'Validations', 'action' => 'rebond', $projet['Deliberation']['id']],
        [
            'type' => 'primary',
            'escapeTitle' => false,
            'tag'=> $this->Lock->lockButton($projet['Deliberation']),
            'style' => $this->Lock->lockStyle($projet['Deliberation']),
            'disabled'=> $this->Lock->lockDisabled($projet['Deliberation']),
            'title' => $this->Lock->lockTitle($projet['Deliberation'], __('Envoyer le projet à')),
        ]
    );
}
$linkBarre .= $this->Bs->close();

$this->BsForm->setLeft(0);
$this->BsForm->setRight(12);

$linkBarre .= $this->Bs->div('btn-group');
$linkBarre .= $projet['Deliberation']['isLocked'] ? $this->Bs->btn(
    $this->Bs->icon('check') . ' ' . __('Refuser'),
    ['controller' => 'Validations', 'action' => 'traiter', $projet['Deliberation']['id'], true],
    [
        'type' => 'danger',
        'escapeTitle' => false,
        'tag'=> $this->Lock->lockButton($projet['Deliberation']),
        'style' => $this->Lock->lockStyle($projet['Deliberation']),
        'disabled'=> $this->Lock->lockDisabled($projet['Deliberation']),
        'title' => $this->Lock->lockTitle($projet['Deliberation'], __('Refuser')),
    ]
) : $this->Bs->confirm(
    $this->Bs->icon('times') . ' ' . __('Refuser'),
    [
          'controller' => 'Validations',
          'action' => 'traiter',
          $projet['Deliberation']['id'], '0'
        ],
    [
          'type' => 'danger',
          'escapeTitle' => false,
          'tag'=> $this->Lock->lockButton($projet['Deliberation']),
          'title' => $this->Lock->lockTitle($projet['Deliberation'], __('Refuser le projet')),
          'disabled'=> $this->Lock->lockDisabled($projet['Deliberation']),
          'texte' => $this->BsForm->create(false, [
                  'url' => ['controller' => 'Validations', 'action' => 'traiter', $projet['Deliberation']['id'], false],
                  'novalidate' => true])
              . $this->Html->tag('p', __('Souhaitez-vous ajouter un commentaire ?'))
              . $this->BsForm->input('Commentaire.texte', [
                  'type' => 'textarea',
                  'class' => 'app-text-counter',
                  'label' => false,
                  'cols' => '3'
                  ])
              . $this->BsForm->end()
        ,
        'header' => __('Vous allez refuser ce projet !'),
        'style' => [
            'border-bottom-right-radius' => '4px',
            'border-top-right-radius' => '4px',
        ]
            ],
    ['form' => true]
);
$linkBarre .= $this->Bs->close(1);

$linkBarre .= $this->Bs->div('btn-group');
$linkBarre .= $projet['Deliberation']['isLocked'] ? $this->Bs->btn(
    $this->Bs->icon('check') . ' ' . __('Valider'),
    ['controller' => 'Validations', 'action' => 'traiter', $projet['Deliberation']['id'], true],
    [
        'type' => 'success',
        'escapeTitle' => false,
        'tag'=> $this->Lock->lockButton($projet['Deliberation']),
        'style' => $this->Lock->lockStyle($projet['Deliberation']),
        'disabled'=> $this->Lock->lockDisabled($projet['Deliberation']),
        'title' => $this->Lock->lockTitle($projet['Deliberation'], __('Valider')),
    ]
) :
        ($this->Bs->confirm($this->Bs->icon('check') . ' ' . __('Valider'), [
                'controller' => 'Validations', 'action' => 'traiter', $projet['Deliberation']['id'], '1'
        ], [
                'type' => 'success',
        'escapeTitle' => false,
        'title' => __('Accepter le projet'),
        'texte' => $this->BsForm->create(false, [
            'url' => ['controller' => 'Validations', 'action' => 'traiter', $projet['Deliberation']['id'], true],
            'novalidate' => true])
        . '<p>' . __('Souhaitez-vous ajouter un commentaire ?') . '</p>'
        . $this->BsForm->input('Commentaire.texte', [
            'type' => 'textarea',
            'class' => 'app-text-counter',
            'label' => false, 'cols' => '3'
            ])
        . $this->BsForm->end()
        ,
        'header' => __('Vous allez valider ce projet !'),
        'style' => [
            'border-bottom-right-radius' => '4px',
            'border-top-right-radius' => '4px',
            ]
            ], ['form' => true]));

$linkBarre .= $this->Bs->close(2);
$this->BsForm->setDefault();
// affichage  du titre
$title= '<span class="label label-'.($projet['Deliberation']['etat'] == -1?'danger':'default').'" ' . (!empty($projet['listeSeances'][0]['color']) ? 'style="background-color: ' . $projet['listeSeances'][0]['color'] . '"' : '') . '>' . $projet['Deliberation']['id'] . '</span>';

if (!empty($projet['Multidelib'])) {
    $listeIds = $projet['Deliberation']['id'];
    foreach ($projet['Multidelib'] as $delibRattachee) {
        $listeIds .= ', ' . $delibRattachee['id'];
    }
    $title .= ' '.__('Projet multi-délibérations n°') . $projet['Deliberation']['id'];
}

if (!empty($projet['User'])) {
    $title.=' '.$this->Bs->icon(
        'users',
        '',
        [
            'data-toggle' => 'tooltip',
            'data-placement' => 'right',
            'escape' => false,
            'title' => __('Multi-rédacteurs')
        ]
    );
}

echo $this->Bs->div('panel panel-'.($projet['Deliberation']['etat']==-1?'danger':'default'));
echo $this->Bs->div(
    'panel-heading',
    $this->Bs->row() .
    $this->Bs->col('xs8') .

    $this->Bs->div('media') .
        $this->Bs->div(null, $this->Bs->btn($this->Bs->icon($this->ProjetUtil->etat_icon($projet['Deliberation']['iconeEtat']['image']), ['lg']), null, [
        'tag' => 'button',
        'type' => empty($projet['iconeEtat']['status']) ? $this->ProjetUtil->etat_icon_type($projet['Deliberation']['iconeEtat']['image']) : $projet['iconeEtat']['status'],
        'size' => 'lg',
        //'disabled'=>'disabled',
        'data-toggle' => 'popover',
        'data-content' => $this->ProjetUtil->etat_icon_help($projet['Deliberation']['iconeEtat']['image']),
        'data-placement' => 'right',
        'escapeTitle' => false,
        'title' => $projet['Deliberation']['iconeEtat']['titre']
            ]), ['class' => 'media-left']).
    $this->Bs->div('media-body') .
    $this->Bs->tag('h4', $projet['Deliberation']['objet'], ['class' => 'media-heading']) .
    $this->Bs->div(null, $title).
    $this->Bs->close(2)   .


//        $this->Bs->tag('p',
//                .' '. $this->Bs->tag('h3', $projet['Deliberation']['objet'], array('class' => 'text-right')), array('class' => 'text-left')) .
//
//        $title . ($projet['Deliberation']['etat']==-1?' '.__('(Versionné)'):'').
//
        $this->Bs->close() .
        $this->Bs->col('xs4') .
        $this->Bs->div('text-right', $this->Bs->btn($projet['Typeacte']['name'], null, [
                    'tag' => 'button',
                    'type' => 'default',
                        ])).
        $this->Bs->close(2)
);
echo $this->Bs->div('panel-body');
//echo $this->Bs->tabPane($title);
echo $linkBarre;
echo $this->Bs->tag('br /');

$aTab = [
    'infos' => __('Informations principales'),
    'circuits' => __('Circuit(s)')];

if (!empty($commentaires)) {
    $aTab['commentaires'] = __('Commentaire(s)');
}
if (!empty($infosupdefs)) {
    $aTab['Infos_suppl'] = __('Information(s) supplémentaire(s)');
}
if (!empty($historiques)) {
    $aTab['historiques'] = __('Historique(s)');
}
if (!empty($projet['Multidelib'])) {
    $aTab['multidelib'] = __('Délibérations rattachée(s)');
}


echo $this->Bs->tab($aTab, ['active' => 'infos', 'class' => '-justified']) .
    $this->Bs->tabContent();

echo $this->Bs->tabPane('infos', ['class' => 'active']);

echo $this->Bs->tag('br');

echo $this->element('projetInfo', ['projet' => $projet, 'tab_anterieure' => !empty($tab_anterieure)?$tab_anterieure:null]);
echo $this->Bs->tag('br');

// multi-délibs
if (empty($projet['Multidelib']) && !empty($projet['Annexe'])) {
    echo $this->Bs->tag('br');
    $this->BsForm->setLeft(2);
    $this->BsForm->setRight(10);
    echo $this->Bs->row();
    echo $this->Bs->col('xs' . $this->BsForm->getLeft(), null, ['class' => 'text-right']) . $this->Html->tag('label', __('Annexe(s) :')) . $this->Bs->close().
    $this->Bs->col('xs'.$this->BsForm->getRight());
    echo $this->element('annexe_view', array_merge(['ref' => 'delibPrincipale'], ['annexes' => $projet['Annexe']]));
    echo $this->Bs->close(2);
}
echo $this->Bs->tabClose();

echo $this->Bs->tabPane('circuits');
echo $this->Bs->tag('h4', 'Circuit(s)');
echo $this->Bs->row() .
    $this->Bs->col('xs4') . '<b>' . __('Circuit') . ' :</b> ' . $projet['Circuit']['libelle']
 . $this->Bs->close() .
 $this->Bs->col('xs6') . $visu
 . $this->Bs->close(2);
echo $this->Bs->tabClose();

if (!empty($commentaires)) {
    echo $this->Bs->tabPane('commentaires');
    echo $this->Bs->tag('h4', __('Commentaire(s)'));

    $sLis = '';
    foreach ($commentaires as $commentaire) {
        $btn_action='';
        if ($commentaire['Commentaire']['agent_id'] == AuthComponent::user('id')) {
            $btn_action = $this->Bs->btn(
                $this->Bs->icon('trash') . ' ' .'Supprimer',
                [
                        'controller' => 'commentaires',
                    'action' => 'delete', $commentaire['Commentaire']['id'] , $projet['Deliberation']['id']],
                ['type'=> 'danger',
                        'escapeTitle'=> false,
                        'size'=>'xs'
                            ]
            );
        } else {
            $btn_action = $this->Bs->btn(
                $this->Bs->icon('check') . ' ' .'Prendre en compte',
                [
                        'controller' => 'commentaires',
                    'action' => 'prendreEnCompte', $commentaire['Commentaire']['id'] , $projet['Deliberation']['id']],
                ['type'=> 'success',
                        'escapeTitle'=> false,
                        'size'=>'xs'
                        ]
            );
        }
        $sLis.= $this->Html->tag(
            'li',
            $this->Bs->tag('h5', '<span class="label label-info">' . $commentaire['User']['prenom'] . ' ' . $commentaire['User']['nom'] . '</span> '
                . $this->Time->i18nFormat($commentaire['Commentaire']['created'], '%d/%m/%Y à %k:%M') . ' ' . $btn_action, ['class' => 'list-group-item-heading'])
            . $this->Bs->tag(
                'p',
                nl2br($commentaire['Commentaire']['texte']),
                ['class' => 'list-group-item-text']
            ),
            ['escape' => false, 'class' => 'list-group-item']
        );
    }
    echo $this->Bs->row()
        . $this->Bs->col('xs12')
    . $this->Bs->tag('ul', $sLis, ['class' => 'list-group'])
    . $this->Bs->close(2);

    echo $this->Bs->tabClose();
}


if (!empty($infosupdefs)) {
    echo $this->Bs->tabPane('Infos_suppl');
    echo $this->Bs->tag('h4', 'Informations Supplémentaire(s)');
    echo $this->element('projetInfoSupp', ['infosupdefs' => $infosupdefs]);
    echo $this->Bs->tabClose();
}

if (!empty($historiques)) {
    echo $this->Bs->tabPane('historiques');
    echo $this->Bs->tag('h4', __('Historique(s)'));
    echo $this->Bs->table([
        ['title' => __('Date')],
        ['title' => __('Utilisateur')],
        ['title' => __('Annotation / observation')],
        ['title' => __('Consultation'),'class'=>'text-right'],
    ], ['striped','hover']);

    foreach ($historiques as $historique) {
        echo $this->Bs->cell($this->Time->i18nFormat($historique['Historique']['created'], '%d/%m/%Y %k:%M:%S'));
        echo $this->Bs->cell($historique['User']['prenom'] . ' ' . $historique['User']['nom']);
        echo $this->Bs->cell(nl2br($historique['Historique']['commentaire']));
        echo $this->Bs->cell($this->Bs->btn(
            $this->Bs->icon('eye', null, ['stylePrefix' => 'far']),
            ['controller'=>'versions',
                'action'=>'index',
                $historique['Historique']['revision_id']],
            [
                'tag' => empty($historique['Historique']['revision_id']) ? 'button' : null,
                'class'=>'pull-right',
                'type' => 'default',
                'disabled' => empty($historique['Historique']['revision_id']),
                'escapeTitle' => false,
                'title' => empty($historique['Historique']['revision_id'])?  __('Pas de révision disponible') : __('Consulter la révision')]
        ));
    }
    echo $this->Bs->endTable();
    echo $this->Bs->tabClose();
}

// Multidelib
if (!empty($projet['Multidelib'])) {
    echo $this->Bs->tabPane('multidelib', ['class' => (isset($nameTab) && $nameTab == 'multidelib') ? 'active' : '']);
    echo $this->element('Deliberations/Multidelib/view', [
        'delib' => $projet['Deliberation'],
        'annexes' => $projet['Annexe'],
        'natureLibelle' => $projet['Typeacte']['name']]);
    foreach ($projet['Multidelib'] as $delibRattachee) {
        echo $this->element('Deliberations/Multidelib/view', [
            'delib' => $delibRattachee,
            'annexes' => $delibRattachee['Annexe'],
            'natureLibelle' => $projet['Typeacte']['name']]);
    }
    echo $this->Bs->tabClose();
}

echo $this->Bs->tabPaneClose();
echo $this->Bs->close(2);
?>
<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {

        <?php if (!empty($majDeleg)): ?>

            function afficheMAJ() {
                $("div.nomcourante").parent().append('<?php
    echo $this->Html->tag('div', $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-repeat']) . __(' Mise à jour'), [
            'controller' => 'Signatures',
        'action' => 'MajEtatParapheur',
        $projet['Deliberation']['id']
    ], ['escape' => false, 'class' => 'btn btn-inverse']), ['class' => 'majDeleg', 'title' => __('Mettre à jour le statut des étapes de délégations')]);
    ?>')
            }

            afficheMAJ();
<?php endif; ?>
<?php
if (isset($visas_retard) && !empty($visas_retard)):
    foreach ($visas_retard as $visa):
        ?>
                $('#etape_<?php echo $visa['Visa']['numero_traitement']; ?> .delegation').before('<?php
        echo $this->Html->link(
            $this->Html->tag('i', '', ['class' => 'fa fa-repeat']),
            ['plugin' => 'cakeflow', 'controller' => 'traitements', 'action' => 'traiterDelegationsPassees', $visa['Visa']['traitement_id'], $visa['Visa']['numero_traitement'], 'traiter'],
            ['escape' => false, 'style' => 'text-decoration:none;margin-right:5px;', 'title' => __('Mettre à jour le statut de cette étape')]
        );
        ?>');
        <?php
    endforeach;
endif;
?>
    });
});
</script>
