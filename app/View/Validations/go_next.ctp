<?php

$this->Html->addCrumb(__('Sauter une étape ou des étapes'));
echo $this->Bs->tag('h3', __('Sauter une étape ou des étapes'));

echo $this->BsForm->create('Traitement', [
    'url' => [
        'controller' => 'validations',
        'action' => 'goNext', $delib_id
    ]
    ]
);

echo $this->BsForm->select('etape', $etapes, ['label' => __('Destinataire'), 'title' => __('Sélectionnez l\'étape'), 'help' => __('A quelle étape voulez-vous envoyer le projet ?')]);

echo $this->BsForm->input('Commentaire', ['type' => 'textarea', 'label' => __('Commentaire'), 'rows' => '2', 'required' => true]) ;

echo $this->Html2->btnSaveCancel('', $previous);
echo $this->BsForm->end();
echo $this->Bs->scriptBlock("$('#TraitementGoNextForm').addClass('waiter')");
