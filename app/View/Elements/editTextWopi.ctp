<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$templateId = 'Template_'.$type.'_'.$delib['Deliberation']['id'];

echo $this->Bs->link($this->Bs->icon('file-alt', ['4x']), 'javascript:void(0);', [
        'class' => 'media-left wd-modal-edit-wopi',
        'data-wd-edit-text-id' => $templateId,
        'escapeTitle' => false]) .
    $this->Bs->div('media-body') .
    $this->Bs->tag('h4', $delib['Deliberation'][$type . "_name"], ['class' => 'media-heading']) .
    $this->Bs->div('btn-group') .
    $this->Bs->btn(
        $this->Bs->icon('pencil-alt') . ' ' . __('Modifier'),
        'javascript:void(0);',
        [
            'type' => 'primary',
            'escapeTitle' => false,
            'size' => 'xs',
            'class' => 'media-left wd-modal-edit-wopi',
            'data-wd-edit-text-id' => $templateId,
            'data-wd-edit-text-wopi-client-url' => $wopiClientUrl,
            'data-wd-edit-text-wopi-src' => $wopiFileSrc,
        ]
    );
?>
