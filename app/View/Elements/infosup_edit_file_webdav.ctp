<?php

if (!empty($infosup['name']) && !isset($infosup['tmp_name']) && $infosupdef['type'] == 'odtFile') {
    $display_editer_fichier = 'block';
} else {
    $display_editer_fichier = 'none';
}

$link = $download = [
    'controller' => 'Infosups',
    'action' => 'download',
    $infosup['id']];

if ($infosupdef['type'] == 'odtFile') {
    $link = $infosup['file_infosup_url'];
}

echo $this->Bs->div('editer_fichier_' . $infosupdef['code'], null, [
        'style' => 'display:' . $display_editer_fichier]) .
    $this->Bs->row() .
    $this->Bs->col('xs3') .
    $this->Bs->tag('label', $infosupdef['nom'], ['class' => 'pull-right']) .
    $this->Bs->close() .
    $this->Bs->col('xs9') .
    $this->Bs->div('media') .
    $this->Bs->link($this->Bs->icon('file-alt', ['4x']), $link, ['class' => 'media-left', 'escape' => false]) .
    $this->Bs->div('media-body') .
    $this->Bs->tag('h4', $infosup['name'], ['class' => 'media-heading']) .
    $this->Bs->div('btn-group');

echo $this->Bs->btn($this->Bs->icon('trash') . ' ' . __('Supprimer'), 'javascript:void(0);', [
    'type' => 'danger',
    'escapeTitle' => false,
    'size' => 'xs',
    'data-wd-infosup-infosupdef-code' => $infosupdef['code'],
    'confirm' => __('Voulez-vous vraiment supprimer le fichier joint ?' . "\n\n" . 'Attention : ne prendra effet que lors de la sauvegarde' . "\n"),
    'class' => 'media-left wd-infosup-file'
]);

if (!$disabled) {
    if ($infosupdef['type'] == 'odtFile') {
        echo $this->Bs->btn($this->Bs->icon('pencil-alt') . ' ' . __('Modifier'), $link, [
            'type' => 'primary',
            'escapeTitle' => false,
            'size' => 'xs',
            'class' => 'media-left',
        ]);
    }

    echo $this->BsForm->hidden(
        $fieldName . '.file_infosup',
        [
            'id' => 'infosup_hidden_' . $infosupdef['code'],
            'value' => (!empty($infosup['file_infosup'])) ? $infosup['file_infosup'] : '']
    );
    echo $this->BsForm->hidden(
        $fieldName . '.file_infosup_url',
        ['value' => (!empty($infosup['file_infosup_url'])) ? $infosup['file_infosup_url'] : '']
    );

    echo $this->BsForm->hidden(
        $fieldName . '.name',
        ['value' => (!empty($infosup['name'])) ? $infosup['name'] : '']
    );
}

echo $this->Bs->close(6);