<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

echo $this->Bs->row();

echo $this->Bs->col('xs4').
        $this->Bs->tag(
            'ul',
            $this->Bs->link(
                $this->Bs->tag('span', '', ['class' => 'badge']).
                        $this->Bs->icon('pause').' '. 'Projets en cours de rédaction',
                ['action' => 'projets',  'new'],
                ['id'=>'newCount',
                        'title'=>__('Visualiser les projets'),
                        'escapeTitle' => false,
                        'class' => 'list-group-item list-group-item-info active']
            ).

            $this->Bs->tag(
                'li',
                $this->Bs->tag('h4', 'Par types d\'acte', ['class'=>'list-group-item-heading']).
            $this->Bs->tag('canvas', '', ['id'=>'newProjectByTypeActePieChart', 'class'=>'text-center']).
            $this->Bs->tag('p', '', ['id'=>'newProjectByTypeActeLegend', 'class'=>'text-center']).
            $this->Bs->tag('p', $expand.' '.$this->Bs->btn(
                $this->Bs->icon('list'),
                ['action' => 'projets',  'new', 'type_acte'],
                [
                    'type'=> 'primary',
                    'size'=> 'xs',
                    'title'=>__('Visualiser les projets'),
                    'escapeTitle' => false]
            )),
                ['class' => 'list-group-item']
            ).
        $this->Bs->tag(
            'li',
            $this->Bs->tag('h4', 'Par services', ['class'=>'list-group-item-heading']).
                $this->Bs->tag('canvas', '', ['id'=>'newProjectByServicePieChart', 'class'=>'text-center']).
                $this->Bs->tag('p', $expand.' '.$this->Bs->btn(
                    $this->Bs->icon('list'),
                    ['action' => 'projets', 'new', 'service'],
                    [
                    'type'=> 'primary',
                    'size'=> 'xs',
                    'title'=>__('Visualiser les projets'),
                    'escapeTitle' => false]
                )),
            ['class' => 'list-group-item']
        ),
            ['class'=>'list-group']
        ).$this->Bs->close();
echo $this->Bs->col('xs4').
        $this->Bs->tag(
            'ul',
            $this->Bs->link(
                $this->Bs->tag('span', '', ['class' => 'badge']).
                        $this->Bs->icon('road ').' '. 'Projets en cours de validation',
                ['action' => 'projets',  null,  'valid'],
                ['id'=>'validCount',
                        'title'=>__('Visualiser les projets'),
                        'escapeTitle' => false,
                        'class' => 'list-group-item list-group-item-success active']
            ).

            $this->Bs->tag(
                'li',
                $this->Bs->tag('h4', 'Par types d\'acte', ['class'=>'list-group-item-heading']).
            $this->Bs->tag('canvas', '', ['id'=>'validProjectByTypeActePieChart', 'class'=>'text-center']).
            $this->Bs->tag('p', $expand.' '.$this->Bs->btn(
                $this->Bs->icon('list'),
                ['action' => 'projets', 'valid', 'type_acte'],
                [
                    'type'=> 'primary',
                    'size'=> 'xs',
                    'title'=>__('Visualiser les projets'),
                    'escapeTitle' => false]
            )),
                ['class' => 'list-group-item']
            ).
            $this->Bs->tag(
                'li',
                $this->Bs->tag('h4', 'Par services', ['class'=>'list-group-item-heading']).
            $this->Bs->tag('canvas', '', ['id'=>'validProjectByServicePieChart', 'class'=>'text-center']).
            $this->Bs->tag('p', $expand.' '.$this->Bs->btn(
                $this->Bs->icon('list'),
                ['action' => 'projets', 'valid', 'service'],
                [
                    'type'=> 'primary',
                    'size'=> 'xs',
                    'title'=>__('Visualiser les projets'),
                    'escapeTitle' => false]
            )),
                ['class' => 'list-group-item']
            ),
            ['class'=>'list-group']
        ).$this->Bs->close();
echo $this->Bs->col('xs4').
                $this->Bs->tag(
                    'ul',
                    $this->Bs->link(
                        $this->Bs->tag('span', '', ['class' => 'badge']).
                        $this->Bs->icon('hourglass-end').' '. 'Projets en retard',
                        ['action' => 'projets',  null,  'delay'],
                        ['id'=>'delayCount',
                        'title'=>__('Visualiser les projets'),
                        'escapeTitle' => false,
                        'class' => 'list-group-item list-group-item-danger active']
                    ).

            $this->Bs->tag(
                'li',
                $this->Bs->tag('h4', 'Par types d\'acte', ['class'=>'list-group-item-heading']).
            $this->Bs->tag('canvas', '', ['id'=>'delayProjectByTypeActePieChart', 'class'=>'text-center']).
            $this->Bs->tag('p', $expand.' '.$this->Bs->btn(
                $this->Bs->icon('list'),
                ['action' => 'projets', 'delay', 'type_acte'],
                [
                    'type'=> 'primary',
                    'size'=> 'xs',
                    'title'=>__('Visualiser les projets'),
                    'escapeTitle' => false]
            )),
                ['class' => 'list-group-item']
            ).
            $this->Bs->tag(
                'li',
                $this->Bs->tag('h4', 'Par services', ['class'=>'list-group-item-heading']).
            $this->Bs->tag('canvas', '', ['id'=>'delayProjectByServicePieChart', 'class'=>'text-center']).
            $this->Bs->tag('p', $expand.' '.$this->Bs->btn(
                $this->Bs->icon('list'),
                ['action' => 'projets', 'delay', 'service'],
                [
                    'type'=> 'primary',
                    'size'=> 'xs',
                    'title'=>__('Visualiser les projets'),
                    'escapeTitle' => false]
            )),
                ['class' => 'list-group-item']
            ),
                    ['class'=>'list-group']
                ).$this->Bs->close();
echo $this->Bs->close();

 echo $this->Bs->scriptBlock(
     'require(["domReady"], function (domReady) {'
    . 'domReady(function () {'
    .'
    {
        $(\'#newCount span[class*="badge"]\').html(data_new_dashletCount);

        var newProjectByTypeActePieChart_ctx = document.getElementById(\'newProjectByTypeActePieChart\').getContext(\'2d\');
        if(typeof data_new_dashletChartByTypeActe !== "undefined"){
            var newProjectByTypeActePieChart = new Chart(newProjectByTypeActePieChart_ctx, {
              type: \'doughnut\',
              data: data_new_dashletChartByTypeActe

            });
        }
        var newProjectByServicePieChart_ctx = $("#newProjectByServicePieChart").get(0).getContext("2d");
        if(typeof data_new_dashletChartByService !== "undefined"){
            var newProjectByServicePieChart = new Chart(newProjectByServicePieChart_ctx, {
              type: \'doughnut\',
              data: data_new_dashletChartByService
            });
        }
    }

    {
        $(\'#validCount span[class*="badge"]\').html(data_valid_dashletCount);

        var validProjectByTypeActePieChart_ctx = $("#validProjectByTypeActePieChart").get(0).getContext("2d");
        if(typeof data_valid_dashletChartByTypeActe !== "undefined"){
            var validProjectByTypeActePieChart = new Chart(validProjectByTypeActePieChart_ctx, {
              type: \'doughnut\',
              data: data_valid_dashletChartByTypeActe
            });
        }

        var validProjectByServicePieChart_ctx = $("#validProjectByServicePieChart").get(0).getContext("2d");
        if(typeof data_valid_dashletChartByService !== "undefined"){
            var validProjectByServicePieChart = new Chart(validProjectByServicePieChart_ctx, {
              type: \'doughnut\',
              data: data_valid_dashletChartByService
            });
        }
    }

    {
        $(\'#delayCount span[class*="badge"]\').html(data_delay_dashletCount);

        var delayProjectByTypeActePieChart_ctx = $("#delayProjectByTypeActePieChart").get(0).getContext("2d");
        if(typeof data_delay_dashletChartByTypeActe !== "undefined"){
            var delayProjectByTypeActePieChart = new Chart(delayProjectByTypeActePieChart_ctx, {
              type: \'doughnut\',
              data: data_delay_dashletChartByTypeActe
            });
        }

        var delayProjectByServicePieChart_ctx = $("#delayProjectByServicePieChart").get(0).getContext("2d");
        if(typeof data_delay_dashletChartByService !== "undefined"){
            var delayProjectByServicePieChart = new Chart(delayProjectByServicePieChart_ctx, {
              type: \'doughnut\',
              data: data_delay_dashletChartByService
            });
        }

    }

'  . '});'
        . '});'
 );
