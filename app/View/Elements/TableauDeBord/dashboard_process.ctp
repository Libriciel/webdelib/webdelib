<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

echo $this->Bs->tag('h4', __('Traitement des projets dans les circuits de validation'));

echo $this->Bs->row();
echo $this->Bs->col('xs4').
        $this->Bs->tag(
            'ul',
            $this->Bs->link(
                $this->Bs->tag('span', '', ['class' => 'badge']).
                        $this->Bs->icon('clock').' '. 'Projets à venir',
                ['action' => 'traitements',  null,  'process_future'],
                ['id'=>'process_futureCount',
                        'title'=>__('Visualiser les traitemens'),
                        'escapeTitle' => false,
                        'class' => 'list-group-item list-group-item-info active']
            ).

            $this->Bs->tag(
                'li',
                $this->Bs->tag('h4', 'Par services', ['class'=>'list-group-item-heading']).
            $this->Bs->tag('canvas', '', ['id'=>'process_futureProjectByServicePolarAreaChart', 'class'=>'text-center']).
            $this->Bs->tag('p', $expand.' '.$this->Bs->btn(
                $this->Bs->icon('list'),
                ['action' => 'traitements', 'process_future', 'service'],
                [
                    'type'=> 'primary',
                    'size'=> 'xs',
                    'title'=>__('Visualiser les traitemens'),
                    'escapeTitle' => false]
            )),
                ['class' => 'list-group-item']
            ),
            ['class'=>'list-group']
        ).$this->Bs->close();


echo $this->Bs->col('xs4').
                $this->Bs->tag(
                    'ul',
                    $this->Bs->link(
                        $this->Bs->tag('span', '', ['class' => 'badge']).
                        $this->Bs->icon('play').' '. 'Projets à traiter',
                        ['action' => 'traitements',  null,  'process'],
                        ['id'=>'processCount',
                        'escapeTitle' => false,
                        'title'=>__('Visualiser les traitemens'),
                        'class' => 'list-group-item list-group-item-info active']
                    ).

            $this->Bs->tag(
                'li',
                $this->Bs->tag('h4', 'Par services', ['class'=>'list-group-item-heading']).
            $this->Bs->tag('canvas', '', ['id'=>'processProjectByServicePolarAreaChart', 'class'=>'text-center']).
            $this->Bs->tag('p', $expand.' '.$this->Bs->btn(
                $this->Bs->icon('list'),
                ['action' => 'traitements', 'process', 'service'],
                [
                    'type'=> 'primary',
                    'size'=> 'xs',
                    'title'=>__('Visualiser les traitemens'),
                    'escapeTitle' => false]
            )),
                ['class' => 'list-group-item']
            ),
                    ['class'=>'list-group']
                ).$this->Bs->close();
echo $this->Bs->col('xs4').
        $this->Bs->tag(
            'ul',
            $this->Bs->link(
                $this->Bs->tag('span', '', ['class' => 'badge']).
                        $this->Bs->icon('lock').' '. 'Projets traités',
                ['action' => 'traitements',  null,  'processed'],
                ['id'=>'processedCount',
                        'escapeTitle' => false,
                        'title'=>__('Visualiser les traitemens'),
                        'class' => 'list-group-item list-group-item-info active']
            ).

            $this->Bs->tag(
                'li',
                $this->Bs->tag('h4', 'Par services', ['class'=>'list-group-item-heading']).
            $this->Bs->tag('canvas', '', ['id'=>'processedProjectByServicePolarAreaChart', 'class'=>'text-center']).
            $this->Bs->tag('p', $expand.' '.$this->Bs->btn(
                $this->Bs->icon('list'),
                ['action' => 'traitements', 'processed', 'service'],
                [
                    'type'=> 'primary',
                    'size'=> 'xs',
                    'title'=>__('Visualiser les traitemens'),
                    'escapeTitle' => false]
            )),
                ['class' => 'list-group-item']
            ),
            ['class'=>'list-group']
        ).$this->Bs->close();
echo $this->Bs->close();

echo $this->Bs->scriptBlock(
    'require(["domReady"], function (domReady) {'
    . 'domReady(function () {'
    .'
   {
        $(\'#process_futureCount span[class*="badge"]\').html(data_process_future_dashletCount);

        var process_futureProjectByServicePolarAreaChart_ctx = $("#process_futureProjectByServicePolarAreaChart").get(0).getContext("2d");
        if(typeof data_process_future_dashletPolarAreaChartByService !== "undefined"){
            var process_futureProjectByServicePolarAreaChart = new Chart(process_futureProjectByServicePolarAreaChart_ctx, {
              type: \'polarArea\',
              data: data_process_future_dashletPolarAreaChartByService
            });
        }
    }
    {
        $(\'#processCount span[class*="badge"]\').html(data_process_dashletCount);

        var processProjectByServicePolarAreaChart_ctx = $("#processProjectByServicePolarAreaChart").get(0).getContext("2d");
        if(typeof data_process_dashletPolarAreaChartByService !== "undefined"){
            var processProjectByServicePolarAreaChart = new Chart(processProjectByServicePolarAreaChart_ctx, {
              type: \'polarArea\',
              data: data_process_dashletPolarAreaChartByService
            });
        }
    }

    {
        $(\'#processedCount span[class*="badge"]\').html(data_processed_dashletCount);

        var processedProjectByServicePolarAreaChart_ctx = $("#processedProjectByServicePolarAreaChart").get(0).getContext("2d");
        if(typeof data_processed_dashletPolarAreaChartByService !== "undefined"){
            var processedProjectByServicePolarAreaChart = new Chart(processedProjectByServicePolarAreaChart_ctx, {
              type: \'polarArea\',
              data: data_processed_dashletPolarAreaChartByService
            });
        }
    }'
    . '});'
        . '});'
);
