<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

echo $this->Bs->modal(
    __('Nouvelle annexe'),
        $this->Form->hidden('Annexe.TemplateAnnexeNum.projet_id', ['class' => 'wd-modal-annexe-add-projet-id', 'value' => !empty($id) ? $id : 'TemplateProjetId', 'disabled'=> true]) .
        $this->Form->hidden('Annexe.TemplateAnnexeNum.numAnnexe', ['class' => 'wd-modal-annexe-add-annexe-position','value' => 0, 'disabled'=> true]) .
        $this->Bs->div('error-message alert alert-danger wd-modal-annexe-add-erreur', '', ['style' => 'display: none']) .
        // $this->Bs->tag('br') .
        $this->Bs->div('spacer', '') .
        $this->BsForm->input('Annexe.TemplateAnnexeNum.file', [
            'label' => __('Fichier'),
            'type' => 'file',
            'data-icon' =>$this->Bs->icon('file-alt'),
            'data-btnClass' => 'btn-primary',
            'data-text' => $this->Bs->icon('file-alt') .' ' . ('Choisir un fichier'),//FIX
            //'data-text' => ' ' . __('Choisir un fichier'),
            //'data-placeholder' => __('Pas de fichier'),
            'data-badge' => false,
            'required'=> true,
            'disabled'=> true,
            'title' => __('Choisir un fichier'),
            'class' => 'wd-nofilestyle'
            ]) .// filestyle fileuploader
        $this->Bs->div('spacer', '') .
        $this->BsForm->input('Annexe.TemplateAnnexeNum.titre', [
            'label' => __('Titre'),
            'value' => '',
            'disabled'=> true,
            'autocomplete' => 'off']) .
        $this->Bs->div('spacer', '') .
        $this->BsForm->checkbox('Annexe.TemplateAnnexeNum.ctrl', [
            'label' => __('Joindre au contrôle de légalité'),
            'disabled'=> true ,
            'checked' => false]) .
        $this->BsForm->input('Annexe.TemplateAnnexeNum.typologiepiece_code', [
                'type'=>'select',
                'label' => __('Type de pièce'),
                'data-placeholder' => __('Sélectionner le type de pièce'),
                'data-allow-clear' => true,
                'disabled'=> true,
                'empty' => true,
                'autocomplete' => 'off',
                'escape' => false]).
        $this->Bs->div('spacer', '') .
        $this->BsForm->checkbox('Annexe.TemplateAnnexeNum.fusion', [
            'label' => __('Joindre à la fusion'),
            'disabled'=> true,
            'checked' => false]) .
        $this->Bs->div('spacer', ''),
    [
    'class' => 'wd-modal-annexe-add',
    'data' => ['data-wd-projet-id' => !empty($id) ? $id : 'Template'],
    'form' => false],
    [
    'open' => [
        'name' => $this->Bs->icon('plus-circle') . ' ' . __('Ajouter une annexe'),
        'class' => 'wd-modal-btn-annexe-add',
        'options' => [
            'data-wd-projet-id' => !empty($id) ? $id : 'Template',
            'type' => 'success',
            'escapeTitle' => false]],
    'close' => [
      'name' => $this->Bs->icon('times-circle') . ' ' . __('Annuler'),
      'class' => 'btn-danger'],
    'confirm' => [
        'name' => $this->Bs->icon('plus-circle') . ' ' . __('Ajouter annexe'),
        'link' => 'javascript:void(0);',
        'class' => 'btn-success wd-modal-btn-annexe-add-confirm',
        'options' => ['escapeTitle' => false]]
]
);
