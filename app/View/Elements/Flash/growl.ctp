<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->scriptStart(['inline' => false]);

if (empty($params['settings'])) {
    $params['settings'] = [];
}
$type ='';

echo 'require(["domReady", "appMain"], function (domReady) {
    domReady(function () {
    $.growl({';
if (!empty($params['type'])) {
    switch ($params['type']) {
        case 'info':
            //FIX
            $type = 'info';
            $options = [
                'icon' => 'fa fa-info-circle',
                'glue' => 'before',
                'title' => '<strong>' . __('Information') . '</strong>'
            ];
            break;
        case 'important':
        case 'success':
            //FIX
            $type = 'success';
            $options = [
                'icon' => 'fa fa-check-circle',
                'glue' => 'before',
                'title' => '<strong>' . __('Important') . '</strong>'
            ];
            break;
        case 'warning':
            $type = 'warning';
            $options = [
                'sticky' => 'true',
                'icon' => 'fa fa-exclamation-triangle',
                'title' => '<strong>' . __('Attention') . '</strong>'
            ];
            break;
        case 'warning':
        case 'error':
        case 'danger':
            $type = 'danger';
            $options = [
                'sticky' => 'true',
                'icon' => 'fa fa-exclamation-circle',
                'title' => '<strong>' . __('Erreur') . '</strong>'
            ];
            break;
        default:
            $type = 'info';
            $options = [
                'icon' => 'fa fa-info-circle',
                'title' => '<strong>' .  __('Information') . '</strong>'
            ];
    }
} else {
    $type = 'info';
    $options = [
        'icon' => 'fa fa-info-circle',
        'title' => '<strong>' . __('Information') . '</strong>'
    ];
}

foreach ($options as $key => $option) {
    echo $key . ': \' ' . $option . '\',';
}
echo 'message: \' ' . str_replace(["\rn", "\r", "\n",'\rn', '\r', '\n'], '<br />', h($message)) . '\'';
echo '},{';
$params['settings'] += ['offset' => ['x' => 80, 'y' => 100]];
foreach ($params['settings'] as $key => $setting) {
    if (is_array($setting)) {
        $params = $key . ': {';
        foreach ($setting as $key_param => $param) {
            $params.= $key_param . ': \'' . $param . '\',';
        }
        echo substr($params, 0, -1) . '},';
    } else {
        echo $key . ': \'' . $setting . '\', ';
    }
}
echo 'type: \'' . $type . '\', ';
echo 'template: \'<div data-growl="container" class="alert" role="alert">'
 . '<button type="button" class="close" data-growl="dismiss">'
 . '<span aria-hidden="true">×</span><span class="sr-only">' . __('Fermer') . '</span></button>'
 . '<span data-growl="icon"></span>&nbsp;<span data-growl="title"></span><br />'
 . '<span data-growl="message"></span></div>\'';
echo '});'
. '});});';

$this->Html->scriptEnd();
