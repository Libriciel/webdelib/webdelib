<?php
echo $this->Bs->link($this->Bs->icon('file-alt', ['4x']), (!empty($filename)?$filename:'javascript:void(0);'), [
        'class' => 'media-left',
        'escapeTitle' => false]) .
    $this->Bs->div('media-body') .
    $this->Bs->tag('h4', $delib['Deliberation'][$type . "_name"], ['class' => 'media-heading']) .
    $this->Bs->div('btn-group') .
    $this->Bs->btn(
        $this->Bs->icon('pencil-alt') . ' ' . __('Modifier'),
        (!empty($filename)?$filename:'javascript:void(0);'),
        [
            'type' => 'primary',
            'escapeTitle' => false,
            'size' => 'xs',
            'class' => 'media-left',
        ]
    );