<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

echo $this->Bs->table([[
    'title' => (!empty($traitement_lot) ? $this->Form->checkbox(null, [
        'name'=>'masterCheckbox',
        'id'=> 'masterCheckbox',
        'autocomplete'=>'off']):'').'État'],
    ['title' => 'Vue synthétique', ['width'=>'100%']],
    ['title' => 'Actions']
        ], ['hover', 'striped',/*'bordered'*/]);


if (!empty($projets)) {
    foreach ($projets as $projet) {
        //Gestion des alertes de retard
        if (in_array('traiter', $projet['Actions'], true)) {
            if (!empty($projet['iconeEtat']['status'])) {
                if ($projet['iconeEtat']['status'] == 'warning') {
                    echo $this->Bs->lineColor('warning');
                }
                if ($projet['iconeEtat']['status'] == 'danger') {
                    echo $this->Bs->lineColor('danger');
                }
            }
        }
        $Service_libelle = '<strong>' . __('Service émetteur') . ' : </strong>';

        if (isset($projet['Deliberation']['Service']['name'])) {
            $Service_libelle.=$projet['Deliberation']['Service']['name'];
        } elseif (isset($projet['Service']['name'])) {
            $Service_libelle.=$projet['Service']['name'];
        }
        $Deliberation_objet = '<strong>' . __('Libellé') . ' : </strong>' . (isset($projet['Deliberation']['objet_delib']) && !empty($projet['Deliberation']['objet_delib']) ? $projet['Deliberation']['objet_delib'] : $projet['Deliberation']['objet']);

        $Circuit_last_viseur = '';
        if (!empty($projet['last_viseur'])) {
            $Circuit_last_viseur .= '<strong>' . __('Dernière action de') . ' :' . '</strong><br/>' . $projet['last_viseur'] . '<br/>' . $this->Time->i18nFormat($projet['last_viseur_date'], '%d/%m/%Y à %k:%M');
        }

        $Circuit_nom = '<strong>' . __('Circuit') . ' : </strong>'.(!empty($projet['Circuit']['nom'])?$projet['Circuit']['nom']:'');
        $Circuit_current_step = '<strong>' . __('Étape courante') . ' : </strong>'._($projet['Circuit']['current_step']);

        $Deliberation_titre = '<strong>' . __('Titre') . ' : </strong>'.$projet['Deliberation']['titre']; //
        $Deliberation_date_limite = '<strong>' . __('A traiter avant le') . ' : </strong>' . $this->Time->i18nFormat($projet['Deliberation']['date_limite'], '%d/%m/%Y') . '<br />';

        $Deliberation_rapporteur_principal = '<strong>' . __('Rapporteur') . ' : </strong>'.$projet['Deliberation']['rapporteur_principal'];

        $this->BsForm->setLeft(0);
        $this->BsForm->setRight(10);
        $Seance_libelle = '<strong>' . __('Séance(s)') . ' :</strong><br/>';
        if (in_array('attribuerSeance', $projet['Actions'], true)) {
            $Seance_libelle .=  $this->BsForm->create(
                'Deliberation',
                [
                'url' => [
                  'controller' => 'deliberations',
                  'action' => 'attribuerSeance'],
                'type' => 'post']
            );
            $Seance_libelle .=  $this->BsForm->select(
                'Deliberation.seance_id',
                $projet['Seances'],
                [
                  'class' => 'selectmultiple',
                  'data-placeholder' => __('Sélectionner des séances'),
                  'data-allow-clear' => true,
                  'autocomplete' => 'off',
                  'label' => false,
                  'id' => 'DeliberationSeanceId['.$projet['Deliberation']['id'].']',
                  'empty' => true,
                  'multiple' => true
              ]
            );
            $Seance_libelle.= $this->Bs->btn($this->Bs->icon('save') . ' ' . __('Enregistrer'), null, [
            'tag' => 'button',
            'type' => 'success',
            'id' => 'boutonValider',
            'escapeTitle' => false]);
            $Seance_libelle.=$this->Form->hidden('Deliberation.id', ['value' => $projet['Deliberation']['id']]);
            $Seance_libelle.= $this->BsForm->end();
        } else {
            foreach ($projet['listeSeances'] as $seance) {
                $Seance_libelle.= $this->Bs->icon('tag', [], (!empty($seance['Typeseance']['color']) ? ['style' => 'color: ' . $seance['Typeseance']['color']] : [])) .
                    ' ' . $seance['libelle'] . (isset($seance['date']) && !empty($seance['date']) ? ' : ' . $this->Time->i18nFormat($seance['date'], '%d/%m/%Y') . ' à ' . $this->Time->i18nFormat($seance['date'], '%k:%M') : '') . $this->Bs->tag('br');
            }
        }
        $Typeacte_name = '';
        if (isset($projet['Typeacte']['name'])) {
            $Typeacte_name = '<strong>' . __('Type d\'acte') . ' : </strong>'. ucfirst(mb_strtolower($projet['Typeacte']['name'], 'UTF-8'));
        }
        $Theme_libelle = '<strong>' . __('Thème') . ' : </strong>'.(!empty($projet['Theme']['libelle'])?$projet['Theme']['libelle']:'');

        $Deliberation_num_pref = '<strong>' . __('Classification') . ' : </strong>' . $projet['Deliberation']['num_pref'];

        $etat = $this->Bs->btn(
            $this->Bs->icon($this->ProjetUtil->etat_icon($projet['iconeEtat']['image']), ['lg','iconType' =>  $this->ProjetUtil->etat_iconType($projet['iconeEtat']['image'])]),
            null,
            [
        'tag' => 'button',
        'type' => empty($projet['iconeEtat']['status']) ? $this->ProjetUtil->etat_icon_type($projet['iconeEtat']['image']) : $projet['iconeEtat']['status'],
        'size' => 'lg',
        'option_type' => 'button',
        //'disabled'=>'disabled',
        'data-toggle' => 'popover',
        'data-content' => $this->ProjetUtil->etat_icon_help($projet['iconeEtat']['image']),
        'data-placement' => 'right',
        'escape' => false,
        'title' => $projet['iconeEtat']['titre']
            ]
        );

        $etat.= '<h4 class="text-center"><span class="label label-default" ' . (!empty($projet['listeSeances'][0]['color']) ? 'style="background-color: ' . $projet['listeSeances'][0]['color'] . '"' : '') . '>' . $projet['Deliberation']['id'] . '</span></h4>';

        if (!empty($traitement_lot)) {
            $etat.=$this->Bs->tag('br') . $this->Bs->div('text-center', $this->BsForm->checkbox('Deliberation.check.id_' . $projet['Deliberation']['id'], [
                            'checked' => false,
                            'class' => 'masterCheckbox_checkbox',
                            'inline' => true,
                            'label' => false,
                            'autocomplete' => 'off'
        ]));
        }
        if (!empty($projet['User'])) {
            $etat.=$this->Bs->tag('br') . $this->Bs->div(
                'text-center',
                $this->Bs->icon('users', null, [
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'right',
                            'escape' => false,
                            'title' => __('Multi-rédacteurs')
                        ])
            );
        }
        //affichage du dernier commentaire + infotips des 3 dernieres commentaires
        $commentaires = $projet['Deliberation']['commentaires'];
        $Deliberation_commentaires = '';
        if (!empty($commentaires)) {
            $Deliberation_commentaires .= '<strong>' . __('Dernier commentaire') . ' :</strong><br/>';

            $sLis = '';
            //on recupere le dernier commentaire posté pour l'affichage
            foreach ($commentaires as $key => $commentaire) {
                //on prends le dernier commentaire reçu
                if ($key == 0) {
                    $lastCommentaire = $commentaire['User']['prenom'] . ' ' . $commentaire['User']['nom']
                        . $this->Time->i18nFormat($commentaire['Commentaire']['created'], '%d/%m/%Y à %k:%M') . '<br/>'
                        . $this->Text->truncate($commentaire['Commentaire']['texte'], 100, [/* 'ellipsis' => '...', */ 'exact' => false]);
                }

                $sLis .= $this->Bs->div('list-group-item', '<span class="label label-info">' . $commentaire['User']['prenom'] . ' ' . $commentaire['User']['nom'] . '</span><br/><div class="spacer"></div>'
                    . $this->Time->i18nFormat($commentaire['Commentaire']['created'], '%d/%m/%Y à %k:%M')
                    . $this->Bs->tag('p', $commentaire['Commentaire']['texte'], ['class' => 'list-group-item-text']));
            }
            $sLis .= $this->Bs->link(__('Visualiser tous les commentaire(s)'), [
            'controller' => 'deliberations',
            'action' => 'view',
            $projet['Deliberation']['id'], 'nameTab' => 'commentaires']);
            $Deliberation_commentaires .= $lastCommentaire . ' ...' . $this->Bs->link(
                __('Visualiser la suite'),
                '#btnPopover' . $projet['Deliberation']['id'],
                [
                    'tag' => 'button',
                    'size' => 'lg',
                    'data-toggle' => 'popover',
                    'data-content' => h($sLis),
                    'id' => 'btnPopover' . $projet['Deliberation']['id'],
                    'data-html' => true,
                    'data-placement' => 'bottom',
                    'escape' => false,
                    'title' => __('Commmentaire(s)')
                        ]
            );
        }

        //affichage de la liste des annexes
        $Deliberation_annexes = '<strong>' . __('Annexe(s)') .' : '. '</strong>';
        if (!empty($projet['Deliberation']['annexes'])) {
            $Deliberation_annexes .= '<div class="dropdown">'
                . '<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">'
                . __('Liste des annexes')
                . ' <span class="label label-info label-as-badge">' . count($projet['Deliberation']['annexes']) . '</span>'
                . ' <span class="caret"></span>'
                . '</button>'
                . '<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">';
            foreach ($projet['Deliberation']['annexes'] as $annexe) {
                $Deliberation_annexes.=
                    '<li role="presentation">' .
                    $this->Bs->btn($this->Bs->icon('download'). ' ' . $annexe['filename'], [
                        'controller' => 'annexes', 'action' => 'download', $annexe['id']], [
                            'escapeTitle'=> false,
                            'role' => 'menuitem',
                        'tabindex' => -1]) .
                    '</li>';
            }
            $Deliberation_annexes .= '</ul></div>';
        }
        if (in_array('view', $projet['Actions'], true)) {
            echo $this->Bs->cell($etat);

            /**
             * COLONE VUE SYNTHETIQUE : Creation du tableau a partir du JSON 9cases
             */
            $contentFields = $this->Bs->row();
            foreach ($projet['fields'] as $key => $field) {
                if (isset($field['model']) && $field['model'] == 'Infosupdef') {
                    //$key . 'h' .
                    $content = (!empty($field['nom']) ? '<strong>' . $field['nom'] . '</strong> : ' : '');
                    if (isset($field['code']) && isset($field['compacte'][$field['code']])) {
                        if ($field['type'] == 'richText') {
                            $content .=  $this->Html->link('[Afficher le texte]', 'javascript:void(0);', [
                                    'class' => 'showTexteEnrichi',
                                    'data-infosup-hide'=> 'off',
                                    'data-infosup-hide-on-texte'=> __("[Masquer le texte]"),
                                    'data-infosup-hide-off-texte'=> __("[Afficher le texte]"),
                                    'data-infosup-id'=> 'Fck_'.$field['code'].'_'.$projet['Deliberation']['id'],
                                     ]) .
                                $this->Bs->div('spacer') . $this->Bs->close() .
                                $this->Form->input($field['code'], [
                                    'label' => '',
                                    'type' => 'textarea',
                                    'readonly'=> true,
                                    'style' => 'display: none',
                                    'class' => 'Fck_'.$field['code'].'_'.$projet['Deliberation']['id'],
                                    'value' => $field['compacte'][$field['code']],
                                    'id' => 'Fck_'.$field['code'].'_'.$projet['Deliberation']['id']]) .
                        $this->Bs->div('spacer') . $this->Bs->close();
                        } elseif ($field['type'] == 'boolean') {
                            $content .= ($field['compacte'][$field['code']]==1?__('Oui'):__('Non'));
                        } elseif ($field['type'] == 'listmulti') {
                            $content .= implode(', ', $field['compacte'][$field['code']]);
                        } else {
                            $content .= $field['compacte'][$field['code']];
                        }
                    }
                    $contentFields .= $this->Bs->col('xs4') . $content . $this->Bs->close();
                } else {
                    if (!empty($field['model']) && !empty($field['fields'])) {
                        $contentFields .= $this->Bs->col('xs4') . ${$field['model'] . '_' . $field['fields']} . $this->Bs->close();
                    } else {
                        // Cellule vide
                        $contentFields .= $this->Bs->col('xs4') . $this->Bs->close();
                    }
                }
                if ($key == 2 || $key == 5 || $key == 8) {
                    $contentFields .= $this->Bs->close();
                    if ($key != 8) {
                        $contentFields .= $this->Bs->row();
                    }
                }
            }
            $contentFields .= $this->Bs->close();

            echo $this->Bs->cell($contentFields, 'max');

            echo $this->element('Projet/actions', ['projet' => $projet, 'cell_class' => 'text-right']);
        }
    }
} else {
    echo $this->Bs->cell(
        $this->Bs->tag('p', $this->Bs->icon('folder-open')
                    . ' ' . __('Aucun projet à afficher')),
        'text-center',
        ['colspan' => 3]
    );
}
echo $this->Bs->endTable();
