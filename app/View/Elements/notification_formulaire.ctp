<?php
echo $this->Bs->div(null,null,['id' => $form]);
echo $this->Html->tag('p', __('Nous vous notifierons par e-mail à chaque fois qu\'il arrive quelque chose vous concernant.') .
    __(' Les paramètres ci-dessous vous permettront de contrôler la façon dont vous souhaitez être averti.'));
$this->BsForm->setLeft(0);

echo $this->BsForm->checkbox('User.not_accept_notif', [
        'label' => __('Ne jamais notifier'),
        'autocomplete' => 'off',
        'id' => 'notificationsFormNotAccept',
        'checked' => $not_accept_notif
    ]
);
$this->BsForm->setLeft($left);
$this->BsForm->setRight($right);

echo $this->Html->tag('div', null, ['id' => 'notificationsFormDiv']) .
    $this->Html->tag('h6', 'Je souhaite recevoir des alertes mail pour :');

echo $this->BsForm->radio('User.mail_error_send_tdt_auto', $notif, [
        'label' => __('Une erreur lors  de l\'envoi automatique des actes à télétransmettre'),
        'autocomplete' => 'off',
        'id' => $form.'mail_error_send_tdt_auto',
        'value' => $notificationsForm['User']['mail_error_send_tdt_auto']
    ]) .
    $this->BsForm->radio('User.mail_error_sae', $notif, [
        'label' => __('Un versement sae est en erreur ou a été refusé'),
        'autocomplete' => 'off',
        'id' => $form.'mail_error_sae',
        'value' =>$notificationsForm['User']['mail_error_sae']
    ]).
    $this->BsForm->radio('User.mail_majtdtcourrier', $notif, [
        'label' => __('Un acte télétransmis est en erreur ou a été annulé'),
        'autocomplete' => 'off',
        'id' => $form.'mail_majtdtcourrier',
        'value' =>$notificationsForm['User']['mail_majtdtcourrier']
    ]).
    $this->BsForm->radio('User.mail_insertion', $notif, [
        'label' => __('Un projet est inséré dans un circuit dont je suis valideur'),
        'autocomplete' => 'off',
        'id' => $form.'mail_insertion',
        'value' => $notificationsForm['User']['mail_insertion']
    ]) .
    $this->BsForm->radio('User.mail_traitement', $notif, [
        'label' => __('Un projet est à traiter dans ma bannette'),
        'autocomplete' => 'off',
        'id' => $form.'mail_traitement',
        'value' => $notificationsForm['User']['mail_traitement']
    ]) .
    $this->BsForm->radio('User.mail_refus', $notif, [
        'label' => __('Un projet que j\'ai créé ou validé a été refusé'),
        'autocomplete' => 'off',
        'id' => $form.'mail_refus',
        'value' => $notificationsForm['User']['mail_refus']
    ]) .
    $this->BsForm->radio('User.mail_modif_projet_cree', $notif, [
        'label' => __('Un projet que j\'ai créé est modifié'),
        'autocomplete' => 'off',
        'id' => $form.'mail_modif_projet_cree',
        'value' =>$notificationsForm['User']['mail_modif_projet_cree']
    ]) .
    $this->BsForm->radio('User.mail_modif_projet_valide', $notif, [
        'label' => __('Un projet que j\'ai visé est modifié'),
        'autocomplete' => 'off',
        'id' => $form.'mail_modif_projet_valide',
        'value' => $notificationsForm['User']['mail_modif_projet_valide']
    ]) .
    $this->BsForm->radio('User.mail_retard_validation', $notif, [
        'label' => __('Un projet que je dois traiter est en retard'),
        'autocomplete' => 'off',
        'id' => $form.'mail_retard_validation',
        'value' => $notificationsForm['User']['mail_retard_validation']
    ]) .
    $this->BsForm->radio('User.mail_projet_valide', $notif, [
        'label' => __('Un projet que j\'ai rédigé est validé'),
        'autocomplete' => 'off',
        'id' => $form.'mail_projet_valide',
        'value' => $notificationsForm['User']['mail_projet_valide']
    ]) .
    $this->BsForm->radio('User.mail_projet_valide_valideur', $notif, [
        'label' => __('Un projet que j\'ai traité est validé'),
        'autocomplete' => 'off',
        'id' => $form.'mail_projet_valide_valideur',
        'value' => $notificationsForm['User']['mail_projet_valide_valideur']
    ]);

$this->BsForm->setDefault();
$this->Bs->close();

?>
<script type="text/javascript">
    require(['domReady'], function (domReady) {
        domReady(function () {
            notificationAcceptCheck('<?php echo $form ?>');

            $("#<?php echo $form ?> #notificationsFormNotAccept").on('change', function () {
                notificationAcceptCheck('<?php echo $form ?>');
                displayNotification('<?php echo $form ?>');
            });
            $("#<?php echo $form ?> #notificationsFormDiv input[type=radio]").change(function() {
                displayNotification('<?php echo $form ?>');
            });

            function displayNotification(form) {
                if($("#" + form + " #notificationsFormDiv input[type=radio]").filter( "[value=1]").filter(":checked").length === 0){
                    $("#" + form + " #notificationsFormNotAccept").prop("checked", true);
                    $("#" + form + " #notificationsFormNotAccept").prop("readOnly", true);
                }
                else {
                    $("#" + form + " #notificationsFormNotAccept").prop("checked", false);
                    $("#" + form + " #notificationsFormNotAccept").prop("readOnly", false);
                }
            }

            function notificationAcceptCheck(form) {
                if ($("#" + form + " #notificationsFormNotAccept").is(":checked")) {
                    $("#" + form + " #notificationsFormDiv input[type=radio]").filter( "[value=1]").prop("checked", false);
                    $("#" + form + " #notificationsFormDiv input[type=radio]").filter( "[value=0]").prop("checked", true);
                }
            }
        });
    });
</script>
