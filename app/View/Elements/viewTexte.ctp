<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/* Initialisation des paramètres */
if (empty($type)) {
    return;
}

$file_icon = 'file-alt';
$download_action = 'download';
$download_controller = 'projets';
switch ($type) {
    case 'projet':
        $libelle = __('Texte de projet');
        $textKey = 'texte_projet';
        $filename = !isset($revision) || $revision==false ? $delib['texte_projet_name']  : $delib['data_version']['Deliberation']['texte_projet_name'] ;
        break;
    case 'synthese':
        $libelle = __('Note de synthèse');
        $textKey = 'texte_synthese';
        $filename = !isset($revision) || $revision==false ?  $delib['texte_synthese_name'] : $delib['data_version']['Deliberation']['texte_synthese_name'];
        break;
    case 'deliberation':
        $libelle = __('Texte d\'acte');
        $textKey = 'deliberation';
        $filename = !isset($revision) || $revision==false ?  $delib['deliberation_name'] : $delib['data_version']['Deliberation']['deliberation_name'];
        break;
    case 'tdt_tampon':
        $download_controller = 'actes';
        if(empty($delib['tdt_tampon'])){
            break;
        }
        $libelle = __('Tampon TDT');
        $textKey = 'tdt_data_pdf';
        $filename = !isset($revision) || $revision==false ?  __('Tdt_tampon_%s.pdf', $delib['id']) : __('Tdt_tampon_%s.pdf', $delib['data_version']['Deliberation']['id']);
        $file_icon = 'file-pdf';
        $download_action = 'downloadTdtTampon';
        break;
    case 'parapheur_bordereau':
        $download_controller = 'actes';
        if(empty($delib['parapheur_bordereau'])){
            break;
        }
        $libelle = __('Bordereau de signature');
        $textKey = 'parapheur_bordereau';
        $filename = !isset($revision) || $revision==false ?  __('Bordereau_signature_%s.pdf', $delib['id']) : __('Bordereau_signature_%s.pdf', $delib['data_version']['Deliberation']['id']);
        $file_icon = 'file-pdf';
        $download_action = 'downloadBordereau';
        break;
}

if (!empty($delib[$textKey])) {
    echo $this->Bs->row();
    echo $this->Bs->col('xs'.$this->BsForm->getLeft(), null, ['class'=>'text-right']) . $this->Html->tag('label', __('%s :', $libelle)) . ' ';
    echo $this->Bs->close();
    echo $this->Bs->col('xs'.$this->BsForm->getRight());
    echo $this->Bs->icon($file_icon, null, ['stylePrefix' => 'far'])
    . ' ' . $filename . ' ' .
    $this->Bs->div('btn-group', null, ['style'=> 'margin-left: 1em']) .
    $this->Bs->btn($this->Bs->icon('download') . ' ' . __('Télécharger'), [
        'controller' => $download_controller,
        'action' => $download_action,
        !isset($revision) || (isset($revision) && !$revision) ? $delib['id'] : $delib['data_version']['Deliberation']['id'], //Si dans version prend l'id de la délibération suivant la structure de la révision.
        $textKey, // type de document
        !isset($revision) ? false : $revision], //si révision renseigne son id.
        [
            'type' => 'default',
            'size' => 'xs',
            'class' => 'media-left',
            'escape' => false,
        ]) .
    $this->Bs->close(3);

    /* echo $this->Html->tag('span', $libelle);
      echo ' : ';
      echo $this->Html->link($filename, array('action' => 'download', $delib['id'], $textKey)); */
}
