<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

// Initialisation des paramètres
if (empty($mode)) {
    $mode = 'edit';
}
if ($mode == 'edit' && empty($ref)) {
    return;
}
// affichage des annexes
//echo $this->Bs->tag('h4', __('Liste des annexes'));

$aTitles = [
    ['title' => __('Position')],
    ['title' => __('Nom du fichier')],
    ['title' => __('Titre')],
    ['title' => __('Taille')],
    ['title' => __('Joindre au contrôle de légalité')],
    ['title' => __('Type de pièce')],
    ['title' => __('Joindre à la fusion')]
];
$aTableOptions = ['caption' => __('Liste des annexes'), 'class' => ['wd-projet-annexe-table']];
if ($mode == 'edit') {
    $aTitles[] = ['title' => __('Actions')];
    $aTableOptions['attributes']['data-wd-projet-id'] = !empty($id) ? $id : 'TemplateProjetId';
}
if (empty($annexes)) {
    $annexes = [];
    //$aTableOptions['attributes']['style'] = 'display:none;';
}
echo $this->Bs->table($aTitles, ['hover', 'striped', 'bootstrap-table'], $aTableOptions);

//Mettre dans le modèle
$aPosition = [];
for ($index = 0; $index < count($annexes); $index++) {
    $aPosition[$index + 1] = $index + 1;
}
if ($mode == 'edit') {
    echo $this->Bs->lineAttributes(['style' => 'display: none', 'class' => 'success annexeLine']);
    echo $this->Bs->cell('TemplateProjetId', null, ['id' => 'AnnexeAddPosition']);
    echo $this->Bs->cell('TemplateProjetId', null, ['id' => 'AnnexeAddNameFile']);
    echo $this->Bs->cell('TemplateProjetId', null, ['id' => 'AnnexeAddTitle']);
    echo $this->Bs->cell('TemplateProjetId', null, ['id' => 'AnnexeAddTaille']);
    echo $this->Bs->cell('TemplateProjetId', null, ['id' => 'AnnexeAddTdt']);
    echo $this->Bs->cell('TemplateProjetId', null, ['id' => 'AnnexeAddTdtTypologiepieceCodeLibelle']);
    echo $this->Bs->cell('TemplateProjetId', null, ['id' => 'AnnexeAddFusion']);
    echo $this->Bs->cell(
    $this->Bs->div('btn-group', $this->Bs->btn($this->Bs->icon('undo').' '. __('Annuler'), 'javascript:void(0);', [
        'type' => 'warning',
        'size' => 'sm',
        'title' => __('Annuler l\'ajout de cette annexe'),
        'escape' => false,
        'id' => 'AnnexeAddUndo',
        'data-wd-projet-id' => 'TemplateProjetId',
        'data-wd-projet-annexe-id' => 'TemplateProjetId'
    ]))
);
}

foreach ($annexes as $key => $annexe) {
    if ($mode == 'edit') {
        $aOptions = [
            'id' => $mode . 'Annexe' . $annexe['id'],
            'data-wd-projet-annexe-id' => $annexe['id'],
            'data-wd-projet-position' => $annexe['position'],
            'data-wd-projet-position-init' => $annexe['position']
        ];
        if (!empty($id)) {
            $aOptions['data-wd-projet-id'] = $id;
        }
        $this->Bs->lineAttributes($aOptions);

        $sPosition = $this->Html->tag('div', $annexe['position'], [
            'class' => 'annexe-view wd-annexe-edit-position'
        ]);
        $sPosition .= $this->Bs->div('annexe-edit', null, ['style' => 'display:none;']);
        $sPosition .= !empty($id) ? $this->BsForm->hidden('AnnexesAModifier.' . $annexe['id'] . '.foreign_key', [
          'value' => $id,
          'disabled' => true]) :'';
        $sPosition .= $this->BsForm->select('AnnexesAModifier.' . $annexe['id'] . '.position', $aPosition, [
            'value' => $annexe['position'],
            'autocomplete' => 'off',
            'label' => false,
            'disabled' => true,
            'class' => 'input-sm selectone wd-projet-annexe-select', //selectone
            'data-wd-projet-annexe-id' => $annexe['id'],
            'data-wd-projet-id' => $id,
        ]);
        $sPosition .= $this->Bs->close();

        echo $this->Bs->cell($sPosition);

        // lien de téléchargement de la version pdf de l'annexe
        $sAnnexeTitre = $this->Html->tag('div', $annexe['filename'],  ['class' => 'annexefilename annexe-view']);
        $sAnnexeTitre .= $this->Bs->div('annexe-edit', null, ['style' => 'display:none;']);
        if (!empty($annexe['edit'])) {
            $sAnnexeTitre .= $this->Html->link($this->Bs->icon('pencil-alt') . ' ' . $annexe['filename'], $annexe['file_url'], [
                'class' => 'urlWebdavAnnexeEdit',
                'escape' => false,
                'title' => __('Modifier le fichier directement depuis votre poste en utilisant le protocole WebDAV')]);
            $sAnnexeTitre .= $this->BsForm->hidden('AnnexesAModifier.' . $annexe['id'] . '.file', [
              'class' => 'urlWebdavAnnexeHidden',
              'value' => $annexe['file'],
              'disabled'=> 'disabled'
            ]);
        } else {
            $sAnnexeTitre .= $this->Html->tag('span', $annexe['filename'], [
                'class' => 'urlWebdavAnnexeDisable',
                'escape' => false,
                'title' => __('Édition WebDAV désactivée pour ce type de fichier')
            ]);
        }
        $sAnnexeTitre .= $this->Bs->close();

        echo $this->Bs->cell($sAnnexeTitre);

        $sTitre = $this->Html->tag('div', $annexe['titre'], [
            'class' => 'annexe-view',
            'id' => 'afficheAnnexeTitre' . $annexe['id'],
            'data-valeur-init' => $annexe['titre']]);
        $sTitre .= $this->Bs->div('annexe-edit', null, ['style' => 'display:none;']);
        $this->BsForm->setLeft(0);
        $sTitre .= $this->BsForm->input('AnnexesAModifier.' . $annexe['id'] . '.titre', [
                    'id' => 'modifieAnnexeTitre' . $annexe['id'],
                    'label' => false,
                    'inline' => true,
                    'value' => $annexe['titre'],
                    'maxlength' => '200',
                    'disabled' => true,
                    'class' => 'editAnnexeTitre',
                ]) . $this->Bs->close();
        $this->BsForm->setLeft(3);
        echo $this->Bs->cell($sTitre);
        echo $this->Bs->cell(AppTools::humanFilesize($annexe['size']));
        $sCtrlLegalite = $this->Html->tag('div', $annexe['joindre_ctrl_legalite'] ? __('Oui') : __('Non'), [
            'id' => 'afficheAnnexeCtrl' . $annexe['id'],
            'class' => 'annexe-view',
            'data-valeurinit' => $annexe['joindre_ctrl_legalite']]);
        $sCtrlLegalite .= $this->Bs->div('annexe-edit', null, ['style' => 'display:none;']);
        $sCtrlLegalite .= $this->BsForm->checkbox('AnnexesAModifier.' . $annexe['id'] . '.joindre_ctrl_legalite', [
                    'id' => 'modifieAnnexeCtrl' . $annexe['id'],
                    'label' => false,
                    'inline' => true,
                    'checked' => ($annexe['joindre_ctrl_legalite'] == 1),
                    'disabled' => 'disabled',
                ]) . $this->Bs->close();


        echo $this->Bs->cell($sCtrlLegalite);

        $typologiepieceCode = $this->Html->tag('div', (!empty($annexe['typologiepiece_code_libelle']) ? $annexe['typologiepiece_code_libelle']: ''), [
            'id' => 'afficheTypologiepieceCode' . $annexe['id'],
            'class' => 'annexe-view',
            'data-valeurinit' => $annexe['typologiepiece_code']]);
        $typologiepieceCode .= $this->Bs->div('annexe-edit', null, ['style' => 'display:none;']);
        $typologiepieceCode .= $this->BsForm->input('AnnexesAModifier.' . $annexe['id'] . '.typologiepiece_code', [
                'id' => 'modifieAnnexeTypologiepieceCode' . $annexe['id'],
                'type'=>'select',
                'label' => false,
                'data-placeholder' => __('Sélectionner le type de pièce'),
                'data-allow-clear' => 'true',
                'autocomplete' => 'off',
                'empty' => true,
                'disabled'=> true,
                'escape' => false]) . $this->Bs->close();

        echo $this->Bs->cell($typologiepieceCode);

        //$this->Html->tag('span', $annexe['joindre_fusion'] ? 'Oui' : 'Non')
        $sJoindreFusion = $this->Html->tag('div', $annexe['joindre_fusion'] ? __('Oui') : __('Non'), [
            'id' => 'afficheAnnexeFusion' . $annexe['id'],
            'class' => 'annexe-view',
            'data-valeur-init' => $annexe['joindre_fusion']]);

        $sJoindreFusion .= $this->Bs->div('annexe-edit', null, ['style' => 'display:none;']);
        $sJoindreFusion .= $this->BsForm->checkbox('AnnexesAModifier.' . $annexe['id'] . '.joindre_fusion', [
                    'id' => 'modifieAnnexeFusion' . $annexe['id'],
                    'label' => false,
                    'inline' => true,
                    'checked' => ($annexe['joindre_fusion'] == 1),
                    'disabled' => 'disabled',
                ]) . $this->Bs->close();

        echo $this->Bs->cell($sJoindreFusion);

        $sActions = $this->Bs->div('btn-group annexe-edit-btn');
        $sActions .= $this->Bs->btn($this->Bs->icon('download'), [
            'controller' => 'annexes',
            'action' => 'download', $annexe['id']], [
            'type' => 'default',
            'size' => 'sm',
            'title' => __('Télécharger l\'annexe'),
            'escape' => false,
            'id' => __('voirAnnexe') . $annexe['id'] . $ref,
        ]);

        $sActions .= $this->Bs->btn($this->Bs->icon('pencil-alt'), 'javascript:void(0);', [
            'type' => 'primary',
            'size' => 'sm',
            'title' => __('Modifier les caractéristiques de l\'annexe'),
            'escape' => false,
            'id' => 'modifierAnnexe' . $annexe['id'] . $ref,
            'data-wd-projet-annexe-id' => $annexe['id'],
            'data-wd-projet-ref' => $ref,
            'class' => 'wd-projet-annexe-edit'
        ]);

        $sActions .= $this->Bs->btn($this->Bs->icon('trash'), 'javascript:void(0);', [
                    'type' => 'danger',
                    'size' => 'sm',
                    'title' => __('Supprimer cette annexe'),
                    'id' => 'supprimerAnnexe' . $annexe['id'] . $ref,
                    'escapeTitle' => false,
                    'data-wd-projet-annexe-id' => $annexe['id'],
                    'data-wd-projet-id' => $id,
                    'class' => 'wd-projet-annexe-delete',
                ]) . $this->Bs->close();

        $sActions .= $this->Bs->div('btn-group annexe-cancel-btn', null, ['style' => 'display:none;']);
        $sActions .= $this->Bs->btn($this->Bs->icon('undo') . ' ' . __('Annuler'), 'javascript:void(0);', [
                    'type' => 'warning',
                    'size' => 'sm',
                    'title' => __('Annuler les modifications de l\'annexe'),
                    'escapeTitle' => false,
                    'data-wd-projet-annexe-id' => $annexe['id'],
                    'data-wd-projet-id' => $id,
                    'class' => 'wd-projet-annexe-edit-exit',
                ]) . $this->Bs->close();

        $sActions .= $this->Bs->div('btn-group annexe-delete-btn', null, ['style' => 'display:none;']);
        $sActions .= $this->Bs->btn($this->Bs->icon('undo') . ' ' . __('Annuler'), 'javascript:void(0);', [
                    'type' => 'warning',
                    'size' => 'sm',
                    'title' => __('Annuler la suppression de l\'annexe'),
                    'escapeTitle' => false,
                    'data-wd-projet-annexe-id' => $annexe['id'],
                    'data-wd-projet-id' => $id,
                    'class' => 'wd-projet-annexe-delete-exit',
                ]) . $this->Bs->close();

        echo $this->Bs->cell($sActions);
    }
}
echo $this->Bs->endTable();

if ($mode != 'edit') {
    return;
}
// div pour la suppression des annexes
echo $this->Html->tag('div', '', [
  'class' => 'wd-annexe-delete-data',
  'data-wd-projet-id' => !empty($id) ? $id : 'TemplateProjetId',
  'style' => 'display:none'
]); //supprimeAnnexes

// div pour l'ajout des annexes
echo $this->Html->tag('div', '', [
  'class' => 'wd-annexe-add-data',
  'data-wd-projet-id' => !empty($id) ? $id : 'TemplateProjetId',
  //'style' => 'display: none'
]);
echo $this->Html->tag(null, '<br />');

echo $this->element('annexeModal', ['ref' => $ref, 'id' => !empty($id) ? $id : 'TemplateProjetId']);
