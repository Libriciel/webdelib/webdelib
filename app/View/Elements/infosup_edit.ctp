<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

if (!empty($infosupdefs)) {
    foreach ($infosupdefs as $infosupdef) {
        // Amélioration 4.1 : on ne peut modifier une infosup qu'en fonction du profil
        $disabled = true;

        foreach ($infosupdef['Profil'] as $profil) {
            if ($profil['id'] == AuthComponent::user('profil_id')) {
                $disabled = false;
            }
        }

        $fieldName = 'Infosup.' . $infosupdef['Infosupdef']['code'];

        $fieldId = 'Infosup' . Inflector::camelize($infosupdef['Infosupdef']['code']);

        $value = $this->html->value($fieldName);

        switch ($infosupdef['Infosupdef']['type']) {
            // texte
            case 'text':
                if (!$disabled) {
                    echo $this->BsForm->input($fieldName, [
                        'label' => $infosupdef['Infosupdef']['nom'],
                        'type' => 'textarea',
                        'rows' => '2',
                        'value' => empty($value) ? $infosupdef['Infosupdef']['val_initiale'] : null,
                        'title' => $infosupdef['Infosupdef']['commentaire'],
                        'id' => 'infosup_' . $infosupdef['Infosupdef']['code'],
                        ]);
                } else {
                    echo $this->BsForm->input($fieldName, [
                        'label' => $infosupdef['Infosupdef']['nom'],
                        'type' => 'textarea',
                        'rows' => '2',
                        'value' => empty($value) ? $infosupdef['Infosupdef']['val_initiale'] : null,
                        'title' => $infosupdef['Infosupdef']['commentaire'],
                        'readonly' => $disabled,
                        'style' => $disabled ? 'cursor: not-allowed' : '',
                        'id' => 'infosup_' . $infosupdef['Infosupdef']['code']]);
                }
                break;

            // booléen
            case 'boolean':
                $checked = '';
                if (!is_null($value) && $value == true) {
                    $checked = true;
                } elseif (is_null($value) && !is_null($infosupdef['Infosupdef']['val_initiale'])) {
                    $checked = $infosupdef['Infosupdef']['val_initiale'];
                }
                if (!$disabled) {
                    $options = [
                        'autocomplete' => 'off',
                        'title' => $infosupdef['Infosupdef']['commentaire'],
                        'label' => $infosupdef['Infosupdef']['nom'],
                        'id' => 'infosup_' . $infosupdef['Infosupdef']['code'],
                        'checked' => $checked
                      ];
                    echo $this->BsForm->checkbox($fieldName, $options);
                } else {
                    echo $this->BsForm->checkbox($fieldName, [
                        'autocomplete' => 'off',
                        'title' => $infosupdef['Infosupdef']['commentaire'],
                        'label' => $infosupdef['Infosupdef']['nom'],
                        'checked' => $checked,
                        'disabled' => 'disabled',
                        'style' => 'cursor: not-allowed',
                        'id' => 'infosup_' . $infosupdef['Infosupdef']['code']]);
                    echo $this->Form->input($fieldName, ['type' => 'hidden', 'id' => false]);
                }
                break;

            //date
            case 'date':
                if (!$disabled) {
                    echo $this->BsForm->datetimepicker(
                        $fieldName,
                        [
                        'language' => 'fr',
                        'autoclose' => 'true',
                        'format' => 'dd/mm/yyyy',
                        'minView' => 2],
                        [
                        'label' => $infosupdef['Infosupdef']['nom'],
                        'title' => __('Choisissez une date'),
                        'style' => $disabled ? 'cursor: not-allowed' : 'cursor:pointer',
                        'help' => __('Cliquez sur le champ ci-dessus pour choisir la date '),
                        'value' => empty($value) ? $infosupdef['Infosupdef']['val_initiale'] : null]
                    );
                } else {
                    echo $this->BsForm->datetimepicker(
                        $fieldName,
                        [
                        'language' => 'fr',
                        'autoclose' => 'true',
                        'format' => 'dd/mm/yyyy',
                        'minView' => 2],
                        [
                        'label' => $infosupdef['Infosupdef']['nom'],
                        'disabled' => 'disabled',
                        'value' => empty($value) ? $infosupdef['Infosupdef']['val_initiale'] : null]
                    );
                }
                break;

            // RichText
            case 'richText':
                if (!$disabled) {
                    echo $this->BsForm->input($fieldName, [
                        'label' => $infosupdef['Infosupdef']['nom'],
                        'type' => 'textarea',
                        'value' => empty($value) ? $infosupdef['Infosupdef']['val_initiale'] : null,
                        'class' => $fieldId]);
                    echo $this->TinyMCE->load($fieldId);
                } else {
                    echo $this->BsForm->input($fieldName, [
                        'label' => $infosupdef['Infosupdef']['nom'],
                        'type' => 'textarea',
                        'style' => 'cursor: not-allowed',
                        'readonly' => true,
                        'value' => empty($value) ? $infosupdef['Infosupdef']['val_initiale'] : null,
                        'class' => $fieldId,
                        'id' => $fieldId]);
                    echo $this->TinyMCE->load($fieldId, true);
                }
                break;

            // fichier ODT
            case 'odtFile':
            // autre fichier
            case 'file':
                echo $this->element('infosup_edit_file_upload', [
                    'infosup' => $value,
                    'infosupdef' => $infosupdef['Infosupdef'],
                    'fieldName' => $fieldName,
                    'disabled' => $disabled
                ]);
                if($value!==null) {
                    echo $this->element('infosup_edit_file_webdav', [
                        'infosup' => $value,
                        'infosupdef' => $infosupdef['Infosupdef'],
                        'fieldName' => $fieldName,
                        'disabled' => $disabled
                    ]);
                    echo $this->element('infosup_edit_file_download', [
                        'infosup' => $value,
                        'infosupdef' => $infosupdef['Infosupdef'],
                        'fieldName' => $fieldName,
                        'disabled' => $disabled
                    ]);
                }
                break;
            // liste simple
            case 'list':
                if (!$disabled) {
                    echo $this->BsForm->input(
                        $fieldName,
                        [
                        'type' => 'select',
                        'options' => $infosuplistedefs[$infosupdef['Infosupdef']['code']],
                        'title' => $infosupdef['Infosupdef']['commentaire'],
                        'label' => $infosupdef['Infosupdef']['nom'],
                        'data-placeholder' => __('Sélectionnez un/une %s', $infosupdef['Infosupdef']['nom']),
                        'data-allow-clear' => true,
                        'empty' => true,
                        'autocomplete' => 'off',
                        'id' => 'infosup_' . $infosupdef['Infosupdef']['code'],
                        'class' => 'selectone']
                    );
                } else {
                    echo $this->BsForm->input(
                        $fieldName,
                        [
                        'type' => 'select',
                        'options' => $infosuplistedefs[$infosupdef['Infosupdef']['code']],
                        'title' => $infosupdef['Infosupdef']['commentaire'],
                        'label' => $infosupdef['Infosupdef']['nom'],
                        'disabled' => 'disabled',
                        'data-placeholder' => __('Pas de sélection possible'),
                        'class' => 'selectone',
                        'data-allow-clear' => true,
                        'empty' => true,
                        'id' => 'infosup_' . $infosupdef['Infosupdef']['code']]
                    );
                }
                break;

            // liste multiple
            case 'listmulti':
                $class = $infosupdef['Infosupdef']['reactcomponent'] ? 'selectmultiple selectMultipleOrderBySelect' :'selectmultiple';
                if (!$disabled) {
                    echo $this->BsForm->input(
                        $fieldName,
                        [
                        'type' => 'select',
                        'options' => $infosuplistedefs[$infosupdef['Infosupdef']['code']],
                        'title' => $infosupdef['Infosupdef']['commentaire'],
                        'label' => $infosupdef['Infosupdef']['nom'],
                        //'selected' => $this->html->value('Infosup.'.$fieldName.$infosupdef['Infosupdef']['code']),
                        'help' => $infosupdef['Infosupdef']['commentaire'],
                        'multiple' => true,
                        'empty' => true,
                        'class' => $class,
                        'autocomplete' => 'off',
                        'data-placeholder' => __('Sélectionnez un/une ou plusieurs') . ' ' . $infosupdef['Infosupdef']['nom'],
                        'data-allow-clear' => true,
                        'id' => 'infosup_' . $infosupdef['Infosupdef']['code']]
                    );
                } else {
                    echo $this->BsForm->input(
                        $fieldName,
                        [
                        'type' => 'select',
                        'options' => $infosuplistedefs[$infosupdef['Infosupdef']['code']],
                        'title' => $infosupdef['Infosupdef']['commentaire'],
                        'label' => $infosupdef['Infosupdef']['nom'],
                        'disabled' => 'disabled',
                        'data-placeholder' => __('Pas de sélection possible'),
                        'data-allow-clear' => true,
                        'empty' => true,
                        'class' => $class,
                        'id' => 'infosup_' . $infosupdef['Infosupdef']['code']]
                    );
                }
                break;

            case 'geojson':
                if (!$disabled) {
                    echo $this->BsForm->input($fieldName. '.geojson', [
                        'type' => 'hidden',
                        'value' => isset($value['geojson']) === true ? $value['geojson'] : null,
                        'id' => Inflector::camelize('Infosup' . $infosupdef['Infosupdef']['code'] . '_geojson'),
                    ]);
                    echo $this->BsForm->input($fieldName . '.label', [
                        'size' => '40',
                        'type' => 'text',
                        'label' => $infosupdef['Infosupdef']['nom'],
                        'value' => isset($value['label']) === true ? $value['label'] : null,
                        'placeholder' => __('Saisissez une adresse'),
                        'data-wd-infosupdef-model' => $infosupdef['Infosupdef']['model'],
                        'data-wd-infosupdef-id' => $infosupdef['Infosupdef']['id'],
                        'wd-geojson' => Inflector::camelize('Infosup' . $infosupdef['Infosupdef']['code'] . '_geojson'),
                        'class' => 'wd-autocomplete-geojson',
                        'autocomplete' => 'off',
                    ]);
                } else {
                    echo $this->BsForm->input($fieldName . '.label_disable', [
                        'size' => '40',
                        'type' => 'text',
                        'label' => $infosupdef['Infosupdef']['nom'],
                        'value' => isset($value['label']) === true ? $value['label'] : null,
                        'disabled' => 'disabled',
                    ]);
                }
                break;
        }
    }
}
