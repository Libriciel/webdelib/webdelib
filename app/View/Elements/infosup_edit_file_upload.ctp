<?php

if (empty($infosup['name']) || isset($infosup['tmp_name'])) {
    $display_choisir_fichier = 'block';
    $display_choisir_fichier_input = [];
} else {
    $display_choisir_fichier = 'none';
    $display_choisir_fichier_input = ['disabled' => 'disabled'];
}

echo $this->Bs->div('choisir_fichier_' . $infosupdef['code'], null, [
    'style' => 'display: ' . $display_choisir_fichier]);

$options = [
    'label' => $infosupdef['nom'],
    'autocomplete' => 'off',
    'type' => 'file',
    'class' => 'filestyle',
    'data-btnClass' => 'btn-primary',
    'data-text' => __('Choisir un fichier'),
    'data-placeholder' => __('Pas de fichier'),
    'title' => $infosupdef['commentaire'],
    $display_choisir_fichier_input,
    'id' => 'infosup_' . $infosupdef['code']];

if ($infosupdef['type'] == 'odtFile') {
    $options['help'] = __('Fichier .odt seulement');
    $options['data-buttonText'] = __('Choisir un fichier odt');
}

if ($disabled) {
    $options['data-disabled'] = 'true';
    $options['disabled'] = true;
}

echo $this->BsForm->input($fieldName, $options);

echo $this->Form->isFieldError($fieldName) ?  $this->Form->error($fieldName) : '' ;

echo $this->Bs->close();