<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

// affichage des annexes

if (empty($annexes)) {
    return;
}

$aTitles = [
    ['title' => __('Nom du fichier')],
    ['title' => __('Titre')],
    ['title' => __('Taille')],
    ['title' => __('Joindre au contrôle de légalité')],
    ['title' => __('Type de pièce')],
    ['title' => __('Joindre à la fusion')],
    ['title' => __('Actions')]
];

echo $this->Bs->table($aTitles, ['hover', 'striped'], ['caption' => __('Liste des annexes')]);

foreach ($annexes as $key => $annexe) {
    echo $this->Bs->cell($annexe['filename']);
    echo $this->Bs->cell($annexe['titre']);
    echo $this->Bs->cell(AppTools::humanFilesize($annexe['size']));
    echo $this->Bs->cell($annexe['joindre_ctrl_legalite'] ? __('Oui') : __('Non'));
    echo $this->Bs->cell($annexe['typologiepiece_code_libelle']);
    echo $this->Bs->cell($annexe['joindre_fusion'] ? __('Oui') : __('Non'));

    $isRevision = isset($annexe['annexeVersion_id']) ? $annexe['annexeVersion_id'] : false;

    $sActions = $this->Bs->div('btn-group annexe-edit-btn');
    $sActions.=$this->Bs->btn($this->Bs->icon('download') . ' ' . __('Télécharger'), [
        'controller' => 'annexes',
        'action' => 'download', $annexe['id'],$isRevision], [
        'type' => 'default',
        'size' => 'sm',
        'title' => __('Télécharger l\'annexe'),
        'escape' => false,
        'id' => __('voirAnnexe') . $annexe['id'] . $ref,
    ]);

    echo $this->Bs->cell($sActions);
}
echo $this->Bs->endTable();
