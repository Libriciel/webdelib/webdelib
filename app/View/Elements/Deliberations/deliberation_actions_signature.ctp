<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

if (empty($deliberation['Deliberation']['signee'])
    || ($deliberation['Deliberation']['signee'] === false)
    || (isset($deliberation['Deliberation']['tdt_status']) && $deliberation['Deliberation']['tdt_status'] === null)) {
    $edit =  (in_array($deliberation['Deliberation']['etat'], [10, 3, 4], true) && $deliberation['Deliberation']['parapheur_etat'] != 1) ?
        $this->Bs->btn($this->Bs->icon('pencil-alt'), ['controller' => 'projets', 'action' => 'edit', $deliberation['Deliberation']['id']], ['type' => 'primary',
            'escapeTitle' => false,
            'class' => '',
            'title' => __('Modifier la délibération avant renvoi :') . ' ' . $deliberation['Deliberation']['objet_delib'],
        ]):null;

    echo $this->Bs->cell(
        $this->Bs->div('btn-group-vertical btn-action-m') . $this->Bs->btn($this->Bs->icon('cog'), ['controller' => 'projets', 'action' => 'genereFusionToClient', $deliberation['Deliberation']['id']], ['type' => 'default',
            'escapeTitle' => false,
            'data-waiter-send' => 'XHR',
            'data-waiter-callback' => 'getProgress',
            'data-waiter-type' => 'progress-bar',
            'data-waiter-title' => __('Générer le document du projet : %s', $deliberation['Deliberation']['id']),
            'class' => 'waiter',
            'title' => __('Générer le document:') . ' ' . $deliberation['Deliberation']['objet_delib']
        ]) . $edit . $this->Bs->close()
    );
} else {
    $actions = '';
    $cancelSignature='';
    //Logical grep for cancelSignature on specific type
    if (isset($acl)) {
        $aclKeys = array_keys($acl);
        $cancelSignature = implode("", preg_grep('/(CancelSignature\z)/', $aclKeys));
    }

    if ((isset($acl[$cancelSignature]) && $acl[$cancelSignature])
          && ((isset($signature) && $signature === true)
          && $deliberation['Deliberation']['signee'] === true)) {
        $actions .= in_array($deliberation['Deliberation']['etat'], [5, 11], true) ?
                  $this->Bs->btn($this->Bs->icon('award'), ['controller' => 'Signatures', 'action' => ($type=='deliberation' ? 'deliberationCancelSignature':  'autresActesCancelSignature'), $deliberation['Deliberation']['id']], ['type' => 'danger',
                      'escapeTitle' => false,
                      'tag' => 'button',
                      'disabled' => 'disabled',
                      'title' => __('Annuler la signature de l\'acte: %s', $deliberation['Deliberation']['id']),
                      'confirm' => __("Êtes-vous sûr de vouloir annuler la signature ?", [$deliberation['Deliberation']['id'],$deliberation['Deliberation']['objet_delib']])
                  ]) :
                  $this->Bs->btn($this->Bs->icon('award'), ['controller' => 'Signatures', 'action' => ($type=='deliberation' ? 'deliberationCancelSignature':  'autresActesCancelSignature'), $deliberation['Deliberation']['id']], ['type' => 'danger',
                      'escapeTitle' => false,
                      'title' => __('Annuler la signature de l\'acte: %s', $deliberation['Deliberation']['id']),
                      'confirm' => __("Êtes-vous sûr de vouloir annuler la signature ?", [$deliberation['Deliberation']['id'],$deliberation['Deliberation']['objet_delib']])
                  ]) ;
    }
    if (!empty($deliberation['Actions']) && in_array('editPostSign', $deliberation['Actions'], true)) {
        //Document signé
        $actions .= $this->Bs->btn($this->Bs->icon('file-code'), [
          'controller' => 'actes', 'action' => 'editPostSign', $deliberation['Deliberation']['id']], [
          'type' => 'primary',
          'escapeTitle' => false,
          'title' => __('Modifier les informations supplémentaires: %s', $deliberation['Deliberation']['id'])
        ]);
    }
    $nested_list = [];
    //Document signé
    $nested_list[] = $this->Bs->link(
        $this->Bs->icon('file-pdf', null, ['stylePrefix' => 'far'])  . ' ' . __('Document signé'),
        [   'controller' => 'actes',
              'action' => 'download', $deliberation['Deliberation']['id']
          ],
        [
              'escapeTitle' => false,
              'title' => __('Télécharger le document: %s', $deliberation['Deliberation']['id'])
          ]
    );

    //Bordereau si présent
    if (!empty($deliberation['Deliberation']['parapheur_bordereau'])) {
        $nested_list[] = $this->Bs->link(
            $this->Bs->icon('file-pdf', null, ['stylePrefix' => 'far']) . ' ' . __('Bordereau de signature'),
            ['controller' => 'actes','action' => 'downloadBordereau', $deliberation['Deliberation']['id']],
            [
                'escapeTitle' => false,
                'title' => empty($deliberation['Deliberation']['parapheur_bordereau']) ? __('Bordereau de signature indisponible.') : __('Télécharger le bordereau de signature')]
        );
    }
    if ($deliberation['Deliberation']['etat'] && in_array($deliberation['Deliberation']['etat'], [10, 1, 2, 3, 4], true)) {
        $actions .= $this->element('Projet/typologie_piece_to_tdt', ['acte' => $deliberation]);
    }

    $actions .= $this->Bs->btn($this->Bs->icon('download') . ' <span class="caret"></span>', ['controller' => 'actes', 'action' => 'download', $deliberation['Deliberation']['id']], ['type' => 'default',
      'escapeTitle' => false, 'class' => 'dropdown-toggle',
      'data-toggle' => 'dropdown']) . $this->Bs->nestedList($nested_list, ['class' => 'dropdown-menu dropdown-menu-right ', 'role' => 'menu']);


    echo $this->Bs->cell($this->Bs->div('btn-group-vertical btn-action-m') . $actions . $this->Bs->close());
}
