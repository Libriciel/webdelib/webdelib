<?php

/*
  Affiche une délib rattachée pour la vue view
  Paramètres :
  array	$delib : tableau de donnée de la délib rattachée
  array	$annexes : tableau des annexes
  string	$natureLibelle : libelle de la nature du projet
 */

/* Initialisation des paramètres */
if (empty($delib)) {
    return;
}
if ($delib['etat'] == 3 || $delib['etat'] == 5) {
    echo $this->Html->tag('h4', __('Délibération n°') . $delib['num_delib']);
} else {
    echo $this->Html->tag('h4', $natureLibelle . ' n°' . $delib['id'] . ' : ' . $delib['objet_delib']);
}

$this->BsForm->setLeft(2);
$this->BsForm->setRight(10);
echo $this->Bs->row();
echo $this->Bs->col('xs'.$this->BsForm->getLeft(), null, ['class'=>'text-right']) . $this->Html->tag('label', __('Libellé :')) . $this->Bs->close().
$this->Bs->col('xs'.$this->BsForm->getRight()) . $delib['objet_delib'] . $this->Bs->close(2);

echo $this->element('viewTexte', ['type' => 'deliberation', 'delib' => $delib]);

echo $this->Bs->row();
echo $this->Bs->col('xs'.$this->BsForm->getLeft(), null, ['class'=>'text-right']) . $this->Html->tag('label', __('Annexe(s) :')) . $this->Bs->close().
$this->Bs->col('xs'.$this->BsForm->getRight());

if (!empty($annexes)) {
    echo $this->element('annexe_view', array_merge(['ref' => 'delibMulti'], ['annexes' => $annexes]));
}
echo $this->Bs->close(2);



//if (!empty($annexes)) {
//    echo '<dt>' . __('Annexes') . '</dt>';
//    echo '<dd>';
//    foreach ($annexes as $annexe) {
//        echo '<br />';
//        if ($annexe['titre']) {
//            echo __('Titre') . ' : ' . $annexe['titre'];
//        }
//        echo '<br />' . __('Nom fichier') . ' : ' . $annexe['filename'];
//        echo '<br />' . __('Joindre au contrôle de légalité') . ' : ' . ($annexe['joindre_ctrl_legalite'] ? __('oui') : __('non'));
//        echo '<br />' . $this->Html->link(__('Télécharger'), array('controller' => 'annexes', 'action' => 'download', $annexe['id'])) . '<br />';
//    }
//    echo '</dd>';
//}
