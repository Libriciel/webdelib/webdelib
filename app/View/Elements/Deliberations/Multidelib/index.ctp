<?php

/**
 * Déliberation principale
 */
if (!empty($this->data['Deliberation']['id'])) {
    echo
    $this->Bs->div('panel-group') .
    $this->Bs->div('panel panel-default', NULL, ['id' => 'delibRattachee' . $this->data['Deliberation']['id'], 'class' => 'delibRattachee']) .
    $this->Bs->div('panel-heading', $this->Bs->row(['data-toggle' => 'collapse', 'data-target' => '#collapse' . $this->data['Deliberation']['id'], 'aria-expanded' => 'true', 'aria-controls' => 'collapse' . $this->data['Deliberation']['id']]) .
            $this->Bs->col('xs8') .
            $this->Bs->tag('h4', '&nbsp;&nbsp;' . __('Délibération principale : ') . $this->data['Deliberation']['id'], ['class' => 'pull-left']) .
            $this->Bs->close() .
            $this->Bs->col('xs4') .
            $this->Bs->close(2), ['role' => 'tab', 'id' => 'collapseHeading' . $this->data['Deliberation']['id']]
    ) .
    $this->Bs->div('panel-collapse collapse in', NULL, [
        'id' => 'collapse' . $this->data['Deliberation']['id'],
        'role' => 'tabpanel',
        'aria-labelledby' => 'collapseHeading' . $this->data['Deliberation']['id']
    ]) .
    $this->Bs->div('panel-body',
            $this->BsForm->input('Deliberation.objet', [
                'label' => __('Libellé'),
                'type' => 'textarea',
                'class' => 'libelle-multidelib',
                'data-wd-libelle' => 'multi-deliberation',
                'required' => true]
                ) .
            //Texte acte
            $this->Bs->div('texteDelibOngletDelib', '', ['id' => 'texteDelibOngletDelib']) .
            //Liste des annexes
            $this->Bs->div('delibPrincipaleAnnexeRatt', '', ['id' => 'delibPrincipaleAnnexeRatt']) .
            $this->Html->tag('span', __('Note : les modifications apportées ici ne prendront effet que lors de la sauvegarde du projet.'),
             ['id' => 'delibPrincipaleAnnexeRatt', 'class' => 'help-block'])) .
    $this->Bs->close(3);
}

/**
 * Délibérations rattachées
 */
echo $this->element('Deliberations/Multidelib/edit');
echo $this->element('Deliberations/Multidelib/add');
