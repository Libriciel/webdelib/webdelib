<?php

// Affichage des délibérations rattachées
echo $this->Bs->div('', null, ['id' => 'ajouteMultiDelib']) . $this->Bs->close() .
// Bouton pour ajouter une nouvelle délibération rattachée
$this->Bs->btn($this->Bs->icon('plus-circle').' '.__('Ajouter une délibération rattachée'), 'javascript:void(0);', [
   'type' => 'success',
   'size' => 'md',
   'escapeTitle' => false,
   'id' => 'ajouterMultiDelib'
 ]);


$champsFileGabarit = '';
//Champs file gabarit partie edition; de la delibération rattachée
if (!empty($gabarits_acte['Typeacte']['gabarit_acte_name'])) {
    $champsFileGabarit = $this->Bs->div('editTexteActeGabarit', $this->Bs->col('xs3') .
            $this->Bs->tag('label', __('Texte acte'), ['class' => 'pull-right']) .
            $this->Bs->close() .
            $this->Bs->col('xs9') .
            $this->BsForm->setLeft(0) .
            $this->BsForm->setRight(12) .
            $this->Bs->div('media') .
            $this->Bs->link($this->Bs->icon('file-alt', ['4x']), 'javascript:void(0);', ['class' => 'media-left', 'escape' => false]) .
            $this->Bs->div('media-body') .
            $this->Bs->tag('h4', $gabarits_acte['Typeacte']['gabarit_acte_name'], ['class' => 'media-heading']) .
            $this->Bs->btn($this->Bs->icon('trash') . ' ' . __('Supprimer'), 'javascript:void(0);', [
                'type' => 'danger',
                'escapeTitle' => false,
                'size' => 'xs',
                'help' => __('Format de fichier png, jpg ou jpeg'),
                'title' => __('Cliquez ici pour ne pas utiliser le gabarit proposé par défaut'),
                'data-wd-projet-add-id' => 'TemplateProjetId',
                'class' => 'media-left wd-projet-multi-gabarit-delete', //supprimerGabaritMultidelibCreation
                'confirm' => __('Voulez-vous vraiment supprimer %s du projet ?', $gabarits_acte['Typeacte']['gabarit_acte_name'])
            ]) .
            $this->BsForm->hidden("MultidelibAdd.TemplateProjetId.gabarit", ['value' => '1', 'disabled' => true, 'class' => 'gabarit_acte_multidelib']) .
            $this->Bs->close() .
            $this->Bs->div('help-block', __('Gabarit par défaut pour ce type d\'acte, pour le modifier en WebDAV, veuillez valider le formulaire puis revenir en édition.')) .
            $this->BsForm->setDefault() .
            $this->Bs->close(2)
    );
}
//Champs de selection du gabarit de la deliberation rattachée
$this->BsForm->setLeft(3);
$this->BsForm->setRight(7);
$champsFileGabarit .=
        $this->Bs->div('selectTexteActeGabarit',
        //Upload texte acte
        $this->BsForm->input("MultidelibAdd.TemplateProjetId.deliberation", [
            'label' => __('Texte acte'),
            'title' => __('Texte acte'),
            'type' => 'file',
            'data-btnClass' => 'btn-primary',
            //'data-icon' => $this->Bs->icon('file-alt'),
            'data-text' => $this->Bs->icon('file-alt') . ' ' . __('Choisir un fichier'),
            'data-placeholder' => __('Pas de fichier'),
            'data-badge' => 'true',
            'accept' => 'application/vnd.oasis.opendocument.text',
            //'class' => 'pull-left', //filestyle texte_acte_multidelib
            //'class' => 'filestyle fileuploader',
            //'data-disabled' => 'true',
            //'disabled' => 'disabled',
            'data-disabled' => 'true',
            'disabled' => true,
            'after' => $this->Bs->close() . $this->Bs->div('col-md-2', null). $this->Bs->btn($this->Bs->icon('eraser') . ' ' . __('Effacer'), 'javascript:void(0);', [
                'type' => 'danger',
                'class' => 'wd-filestyle-clear',
                'title' => __('Cliquez ici pour ne pas utiliser le texte sélectionné'),
                'escapeTitle' => false
              ],['role'=>'group']) . $this->Bs->close()
        ])
        , ['style' => (!empty($gabarits_acte['Typeacte']['gabarit_acte_name'])) ? 'display:none' : 'display:block']
). $this->Bs->close();
$this->BsForm->setDefault();


// Panel add Multidelib
echo $this->Bs->div('panel panel-default panel-group delibRattacheeAddTemplate', NULL, ['data-wd-projet-add-id' => 'TemplateProjetId', 'style' => 'display: none']) .
 $this->Bs->div('panel-heading', $this->Bs->row(['data-toggle' => 'collapse', 'data-target' => '#collapseTemplateProjetId', 'aria-expanded' => 'true', 'aria-controls' => 'collapseTemplateProjetId']) .
        $this->Bs->col('xs8') .
        $this->Bs->tag('h4', __('Nouvelle délibération rattachée'), ['class' => 'pull-left']) .
        $this->Bs->close() .
        $this->Bs->col('xs4') .
        $this->Bs->tag('p', $this->Bs->btn(__('Annuler'), 'javascript:void(0);', ['type' => 'warning', 'id' => 'annulerAjouterDelibRattachee', 'class' => 'text-right', 'icon' => 'undo']
                ), ['class' => 'text-right']) .
        $this->Bs->close(2), ['role' => 'tab', 'id' => 'collapseHeadingTemplateProjetId']
) .
 $this->Bs->div('panel-collapse collapse in', NULL, [
    'id' => 'collapseTemplateProjetId',
    'role' => 'tabpanel',
    'aria-labelledby' => 'collapseHeadingTemplateProjetId'
]) .
 $this->BsForm->setDefault() .
 $this->Bs->div('panel-body',
        $this->BsForm->input('MultidelibAdd.TemplateProjetId.objet_delib', [
            'label' => __('Libellé'),
            'type' => 'textarea',
            'class' => 'libelle-multidelib',
            'disabled' => true,
            'required' => true
          ]) .
        //Gabarit par defaut
        $this->Html->tag('span', $champsFileGabarit, ['id' => 'MultidelibTemplateProjetIdGabaritBloc', 'class' => 'MultidelibGabaritBloc']) .
        //Annexes
        $this->Bs->row() .
        $this->Bs->col('xs12') .
        $this->element('annexe_edit', ['ref' => 'delibRattacheeTemplateProjetId', 'annexes' => []]) .
        $this->Bs->close(2) .
        //espace fin de table
        $this->Bs->div('spacer') . $this->Bs->close()
),
 $this->Bs->close(2);
