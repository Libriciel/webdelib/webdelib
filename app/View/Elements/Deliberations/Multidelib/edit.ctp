<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

if (!empty($this->data['Multidelib'])) {
    foreach ($this->data['Multidelib'] as $multidelib) {
        //liste des boutons d'actions
        $actionsDelib = $this->Bs->div(
            'btn-group text-right',
                $this->Bs->btn($this->Bs->icon('pencil-alt') . ' ' . __('Modifier'), 'javascript:void(0);', [
                      'type' => 'primary',
                      'escapeTitle' => false,
                      'data-wd-projet-id' => $multidelib['id'],
                      'class' => 'multiDeliberationModifier'
                ]) .
                $this->Bs->btn($this->Bs->icon('undo') . ' ' . __('Annuler la modification'), 'javascript:void(0);', [
                    'type' => 'warning',
                    'escapeTitle' => false,
                    'data-wd-projet-id' => $multidelib['id'],
                    'style' => 'display:none; border-bottom-left-radius:4px; border-top-left-radius:4px',
                    'class' => 'multiDeliberationModifierAnnuler'
                ]) .
                $this->Bs->btn($this->Bs->icon('trash') . ' ' . __('Supprimer'), 'javascript:void(0);', [
                    'type' => 'danger',
                    'escapeTitle' => false,
                    'data-wd-projet-id' => $multidelib['id'],
                    'class' => 'multiDeliberationSupprimer',
                    'style' => 'border-bottom-right-radius:4px;border-top-right-radius:4px'
                ]) .
                $this->Bs->btn($this->Bs->icon('undo') . ' ' . __('Annuler la suppression'), 'javascript:void(0);', [
                    'type' => 'warning',
                    'escapeTitle' => false,
                    'data-wd-projet-id' => $multidelib['id'],
                    //'id' => 'annulerSupprimerDelibRattachee' . $multidelib['id'],
                    'style' => 'display:none; border-bottom-left-radius:4px;border-top-left-radius:4px',
                    'class' => 'multiDeliberationSupprimerAnnuler',
        ])
        );

        //champ file du projet
        $champsFile = '';
        if (!empty($multidelib['deliberation_name'])) {
            //Champs d'edition du fichier du projet
            $champsFile = $this->Bs->div(
                'editTexteActe',
                $this->Bs->col('xs3') .
                    $this->Bs->tag('label', __('Texte acte'), ['class' => 'pull-right']) .
                    $this->Bs->close() .
                    $this->Bs->col('xs9') .
                    $this->BsForm->setLeft(0) .
                    $this->BsForm->setRight(12) .
                    $this->Bs->div('media') .
                    $this->Bs->link($this->Bs->icon('file-alt', ['4x']), 'javascript:void(0);', ['class' => 'media-left', 'escapeTitle' => false]) .
                    $this->Bs->div('media-body') .
                    $this->Bs->tag('h4', $multidelib['deliberation_name'], ['class' => 'media-heading']) .
                    $this->Bs->div('btn-group') .
                    $this->Bs->btn($this->Bs->icon('pencil-alt') . ' ' . __('Modifier'), $multidelib['file_url_deliberation'], [
                        'type' => 'primary',
                        'escapeTitle' => false,
                        'size' => 'xs',
                        'class' => 'media-left',
                    ]) .
                    $this->Bs->btn($this->Bs->icon('trash') . ' ' . __('Supprimer'), 'javascript:void(0);', [
                        'type' => 'danger',
                        'data-wd-projet-id' => $multidelib['id'],
                        'escapeTitle' => false,
                        'size' => 'xs',
                        'class' => 'media-left multiDeliberationSupprimerTextDeliberation'
                    ]) .
                    (!empty($multidelib['file_deliberation']) ?
                      $this->BsForm->hidden('Multidelib.' . $multidelib['id'] . '.file_deliberation', [
                        'value' => $multidelib['file_deliberation'],
                        'class' => 'wd-projet-multi-texte-deliberation-file',
                        'disabled' => true,
                        ]) : '') .
                    $this->BsForm->setDefault() .
                    $this->Bs->close(4) .
                    //bouton annuler
                    $this->Bs->div('annulerSupprimerDelibRattachee' . $multidelib['id'] . ' media', $this->Bs->link($this->Bs->icon('file-alt', ['4x']), 'javascript:void(0);', ['class' => 'media-left', 'escapeTitle' => false]) .
                            $this->Bs->div('media-body') .
                            $this->Bs->tag('h4', $multidelib['deliberation_name'], ['class' => 'media-heading']) .
                            $this->Bs->div('btn-group') .
                            $this->Bs->btn($this->Bs->icon('undo') . ' ' . __('Annuler la suppression'), 'javascript:void(0);', [
                                'type' => 'warning',
                                'onclick' => 'annulerSupprimerDelibRattachee(' . $multidelib['id'] . ')',
                                'escapeTitle' => false,
                                'size' => 'xs',
                                'class' => 'media-left',
                                'style' => 'display:block'
                            ]), ['style' => 'display:none;margin-top:0;']) .
                    $this->Bs->close(2) .
                    $this->Bs->div('spacer') . $this->Bs->close()
            );
        }
        $this->BsForm->setLeft(3);
        $this->BsForm->setRight(7);
        //Champs de selection du fichier du projet
        $champsFile .=
                $this->Bs->div(
                    'selectTexteActe',
                //Upload texte acte
                $this->BsForm->input("Multidelib." . $multidelib['id'] . ".deliberation", [
                    'label' => __('Texte acte'),
                    'title' => __('Texte acte'),
                    'type' => 'file',
                    'data-btnClass' => 'btn-primary',
                    //'data-icon' => $this->Bs->icon('file-alt'),
                    'data-text' => $this->Bs->icon('file-alt') . ' ' . __('Choisir un fichier'),
                    'data-placeholder' => __('Pas de fichier'),
                    'data-badge' => 'true',
                    'accept' => 'application/vnd.oasis.opendocument.text',
                    'title' => __('Document'),
                    'class' => 'pull-left', //filestyle texte_acte_multidelib
                    'data-disabled' => 'true',
                    'disabled' => 'disabled',
                    'after' => $this->Bs->close() . $this->Bs->div('col-md-2', null). $this->Bs->btn($this->Bs->icon('eraser') . ' ' . __('Effacer'), 'javascript:void(0);', [
                        'type' => 'danger',
                        'class' => 'wd-filestyle-clear',
                        'data-wd-filestyle-id' => 'Multidelib' . $multidelib['id'] . 'Deliberation',
                        'title' => __('Cliquez ici pour ne pas utiliser le texte sélectionné'),
                        'escapeTitle' => false
                      ], ['role'=>'group']) . $this->Bs->close()
                    ]),
                    ['style' => (!empty($multidelib['deliberation_name'])) ? 'display:none' : 'display:block']
                ). $this->Bs->close();
        $this->BsForm->setDefault();

        //liste des annexes
        $MultidelibAnnex = [];
        if (!empty($multidelib['id']) && isset($this->data['MultidelibAnnex'][$multidelib['id']])) {
            $MultidelibAnnex = $this->data['MultidelibAnnex'][$multidelib['id']];
        }

        echo
        $this->Bs->div('panel-group') .
        $this->Bs->div('panel panel-default delibRattachee', null, [
            'id' => 'delibRattachee' . $multidelib['id']]) .
        $this->Bs->div(
            'panel-heading',
            $this->Bs->row([
                    'data-toggle' => 'collapse',
                    //'data-target'=>'#collapse' . $multidelib['id'],
                    'aria-expanded' => 'false',
                    'class' => 'collapsed',
                    'aria-controls' => 'collapse' . $multidelib['id']]) .
                $this->Bs->col('xs8') .
                $this->Bs->tag('h4', __('Délibération rattachée N° ') . (!empty($multidelib['id'])?$multidelib['id']:'') . '<br/>' . (!empty($multidelib['objet_delib']) ? $multidelib['objet_delib'] : ''), ['class' => 'pull-left']) .
                $this->Bs->close() .
                $this->Bs->col('xs4') .
                //liste des boutons d'actions
                $this->Bs->tag('p', $actionsDelib, ['class' => 'text-right']) .
                $this->Bs->close() .
                $this->Bs->close(),
            ['role' => 'tab', 'id' => 'collapseHeading' . $multidelib['id']]
        ) .
        $this->Bs->div('panel-collapse collapse', null, [
            'id' => 'collapse' . $multidelib['id'],
            'role' => 'tabpanel',
            'aria-expanded' => 'false',
            'aria-labelledby' => 'collapseHeading' . $multidelib['id']
        ]) .
        $this->Bs->div(
            'panel-body',
                $this->BsForm->input('Multidelib.' . $multidelib['id'] . '.objet_delib', [
                    'label' => __('Libellé'),
                    'value' => $multidelib['objet_delib'],
                    'type' => 'textarea',
                    'class' => 'libelle-multidelib',
                    'disabled' => true,
                    'required' => true]) .
                //champ file projet
                $champsFile .
                //Annexes
                $this->Bs->row() .
                $this->Bs->col('xs12') .
                $this->element('annexe_edit', [
                    'ref' => 'delibRattachee',
                    'id' => $multidelib['id'],
                    'annexes' => $MultidelibAnnex
                ]) .
                $this->Html->tag('span', __('Note : les modifications apportées ici ne prendront effet que lors de la sauvegarde du projet.'), ['class' => 'help-block']) .
                $this->Bs->close(2)
        ) .
        $this->Bs->close() .
        //Pour la modification
        $this->BsForm->hidden('Multidelib.' . $multidelib['id'] . '.id', [
          'value' => (!empty($multidelib['id']) ? $multidelib['id'] : ''),
          'disabled' => true
          ]) .
        // info pour la suppression
        $this->BsForm->hidden('MultidelibASupprimer.' . $multidelib['id'], [
          'value' => (!empty($multidelib['id']) ? $multidelib['id'] : ''),
          'disabled' => true
          ]) .
        //espace fin de table
        $this->Bs->div('spacer') . $this->Bs->close() .
        $this->Bs->close(2);
    }
}
