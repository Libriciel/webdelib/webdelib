<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$ActionsList = '';
if ($deliberation['Deliberation']['signee']) {
    if (!empty($deliberation['Actions']) && in_array('editPostSign', $deliberation['Actions'], true)) {
        //Document signé
        $ActionsList .= $this->Bs->btn($this->Bs->icon('file-code'), [
          'controller' => 'actes', 'action' => 'editPostSign', $deliberation['Deliberation']['id']], [
          'type' => 'primary',
          'escapeTitle' => false,
          'title' => __('Modifier les informations supplémentaires: %s', $deliberation['Deliberation']['id'])
        ]);
    }
    $ActionsList .= $this->Bs->btn(
        $this->Bs->icon('download'),
        [
        'controller' => 'actes', 'action' => 'download', $deliberation['Deliberation']['id']
      ],
        [
        'title' => __('Télécharger la délibération signée'),
        'type' => 'default',
        'escapeTitle' => false
      ]
    ) ;
} else {
    $ActionsList='';
    // Si la délibération n'est pas en cours de signature on peut modifier l'acte
    if (in_array($deliberation['Deliberation']['parapheur_etat'], [null, 0, -1], true)) {
        $ActionsList .= $this->Bs->btn($this->Bs->icon('pencil-alt'), [
            'controller' => 'projets', 'action' => 'edit', $deliberation['Deliberation']['id']], ['type' => 'primary',
          'escapeTitle' => false,
          'class' => '',
          'title' => __('Modifier la délibération'),
      ]);
    }
    $ActionsList .= $this->Bs->btn($this->Bs->icon('cog'), ['controller' => 'projets', 'action' => 'genereFusionToClient', $deliberation['Deliberation']['id']], ['type' => 'default',
          'escapeTitle' => false,
          'data-waiter-send' => 'XHR',
          'data-waiter-callback' => 'getProgress',
          'data-waiter-type' => 'progress-bar',
          'data-waiter-title' => __('Générer la délibération : %s', $deliberation['Deliberation']['id']),
          'class' => 'waiter',
          'title' => __('Générer la délibération')
      ]);
}

    echo $this->Bs->cell(
        $this->Bs->div('btn-group-vertical btn-action-m') .
        $ActionsList .
        $this->Bs->close()
    );

$this->Bs->close();
