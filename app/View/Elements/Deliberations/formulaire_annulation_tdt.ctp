<?php

    $formModalEditDeliberationNumber =
        $this->BsForm->create('Deliberation', ['type' => 'post', 'url' => ['controller' => 'Teletransmettre', 'action' => array_key_exists ('deliberationCancelSendTDT', $acl) ? 'deliberationCancelSendToTDT' : 'autresActesCancelSendToTDT', $deliberation['Deliberation']['id']]]);
    $formModalEditDeliberationNumber.= (in_array($deliberation['Deliberation']['tdt_status'],[-1,-3]) ?'        
        <p>
            Avant de pouvoir renvoyer votre acte vers le TDT, il est nécessaire de : 
        </p>
        <ul>
            <li>
            S\'assurer d\'avoir corrigé l’erreur.
            </li>
            
             <li>
            Attribuer un nouveau numéro d’acte à ce nouvel envoi.
            </li>' : '        
        <p>
            Avant de pouvoir renvoyer votre acte vers le TDT, il est nécessaire de : 
        </p>
        <ul>
            <li>
            S\'assurer d\'avoir annulé l\'envoi initial depuis le TDT;
            </li>
            
             <li>
            Attribuer un nouveau numéro d’acte à ce nouvel envoi.
            </li>');

    $formModalEditDeliberationNumber.='
        </ul>
    ';
    $formModalEditDeliberationNumber .=
        $this->BsForm->input('Deliberation.num_delib', ['label'=>'Numéro d\'Acte :  ','value' => $deliberation['Deliberation']['num_delib']]);
    $formModalEditDeliberationNumber .=
        (isset($acl['deliberationCancelSignature']) && $acl['deliberationCancelSignature'] == true || isset($acl['autresActesCancelSignature']) && $acl['autresActesCancelSignature'] == true )?
                $this->BsForm->checkbox('Deliberation.signee', ['label' => __('Annuler la signature'), 'autocomplete' => 'off', 'help'=> __("Cocher la case ci-contre si vous souhaitez annuler la signature de l'acte initial. Si vous devez uniquement modifier la classification de votre acte, il n'est pas nécessaire d'annuler la signature."), 'checked' => false]) :
                null ;
    $formModalEditDeliberationNumber .= ($this->action=='transmit') ? '<p>
        Après avoir cliqué sur le bouton '. (in_array($deliberation['Deliberation']['tdt_status'],[-1,-3]) ? '"Changer l’état de l’acte"' : '"Confirmer l\'annulation"') .', votre acte reviendra : 
        <ul>' .
        ((isset($acl['deliberationCancelSignature']) && $acl['deliberationCancelSignature'] == true || isset($acl['autresActesCancelSignature']) && $acl['autresActesCancelSignature'] == true ) ?
            '<li> dans Post-séance > à télétransmettre  si vous n\'avez pas annulé la signature.</li>' .'<li> dans Post-séance > Signatures si vous avez annulé la signature.</li>' : '<li> dans Post-séance > à télétransmettre.</li>') .
        '</ul>
    </p>': '<p>
        Après avoir cliqué sur le bouton '. (in_array($deliberation['Deliberation']['tdt_status'],[-1,-3]) ? '"Changer l’état de l’acte"' : '"Confirmer l\'annulation"') .', votre acte reviendra :
        <ul>'.
            ((isset($acl['deliberationCancelSignature']) && $acl['deliberationCancelSignature'] == true || isset($acl['autresActesCancelSignature']) && $acl['autresActesCancelSignature'] == true ) ?
            '<li> dans Tous les projets > Autres Actes validés si vous avez annulé la signature.</li>' . "<li> dans Tous les projets > Autres Actes > À télétransmettre  si vous n'avez pas annulé la signature.</li>":"<li> dans Tous les projets > Autres Actes > À télétransmettre.</li>").
        '</ul>
    </p>';
    $formModalEditDeliberationNumber .= $this->BsForm->end();


    echo  $formModalEditDeliberationNumber;