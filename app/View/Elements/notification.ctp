<?php
if($this->Session->read('Auth.User.preference_first_login_active')) {
    return;
}
if($this->Session->read('Auth.User.preference_first_login_later')) {
    return;
}

 echo $this->Bs->modal('Réglages de notification',
                $this->BsForm->create('notificationUser', [
                    'type' => 'post',
                    'url' => ['admin' => false, 'controller' => 'notifications', 'action' => 'notification']
                ]).
                $this->element('notification_formulaire',[
                    'left'=> 9 ,
                    'right'=> 3,
                    'form'=> 'notification_user',
                    'notif' => $notif,
                    'not_accept_notif' => $not_accept_notif
                ]).
                $this->BsForm->end(),
                [   'form' => true,
                    'id' => 'notification-user'
                ],
                [
                    'open' => [],
                    'close' => [
                        'name' => 'Me le rappeler plus tard',
                        'url' => ['admin' => false, 'controller' => 'notifications', 'action' => 'notification', 1],
                        'class' => ''
                    ],
                    'id' => 'cancel-notification-user',
                    'confirm' => [
                        'name' =>$this->Bs->icon('save') .' ' . 'Enregistrer',
                        'class' => 'btn-success',
                        ]
                ]
            );

echo $this->Bs->scriptBlock(
    'require(["domReady"], function (domReady) {
        domReady(function () {
        $("#notification-user").modal("show") 
        });
    });'
);