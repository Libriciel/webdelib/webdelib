<?php

if (isset($traitement_lot) && ($traitement_lot == true) && !empty($actions_possibles)) {
    echo $this->Bs->div('spacer');
    $this->BsForm->setLeft(0);
    $this->BsForm->setRight(12);
    //echo $this->Bs->div(/*'pull-left'*/)
    echo $this->Bs->row() .
            $this->Bs->col('xs3') .
                        $this->BsForm->input('traitement_lot.action', [
                            'type'=>'select',
                            'options' =>  $actions_possibles,
                            'autocomplete' => 'off',
                            'empty' => true,
                            'class' => 'selectone app-traitementParLot',
                            'data-placeholder' => __('Sélectionner une action'),
                            'data-allow-clear' => true,
                            'label' => 'Actions disponibles : ',
                    ])
                    . $this->Bs->close()
            . $this->Bs->col('xs1', ['class'=>'wd-traitement-lot-arrow', 'style' => 'width:2em'])
                    . $this->Bs->icon('arrow-right', null, ['style' => 'margin-top: 2.6em'])
            . $this->Bs->close();

    if (isset($modeles) && isset($actions_possibles['generation'])) {
//        $this->BsForm->setDefault();
        echo $this->Bs->col('xs3', [
          'class'=>'wd-traitement-lot',
          'data-wd-action'=>'generation',
          'style' => 'display: none'
        ]) .
                $this->Bs->row() .
                    $this->Bs->col('xs8') .
                        $this->BsForm->select('traitement_lot.modele', $modeles, [
                            'data-placeholder' => __('Sélectionner un modèle'),
                            'class' => 'selectone',
                            'empty' => true,
                            'label' => false])
                    . $this->Bs->close()
                    . $this->Bs->col('xs1')
                        . $this->Bs->icon('arrow-right', null, ['style' => 'margin-top: 0.8em'])
                    . $this->Bs->close()
                    .$this->Bs->col('xs2')
                        . $this->Form->button($this->Bs->icon('cogs') . ' ' . __("Exécuter") . '<span id="nbDeliberationsChecked"></span>', [
                            'type' => 'button',
                            'class' => 'btn btn-primary pull-left',
                            'title' => __("Exécuter"),
                            'id' => 'generer_multi_delib',
                            'data-wd-action-url' => '/Deliberations/generation',
                            'style' => 'margin-left: 10px'
                        ])
                    . $this->Bs->close(2) . $this->Bs->close() ;
        ;

//        . $this->Bs->close(), array('style' => 'margin-left: 0.8em'));
    }



    if (isset($actions_possibles['generationNumerotation'])) {
        echo
            $this->Bs->col('xs4', [
              'class'=>'wd-traitement-lot',
              'data-wd-action'=>'generationNumerotation',
              'style' => 'display: none']) .
            $this->Bs->btn(
                $this->Bs->icon('cog') . ' ' . __('Générer les numéro(s) d\'acte(s)'),
                null,
                [
                'tag' => 'button',
                'type' => 'primary',
                'name' => 'genereNumActe',
                'id' => 'genereNumActe',
                'data-wd-action-url' => '/actes/generationNumerotation',
                'value' => true,
                'escapeTitle' => false,
                'confirm' => __('Attention ! L\'action est définitive, les numéros d\'actes vont être définis pour les actes séléctionnés'),
                'title' => __('Générer le(s) numéro(s) d\'acte(s) pour le(s) acte(s) sélectionné(s)')]
            ).
                $this->Bs->close();
    }

    if (isset($actions_possibles['autresActesAbandon'])) {
        echo
            $this->Bs->col('xs4', [
              'class'=>'wd-traitement-lot',
              'data-wd-action'=>'autresActesAbandon',
              'style' => 'display: none']) .
            $this->Bs->btn(
                $this->Bs->icon('minus-circle') . ' ' . __('Abandonner le(s) acte(s)'),
                null,
                [
                    'tag' => 'button',
                    'type' => 'warning',
                    'option_type' => 'button',
                    'name' => 'autresActesAbandon',
                    'id' => 'autresActesAbandon',
                    'data-wd-action-url' => '/autres_actes/autresActesAbandon',
                    'value' => true,
                    'data-toggle'=>"modal",
                    'data-target'=>"#modalSteps",
                    'escapeTitle' => false,
                    'confirm' => __('Attention ! L\'action est définitive, les actes sélectionnés seront abandonnés.'),
                    'title' => __('Abandonner le(s) acte(s) sélectionné(s)')]
            ).
            $this->Bs->close();
    }


    if (isset($actions_possibles['validerUrgence'])) {
        echo $this->Bs->col('xs4', [
              'class'=>'wd-traitement-lot',
              'data-wd-action'=>'validerUrgence',
              'style' => 'display: none']) .

                $this->Bs->btn($this->Bs->icon('eject') . ' ' . __('Valider en urgence'), null, [
                    'id' => 'validerUrgence',
                    'data-wd-action-url' => '/validations/validerUrgence',
                    'tag' => 'button',
                    'escapeTitle' => false,
                    'title' => __('Valider en urgence le(s) acte(s) sélectionné(s)'),
                    'type' => 'danger',
                    'name' => 'validerUrgence',
                    'value' => true,
                ]);
        echo $this->Bs->close();
    }

    if (isset($actions_possibles['valider'])) {
        echo $this->Bs->col('xs4', [
              'class'=>'wd-traitement-lot',
              'data-wd-action'=>'valider',
              'style' => 'display: none']) .

                $this->Bs->btn($this->Bs->icon('check') . ' ' . __('Valider'), null, [
                    'id' => 'valider',
                    'data-wd-action-url' => '/validations/valider',
                    'tag' => 'button',
                    'escapeTitle' => false,
                    'title' => __('Valider le(s) acte(s) sélectionné(s)'),
                    'type' => 'success',
                    'name' => 'valider',
                    'value' => true,
                ]);
        echo $this->Bs->close();
    }

    if (isset($actions_possibles['refuser'])) {
        echo $this->Bs->col('xs4', [
              'class'=>'wd-traitement-lot',
              'data-wd-action'=>'refuser',
              'style' => 'display: none']) .

                $this->Bs->btn($this->Bs->icon('times') . ' ' . __('Refuser'), null, [
                    'id' => 'refuser',
                    'data-wd-action-url' => '/validations/refuser',
                    'tag' => 'button',
                    'escapeTitle' => false,
                    'title' => __('Refuser le(s) acte(s) sélectionné(s)'),
                    'type' => 'danger',
                    'name' => 'refuser',
                    'value' => true,
                ]);
        echo $this->Bs->close();
    }

    if (isset($actions_possibles['signatureManuscrite'])) {
//        $this->BsForm->setFormType('horizontal');
        $this->BsForm->setLeft(0);
        $this->BsForm->setRight(12);
        echo $this->Bs->col('xs4', [
          'class'=>'wd-traitement-lot',
          'data-wd-action'=>'signatureManuscrite',
          'data-wd-action-url' =>  $this->action === 'autresActesValides' ?
            '/signatures/sendAutresActesToSignature' :'/signatures/sendDeliberationsToSignature',
          'style' => 'display: none']) .
            $this->BsForm->dateTimepicker(
                'signatureManuscrite.date_acte_all',
                [
                    'language' => 'fr',
                    'autoclose' => 'true',
                    'minView' => '2',
                    'fontAwesome' => true, //calendar
                    'orientation' => 'bottom-right',
                    'pickerPosition' => 'bottom-right'],
                [
                    'label' => __('Date de signature :&nbsp;&nbsp;'),
                    'after' => '</div>',
                    'help' => __('Date appliquée à tous les actes sélectionnés pour signature manuscrite'),
                    'title' => __('Choisissez la date de signature'),
                    'style' => 'cursor:pointer;width:8em;border-top-right-radius:0px;border-bottom-right-radius:0px;background-color: white;',
                    'readonly' => 'readonly',
                    'autocomplete' => 'off',
                    'button' => $this->Bs->btn($this->Bs->icon('award') . ' ' . __('Signature manuscrite'), null, [
                        'id' => 'signatureManuscrite',
                        'tag' => 'button',
                        'style' => 'border-top-left-radius:0px;border-bottom-left-radius:0px',
                        'name' => 'data[signatureManuscrite][is_checked]',
                        'value' => true,
                        'type' => 'success',
                        'escapeTitle' => false,
                        'title' => __('Définir la signature comme manuscrite')])]
            );
        echo $this->Bs->close();
    }

    if (isset($actions_possibles['signatureElectronique'])) {
        $this->BsForm->setLeft(0);
        $this->BsForm->setRight(12);

        $select_circuits = ['' => ''];
        if (!empty($circuits['Parapheur'])) {
            $select_circuits = array_merge($select_circuits, $circuits['Parapheur']);
        }

        echo $this->Bs->col('xs4', [
          'class'=>'wd-traitement-lot',
          'data-wd-action'=>'signatureElectronique',
          'data-wd-action-url' =>  $this->action === 'autresActesValides' ?
              '/signatures/sendAutresActesToSignature' :'/signatures/sendDeliberationsToSignature',
          'style' => 'display: none']) .
        $this->BsForm->selectGroup('Parapheur.circuit_id', $select_circuits, [
            'content' => $this->Bs->icon('cloud-upload-alt')
                . ' ' . __('Envoyer au parapheur') . ' <span id="nbProjetChecked"></span>',
            'type' => 'submit',
            'id' => 'parapheurCircuit',
            'state' => 'success',
            'side' => 'right'], [
            'class' => 'selectone',
            'style' => 'min-width:20em',
            'empty' => true,
            'data-placeholder' => __('Choisir un circuit'),
            'label' => __('Signature électronique :&nbsp;&nbsp;')]) .
        $this->Bs->close();
    }

    if (isset($actions_possibles['sendToGed'])) {
        echo $this->Bs->col('xs4', [
          'class'=>'wd-traitement-lot',
          'data-wd-action'=>'sendToGed',
          'style' => 'display: none']) .

        $this->Bs->btn($this->Bs->icon('cloud-upload-alt') . ' ' . __('Envoyer à la GED'), null, [
            'id' => 'sendToGed',
            'data-wd-action' => '/ExportGed/sendToGedAutreActe',
            'tag' => 'button',
            'escapeTitle' => false,
            'title' => __('Envoyer à la GED'),
            'type' => 'primary',
            'name' => 'data[sendToGed][is_checked]',
            'value' => true,
        ]);
        echo $this->Bs->close();
    }


    if (isset($actions_possibles['depotManuel'])) {
        echo $this->Bs->col('xs4', [
          'class'=>'wd-traitement-lot',
          'data-wd-action'=>'depotManuel',
          'style' => 'display: none']) .
            $this->Bs->btn($this->Bs->icon('university') .' '. __('Déclarer un dépôt manuel'), null, [
                'id' => 'depotManuel',
                'data-wd-action-url' => '/Teletransmettre/depotManuel',
                'tag' => 'button',
                'escapeTitle' => false,
                'title' => __('Déclarer un dépôt manuel sur le TdT.'),
                'type' => 'success',
                'value' => true,
            ]);
        echo $this->Bs->div(
            'alert alert-danger',
            __('Attention: vous allez déclarer que vous avez déposé l\'acte sans passer par l\'application. Ceci est irrévocable !'),
            ['role'=>'alert', 'style'=>'margin-top:2em']
        );
        echo $this->Bs->close();
    }

    if (isset($actions_possibles['sendToTdt'])) {
        // if (isset($actions_possibles['sendToTdt'])) {
        //     echo $this->Bs->col('xs4', [
        //       'class'=>'wd-traitement-lot',
        //       'data-wd-action'=>'sendToTdt',
        //       'style' => 'display: none']) .
        //
        //         $this->Bs->btn($this->Bs->icon('cloud-upload-alt') .' '. __('Envoyer au TdT'), null, [
        //             'id' => 'sendToTdt',
        //             'data-wd-action-url' => '/Deliberations/sendToTdt',
        //             'tag' => 'button',
        //             'escapeTitle' => false,
        //             'title' => __('Envoyer au TdT.'),
        //             'type' => 'success',
        //             'value' => true,
        //         ]);
        //     echo $this->Bs->close();
        // }

        echo $this->Bs->col('xs4', [
              'class'=>'wd-traitement-lot',
              'data-wd-action'=>'sendToTdt',
              'style' => 'display: none']) .
            $this->Bs->btn(
                $this->Bs->icon('cloud-upload-alt') .' '. __('Envoyer au TdT'),
                null,
                [
                    'tag' => 'button',
                    'type' => 'success',
                    'name' => 'sendToTdt',
                    'id' => 'sendToTdt',
                    'data-wd-action-url' => '/teletransmettre/sendToTdt',
                    'value' => true,
                    'escapeTitle' => false,
                    'confirm' => __('Envoyer au TdT.'),
                    'title' => __('Envoyer au TdT.')]
            ).
            $this->Bs->close();
    }

    echo $this->Bs->close(); ?>
  <script type="text/javascript">
  require(['domReady'], function (domReady) {
      domReady(function () {

          $('#ParapheurCircuitId').select2({dropdownAutoWidth : true}); //Pour bien afficher les options
          //Lors d'action sur une checkbox :
          $('input[type=checkbox]').on("change", function () {
              $('#traitement_lotAction').val('').trigger('change');
              selectionChange($(this));
          });

          $('#traitement_lotAction').on("change", function () {
              selectionChange($(this));
          });

          $('#traitement_lotAction').trigger('change');

          $('#generer_multi_delib').click(function () {
              var form_attributes = $(this).closest("form").clone().get(0).attributes;
              var form_now = $(this).closest("form");

              $.each(form_attributes, function (index, attr) {
                  if(!_.isUndefined(attr)) {
                      form_now.removeAttr(attr.name);
                  }
              });

              form_now.
                      attr('action', '<?php echo $this->Html->url([
                          'controller' => 'TraitementParLot',
                          'action' => 'traiter']); ?>').
                      attr('method', 'post').
                      attr('data-waiter-title', '<?php echo __('Traitement par lot'); ?>').
                      attr('class', 'waiter').
                      attr('id', 'Deliberation');

              var find_waiter = new RegExp(/waiter/);
              if(!_.isNull(form_attributes['class']) && !find_waiter.test(form_attributes['class'].value.toLowerCase())){
                  form_now.attr('class', form_attributes['class'].value + ' waiter');
              }else{
                  form_now.appAttendable({});
              }

              if ($('#traitement_lotAction').val() == 'generation') {
                  form_now.attr('data-waiter-send', 'XHR').
                      attr('data-waiter-title', '<?php echo __('Génération du document'); ?>').
                      attr('data-waiter-type', 'progress-bar').
                      attr('data-waiter-callback', 'getProgress');
              }

              form_now.on('progress_send', function (e) {
                  $(this).off('progress_send');
                  $.each(($(this).closest('form').clone().get(0).attributes), function( index, attr ) {
                      if(!_.isUndefined(attr)) {
                          form_now.removeAttr(attr.name);
                      }
                  });
                  $.each(form_attributes, function( index, attr ) {
                      if(!_.isUndefined(attr)) {
                          form_now.attr(attr.name, attr.value);
                      }
                  });
              });

              setTimeout(function () {
                  form_now.trigger("submit");
              }, 0);
          });

          function selectionChange(select) {
              nbChecked = $('td input[type=checkbox]:checked').length;

              //Apposer ou non la class disabled au bouton selon si des checkbox sont cochées (style)
              if (nbChecked > 0 && $('#DeliberationAction').val() != '') {
                  $('#generer_multi_delib').removeClass('disabled');
                  $("#generer_multi_delib").prop("disabled", false);
              } else {
                  $('#generer_multi_delib').addClass('disabled');
                  $("#generer_multi_delib").prop("disabled", true);
              }

              if ($('#traitement_lotAction').val() == 'signatureManuscrite') {
                  select.closest("form").attr('data-waiter-title', '<?php echo __('Signature en cours'); ?>');
              }
              if ($('#traitement_lotAction').val() == 'signatureElectronique') {
                  select.closest("form").attr('data-waiter-title', '<?php echo __('Envoi en signature en cours'); ?>');
              }
              if ($('#traitement_lotAction').val() == 'abandon') {
                select.closest("form").attr('data-waiter-title', '<?php echo __('Abandon(s) en cours'); ?>');
              }
              if ($('#traitement_lotAction').val() == 'generationNumerotation') {
                  select.closest("form").attr('data-waiter-title', '<?php echo __("Génération de la numérotation en cours"); ?>');
              }
              if ($('#traitement_lotAction').val() == 'validerUrgence') {
                console.log(select.closest("form"));
                  select.closest("form").attr('data-waiter-title', '<?php echo __("Validation en urgence"); ?>');
              }

              if ($('#traitement_lotAction').val() == 'valider') {
                  select.closest("form").attr('data-waiter-title', '<?php echo __("Validation en cours"); ?>');
              }

              if ($('#traitement_lotAction').val() == 'refuser') {
                  select.closest("form").attr('data-waiter-title', '<?php echo __("Refus en cours"); ?>');
              }

              if ($('#traitement_lotAction').val() == 'depotManuel') {
                  select.closest("form").attr('data-waiter-title', '<?php echo __("Déclaration de dépôt manuel sur le TDT"); ?>');
              }

              $.each( $('div.wd-traitement-lot'), function () {
                  if ($(this).attr('data-wd-action') === $('#traitement_lotAction').val() ) {
                      if (nbChecked > 0 && _.isUndefined($(this).find(':button').attr('data-lock'))) {
                          $(this).find(':button').removeClass('disabled');
                          $(this).find(':button').prop('disabled',false);
                      } else {
                          $(this).find(':button').addClass('disabled');
                          $(this).find(':button').prop('disabled',true);
                      }
                      $(this).attr('style', 'padding-top: 2em');
                      $(this).show('slow');
                      $(this).closest("form").prop('action', $(this).find(':button').attr('data-wd-action-url'));
                  } else {
                      $(this).hide();
                  }
              });

              if($('#ParapheurCircuitId  option:selected').text() < 1 || nbChecked == 0) {
                  $('#parapheurCircuit').addClass('disabled');
                  $("#parapheurCircuit").prop("disabled", true);
              }else{
                  $('#parapheurCircuit').removeClass('disabled');
                  $("#parapheurCircuit").prop("disabled", false);
              }

              $('#nbDeliberationsChecked').text('(' + nbChecked + ')');
          }
          // On select change
          $('#ParapheurCircuitId').on("change", function () {
              if($('#ParapheurCircuitId  option:selected').text() < 1|| nbChecked == 0) {
                  $('#parapheurCircuit').addClass('disabled');
                  $("#parapheurCircuit").prop("disabled", true);
              }else{
                  $('#parapheurCircuit').removeClass('disabled');
                  $("#parapheurCircuit").prop("disabled", false);
              }
          });
      });
  });
  </script>
  <?php
}
?>
