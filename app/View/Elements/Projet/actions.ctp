<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

    $actions = $this->Bs->div('btn-group-vertical');

        $actions.= $this->Bs->btn($this->Bs->icon('eye', null, ['stylePrefix' => 'far']), [
            'controller' => 'projets', 'action' => 'view', $projet['Deliberation']['id']], [
            'type' => 'default',
            'escape' => false,
            'class' => 'btn-action-m',
            'title' => __('Visualiser le projet'),
        ]);

        if (in_array('edit', $projet['Actions'], true) && empty($projet['Deliberation']['signee'])) {
            $actions.= $this->Bs->btn(
                $this->Bs->icon('pencil-alt'),
                [
                  'controller' => 'projets',
                  'action' => 'edit',
                  $projet['Deliberation']['id']
              ],
                [
                'type' => 'primary',
                'option_type'=> 'button',
                'escape' => false,
                'class' => 'btn-action-m',
                'style' => $this->Lock->lockStyle($projet['Deliberation']),
                'tag'=> $this->Lock->lockButton($projet['Deliberation']),
                'disabled'=> $this->Lock->lockDisabled($projet['Deliberation']),
                'title' => $this->Lock->lockTitle($projet['Deliberation'], __('Modifier le projet')),
                'confirm' => $this->Lock->lockConfirm($projet['Deliberation'])
                ]
            );
        }

        if (in_array('traiter', $projet['Actions'], true)) {
            $actions.= $this->Bs->btn(
                $this->Bs->icon('play'),
                [
              'controller' => 'validations',
              'action' => 'traiter', $projet['Deliberation']['id']
            ],
                [
                'type' => 'success',
                'escapeTitle' => false,
                'option_type'=> 'button',
                'class'=>'btn-action-m',
                'tag'=> $this->Lock->lockButton($projet['Deliberation']),
                'style' => $this->Lock->lockStyle($projet['Deliberation']),
                'disabled'=> $this->Lock->lockDisabled($projet['Deliberation']),
                'title' => $this->Lock->lockTitle($projet['Deliberation'], __('Traiter le projet')),
                'confirm' => $this->Lock->lockConfirm($projet['Deliberation'])
            ]
            );
        }

        if (in_array('goNext', $projet['Actions'], true)) {
            $actions.= $this->Bs->btn($this->Bs->icon('fast-forward'), ['controller' => 'validations', 'action' => 'goNext', $projet['Deliberation']['id']], [
                'type' => 'warning',
                'class'=>'btn-action-m',
                'title' => __('Sauter une ou des étapes pour le projet'),
                'escape' => false]);
        }

        if (in_array('validerEnUrgence', $projet['Actions'], true)) {
            $actions.= $this->Bs->btn($this->Bs->icon('eject'), ['controller' => 'validations', 'action' => 'validerEnUrgence', $projet['Deliberation']['id']], [
                'type' => 'danger', //'danger',
                'escape' =>false,
                'escapeTitle' => false,
                'class' => 'btn-action-m',
                'title' => __('Valider en urgence le projet : %s', $projet['Deliberation']['id'])//), __('Confirmez-vous la validation en urgence du projet "%s"?', $projet['Deliberation']['id'] )
                ]);
            //'title' => __('Valider en urgence le projet : %s', $projet['Deliberation']['id'])), __('Confirmez-vous la validation en urgence du projet "%s"?', $projet['Deliberation']['id'] ));
        }

        if (in_array('attribuerCircuit', $projet['Actions'], true)) {
            $actionAttribuer = ['controller' => 'validations', 'action' => 'attribuerCircuit', $projet['Deliberation']['id']];
            $actions.= $this->Bs->btn(
                $this->Bs->icon('road'),
                $actionAttribuer,
                ['type' => 'primary',
                'escapeTitle' => false,
                'class'=>'btn-action-m',
                'title' => __('Insérer le projet dans un circuit')],
                false
            );
        }
        if ((in_array('delete', $projet['Actions'], true) && !isset($projet['Deliberation']['num_delib']))
            || (in_array('delete', $projet['Actions'], true) && isset($projet['Deliberation']['num_delib']) && $projet['Deliberation']['num_delib']==null)) {
            $actions.= $this->Bs->btn($this->Bs->icon('trash'), ['controller' => 'projets', 'action' => 'delete', $projet['Deliberation']['id']], [
                'type' => 'danger',
                'escapeTitle' => false,
                'class'=>'btn-action-m',
                'confirm' => __('Confirmez-vous la suppression du projet \'%s\' ?', $projet['Deliberation']['objet']),
                'title' => __('Supprimer le projet'),
            ]);
        }

        if (in_array('editPostSign', $projet['Actions'], true)) {
            $actions.=$this->Bs->btn($this->Bs->icon('file-code'), [
              'controller' => 'actes', 'action' => 'editPostSign', $projet['Deliberation']['id']], [
              'type' => 'primary',
              'escapeTitle' => false,
              'title' => __('Modifier les informations supplémentaires: %s', $projet['Deliberation']['id'])
            ]);
        }

        if (in_array('generer', $projet['Actions'], true)) {
            $actions.=$this->Bs->btn($this->Bs->icon('cog'), ['controller' => 'projets', 'action' => 'genereFusionToClient', $projet['Deliberation']['id']], ['type' => 'default',
                'escapeTitle' => false,
                'data-waiter-send' => 'XHR',
                'data-waiter-callback' => 'getProgress',
                'data-waiter-type' => 'progress-bar',
                'data-waiter-title' => __('Générer le document du projet'),
                'class' => 'waiter btn-action-m',
                'title' => __('Générer le document du projet')
            ]);
            /* $actions.= $this->Bs->btn('Générer <rerspan class="caret"></span>',
              array(),
              array('type' => 'default',
              'escape'=>false,'class'=>'dropdown-toggle',
              'data-toggle'=>'dropdown')).
              $this->Bs->nestedList(array(
              $this->Bs->link('Pdf', array('controller' => 'deliberations', 'action' => 'genereFusionToClient', $projet['Deliberation']['id'] , 'projet'),
              array(
              'title' => 'Générer le document PDF du projet ' . $projet['Deliberation']['objet'],
              'class' => 'waiter',
              'data-waiter-title' => 'Génération du PV sommaire en cours')),
              $this->Bs->link('Odt', array('controller' => 'deliberations', 'action' => 'genereFusionToClient', $projet['Deliberation']['id'] , 'deliberation'),
              array(
              'title' => 'Générer le document PDF du projet ' . $projet['Deliberation']['objet'],
              'class' => 'waiter',
              'data-waiter-title' => 'Génération du PV complet en cours'))
              )
              , array('class'=>'dropdown-menu','role'=>'menu')); */
        }

        if (in_array('telecharger', $projet['Actions'], true)) {
            $actions.=$this->Bs->btn($this->Bs->icon('download'), ['controller' => 'actes', 'action' => 'download', $projet['Deliberation']['id']], ['type' => 'default',
                'escape' => false,
                'class'=>'btn-action-m',
                'title' => __('Visionner le document PDF du projet ')
            ]);
            $actions.= $this->Bs->close(1);
        }

        $actions.= $this->Bs->close(2);
        echo $this->Bs->cell($actions, $cell_class);
