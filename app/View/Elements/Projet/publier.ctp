<?php

    echo $this->Bs->div('spacer');
    $this->BsForm->setLeft(0);
    $this->BsForm->setRight(12);
    echo $this->Bs->row() .
        $this->Bs->col('xs3') .
        $this->BsForm->input('traitement_lot.action', [
            'type'=>'select',
            'options' =>  $actions_possibles,
            'autocomplete' => 'off',
            'empty' => true,
            'class' => 'selectone app-traitementParLot',
            'data-placeholder' => __('Sélectionner une action'),
            'data-allow-clear' => true,
            'label' => 'Actions disponibles : ',
        ])
        . $this->Bs->close()
        . $this->Bs->col('xs1', ['class'=>'wd-traitement-lot-arrow', 'style' => 'width:2em'])
        . $this->Bs->icon('arrow-right', null, ['style' => 'margin-top: 2.6em'])
        . $this->Bs->close();

if (isset($actions_possibles['publier'])) {
        $this->BsForm->setLeft(0);
        $this->BsForm->setRight(12);
        echo $this->Bs->col('xs4', [
                'class'=>'wd-traitement-lot',
                'data-wd-action'=>'publier',
                'style' => 'display: none']) .
            $this->BsForm->dateTimepicker(
                'publier.date_acte_all',
                [
                    'language' => 'fr',
                    'autoclose' => 'true',
                    'minView' => '2',
                    'fontAwesome' => true, //calendar
                    'orientation' => 'bottom-right',
                    'pickerPosition' => 'bottom-right'],
                [
                    'label' => __('Date de publication :&nbsp;&nbsp;'),
                    'after' => '</div>',
                    'help' => __('Date appliquée à tous les actes sélectionnés pour publication'),
                    'title' => __('Choisissez la date de publication'),
                    'style' => 'cursor:pointer;width:8em;border-top-right-radius:0px;border-bottom-right-radius:0px;background-color: white;',
                    'readonly' => 'readonly',
                    'autocomplete' => 'off',
                    'button' => $this->Bs->btn($this->Bs->icon('globe') . ' ' . __('Publier'), null, [
                        'id' => 'publier',
                        'data-wd-action-url' =>  '/publier/publish',
                        'tag' => 'button',
                        'style' => 'border-top-left-radius:0px;border-bottom-left-radius:0px',
                        'name' => 'data[publier][is_checked]',
                        'value' => true,
                        'type' => 'success',
                        'escapeTitle' => false,
                        'title' => __('Définir la date de publication')])]
            );
        echo $this->Bs->close();

}

if (isset($actions_possibles['publier_manuellement'])) {
        $this->BsForm->setLeft(0);
        $this->BsForm->setRight(12);
        echo $this->Bs->col('xs4', [
                'class'=>'wd-traitement-lot',
                'data-wd-action'=>'publier_manuellement',
                'style' => 'display: none']) .
            $this->BsForm->dateTimepicker(
                'publier_manuellement.date_acte_all',
                [
                    'language' => 'fr',
                    'autoclose' => 'true',
                    'minView' => '2',
                    'fontAwesome' => true, //calendar
                    'orientation' => 'bottom-right',
                    'pickerPosition' => 'bottom-right'],
                [
                    'label' => __('Date de publication :&nbsp;&nbsp;'),
                    'after' => '</div>',
                    'help' => __('Date appliquée à tous les actes sélectionnés pour publication'),
                    'title' => __('Choisissez la date de publication'),
                    'style' => 'cursor:pointer;width:8em;border-top-right-radius:0px;border-bottom-right-radius:0px;background-color: white;',
                    'readonly' => 'readonly',
                    'autocomplete' => 'off',
                    'button' => $this->Bs->btn($this->Bs->icon('globe') . ' ' . __('Déclarer une publication manuelle'), null, [
                        'id' => 'publier_manuellement',
                        'data-wd-action-url' =>  '/publier/publish',
                        'tag' => 'button',
                        'style' => 'border-top-left-radius:0px;border-bottom-left-radius:0px',
                        'name' => 'data[publier_manuellement][is_checked]',
                        'value' => true,
                        'type' => 'success',
                        'escapeTitle' => false,
                        'title' => __('Définir la date de publication')])]
            );
        echo $this->Bs->div(
            'alert alert-danger',
            __('Attention: vous allez déclarer que vous allez publier l\'acte manuellement. Ceci est irrévocable !'),
            ['role'=>'alert', 'style'=>'margin-top:2em']
        );
        echo $this->Bs->close();
}

    echo $this->Bs->close(); ?>
    <script type="text/javascript">
        require(['domReady'], function (domReady) {
            domReady(function () {

                //Lors d'action sur une checkbox :
                $('input[type=checkbox]').on("change", function () {
                    $('#traitement_lotAction').val('').trigger('change');
                    selectionChange($(this));
                });

                $('#traitement_lotAction').on("change", function () {
                    selectionChange($(this));
                });

                $('#traitement_lotAction').trigger('change');

                function selectionChange(select) {
                    nbChecked = $('td input[type=checkbox]:checked').length;

                    //Apposer ou non la class disabled au bouton selon si des checkbox sont cochées (style)
                    if (nbChecked > 0 && $('#DeliberationAction').val() != '') {
                        $('#generer_multi_delib').removeClass('disabled');
                        $("#generer_multi_delib").prop("disabled", false);
                    } else {
                        $('#generer_multi_delib').addClass('disabled');
                        $("#generer_multi_delib").prop("disabled", true);
                    }

                    if ($('#traitement_lotAction').val() == 'signatureManuscrite') {
                        select.closest("form").attr('data-waiter-title', '<?php echo __('Signature en cours'); ?>');
                    }

                    $.each( $('div.wd-traitement-lot'), function () {
                        if ($(this).attr('data-wd-action') == $('#traitement_lotAction').val() ) {
                            if (nbChecked > 0 && _.isUndefined($(this).find(':button').attr('data-lock'))) {
                                $(this).find(':button').removeClass('disabled');
                                $(this).find(':button').prop('disabled',false);
                            } else {
                                $(this).find(':button').addClass('disabled');
                                $(this).find(':button').prop('disabled',true);
                            }
                            $(this).attr('style', 'padding-top: 2em');
                            $(this).show('slow');
                            //On modifie la propriété action du formulaire pour réaliser la bonne action
                            // Si action est différetne de signature => Formulaire principal + action commune
                            if (this.id.search('signature') == -1 ) {
                                $(this).closest("form").prop('action', $(this).find(':button').attr('data-wd-action-url'));
                            }
                        } else {
                            $(this).hide();
                        }
                    });

                    $('#nbDeliberationsChecked').text('(' + nbChecked + ')');
                }
            });
        });
    </script>
