<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

// echo $this->Bs->btn(
//         $this->Bs->icon('university'),
//         [
//       'controller' => 'deliberations',
//       'action' => 'editTeletransmission', $projet['Deliberation']['id']],
//         [
//         'type' => 'primary',
//         'title' => __('Modifier les informations de télétransmission'),
//         'escapeTitle' => false]
//       );
if (!empty($acte['Actions']) && in_array('sendToTdt', $acte['Actions'], true)) {
    echo $this->Bs->modal(
        __('Modifier les informations de télétransmission du projet n°%s', $acte['Deliberation']['id']),
        '',
        [
            'form' => false,
            'id' => 'modify-tdt-'.$acte['Deliberation']['id'],
            'size' => 'lg'
          ],
        [
              'close' => [
                'name' => $this->Bs->icon('times-circle') . ' Annuler',
                'class' => 'btn-default',
                'type'=>'default',
              ],
              'open' => [
                  'name' => $this->Bs->icon('university'), //. '</span>'
                  'class' => ($acte['isTypologiepieceEmpty'] ? 'btn-warning' : 'btn-primary').' class-wd-tdt-typologieForm',
                  //'type'=> 'primary',

                  'options'=>[
                      'tag'=>'button',
                      'option_type'=>'button',
                      'title' => __('Modifier les informations de télétransmission'),
                  ]
              ],
              'confirm' => [
              'name' => $this->Bs->icon('save') .' ' .  __('Enregistrer'),
              'class' => 'btn-success',
              'options' => [
                'id' => 'modifyBtn-tdt-'.$acte['Deliberation']['id'],
                'escapeTitle'=> false
              ],
              'link' => '#'
            ]
          ]
    );
}
