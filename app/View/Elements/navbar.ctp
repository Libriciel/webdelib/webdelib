<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Navbar->create([
    'inverse' => true,
    'fixed' => 'top',
    'responsive' => true,
    'fluid' => true]);

$this->Navbar->brand(__('webdelib'), [
    'admin' => false,
    'prefix' => null,
    'plugin' => null,
    'controller' => 'dossiers',
    'action' => 'index']);

$this->Navbar->link($this->Bs->icon('home', ['lg']), [
    'admin' => false,
    'prefix' => null,
    'plugin' => null,
    'controller' => 'dossiers',
    'action' => 'index'
        ], ['escapeTitle' => false]);

$this->Navbar->link($infoCollectivite['nom'], [
    'admin' => false,
    'prefix' => null,
    'plugin' => null,
    'controller' => 'dossiers',
    'action' => 'index'
        ], ['escapeTitle' => false]);

$this->Menu->createMenuPrincipale($this->Navbar);

if ($this->fetch('filtre')) {
    $this->Navbar->block(
        $this->Bs->btn(__('Filtrer'), 'javascript:void(0);', [
                'id' => 'appFilter_slideUpAndDown',
                'class' => 'navbar-btn',
                'type' => 'primary',
                'title' => __('Afficher-masquer les critères du filtre'),
                    //'escape'=> false,filtreCriteres
            /* 'icon'=>'fa fa-filter"' */            ]),
        ['list' => false]
    );
} else {
    $this->Navbar->block(
        $this->Bs->btn(__('Filtrer'), 'javascript:void(0);', [
                'id' => 'appFilter_slideUpAndDown',
                'class' => 'navbar-btn',
                'type' => 'primary',
                'title' => __('Afficher-masquer les critères du filtre'),
                'disabled' => "disabled"], null, ['escape' => false]),
        ['list' => false]
    );
}
//padding: 3px 20px; 'aria-label' => 'menu utilisateur'
$this->Navbar->beginMenu($this->Bs->icon('bars', ['lg']), null, [
    'pull' => 'right'
]);
if ($this->permissions->check('Preferences', 'read')) {
    $this->Navbar->link(
        $this->Bs->icon('user') . ' ' . $infoUser,
        ['admin' => false, 'prefix' => null, 'plugin' => null, 'controller' => 'preferences', 'action' => 'index'],
        ['escapeTitle' => false, 'title' => __('Préférences utilisateur')]
    );
} else {
    $this->Navbar->link(
        $this->Bs->icon('user') . ' ' . $infoUser,'#', ['escapeTitle' => false, 'class' => 'navbar-li-text']
    );
}

$this->Navbar->link(
    $this->Bs->icon('sitemap') . ' ' . $infoServiceEmeteur,
    [
        'admin' => false,
        'prefix' => null,
        'plugin' => null,
        'controller' => 'preferences',
        'action' => 'changeServiceEmetteur'
    ],
    [
        'escapeTitle' => false,
        'title' => __('Changer le service émetteur')
    ]
);

$this->Navbar->link(
    $this->Bs->icon('bell') . ' ' . __('Notifications'),
    [
        'admin' => false,
        'prefix' => null,
        'plugin' => null,
        'controller' => 'notifications',
        'action' => 'index'
    ],
    [
        'escapeTitle' => false,
        'title' => __('Notifications utilisateurs')
    ]
);
if ($this->permissions->check('Preferences/changeFormatSortie', 'read')) {
    $this->Navbar->divider();
    $this->Navbar->link(
        $this->Bs->icon('cog')
        .' '.  __('Génération en ODT') .
        $this->Bs->div('', $this->Bs->icon($infoFormatSortie === 'odt' ? 'toggle-on' : 'toggle-off'), ['style'=>'float: right']),
        [
            'admin' => false,
            'prefix' => null,
            'plugin' => null,
            'controller' => 'preferences',
            'action' => 'changeFormatSortie', ($infoFormatSortie === 'odt' ? 'pdf' : 'odt')
        ],
        [
          'escapeTitle' => false,
          'title' => ($infoFormatSortie ? __('Désactiver les générations au format ODT') : __('Activer les générations au format ODT'))]
    );
}
$this->Navbar->divider();
$this->Navbar->link(
    $this->Bs->icon('question') . ' ' . __('Aide'),
    ['admin' => false, 'plugin' => null, 'controller' => 'pages', 'action' => 'help'],
    ['escape' => false, 'title' => __('Documents d\'aide')]
);
$this->Navbar->link(
    $this->Bs->icon('user-secret') . ' ' . __('Politique de confidentialité'),
    ['admin' => false, 'plugin' => null, 'controller' => 'collectivites', 'action' => 'rgpd'],
    ['escape' => false, 'title' => __('Règlement Général de Protection des Données')]
);
$this->Navbar->divider();
$this->Navbar->link(
    $this->Bs->icon('sign-out-alt') . ' ' . __('Se déconnecter'),
    ['admin' => false, 'prefix'=>null, 'plugin' => null, 'controller' => 'users', 'action' => 'logout'],
    ['escapeTitle' => false, 'wrap' => 'span', 'class' => 'text-nowrap']
);
$this->Navbar->endMenu();

$block = $this->Form->create('User', [
    'id' => 'quickSearch',
    'role' => 'search',
    'class' => 'navbar-form navbar-right',
    'url' => [
        'admin' => false,
        'prefix' => null,
        'plugin' => null,
        'controller' => 'search',
        'action' => 'quickSearch'
    ]
]
);

$block.= $this->BsForm->hidden('type', ['value' => 'quick']);
$this->BsForm->setMax();
$block .= '<div class="form-group">';
$block .= $this->BsForm->inputGroup(
    'User.search',
    [
        [
            'content' => $this->Bs->icon('search'),
            'id' => 'search_button',
            'title' => __('Recherche globale'),
            'type' => 'submit',
            'state' => 'primary',
        ]
    ],
    [
    'placeholder' => __('Rechercher'),
    'class' => 'form-control span2',
    'autocomplete' => 'off'
        ],
    ['multiple' => true, 'side' => 'right'
        ]
);
$block.= $this->Bs->btn($this->Bs->icon('search-plus'), [
    'admin' => false,
    'prefix' => null,
    'plugin' => null,
    'controller' => 'search', 'action' => 'index', 'all'], [
    'type' => 'default',
    'title' => __('Recherche multi-critères'),
    'escapeTitle' => false,
        ]);
$block.= '</div>';

$this->BsForm->setDefault();

$block.= $this->Form->end();

$this->Navbar->block($block, ['list' => false]);

echo $this->Navbar->end(true);
