<?php

/**
 * Affichage du filtre
 *
 */
if ($this->Session->check('Filtre.Criteres')) {
    $this->append('filtre'); 
    $filtre = $this->BsForm->create(null, ['url' => $this->Session->read('Filtre.url'), 'id' => 'filterForm', 'novalidate' => true]);
//    $note1 = false;
    $newLine = true;
    $line = [1 => '6', 2 => '6', 3 => '4'];
    //debug($criteres);
    foreach ($this->Session->read('Filtre.Criteres') as $nom => $options) {
        $filtre .= ($newLine == true ? $this->Bs->row() : '') . $this->Bs->col('xs' . (!empty($options['column']) ? $line[$options['column']] : $line[2]));
        if (array_key_exists('type', $options['inputOptions'])) {
            $options['inputOptions']['autocomplete'] = 'off';
            switch ($options['inputOptions']['type']) {
                case 'text':
//                    $options['inputOptions']['onKeyUp'] = "critereChange(this);";
//                    $options['inputOptions']['onPaste'] = "critereChange(this);";
//                    $options['inputOptions']['label'] .= ' *'; // note
//                    $note1 = true;
                    $filtre .= $this->BsForm->input('Critere.' . $nom, $options['inputOptions']);
                    break;

                case 'date':
                    $filtre .= $this->BsForm->datetimepicker('Critere.' . $nom, $options['Datepicker'], $options['inputOptions']);
                    break;

                case 'checkbox':
                    $filtre .= $this->BsForm->checkbox('Critere.' . $nom, $options['inputOptions']);
                    break;

                case 'select':
                    if (!array_key_exists('multiple', $options['inputOptions']) || !$options['inputOptions']['multiple']) {
                        if(isset($options['inputOptions']['selected']))
                        {
                            $options['inputOptions']['value'] = $options['inputOptions']['selected'];
                        }
                        if (!isset($options['inputOptions']['class'])) {
                            $options['inputOptions']['class'] = 'selectone';
                        } else {
                            $options['inputOptions']['class'] .= ' selectone';
                        }
                    }else{
                        if (!isset($options['inputOptions']['class'])) {
                            $options['inputOptions']['class'] = 'selectmultiple';
                        } else {
                            $options['inputOptions']['class'] .= ' selectmultiple';
                        }
                    }
                    $filtre .= $this->BsForm->input('Critere.' . $nom, $options['inputOptions']);
                    break;

                default:
                    $filtre .= $this->BsForm->input('Critere.' . $nom, $options['inputOptions']);
            }
        } else {
            $filtre .= $this->BsForm->input('Critere.' . $nom, $options['inputOptions']);
        }
        $newLine++;
        if ($options['retourLigne']) {
            $filtre .= $this->Bs->close(2);
            $newLine = true;
        } else {
            $filtre .= $this->Bs->close();
            $newLine = false;
        }
    }

    /* if ($note1){
      $filtre .= $this->Html->tag('em',__("* Vous pouvez indiquer seulement les premières lètres du terme recherchés, par exemple \"dup\" pour trouver \"Dupon\""));
      } */

    $filtre .= $this->Bs->div('btn-group col-md-offset-' . $this->BsForm->getLeft(), null) .
            $this->Bs->btn($this->Bs->icon('undo') . ' ' . __('Réinitialiser'), 'javascript:void(0);', ['id' => 'appFilter_destroy', 'type' => 'danger', 'escapeTitle' => false, 'disabled' => ($this->Session->read('Filtre.Fonctionnement.actif') ? false : 'disabled'), 'title' => __('Filtre actif, cliquer ici pour annuler le filtre')]) . $this->Bs->btn($this->Bs->icon('filter') . ' ' . __('Appliquer le filtre'), null, ['tag' => 'button', 'type' => 'success', 'id' => 'boutonValider', 'escapeTitle' => false, 'title' => __('Appliquer le filtre')]) .
            $this->Bs->close();

    $filtre .= $this->Form->hidden('filtreFonc.affiche', ['value' => true]);
    $filtre .= $this->BsForm->end() . $this->Bs->tag('br /');

    /* echo $this->Bs->div('panel panel-default', null,  array('id'=>'filtreCriteres','style'=>$this->Session->read('Filtre.Fonctionnement.affiche')?'':'display: none')).
      $this->Bs->div('panel-body', $filtre).$this->Bs->close(2); */

    echo $this->Bs->div('well', $filtre, ['id' => 'appFilter', 'style' => $this->Session->read('Filtre.Fonctionnement.affiche') ? '' : 'display: none']) . $this->Bs->close();

    $this->end();
}
