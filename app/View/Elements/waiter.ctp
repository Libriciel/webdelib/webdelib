<div id="waiter" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="waiterLabel"><?php echo __('Action en cours de traitement'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="waiter-progress" style="display: none">
                    <div class="progress">
                      <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="spacer"></div>
                    <p  class="waiter-comment">Veuillez patienter...</p>
                </div>

                <div class="waiter-default" style="display: none">
                    <?php echo $this->Html->image('loader-circle.gif', ['alt' => __('Loading'), 'id' => 'waiter-image']); ?>
                    <div class="spacer"></div>
                    <p>Veuillez patienter...</p>
                </div>
              </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
