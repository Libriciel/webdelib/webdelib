<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

?>
<meta http-equiv="X-Frame-Options" content="SAMEORIGIN">

<div id="modalOffice" class="modal fade wd-modal-edit-text-wopi" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header"></div>
            <div class="modal-body">
                <div style="display: inline-block;">
<!--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                    <form action="" encType="multipart/form-data" method="post" target="_blank" id="collabora-submit-form">
                        <input name="access_token" value="test" type="hidden"/>
                    </form>
<!--                    <iframe src="" id="collabora-online-viewer"  height="500"  width="500" name="collabora-online-viewer" sandbox="allow-scripts">-->
<!--                    </iframe>-->
                </div>
            </div>
            <div class="modal-footer"></div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
