<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$delib = $this->data;

if (!empty($delib['Deliberation']['id'])) {
    $id = $delib['Deliberation']['id'];
}

$type_actif=''; //FIX
switch ($type) {
    case 'texte_projet':
        $libelle = __('Texte projet');
        $type_actif = 'texte_projet'; //FIX
        break;

    case 'texte_synthese':
        $libelle = __('Note synthèse');
        $type_actif = 'texte_synthese';//FIX
        break;

    case 'deliberation':
        $libelle = __('Texte acte');
        $type_actif = 'texte_acte';//FIX
        break;
    default:
        break;
}

if (empty($delib['Deliberation'][$type_actif . "_active"])) {
    return;
}

$filename = '';
if (!empty($names[$type . "_name"])) {
    $delib['Deliberation'][$type . "_name"] = $names[$type . "_name"];
    $filename = $names[$type . "_name"];
} elseif (!empty($delib['Deliberation']['file_url_' . $type])) {
    $filename = $delib['Deliberation']['file_url_' . $type];
    if (Configure::read('FeaturesFlags.wopi')) {
        $wopiFileSrc = $delib['Deliberation']['file_wopi_' . $type];
    }
}

if (empty($delib['Deliberation'][$type . "_name"]) || isset($validationErrorsArray[$type . '_type'])) {
    $display_choisir_fichier = 'block';
    $display_editer_fichier = 'none';
    $display_choisir_fichier_input = [];
} else {
    $display_choisir_fichier = 'none';
    $display_editer_fichier = 'block';
    $display_choisir_fichier_input = ['disabled' => 'disabled'];
}

//AFFICHAGE DU SELECTEUR DE FICHIER
$this->BsForm->setLeft(3);
$this->BsForm->setRight(7);
echo $this->Bs->div(
    'choisir_fichier_' . $type,
    $this->BsForm->input("Deliberation.{$type}_upload", [
            'label' => __("$libelle"),
            'title' => __("$libelle"),
            'type' => 'file',
            'data-btnClass' => 'btn-primary',
            'data-icon' => $this->Bs->icon('file-alt'),
            'data-text' => ' ' . __('Choisir un fichier'),
            'data-placeholder' => __('Pas de fichier'),
            'data-badge' => 'true',
            'accept' => 'application/vnd.oasis.opendocument.text',
            $display_choisir_fichier_input,
            'class' => 'filestyle fileuploader',
            'after' => $this->Bs->close() . $this->Bs->div('col-md-2', null). $this->Bs->btn($this->Bs->icon('eraser') . ' ' . __('Effacer'), 'javascript:void(0);', [
                'type' => 'danger',
                'class' => 'wd-filestyle-clear',
                'title' => __('Cliquez ici pour ne pas utiliser le texte sélectionné'),
                'data-wd-filestyle-id' => 'Deliberation'.Inflector::camelize($type).'Upload',
                'escapeTitle' => false
              ], ['role'=>'group']) . $this->Bs->close()
        ]),
    ['style' => 'display:' . $display_choisir_fichier]
). $this->Bs->close();
$this->BsForm->setDefault();

//AFFICHAGE DE L'EDITEUR DE FICHIER
echo $this->Bs->div('editer_fichier_' . $type, null, ['style' => 'display:' . $display_editer_fichier]);
    echo $this->Bs->row() .
        $this->Bs->col('xs3') .
        $this->Bs->tag('label', $libelle, ['class' => 'pull-right']) .
        $this->Bs->close() .
        $this->Bs->col('xs9') .
        $this->Bs->div('media');

        if (Configure::read('FeaturesFlags.wopi')) {
            echo $this->element('editTextWopi',
                [
                    'delib' => $delib,
                    'type' => $type,
                    'wopiClientUrl' => $wopiClientUrl,
                    'wopiFileSrc' => $wopiFileSrc,
                ]
            );
        } else {
            echo $this->element('editTextWebdav', [
                'filename'=>$filename,
                'delib' => $delib,
                'type' => $type,
            ]
            );
        }
        echo $this->Bs->btn(
            $this->Bs->icon('trash') . ' ' . __('Supprimer'),
            'javascript:void(0);',
            [
            'type' => 'danger',
            'escapeTitle' => false,
            'size' => 'xs',
            'class' => 'media-left wd-projet-texte',
            'data-wd-projet-texte-type'=> $type,
            'id' => 'delete_gabarit',
                ]
        ) .
        $this->Bs->close(6);
        if(!empty($delib['Deliberation']['file_' . $type])){
            echo $this->BsForm->hidden('Deliberation.file_' . $type, ['value' => $delib['Deliberation']['file_' . $type]]);
        }
        echo $this->Bs->tag('br');

//AFFICHAGE DES ERREURS DE VALIDATION
if (!empty($validationErrorsArray[$type . '_type'])) {
    echo("<div class='error-message'>" . $validationErrorsArray[$type . '_type'][0] . '</div>');
}
