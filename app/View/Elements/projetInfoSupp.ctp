<?php

echo '<dd><br />';
foreach ($infosupdefs as $infosupdef) {
    echo '<b>'.$infosupdef['Infosupdef']['nom'] . ' : </b>';
    if (!empty($infosupdef['Infosupdef']['code']) &&
            !empty($infosups) &&
            array_key_exists($infosupdef['Infosupdef']['code'], $infosups)) {

        //debug($infosupdef['Infosupdef']['nom'] . ' : ' . $infosupdef['Infosupdef']['type']);
        switch ($infosupdef['Infosupdef']['type']) {

            case 'richText':        // Rich Text
                echo $this->Html->link('[Afficher le texte]', 'javascript:void(0);', ['class' => 'showTexteEnrichi', 'data-infosup-hide'=> 'off', 'data-infosup-hide-on-texte'=> __("[Masquer le texte]"), 'data-infosup-hide-off-texte'=> __("[Afficher le texte]"), 'data-infosup-id'=> 'Fck_'.$infosupdef['Infosupdef']['id']]);
                echo '<div class="spacer"></div>';
                echo $this->Form->input($infosupdef['Infosupdef']['code'], ['label' => '', 'type' => 'textarea', 'readonly'=> true, 'style' => 'display: none', 'value' => $infosups[$infosupdef['Infosupdef']['code']], 'class' => 'Fck_'.$infosupdef['Infosupdef']['id'], 'id' => 'Fck_'.$infosupdef['Infosupdef']['id']]);
                echo '<div class="spacer"></div>';
                break;

            case 'listmulti':
                // liste multi
                echo implode(', ', $infosups[$infosupdef['Infosupdef']['code']]);
                break;
            case 'boolean':       // BOOLEAN
                echo ($infosups[$infosupdef['Infosupdef']['code']] == 1) ? '<i class="fa fa-check"></i> ' : '' ;
                break;
            case 'file':
            case 'odtFile':
                if (!empty($infosups[$infosupdef['Infosupdef']['code']]['name'])) {
                    $download = [
                        'controller' => 'Infosups',
                        'action' => 'download',
                        $infosups[$infosupdef['Infosupdef']['code']]['id']];
                    echo $this->Bs->div(null,null,['style'=>'display: inline-block;']);
                    echo $infosups[$infosupdef['Infosupdef']['code']]['name'];
                    echo $this->Bs->div(null,null,['style'=>'display: inline-block;margin']);
                    echo $this->Bs->link($this->Bs->icon('download'), $download, [
                        'escapeTitle' => false,
                    ]);
                    echo $this->Bs->close(2);
                }
                break;
            case 'geojson':
                $correlations = json_decode($infosupdef['Infosupdef']['correlation'], true);
                if(empty($infosups[$infosupdef['Infosupdef']['code']]['geojson']) === false) {
                    $geojson = json_decode($infosups[$infosupdef['Infosupdef']['code']]['geojson'], true);
                    $flippedCorrelations = array_flip($correlations);
                    $values = array_intersect_key($geojson['properties'], $flippedCorrelations);

                    echo sprintf('%s ', h($values[$correlations['label']]));
                    echo $this->Html->link('[Afficher les détails]', 'javascript:void(0);', ['class' => 'showTexteEnrichi', 'data-infosup-hide'=> 'off', 'data-infosup-hide-on-texte'=> __("[Masquer les détails]"), 'data-infosup-hide-off-texte'=> __("[Afficher les détails]"), 'data-infosup-id'=> 'Geojson_'.$infosupdef['Infosupdef']['code']]);
                    echo '<ul style="display: none;" id="Geojson_'.$infosupdef['Infosupdef']['code'].'">';
                    foreach($values as $key => $value) {
                        echo '<li><strong>'.$flippedCorrelations[$key].':</strong> '.h($value).'</li>';
                    }
                    echo '</ul>';
                }
                break;
            default:
                if (isset($infosups[$infosupdef['Infosupdef']['code']])){
                  echo $infosups[$infosupdef['Infosupdef']['code']];
                }
        }
    }
    echo '<br />';
}
echo '</dd>';
