<?php

echo $this->Bs->div(null, null, ['id' => 'footer']);
echo $this->Bs->div('container');
echo $this->Bs->tag('p', 'webdelib v'
        . $this->Bs->tag('span', VERSION, ['style' => 'font-weight: bold']) 
        . __(' &copy; 2006-%s  %s.', date('Y'), $this->Bs->link('Libriciel SCOP', 'https://www.libriciel.fr', ['target'=>'_blank']))
        , ['class' => 'muted']);
echo $this->Bs->close(2);
