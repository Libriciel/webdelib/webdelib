<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

echo $this->Bs->div('well');
echo $this->Bs->row();
echo $this->Bs->col('xs6');
echo $this->Html->tag('label', __('Service émetteur :')) . ' ' . $projet['Service']['name'] . $this->Bs->tag('br');

// un seul rédacteur
if (empty($projet['User'])) {
    echo $this->Html->tag('label', __('Rédacteur :')) . ' ' . $this->Html->link($projet['Redacteur']['prenom'] . ' ' . $projet['Redacteur']['nom'], ['controller' => 'users', 'action' => 'view', $projet['Redacteur']['id']]);
} else { // plusieurs rédacteurs
    $redacteurs = '';
    echo $this->Html->tag('label', __('Rédacteurs :')) . ' ' . $this->Html->link($projet['Redacteur']['prenom'] . ' ' . $projet['Redacteur']['nom'], ['controller' => 'users', 'action' => 'view', $projet['Redacteur']['id']]);
    foreach ($projet['User'] as $user) {
        $redacteurs .= ', ' . $this->Html->link($user['prenom'] . ' ' . $user['nom'], ['controller' => 'users', 'action' => 'view', $user['id']]);
    }
    echo $redacteurs;
}
echo $this->Bs->close();

echo $this->Bs->col('xs6');
echo $this->Html->tag('label', __('Date de création :')) . ' ' . $this->Time->i18nFormat($projet['Deliberation']['created'], '%d/%m/%Y à %kh%M') . $this->Bs->tag('br /');
echo $this->Html->tag('label', __('Date de modification :')) . ' ' . $this->Time->i18nFormat($projet['Deliberation']['modified'], '%d/%m/%Y à %kh%M');
echo $this->Bs->tag('br /');
echo $this->Bs->close();

echo $this->Bs->close();

echo $this->Bs->close();
$this->BsForm->setLeft(2);
$this->BsForm->setRight(10);
echo $this->Bs->row();
echo $this->Bs->col('xs'.$this->BsForm->getLeft(), null, ['class'=>'text-right']) . $this->Html->tag('label', __('État :')) . $this->Bs->close().
$this->Bs->col('xs'.$this->BsForm->getRight()) . $projet['Deliberation']['iconeEtat']['titre'] . $this->Bs->close(2);

echo $this->Bs->row();
echo $this->Bs->col('xs'.$this->BsForm->getLeft(), null, ['class'=>'text-right']) . $this->Html->tag('label', __('Titre :')) . $this->Bs->close().
$this->Bs->col('xs'.$this->BsForm->getRight()) . $projet['Deliberation']['titre'] . $this->Bs->close(2);

echo $this->Bs->row();
echo $this->Bs->col('xs'.$this->BsForm->getLeft(), null, ['class'=>'text-right']) . $this->Html->tag('label', __('Thème :')) . $this->Bs->close().
$this->Bs->col('xs'.$this->BsForm->getRight()) . $projet['Theme']['libelle'] . $this->Bs->close(2);

echo $this->Bs->row();
echo $this->Bs->col('xs'.$this->BsForm->getLeft(), null, ['class'=>'text-right']) . $this->Html->tag('label', __('Rapporteur :')) . $this->Bs->close().
$this->Bs->col('xs'.$this->BsForm->getRight()) . $projet['Rapporteur']['prenom'] . ' ' . $projet['Rapporteur']['nom'] . $this->Bs->close(2);

echo $this->Bs->row();
echo $this->Bs->col('xs'.$this->BsForm->getLeft(), null, ['class'=>'text-right']) . $this->Html->tag('label', __('Classification :')) . $this->Bs->close().
$this->Bs->col('xs'.$this->BsForm->getRight()) .  $projet['Deliberation']['num_pref'] . $this->Bs->close(2);

echo $this->Bs->row();
echo $this->Bs->col('xs'.$this->BsForm->getLeft(), null, ['class'=>'text-right']) . $this->Html->tag('label', __('Type de pièce principale: ')) . $this->Bs->close().
$this->Bs->col('xs'.$this->BsForm->getRight()) .  (!empty($projet['Deliberation']['typologiepiece_code_libelle']) ? $projet['Deliberation']['typologiepiece_code_libelle'] : '')  . $this->Bs->close(2);

echo $this->Bs->row();
echo $this->Bs->col('xs'.$this->BsForm->getLeft(), null, ['class'=>'text-right']) . $this->Html->tag('label', __('Envoi de documents papiers complémentaires: ')) . $this->Bs->close().
$this->Bs->col('xs'.$this->BsForm->getRight()) .  (!empty($projet['Deliberation']['tdt_document_papier']) ? __('Oui'): __('Non'))  . $this->Bs->close(2);

echo $this->Bs->row();
echo $this->Bs->col('xs'.$this->BsForm->getLeft(), null, ['class'=>'text-right']) .
    $this->Html->tag('label', __('Date limite :')) . $this->Bs->close().
    $this->Bs->col('xs'.$this->BsForm->getRight()) .
    $this->Time->i18nFormat($projet['Deliberation']['date_limite'], '%d/%m/%Y') .
    $this->Bs->close(2);

if (!empty($projet['Deliberation']['date_envoi_signature'])) {
    echo $this->Bs->row();
    echo $this->Bs->col('xs' . $this->BsForm->getLeft(), null, ['class' => 'text-right'])
        . $this->Html->tag('label', __('Date de l\'envoi en signature :')) . $this->Bs->close() .
        $this->Bs->col('xs' . $this->BsForm->getRight())
        . $this->Time->i18nFormat($projet['Deliberation']['date_envoi_signature'], '%d/%m/%Y')
        . $this->Bs->close(2);
}
if (!empty($projet['Deliberation']['signature_date'])) {
    echo $this->Bs->row();
    echo $this->Bs->col('xs' . $this->BsForm->getLeft(), null, ['class' => 'text-right'])
        . $this->Html->tag('label', __('Date de la signature :')) . $this->Bs->close() .
        $this->Bs->col('xs' . $this->BsForm->getRight())
        . $this->Time->i18nFormat($projet['Deliberation']['signature_date'], '%d/%m/%Y')
        . $this->Bs->close(2);
}
if (!empty($projet['Deliberation']['date_acte'])) {
    echo $this->Bs->row();
    echo $this->Bs->col('xs' . $this->BsForm->getLeft(), null, ['class' => 'text-right'])
        . $this->Html->tag('label', __('Date de l\'acte :')) . $this->Bs->close() .
        $this->Bs->col('xs' . $this->BsForm->getRight())
        . $this->Time->i18nFormat($projet['Deliberation']['date_acte'], '%d/%m/%Y')
        . $this->Bs->close(2);
}

if (!empty($projet['listeSeances'])) {
    echo $this->Bs->row();
    echo $this->Bs->col('xs'.$this->BsForm->getLeft(), null, ['class'=>'text-right']) . $this->Html->tag('label', __('Date Séance :')) . ' ';
    echo $this->Bs->close();
    echo $this->Bs->col('xs'.$this->BsForm->getRight());
    foreach ($projet['listeSeances'] as $seance) {
        echo $this->Bs->icon('tag', [], (!empty($seance['Typeseance']['color']) ? ['style' => 'color: ' . $seance['Typeseance']['color']] : [])) .
        ' ' . $seance['libelle'] . (isset($seance['date']) && !empty($seance['date']) ? ' : ' . $this->Time->i18nFormat($seance['date'], '%d/%m/%Y') . ' à ' . $this->Time->i18nFormat($seance['date'], '%kh%M') : '') . $this->Bs->tag('br');
    }
    echo $this->Bs->close(2);
}

// pour visualiser le Texte
if (!empty($projet['Deliberation']['texte_projet_name'])) {
    echo $this->element('viewTexte', [
        'type' => 'projet',
        'delib' => $projet['Deliberation'],
        'revision'=> isset($revision) ? $revision: false,
    ]);
}

if (!empty($projet['Deliberation']['texte_synthese_name'])) {
    echo $this->element('viewTexte', ['type' => 'synthese', 'delib' => $projet['Deliberation'],'revision'=> isset($revision) ? $revision: false,]);
}

if (empty($projet['Multidelib'])) {
    if (!empty($projet['Deliberation']['deliberation_name'])) {
        echo $this->element('viewTexte', ['type' => 'deliberation', 'delib' => $projet['Deliberation']]);
    }
}

if (!empty($projet['Deliberation']['parapheur_bordereau'])) {
    echo $this->element('viewTexte', ['type' => 'parapheur_bordereau', 'delib' => $projet['Deliberation']]);
}

if (!empty($projet['Deliberation']['tdt_data_pdf'])) {
    echo $this->element('viewTexte', ['type' => 'tdt_tampon', 'delib' => $projet['Deliberation']]);
}

// Version antérieure
if (!empty($tab_anterieure)) {
    echo $this->Bs->tag('br /');
    $sLis = '';
    $i = 0;
    foreach ($tab_anterieure as $key => $anterieure) {
        $sLis.=$this->Bs->tag('li', $this->Bs->tag('span', count($tab_anterieure) - $i, ['class' => 'badge'])
                . $this->Html->link(__('Version antérieure du ') . $anterieure['date_version'] . ' (Identifiant: ' . $anterieure['id'] . ')', $anterieure['lien']), ['class' => 'list-group-item list-group-item-danger']);
        $i++;
    }

    echo $this->Bs->row();
    echo $this->Bs->col('xs'.$this->BsForm->getLeft(), null, ['class'=>'text-right']) . $this->Html->tag('label', __('Version(s) :')) . $this->Bs->close().
    $this->Bs->col('xs'.$this->BsForm->getRight()) .  $this->Bs->tag('ul', $sLis, ['class' => 'list-group']) . $this->Bs->close(2);
}
