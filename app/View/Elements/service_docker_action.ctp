<?php
if(empty($data['state']) AND empty($data['status']) AND empty($data['log'])) {
    return;
}
echo $this->Bs->div('form-group');
echo $this->Bs->tag('label',  __('Informations'),  ['class'=>'control-label col-md-3']);
echo $this->Bs->div('col-md-9');
echo $this->Bs->div('well');
echo
    __('Etat du service : ') . $data['state']. $this->Bs->tag('br /') .
    __('Statut du service : ') . $data['status'] . $this->Bs->tag('br /');
if($service ==='flow') {
    echo
        __('Dernier fichier de donnée : ') . $this->Bs->link('Donnees.txt', [
            'controller' => 'connecteurs', 'action' => 'admin_fusion_download_data']) . $this->Bs->tag('br /') .
        __('Dernier fichier de donnée linéarisé : ') . $this->Bs->link('Serialize.txt', [
            'controller' => 'connecteurs', 'action' => 'admin_fusion_download_serialize']) . $this->Bs->tag('br /') .
        __('Dernier fichier fusionné : ') . $this->Bs->link('Resultat.odt', [
            'controller' => 'connecteurs', 'action' => 'admin_fusion_download_file']). $this->Bs->tag('br /');
}
echo $this->Bs->tag('br /');
echo $this->Bs->div('row');
echo $this->Bs->col('md12 of0');
echo $this->Bs->div('btn-group') .
$this->Bs->btn($this->Bs->icon('sync') . ' ' . 'Redémarrer le service', [
    'controller' => 'connecteurs', 'action' => 'maintenance', $service, 'restart'
], ['type' => 'primary', 'title' => __('Redémarrer le service'), 'escapeTitle' => false]) .
$this->Bs->btn($this->Bs->icon('play') . ' ' . 'Démarrer le service', [
    'controller' => 'connecteurs', 'action' => 'maintenance', $service, 'start'
], ['type' => 'success', 'title' => __('Démarrer le service'), 'escapeTitle' => false]) .
$this->Bs->btn($this->Bs->icon('stop') . ' ' . 'Arrêter le service', [
    'controller' => 'connecteurs', 'action' => 'maintenance', $service, 'stop'
], ['type' => 'danger', 'title' => __('Arrêter le service'), 'escapeTitle' => false]) .
$this->Bs->close();

if(!empty($data['log'])) {
    echo $this->Bs->modal(
        __('Derniers logs du service %s (200 lignes)', $service),
                $this->Bs->div('btn-group', $data['log'], ['style'=>'font-family: courier new;color: green;font-weight: 500;background-color: black;width: 100%'])
                ,
                [   'form' => false,
                    'id' => 'services-logs-' . $service,
                    'size' => 'lg',
                ],
                [
                    'open' => [
                        'name' => $this->Bs->icon('history') .' '. __('Voir les derniers logs'),
                        'class' => 'btn-default',
                        'options' => [
                             'tag'=>'button',
                             'style' => 'margin-left:15px',
                             'option_type'=>'button',
                             'title' => __('Voir les logs derniers du service'),
                         ]
                    ],
                    'close' => [
                        'class' => 'btn-default',
                        'name' => 'Fermer',
                    ],
                    'id' => 'cancel-logs',
                    'confirm' => null
                ]
            );
}
echo $this->Bs->close(5);