<?php

if (!empty($infosup['name']) && !isset($infosup['tmp_name']) && $infosupdef['type'] !== 'odtFile') {
    $display_editer_fichier = 'block';
} else {
    $display_editer_fichier = 'none';
}

$download = [
    'controller' => 'Infosups',
    'action' => 'download',
    $infosup['id']];

echo $this->Bs->div('editer_fichier_' . $infosupdef['code'], null, [
        'style' => 'display:' . $display_editer_fichier]) .
    $this->Bs->row() .
    $this->Bs->col('xs3') .
    $this->Bs->tag('label', $infosupdef['nom'], ['class' => 'pull-right']) .
    $this->Bs->close() .
    $this->Bs->col('xs9') .
    $this->Bs->div('media') .
    $this->Bs->link($this->Bs->icon('file-alt', ['4x']), $download, ['class' => 'media-left', 'escape' => false]) .
    $this->Bs->div('media-body') .
    $this->Bs->tag('h4', $infosup['name'], ['class' => 'media-heading']) .
    $this->Bs->div('btn-group');

    echo $this->Bs->btn($this->Bs->icon('trash') . ' ' . __('Supprimer'), 'javascript:void(0);', [
        'type' => 'danger',
        'escapeTitle' => false,
        'size' => 'xs',
        'data-wd-infosup-infosupdef-code' => $infosupdef['code'],
        'confirm' => __('Voulez-vous vraiment supprimer le fichier joint ?' . "\n\n" . 'Attention : ne prendra effet que lors de la sauvegarde' . "\n"),
        'class' => 'media-left wd-infosup-file'
    ]);

    echo $this->Bs->btn($this->Bs->icon('download') . ' ' . __('Visualiser'), $download, [
        'type' => 'default',
        'escapeTitle' => false,
        'size' => 'xs',
        'class' => 'media-left',
    ]);
echo $this->Bs->close(6);
echo $this->Html->tag('br');