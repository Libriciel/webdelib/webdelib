<?php



$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Acteurs'), ['action' => 'index']);

if ($this->Html->value('Acteur.id')) {
    echo $this->Bs->tag('h3', __('Modifier l\' acteur'));
    $this->Html->addCrumb(__('Modification d\'un acteur'));

    echo $this->BsForm->create('Acteur', ['url' => ['controller' => 'acteurs', 'action' => 'edit', $this->Html->value('Acteur.id')], 'type' => 'post', 'name' => 'ActeurBsForm']);
} else {
    echo $this->Bs->tag('h3', __('Ajouter un acteur'));
    $this->Html->addCrumb(__('Ajout d\'un acteur'));

    echo $this->BsForm->create('Acteur', ['url' => ['controller' => 'acteurs', 'action' => 'add'], 'type' => 'post', 'name' => 'ActeurBsForm']);
}
echo $this->Bs->row();

echo $this->Bs->col('lg6') .
 $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
 $this->Html->tag('div', __('Identité'), ['class' => 'panel-heading']) .
 $this->Html->tag('div', null, ['class' => 'panel-body']) .
 $this->BsForm->input('Acteur.salutation', ['label' => __('Civilité')]) .
 $this->BsForm->input('Acteur.nom', ['label' => __('Nom') . ' <abbr title="obligatoire">*</abbr>']) .
 $this->BsForm->input('Acteur.prenom', ['label' => __('Prénom') . ' <abbr title="obligatoire">*</abbr>']) .
 $this->BsForm->input('Acteur.titre', ['label' => __('Titre')]) .
 $this->Bs->close(2);

echo $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
 $this->Html->tag('div', __('Adresse postale'), ['class' => 'panel-heading']) .
 $this->Html->tag('div', null, ['class' => 'panel-body']) .
 $this->BsForm->input('Acteur.adresse1', ['label' => __('Adresse 1')]) .
 $this->BsForm->input('Acteur.adresse2', ['label' => __('Adresse 2')]) .
 $this->BsForm->input('Acteur.cp', ['label' => __('Code postal')]) .
 $this->BsForm->input('Acteur.ville', ['label' => __('Ville')]) .
 $this->Bs->close(2);

echo $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
 $this->Html->tag('div', __('Contacts'), ['class' => 'panel-heading']) .
 $this->Html->tag('div', null, ['class' => 'panel-body']) .
 $this->BsForm->input('Acteur.telfixe', ['label' => __('Téléphone fixe')]) .
 $this->BsForm->input('Acteur.telmobile', ['label' => __('Téléphone mobile')]) .
 $this->BsForm->input('Acteur.email', ['label' => __('Email')]) .
 $this->Bs->close(3);

echo $this->Bs->col('lg6');

echo $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
 $this->Html->tag('div', 'Suppléant', ['class' => 'panel-heading']) .
 $this->Html->tag('div', null, ['class' => 'panel-body']);

echo $this->BsForm->select('Acteur.suppleant_id', $acteurs, ['empty' => true, 'label' => __('Élus'),
    'class' => 'selectone',
    'data-placeholder' => __('Sélectionner un suppleant'),
    'data-allow-clear' => true,
    'selected' => $this->Html->value('Acteur.suppleant_id')]) .
 $this->Bs->close(2);

echo $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
 $this->Html->tag('div', __('Type'), ['class' => 'panel-heading']) .
 $this->Html->tag('div', null, ['class' => 'panel-body']) .
 $this->BsForm->select('Acteur.typeacteur_id', $typeacteurs, [//data[Acteur][typeacteur_id]
    'id' => 'ActeurTypeacteurId',
    'label' => __('Type d\'acteur') . ' <abbr title="obligatoire">*</abbr>',
    'class' => 'selectone',
    'data-placeholder' => __('Sélectionner un type'),
    'data-allow-clear' => false,
    'autocomplete' => 'off',
    'empty' => true,
    'selected' => $this->Html->value('Acteur.typeacteur_id')
]). ($this->Form->isFieldError('Acteur.typeacteur_id') ?
        $this->Form->error('Acteur.typeacteur_id') : '') ;

echo $this->Bs->div('infoElus');
echo $this->BsForm->input('Acteur.position', ['label' => __('Ordre dans le conseil'), 'autocomplete' => 'off', 'size' => '3']) .
 $this->BsForm->select('Service.Service', $services, [
    'label' => __('Délégation(s)'),
    'default' => $selectedServices,
    'data-placeholder' => 'Aucun service',
    'multiple' => 'multiple',
    'class' => 'selectmultiple',
    'empty' => true,
    'autocomplete' => 'off',
    'escape' => false]);
echo $this->Bs->close(3);

echo $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
 $this->Html->tag('div', __('Autres informations'), ['class' => 'panel-heading']) .
 $this->Html->tag('div', null, ['class' => 'panel-body']) .
 $this->BsForm->input('Acteur.note', ['type' => 'textarea', 'label' => __('Note'), 'cols' => '30']) .
 $this->Bs->close(2);

$this->BsForm->setLeft(6);
$this->BsForm->setRight(6);
$notif_insertion = $this->html->value('Acteur.notif_insertion');
echo $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
 $this->Html->tag('div', __('Notifications'), ['class' => 'panel-heading']) .
 $this->Html->tag('div', null, ['class' => 'panel-body']) .
$this->Html->tag('p', __('Les paramètres ci-dessous permettent de gérer les notifications par mails envoyées aux acteurs.')) .
 $this->BsForm->radio('Acteur.notif_insertion', $notif, [
     'value' => empty($notif_insertion) ? false : $this->html->value('Acteur.notif_insertion'),
     'label' => __('L\'acteur est notifié lorsqu\'un projet dont il est le rapporteur est inséré dans un circuit')]) .
$this->BsForm->radio('Acteur.notif_projet_valide', $notif, [
    'label' => __('L\'acteur est notifié lorsqu\'un projet dont il est le rapporteur est validé'),
    'value' => empty($this->html->value('Acteur.notif_projet_valide')) ? false : $this->html->value('Acteur.notif_projet_valide')]) .

 $this->Bs->close(4);
$this->BsForm->setDefault();

echo $this->BsForm->hidden('Acteur.id');

echo $this->Html2->btnSaveCancel('', $previous) .
 $this->BsForm->end();
?>

<script>
require(['domReady'], function (domReady) {
    domReady(function () {
        var type_elus = [<?php echo $type_elus; ?>];

        afficheActeurElu();
        $('#ActeurTypeacteurId').change(function () {
            afficheActeurElu();
        });

        function afficheActeurElu() {
            if (($('#ActeurTypeacteurId').val() === "") || ($('#ActeurTypeacteurId').val() === "")) {
                $('.infoElus').hide();
            } else {
                if (jQuery.inArray(parseInt($('#ActeurTypeacteurId').val()), type_elus) !== -1) {
                    $('.infoElus').show();
                } else {
                    $('.infoElus').hide();
                }
            }
        }
    });
});
</script>
