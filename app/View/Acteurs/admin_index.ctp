<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Acteurs'));
$this->Html->addCrumb(__('Liste des acteurs'));

echo $this->Bs->tag('h3', __('Liste des acteurs'));
if ($this->permissions->check('Acteurs', 'create')) {
    echo $this->Html2->btnAdd(__("Ajouter un acteur"), __("Ajouter"));
}

echo $this->Bs->table([['title' => $this->Paginator->sort('position', __('N° d\'ordre'))], ['title' => 'Civilité'], ['title' => $this->Paginator->sort('nom', __('Nom'))], ['title' => $this->Paginator->sort('prenom', __('Prénom'))], ['title' => $this->Paginator->sort('titre', __('Titre'))], ['title' => $this->Paginator->sort('Typeacteur.nom', __('Type d\'acteur'))], ['title' => __('Élus'), 'class' => 'text-center'], ['title' => __('Suppléant')], ['title' => __('Actions'), 'class' => 'text-center']], ['hover', 'striped']);
foreach ($acteurs as $acteur) {
    echo $this->Bs->cell($acteur['Acteur']['libelleOrdre']);
    $service = '';
    foreach ($acteur['Service'] as $aService) {
        $service.=$aService['name'] . $this->Html->tag(null, '<br />');
    }
    echo $this->Bs->cell($acteur['Acteur']['salutation']);
    echo $this->Bs->cell($acteur['Acteur']['nom']);
    echo $this->Bs->cell($acteur['Acteur']['prenom']);
    echo $this->Bs->cell($acteur['Acteur']['titre']);
    echo $this->Bs->cell($acteur['Typeacteur']['nom']);
    echo $this->Bs->cell($acteur['Typeacteur']['elu'], 'text-center');
    echo $this->Bs->cell(isset($acteur['Acteur']['suppleant_id']) ? $acteur['Suppleant']['prenom'] . " " . $acteur['Suppleant']['nom'] : '');
    echo $this->Bs->cell($this->Bs->div('btn-group') .
        (
            $this->permissions->check('Acteurs', 'read')
                ? $this->Bs->btn($this->Bs->icon('eye', null, ['stylePrefix' => 'far']), ['controller' => 'acteurs', 'action' => 'view', $acteur['Acteur']['id']], ['type' => 'default', 'title' => __('Visualiser'), 'escapeTitle' => false])
                : ''
        ) .
        (
            $this->permissions->check('Acteurs', 'update')
                ? $this->Bs->btn($this->Bs->icon('pencil-alt'), ['controller' => 'acteurs', 'action' => 'edit', $acteur['Acteur']['id']], ['type' => 'primary', 'title' => __('Modifier'), 'escapeTitle' => false])
                : ''
        ) .
        (
            $this->permissions->check('Acteurs', 'delete')
                ? $this->Bs->btn($this->Bs->icon('trash'), ['controller' => 'acteurs', 'action' => 'delete', $acteur['Acteur']['id']], ['type' => 'danger', 'title' => __('Supprimer'), 'escapeTitle' => false], __('Êtes-vous sûr de vouloir supprimer %s %s ?', $acteur['Acteur']['prenom'], $acteur['Acteur']['nom']))
                : ''
        ) .
        $this->Bs->close(), 'text-center');
}
echo $this->Bs->endTable() .
 $this->Paginator->numbers(['before' => '<ul class="pagination">', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a', 'tag' => 'li', 'after' => '</ul><br />']);
