<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Acteurs'), ['action' => 'index']);
$this->Html->addCrumb(__('Fiche acteur'));

foreach ($acteur['Service'] as $service) {
    $services[] = $service['name'];
}

$panel_left = '<b>' . __('Identité') . ' : </b>' . $acteur['Acteur']['salutation'] . ' ' . $acteur['Acteur']['prenom'] . ' ' . $acteur['Acteur']['nom'] .
        ($acteur['Acteur']['titre'] ? ', ' : '') . $acteur['Acteur']['titre'] . '<br />' .
        '<b>' . __('Adresse postale') . ' : </b>' . $acteur['Acteur']['adresse1'] . '<br />' .
        ($acteur['Acteur']['adresse2'] ? $acteur['Acteur']['adresse2'] . '<br />' : '' ) .
        ($acteur['Acteur']['cp'] ? $acteur['Acteur']['cp'] . '<br />' : '' ) .
        ($acteur['Acteur']['ville'] ? $acteur['Acteur']['ville'] . '<br />' : '') .
        '<b>' . __('Contacts') . ' : </b><br />' .
        __('Téléphone fixe') . ' : ' . $acteur['Acteur']['telfixe'] . '<br />' .
        __('Téléphone mobile') . ' : ' . $acteur['Acteur']['telmobile'] . '<br />' .
        __('Adresse email') . ' : ' . $acteur['Acteur']['email'] . '<br />';

$panel_right = '<b>' . __('Type') . ' : </b>' . $acteur['Typeacteur']['nom'] . '<br />' .
        ($acteur['Typeacteur']['elu'] ? '<b>' . __('Numéro d\'ordre dans le conseil') . ' : </b>' .
                $acteur['Acteur']['position'] . '<br />' .
                '<b>' . __('Délégations') . ' : </b>' . (!empty($service) ? implode(',', $services) : '')
                 : '');

$panel_bottom = $this->Bs->row() .
        $this->Bs->col('xs4') . '<b>' . __('Note') . ' : </b>' . $acteur['Acteur']['note'] . '<br />' . $this->Bs->close() .
        $this->Bs->col('xs4') . '<b>' . __('Date de création') . ' : </b>' . $acteur['Acteur']['created'] . '<br />' . $this->Bs->close() .
        $this->Bs->col('xs4') . '<b>' . __('Date de modification') . ' : </b>' . $acteur['Acteur']['modified'] . '<br />' . $this->Bs->close(2);

echo $this->Bs->panel(__('Fiche acteur')) .
 $this->Bs->row() .
 $this->Bs->col('xs6') . $panel_left .
 $this->Bs->close() .
 $this->Bs->col('xs6') . $panel_right .
 $this->Bs->close(2) .
 $this->Bs->div('spacer') . $this->Bs->close() .
 $this->Bs->div('spacer') . $this->Bs->close() .
 $panel_bottom .
 $this->Bs->div('spacer') . $this->Bs->close() .
 $this->Bs->endPanel() .
 $this->Bs->row() .
 $this->Bs->col('md4 of5') .
 $this->Bs->div('btn-group', null, ['id' => "actions_fiche"]) .
 $this->Html2->btnCancel($previous) .
 $this->Bs->close(3);
