<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('%s n° %s',
    ucfirst(
        $revision['deliberations_versions']['data_version']['Typeacte']['name']),
    $revision['deliberations_versions']['data_version']['Deliberation']['id']), [
        'controller' => 'projets',
        'action'=>'view',
        $revision['deliberations_versions']['data_version']['Deliberation']['id']
]);
$this->Html->addCrumb(__('Révision'));

// Initialisation des boutons action de la vue
$linkBarre = $this->Bs->div('btn-toolbar', null, ['role' => 'toolbar']);
$linkBarre .= $this->Bs->div('btn-group'); //
$linkBarre .= $this->Html2->btnCancel($previous);

$linkBarre .= $this->Bs->close();
$linkBarre .= $this->Bs->div('btn-group');

if (!empty($revision['deliberations_versions']['data_version']['Deliberation']['signee'])) {
    //Bordereau si présent
    !empty($revision['deliberations_versions']['data_version']['Deliberation']['parapheur_bordereau']) ?
        $linkBarre .= $this->Bs->btn(
            $this->Bs->icon('file-pdf', null, ['stylePrefix' => 'far']) . ' ' . __('Bordereau de signature'),
            [
                'controller' => 'actes',
                'action' =>
                    'downloadBordereau',
                    $revision['deliberations_versions']['data_version']['Deliberation']['id'],
                    $revision['deliberations_versions']['id']
            ],

            [
                'type' => 'default',
                'escapeTitle' => false,
                'title' => empty($revision['deliberations_versions']['data_version']['Deliberation']['parapheur_bordereau']) ? __('Bordereau de signature indisponible.') : __('Télécharger le bordereau de signature')]
        ): null ;
    //Document signé
    $linkBarre .=
        $this->Bs->btn(
            $this->Bs->icon('file-pdf', null, ['stylePrefix' => 'far']) . ' ' . __('Document signé'),
            [
                'controller' => 'actes',
                'action' =>
                    'download',
                    $revision['deliberations_versions']['data_version']['Deliberation']['id'],
                    $revision['deliberations_versions']['id']
            ],
            [
                'type' => 'default',
                'escapeTitle' => false,
                'data-waiter-title' => __('Télécharger le document : %s', $revision['deliberations_versions']['data_version']['Deliberation']['id']),
                'title' => __('Télécharger le document:') . ' ' . $revision['deliberations_versions']['data_version']['Deliberation']['objet_delib']
            ]
        );
}

$linkBarre .= $this->Bs->close();
$linkBarre .= $this->Bs->close(2);

// affichage  du titre
$title = '<span class="label label-' . ($revision['deliberations_versions']['data_version']['Deliberation']['etat'] == -1 ? 'danger' : 'default') . '" ' . (!empty($revision['deliberations_versions']['data_version']['listeSeances'][0]['color']) ? 'style="background-color: ' . $revision['deliberations_versions']['data_version']['listeSeances'][0]['color'] . '"' : '') . '>' . $revision['deliberations_versions']['data_version']['Deliberation']['id'] . '</span> ';

if (!empty($revision['deliberations_versions']['data_version']['Multidelib'])) {
    $listeIds = $revision['deliberations_versions']['data_version']['Deliberation']['id'];
    foreach ($revision['deliberations_versions']['data_version']['Multidelib'] as $delibRattachee) {
        $listeIds .= ', ' . $delibRattachee['id'];
    }
    $title .= ' ' . __('Projet multi-délibérations n°') . $revision['deliberations_versions']['data_version']['Deliberation']['id'];
}

if (!empty($revision['deliberations_versions']['data_version']['User'])) {
    $title.=' ' . $this->Bs->icon(
        'users',
        '',
        [
                'data-toggle' => 'tooltip',
                'data-placement' => 'right',
                'escape' => false,
                'title' => __('Multi-rédacteurs')
                    ]
    );
}

echo $this->Bs->div('panel panel-warning');
echo $this->Bs->div(
    'panel-heading',
    $this->Bs->row() .
    $this->Bs->col('xs8') .
        $this->Bs->div('media') .
        $this->Bs->div('media-body') .
        $this->Bs->tag('h4', $revision['deliberations_versions']['data_version']['Deliberation']['objet'], ['class' => 'media-heading']) .
        $this->Bs->div(null, $title .' '. __('Révision du %s par %s', [$this->Time->i18nFormat($revision['deliberations_versions']['data_version']['Deliberation']['modified'], '%d/%m/%Y à %kh%M'),$revision['deliberations_versions']['userName']])) .
        $this->Bs->close(2) .
//        $this->Bs->tag('p',
//                .' '. $this->Bs->tag('h3', $revision['deliberations_versions']['Deliberation']['objet'], array('class' => 'text-right')), array('class' => 'text-left')) .
//
//        $title . ($revision['deliberations_versions']['Deliberation']['etat']==-1?' '.__('(Versionné)'):'').
//
        $this->Bs->close() .
        $this->Bs->col('xs4') .
        $this->Bs->div('text-right', $this->Bs->btn(isset($revision['deliberations_versions']['data_version']['Deliberation']['num_delib']) ? ucfirst($revision['deliberations_versions']['data_version']['Typeacte']['name']) . ' n° ' . $revision['deliberations_versions']['data_version']['Deliberation']['num_delib'] : ucfirst($revision['deliberations_versions']['data_version']['Typeacte']['name']), null, [
                    'tag' => 'button',
                    'type' => 'warning',
        ])) .
        $this->Bs->close(2)
);

// panels
echo $this->Bs->div('panel-body');
//echo $this->Bs->tabPane($title);
echo $linkBarre;
echo $this->Bs->tag('br /');

// onglets
$aTab = [
    'infos' => __('Informations principales'),
];

echo $this->Bs->tab($aTab, [
        'active' => isset($nameTab) ? $nameTab : 'infos',
        'class' => '-justified']) .
    $this->Bs->tabContent();

// Informations principales
echo $this->Bs->tabPane('infos', ['class' => ((isset($nameTab) && $nameTab == 'infos') || (!isset($nameTab))) ? 'active' : '']);
echo $this->Bs->tag('br');
// ouvre l'élément projetInfo
echo $this->element('Deliberations/projetInfo', ['projet' => $revision['deliberations_versions'],'revision'=>$revision['deliberations_versions']['id']]);
echo $this->Bs->tag('br');
// multi-délibs
$this->BsForm->setLeft(2);
$this->BsForm->setRight(10);
echo $this->Bs->row();
echo $this->Bs->col('xs' . $this->BsForm->getLeft(), null, ['class' => 'text-right']) . $this->Html->tag('label', __('Annexe(s) :')) . $this->Bs->close().
    $this->Bs->col('xs'.$this->BsForm->getRight());

if (!empty($revision['deliberations_versions']['data_version']['Annexe'])) {
    echo $this->element('annexe_view', array_merge(['ref' => 'delibPrincipale'], ['annexes' => $revision['deliberations_versions']['data_version']['Annexe']]));
}

echo $this->Bs->close(2);
echo $this->Bs->tabClose();

echo $this->Bs->tabPaneClose();

echo $this->Bs->tag('br /');
echo $this->Bs->tag('hr /');
echo $this->Bs->close(2);
