<h1><?php echo $this->pageTitle = 'Erreur 401:  Accès à l\'action refusé';?></h1>

<?php
    if( Configure::read( 'debug' ) > 0 ) {
        echo $this->element( 'exception_stack_trace' );
    }