<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Recherche multi-critères'));

echo $this->Bs->tag('h3', __('Recherche multi-critères'));

echo $this->BsForm->create('Search', [
    'id'=> 'appSearch',
    'type' => 'post',
    'url' => [
        'admin' => false,
        'prefix' => null,
        'controller' => 'search',
        'action' => 'index'
    ],
    'name' => 'Deliberation',
    'class' => 'form-horizontal waiter',
    'autocomplete' => 'off']);

echo $this->Bs->div('well');
//Formulaire par défault
echo $this->BsForm->select('Deliberation.id', [], [
    'class' => 'class-wd-selectToken',
    'multiple' => 'multiple',
    'data-placeholder' => __('Renseigner des numéros identifiants ou intervalles'),
    'label' => __('Identifiant(s)'),
]);
// Libellé
echo $this->BsForm->input('Deliberation.texte', [
    'label' => __('Libellé'),
    'placeholder' => __('Renseigner le texte recherché'),
]);
// Type(s) Acte(s)
echo $this->BsForm->select('Deliberation.typeacte_id', $typeActes, [
    'label' => __('Type(s) d\'acte(s)'),
    'class' => 'selectmultiple',
    'multiple' => true,
    'data-placeholder' => __('Sélectionner des types d\'actes'),
    'width' => '100%',
    'empty' => true,
    'escapeTitle' => false
]);
// Rapporteur
echo $this->BsForm->select('Deliberation.rapporteur_id', $rapporteurs, [
    'label' => __('Rapporteur(s)'),
    'class' => 'selectmultiple',
    'data-placeholder' => __('Sélectionner des rapporteurs'),
    'multiple' => true,
    'width' => '100%',
    'empty' => true]);
// Date
echo $this->BsForm->select('Deliberation.seance_id', $date_seances, [
    'label' => __('Date(s) séance(s)'),
    'class' => 'selectmultiple',
    'data-placeholder' =>  __('Sélectionner des séances'),
    'multiple' => true,
    'width' => '100%',
    'empty' => true]);
// Service Emetteur
echo $this->BsForm->select('Deliberation.service_id', $services, [
    'class' => 'selectmultiple',
    'data-placeholder' =>  __('Sélectionner des services'),
    'multiple' => true,
    'width' => '100%',
    'label' => __('Service(s) émetteur(s)'),
    'empty' => true,
    'escape' => false]);
// Rédacteurs
echo $this->BsForm->select('Deliberation.redacteur_id', $redacteurs, [
    'label' => __('Rédacteur(s)'),
    'data-placeholder' =>  __('Sélectionner des rédacteurs'),
    'class' => 'selectmultiple',
    'width' => '100%',
    'required' => false,
    'multiple' => true,
    'empty' => true]);
// Thème
echo $this->BsForm->select('Deliberation.theme_id', $themes, [
    'class' => 'selectmultiple',
    'data-placeholder' =>  __('Sélectionner des thèmes'),
    'label' => __('Thème(s)'),
    'width' => '100%',
    'escape' => false,
    'multiple' => true,
    'empty' => true]);
// Circuit
echo $this->BsForm->select('Deliberation.circuit_id', $circuits, [
    'class' => 'selectmultiple',
    'data-placeholder' => __('Sélectionner des circuits'),
    'label' => __('Circuit(s)'),
    'width' => '100%',
    'multiple' => true,
    'empty' => true]);
// Etat
echo $this->BsForm->select('Deliberation.etat', $etats, [
    'class' => 'selectone',
    'data-placeholder' => __('Sélectionner un état'),
    'data-allow-clear'=> true,
    'label' => __('État'),
    'width' => '100%',
    'empty' => true]);
// Numéro d'acte
echo $this->BsForm->select('Deliberation.num_delib', null, [
    'class' => 'class-wd-selectToken',
    'data-placeholder' => '',
    'width' => '100%',
    'empty' => true,
    'data-placeholder' => __('Renseigner des numéros d\'actes'),
    'multiple' => 'multiple',
    'label' => __("Numéro d'acte"),
]);

// Date de signature
echo $this->Bs->row();
echo $this->Bs->col('xs' . $this->BsForm->getLeft()) . $this->Bs->div('text-right', '<strong>' . __('Date de signature')) . '</strong>' . $this->Bs->close();
$tailleCol = $this->BsForm->getRight() / 3;
$this->BsForm->setMax();
echo $this->Bs->col('xs' . $tailleCol);
echo $this->BsForm->datetimepicker('Deliberation.dateDebut', [
    'language' => 'fr',
    'autoclose' => 'true',
    'format' => 'dd/mm/yyyy',
    'minView' => 2,
        ], [
    'placeholder' => __('Depuis'),
    'label' => false,
    'type' => 'date',
    'style' => 'cursor:pointer;background-color: white;',
    'help' => __('Cliquez sur le champ ci-dessus pour choisir la date'),
    'title' => __('Filtre sur les dates des commentaires')]);
echo $this->Bs->close();
echo $this->Bs->col('xs' . $tailleCol);
echo $this->BsForm->datetimepicker('Deliberation.dateFin', [
    'language' => 'fr',
    'autoclose' => 'true',
    'format' => 'dd/mm/yyyy',
    'minView' => 2,
        ], [
    'placeholder' => __('Jusqu\'à'),
    'label' => false,
    'type' => 'date',
    'style' => 'cursor:pointer;background-color: white;',
    'help' => __('Cliquez sur le champ ci-dessus pour choisir la date'),
    'title' => __('Filtre sur les dates des commentaires')]);
echo $this->Bs->close();
echo $this->Bs->col('xs' . $tailleCol);
echo $this->BsForm->select('difDate', [
    '-1 day' => __('1 jour'),
    '-1 week' => __('1 semaine'),
    '-1 month' => __('1 mois'),
    '-1 year' => __('1 an')], [
    'class' => 'selectone',
    'width' => '100%',
    'data-placeholder' => __('Sélectionner un intervale'),
    'data-allow-clear'=> true,
    'label' => false,
    'required' => false,
    'empty' => true]);

echo $this->Bs->close(2);

$this->BsForm->setDefault();

// Date en préfecture
echo $this->Bs->row();
echo $this->Bs->col('xs' . $this->BsForm->getLeft()) . $this->Bs->div('text-right', '<strong>' . __('Date en préfecture')) . '</strong>' . $this->Bs->close();
$tailleCol = $this->BsForm->getRight() / 3;

$this->BsForm->setMax();
echo $this->Bs->col('xs' . $tailleCol);
echo $this->BsForm->datetimepicker('Deliberation.dateDebutAr', [
    'language' => 'fr',
    'autoclose' => 'true',
    'format' => 'dd/mm/yyyy',
    'minView' => 2,
        ], [
    'placeholder' => __('Depuis'),
    'label' => false,
    'type' => 'date',
    'style' => 'cursor:pointer;background-color: white;',
    'help' => __('Cliquez sur le champ ci-dessus pour choisir la date'),
    'title' => __('Filtre sur les dates des commentaires')]);
echo $this->Bs->close();
echo $this->Bs->col('xs' . $tailleCol);
echo $this->BsForm->datetimepicker('Deliberation.dateFinAr', [
    'language' => 'fr',
    'autoclose' => 'true',
    'format' => 'dd/mm/yyyy',
    'minView' => 2,
        ], [
    'placeholder' => __('Jusqu\'à'),
    'label' => false,
    'type' => 'date',
    'style' => 'cursor:pointer;background-color: white;',
    'help' => __('Cliquez sur le champ ci-dessus pour choisir la date'),
    'title' => __('Filtre sur les dates des commentaires')]);
echo $this->Bs->close();
echo $this->Bs->col('xs' . $tailleCol);
echo $this->BsForm->select('difDateAr', [
    '-1 day' => __('1 jour'),
    '-1 week' => __('1 semaine'),
    '-1 month' => __('1 mois'),
    '-1 year' => __('1 an')], [
    'label' => false,
    'width' => '100%',
    'class' => 'selectone',
    'data-placeholder' => __('Sélectionner un intervale'),
    'required' => false,
    'empty' => true]);

echo $this->Bs->close(3);
$this->BsForm->setDefault();

//Formulaire informations supplémentaires
if (!empty($infosupdefs)) {
    echo $this->Bs->div('well');

    foreach ($infosupdefs as $infosupdef) {
        $fieldName = 'Infosup.' . $infosupdef['Infosupdef']['id'];
        if ($infosupdef['Infosupdef']['type'] == 'text' || $infosupdef['Infosupdef']['type'] == 'richText') {
            echo $this->BsForm->input($fieldName, ['label' => $infosupdef['Infosupdef']['nom'], 'title' => $infosupdef['Infosupdef']['commentaire']]);
        } elseif ($infosupdef['Infosupdef']['type'] == 'date') {
            // Date
            echo $this->Bs->row();
            echo $this->Bs->col('xs' . $this->BsForm->getLeft()) . $this->Bs->div('text-right', '<strong>' . $infosupdef['Infosupdef']['nom']) . '</strong>' . $this->Bs->close();
            $tailleCol = $this->BsForm->getRight() / 3;
            $infosupdefId = Inflector::camelize($infosupdef['Infosupdef']['id']);
            $this->BsForm->setMax();
            echo $this->Bs->col('xs' . $tailleCol);
            echo $this->BsForm->datetimepicker("Infosup." . $infosupdefId.'.dateDebut', [
                'language' => 'fr',
                'autoclose' => 'true',
                'format' => 'dd/mm/yyyy',
                'minView' => 2,
                    ], [
                'placeholder' => __('Depuis'),
                'label' => false,
                'type' => 'date',
                'class' => 'searchInfosupDateDebut',
                'data-wd-infosups-id' => $infosupdefId,
                'style' => 'cursor:pointer;background-color: white;',
                'help' => __('Cliquez sur le champ ci-dessus pour choisir la date'),
                'title' => __('Filtre sur les dates des commentaires')]);
            echo $this->Bs->close();
            echo $this->Bs->col('xs' . $tailleCol);
            echo $this->BsForm->datetimepicker("Infosup." . $infosupdefId.'.dateFin', [
                'language' => 'fr',
                'autoclose' => 'true',
                'format' => 'dd/mm/yyyy',
                'minView' => 2,
                    ], [
                'placeholder' => __('Jusqu\'à'),
                'label' => false,
                'type' => 'date',
                'style' => 'cursor:pointer;background-color: white;',
                'class' => 'searchInfosupDateFin',
                'data-wd-infosups-id' => $infosupdefId,
                'help' => __('Cliquez sur le champ ci-dessus pour choisir la date'),
                'title' => __('Filtre sur les dates des commentaires')]);
            echo $this->Bs->close();
            echo $this->Bs->col('xs' . $tailleCol);
            echo $this->BsForm->select("Infosup." . $infosupdefId.'.difDate', [
                '-1 day' => __('1 jour'),
                '-1 week' => __('1 semaine'),
                '-1 month' => __('1 mois'),
                '-1 year' => __('1 an')], [
                'label' => false,
                'width' => '100%',
                'class' => 'selectone searchInfosupInterval',
                'data-wd-infosups-id' => $infosupdefId,
                'data-placeholder' => __('Sélectionner un intervale'),
                'required' => false,
                'empty' => true]);

            echo $this->Bs->close(2);
            $this->BsForm->setDefault();
        } elseif ($infosupdef['Infosupdef']['type'] == 'boolean') {
            echo $this->BsForm->select($fieldName, $listeBoolean, [
                'label' => $infosupdef['Infosupdef']['nom'],
                'empty' => true,
                'width' => '100%',
                'required' => false,
                'class' => 'selectone',
                'data-placeholder' => __('Sélectionner une valeurs de la liste "%s"', $infosupdef['Infosupdef']['nom'])
            ]);
        } elseif (in_array($infosupdef['Infosupdef']['type'], ['list','listmulti'], true)) {
            if (!empty($infosuplistedefs[$infosupdef['Infosupdef']['code']])) {
                echo $this->BsForm->select($fieldName, $infosuplistedefs[$infosupdef['Infosupdef']['code']], [
                    'label' => $infosupdef['Infosupdef']['nom'],
                    'empty' => true,
                    'width' => '100%',
                    'required' => false,
                    'multiple' => true,
                    'class' => 'selectmultiple',
                    'data-placeholder' => __('Sélectionner une ou plusieurs valeurs de la liste "%s"', $infosupdef['Infosupdef']['nom'])
                ]);
            }
        }
    }
    echo $this->Bs->close();
}

if (!empty($models)) {
    echo $this->Bs->div('well');
    echo $this->Bs->row();
    echo $this->Bs->col('xs' . $this->BsForm->getLeft()) . $this->Bs->div('text-right', '<strong>' . __('Générer la recherche')) . '</strong>' . $this->Bs->close();
    $tailleCol = $this->BsForm->getRight() / 3;
    $this->BsForm->setMax();
    echo $this->Bs->col('xs' . $tailleCol);
    echo $this->BsForm->select('Deliberation.model', $models, [
        'label' => false,
        'data-placeholder' => __('Sélectionner un modèle de document'),
        'data-allow-clear'=> true,
        'help' => __('Choisir un modèle de document'),
        'required' => false,
        'width' => '100%',
        'empty' => true,
        'class' => 'selectone',
    ]);
    echo $this->Bs->close();
    echo $this->Bs->col('xs' . $tailleCol);
    echo $this->BsForm->checkbox('Recherche.is_genere_multi', [
    'autocompvare' => 'off',
    'label' => 'Générations multiples',
    ]);
    echo $this->Bs->close();
    echo $this->Bs->col('xs' . $tailleCol);
    echo $this->BsForm->select('Recherche.format_output', ['pdf'=>__('Pdf'), 'zip'=>__('Zip')], [
        'label' => false,
        'help' => __('Choisir le format de sortie pour la génération multiples'),
        'required' => false,
        'width' => '100%',
        'empty' => false,
        'default'=> 'pdf',
        'class' => 'selectone',
    ]);
    echo $this->Bs->close(2);
    $this->BsForm->setDefault();
    echo $this->Bs->row();
    echo $this->Bs->col('xs' . $this->BsForm->getLeft()) . $this->Bs->close();
    echo $this->Bs->col('xs' . $this->BsForm->getRight());
    echo $this->html->tag('em', __(
        'Note : Pour générer la recherche, sélectionnez le modèle d\'édition, puis cliquez sur le bouton "Rerchercher".'.$this->Html->tag('br')
             .'Vous pouvez aussi sortir le resultat sous forme de générations mutilples en cochant Générations mutilples puis sélectionnez le format de sortie.'
    ), ['class' => 'help-block']) ;
    echo $this->Bs->close(2);
    $this->BsForm->setDefault();
    echo $this->Bs->close();
}

// tri
echo $this->Bs->div('well');
echo $this->BsForm->hidden('Deliberation.tri', ['id' => 'strOrder']);
echo $this->BsForm->select('order.order', $filtre = [
    'Deliberation.id asc' => __('Identifiant') . ' ' . $this->Bs->icon('sort-numeric-down'),
    'Deliberation.id desc' => __('Identifiant') . ' ' . $this->Bs->icon('sort-numeric-up'),
    'Deliberation.num_delib asc' => __('N° d\'acte') . ' ' . $this->Bs->icon('sort-numeric-down'),
    'Deliberation.num_delib desc' => __('N° d\'acte') . ' ' . $this->Bs->icon('sort-numeric-up'),
    'Deliberation.objet asc' => __('Libellé') . ' ' . $this->Bs->icon('sort-alpha-down'),
    'Deliberation.objet desc' => __('Libellé') . ' ' . $this->Bs->icon('sort-alpha-up'),
    'DeliberationSeance_Filter.position asc' => __('Ordre du jour') . ' ' . $this->Bs->icon('sort-numeric-down'),
    'DeliberationSeance_Filter.position desc' => __('Ordre du jour') . ' ' . $this->Bs->icon('sort-numeric-up'),
    'Theme.order asc' => __('Thème') . ' ' . $this->Bs->icon('sort-alpha-down'),
    'Theme.order desc' => __('Thème') . ' ' . $this->Bs->icon('sort-alpha-up'),
    'Seance_Filter.date asc' => __('Date de séance') . ' ' . $this->Bs->icon('sort-numeric-down'),
    'Seance_Filter.date desc' => __('Date de séance') . ' ' . $this->Bs->icon('sort-numeric-up'),
    'Deliberation.date_acte asc' => __('Date de signature') . ' ' . $this->Bs->icon('sort-numeric-down'),
    'Deliberation.date_acte desc' => __('Date de signature') . ' ' . $this->Bs->icon('sort-numeric-up'),
    'Deliberation.tdt_ar_date asc' => __('Date en préfecture') . ' ' . $this->Bs->icon('sort-numeric-down'),
    'Deliberation.tdt_ar_date desc' => __('Date en préfecture') . ' ' . $this->Bs->icon('sort-numeric-up'),
        ], [
    'label' => __('Ordonner'),
    'help' => __('Ordonner le résultat par ordre croissant ou décroissant'),
    'required' => false,
    'data-placeholder' => __('Sélectionner des ordres de tri'),
    'class' => 'selectMultipleOrder ',//selectmultiple selectMultipleOrderBySelect
    'width' => '100%',
    'escape' => true,
    'multiple' => 'multiple',
]);
echo $this->Bs->close();

echo $this->BsForm->button($this->Bs->icon('search') . ' ' . __('Rechercher'), [
    'type' => 'submit', 'div' => false,
    'class' => 'btn btn-success col-md-offset-5',
    'name' => 'Rechercher',
    'formnovalidate' => true,
    'id' => 'submitSearchForm']);
echo $this->Bs->close();
echo $this->BsForm->end();
