<?php
$defaultModel=$this->Paginator->defaultModel();
$nbProjets = !empty($defaultModel) ? $this->Paginator->counter('{:count}') : $nbProjets;
$titre = $titreVue . ' (' . $nbProjets . ' ' . ($nbProjets > 1 ? 'projets' : 'projet') . ')';

echo $this->Bs->tag('h3', __('Résultat de la recherche') . ' (' . $nbProjets . ' ' . ($nbProjets > 1 ? 'projets' : 'projet') . ')');
$this->Html->addCrumb(__('Résultat de la recherche'));


if (isset($traitement_lot) && ($traitement_lot == true) && $nbProjets > 0) {
    echo $this->Form->create('Deliberation', ['url' => ['controller' => 'TraitementParLot', 'action' => 'index'], 'type' => 'post', 'class'=>'waiter']);
}

echo $this->element('9cases', ['projets' => $this->data, 'traitement_lot' => isset($traitement_lot) ? $traitement_lot : null]
);

//paginate
echo $this->element('paginator', ['paginator' => $this->Paginator]);

echo $this->element('Projet/traitement_lot', ['modeles' => isset($modeles) ? $modeles : null, 'traitement_lot' => isset($traitement_lot) ? $traitement_lot : null]
);