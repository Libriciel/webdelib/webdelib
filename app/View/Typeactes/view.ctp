<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Types d\'actes'), ['action' => 'index']);
$this->Html->addCrumb($typeacte['Typeacte']['name']);

$panel_left = '<b>' . __('Modèle de projet') . ' : </b>' . $typeacte['Modelprojet']['name']
        . '<br />' . '<b>' . __('Modèle de document final : ') . '</b>' . $typeacte['Modeldeliberation']['name']
        . '<br />' . '<b>' . __('Date de création') . ' : </b>' . $typeacte['Typeacte']['created']
        . '<br />' . '<b>' . __('Gabarit : texte de projet') . '</b> - '
        .(!empty($typeacte['Typeacte']['gabarit_projet']) ? $this->Html->link($typeacte['Typeacte']['gabarit_projet_name'], ['action' => 'downloadGabarit', $typeacte['Typeacte']['id'], 'projet']) : '')
        .' '.__('(actif : %s)', (!empty($typeacte['Typeacte']['gabarit_projet_active'])?'oui':'non'))
        . '<br />' . '<b>Gabarit : note de synthèse</b> - '
        .(!empty($typeacte['Typeacte']['gabarit_synthese']) ? $this->Html->link($typeacte['Typeacte']['gabarit_synthese_name'], ['action' => 'downloadGabarit', $typeacte['Typeacte']['id'], 'synthese']) : '')
        .' '.__('(actif : %s)', (!empty($typeacte['Typeacte']['gabarit_synthese_active'])?'oui':'non'))
        . '<br />' . '<b>' . __('Gabarit : texte d\'acte') . '</b> - '
        .(!empty($typeacte['Typeacte']['gabarit_acte']) ? $this->Html->link($typeacte['Typeacte']['gabarit_acte_name'], ['action' => 'downloadGabarit', $typeacte['Typeacte']['id'], 'acte']) : '')
        .' '.__('(actif : %s)', (!empty($typeacte['Typeacte']['gabarit_acte_active'])?'oui':'non'));

$panel_right = '<b>' . __('Nature : ') . '</b>' . (isset($typeacte['Nature']['name']) ? $typeacte['Nature']['name'] : '')
        . '<br />' . '<b>' . __('Date de création : ') . '</b>' . $typeacte['Typeacte']['created'] . '<br />'
        . '<br />' . '<b>' . __('Date de modification : ') . '</b>' . $typeacte['Typeacte']['modified'] . '<br />';

echo $this->Bs->tag('h3', __('Types d\'acte')) .
 $this->Bs->panel(__('Fiche type d\'acte : %s', $typeacte['Typeacte']['name'])) .
 $this->Bs->row() .
 $this->Bs->col('xs6') . $panel_left .
 $this->Bs->close() .
 $this->Bs->col('xs6') . $panel_right .
 $this->Bs->close(2) .
 $this->Bs->endPanel() .
 $this->Bs->row() .
 $this->Bs->col('md4 of5') .
 $this->Bs->div('btn-group', null, ['id' => "actions_fiche"]) .
 $this->Html2->btnCancel() .
(
    $this->permissions->check('Typeactes', 'update')
        ? $this->Bs->btn($this->Bs->icon('pencil-alt') . ' ' . __('Modifier'), ['controller' => 'typeactes', 'action' => 'edit', $typeacte['Typeacte']['id']], ['type' => 'primary', 'title' => 'Modifier', 'escapeTitle' => false])
        : ''
) .
 $this->Bs->close(3);
