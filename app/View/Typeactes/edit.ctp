<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Types d\'actes'), ['action' => 'index']);

if ($this->Html->value('Typeacte.id')) {
    $this->Html->addCrumb(__('Modification d\'un type d\'acte'));
    echo $this->Bs->tag('h3', $this->Html->value('Typeacte.name')) .
    $this->BsForm->create('Typeacte', ['url'=>['controller' => 'typeactes', 'action' => 'edit'], 'type' => 'file', $this->Html->value('Typeacte.id')]);
} else {
    $this->Html->addCrumb(__('Ajouter un type d\'acte'));
    echo $this->Bs->tag('h3', __('Ajouter un type d\'acte')) .
    $this->BsForm->create('Typeacte', ['url' => ['controller' => 'typeactes', 'action' => 'add'], 'type' => 'file']);
}

echo $this->Bs->div('panel panel-default') .
 $this->Bs->div('panel-heading', __('Informations générales')) .
 $this->Bs->div('panel-body') .
 $this->BsForm->input('Typeacte.name', ['label' => __('Libellé'), 'required' => true, 'type' => 'text']) .
 $this->BsForm->select('Typeacte.compteur_id', $compteurs, ['label' => __('Compteur'), 'class' => 'selectone', 'default' => $this->Html->value('Typeacte.compteur_id'), 'data-placeholder' => __('Sélectionner un compteur'), 'inline' => true, 'required' => true, 'autocomplete' => 'off']) .
 $this->BsForm->select('Typeacte.nature_id', $natures, ['label' => __('Nature'), 'class' => 'selectone', 'inline' => true, 'required' => true, 'data-placeholder' => __('Sélectionner une nature'), 'autocomplete' => 'off', 'value' => !empty($this->request->data['Typeacte']['nature_id']) ? $this->request->data['Typeacte']['nature_id'] : false]);

$checked = false;
if ($this->action == 'add') {
    $checked = true;
}

echo $this->BsForm->radio('Typeacte.teletransmettre', ['1' => __('Oui'), '0' => __('Non')], ['label' => __('L\'acte est-il télétransmissible ?'), 'div' => false, 'value' => $this->html->value('Typeacte.teletransmettre')]);

echo $this->BsForm->radio('Typeacte.deliberant', ['1' => __('Oui'), '0' => __('Non')], ['label' => __('L\'acte est-il délibérant ?'), 'div' => false, 'value' => $this->html->value('Typeacte.deliberant')]);

echo $this->BsForm->radio('Typeacte.publier',['1' => __('Oui'), '0' => __('Non')],  ['label' => __('L\'acte est-il publiable ?'), 'div' => false, 'value' => $this->html->value('Typeacte.publier')]);

echo $this->BsForm->radio('Typeacte.verser_sae',['1' => __('Oui'), '0' => __('Non')],  ['label' => __('L\'acte est-il archivable ?'), 'div' => false, 'value' => $this->html->value('Typeacte.verser_sae')]) .
    $this->Bs->close(2);


echo $this->Bs->div('panel panel-default') .
 $this->Bs->div('panel-heading', __('Modèles pour les éditions')) .
 $this->Bs->div('panel-body') .
 $this->BsForm->select('Typeacte.modele_projet_id', $models_projet, ['label' => __('Projet'), 'class' => 'selectone', 'data-placeholder' => __('Sélectionner un modèle'), 'empty' => false, 'required' => true, 'inline' => true, 'autocomplete' => 'off', 'value' => $this->Html->value('Typeacte.modele_projet_id')]) .
 $this->BsForm->select('Typeacte.modele_final_id', $models_docfinal, ['label' => __('Document final'), 'class' => 'selectone', 'data-placeholder' => __('Sélectionner un modèle'), 'empty' => false, 'required' => true, 'inline' => true, 'autocomplete' => 'off', 'value' => $this->Html->value('Typeacte.modele_deliberation_id')]) .
 $this->BsForm->select('Typeacte.modele_bordereau_projet_id', $models_bordereau, ['label' => __('Bordereau'), 'class' => 'selectone', 'data-placeholder' => __('Sélectionner un modèle'), 'empty' => true, 'data-allow-clear' => true, 'inline' => true, 'autocomplete' => 'off', 'value' => $this->Html->value('Typeacte.modele_bordereau_projet_id')]) .
 $this->Bs->close(2);

echo $this->Bs->div('panel panel-default') .
 $this->Bs->div('panel-heading', __('Configuration des textes (Gabarits / Textes par défaut)')) .
 $this->Bs->div('panel-body');

foreach (['projet', 'synthese', 'acte'] as $gabarit) {
    //champ file projet
    if (!empty($this->data['Typeacte']['gabarit_' . $gabarit])) {
        echo $this->BsForm->setLeft(0)
            . $this->Bs->row()
                . $this->Bs->col('xs2')
                    . $this->BsForm->checkbox('gabarit_' . $gabarit.'_active' , ['title'=> __('Active/désactive le texte'), 'label'=> __('Afficher le texte'), 'checked' => $this->data['Typeacte']['gabarit_' . $gabarit.'_active']]). $this->Bs->close() .
                    $this->Bs->col('xs10') .
                    $this->Bs->div('media')
                    . $this->Bs->link($this->Bs->icon('file-alt', ['4x']), 'javascript:void(0);', ['class' => 'media-left', 'escape' => false]) .
                    $this->Bs->div('media-body') .
                    $this->Bs->tag('h4', $this->data['Typeacte']['gabarit_' . $gabarit . '_name'], ['class' => 'media-heading']) .
                    $this->Bs->div('btn-group') .
                    $this->Bs->btn($this->Bs->icon('download') . ' ' . __('Télécharger'), ['controller' => 'typeactes', 'action' => 'downloadGabarit', $typeacte_id, $gabarit], ['escapeTitle' => false, 'type' => 'default', 'size' => 'xs', 'class' => 'media-left']) .
                    $this->Bs->btn($this->Bs->icon('pencil-alt') . ' ' . __('Modifier'), ${'file_url_gabarit_' . $gabarit}, ['type' => 'primary', 'size' => 'xs', 'class' => 'media-left', 'escapeTitle' => false]) .
                    $this->Bs->btn($this->Bs->icon('trash') . ' ' . __('Supprimer'), ['controller' => 'typeactes', 'action' => 'deleteGabarit', $typeacte_id, $gabarit], ['escapeTitle' => false, 'type' => 'danger', 'size' => 'xs', 'confirm' => __('Voulez-vous vraiment supprimer %s du projet ?', $this->data['Typeacte']['gabarit_' . $gabarit . '_name']), 'class' => 'media-left']) .
                    $this->BsForm->hidden('file_gabarit_' . $gabarit, ['value' => ${'file_gabarit_' . $gabarit}]) .
        $this->Bs->close(5);
    } else {
        echo $this->BsForm->setLeft(0) .
        $this->Bs->row()
                . $this->Bs->col('xs2')
                . $this->BsForm->checkbox('gabarit_' . $gabarit.'_active' , ['title'=> __('Active/désactive le texte'), 'label'=> __('Afficher le texte'), 'checked' => !empty($this->data['Typeacte']) && !empty($this->data['Typeacte']['gabarit_' . $gabarit.'_active']) ? true : false])
                . $this->Bs->close() .
                $this->Bs->col('xs10') .
                $this->Bs->row().
                    $this->Bs->col('xs8') .
                    $this->BsForm->input('Typeacte.gabarit_' . $gabarit . '_upload', ['label' => false, 'type' => 'file', 'data-btnClass' => 'btn-primary', 'data-placeholder' => __('Pas de fichier'), 'data-text' => ' ' . __('Choisir un fichier (%s)', ucfirst($gabarit)), 'data-icon' => $this->Bs->icon('file-alt'), 'data-badge' => 'true', 'help' => __('Les modifications apportées ici ne prendront effet que lors de la sauvegarde du type d\'acte.'), 'title' => __('Choisir un fichier'), 'class' => 'filestyle']) . $this->Bs->close() .
                    $this->Bs->col('xs4') .
                    $this->Bs->div('btn-group btn-group-right') .
                    $this->Bs->btn($this->Bs->icon('eraser') . ' ' . __('Effacer'), '#TypeacteGabarit' . ucfirst($gabarit) . 'Upload', ['type' => 'danger', 'escapeTitle' => false, 'class' => 'btn-danger-right wd-filestyle-clear', 'data-wd-filestyle-id' => 'TypeacteGabarit' . ucfirst($gabarit) . 'Upload']) .
            $this->Bs->close(5);
    }


    echo $this->Bs->tag('br');
}
$this->BsForm->setDefault();
echo $this->Bs->close(2) . $this->BsForm->hidden('Typeacte.id') .
 $this->Html2->btnSaveCancel('', $previous) .
 $this->BsForm->end();
