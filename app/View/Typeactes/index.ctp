<?php

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Types d\'actes'), ['action' => 'index']);
echo $this->Bs->tag('h3', __('Liste des types d\'acte'));

if ($this->permissions->check('Typeactes', 'create')) {
    echo $this->Html2->btnAdd(__("Ajouter un type d'acte", "Ajouter"));
}
echo $this->Bs->table([['title' => __('Libellé')], ['title' => __('Compteur')], ['title' => __('Modèles')], ['title' => __('Nature')], ['title' => __('Transmissible'), 'class' => 'text-center'], ['title' => __('Actions'), 'class' => 'text-center']], ['hover', 'striped']);
foreach ($typeactes as $typeacte) {
    echo $this->Bs->cell($typeacte['Typeacte']['name']);
    echo $this->Bs->cell($typeacte['Compteur']['nom']);
    echo $this->Bs->cell(__('Document préparatoire : ') . $typeacte['Modelprojet']['name'] .
            $this->Html->tag(null, '<br />') .
            __('Document final : ') . $typeacte['Modeldeliberation']['name'].
            (!empty($typeacte['Modelbordereau']['name'])?
            $this->Html->tag(null, '<br />') .
            __('Bordereau : ') . $typeacte['Modelbordereau']['name']:'')
          );
    echo $this->Bs->cell($typeacte['Nature']['name']);


    $actions = $this->Bs->div('btn-group');
    if ($this->permissions->check('Typeactes', 'read')) {
        $actions .= $this->Bs->btn($this->Bs->icon('eye', null, ['stylePrefix' => 'far']), ['controller' => 'typeactes', 'action' => 'view', $typeacte['Typeacte']['id']], ['type' => 'default', 'escapeTitle' => false, 'title' => __('Visualiser')]);
    }
    if ($this->permissions->check('Typeactes', 'update')) {
        $actions .= $this->Bs->btn($this->Bs->icon('pencil-alt'), ['controller' => 'typeactes', 'action' => 'edit', $typeacte['Typeacte']['id']], ['type' => 'primary', 'escapeTitle' => false, 'title' => __('Modifier')]);
    }
    if ($this->permissions->check('Typeactes', 'delete')) {
        $actions .= $this->Bs->btn($this->Bs->icon('trash'), ['controller' => 'typeactes', 'action' => 'delete', $typeacte['Typeacte']['id']], ['type' => 'danger', 'escapeTitle' => false, 'confirm' => __('Êtes-vous sûr de vouloir supprimer %s ?', $typeacte['Typeacte']['name']), 'title' => __('Supprimer'), 'class' => empty($typeacte['Typeacte']['is_deletable']) ? 'disabled' : '']);
    }
    $actions .= $this->Bs->close();
    echo $this->Bs->cell($typeacte['Typeacte']['teletransmettre'] ? __('Oui') : __('Non'), 'text-center');
    echo $this->Bs->cell($actions, 'text-center');
}
echo $this->Bs->endTable().$this->element('paginator', ['paginator' => $this->Paginator]);
