<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class ProjetUtilHelper extends AppHelper
{
    private $etat_icon;

    /**
     *
     * @param View $view
     * @param type $settings
     */
    public function __construct(View $view, $settings = [])
    {
        $this->etat_icon=[
                'nouvelle_version'=>[
                    'type'=>'danger',
                    'icon'=>'pause',
                    'help'=> __(' Le projet a été re-créé, car il a été refusé.')
                    ],
                'refuse'=>[
                    'type'=>'danger',
                    'icon'=>'stop',
                    'help'=> __('Le projet n\'a pas été validé, une nouvelle version a été créée.')],
                'encours'=>[
                    'type'=>'success',
                    'icon'=>'pause',
                    'help'=> __('Le projet est en cours de rédaction.')],
                'circuit_attente'=>[
                    'type'=>'default',
                    'icon'=>'clock',
                    'help'=> __('Le projet est en attente dans une étape du circuit.')],
                'attente_circuit'=>[
                    'type'=>'success',
                    'icon'=>'pause',
                    'help'=> __('Le projet est en attente d\'insertion dans un circuit.')],
                'circuit_user_atraiter'=>[
                    'type'=>'success',
                    'icon'=>'play',
                    'help'=> __('Le projet est à valider.')],
                'circuit_user_fini'=>[
                    'libelle' => __('Traitement effectué.'),
                    'type'=>'default',
                    'icon'=>'lock',
                    'help'=> __('Le projet est en attente dans une étape du circuit.')],
                'circuit_redacteur_attente'=>[
                    'type'=>'warning',
                    'icon'=>'clock',
                    'help'=> __('Le projet est en attente dans une étape du circuit.')],
                'fini'=>[
                    'libelle' => __('Validé'),
                    'type'=>'success',
                    'icon'=>'check',
                    'help'=> __('Le circuit de validation est terminé pour ce projet.')],
                'valide_editable'=>[
                    'libelle' => __('Validé'),
                    'type'=>'success',
                    'icon'=>'check',
                    'help'=> __('Le circuit de validation est terminé pour ce projet.')],
                'voter_pour'=>[
                        'type'=>'success',
                        'icon'=>'thumbs-up',
                        'help'=> __('Le projet est adopté.')],
                'voter_contre'=>[
                        'type'=>'danger',
                        'icon'=>'thumbs-down',
                        'help'=> __('Le projet est rejeté.')],
                'transmis'=>[
                        'type'=>'success',
                        'icon'=>'university',
                        'help'=> __('Le projet est transmis au TdT.')],
                'a_renvoyer_signature'=>[
                        'type'=>'danger',
                        'icon'=>'signature',
                        'iconType' => 'ls',
                        'help'=> __('Signature annulée.')],
                'transmis_manuellement'=>[
                        'type'=>'success',
                        'icon'=>'university',
                        'help'=> __('Le projet a été déposé manuellement.')],
                'abandonné'=>[
                        'type'=>'danger',
                        'icon'=>'minus-circle',
                        'help'=> __('Le projet a été abandonné.')],
            ];

        parent::__construct($view, $settings);
    }

    /**
     *
     * @param type $value
     * @return type
     */
    public function etat_icon($value)
    {
        return $this->etat_icon[$value]['icon'];
    }

    /**
     *
     * @param type $value
     * @return type
     */
    public function etat_icon_type($value)
    {
        return $this->etat_icon[$value]['type'];
    }

    /**
     * @version 5.1
     * @param type $value
     * @return type
     */
    public function etat_iconType($value)
    {
        if (isset($this->etat_icon[$value]['iconType'])) {
            return $this->etat_icon[$value]['iconType'];
        }
        return '';
    }

    /**
     *
     * @param type $value
     * @return type
     */
    public function etat_icon_help($value)
    {
        return $this->etat_icon[$value]['help'];
    }
}
