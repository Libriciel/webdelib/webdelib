<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @version     5.1.0
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * Helper TinyMCE
 *
 * @package app.View.Helper
 * @version 5.1.0
 */
class TinyMCEHelper extends Helper
{
    public $helpers = ['Html'];

    /**
     * load TinyMCE
     * @param int $id
     * @version 5.0
     * @since 4.0
     */
    public function load($class, $disable = false)
    {
        $code = 'require(["domReady"], function (domReady) {
                    domReady(function () {
                    tinymce.baseURL = "/js/components/tinymce";
                    tinymce.init({
                        themes: "modern",'
                    . ($disable ? 'readonly : true,' : '')
                    .'
                        mode : "specific_textareas",
                        browser_spellcheck: true,
                        language_url : "/js/components/tinymce-i18n/langs/fr_FR.js",
                        editor_selector: \'' . $class . '\'})
                        '
                    . '});'
                . '});';

        return $this->Html->scriptBlock($code);
    }
}
