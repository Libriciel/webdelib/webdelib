<?php

class CheckHelper extends AppHelper
{

    /**
     * Helpers utilisés.
     *
     * @var array
     */
    public $helpers = ['Html', 'Bs'];

    // @deprecated use HtmlHelper::tag()
    public function d($textContent, $classAttribute = null)
    {
        return $this->Html->tag('div', $textContent, ['class' => $classAttribute]);
    }

    // @deprecated use HtmlHelper::tag()
    public function t($tagName, $textContent, $classAttribute = null)
    {
        return $this->Html->tag($tagName, $textContent, ['class' => $classAttribute]);
    }

    /**
     * ...
     *
     * @param string $textTitle Le texte précédant le lien du dépliant
     * @param string $linkTitle Le texte du lien du dépliant
     * @param string $textContent Le texte contenu par le dépliant (caché)
     * @param string $id L'id du div (caché) contenant le texte du dépliant
     * @param string $classAttribute La classe (optionnelle) du div contenant l'ensemble du dépliant
     * @return string
     */
    public function depliant($textTitle, $linkTitle, $textContent, $id, $classAttribute = null)
    {
        $id = substr(md5(rand()), 0, 7);
        $link = $this->Html->link($linkTitle, '#', ['style' => 'cursor: pointer;', 'onclick' => "javascript:$(\"#{$id}\").toggle(); return false;"]);
        return $this->Html->tag('div', "{$textTitle} {$link}", ['class' => $classAttribute])
                . $this->Html->tag('div', $textContent, ['id' => $id, 'style' => 'display:none;']);
    }

    /**
     * Retourne les éléments de liste dont la valeur de la clé "okko" est "ok".
     *
     * @param array $liste Une liste de vérifications contenant les clés
     * 	"libelle", "okko" et "message"
     * @return array
     */
    protected function _listeOk(array $liste)
    {
        return Hash::extract($liste, '{n}[okko=/ok/]');
    }

    /**
     * Retourne les éléments de liste dont la valeur de la clé "okko" est "ko".
     *
     * @param array $liste Une liste de vérifications contenant les clés
     * 	"libelle", "okko" et "message"
     * @return array
     */
    protected function _listeKo(array $liste)
    {
        return Hash::extract($liste, '{n}[okko=/ko/]');
    }

    /**
     * Retourne les éléments de liste dont la valeur de la clé "okko" est "info".
     *
     * @param array $liste Une liste de vérifications contenant les clés
     * 	"libelle", "okko" et "message"
     * @return array
     */
    protected function _listeInfo(array $liste)
    {
        return Hash::extract($liste, '{n}[okko=/info/]');
    }

    /**
     * ...
     *
     * @param array $liste Une liste de vérifications contenant les clés
     * 	"libelle", "okko" et "message"
     * @param string $idDepliant L'id du dépliant le cas échéant
     * @return string
     */
    public function liste(array $liste = null, $idDepliant = 'idDepliant')
    {

        if (!is_null($liste)) {
            $result = '';
            $listesanscles = array_values($liste);
            $listeOk = $this->_listeOk($listesanscles);
            if (!empty($listeOk)) {
                $libellesliste = Hash::extract($listeOk, '{n}.libelle');
                $listeaffichee = implode(', ', $libellesliste);
                $result .= $this->Html->tag('div', $this->Bs->icon('check-square') .' '. $listeaffichee, ['style'=>'background: #DFF0D8']);
            }


            $listeInfo = $this->_listeInfo($listesanscles);
            if (!empty($listeInfo)) {
                $libellesliste = Hash::extract($listeInfo, '{n}.libelle');
                $listeaffichee = implode(', ', $libellesliste);
                $result .= $this->Html->tag('div', $this->Bs->icon('info-circle') .' '. $listeaffichee, ['style'=>'background: #D9EDF7']);
            }

            $listeKo = $this->_listeKo($listesanscles);
            if (!empty($listeKo)) {
                $libellesliste = Hash::extract($listeKo, '{n}.libelle');
                $messageliste = Hash::extract($listeKo, '{n}.message');
                $message = implode('<br/>', $messageliste);
                $listeaffichee = implode(', ', $libellesliste);
                if ($message != '') {
                    $result .= $this->depliant($listeaffichee, 'plus d\'info', $message, $idDepliant, 'ko');
                    return $result;
                }
//                $this->Bs->icon('cog', null, ['stylePrefix' => 'fa'])
                $result .= $this->Html->tag('div', $this->Bs->icon('times') .' '. $listeaffichee, ['style'=>'background: #F2DEDE']);
            }

            return $result;
        }
    }

}
