<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('HtmlHelper', 'View/Helper');
App::uses('Inflector', 'Utility');

/**
 *
 * Helper CakePHP to create Twitter Bootstrap elements
 * @author AWL
 *
 */
class BsHelper extends HtmlHelper
{
    /**
     * The name of the helper
     *
     * @var string
     */
    public $name = 'Bs';

    /* -----------------------------------------
     *
     * 			CONFIG
     *
     * ---------------------------------------- */

    /**
     * Path for Bootstrap CSS
     *
     * @var string
     */
    public $pathCSS = '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css';

    /**
     * Path for Font Awesome
     *
     * @var string
     */
    public $faPath = '//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css';

    /**
     * Path for Bootstrap addon
     *
     * @var string
     */
    public $bsAddonPath = 'BsHelpers.bs_addon';

    /**
     * Path for CSS datapicker bootstrap
     *
     * @var string
     */
    public $dpCssPath = 'BsHelpers.datepicker';

    /**
     * If Font Awesome is loaded
     * @var bool
     */
    public $faLoad = true;

    /**
     * If Bootstrap addon is loaded
     * @var bool
     */
    public $bsAddonLoad = true;

    /**
     * If Datepicker Bootrstrap is loaded
     * @var bool
     */
    public $dpLoad = false;

    /**
     * Prefix version for Font Awesome
     * @var bool
     */
    public $faPrefix = 'fa';

    /**
     * Path for JS bootstrap
     *
     * @var string
     */
    public $pathJS = '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js';

    /**
     * Path for JQuery
     *
     * @var string
     */
    public $pathJquery = 'http://code.jquery.com/jquery-1.11.1.min.js';

    /**
     * Path for JS datapicker bootstrap
     *
     * @var string
     */
    public $dpJsPath = 'BsHelpers.datepicker';

    /* ---------------------------------------------*
     * 						    *
     * 			LAYOUT                      *
     * 					            *
     * -------------------------------------------- */

    /**
     * Initialize an HTML document and the head
     *
     * @param string $titre The name of the current page
     * @param string $description The description of the current page
     * @param string $lang The language of the current page. By default 'fr' because we are french
     * @return string
     */
    public function html($titre = '', $description = '', $lang = 'fr')
    {
        $out = '<!DOCTYPE html>';
        $out .= '<html lang="' . $lang . '">';
        $out .= '<head>';
        $out .= '<meta charset="utf-8">';
        $out .= '<title>' . $titre . '</title>';
        $out .= '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
        $out .= '<meta name="description" content="' . $description . '">';

        return $out;
    }

    /**
     * Initialize an HTML 5 document and the head
     *
     * @param string $titre The name of the current page
     * @param string $description The description of the current page
     * @param string $lang The language of the current page. By default 'fr' because we are french
     * @return string
     */
    public function html5($titre = '', $description = '', $lang = 'fr')
    {
        $out = $this->html($titre, $description, $lang);

        // Script JS for IE and HTML 5
        $out .= '<!--[if lt IE 9]>';
        $out .= '<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>';
        $out .= '<![endif]-->';

        return $out;
    }

    /**
     * Close the head element and initialize the body element
     *
     * @param string $classBody Class for the body element
     * @return string
     */
    public function body($classBody = '')
    {
        $out = '</head>';
        $out .= ($classBody != '') ? '<body class="' . $classBody . '">' : '<body>';
        return $out;
    }

    /**
     * Close the body element and the html element
     *
     * @return string
     */
    public function end()
    {
        $out = '</body>';
        $out .= '</html>';

        return $out;
    }

    /**
     * Load CSS for the current page
     *
     * @param array $path Names of CSS for the current page
     * @param array $options Options for the css element
     * @return string A link tag for the head element
     */
    public function css($path = [], $options = [])
    {
        $out = parent::css($this->pathCSS);
        if ($this->faLoad) {
            $out .= parent::css($this->faPath);
        }
        if ($this->bsAddonLoad) {
            $out .= parent::css($this->bsAddonPath);
        }
        if ($this->dpLoad) {
            $out .= parent::css($this->dpCssPath);
        }

        // Others CSS
        foreach ($path as $css) {
            $out .= parent::css($css, $options);
        }

        return $out;
    }

    /**
     * Load JS for the current page
     *
     * @param array $arrayJs Names of JS for the current page
     * @return string A script tag for the head element
     */
    public function js($arrayJs = [])
    {
        $out = parent::script($this->pathJquery);
        $out .= parent::script($this->pathJS);
        if ($this->dpLoad) {
            $out .= parent::script($this->dpJsPath);
        }

        // Others JS
        foreach ($arrayJs as $js) {
            $out .= parent::script($js);
        }

        return $out;
    }

    /**
     * Close div elements
     *
     * @param int $nb Number of div you want to close
     * @return string End tags div
     */
    public function close($nb = 1)
    {
        $out = '';
        for ($i = 0; $i < $nb; $i++) {
            $out .= '</div>';
        }
        return $out;
    }

    /**
     * Open a header element
     *
     * @param array $options Options of the header element
     * @return string Tag header
     */
    public function header($options = [])
    {
        $out = parent::tag('header', null, $options);
        return $out;
    }

    /**
     * Close the header element
     *
     * @return string End tag header
     */
    public function closeHeader()
    {
        return '</header>';
    }

    /* ---------------------------------*
     * 					*
     * 			GRID            *
     * 					*
     * -------------------------------- */

    /**
     * Open a Bootstrap container
     *
     * @param array $options Options of the div element
     * @return string Div element with the class 'container'
     */
    public function container($options = [])
    {
        $out = '';
        $class = '';
        $classfirst = 'container';
        if (isset($options['class'])) {
            if (substr($options['class'], 0, 1) == '-') {
                $classfirst.= $options['class'];
            } else {
                $class .= ' ' . $options['class'];
            }
        }
        $out .= parent::div($classfirst . (!empty($class) ? $class : ''), null, $options);
        return $out;
    }

    /**
     * Open a Bootstrap row
     *
     * @param array $options Options of the div element
     * @return string Div element with the class 'row'
     */
    public function row($options = [])
    {
        $out = '';
        $class = 'row';
        if (isset($options['class'])) {
            $class .= ' ' . $options['class'];
        }
        $out .= parent::div($class, null, $options);
        return $out;
    }

    /**
     * Create a <div class="col"> element.
     *
     * Differents layouts with options.
     *
     * ### Construction
     *
     * $this->Bs->col('xs3 of1 ph9', 'md3');
     *
     * It means : - For XS layout, a column size of 3, offset of 1 and push of 9.
     * 			  - For MD layout, a column size of 3.
     *
     * You can give all parameters you want before $attributes. The rule of params is :
     *
     * 'LAYOUT+SIZE OPTIONS+SIZE'
     *
     * LAYOUT -> not obligatory for the first param ('xs' by default) .
     * SIZE -> size of the column in a grid of 12 columns.
     * OPTIONS -> Not obligatory. Offset, push or pull. Called like this : 'of', 'ph' or 'pl'.
     * SIZE -> size of the option.
     *
     *
     * ### Attributes
     *
     * Same options that HtmlHelper::div();
     *
     * @return string DIV tag element
     */
    public function col()
    {
        $class = '';
        $devices = [];
        $attributes = [];

        $args = func_get_args();
        foreach ($args as $arg) {
            if (!is_array($arg)) {
                $devices[] = $arg;
            } else {
                $attributes = $arg;
            }
        }

        $arrayDevice = ['xs', 'sm', 'md', 'lg'];
        $arrayOptions = ['of', 'ph', 'pl'];

        foreach ($devices as $device) {
            $ecran = null;
            $taille = null;
            $opt = null;
            $replace = ['(', ')', '-', '_', '/', '\\', ';', ',', ':', ' '];
            $device = str_replace($replace, '.', $device);
            $device = explode('.', $device);

            // Sould define the device in first
            foreach ($device as $elem) {
                if (!$ecran) {
                    $nom = substr($elem, 0, 2);
                    if (in_array($nom, $arrayDevice, true)) {
                        $ecran = $nom;
                        $taille = substr($elem, 2);
                    }
                } else {
                    if ($opt) {
                        $opt .= ' ' . $this->__optCol($elem, $ecran);
                    } else {
                        $opt = $this->__optCol($elem, $ecran);
                    }
                }
            }
            if (isset($ecran) && $taille) {
                if ($opt) {
                    $class .= 'col-' . $ecran . '-' . $taille . ' ' . $opt . ' ';
                } else {
                    $class .= 'col-' . $ecran . '-' . $taille . ' ';
                }
            }
        }
        $class = substr($class, 0, - 1);
        if (isset($attributes['class'])) {
            $class .= ' ' . $attributes['class'];
        }
        $out = parent::div($class, null, $attributes);
        return $out;
    }

    /**
     * Complementary function with BsHelper::col()
     *
     * Add the correct class for the option in parameter
     *
     * @param array $elem // class apply on the col element {PARAMETRE OBLIGATOIRE}
     * @param string $screen // layout {PARAMETRE OBLIGATOIRE}
     * @return string The class corresponding to the option
     */
    private function __optCol($elem, $screen)
    {
        $attr = substr($elem, 0, 2);
        $size = substr($elem, 2);
        $res = null;
        if (is_integer($size) || !($size == 0 && $screen == 'sm')) {
            switch ($attr) {
                case 'pl':
                    $res = 'col-' . $screen . '-pull-' . $size;
                    break;

                case 'ph':
                    $res = 'col-' . $screen . '-push-' . $size;
                    break;

                case 'of':
                    $res = 'col-' . $screen . '-offset-' . $size;
                    break;
                default:
                    $res = null;
                    break;
            }
        }
        return $res;
    }

    /* -----------------------------------------*
     * 						*
     * 			TABLES                  *
     * 					        *
     * ---------------------------------------- */

    /**
     * Number of column
     *
     * @var int
     */
    protected $_nbColumn = 0;

    /**
     * Visibility of cells
     *
     * @var array
     */
    protected $_tableClassesCells = [];

    /**
     * Visibility of rows
     *
     * @var array
     */
    protected $_tableAttributesLine = [];

    /**
     * Position of the cell
     *
     * @var int
     */
    protected $_cellPos = 0;

    /**
     * To know if a line is open or not
     *
     * @var bool
     */
    protected $_openLine = 0;

    /**
     * Initialize the table with the head and the body element.
     *
     * @param array $titles 'title' => title of the cell
     * 'width' => width in percent of the cell
     * 'hidden' => layout
     * @param array $class classes of the table (hover, striped, etc)
     * @param array $options classes of the table (hover, striped, etc)
     * @return string
     */
    public function table($titles, $class, $options = [])
    {
        $classes = '';
        $attributes = '';
        $out = '<div class="table-responsive">';

        if (!empty($class)) {
            foreach ($class as $opt) {
                $classes .= ' table-' . $opt;
            }
        }
        if (!empty($options['class'])) {
            foreach ($options['class'] as $opt) {
                $classes .= ' ' . $opt;
            }
        }
        if (!empty($options['attributes'])) {
            foreach ($options['attributes'] as $key => $value) {
                $attributes .=' ' . $key . '="' . $value . '"';
            }
        }
        $out .= '<table class="table' . $classes . '"' . $attributes . '>';
        if (!empty($options['caption'])) {
            $out .='<caption>' . $options['caption'] . '</caption>';
        }

        if ($titles != null) {
            $out .= '<thead>';
            $out .= '<tr>';

            $tableClassesCells = [];
            $tablePos = 0;
            $nbColumn = count($titles);
            $width = false;

            foreach ($titles as $title) {
                $classVisibility = '';
                if (isset($title['hidden'])) {
                    foreach ($title['hidden'] as $h) {
                        $classVisibility .= ($classVisibility != '') ? ' ' : '';
                        $classVisibility .= 'hidden-' . $h;
                    }
                }
                $tableClassesCells[$tablePos] = $classVisibility;

                $out .= '<th class="';
                if (isset($title['width'])) {
                    $out .= 'l_' . $title['width'];
                    if (!$width) {
                        $width = true;
                    }
                }
                $out .= (!empty($title['class'])) ? ' ' . $title['class'] : '';
                $out .= ($classVisibility != '') ? ' ' : '';
                $out .= $classVisibility . '">' . $title['title'] . '</th>';
                $tablePos ++;
            }

            $out .= '</tr>';
            $out .= '</thead>';
            $out .= '<tbody>';

            $this->_nbColumn = $nbColumn - 1;
            $this->_tableClassesCells = $tableClassesCells;
            $this->_cellPos = 0;
            $this->_openLine = 0;
        }
        return $out;
    }

    /**
     * Create a cell (<td>)
     *
     * @param string $content Informations in the cell
     * @param string $class Classe(s) of the cell
     * @param string $options HTML attributes
     * @param bool $autoformat Close or not the cell when it is the last of the line
     * @return string
     */
    public function cell($content, $class = '', $options = [], $autoformat = true)
    {
        $out = '';
        $classVisibility = '';
        $cellPos = $this->_cellPos;

        if ($cellPos == 0 && $this->_openLine == 0) {
            $out .= '<tr';
            foreach ($this->_tableAttributesLine as $key => $value) {
                $out .=' ' . $key . '="' . $value . '"';
            }
            $out .= '>';
        }

        //$this->_openLine = 0;

        if (isset($this->_tableClassesCells[$cellPos])) {
            $classVisibility = $this->_tableClassesCells[$cellPos];
        }

        if ($classVisibility != '' || $class != '') {
            $out .= '<td class="' . $class;
            $out .= ($class != '') ? ' ' : '';
            $out .= $classVisibility . '"';
        } else {
            $out .= '<td';
        }

        foreach ($options as $key => $value) {
            $out .= ' ' . $key . '="' . $value . '"';
        }
        $out .= '>';

        $out .= $content;

        if ($autoformat) {
            $out .= '</td>';

            if ($cellPos == $this->_nbColumn) {
                $out .= '</tr>';
                $this->_cellPos = 0;
            } else {
                $this->_cellPos = $cellPos + 1;
            }
        } else {
            if ($cellPos == $this->_nbColumn) {
                $this->_cellPos = 0;
            } else {
                $this->_cellPos = $cellPos + 1;
            }
        }
        return $out;
    }

    /**
     * Color a line (<tr>)
     *
     * @param string $color Colorof the line (active, warning, danger or success)
     * @return string
     */
    public function lineColor($color)
    {
        $out = '<tr class="' . $color . '">';
        $this->_openLine = 1;
        return $out;
    }

    /**
     * Creation du panel
     *
     * @param string $title set panel title
     * @return string
     */
    public function panel($title = "&nbsp;")
    {
        $out = parent::div('panel panel-default') .
                parent::div('panel-heading', $title) .
                parent::div('panel-body');
        return $out;
    }

    public function endPanel()
    {
        return $this->close(2);
    }

    /**
     * Set number of column for a table (<tr>)
     *
     * @param string $value set and override the table colum number
     * @return string
     */
    public function setTableNbColumn($value)
    {
        $this->_nbColumn = $value;
    }

    /**
     * Attributes for a line (<tr>)
     *
     * @param array $attributes for the line
     * @return string
     */
    public function lineAttributes($attributes)
    {
        $this->_tableAttributesLine = $attributes;
    }

    /**
     * Attributes for a line (<tr>)
     *
     * @param array $attributes for the line
     * @return string
     */
    public function linePosition($position)
    {
        $this->_cellPos = $position;
    }

    /**
     * Close the table
     *
     * @return string
     */
    public function endTable()
    {
        $out = '</tbody>';
        $out .= '</table>';
        $out .= '</div>';
        return $out;
    }

    /* ---------------------------------*
     * 					*
     * 			OTHERS          *
     * 					*
     * -------------------------------- */

    /**
     * Create a bootstrap alert element.
     *
     * @param string $text    Alert content
     * @param string $state   Bootstrap state
     * @param array  $options HTML attributes
     *
     * @return string
     */
    public function alert($text, $state, $options = [])
    {
        if (!isset($options['class'])) {
            $options['class'] = 'alert alert-' . $state;
        } else {
            $options['class'] = 'alert alert-' . $state . ' ' . $options['class'];
        }
        if (!isset($options['dismiss']) || $options['dismiss'] == 'true') {
            $dismiss = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
        }
        unset($options['dismiss']);
        $out = '<div ';
        foreach ($options as $key => $value) {
            $out .= ' ' . $key . '="' . $value . '"';
        }
        $out .= '>';
        if (isset($dismiss)) {
            $out .= $dismiss;
        }
        $out .= $text;
        $out .= '</div>';
        return $out;
    }

    /**
     * Picture responsive
     *
     * Extends from HtmlHelper:image()
     *
     * @param string $path Path to the image file, relative to the app/webroot/img/ directory.
     * @param array $options Array of HTML attributes. See above for special options.
     * @return string End tag header
     */
    public function image($path, $options = [])
    {
        if (isset($options['class'])) {
            $options['class'] = 'img-responsive ' . $options['class'];
        } else {
            $options['class'] = 'img-responsive';
        }
        return parent::image($path, $options);
    }

    /**
     * Create a Font Awesome Icon
     *
     * @param string $iconLabel label of the icon
     * @param array $classes like 'fixed-width', 'large', '2x', etc.
     * @param array $attributes more attributes for the tag
     * @return string
     */
    public function icon($iconLabel, $classes = [], $attributes = [])
    {
        $class = '';
        $more = '';
        $prefix = $this->faPrefix;
        $prefixType = '';

        isset($classes['iconType']) &&  !empty($classes['iconType']) ? $prefix = $classes['iconType'] : null;
        // if (isset($classes['iconType'])) {
        //     $prefix = !empty($classes['iconType']) ? $classes['iconType'] : '';
        //     //unset($classes['iconType']);
        // }

        if (!empty($classes)) {
            foreach ($classes ?? [] as $opt) {
                $class .= ' ' . $prefix . '-' . $opt;
            }
        }

        if (!empty($attributes)) {
            if (!empty($attributes['class'])) {
                $class .= ' ' . $attributes['class'];
            }
            unset($attributes['class']);
            if (!empty($attributes['stylePrefix'])) {
                $prefixType = $attributes['stylePrefix'];
            }
            unset($attributes['stylePrefix']);
            foreach ($attributes as $key => $attr) {
                $more .= ' ' . $key . '="' . $attr . '"';
            }
        }
        if (!empty($prefixType)) {
            return '<span class="' . $prefixType . ' ' . $prefix. '-' . $iconLabel . $class . '"' . $more . '></span>';
        }

        return '<span class="' . $prefix . ' ' . $prefix. '-' . $iconLabel . $class . '"' . $more . '></span>';
    }

    /**
     * Create a Bootstrap Button or Link
     *
     * @param string $text text in the button
     * @param string $url url of the link
     * @param array $options 'size' => lg, sm or xs, to change the size of the button
     * 						 'type' => primary, success, etc, to change the color
     * 						 'tag' => to change the tag
     * 						 and more... (like 'class')
     * @param array $confirmMessage to add a confirm pop-up
     * @return string
     */
    public function btn($text, $url = [], $options = [], $confirmMessage = false)
    {
        $tag = 'a';
        if (!empty($options['tag'])) {
            $tag = $options['tag'];
        }

        if (!empty($options['icon'])) {
            $icon = '';
            if (substr($options['icon'], 0, 9) == 'fa') {
                $icon = '<i class="' . ($options['icon']) . '"></i>';
            } else {
                $icon = $this->icon($options['icon']);
            }

            $options['escapeTitle'] = false;
            $text = $icon . (!empty($text) ? ' ' . $text : '');
        }

        $class = 'btn';
        $class .= (!empty($options['type'])) ? ' btn-' . $options['type'] : '';
        $class .= (!empty($options['size'])) ? ' btn-' . $options['size'] : '';
        $class .= (isset($options['class'])) ? ' ' . $options['class'] : '';
        $options['class'] = $class;

        if ($tag != 'a') {
            unset($options['tag']);
            unset($options['type']);
            unset($options['size']);
            if (!empty($options['option_type'])) {
                $options['type'] = $options['option_type'];
                unset($options['option_type']);
            }
        }

        if ($tag != 'a') {
            return parent::tag($tag, $text, $options);
        } else {
            //ancienne méthode avec probleme encode utf8_encode
            //return parent::link($text, $url, $options, $confirmMessage);
            if ($confirmMessage) {
                $options['confirm'] = $confirmMessage;
                unset($confirmMessage);
            }

            return parent::link($text, $url, $options);
        }
    }

    /**
     * Create a Bootstrap Modal.
     *
     * @param array $titles The content of the body
     * @param array $options Used to add custom ID, class or a form into the modal
     * @return string Bootstrap modal
     */
    public function tab($titles, $options = [])
    {
        if (!(isset($options['id']) && $options['id'] != '')) {
            /*$options['id'] = $options['id'];
        } else {*/
            $cle1 = "zarnfjdlvjezprizejrjpzojazjpodffp";
            $cle2 = "251848416487764197191944948794449";
            $cle = '';
            for ($i = 0; $i < 15; $i++) {
                $tab = [$cle1, $cle2];
                if ($i == 0) {
                    $cle .= $cle1[rand(0, strlen($cle1) - 1)];
                } else {
                    $t = $tab[rand(0, 1)];
                    $cle .= $t[rand(0, strlen($t) - 1)];
                }
            }
            $options['id'] = $cle;
        }

        $classfirst = 'nav';
        $class = $classfirst . ' nav-tabs';
        if (!empty($options['class'])) {
            if (is_array($options['class'])) {
                foreach ($class as $opt) {
                    if (substr($opt, 0, 1) == '-') {
                        $class .= ' ' . $classfirst . $options['class'];
                    } else {
                        $class .= ' ' . $options['class'];
                    }
                }
            } else {
                if (substr($options['class'], 0, 1) == '-') {
                    $class .= ' ' . $classfirst . $options['class'];
                } else {
                    $class .= ' ' . $options['class'];
                }
            }
        }
        $out = '<!-- Nav tabs -->';
        $out .=$this->tag('ul', null, ['id' => $options['id'], 'class' => $class, 'role' => 'tablist']);
        foreach ($titles as $key => $title) {
            $out .=$this->tag('li', null, (!empty($options['active']) && $options['active'] == $key ? ['class' => 'active'] : []));
            $out .=$this->link($title, '#' . $key, ['role' => 'tab', 'data-toggle' => 'tab']);
            $out .=$this->tag('/li', null);
        }
        $out .=$this->tag('/ul', null);

        /* $out .=$this->scriptBlock('
          $(\'#'.$options['id'].' a\').click(function (e) {
          e.preventDefault()
          $(this).tab(\'show\')
          })'); */


        return $out;
    }

    /**
     *
     * @param type $options
     * @return type
     */
    public function tabContent($options = [])
    {
        return '<!-- Tab panes -->' . parent::div('tab-content');
    }

    /**
     *
     * @param type $title
     * @param type $options
     * @return type
     */
    public function tabPane($title, $options = [])
    {
        $class = 'tab-pane';
        if (!empty($options['class'])) {
            if (is_array($options['class'])) {
                foreach ($class as $opt) {
                    $class .= ' ' . $options['class'];
                }
            } else {
                $class .= ' ' . $options['class'];
            }
        }

        return '<!-- Panes -->' . parent::div($class, null, ['id' => $title]);
    }

    /**
     *
     * @return type
     */
    public function tabPaneClose()
    {
        return parent::tag('/div') . '<!-- Tab panes close -->';
    }

    /**
     *
     * @return type
     */
    public function tabClose()
    {
        return parent::tag('/div') . '<!-- Tab close -->';
    }

    /**
     * Create a Bootstrap Modal.
     *
     * @param string $header The text in the header
     * @param string $body The content of the body
     * @param array $options Used to add custom ID, class or a form into the modal
     * @param array $buttons Informations about open, close and confirm buttons
     * @return string Bootstrap modal
     */
    public function modal($header, $body, $options = [], $buttons = [])
    {
        $classes = (isset($options['class'])) ? $options['class'] : '';
        // Is it a form ?
        $form = (isset($options['form']) && $options['form'] == true) ? true : false;

        // If it's a form then there is a submit button
        $type = ($form) ? 'submit' : 'button';

        $data = '';
        if (!empty($options['data'])) {
            foreach ($options['data'] as $data_key => $data_value) {
                $data .= $data_key . '="'.$data_value.'" ';
            }
            unset($options['data']);
        }

        // Generate a random id if it doesn't exist
        if (isset($options['id']) && $options['id'] != '') {
            $id = $options['id'];
        } else {
            $cle1 = "zarnfjdlvjezprizejrjpzojazjpodffp";
            $cle2 = "251848416487764197191944948794449";
            $cle = '';
            for ($i = 0; $i < 15; $i++) {
                $tab = [$cle1, $cle2];
                if ($i == 0) {
                    $cle .= $cle1[rand(0, strlen($cle1) - 1)];
                } else {
                    $t = $tab[rand(0, 1)];
                    $cle .= $t[rand(0, strlen($t) - 1)];
                }
            }
            $id = $cle;
        }

        // Create the open button
        $out ='';
        if (!empty($buttons)) {
            if (isset($buttons['open']) && $buttons['open'] != '') {
                if (is_array(($buttons['open']))) {
                    // Create a simple font-awesome icon instead of a button
                    if (isset($buttons['open']['button']) && $buttons['open']['button'] === false) {
                        $out = $this->icon($buttons['open']['name'], ['open-modal'], ['data-toggle' => 'modal', 'data-target' => '#' . $id]);
                    } elseif(isset($buttons['open']['name']) && isset($buttons['open']['class'])) {
                        $out = $this->btn(__($buttons['open']['name']), null, array_merge($buttons['open']['options'], ['tag' => 'button', 'class' => $buttons['open']['class'], 'data-toggle' => 'modal', 'data-target' => '#' . $id]));
                    }
                } else {
                    $out = $this->btn(__($buttons['open']), null, ['tag' => 'button', 'class' => 'btn-primary btn-lg', 'data-toggle' => 'modal', 'data-target' => '#' . $id]);
                }
            } else {
                $out = $this->btn(__('Voir'), null, ['tag' => 'button', 'class' => 'btn-primary btn-lg', 'data-toggle' => 'modal', 'data-target' => '#' . $id]);
            }
        } else {
            $out = $this->btn(__('Voir'), null, ['tag' => 'button', 'class' => 'btn-primary btn-lg', 'data-toggle' => 'modal', 'data-target' => '#' . $id]);
        }

        // Modal
        $out .= '<div class="modal fade ' . $classes . '" id="' . $id . '" tabindex="-1" role="dialog" aria-labelledby="' . $id . 'Label" aria-hidden="true" ' . $data . '>';
        $out .= '<div class="modal-dialog' . (isset($options['size']) ? ' modal-' . $options['size'] : '') . '">';

        // Content
        $out .= '<div class="modal-content">';

        if ($form) {
            $close = strpos($body, '>');
            $out .= substr($body, strpos($body, '<form'), $close + 1);
            $body = substr($body, $close + 1, strpos($body, '</form>') - ($close + 1));
        }

        // Header
        $out .= '<div class="modal-header">';
        $out .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
        $out .= '<h4 class="modal-title" id="' . $id . 'Label">' . $header . '</h4>';
        $out .= '</div>';
        // End header
        // Body
        $out .= '<div class="modal-body">';
        // if the body dont begin by a html tag
        $out .= (strpos($body, '<') !== 0) ? '<p>' . $body . '</p>' : $body;
        $out .= '</div>';
        // End body
        // Footer
        $outFooter = '';
        if (!empty($buttons)) {
            if (isset($buttons['close'])) {

                // If the value of 'close' is an array (like 'close' => array('name' => 'Close') )
                if (is_array($buttons['close']) && isset($buttons['close']['url'])) {
                    $outFooter .= $this->btn(
                        __($buttons['close']['name']),
                        Router::url($buttons['close']['url']),
                        [
                        'type' => 'default',
                        'class' => $buttons['close']['class']]
                    );
                } elseif (is_array($buttons['close'])) {
                    $outFooter .= $this->btn(__($buttons['close']['name']), null, ['tag' => 'button', 'class' => $buttons['close']['class'], 'data-dismiss' => 'modal']);
                } else {
                    $outFooter .= $this->btn(__($buttons['close']), null, ['tag' => 'button', 'class' => 'btn', 'data-dismiss' => 'modal']);
                }
            // If 'close' index exist => create the button
            } elseif (in_array('close', $buttons, true)) {
                $outFooter .= $this->btn('<span class="fa fa-times-circle"></span>'. ' ' .__('Annuler'), null, ['tag' => 'button', 'class' => 'btn', 'data-dismiss' => 'modal']);
            }
            if (isset($buttons['confirm'])) {
                if (!isset($buttons['confirm']['options'])) {
                    $buttons['confirm']['options'] = [];
                }
                // Check if it's a form
                if ($form) {
                    $class = (isset($buttons['confirm']['class'])) ? $buttons['confirm']['class'] : 'btn-success';
                    $outFooter .= $this->btn(__($buttons['confirm']['name']), null, array_merge($buttons['confirm']['options'], ['tag' => 'button', 'class' => $class, 'type' => $type]));
                } else {
                    $class = (isset($buttons['confirm']['class'])) ? $buttons['confirm']['class'] : 'btn-success';
                    $outFooter .= $this->btn(
                        __($buttons['confirm']['name']), $buttons['confirm']['link'],
                        array_merge($buttons['confirm']['options'], ['class' => $class])
                    );
                }

            // If 'confirm' index exist => create the button
            } elseif (in_array('confirm', $buttons, true)) {
                $outFooter .= $this->btn(__('Confirmer'), null, ['tag' => 'button', 'class' => 'btn-success', 'type' => $type]);
            }

            if ($outFooter != '') {
                $out .= '<div class="modal-footer">';
                $out .= $outFooter;
                $out .= '</div>';
            }
        }
        $out .= ($form) ? '</form>' : '';
        // End footer

        $out .= '</div>';
        // End Content

        $out .= '</div>';
        $out .= '</div>';

        return $out;
    }

    /**
     * Create a button with a confirm like a Bootstrap Modal
     *
     * @param string $button the name of the button, the header and the confirm button in the modal
     * @param string $link the link to redirect after the confirm
     * @param array $options Options for the confirm (button, texte, header, color)
     * @param array $options Options for the Modal
     * @return string
     */
    public function confirm($button, $link, $options = [], $optionsModal = [])
    {
        $out = '';
        $buttons = [
            'open' => [
                'name' => $button
            ],
            'close',
            'confirm' => [
                'link' => $link
            ]
        ];
        if (!empty($options['icon'])) {
            $icon = '';
            if (substr($options['icon'], 0, 9) == 'fa') {
                $icon = '<span class="' . ($options['icon']) . '"></span>';
            } else {
                $icon = $this->icon($options['icon']);
            }

            $options['escapeTitle'] = false;
            $text = $icon . (!empty($text) ? ' ' . $text : '');
            $buttons['open']['options']['escapeTitle'] = false;
            $buttons['open']['name'] = $icon . (!empty($button) ? ' ' . $button : '');
            unset($options['icon']);
        }

        $buttons['open']['class'] = $buttons['confirm']['class'] = (isset($options['type']) && $options['type'] != '') ? 'btn-' . $options['type'] : 'btn-success';
        $buttons['open']['options']['title'] = !empty($options['title']) ? $options['title'] : '';
        $buttons['confirm']['name'] = (isset($options['button']) && $options['button'] != '') ? $options['button'] : $button;
        $body = (isset($options['texte']) && $options['texte'] != '') ? $options['texte'] : 'Voulez-vous vraiment continuer votre action ?';
        $header = (isset($options['header']) && $options['header'] != '') ? $options['header'] : $button;
        //Fix border-radius
        $buttons['open']['options']['id'] = !empty($options['id']) ? parent::style($options['id']) : null;
        $buttons['open']['options']['style'] = !empty($options['style']) ? parent::style($options['style']) : null;

        return $this->modal($header, $body, $optionsModal, $buttons) . $out;
    }
}
