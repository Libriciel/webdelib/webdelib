<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppHelper', 'View/Helper');


class LockHelper extends AppHelper
{
    public $helpers = ['Session'];

    public function lockStyle($projet)
    {
        if ($projet['isLocked'] && in_array((int)$this->Session->read('Auth.User.Profil.role_id'), [2], true)) {
            return 'opacity: .65';
        }

        return '';
    }
    public function lockButton($projet)
    {
        if ($projet['isLocked'] && in_array((int)$this->Session->read('Auth.User.Profil.role_id'), [1,3], true)) {
            return 'button';
        }

        return '';
    }
    public function lockDisabled($projet)
    {
        if ($projet['isLocked'] && in_array((int)$this->Session->read('Auth.User.Profil.role_id'), [1,3], true)) {
            return 'disabled';
        }

        return null;
    }

    public function lockConfirm($projet)
    {
        if ($projet['isLocked'] && $this->Session->read('Auth.User.Profil.role_id') == 2) {
            return __('Souhaitez-vous enlever le verrou sur ce projet ?');
        }

        return null;
    }

    public function lockTag($projet)
    {
        if ($projet['isLocked'] && in_array((int)$this->Session->read('Auth.User.Profil.role_id'), [1,3], true)) {
            return 'button';
        }

        return null;
    }

    public function lockTitle($projet, $text)
    {
        if ($projet['isLocked']) {
            $lock_user_name = $projet['lock_user_name'];
            $lock_last_time = $projet['lock_last_time'];

            if ($this->Session->read('Auth.User.Profil.role_id')== 2) {
                return __('Enlever le verrou sur le projet (%s depuis le %s)', $lock_user_name, $lock_last_time);
            }
            if (in_array((int)$this->Session->read('Auth.User.Profil.role_id'), [1,3], true)) {
                return __('Projet en cours d\'édition par (%s) depuis le %s.', $lock_user_name, $lock_last_time);
            }
        }

        return $text;
    }
}
