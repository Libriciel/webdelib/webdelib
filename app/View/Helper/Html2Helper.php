<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class Html2Helper extends HtmlHelper
{
    public $helpers = ['Form', 'Bs', 'BsForm'];

    /**
     *
     * @param type $value
     * @param type $title
     * @param type $action
     * @param type $class
     * @param type $id
     * @return type
     */
    public function btnCreate($value = "Créer", $title = "Créer", $action = 'add', $class = 'btn-primary', $id = 'bouton_ajouter')
    {
        return $this->Bs->btn($this->Bs->icon('plus') . ' ' . __($value), is_array($action) ? $action : ["action" => $action], ['id' => $id, 'type' => "primary", 'escapeTitle' => false, 'title' => $title]);
    }

    /**
     *
     * @param type $value
     * @param type $title
     * @param type $action
     * @param type $class
     * @param type $id
     * @return type
     */
    public function btnAdd($value = "Ajouter", $title = "Ajouter", $action = 'add', $class = 'btn-primary', $id = 'bouton_ajouter')
    {
        return $this->Bs->btn($this->Bs->icon('plus-circle') . ' ' . __($value), is_array($action) ? $action : ["action" => $action], ['id' => $id, 'type' => "primary", 'escapeTitle' => false, 'title' => $title]);
    }

    /**
     *
     * @param type $action
     * @param type $type
     * @param type $value
     * @return type
     */
    public function btnCancel($action = "index", $type = 'default', $value = "Retour")
    {
        return $this->Bs->btn($this->Bs->icon('arrow-left') . ' ' . __($value), is_array($action) ? $action : ["action" => $action], ['type' => $type, 'escapeTitle' => false, 'title' => __('Revenir à la page précédente')]);
    }

    /**
     *
     * @param type $action
     * @param type $type
     * @param type $value
     * @return type
     */
    public function btnCancelSend($action = "index", $type = 'default', $value = "Annuler")
    {
        return $this->Bs->btn(
            $this->Bs->icon('times-circle') . ' ' . __($value),
            is_array($action) ? $action : ["action" => $action],
            [
            'type' => $type,
            'escapeTitle' => false,
            'title' => __('Annuler les modifications')]
        );
    }

    /**
     *
     * @param type $onclick
     * @param type $urlCancel
     * @param type $titleSave
     * @param type $valueSave
     * @param type $urlSave
     * @return type
     */
    public function btnSaveCancel($onclick = '', $urlCancel = "javascript:history.go(-1)", $titleSave = "Enregistrer", $valueSave = 'Enregistrer', $urlSave = null)
    {
        return $this->Bs->div('form-group').$this->Bs->div('col-md-offset-' . $this->BsForm->getLeft() . ' col-md-' . $this->BsForm->getRight()) .
        $this->Bs->div(
            'btn-group',
            $this->Bs->btn($this->Bs->icon('times-circle') . ' ' . __('Annuler'), $urlCancel, [
                    'type' => 'default',
                    'id' => 'boutonAnnuler',
                    'escapeTitle' => false,
                    'title' => __('Annuler les modifications')]) .
                $this->Bs->btn($this->Bs->icon('save') . ' ' . __($valueSave), $urlSave, [
                    'tag' => 'button',
                    'type' => 'success',
                    'option_type' => 'submit',
                    'id' => 'boutonValider',
                    'escapeTitle' => false,
                    'title' => $titleSave,
                    'onclick' => $onclick])
        ) .
                $this->Bs->close(2);
    }
}
