<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppHelper', 'View/Helper');
App::uses('SaeVersement', 'Model');


class SaeHelper extends AppHelper
{
    public $helpers = ['Html','Bs','Session'];

    public function btnEtat($etat, $message)
    {
        $disabled = false;
        $btnIcon='';
        $btnType='default';
        switch ($etat) {
            case SaeVersement::REFUSER:
                $btnIcon='times';
                $btnType='danger';
                break;
            case SaeVersement::ERREUR:
                $btnIcon='exclamation-triangle ';
                $btnType='warning';
                break;
            case SaeVersement::VERSER:
                $btnIcon='clock';
                break;
            case SaeVersement::ACCEPTER:
                $btnIcon='check-square';
                break;
            case SaeVersement::VERSER_MANUELLEMENT:
                $btnIcon='check-square';
                $message = __('Le versement a été effectué manuellement');
                break;
            default:
                $btnIcon='paper-plane';
                $disabled = true;
                break;
        }

        return $this->Bs->btn(
            $this->Bs->icon($btnIcon),
            null,
            [
                'tag' => 'button',
                'option_type' => 'button',
                'data-toggle' => 'popover',
                'data-content' => !empty($message) ?__('%s', $message) : '',
                'data-placement' => 'bottom',
                'disabled' => $disabled,
                'escape' => false,
                'type' => $btnType,
                'escapeTitle' => false,
                'title' => __('Commentaire&nbsp;du&nbsp;versement&nbsp;au&nbsp;SAE')
            ]
        );
    }
}
