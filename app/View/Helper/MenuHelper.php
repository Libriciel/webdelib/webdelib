<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class MenuHelper extends Helper
{
    public $helpers = [
        'Session',
        'Bs',
        'Navbar' => [
            'className' => 'Bootstrap3.BootstrapNavbar'
          ]
        ];
    public $_navbar;

    /**
     *
     * @param type $navbar
     */
    public function createMenuPrincipale(&$navbar)
    {
        $this->_navbar = $navbar;
        $session_navbar=$this->Session->read('Auth.Navbar');
        if ($this->Session->check('Auth.Navbar') && !empty($session_navbar)) {
            foreach ($session_navbar as $key => $menu) {
                $this->_createMenu($key, $menu);
            }
        }
    }

    /**
     *
     * @param type $title
     * @param type $menu
     */
    private function _createMenu($title, $menu)
    {
        $this->_navbar->beginMenu($title, null);
        foreach ($menu['subMenu'] as $key => $value) {
            switch ($value['html']) {
                case 'link':
                    $this->Navbar->link(
                        (
                            !empty($value['icon']) ? $this->Bs->icon($value['icon'], isset($value['iconType']) ? ['iconType'=>$value['iconType']] : null) . ' ' : ''
                        )
                        . $value['libelle'],
                        $value['url'],
                        ['escape' => false,
                        'title' => $value['title']]
                    );

                    break;
                case 'subMenu':
                    foreach ($value['content'] as $keyContent => $content) {
                        $this->_createMenu($keyContent, $content);
                    }
                    break;

                case 'divider':
                    $this->Navbar->divider();
                    break;


                default:
                    break;
            }
        }
        $this->_navbar->endMenu();
    }
}
