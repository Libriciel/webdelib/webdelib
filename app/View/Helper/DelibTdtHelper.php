<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppHelper', 'View/Helper');


class DelibTdtHelper extends AppHelper
{
    public $helpers = ['Html','Time','Bs','Session'];

    /**
     * [retourneEtat description]
     * @param  [type] $deliberation [description]
     * @return [type]               [description]
     *
     * @version 5.1.0
     */
    public function retourneEtat($deliberation)
    {
        switch ($deliberation['Deliberation']['etat']) {
            case 5:
                if ($deliberation['Deliberation']['tdt_status']===1) {
                    // __("Acquittement reçu le %s", $this->Time->i18nFormat($deliberation['Deliberation']['tdt_ar_date'], '%A %d %B %Y'))
                    return $this->Html->link(
                        __("Acquittement reçu le %s", $this->Time->i18nFormat($deliberation['Deliberation']['tdt_ar_date'], '%A %d %B %Y')),
                        ['controller' => 'actes', 'action' => 'getBordereauTdt', $deliberation['Deliberation']['id']],
                        [
                            'title' => __('Télécharger le bordereau d\'acquittement de transaction')]
                    );
                } elseif ($deliberation['Deliberation']['tdt_status']===-2) {
                    return __('Transaction annulée');
                } elseif ($deliberation['Deliberation']['tdt_status']===-1) {
                    $etatDeliberation = "Transaction en erreur";
                    $messageErreur = __('Erreur non identifiée. Veuillez contacter votre administrateur');
                    if (!empty($deliberation['Deliberation']['tdt_message'])) {
                        $messageErreur = $deliberation['Deliberation']['tdt_message'];
                    }
                    $link = $this->Html->link("Détails", '#', ['style' => 'cursor: pointer;', 'onclick' => "javascript:$(\"#".$deliberation['Deliberation']['id']."\").toggle(); return false;"]);
                    return $this->Html->tag('div', "{$etatDeliberation} {$link}", ['class' => "info"])
                        . $this->Html->tag('div', $messageErreur, ['id' => $deliberation['Deliberation']['id'], 'style' => 'display:none;']);
                } elseif ($deliberation['Deliberation']['tdt_status']===-3) {
                    return __('En attente de réception (Erreur de communication avec le Tdt)');
                }

                return __('En attente de réception');
            case 11:
                return __('Déposé(e) manuellement');
        }
    }

    /**
     * [retourneMessages description]
     *
     * les types de document:
     *    2 : Courrier simple;
     *    3 : Demande de pièces complémentaires;
     *    4 : Lettre d'observation;
     *    5 : Déféré au tribunal administratif.
     *
     * les documents envoyés:
     *   -1 : Erreur;
     *    0 : Annulé;
     *    1 : Posté;
     *    2 : En attente de transmission;
     *    3 : Transmis;
     *    4 : Acquittement reçu;
     *    5 : Validé;
     *    6 : Refusé.
     *
     * les documents reçus:
     *    7 :
     *    8 :
     *
     * @param  [type] $deliberation [description]
     * @param  [type] $tdt          [description]
     * @param  [type] $tdt_host     [description]
     * @return [type]               [description]
     */
    public function retourneMessages($deliberation, $tdt, $tdt_host)
    {
        $messages = '';
        if (!empty($deliberation['TdtMessage'])) {
            foreach ($deliberation['TdtMessage'] as $message) {
                $libelle = '';
                $reponse = false;
                switch ($message['tdt_type']) {
                    case 2:
                        $libelle = __("Courrier simple");
                        $reponse = true;
                        break;
                    case 3:
                        $libelle = __("Demande de pièces complémentaires");
                        $reponse = true;
                        break;
                    case 4:
                        $libelle = __("Lettre d'observation");
                        break;
                    case 5:
                        $libelle = __("Déféré au tribunal administratif");
                        break;
                    default:
                        break;
                }
                if (!empty($libelle)) {
                    $messages .= '<div style="white-space: nowrap;">';
                    $messages .= $this->Html->link($libelle . ' ' . $this->Bs->icon('download'), ['controller' => 'actes', 'action' => 'downloadTdtMessage', $message['tdt_id']], ['escape' => false, 'title' => __('Télécharger le document')]);
                    if (empty($message['Reponse']) && $reponse && empty($message['parent_id'])) {
                        $messages .= ' ';
                        //Gestion des réponses Pastell
                        if (!empty($deliberation['Deliberation']['pastell_id']) && $tdt == 'PASTELL') {
                            $coll = $this->Session->read('user.Collectivite');
                            $messages .= $this->Html->link($this->Bs->icon('paper-plane'), $tdt_host . '/document/detail.php?id_d=' . $deliberation['Deliberation']['pastell_id'] . '&id_e=' . $coll['Collectivite']['id_entity'], ['escape' => false, 'target' => '_blank', 'title' => __('Répondre')]);
                        }
                        //Gestion des réponses S2low
                        if (!empty($deliberation['Deliberation']['tdt_id']) && $tdt == 'S2LOW') {
                            $messages .= $this->Html->link($this->Bs->icon('paper-plane'), $tdt_host . '/modules/actes/actes_transac_show.php?id=' . $message['tdt_id'], ['escape' => false, 'target' => '_blank', 'title' => __('Répondre')]);
                        }
                    } else {
                        foreach ($message['Reponse'] as $reponse) {
                            $messages .= '<br /><i class="fa fa-long-arrow-right" style="padding-left:10px;"></i> ';
                            $messages .= $this->Html->link(__(' Réponse envoyée') . ' <i class="fa fa-download"></i>', ['controller' => 'actes', 'action' => 'downloadTdtMessage', $reponse['tdt_id']], ['escape' => false, 'title' => __('Télécharger la réponse envoyée')]);
                        }
                        $messages .= '</div>';
                    }
                }
            }
        }
        return $messages;
    }

    public function retourneAnnexes($deliberation)
    {
        $annexes = '';
        if (!empty($deliberation['Annexe'])) {
            $annexes.='<div class="dropdown">'
                . '<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">'
                . __('Liste des annexes');
            if (!empty($deliberation['Deliberation']['tdt_ar_annexes_status'])) {
                $annexes.=' <span class="label label-' . $deliberation['Deliberation']['tdt_ar_annexes_status'] . ' label-as-badge"'
                    . 'title="' . $deliberation['Deliberation']['tdt_ar_annexes_status_libelle'] . '"'
                    . '>' . count($deliberation['Annexe']) . '</span>';
            } else {
                $annexes.=' <span class="label label-info label-as-badge">' . count($deliberation['Annexe']) . '</span>';
            }

            $annexes.=' <span class="caret"></span>'
                . '</button>'
                . '<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">';
            foreach ($deliberation['Annexe'] as $annexe) {
                $annexes.='<li role="presentation">' .
                    $this->Bs->btn($this->Bs->icon('download').' '. __('%s (%s)', $annexe['filename'], $annexe['typologiepiece_code_libelle']), ['controller' => 'annexes', 'action' => 'downloadTampon', $annexe['id']], [
                        'role' => 'menuitem',
                        'escapeTitle'=> false,
                        'tabindex' => -1
                    ])
                    . '</li>';
            }
            echo '</ul>'
        . '</div>';
        }
        return $annexes;
    }
}
