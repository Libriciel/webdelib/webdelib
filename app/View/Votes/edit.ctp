<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Séances à traiter'), ['controller'=>'seances', 'action'=>'index']);
$this->Html->addCrumb($seance['Typeseance']['libelle'] . ' ' . __('du') . ' ' . $this->Time->i18nFormat($seance['Seance']['date'], '%d/%m/%Y'). __(' à ') . $this->Time->i18nFormat($seance['Seance']['date'], '%kh%M'), ['controller'=>'votes', 'action'=>'index', $seance['Seance']['id']]);
$this->Html->addCrumb(__('Liste des présents'));//, array($this->request['controller'], 'action'=>'index'));

if (isset($message)) {
    echo $this->Bs->div('message', $message, ['id' => 'flashMessage']);
}

$options = ['3', '2', '4', '5'];

echo $this->Bs->tag('h3', __('Vote pour le projet %s ', $deliberation['Deliberation']['objet_delib']));

//Premier panel : Liste des présents
echo $this->Bs->div('panel panel-default') .
    $this->Bs->div('panel-heading', __('Liste des présents')) .
    $this->Bs->div('panel-body') .
    $this->Bs->div('btn-group').
    $this->Bs->confirm(
        $this->Bs->icon('save') . ' ' . __('Enregistrer la liste'),
        [
            'controller' => 'votes',
            'action' => 'listerPresents',
            $deliberation['Deliberation']['id'],
            $seance['Seance']['id']
        ],
        [
        'type' => 'success',
        'button' => $this->Bs->icon('save') . ' ' .'Enregister',
//        'icon' => 'list',
        'title' => __('Enregistrer la liste des présents'),
        'texte' => $this->requestAction(
            [
                'controller'=>'votes',
                'action'=>'listerPresents',
                $deliberation['Deliberation']['id'],
                $seance['Seance']['id']
            ],
            ['return']
        ),
            'header' => __('Liste des présents')
        ],
        ['form'=>true,'size'=>'lg', 'id'=>'modalListerPresents']
    ) .
    $this->Bs->btn(
        __('Récupérer la liste'),
        ['controller'=>'votes',
            'action'=>'copyFromPrevious',
            $deliberation['Deliberation']['id'],
            $seance['Seance']['id']],
        [
            'type'=>'primary',
            'title' => 'Récupérer la liste de la précédente délibération',
            'icon'=>'cloud-download-alt']
    ).
    $this->Bs->close(3);


//Second panel : Liste des présents
$options_menu_deroulant = [1 => __('Détail des voix'), 2 => __('Total des voix'), 3 => __('Résultat'), 4 => __('Prendre acte')];

echo $this->Bs->div('panel panel-default') .
    $this->Bs->div('panel-heading', 'Vote') .
     $this->Bs->div('panel-body') .
     //creation du formulaire
     $this->BsForm->create('Votes', [
         'type'=>'post',
         'url' => ['controller' => 'votes', 'action' => 'edit', $deliberation['Deliberation']['id'], $seance['Seance']['id']],
         'class' => 'appVote',
         'novalidate' => true]) .
     $this->BsForm->select('Deliberation.president_id', $acteurs, [
        'label' => __('Président'),
        'div'=> false,
        'class' => 'selectone',
        'data-placeholder'=> __('Sélectionner le président de séance pour ce vote'),
        'data-allow-clear'=> true,
        'value' => !empty($selectedPresident) ? $selectedPresident : null,
        'empty' => true,
        'autocomplete' => 'off',
        'selected' => $selectedPresident
     ]) .
     $this->Bs->tag('br').$this->Bs->tag('br').$this->Bs->tag('br').
     $this->BsForm->select(
         'Vote.typeVote',
         $options_menu_deroulant,
         [
        'label'=>__('Saisie du vote'),
        'div'=>false,
        'default'=>1,
        'autocomplete'=>'off',
        'class' => 'selectone',
        'empty'=>false]
     ).$this->Bs->tag('br').$this->Bs->tag('br');

//TABLEAU VOTE
$this->BsForm->setLeft(0);
$this->BsForm->setRight(12);
echo $this->Bs->row(['id' => 'voteDetail']) .
    $this->Bs->col('xs12');

//Tableau vote : premiére partie
$attribute = [];
$attribute['attributes']['id'] = 'tableDetailVote';
$attribute['attributes']['name'] = 'tableDetailVote';

echo $this->Bs->table(
    [
        ['title' => __('Élus')],
        ['title' => __('Vote'), ['colspan'=> 4]],
    ],
    ['striped'],
    $attribute
);

$this->Bs->setTableNbColumn(4);

echo $this->Bs->cell('') .
    $this->Bs->cell(__('Pour')) .
        $this->Bs->cell(__('Contre')).
        $this->Bs->cell(__('Abstention')).
        $this->Bs->cell(__('Pas de participation'));
foreach ($presents as $present) {
    $this->Bs->lineAttributes(['class'=>'typeacteur_'.$present['Acteur']['typeacteur_id']]);

    //Cellule elu
    if ($present['Listepresence']['present']==true && empty($present['Listepresence']['suppleant_id'])) {
        $cell_elu = $present['Acteur']['prenom'].' '.$present['Acteur']['nom'];
    } elseif ($present['Listepresence']['present']==true && !empty($present['Listepresence']['suppleant_id'])) {
        $cell_elu = $present['Listepresence']['suppleant'];
    } elseif ($present['Listepresence']['present']==false && !empty($present['Listepresence']['mandataire'])) {
        $cell_elu = $present['Acteur']['prenom'].' '.$present['Acteur']['nom'];
        $cell_elu .= ' (donne mandat à '.$present['Listepresence']['mandataire'].')';
    } elseif ($present['Listepresence']['present']==false) {
        $cell_elu = $present['Acteur']['prenom'].' '.$present['Acteur']['nom'].' '.__('(Absent)');
    }

    echo $this->Bs->cell($cell_elu);

    //Cellule vote
    if ($present['Listepresence']['present'] == false && empty($present['Listepresence']['mandataire'])) {
        echo $this->Bs->cell('') .
            $this->Bs->cell('') .
             $this->Bs->cell('').
             $this->Bs->cell('');
    } else {
        foreach ($options as $option) {
            // debug($this->request->data['detailVote'][$present['Acteur']['id']].'=>'.$option);
            //debug($this->Html->value('detailVote.'.$present['Acteur']['id']));
            $cell_vote = $this->BsForm->radio(
                'detailVote.'.$present['Acteur']['id'],
                [$option => null],
                [
                'inline' => false,
                'label'=> false,
                'hiddenField' => false,
                'autocomplete'=>'off',
                'class' => 'class-wd-vote-detail',
                //'legend' => false,
                'checked'=> (!empty($this->request->data['detailVote'][$present['Acteur']['id']]) && $this->request->data['detailVote'][$present['Acteur']['id']]==$option) ? false : false,
                ]
            ); //vote(this)

            echo $this->Bs->cell($cell_vote);
        }
    }
}

//Tableau vote : seconde partie
$this->Bs->setTableNbColumn(4);

echo $this->Bs->cell(__('Raccourcis pour les votes')) .
    $this->Bs->cell(__('Pour')) .
     $this->Bs->cell(__('Contre')).
     $this->Bs->cell(__('Abstention')).
     $this->Bs->cell(__('Pas de participation'));

//Option liste des presents - Tous les présents
echo $this->Bs->cell(__('Tous les présents'));

foreach ($options as $option) {
    $cell_vote = $this->BsForm->radio(false, [$option => null], [
        'id'=>'racc_tous_',
        'inline'=> false,
        'legend' => false,
        'hiddenField' => false,
        'label'=> false,
        'name' => 'racc_tous',
        'class' => 'class-wd-vote-racc',
        'data-wd-vote-type'=> 'tous',
        'checked'=> false,
        'value'=> false,
        'autocomplete' => 'off'
    ]); //vote(this)

    echo $this->Bs->cell($cell_vote);
}

//Par groupes d'acteurs - Majorité
foreach ($typeacteurs as $typeacteur_id => $typeacteur_nom) {
    echo $this->Bs->cell($typeacteur_nom);
    $scope = 'typeacteur_'.$typeacteur_id;

    foreach ($options as $option) {
        $cell_vote = $this->BsForm->radio(false, [$option => null], [
            'id'=> 'racc_'.$scope.'_',
            'legend' => false,
            'label'=> false,
            'checked'=> false,
            'hiddenField' => false,
            'name' => 'racc_'.$scope,
            'class' => 'class-wd-vote-racc',
            'data-wd-vote-type'=> $scope,
            'inline' => false,
            'value'=> false,
            'autocomplete' => 'off'
        ]); //vote(this)

        echo $this->Bs->cell($cell_vote);
    }
}

$this->Bs->setTableNbColumn(4);

echo $this->Bs->cell('') .
    $this->Bs->cell(__('Pour')) .
        $this->Bs->cell(__('Contre')).
        $this->Bs->cell(__('Abstention')).
        $this->Bs->cell(__('Pas de participation'));

//Resultats - Total
//    echo $this->Bs->cell("Total (".count(Hash::extract($presents, '{n}.Listepresence[present=true]')).")");
echo $this->Bs->cell(__('Total des saisies'), '', ['id' => 'nombreVotants']);

foreach ($options as $option) {
    echo  $this->Bs->cell(
        $this->Form->input('Vote.res'.$option, [
            'id'=> 'VoteRes'.$option,
            'disabled' => true,
            'label'=>false,
            'name' => 'VoteRes'.$option,
            'autocomplete' => 'off'])
    );
}

echo $this->Bs->endTable() .
    $this->Bs->btn($this->Bs->icon('eraser') . ' ' . __('Remise à zéro des votes'), 'javascript:void(0);', [
        'type' => 'primary',
        'class' => 'class-wd-vote-erase',
        'data-wd-vote-erase-id' => 'tableDetailVote',
        'escapeTitle' => false,
        'title' => __('Remise à zéro des votes')
    ]) .$this->Bs->tag('br', null).$this->Bs->tag('br', null).
$this->Bs->close(2);

//TABLEAU VOTE TOTAL
echo $this->Bs->div(null, null, ['id' => 'voteTotal']);
echo $this->Bs->table(
    [
        ['title' => ''],
        ['title' => __('Vote'), ['colspan' => 4]],
    ],
    ['striped'],
    $attribute
);

$this->Bs->setTableNbColumn(4);
echo $this->Bs->cell('') .
    $this->Bs->cell(__('Pour')) .
     $this->Bs->cell(__('Contre')).
     $this->Bs->cell(__('Abstention')).
     $this->Bs->cell(__('Pas de participation'));
echo $this->Bs->cell(__('Total des saisies'), null, ['id' => 'NombreTotalVoix']) .
    $this->Bs->cell($this->Form->input('Deliberation.vote_nb_oui', ['label' => false, 'size' => '3', 'default' => 0])) .
     $this->Bs->cell($this->Form->input('Deliberation.vote_nb_non', ['label'=>false, 'size' => '3','default'=>0])).
     $this->Bs->cell($this->Form->input('Deliberation.vote_nb_abstention', ['label'=>false, 'size' => '3','default'=>0])).
     $this->Bs->cell($this->Form->input('Deliberation.vote_nb_retrait', ['label'=>false, 'size' => '3','default'=>0]));
echo $this->Bs->endTable() . $this->Bs->close();

$this->BsForm->setDefault();
//TABLEAU VOTE RESULTAT
echo $this->Bs->div(null, null, ['id' => 'voteResultat']) .
    $this->BsForm->radio('Deliberation.etat', ['3' => __('Adopté'), '4' => __('Rejeté')], [
        'label' => __('Vote'), 'checked' => $deliberation['Deliberation']['etat']]) .
$this->Bs->close();

//TABLEAU VOTE PRENDS ACTE
echo $this->Bs->div(null, null, ['id' => 'votePrendsActe']) .
    $this->BsForm->radio('Deliberation.vote_prendre_acte', ['1' => __('Oui'), '0' => __('Non')], [
        'label' => __('Prend acte')]) .
$this->Bs->close();

//COMMENTAIRE
$this->BsForm->setLeft(3);
$this->BsForm->setRight(0);
echo $this->Bs->div(null, null, ['id' => 'optional']) .
    $this->BsForm->input('Deliberation.vote_commentaire', ['label' => __('Commentaire'),
        'style' => 'width:50%; max-width:90%; padding: 5px; max-height: 200px;',
        'type' => 'textarea', 'rows' => '6', 'cols' => '60', 'maxlength' => '1000',
    ]) .
$this->Bs->close(2);

$this->BsForm->setDefault();

echo $this->Html2->btnSaveCancel('', $previous, __('Enregistrer le vote'));

//Fin du formulaire
echo $this->BsForm->end() . $this->Bs->tag('br') .$this->Bs->tag('br');
echo $this->Bs->close(2);
