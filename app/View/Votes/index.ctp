<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Séances à traiter'), ['controller' => 'seances', 'action' => 'index']);

$this->Html->addCrumb($seance['Typeseance']['libelle'] . ' ' . __('du') . ' ' . $this->Time->i18nFormat($date_seance, '%d/%m/%Y'). __(' à ') . $this->Time->i18nFormat($date_seance, '%kh%M'));

$seanceLibelle = $seance['Typeseance']['libelle'] . __(' du ') . $this->Time->i18nFormat($date_seance, '%d/%m/%Y à %k:%M');
echo $this->Bs->tag('h3', __('Liste des projets : ') . $seanceLibelle);

echo $this->Bs->div('btn-toolbar', null, ['role' => "toolbar"]);
if ($this->permissions->check('Votes', 'update')
) {
    echo $this->Bs->confirm(
        $this->Bs->icon('save') . ' ' . __('Renseigner la liste de présence du procès-verbal'),
        [
            'controller' => 'votes',
            'action' => 'listerPresencesGlobale',
            $seance_id
        ],
        [
            'type' => 'success',
            'button' => $this->Bs->icon('save') . ' ' . 'Enregister',
//        'icon' => 'list',
            'title' => __('Renseigner la liste de présence du procès-verbal'),
            'texte' => $this->requestAction(
                [
                    'controller' => 'votes',
                    'action' => 'listerPresencesGlobale',
                    $seance_id
                ],
                ['return']
            ),
            'header' => __('Liste des présents de la séance')
        ],
        [
            'class' => 'appVote',
            'form' => true,
            'size' => 'lg',
            'id' => 'modalListerPresentsGlobale'
        ]
    );
}
//if ($this->permissions->check('importVotesIdelibre', 'read')
//) {
//    echo $this->Bs->div('btn-group', null, ['role' => "group"]);
//    echo $this->Bs->modal(
//        __('Récupérer les résultats de la séance %s', $seanceLibelle),
//        __('Êtes-vous sûr de vouloir lancer la récupération des résultats ?'),
//        [
//            'form' => false
//        ],
//        [
//            'close' => $this->Bs->icon('times-circle') . ' Annuler',
//            'open' => [
//                'name' => $this->Bs->icon('cloud-download-alt'),
//                'class' => 'btn-primary',
//                'type' => 'danger',
//                'options' => [
//                    'tag' => 'button',
//                    'option_type' => 'button',
//                    'disabled' => in_array('btnImportVotesIdelibreActif', $permissionsCheck, true) ? false : 'disabled',
//                    'title' => __('Récupérer les résultats Idelibre'),
//                ]
//            ],
//            'confirm' => [
//                'name' => $this->Bs->icon('cloud-download-alt') . ' ' . 'Récupérer les résultats',
//                'class' => 'btn-primary',
//                'link' => ['controller' => 'votes', 'action' => 'importVotesIdelibre', $seance_id],
//                'options' => ['escape' => false]
//            ]
//        ]
//    );
//    if(isset($seanceIdelibreVotesEtat)){
//        $btnIcon = '';
//        $btnType = 'default';
//        $dataContent = '';
//        switch ($seanceIdelibreVotesEtat) {
//            case -1:
//                $btnIcon = 'exclamation-triangle ';
//                $btnType = 'warning';
//                foreach ($idelibreVotesWarning as $vote) {
//                    if (isset($vote['delibId'])) {
//                        $dataContent .= __('Projet (position %s): %s', $vote['delibId'], $vote['errorMessage']);
//                    } else {
//                        $dataContent .= __('%s', addslashes($vote['errorMessage']));
//                    }
//                }
//                break;
//            case 1:
//                $btnIcon = 'clock';
//                $dataContent = 'En cours';
//                break;
//            case 2:
//                $btnIcon = 'check-square';
//                $dataContent = 'Récupération effectuée';
//                break;
//        }
//        echo $this->Bs->btn(
//            $this->Bs->icon($btnIcon), null, [
//                'tag' => 'button',
//                'option_type' => 'button',
//                'data-toggle' => 'popover',
//                'data-content' => $dataContent,
//                'data-placement' => 'right',
//                'escape' => false,
//                'type' => $btnType,
//                'escapeTitle' => false,
//                'title' => __('Informations&nbsp;sur&nbsp;la&nbsp;récupération')]
//        );
//    }
//    echo $this->Bs->close();
//}
echo $this->Bs->close();

echo $this->Bs->table([
    ['title' => __('Ordre')],
    ['title' => __('Id.')],
    ['title' => __('État')],
    ['title' => __('N° Acte')],
    ['title' => __('Libellé de l\'acte')],
    ['title' => __('Résultat')],
    ['title' => __('Actions')],
        ], ['hover', 'striped']);

foreach ($this->data as $projet) {
    echo $this->Bs->cell($projet['DeliberationSeance']['position']);
    echo $this->Bs->cell($this->Bs->link($projet['Deliberation']['id'], ['controller' => 'projets', 'action' => 'view', $projet['Deliberation']['id']]));

    if (!empty($projet['iconeEtat']['image'])) {
        if (in_array($projet['iconeEtat']['image'], ['encours', 'circuit_user_fini', 'attente_circuit', 'circuit_user_atraiter', 'circuit_redacteur_attente', 'circuit_attente'], true)) {
            $projet['iconeEtat']['status'] = 'warning';
        }
    }

    $etat = $this->Bs->btn(
        $this->Bs->icon($this->ProjetUtil->etat_icon($projet['iconeEtat']['image']), ['xs','iconType' =>  $this->ProjetUtil->etat_iconType($projet['iconeEtat']['image'])]),
        null,
        [
        'tag' => 'button',
        'type' => empty($projet['iconeEtat']['status']) ? $this->ProjetUtil->etat_icon_type($projet['iconeEtat']['image']) : $projet['iconeEtat']['status'],
        'size' => 'xs',
        'option_type' => 'button',
        //'disabled'=>'disabled',
        'data-toggle' => 'popover',
        'data-content' => $this->ProjetUtil->etat_icon_help($projet['iconeEtat']['image']),
        'data-placement' => 'right',
        'escape' => false,
        'title' => $projet['iconeEtat']['titre']
            ]
    );

    echo $this->Bs->cell($etat);
    echo $this->Bs->cell(!empty($projet['Deliberation']['num_delib']) ? $projet['Deliberation']['num_delib'] : '');
    echo $this->Bs->cell($projet['Deliberation']['objet_delib']);
    $resultat = '';
    if ($projet['Deliberation']['etat'] !== 4 && $projet['Deliberation']['etat']>2) {
        $resultat = $this->Bs->icon(
            'thumbs-up',
            ['2x'],
            ['title' => __('Adopté'), 'class' => 'text-success']
        );
    } elseif ($projet['Deliberation']['etat']>2) {
        $resultat = $this->Bs->icon(
            'thumbs-down',
            ['2x'],
            ['title' => __('Rejeté'), 'class' => 'text-danger']
        );
    }

    echo $this->Bs->cell((!empty($resultat) ? $resultat : ''));
    $actions = $this->Bs->div('btn-group-vertical');

    if (in_array('saisirDebat', $projet['btn_actions'], true)) {
        $actions.= $this->Bs->btn($this->Bs->icon('comments'), [
            'controller' => 'debats',
            'action' => 'edit', $projet['Deliberation']['id'], $seance_id], [
            'type' => 'default',
            'class' => 'btn-action-m',
            'escapeTitle' => false,
            'title' => __('Saisir les debats du projet'),
        ]);
    }
    if (in_array('voter', $projet['btn_actions'], true)) {
        $actions.= $this->Bs->btn($this->Bs->icon('gavel'), [
            'controller' => 'votes',
            'action' => 'edit', $projet['Deliberation']['id'], $seance_id], [
            'type' => 'primary',
            'class' => 'btn-action-m',
            'escapeTitle' => false,
            'title' => __('Voter le projet'),
        ]);
    }
    if (in_array('resetVote', $projet['btn_actions'], true)) {
        $actions.= $this->Bs->btn($this->Bs->icon('undo'), [
            'controller' => 'votes',
            'action' => 'delete', $seance_id, $projet['Deliberation']['id']], [
            'type' => 'warning btn-action-m',
            'escapeTitle' => false,
            'confirm' => __('Réinitialiser le vote pour le projet'),
            'title' => __('Réinitialiser le vote pour le projet'),
        ]);
    }
    if (in_array('genereFusionToClient', $projet['btn_actions'], true)) {
        $actions .= $this->Bs->btn($this->Bs->icon('cog'), ['controller' => 'projets', 'action' => 'genereFusionToClient', $projet['Deliberation']['id']], [
            'type' => 'default',
            'class' => 'waiter btn-action-m',
            'data-waiter-send' => 'XHR',
            'data-waiter-callback' => 'getProgress',
            'data-waiter-type' => 'progress-bar',
            'data-waiter-title' => __('Générer le document du projet'),
            'title' => __('Générer le document du projet'),
            'escapeTitle' => false,
        ]);
    }
    if (in_array('genereFusionToClientByModelTypeNameAndSeanceId', $projet['btn_actions'], true)) {
        $actions.=$this->Bs->btn(
            $this->Bs->icon('cog'),
            [
            'controller' => 'projets', 'action' => 'genereFusionToClientByModelTypeNameAndSeanceId', 'deliberation', $seance_id, $projet['Deliberation']['id']
        ],
            [
                'type' => 'default',
                'class' => 'waiter  btn-action-m',
                'escapeTitle' => false,
                'data-waiter-send' => 'XHR',
                'data-waiter-callback' => 'getProgress',
                'data-waiter-type' => 'progress-bar',
                'data-waiter-title' => __('Générer le document du projet'),
                'title' => __('Générer le document du projet')
            ]
        );
    }
    $actions.= $this->Bs->close();

    echo $this->Bs->cell($actions);
}
echo $this->Bs->endTable();
echo $this->Html2->btnCancel($previous);
