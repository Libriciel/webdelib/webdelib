<?php

echo $this->BsForm->create('Deliberation', ['type' => 'post', 'url' => ['controller' => 'votes', 'action' => 'listerPresents', $delib_id, $seance_id]]);

$this->BsForm->setLeft(0);
$this->BsForm->setRight(12);

echo $this->Bs->table(
    [['title' => __('Élu')], ['title' => __('Présent')], ['title' => __('Mandataire')]],
    ['striped']
);
foreach ($liste_presents as $present) {
    $options = [];
    $suppleant_id = $present['Acteur']['suppleant_id'];
    if (($suppleant_id != null) || isset($present['Acteur']['is_suppleant'])) {
        if (isset($present['Suppleant']['id'])) {
            $options[$present['Acteur']['id']] = __("Titulaire") . " : " . $present['Acteur']['nom'] . ' ' . $present['Acteur']['prenom'];
            $options[$suppleant_id] = __("Suppléant") . " : " . $present['Suppleant']['nom'] . ' ' . $present['Suppleant']['prenom'];
        }
    }

    //cellule Élu
    if (($suppleant_id != null) || isset($present['Acteur']['is_suppleant'])) {
        $cell_elu = $this->BsForm->select('Acteur.' . $present['Acteur']['id'] . '.suppleant_id', $options, ['class' => 'selectone', 'label' => false, 'inline' => true, 'autocomplete' => 'off', 'default' => !empty($present['Listepresence']['suppleant_id']) ? $present['Listepresence']['suppleant_id'] : $present['Acteur']['id']]);
    } else {
        $cell_elu = $present['Acteur']['nom'] . ' ' . $present['Acteur']['prenom'];
    }

    echo $this->Bs->cell($cell_elu);

    //cellule Présent
    echo $this->Bs->cell($this->BsForm->checkbox('Acteur.' . $present['Acteur']['id'] . '.present', [
        'label' => false, 'autocomplete' => 'off',
        'data-wd-acteur-id' => $present['Acteur']['id'],
        'checked' => $present['Listepresence']['present']]
    ));

    //cellule Mandataire
    if (empty($present['Acteur']['id'])) {
        $cell_mandataire = $this->Form->input("Acteur." . $present['Acteur']['id'] . '.mandataire', [
            'id' => 'liste_Acteur' . $present['Acteur']['id'] . 'Present',
            'label' => false, 'class' => 'selectone',
            'options' => $mandataires, 'readonly' => 'readonly',
            'disabled' => $present['Listepresence']['present'] == true ? 'disabled' : null, 'empty' => true]
        );
    } else {
        $disabled = [];
        if ($present['Listepresence']['present'] === true) {
            $disabled = ['disabled' => 'disabled'];
        }
        $cell_mandataire = $this->BsForm->select("Acteur." . $present['Acteur']['id'] . '.mandataire', $mandataires, $disabled + ['id' => 'liste_Acteur' . $present['Acteur']['id'] . 'Present', 'label' => false, 'class' => 'app-selectPresent', 'data-placeholder' => __('Sélectionner un mandataire'), 'data-allow-clear'=> true, 'empty' => true, 'inline' => true, 'autocomplete' => 'off', 'value' => !empty($present['Listepresence']['mandataire']) ? $present['Listepresence']['mandataire'] : false]);
    }
    echo $this->Bs->cell($cell_mandataire);
}

echo $this->Bs->endTable();
echo $this->BsForm->end();
