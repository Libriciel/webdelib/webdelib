<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Connecteurs'), [
        'admin' => true, 'prefix' => 'admin', 'controller' => 'connecteurs', 'action' => 'index'
]);
$this->Html->addCrumb($titre);

echo $this->Bs->tag('h3', $titre);

$true_false = ['true' => __('Oui'), 'false' => __('Non')];
echo $this->BsForm->create(
    'Connecteur',
    [
  'url' => [
    'admin' => true,
    'prefix' => 'admin',
    'controller' => 'connecteurs',
    'action' => 'makeconf', 'conversion'],
  'type' => 'file']
);
?>
<fieldset>
    <legend><?php echo __('Paramétrage général'); ?></legend>
    <?php
    echo $this->BsForm->select('app_upload_max_filesize', $sizes, [
        'class' => 'selectone',
        'label' => __('Taille limite de téléchargement autorisée'),
        'value' => Configure::read('App.upload_max_filesize'),
        'help' => __('Par défaut, l\'application est configuré sur 20 Mo et ne peut dépasser 50 Mo maximum de téléchargement autorisée par fichier.')
    ]);
    ?>
    <legend><?php echo __('Paramétrage du service Portainer'); ?></legend>
    <?php
    echo $this->BsForm->input('Portainer_host', [
        'type' => 'text',
        "placeholder" => __("http://wd-portainer:9000"),
        "help" =>  __('Service pour la maintenance des services (Portainer Community Edition)'),
        'label' => __('URL du service Portainer.io'),
        'value' => Configure::read('portainer.host')]);
    echo $this->BsForm->input('Portainer_admin', [
        'type' => 'text',
        "placeholder" => __("admin"),
        'label' => __('Nom d\'utilisateur'),
        'help' => 'Le compte utilisateur doit être un compte administrateur',
        'value' => Configure::read('portainer.admin')]);
    echo '<div id = "show_hide_password">'.$this->BsForm->inputGroup(
            'Portainer_password',
            [
                [
                    'content' => $this->Bs->icon('eye'),
                    'title' => __('Afficher le mot de passe'),
                    'type' => 'button',
                ]
            ],
            [
                'type' => 'password',
                'placeholder' => __('Mot de passe'),
                'label' => __('Mot de passe'),
                'autocomplete' => 'off',
                'value' => Configure::read('portainer.password'),
            ],
            [
                'before' => '<div class="col-md-9 col-md-offset-0">',
                'multiple' => true,
                'side' => 'right'
            ]
        ). '</div>';
    echo '</fieldset>';
    ?>

<legend><?php echo __('Paramétrage du service lspdf2odt'); ?></legend>
<?php
echo $this->BsForm->input('app_convert_url', [
    'type' => 'text',
    "placeholder" => __("http://wd-lspdf2odt"),
    "help" => "Service de conversion des documents du format pdf vers le format odt pour la fusion documentaire",
    'label' => __('URL du service lspdf2odt'),
    'value' => Configure::read('App.convert.url')]);
echo $this->BsForm->select('app_convert_tool', $convertTools, [
    'class' => 'selectone',
    'label' => __('Type de conversion utilisée'),
    'value' => Configure::read('App.convert.tool'),
]);
echo $this->BsForm->select('app_convert_resolution', $resolutions, [
    'class' => 'selectone',
    'label' => __('Résolution utilisée'),
    'value' => Configure::read('App.convert.resolution'),
]);
echo $this->BsForm->radio('FusionConv_app_fileOdt_convert', $true_false, [
    'label' => __('Conversion des Odt en Odt images'),
    'value' => Configure::read('FusionConv.app.fileOdt.convert') ? 'true' : 'false']);
echo $this->Bs->tag('span',
    __('Par défaut, l\'application convertie les annexes de projets odt en odt image pour la génération de document.'),
    ['class' => 'help-block col-md-9 col-md-offset-3']
);
if (!empty($maintenance)) {
    echo $this->element('service_docker_action', ['service' => 'lspdf2odt', 'data' => $docker_services['lspdf2odt']]);
}
echo '</fieldset>';
?>
<legend><?php echo __('Paramètrage du service Flow'); ?></legend>
<?php
echo $this->BsForm->input('gedooo_url', [
    'type' => 'text',
    "placeholder" => __("http://wd-flow:8080/ODFgedooo"),
    'label' => __('URL du service Flow'),
    "help" => "Service de fusion documentaire",
    'value' => Configure::read('FusionConv.Gedooo.wsdl')]);

if (!empty($maintenance)) {
    echo $this->element('service_docker_action', ['service' => 'flow', 'data' => $docker_services['flow']]);
}
echo '</fieldset>';
?>
<fieldset>
    <legend><?php echo __('Paramétrage du service Cloudooo'); ?></legend>
    <?php
    echo $this->BsForm->input('cloudooo_url', [
        'type' => 'text',
        "placeholder" => __("http://wd-cloudooo:8011"),
        'label' => __('URL du service Cloudooo'),
        "help" => "Service de conversion des documents du format odt vers le format pdf après la fusion documentaire",
        'value' => Configure::read('FusionConv.cloudooo_host')]);
    if (!empty($maintenance)) {
        echo $this->element('service_docker_action', ['service' => 'cloudooo', 'data' => $docker_services['cloudooo']]);
    }
    echo '</fieldset>';
    ?>
    <fieldset>
        <legend><?php echo __('Paramétrage du service PDFStamp'); ?></legend>

<?php
echo $this->BsForm->input('PDFStamp_host', [
    'type' => 'text',
    "placeholder" => __("http://wd-pdf-stamp:8080"),
    "help" => "Service de mise en place du tampon de validation de la préfecture",
    'label' => __('URL du service PDFStamp'),
    'value' => Configure::read('PDFStamp.host')]);
echo $this->BsForm->input('PDFStamp_logo', [
    'type' => 'file',
    'autocomplete' => 'off',
    'label' => __('Logo'),
    'data-btnClass' => 'btn-primary',
    'data-icon' => $this->Bs->icon('file-alt'),
    'data-text' => ' ' . __('Choisir un fichier'),
    'data-placeholder' => __('Pas de fichier'),
    'data-badge' => 'true',
    'accept' => '.png,application/png',
    'class' => 'filestyle',
    'help' => __('Choisir un logo au format png'),
]);
echo $this->Bs->row();
echo $this->Bs->col('xs' . $this->BsForm->getLeft()) . $this->Bs->close();
echo $this->Bs->col('xs' . $this->BsForm->getRight()) . '<img src="' . $imgSrc . '" />'
    . $this->Bs->close(2);
echo $this->Bs->tag('br');
if (!empty($maintenance)) {
    echo $this->element('service_docker_action', ['service' => 'pdf-stamp', 'data' => $docker_services['pdf-stamp']]);
}
echo '</fieldset>';
$this->BsForm->setDefault();
echo $this->Html2->btnSaveCancel('', ['admin' => true, 'prefix' => 'admin', 'controller' => 'connecteurs', 'action' => 'index']);
echo $this->Form->end();
?>
