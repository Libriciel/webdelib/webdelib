<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Connecteurs'), ['admin' => true, 'prefix' => 'admin', 'controller' => 'connecteurs', 'action' => 'index']);
$this->Html->addCrumb($titre);

echo $this->Bs->tag('h3', $titre);
$protocol = Configure::read('LdapManager.Ldap.use');
echo $this->BsForm->create('Connecteur', ['url' => ['controller' => 'connecteurs', 'action' => 'makeconf', 'ldap']]);
?>
    <fieldset>
        <legend><?php echo __('Activation'); ?></legend>
        <?php
        echo $this->BsForm->radio('use', ['true' => __('Oui'), 'false' => __('Non')], ['value' => Configure::read('LdapManager.Ldap.use') ? 'true' : 'false', 'autocomplete' => 'off']);
        ?>
    </fieldset>
    <div id = 'config_content' <?php echo Configure::read('LdapManager.Ldap.use') === false ? 'style="display: none;"' : ''; ?>>
        <fieldset>
            <legend><?php echo __('Informations de connexion'); ?></legend>
            <?php
            echo $this->BsForm->select('type', $protocoles, ['id' => 'TypeAnnaire_LDAP', 'type' => 'select', 'class' => 'selectone', 'autocomplete' => 'off', 'label' => __('Type d\'annuaire'), 'value' => Configure::read('LdapManager.Ldap.type')]);

            echo $this->BsForm->input('host', ['id' => 'Serveur_LDAP', 'type' => 'text', 'placeholder' => __('serveur.mairie.fr'), 'help' => __('Entrer l’adresse IP de votre serveur ou le nom sous la forme serveur.mairie.fr'), 'label' => __('Serveur'), 'value' => Configure::read('LdapManager.Ldap.host')]);

            echo $this->BsForm->input('host_fall_over', ['id' => 'FallOver_LDAP', 'type' => 'text', 'label' => __('Fail over'), 'placeholder' => __('serveur2.mairie.fr'), 'help' => __('Entrer l’adresse IP de votre serveur secondaire ou le nom sous la forme serveur2.mairie.fr'), 'value' => Configure::read('LdapManager.Ldap.host_fall_over')]);

            echo $this->BsForm->input('port', ['id' => 'Port_LDAP', 'type' => 'text', 'placeholder' => __('389'), 'help' => __('Les serveurs LDAP utilisent le port 389 par défaut. Il est possible d’utiliser l’accès en LDAPS en configurant le port 636'), 'label' => __('Port'), 'value' => Configure::read('LdapManager.Ldap.port')]);

            echo $this->BsForm->input('login', ['id' => 'Login_LDAP', 'type' => 'text', 'placeholder' => __('Nom d\'utilisateur'), 'label' => __('Login'), 'value' => Configure::read('LdapManager.Ldap.login')]);

            echo '<div id = "show_hide_password">'.$this->BsForm->inputGroup(
                    'password',
                    [
                        [
                            'content' => $this->Bs->icon('eye'),
                            'title' => __('Afficher le mot de passe'),
                            'type' => 'button',
                        ]
                    ],
                    [
                        'id' => 'Password_LDAP',
                        'type' => 'password',
                        'placeholder' => __('Mot de passe'),
                        'label' => __('Mot de passe'),
                        'autocomplete' => 'off',
                        'value' => Configure::read('LdapManager.Ldap.password'),
                    ],
                    [
                        'before' => '<div class="col-md-9 col-md-offset-0">',
                        'multiple' => true,
                        'side' => 'right'
                    ]
                ). '</div>';

            echo $this->BsForm->input('basedn', ['id' => 'BaseDN_LDAP', 'type' => 'text', 'label' => __('Base DN'), 'placeholder' => __('dc=mairie,dc=fr'), 'help' => __('Emplacement de l\'annuaire à partir duquel les recherches et lectures sont effectuées (sous la forme DC=mairie,DC=fr pour le domaine mairie.fr )'), 'value' => Configure::read('LdapManager.Ldap.basedn')]);

            echo $this->BsForm->radio('tls', ['true' => __('Oui'), 'false' => __('Non')], ['id' => 'TLS_LDAP', 'label' => __('TLS'), 'value' => Configure::read('LdapManager.Ldap.tls') ? 'true' : 'false']);

            echo $this->BsForm->input('version', ['id' => 'Version_LDAP', 'type' => 'text', 'label' => __('Version'), 'placeholder' => __('3'), 'help' => __('La version 3 du protocole est la plus courante'), 'value' => Configure::read('LdapManager.Ldap.version')]);

            echo $this->BsForm->input('suffix', ['id' => 'SuffixeCompte_LDAP', 'type' => 'text', 'label' => __('Suffixe du compte'), 'placeholder' => __('@mairie.fr'), 'help' => __('Suffixe ajouté automatiquement au login de l\'utilisateur lors de l\'authentification'), 'value' => Configure::read('LdapManager.Ldap.account_suffix')]);

            echo $this->BsForm->input('filter', ['id' => 'Filter_LDAP', 'type' => 'text', 'label' => __('Filtre DN de recherche'), 'placeholder' => __('ou=agents,dc=mairie,dc=fr'), 'help' => __('Filtre DN permettant de restreindre la recherche de personnes dans l\'annuaire'), 'value' => Configure::read('LdapManager.Ldap.filter')]);
            ?>
        </fieldset>
        <fieldset>
            <legend><?php echo __('Tableau des concordances'); ?></legend>
            <?php
            $fields = Configure::read('LdapManager.Ldap.fields');
            foreach ($fields['User'] as $key => $value) {
                echo $this->BsForm->input('fields_user_' . $key, ['type' => 'text', 'label' => $key, 'value' => $value]);
            }
            ?>
        </fieldset>

        <?php
        echo $this->Bs->row() .
        $this->Bs->col('xs'.$this->BsForm->getLeft()) . $this->Bs->close() .
        $this->Bs->col('xs'.$this->BsForm->getRight()) .
        $this->Bs->btn($this->Bs->icon('undo').' '.__('Restaurer les valeurs par défaut'), 'javascript:void(0);', ['type' => 'default', 'id' => 'btnValeurDefault', 'escapeTitle' => false, 'name' => __('btnValeurDefault'), 'title' => __('Restaurer les valeurs par défaut de la configuration LDAP'), 'confirm'=> __('Êtes-vous sûr de vouloir restaurer la configuration LDAP par défaut ?')]).
        $this->Bs->close(2) ;
        ?>

    </div>
    <br/>

    <?php
    echo $this->Html2->btnSaveCancel('', ['controller' => 'connecteurs', 'action' => 'index']);
    echo $this->BsForm->end();
    ?>

<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {
        var concordance = {
            OpenLDAP : {
                Username : "uid",
                Note : "info",
                Nom : "sn",
                Prenom : "givenname",
                Email : "mail",
                Telfixe : "telephonenumber",
                Telmobile : "mobile",
                Actif : ""
            },
            ActiveDirectory: {
                Username : "samaccountname",
                Note : "info",
                Nom : "sn",
                Prenom : "givenname",
                Email : "mail",
                Telfixe : "telephonenumber",
                Telmobile : "mobile",
                Actif : "userAccountControl"
            }
        };
        $('#ConnecteurUseTrue').on('change', function (){
            changeActivation($(this));
        });
        $('#ConnecteurUseFalse').on('change', function (){
            changeActivation($(this));
        });

        $("#TypeAnnaire_LDAP").on("change", function() {
            RemplisageTableauConcordances($(this).val());
        });

        $('#btnValeurDefault').click(function() {
            RemplisageTableauConcordances($("#TypeAnnaire_LDAP").val());
        });


        function RemplisageTableauConcordances(typeLdap) {
            $.each( concordance[typeLdap], function( index, valeur ){
                $('#ConnecteurFieldsUser' + index).val(valeur);
            });
        }

        function changeActivation(element) {
            if ($(element).val() === 'true') {
                $('#config_content').show();
                RemplisageTableauConcordances($("#TypeAnnaire_LDAP").val());
            } else {
                $('#config_content').hide();
            }
        }
    });
});
</script>
