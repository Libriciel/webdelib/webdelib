<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Connecteurs'), ['admin' => true, 'prefix' => 'admin', 'controller' => 'connecteurs', 'action' => 'index']);
$this->Html->addCrumb($titre);

echo $this->Bs->tag('h3', $titre);
echo $this->BsForm->create('Connecteur', ['url' => ['admin' => true, 'prefix' => 'admin', 'controller' => 'connecteurs', 'action' => 'makeconf', 'publication'], 'type' => 'file']);
?>
<fieldset>
    <legend><?php echo __('Activation'); ?></legend>
    <?php
        echo $this->BsForm->radio('use_publication', ['true' => __('Oui'), 'false' => __('Non')], [
            'value' => Configure::read('USE_PUBLICATION') ? 'true' : 'false',
            'autocomplete' => 'off'
            ]);
    ?>
</fieldset>
<div class='spacer'></div>
<div id='config_content' <?php echo Configure::read('USE_PUBLICATION') === false ? 'style="display: none;"' : ''; ?>>
    <fieldset>
        <legend><?php echo __('Type de publication'); ?></legend>
        <?php
        echo $this->BsForm->select('publication_protocol', $protocoles, [
            'class' => 'selectone',
            'label' => __('Protocole'),
            'autocomplete' => 'off',
            'value' => Configure::read('PUBLICATION')
        ]);
        ?>
    </fieldset>
    <div id='config_content'>
        <fieldset class="pastell-infos">
            <legend><?php echo __('Connexion'); ?></legend>
            <?php
            echo $this->BsForm->input('publication_host', ['type' => 'text',
                'placeholder' => __('https://pastell.maville.fr'),
                'label' => __('URL du serveur pastell'),
                'value' => Configure::read('PUBLICATION_HOST')]) . '<br />';
            echo $this->BsForm->input('publication_id_e', ['type' => 'text',
                'placeholder' => __('identifiant de collectivité (id_e)'),
                'label' => __('Identifiant de l\'entité'),
                'value' => Configure::read('PUBLICATION_IDE')]) . '<br />';
            echo $this->BsForm->input('publication_login', ['type' => 'text',
                'placeholder' => __('nom d\'utilisateur'),
                'label' => __('Nom d\'utilisateur'),
                'value' => Configure::read('PUBLICATION_LOGIN')]) . '<br />';
            echo '<div id = "show_hide_password">'.$this->BsForm->inputGroup(
                    'publication_pwd',
                    [
                        [
                            'content' => $this->Bs->icon('eye'),
                            'title' => __('Afficher le mot de passe'),
                            'type' => 'button',
                        ]
                    ],
                    [
                        'type' => 'password',
                        'placeholder' => __('Mot de passe'),
                        'label' => __('Mot de passe'),
                        'autocomplete' => 'off',
                        'value' => Configure::read('PUBLICATION_PWD'),
                    ],
                    [
                        'before' => '<div class="col-md-9 col-md-offset-0">',
                        'multiple' => true,
                        'side' => 'right'
                    ]
                ). '</div><br />';
            ?>

        </fieldset>
        <fieldset class="pastell-infos">
            <legend><?php echo __('Configuration'); ?></legend>
            <?php
            echo $this->BsForm->input('publication_type', ['type' => 'text',
                'placeholder' => __('ls-actes-publication'),
                'label' => __('Flux'),
                'help' => __('Veuillez renseigner le nom du flux utilisé dans pastell pour l\'envoi des publications.'),
                'value' => Configure::read('PUBLICATION_FLUX')]);
            ?>
            <?php
            echo $this->BsForm->input('publication_pref_id', ['type' => 'text',
                'placeholder' => __('PREF000'),
                'label' => __('Prefecture ID'),
                'help' => __('Veuillez renseigner l\'identifiant de l\'entité exerçant le contrôle de légalité.'),
                'value' => Configure::read('PUBLICATION_PREF_ID')]);
            ?>
            <?php
            echo $this->BsForm->input('publication_permalien', ['type' => 'text',
                'placeholder' => __('https://serveur.mairie.fr/actes/'),
                'label' => __('Uri permalien'),
                'help' => __('Veuillez renseigner l\'uri de l\'adresse du permalien pour le document d\'acte.'),
                'value' => Configure::read('PUBLICATION_PERMALIEN')]);
            ?>
        </fieldset>
    </div>
    <div class='spacer'> </div>
</div>
<?php
echo $this->Html2->btnSaveCancel('', ['controller' => 'connecteurs', 'action' => 'index']);
echo $this->BsForm->end();
?>
<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {

        $('#ConnecteurUsePublicationTrue').on('change', function (){
            changeActivation($(this), 'config_content');
        });
        $('#ConnecteurUsePublicationFalse').on('change', function (){
            changeActivation($(this), 'config_content');
        });

        changeProtocol();
        $('#ConnecteurPublicationProtocol').on('change', function (){
            changeProtocol($(this));
        });

        function changeActivation(element, panel) {
            if ($(element).val() == 'true') {
                $('#'+ panel).show();
            } else {
                $('#'+ panel).hide();
            }
        }

        function changeProtocol() {
            var protocol = $('#ConnecteurPublicationProtocol').val();
            if (protocol == 'PASTELL') {
                $('.pastell-infos').show();
            } else {
                $('.pastell-infos').hide();
            }
        }
    });
});
</script>
