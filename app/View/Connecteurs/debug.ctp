<div class='spacer'> </div>
<?php
echo $this->Form->create('Connecteur', ['url' => '/connecteurs/makeconf/debug']);
?>
<fieldset>
    <legend><?php echo __('Paramètrage du mode DEBUG'); ?></legend>
    <?php
    $notif = [1 => 'debug', 0 => 'production'];
    echo $this->Form->input('debug', ['before' => '<label>' . __('Mode du DEBUG') . '</label>', 'legend' => false, 'type' => 'radio', 'options' => $notif, 'value' => Configure::read('debug'), 'div' => false, 'default' => 0, 'label' => false]);
    ?>
</fieldset>
<div class='spacer'> </div>
<?php
echo $this->Html2->boutonsSaveCancel('', '/connecteurs/index');
echo $this->Form->end();
