<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Connecteurs'), ['admin' => true, 'prefix' => 'admin', 'controller' => 'connecteurs', 'action' => 'index']);
$this->Html->addCrumb($titre);
?>
    <legend><?php echo $this->Bs->tag('h3', $titre); ?></legend>

    <?php
    $true_false = ['true' => __('Oui'), 'false' => __('Non')];
    echo $this->BsForm->create('Connecteur', ['url' => ['controller' => 'connecteurs', 'action' => 'makeconf', 'mail']]);
    ?>
    <fieldset>
        <?php
        echo $this->BsForm->input('mail_from', ['type' => 'text',
            "placeholder" => __("'Webdelib <webdelib@ma-collectivite.fr>"),
            'label' => __('Mail de l\'expéditeur'),
            'value' => Configure::read('MAIL_FROM')]);
        ?>
    </fieldset>

<?php
echo $this->Html2->btnSaveCancel('', ['controller' => 'connecteurs', 'action' => 'index']);
echo $this->BsForm->end();
?>
<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {

        $('#ConnecteurSmtpUseTrue').on('change', function (){
            changeActivation($(this));
        });
        $('#ConnecteurSmtpUseFalse').on('change', function (){
            changeActivation($(this));
        });

        function changeActivation(element) {
            if ($(element).val() == 'true') {
                $('#config_content').show();
            } else {
                $('#config_content').hide();
            }
        }
    });
});
</script>
