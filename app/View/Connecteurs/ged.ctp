<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Connecteurs'), ['admin' => true, 'prefix' => 'admin', 'controller' => 'connecteurs', 'action' => 'index']);
$this->Html->addCrumb($titre);

echo $this->Bs->tag('h3', $titre);

echo $this->BsForm->create('Connecteur', ['url' => ['controller' => 'connecteurs', 'action' => 'makeconf', 'ged']]
    );
?>
<fieldset>
    <legend><?php echo __('Activation'); ?></legend>
    <?php
    echo $this->BsForm->radio('use_ged', ['true' => __('Oui'), 'false' => __('Non')], ['autocomplete' => 'off', 'value' => Configure::read('USE_GED') ? 'true' : 'false']);
    ?>
</fieldset>
<div class='spacer'></div>
<div id='config_content'>
    <fieldset>
        <legend><?php echo __('Paramètrage de la GED'); ?></legend>
        <?php
        echo $this->BsForm->select('ged_protocol', $protocoles, ['class' => 'selectone', 'label' => __('Protocole'), 'autocomplete' => 'off', 'value' => Configure::read('GED')]);
        ?>
        <div class='spacer'></div>
        <fieldset class="cmis-infos">
        <?php
        echo $this->BsForm->input('ged_url', ['type' => 'text', "placeholder" => __("http://x.x.ma-ville.fr"), 'label' => __('Serveur de la GED'), 'value' => Configure::read('CMIS_HOST')]);
        echo $this->BsForm->input('ged_login', ['type' => 'text', "placeholder" => __("Nom d'utilisateur"), 'label' => __('Nom d\'utilisateur'), 'value' => Configure::read('CMIS_LOGIN')]);
        echo '<div id = "show_hide_password">'.$this->BsForm->inputGroup(
                'ged_passwd',
                [
                    [
                        'content' => $this->Bs->icon('eye'),
                        'title' => __('Afficher le mot de passe'),
                        'type' => 'button',
                    ]
                ],
                [
                    'type' => 'password',
                    'placeholder' => __('Mot de passe'),
                    'label' => __('Mot de passe'),
                    'autocomplete' => 'off',
                    'value' => Configure::read('CMIS_PWD'),
                ],
                [
                    'before' => '<div class="col-md-9 col-md-offset-0">',
                    'multiple' => true,
                    'side' => 'right'
                ]
            ). '</div>';
        echo $this->BsForm->input('ged_repo', ['type' => 'text', "placeholder" => __("/Sites/Web-delib"), 'label' => __('Répertoire de stockage'), 'value' => Configure::read('CMIS_REPO')]);
        echo $this->BsForm->input('ged_xml_version', ['type' => 'select', 'label' => __('Version du schéma XML'), 'options' => [1 => 1, 2 => 2, 3 => 3, 4 => 4], 'selected' => Configure::read('GED_XML_VERSION'), 'autocomplete' => 'off']);
        ?></fieldset>
    </fieldset>
</div>
<?php
echo $this->Html2->btnSaveCancel('', ['controller' => 'connecteurs', 'action' => 'index']);
echo $this->BsForm->end();
?>
<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {

        changeActivation();
        $('#ConnecteurUseGedFalse, #ConnecteurUseGedTrue').on('change', function (){
            changeActivation();
        });

        changeProtocol();
        $('#ConnecteurGedProtocol').on('change', function (){
            changeProtocol();
        });

        function changeActivation() {
            let active = $('#ConnecteurUseGedTrue:checked').val();
            if ( active === 'true') {
                $('#config_content').show();
            } else {
                $('#config_content').hide();
            }
        }

        function changeProtocol() {
            let protocol = $('#ConnecteurGedProtocol').val();
            if (protocol === 'PASTELL') {
                $('.pastell-infos').show();
            } else {
                $('.pastell-infos').hide();
            }
            if (protocol === 'CMIS') {
                $('.cmis-infos').show();
            } else {
                $('.cmis-infos').hide();
            }
        }
    });
});
</script>
