<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Connecteurs'), ['admin' => true, 'prefix' => 'admin', 'controller' => 'connecteurs', 'action' => 'index']);
$this->Html->addCrumb($titre);

    echo $this->Bs->tag('h3', $titre);
    if (empty($protocoles)) {
        echo $this->Bs->alert('<strong>' . __('Attention !') . '</strong> ' . __('Connecteur SAE inexistant.'), 'danger');
    }
    echo $this->BsForm->create('Connecteur', ['url' => ['controller' => 'connecteurs', 'action' => 'makeconf', 'sae']]);
    ?>
    <fieldset style="width: 100%">
        <legend><?php echo __('Activation'); ?></legend>
        <?php
        echo $this->BsForm->radio('use_sae', ['true' => __('Oui'), 'false' => __('Non')], ['value' => Configure::read('USE_SAE') ? 'true' : 'false', 'autocomplete' => 'off']);
        ?>
    </fieldset>
    <div class='spacer'></div>
    <div id='config_content' <?php echo Configure::read('USE_SAE') === false ? 'style="display: none;"' : ''; ?>>
        <fieldset>
            <legend><?php echo __('Connecteur'); ?></legend>
            <?php
            echo $this->BsForm->select('sae_protocol', $protocoles, ['class' => 'selectone', 'label' => __('Type de connecteur'), 'value' => Configure::read('SAE')]);
            ?>
        </fieldset>
        <div class='spacer'></div>
        <fieldset class='pastell-infos'>
            <legend><?php echo __('Configuration'); ?></legend>
            <?php
            echo $this->BsForm->input('sae_pastell_dossier_type_seance', [
                'type' => 'text',
                'label' => __('Type de dossier "séance"'),
                'placeholder' => __('ls-dossier-seance'),
                'autocomplete' => 'off',
                'value' => Configure::read('SAE_PASTELL_DOSSIER_TYPE_SEANCE'),
            ]);
            ?>
        </fieldset>
    </div>
<?php
echo $this->Html2->btnSaveCancel('', ['controller' => 'connecteurs', 'action' => 'index']);
echo $this->BsForm->end();
?>
<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {
        $('#ConnecteurUseSaeTrue').on('change', function (){
            changeActivation($(this));
        });
        $('#ConnecteurUseSaeFalse').on('change', function (){
            changeActivation($(this));
        });
            
        function changeActivation(element) {
            if ($(element).val() == 'true') {
                $('#config_content').show();
            } else {
                $('#config_content').hide();
            }
        }
        
        changeProtocol();
        $('#ConnecteurSaeProtocol').on('change', function (){
            changeProtocol($(this));
        });

        function changeProtocol() {
            var protocol = $('#ConnecteurSaeProtocol').val();
            if (protocol === 'PASTELL') {
                $('.pastell-infos').show();
            } else {
                $('.pastell-infos').hide();
            }
        }
    });
});  
</script>