<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Connecteurs'), ['admin' => true, 'prefix' => 'admin', 'controller' => 'connecteurs', 'action' => 'index']);
$this->Html->addCrumb($titre);

echo $this->Bs->tag('h3', $titre);
$true_false = ['true' => __('Oui'), 'false' => __('Non')];
echo $this->BsForm->create('Connecteur', ['url' => ['controller' => 'connecteurs', 'action' => 'makeconf', 'idelibre'], 'type' => 'file']);
?>
    <fieldset>
        <legend><?php echo __('Activation'); ?></legend>
        <?php
        echo $this->BsForm->radio('use_idelibre', $true_false, ['value' => Configure::read('USE_IDELIBRE') ? 'true' : 'false', 'autocomplete' => 'off']);
        ?>
    </fieldset>
    <div class='spacer'></div>
    <div id='config_content' <?php echo Configure::read('USE_IDELIBRE') === false ? 'style="display: none;"' : ''; ?>>
        <fieldset>
            <legend><?php echo __('Informations d\'authentification'); ?></legend>
            <?php
            echo $this->BsForm->input('idelibre_host', ['type' => 'text', "placeholder" => __("https://idelibre.adullact.org"), 'label' => __('URL'), 'value' => Configure::read('IDELIBRE_HOST')]);

            echo $this->BsForm->input('idelibre_conn', ['type' => 'text', 'placeholder' => __('Nom de la connexion'), 'title' => __('Nom de la variable de connexion dans le fichier database.php de idelibre'), 'label' => __('Connexion de la collectivité'), 'value' => Configure::read('IDELIBRE_CONN')]);

            echo $this->BsForm->input('idelibre_login', ['type' => 'text', 'placeholder' => __('Nom d\'utilisateur'), 'label' => __('Login'), 'value' => Configure::read('IDELIBRE_LOGIN')]);

            echo '<div id = "show_hide_password">'.$this->BsForm->inputGroup(
                    'idelibre_pwd',
                    [
                        [
                            'content' => $this->Bs->icon('eye'),
                            'title' => __('Afficher le mot de passe'),
                            'type' => 'button',
                        ]
                    ],
                    [
                        'type' => 'password',
                        'placeholder' => __('Mot de passe'),
                        'label' => __('Mot de passe'),
                        'autocomplete' => 'off',
                        'value' => Configure::read('IDELIBRE_PWD'),
                    ],
                    [
                        'before' => '<div class="col-md-9 col-md-offset-0">',
                        'multiple' => true,
                        'side' => 'right'
                    ]
                ). '</div>';

//            echo $this->BsForm->input('x_auth_token',
//                [
//                    'type' => 'password',
//                    'placeholder' => __('Token d\'authentification'),
//                    'label' => __('Token d\'authentification api v2'),
//                    'value' => Configure::read('IDELIBRE_X_AUTH_TOKEN')]
//            );

            ?>
        </fieldset>
        <fieldset id='infos_certificat'>
            <legend><?php echo __('Proxy'); ?></legend>
            <?php
            echo $this->BsForm->radio('use_proxy', $true_false, ['value' => Configure::read('IDELIBRE_USEPROXY') ? 'true' : 'false', 'autocomplete' => 'off']);
            ?>
            <div class='spacer'></div>
            <div id="proxy_host" <?php if (!Configure::read('S2LOW_USEPROXY')) {
                echo ' style="display: none;"';
            } ?>>
                <?php
                echo $this->BsForm->input('proxy_host', ['type' => 'text', 'placeholder' => __('http://x.x.x.x:8080'), 'value' => Configure::read('S2LOW_PROXYHOST'), 'label' => __('Adresse du proxy')]);
                ?> </div>
            <div class='spacer'></div>
        </fieldset>
        <fieldset id='infos_fusion'>
            <legend><?php echo __('Retirer les annexes'); ?></legend>
            <?= $this->BsForm->radio('remove_appendix_for_fusion', $true_false, ['value' => Configure::read('IDELIBRE_APPENDIX_FUSION') ? 'true' : 'false', 'autocomplete' => 'off']); ?>
        </fieldset>
    </div>
<?php
echo $this->Html2->btnSaveCancel('', ['controller' => 'connecteurs', 'action' => 'index']);
echo $this->BsForm->end();
?>
<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {
        
        $('#ConnecteurUseIdelibreTrue').on('change', function (){
            changeActivation($(this), 'config_content');
        });
        $('#ConnecteurUseIdelibreFalse').on('change', function (){
            changeActivation($(this), 'config_content');
        });
        
        $('#ConnecteurUseProxyTrue').on('change', function (){
            changeActivation($(this), 'proxy_host');
        });
        $('#ConnecteurUseProxyFalse').on('change', function (){
            changeActivation($(this), 'proxy_host');
        });
        
        $('#ConnecteurIdelibreUseCertTrue').on('change', function (){
            changeActivation($(this), 'idelibre_cert');
        });
        $('#ConnecteurIdelibreUseCertFalse').on('change', function (){
            changeActivation($(this), 'idelibre_cert');
        });
        
        function changeActivation(element, panel) {
            if ($(element).val() == 'true') {
                $('#'+ panel).show();
            } else {
                $('#'+ panel).hide();
            }
        }
        

    });
}); 
</script>