<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Connecteurs'), ['admin' => true, 'prefix' => 'admin', 'controller' => 'connecteurs', 'action' => 'index']);
$this->Html->addCrumb($titre);

echo $this->Bs->tag('h3', $titre);
echo $this->BsForm->create('Connecteur', ['url' => ['admin' => true, 'prefix' => 'admin', 'controller' => 'connecteurs', 'action' => 'makeconf', 'tdt'], 'type' => 'file']);

$true_false = ['true' => __('Oui'), 'false' => __('Non')];
?>
<fieldset>
    <legend><?php echo __('Activation'); ?></legend>
    <?php
        echo $this->BsForm->radio('use_tdt', ['true' => __('Oui'), 'false' => __('Non')], [
            'value' => Configure::read('USE_TDT') ? 'true' : 'false',
            'autocomplete' => 'off'
            ]);
    ?>
</fieldset>
<div class='spacer'></div>
<div id='config_content' <?php echo Configure::read('USE_TDT') === false ? 'style="display: none;"' : ''; ?>>
    <fieldset>
        <legend><?php echo __('Tiers de Télétransmission'); ?></legend>
        <?php
        echo $this->BsForm->select('tdt_protocol', $protocoles, [
            'class' => 'selectone',
            'label' => __('Protocole'),
            'value' => Configure::read('TDT')
        ]);
        ?>
    </fieldset>
    <div class='spacer'></div>
    <fieldset class="s2low-infos">
        <legend><?php echo __('Informations de connexion'); ?></legend>
        <?php
        echo $this->BsForm->input('host', [
            'type' => 'text',
            'placeholder' => __('https://www.s2low.org'),
            'label' => __('URL du serveur'),
            'value' => Configure::read('S2LOW_HOST')
        ]);
        ?>
    </fieldset>
    <fieldset class="s2low-infos">
        <legend><?php echo __('Certificat d\'authentification'); ?></legend>
        <?php
        echo $this->BsForm->input('clientcert', [
            'type' => 'file',
            'data-btnClass' => 'btn-primary',
            'data-icon' => $this->Bs->icon('file-alt'),
            'data-text' => ' ' . __('Choisir un fichier'),
            'data-placeholder' => __('Pas de fichier'),
            'data-badge' => 'true',
            'class' => 'filestyle',
            'help' => __('Choisir un fichier au format p12'),
            'label' => __('Certificat')
        ]);
              echo '<div id = "show_hide_password">'.$this->BsForm->inputGroup(
                'certpwd',
                [
                    [
                        'content' => $this->Bs->icon('eye'),
                        'title' => __('Afficher le mot de passe'),
                        'type' => 'button',
                    ]
                ],
                [
                    'type' => 'password',
                    'placeholder' => __('Mot de passe'),
                    'label' => __('Mot de passe'),
                    'autocomplete' => 'off',
                    'value' => Configure::read('S2LOW_CERTPWD'),
                ],
                [
                    'before' => '<div class="col-md-9 col-md-offset-0">',
                    'multiple' => true,
                    'side' => 'right'
                ]
            ). '</div>';
        if (!empty($clientCert)) {
            $affichCert='<b>' . __('Informations') . '</b>' . $this->Bs->tag('br /') .
                    __('Émetteur : ') . $clientCert['subject']['CN'] . $this->Bs->tag('br /') .
                    __('ID du certificat : ') . $clientCert['name'] . $this->Bs->tag('br /') .
                    __('Titulaire (CN) : ') . $clientCert['subject']['CN'] . $this->Bs->tag('br /') .
                    __('Organisation (O) : ') . $clientCert['subject']['O'] . $this->Bs->tag('br /');
            if (isset($clientCert['subject']['emailAddress'])) {
                $affichCert.=__('Contact : ') . $clientCert['subject']['emailAddress'] . $this->Bs->tag('br /');
            }
            $affichCert.=__('Numéro de série : ') . $clientCert['serialNumber'] . $this->Bs->tag('br /') .
                    __('Début de validité : ') . $this->Time->i18nFormat($clientCert['validFrom_time_t'], '%e %B %Y à %k:%M') . $this->Bs->tag('br /') .
                    __('Fin de validité : ') . $this->Time->i18nFormat($clientCert['validTo_time_t'], '%e %B %Y à %k:%M') . $this->Bs->tag('br /');
            echo $this->Bs->div('well', $affichCert);
        }
        echo $this->BsForm->radio('use_login', ['true' => __('Oui'), 'false' => __('Non')], [
                'value' => Configure::read('S2LOW_USELOGIN') ? 'true' : 'false',
                'autocomplete' => 'off','label'=>__('Utilisation d\'un login :')]);
        ?>
        <fieldset id="s2low_login"  <?php echo Configure::read('S2LOW_USELOGIN') === false ? 'style="display: none;"' : ''; ?>>
            <?php
        echo $this->BsForm->input('s2low_login', [
            'type' => 'text',
            'placeholder' => __('login'),
            'label' => __('Nom d\'utilisateur'),
            'value' => Configure::read('S2LOW_LOGIN')
        ]);

        echo $this->BsForm->input('s2low_pwd', [
            'type' => 'password',
            'placeholder' => __('mot de passe'),
            'label' => __('Mot de passe'),
            'value' => Configure::read('S2LOW_PWD')
        ]);
        ?>
        </fieldset>
        <div class="s2low-infos"<?php if (Configure::read('TDT') != 'S2LOW') {
            echo ' style="display: none;"';
        } ?>>
            <fieldset>
                <legend><?php echo __('Proxy'); ?></legend>
                <?php
                echo $this->BsForm->radio('use_proxy', ['true' => __('Oui'), 'false' => __('Non')], [
                    'value' => Configure::read('S2LOW_USEPROXY') ? 'true' : 'false',
                    'autocomplete' => 'off']);
                ?>
                <div class='spacer'></div>
                <div id="proxy_host" <?php if (!Configure::read('S2LOW_USEPROXY')) {
                    echo ' style="display: none;"';
                } ?>>
                    <?php
                    echo $this->BsForm->input('proxy_host', [
                        'type' => 'text',
                        'placeholder' => __('http://x.x.x.x:8080'),
                        'value' => Configure::read('S2LOW_PROXYHOST'),
                        'label' => __('Adresse du proxy')]);
                    ?> </div>
            </fieldset>
        </div>
    </fieldset>
    <fieldset class="pastell-infos">
        <legend><?php echo __('Bordereau de télétransmission disponible'); ?></legend>
        <?= $this->BsForm->radio('s2low_bordereau_exist', $true_false, ['value' => Configure::read('S2LOW_BORDEREAU_EXIST') ? 'true' : 'false', 'autocomplete' => 'off']); ?>
        <legend><?php echo __('Acte tamponné disponible'); ?></legend>
        <?= $this->BsForm->radio('s2low_acte_tampon_exist', $true_false, ['value' => Configure::read('S2LOW_ACTE_TAMPON_EXIST') ? 'true' : 'false', 'autocomplete' => 'off']); ?>
    </fieldset>
</div>
<?php
echo $this->Html2->btnSaveCancel('', ['controller' => 'connecteurs', 'action' => 'index']);
echo $this->BsForm->end();
?>
<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {

        $('#ConnecteurUseTdtTrue').on('change', function (){
            changeActivation($(this), 'config_content');
        });
        $('#ConnecteurUseTdtFalse').on('change', function (){
            changeActivation($(this), 'config_content');
        });

        changeProtocol();
        $('#ConnecteurTdtProtocol').on('change', function (){
            changeProtocol($(this));
        });


        $('#ConnecteurUseMailsTrue').on('change', function (){
            changeActivation($(this), 'mails_password');
        });
        $('#ConnecteurUseMailsFalse').on('change', function (){
            changeActivation($(this), 'mails_password');
        });

        $('#ConnecteurUseProxyTrue').on('change', function (){
            changeActivation($(this), 'proxy_host');
        });
        $('#ConnecteurUseProxyFalse').on('change', function (){
            changeActivation($(this), 'proxy_host');
        });

        $('#ConnecteurUseLoginTrue').on('change', function (){
            changeActivation($(this), 's2low_login');
        });
        $('#ConnecteurUseLoginFalse').on('change', function (){
            changeActivation($(this), 's2low_login');
        });

        function changeActivation(element, panel) {
            if ($(element).val() == 'true') {
                $('#'+ panel).show();
            } else {
                $('#'+ panel).hide();
            }
        }

        function changeProtocol() {
            var protocol = $('#ConnecteurTdtProtocol').val();
            if (protocol == 'PASTELL') {
                $('.pastell-infos').show();
            } else {
                $('.pastell-infos').hide();
            }
            if (protocol == 'S2LOW') {
                $('.s2low-infos').show();
            } else {
                $('.s2low-infos').hide();
            }
        }
    });
});
</script>
