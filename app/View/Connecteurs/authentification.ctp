<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Connecteurs'), ['admin' => true, 'prefix' => 'admin', 'controller' => 'connecteurs', 'action' => 'index']);
$this->Html->addCrumb($titre);

echo $this->Bs->tag('h3', $titre);
echo $this->BsForm->create(__('Connecteur'), ['url' => ['controller' => 'connecteurs', 'action' => 'makeconf', 'authentification'], 'type' => 'file']);
?>
<fieldset>
    <legend><?php echo __('Activation'); ?></legend>
    <?php
    echo $this->BsForm->radio('authentification_use', ['true' => __('Oui'), 'false' => __('Non')], ['value' => Configure::read('AuthManager.Authentification.use') ? 'true' : 'false', 'autocomplete' => 'off']);
    ?>
</fieldset>
<div id='config_content' <?php echo Configure::read('AuthManager.Authentification.use') === false ? 'style="display: none;"' : ''; ?>>
    <fieldset>
        <legend><?php echo __('Type d\'authentification'); ?></legend>
        <?php
        echo $this->BsForm->select('authentification_protocol', $protocoles, ['label' => __('Type'), 'value' => strtolower(Configure::read('AuthManager.Authentification.type')), 'autocomplete' => 'off', 'escape' => false, 'class' => 'selectone', 'required' => true, 'empty' => false]
        );
        ?>
    </fieldset>
    <div id='cas_infos' <?php echo Configure::read('AuthManager.Authentification.type') === 'CAS' ? '' : 'style="display: none;"'; ?>>
        <fieldset>
            <legend><?php echo __('Informations de connexion'); ?></legend>
            <?php
            echo $this->BsForm->input('cas_host', ['type' => 'text', 'placeholder' => __('cas.x.x.x'), 'label' => __('Serveur'), 'value' => Configure::read('AuthManager.Cas.host')]);
            echo $this->BsForm->input('cas_port', ['type' => 'text', 'placeholder' => __('443'), 'label' => __('port'), 'value' => Configure::read('AuthManager.Cas.port')]);
            echo $this->BsForm->input('cas_uri', ['type' => 'text', 'placeholder' => __('cas_login'), 'label' => __('URI'), 'value' => Configure::read('AuthManager.Cas.uri')]);

            echo $this->BsForm->input('cas_clientcert', ['type' => 'file', 'class' => 'filestyle', 'data-buttonText' => __('Choisir un fichier pem'), 'label' => __('Client certificat (pem)')]);
            if(!empty($clientCert)){
                echo $this->Bs->div('well', '<b>' . __('Informations') . '</b>' . $this->Bs->tag('br /') .
                        __('Émetteur : ') . $clientCert['subject']['CN'] . $this->Bs->tag('br /') .
                        __('ID du certificat : ') . $clientCert['name'] . $this->Bs->tag('br /') .
                        __('Titulaire (CN) : ') . $clientCert['subject']['CN'] . $this->Bs->tag('br /') .
                        __('Organisation (O) : ') . $clientCert['subject']['O'] . $this->Bs->tag('br /') .
                        __('Contact : ') . $clientCert['subject']['emailAddress'] . $this->Bs->tag('br /') .
                        __('Numéro de série : ') . $clientCert['serialNumber'] . $this->Bs->tag('br /') .
                        __('Début de validité : ') . $this->Time->i18nFormat($clientCert['validFrom_time_t'], '%e %B %Y à %k:%M') . $this->Bs->tag('br /') .
                        __('Fin de validité : ') . $this->Time->i18nFormat($clientCert['validTo_time_t'], '%e %B %Y à %k:%M') . $this->Bs->tag('br /')
                );
            }
            ?>
        </fieldset>
    </div>
</div>
<?php
echo $this->Html2->btnSaveCancel('', ['controller' => 'connecteurs', 'action' => 'index']);
echo $this->BsForm->end();
?>
<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {
        $('#ConnecteurAuthentificationUseTrue').on('change', function (){
            changeActivation($(this));
        });
        $('#ConnecteurAuthentificationUseFalse').on('change', function (){
            changeActivation($(this));
        });
        
        changeProtocol();
        $('#ConnecteurAuthentificationProtocol').on('change', function (){
            changeProtocol($(this));
        });
        
        function changeActivation(element) {
            if ($(element).val() === 'true') {
                $('#config_content').show();
            } else {
                $('#config_content').hide();
            }
        }

        function changeProtocol() {
            var protocol = $('#ConnecteurAuthentificationProtocol').val();
            if (protocol === 'cas') {
                $('#cas_infos').show();
            } else {
                $('#cas_infos').hide();
            }
        }
    });
});
</script>