<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Connecteurs'), ['admin' => true, 'prefix' => 'admin', 'controller' => 'connecteurs', 'action' => 'index']);
$this->Html->addCrumb($titre);

echo $this->Bs->tag('h3', $titre);
echo $this->BsForm->create('Connecteur', ['url' => ['controller' => 'connecteurs', 'action' => 'makeconf', 'signature'], 'type' => 'file']);
?>
<fieldset>
    <legend><?php echo __('Activation'); ?></legend>
    <?php
    echo $this->BsForm->radio('use_signature', ['true' => __('Oui'), 'false' => __('Non')], [
            'value' => Configure::read('USE_PARAPHEUR') ? 'true' : 'false',
            'autocomplete' => 'off'
            ]);
    ?>
</fieldset>
<div class='spacer'></div>
<div id='config_content' <?php echo Configure::read('USE_PARAPHEUR') === false ? 'style="display: none;"' : ''; ?>>
    <fieldset>
        <legend><?php echo __('Type de parapheur'); ?></legend>
        <?php
        echo $this->BsForm->select('signature_protocol', $protocoles, [
            'label' => __('Type'),
            'class' => 'selectone',
            'autocomplete' => 'off',
            'value' => Configure::read('PARAPHEUR')
        ]);
        ?>
    </fieldset>
    <div class='spacer'></div>
    <fieldset class='pastell-infos'>
        <legend><?php echo __('Configuration'); ?></legend>
        <?php
        echo $this->BsForm->select('pastell_parapheur_mode',$pastell_parapheur_types, [
            'label' => __('Type de parapheur'),
            'autocomplete' => 'off',
            'class' => 'selectone',
            'value' => Configure::read('PASTELL_PARAPHEUR_MODE')]);
        ?>
    </fieldset>
    <fieldset class='pastell-infos-iparapheur'>
        <legend><?php echo __('Configuration iparapheur'); ?></legend>
        <?php
        echo $this->BsForm->input('pastell_parapheur_type', [
            'type' => 'text',
            'label' => __('Type technique (Parapheur)'),
            'placeholder' => __('Actes'),
            'autocomplete' => 'off',
            'value' => Configure::read('PASTELL_PARAPHEUR_TYPE'),
        ]);
        echo $this->BsForm->input('pastell_visa_type', [
            'type' => 'text',
            'label' => __('Type de dossier "visa"'),
            'placeholder' => __('ls-document-pdf'),
            'autocomplete' => 'off',
            'value' => Configure::read('PASTELL_VISA_TYPE'),
        ]);
        ?>
    </fieldset>
    <fieldset class="iparapheur-infos">
        <legend><?php echo __('Authentification'); ?></legend>
        <?php
        echo $this->BsForm->input('host', [
            'type' => 'text',
            'placeholder' => __('https://iparapheur.x.x.org'),
            'label' => __('Server'),
            'autocomplete' => 'off',
            'value' => Configure::read('IPARAPHEUR_HOST')]);
        echo $this->BsForm->input('uri', [
            'type' => 'text',
            'placeholder' => __('ws-iparapheur'),
            'label' => __('Uri'),
            'autocomplete' => 'off',
            'value' => Configure::read('IPARAPHEUR_URI')]);
        echo $this->BsForm->input('login', [
            'type' => 'text',
            'placeholder' => __('Nom d\'utilisateur'),
            'label' => __('Nom d\'utilisateur'),
            'autocomplete' => 'off',
            'value' => Configure::read('IPARAPHEUR_LOGIN')]);

        echo '<div id = "show_hide_password">'.$this->BsForm->inputGroup(
                'pwd',
                [
                    [
                        'content' => $this->Bs->icon('eye'),
                        'title' => __('Afficher le mot de passe'),
                        'type' => 'button',
                    ]
                ],
                [
                    'type' => 'password',
                    'placeholder' => __('Mot de passe'),
                    'label' => __('Mot de passe'),
                    'autocomplete' => 'off',
                    'value' => Configure::read('IPARAPHEUR_PWD'),
                ],
                [
                    'before' => '<div class="col-md-9 col-md-offset-0">',
                    'multiple' => true,
                    'side' => 'right'
                ]
            ). '</div>';
?>
    </fieldset>
    <fieldset class="iparapheur-infos">
        <legend>Configuration</legend>
        <?php
        echo $this->BsForm->input('type', [
            'type' => 'text',
            'autocomplete' => 'off',
            'label' => __('Type technique'),
            'help' => '',
            'placeholder' => __('Actes'),
            'value' => Configure::read('IPARAPHEUR_TYPE'),
        ]);
        ?>
    </fieldset>
</div>
<?php
echo $this->Html2->btnSaveCancel('', ['controller' => 'connecteurs', 'action' => 'index']);
echo $this->BsForm->end();
?>
<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {

        $('#ConnecteurUseSignatureTrue').on('change', function (){
            changeActivation($(this));
        });
        $('#ConnecteurUseSignatureFalse').on('change', function (){
            changeActivation($(this));
        });

        changeProtocol();
        $('#ConnecteurSignatureProtocol').on('change', function (){
            changeProtocol($(this));
        });
        $('#ConnecteurPastellParapheurMode').on('change', function (){
            changeProtocol($(this));
        });

        function changeActivation(element) {
            if ($(element).val() == 'true') {
                $('#config_content').show();
            } else {
                $('#config_content').hide();
            }
        }

        function changeProtocol() {
            var protocol = $('#ConnecteurSignatureProtocol').val();
            if (protocol == 'PASTELL') {
                $('.pastell-infos').show();
                $('.iparapheur-infos').hide();
            } else {
                $('.pastell-infos').hide();
            }
            if (protocol == 'IPARAPHEUR') {
                $('.iparapheur-infos').show();
                $('.pastell-infos-iparapheur').hide();
            } else {
                var type = $('#ConnecteurPastellParapheurMode').val();
                $('.iparapheur-infos').hide();
                $('.pastell-infos-iparapheur').hide();
                if (type == 'IPARAPHEUR') {
                    $('.pastell-infos-iparapheur').show();
                } else {
                    $('.pastell-infos-iparapheur').hide();

                }
            }
        }
    });
});
</script>
