<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Connecteurs'), [
    'admin' => true,
    'prefix' => 'admin',
    'controller' => 'connecteurs',
    'action' => 'index']
);
$this->Html->addCrumb($titre);

    echo $this->Bs->tag('h3', $titre);
    if (empty($flux_pastell)) {
        echo $this->Bs->alert('<strong>' . __('Attention !') . '</strong> ' . __('Connecteur pastell désactivé. Le fichier pastell.inc est introuvable.'), 'danger');
    }
    echo $this->BsForm->create('Connecteur', [
      'url' => ['controller' => 'connecteurs', 'action' => 'makeconf', 'pastell'],
      'type' => 'file',
      'novalidate' => false,
      'id' => 'appConnecteur']);
    ?>
    <fieldset>
        <legend><?php echo __('Activation'); ?></legend>
        <?php
        echo $this->BsForm->radio('use_pastell', ['true' => __('Oui'), 'false' => __('Non')], [
            'value' => Configure::read('USE_PASTELL') ? 'true' : 'false',
            'autocomplete' => 'off'
            ]);
        ?>
    </fieldset>
    <div class='spacer'> </div>
    <div id='config_content' <?php echo Configure::read('USE_PASTELL') === false ? 'style="display: none;"' : ''; ?>>
        <fieldset class="Pastell-infos">
            <legend><?php echo __('Connexion'); ?></legend>
            <?php
            echo $this->BsForm->input('host', ['type' => 'text',
                'placeholder' => __('https://pastell.maville.fr'),
                'label' => __('URL du serveur pastell'),
                'value' => Configure::read('PASTELL_HOST')]) . '<br />';
            echo $this->BsForm->input('login', ['type' => 'text',
                'placeholder' => __('Nom d\'utilisateur'),
                'label' => __('Nom d\'utilisateur'),
                'autocomplete' => 'off',
                'value' => Configure::read('PASTELL_LOGIN')]) . '<br />';
            echo '<div id = "show_hide_password">'.$this->BsForm->inputGroup(
                'pwd',
                [
                    [
                        'content' => $this->Bs->icon('eye'),
                        'title' => __('Afficher le mot de passe'),
                        'type' => 'button',
                    ]
                ],
                [
                    'type' => 'password',
                    'placeholder' => __('Mot de passe'),
                    'label' => __('Mot de passe'),
                    'autocomplete' => 'off',
                    'value' => Configure::read('PASTELL_PWD'),
                ],
                [
                    'before' => '<div class="col-md-9 col-md-offset-0">',
                    'multiple' => true,
                    'side' => 'right'
                ]
            ). '</div><br />';

//            echo $this->BsForm->input('pwd', ['type' => 'password',
//                'placeholder' => __('mot de passe'),
//                'required' => true,
//                'class' => 'show_hide_password',
//                'autocomplete' => 'off',
//                'value' => Configure::read('PASTELL_PWD')]) . '<br />';
            ?>
            <div class='spacer'> </div>
            <?php
            echo $this->Bs->row();
            echo $this->Bs->col('xs' . $this->BsForm->getLeft()) . $this->Bs->close();
            echo $this->Bs->col('xs' . $this->BsForm->getRight());
            echo $this->Bs->div('btn-group') .
            $this->Bs->btn($this->Bs->icon('save') . ' ' . __('Enregistrer'), 'javascript:void(0);', [
                'type' => 'success',
                'id' => 'btnSaveDataConnexion',
                'title' => __('Enregistrer les informations de connexion'),
                'escapeTitle' => false]) .
            $this->Bs->btn($this->Bs->icon('sync') . ' ' . __('Récupérer la liste des collectivités'), 'javascript:void(0);', [
            'type' => 'primary',
            'id' => 'btnRefreshEntities',
            'title' => __('Récupérer la liste des collectivités'),
            'disabled' => (empty(Configure::read('PASTELL_HOST'))
                xor empty(Configure::read('PASTELL_LOGIN'))
                xor empty(Configure::read('PASTELL_PWD'))
            ),
            'escapeTitle' => false]) .
            $this->Bs->close();
            echo $this->html->tag('em', __('Note : Pour récupérer la liste des collectivités actualisées, veuillez enregistrer toutes les informations de connexion.'), ['class' => 'help-block']) ;
            echo $this->Bs->tag('br');
            echo $this->Bs->tag('br');
        echo $this->Bs->close(2);
        ?>

        </fieldset>
        <fieldset class="Pastell-infos">
            <legend><?php echo __('Configuration'); ?></legend>
            <?php
              echo $this->BsForm->select(
            'Collectivite.id_entity',
            $entities,
            [
                  'selected' => $selected,
                  'class' => 'selectone',
                  'data-allow-clear' => true,
                  'data-placeholder' => __('Sélectionner une collectivité'),
                  'help' => __('Pour charger la liste des collectivités, veuillez cliquer sur le bouton "Récupérer la liste des collectivités".'),
                  'label' => __('Collectivité')]
        );
            echo $this->BsForm->input('type', ['type' => 'text',
                'placeholder' => __('actes-generique'),
                'label' => __('Flux'),
                'help' => __('Veuillez renseigner le nom du flux utilisé dans pastell pour la dématérialisation des actes.'),
                'value' => Configure::read('PASTELL_TYPE')]);
            ?>
        </fieldset>
    </div>
    <div class='spacer'> </div>
<?php
echo $this->Html2->btnSaveCancel('', ['controller' => 'connecteurs', 'action' => 'index']);
echo $this->BsForm->end();
?>
