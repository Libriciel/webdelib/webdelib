<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Connecteurs'), ['admin' => true, 'prefix' => 'admin', 'controller' => 'connecteurs', 'action' => 'index']);
$this->Html->addCrumb($titre);

echo $this->Bs->tag('h3', $titre);
echo $this->BsForm->create('Connecteur', ['url' => ['admin' => true, 'prefix' => 'admin', 'controller' => 'connecteurs', 'action' => 'makeconf', 'mailsec'], 'type' => 'file']);
?>
<fieldset>
    <legend><?php echo __('Activation'); ?></legend>
    <?php
        echo $this->BsForm->radio('use_mailsec', ['true' => __('Oui'), 'false' => __('Non')], [
            'value' => Configure::read('USE_MAILSEC') ? 'true' : 'false',
            'autocomplete' => 'off'
            ]);
    ?>
</fieldset>
<div class='spacer'></div>
<div id='config_content' <?php echo Configure::read('USE_MAILSEC') === false ? 'style="display: none;"' : ''; ?>>
    <fieldset>
        <legend><?php echo __('Type de mail sécurisé'); ?></legend>
        <?php
        echo $this->BsForm->select('mailsec_protocol', $protocoles, [
            'class' => 'selectone',
            'label' => __('Protocole'),
            'autocomplete' => 'off',
            'value' => Configure::read('MAILSEC')
        ]);
        ?>
    </fieldset>
    <fieldset class="mailsec-infos">
    <div class='spacer'></div>

            <legend><?php echo __('Mot de passe'); ?></legend>
            <?php
            echo $this->BsForm->radio('s2low_mailsec_pwd_use', ['true' => __('Oui'), 'false' => __('Non')], [
                //'options' => $true_false,
                'autocomplete' => 'off',
                'value' => Configure::read('S2LOW_MAILSEC_PWD_USE') ? 'true' : 'false',
                'default' => 'false'
            ]);

            ?>
            <div class='spacer'></div>
            <div id="mails_password" <?php if (!Configure::read('USE_MAILSEC')) {
                echo ' style="display: none;"';
            } ?>>
                    <?php
                    echo $this->BsForm->input('s2low_mailsec_pwd', [
                        'type' => 'password',
                        'autocomplete' => 'off',
                        'placeholder' => __('Mot de passe mail sécurisé'),
                        'value' => Configure::read('S2LOW_MAILSEC_PWD'),
                        'label' => __('Mot de passe')
                    ]);
                    ?>
            </div>
        </fieldset>
    <div id='config_content'>
        <fieldset class="pastell-infos">
            <legend><?php echo __('Connexion'); ?></legend>
            <?php
            echo $this->BsForm->input('mailsec_host', ['type' => 'text',
                'placeholder' => __('https://pastell.maville.fr'),
                'label' => __('URL du serveur pastell'),
                'value' => Configure::read('PASTELL_MAILSEC_HOST')]) . '<br />';
            echo $this->BsForm->input('mailsec_id_e', ['type' => 'text',
                'placeholder' => __('Identifiant de collectivité (id_e)'),
                'label' => __('Identifiant de l\'entité'),
                'value' => Configure::read('PASTELL_MAILSEC_IDE')]) . '<br />';
            echo $this->BsForm->input('mailsec_login', ['type' => 'text',
                'placeholder' => __('Nom d\'utilisateur'),
                'label' => __('Nom d\'utilisateur'),
                'value' => Configure::read('PASTELL_MAILSEC_LOGIN')]) . '<br />';
            echo '<div id = "show_hide_password">'.$this->BsForm->inputGroup(
                    'mailsec_pwd',
                    [
                        [
                            'content' => $this->Bs->icon('eye'),
                            'title' => __('Afficher le mot de passe'),
                            'type' => 'button',
                        ]
                    ],
                    [
                        'type' => 'password',
                        'placeholder' => __('Mot de passe'),
                        'label' => __('Mot de passe'),
                        'autocomplete' => 'off',
                        'value' => Configure::read('PASTELL_MAILSEC_PWD'),
                    ],
                    [
                        'before' => '<div class="col-md-9 col-md-offset-0">',
                        'multiple' => true,
                        'side' => 'right'
                    ]
                ). '</div><br />';
            ?>
        </fieldset>
        <fieldset class="pastell-infos">
            <legend><?php echo __('Configuration'); ?></legend>
            <?php
            echo $this->BsForm->input('mailsec_type', ['type' => 'text',
                'placeholder' => __('mailsec'),
                'label' => __('Flux'),
                'help' => __('Veuillez renseigner le nom du flux utilisé dans pastell pour l\'envoi des mails sécurisés.'),
                'value' => Configure::read('PASTELL_MAILSEC_FLUX')]);
            ?>
        </fieldset>
    </div>
    <div class='spacer'> </div>
</div>
<?php
echo $this->Html2->btnSaveCancel('', ['controller' => 'connecteurs', 'action' => 'index']);
echo $this->BsForm->end();
?>
<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {

        $('#ConnecteurUseMailsecTrue').on('change', function (){
            changeActivation($(this), 'config_content');
        });
        $('#ConnecteurUseMailsecFalse').on('change', function (){
            changeActivation($(this), 'config_content');
        });

        changeProtocol();
        $('#ConnecteurMailsecProtocol').on('change', function (){
            changeProtocol($(this));
        });


        $('#ConnecteurS2lowMailsecPwdUseTrue').on('change', function (){
            changeActivation($(this), 'mails_password');
        });
        $('#ConnecteurS2lowMailsecPwdUseFalse').on('change', function (){
            changeActivation($(this), 'mails_password');
        });

        function changeActivation(element, panel) {
            if ($(element).val() == 'true') {
                $('#'+ panel).show();
            } else {
                $('#'+ panel).hide();
            }
        }

        function changeProtocol() {
            var protocol = $('#ConnecteurMailsecProtocol').val();
            if (protocol == 'PASTELL') {
                $('.pastell-infos').show();
            } else {
                $('.pastell-infos').hide();
            }
            if (protocol == 'S2LOW') {
                $('.mailsec-infos').show();
            } else {
                $('.mailsec-infos').hide();
            }
        }
    });
});
</script>
