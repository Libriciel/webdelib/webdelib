<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Connecteurs'));

echo $this->Bs->tag('h3', __('Gestion des connecteurs')) .
 $this->Bs->table([['title' => __('Connecteur')], ['title' => __('Actions')]], ['hover', 'striped']);

foreach ($connecteurs as $id => $connecteur) {
    echo $this->Bs->cell($connecteur);
    echo $this->Bs->cell($this->Bs->div('btn-group btn-group-right') .
            $this->Bs->btn($this->Bs->icon('pencil-alt'), ['controller' => 'connecteurs', 'action' => 'edit', $id], ['escapeTitle' => false, 'type' => 'primary', 'title' => __('Gérer le connecteur : ') . $connecteur]) .
            $this->Bs->close());
}
echo $this->Bs->endTable();

//echo $this->Bs->tag('h4', __('Delegation des circuits', true));

//echo $this->element('ConnectorManager.connectors', array()); 