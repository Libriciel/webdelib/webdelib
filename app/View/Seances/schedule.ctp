<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Séances à traiter'), ['controller' => 'seances', 'action' => 'index']);

echo $this->Bs->tag('h3', __('Ordre du jour de la séance du %s à %s', $this->Time->i18nFormat($date_seance, '%e %B %Y'), $this->Time->i18nFormat($date_seance, '%k h %M')) . ' ('. count($this->data). ' projets)');
$this->Html->addCrumb(__('Ordre du jour'));

// select masqués utilisés par le javascript
//echo $this->Form->input('Deliberation.position', array('options'=>$lst_pos, 'id'=>'selectOrdre', 'label'=>false, 'div'=>false, 'style'=>"display:none; width: auto;", 'onChange'=>"onChangeSelectOrdre(this.value);"));
//echo $this->Form->input('Deliberation.rapporteur_id', array('options'=>$rapporteurs, 'empty'=>true, 'id'=>'selectRapporteur', 'label'=>false, 'div'=>false, 'style'=>"display:none;", 'onChange'=>"onChangeSelectRapporteur(this.value);"));
//cho $this->Form->hidden('Aplication.url', array('value'=>FULL_BASE_URL.$this->webroot));
//echo $this->Form->hidden('Aplication.seanceid', array('value'=>$seance_id));

echo $this->Bs->table([
    ['title' => __('Ordre')],
    ['title' => __('id')],
    ['title' => __('etat')],
    ['title' => $this->Html->link(__('Thème'), ['controller' => 'seances', 'action' => 'sortby',
            $seance_id, 'theme_id'], ['confirm' => __('Êtes-vous sûr de vouloir trier par thème ?')])],
    ['title' => $this->Html->link(__('Service émetteur'), ['controller' => 'seances', 'action' => 'sortby',
            $seance_id, 'service_id'], ['confirm' => __('Êtes-vous sûr de vouloir trier par service ?')])],
    ['title' => $this->Html->link(__('Rapporteur'), ['controller' => 'seances', 'action' => 'sortby',
            $seance_id, 'rapporteur_id'], ['confirm' => __('Êtes-vous sûr de vouloir trier par rapporteur ?')])],
    ['title' => $this->Html->link(__('Libellé'), ['controller' => 'seances', 'action' => 'sortby',
            $seance_id, 'objet'], ['confirm' => __('Êtes-vous sûr de vouloir trier par libellé ?')])],
    ['title' => $this->Html->link(__('Titre'), ['controller' => 'seances', 'action' => 'sortby',
            $seance_id, 'titre'], ['confirm' => __('Êtes-vous sûr de vouloir trier par titre ?')])],
    ['title' => __('Actions')],
  ], ['hover', 'striped'], ['attributes' => ['id' => 'appSeance']]);
$this->BsForm->setFormType('vertical');
foreach ($this->data as $projet) {

    //$this->Html->link($delibPosition, "javascript:onClickLinkOrdre(".$delibPosition.", ".$projet['Deliberation']['id'].");", array('id'=>'linkOrdre'.$delibPosition))
    if (!empty($projet['iconeEtat']['image'])) {
        if (in_array($projet['iconeEtat']['image'], ['encours', 'circuit_user_fini', 'attente_circuit', 'circuit_user_atraiter', 'circuit_redacteur_attente', 'circuit_attente'], true)) {
            $projet['iconeEtat']['status'] = 'warning';
        }
    }

    $etat = $this->Bs->btn(
        $this->Bs->icon($this->ProjetUtil->etat_icon($projet['iconeEtat']['image']), ['xs','iconType' =>  $this->ProjetUtil->etat_iconType($projet['iconeEtat']['image'])]),
        null,
        [
        'tag' => 'button',
        'type' => empty($projet['iconeEtat']['status']) ? $this->ProjetUtil->etat_icon_type($projet['iconeEtat']['image']) : $projet['iconeEtat']['status'],
        'size' => 'xs',
        'option_type' => 'button',
        //'disabled'=>'disabled',
        'data-toggle' => 'popover',
        'data-content' => $this->ProjetUtil->etat_icon_help($projet['iconeEtat']['image']),
        'data-placement' => 'right',
        'escape' => false,
        'title' => $projet['iconeEtat']['titre']
            ]
    );

    echo $this->Bs->cell(
            $this->BsForm->select('DeliberationSeance.position', $aPosition, [
                'value' => $projet['DeliberationSeance']['position'],
                'autocomplete' => 'off',
                'label' => false,
                'class' => 'input-sm selectone wd-change-position',
                'id' => 'DelibOrdreId' . $projet['Deliberation']['id'],
                'data-minimum-results-for-search' => 10,
                'data-wd-seance-id' => $seance_id,
                'data-wd-projet-id' => $projet['Deliberation']['id'],
            ]) .
            $this->Bs->btn(null, ['controller' => 'seances',
                'action' => 'changePosition',
                $seance_id, $projet['Deliberation']['id']], [
                'style' => 'Display:none',
                'id' => 'DelibOrdreId' . $projet['Deliberation']['id'] . 'link'])
    );

    echo $this->Bs->cell($projet['Deliberation']['id']);

    echo $this->Bs->cell($etat);

    /* ($delibPosition != 1?
      $this->Html->link(null, '/deliberations/positionner/' . $seance_id . '/' . $projet['Deliberation']['id'] . '/-1', array('class' => 'link_monter', 'title' => 'Monter', 'escape' => false), false):'').
      ($delibPosition != $lastPosition?
      $this->Html->link(null, '/deliberations/positionner/' . $seance_id . '/' . $projet['Deliberation']['id'] . '/1', array('class' => 'link_descendre', 'title' => 'Descendre', 'escape' => false), false):'')
     */
    echo $this->Bs->cell('[' . $projet['Theme']['order'] . '] ' . $projet['Theme']['libelle']);
    echo $this->Bs->cell($projet['Service']['name']);
    echo $this->Bs->cell(
        $this->BsForm->select('deliberation.rapporteur_id', $rapporteurs, [
                'default' => $projet['Deliberation']['rapporteur_id'],
                'class' => 'input-sm selectone select_rapporteur',
                'data-delib-id' => $projet['Deliberation']['id'],
                'data-seance-id' => $seance_id,
                'data-placeholder' => __('Sélectionner un rapporteur'),
                'data-allow-clear' => true,
                'autocomplete' => 'off',
                'id' => 'DelibRappId' . $projet['Deliberation']['id'],
                'empty' => true
            ]) .
            $this->Bs->btn(null, ['controller' => 'seances',
                'action' => 'changeRapporteur',
                $seance_id, $projet['Deliberation']['id']], [
                'style' => 'Display:none',
                'id' => 'DelibRappId' . $projet['Deliberation']['id'] . 'link'])
    );
    echo $this->Bs->cell($projet['Deliberation']['objet_delib']);
    echo $this->Bs->cell($projet['Deliberation']['titre']);


    echo $this->Bs->cell(
            $this->Bs->div('btn-group') .
            $this->Bs->btn($this->Bs->icon('eye', null, ['stylePrefix' => 'far']), ['controller' => 'projets', 'action' => 'view', $projet['Deliberation']['id']], ['type' => 'default', 'escapeTitle' => false, 'title' => __('Visualiser le projet')]) .
            (   (  in_array('edit',$projet['Actions']) && !$projet['Deliberation']['signee'] && $projet['Deliberation']['isLocked'] && $this->Session->read('Auth.User.Profil.role_id') != 2) ?
                $this->Bs->btn(
                    $this->Bs->icon('pencil-alt'),
                    [
                        'controller' => 'projets',
                        'action' => 'edit',
                        $projet['Deliberation']['id']
                    ],
                    [
                        'type' => 'primary',
                        'tag' => 'button',
                        'option_type'=> 'button',
                        'disabled' =>  'disabled',
                        'escape' => false,
                        'title' => __('Modifier le projet')
                    ]
                )
                :
                ( in_array('edit',$projet['Actions']) && !$projet['Deliberation']['signee']) ? (
                    $this->Bs->btn(
                        $this->Bs->icon('pencil-alt'),
                        [
                            'controller' => 'projets',
                            'action' => 'edit',
                            $projet['Deliberation']['id']],
                        [
                    'type' => 'primary',
                    'option_type'=> 'button',
                    'escape' => false,
                    'title' => __('Modifier le projet'),
                    //Gestion de l'affichage lorsque un verrou est présent sur une délibération.
                    'tag'=> ($projet['Deliberation']['isLocked'] && $this->Session->read('Auth.User.Profil.role_id') != 2) ? 'button': null,
                    'disabled'=> ($projet['Deliberation']['isLocked'] && $this->Session->read('Auth.User.Profil.role_id') != 2) ? 'disabled': null,
                    'confirm' => ($projet['Deliberation']['isLocked'] && $this->Session->read('Auth.User.Profil.role_id') == 2) ? "Souhaitez-vous enlever le verrou sur ce projet ?" : null,
                ]))
                :null)
            .
            $this->Bs->btn($this->Bs->icon('cog'), [
                'controller' => 'projets', 'action' => 'genereFusionToClientByModelTypeNameAndSeanceId', 'projet', $seance_id, $projet['Deliberation']['id']
            ], [
                    'type' => 'default',
                    'class' => 'waiter',
                    'escapeTitle' => false,
                    'data-waiter-send' => 'XHR',
                    'data-waiter-callback' => 'getProgress',
                    'data-waiter-type' => 'progress-bar',
                    'data-waiter-title' => __('Générer le document du projet'),
                    'title' => __('Générer le document du projet')
            ])
            .
            $this->Bs->close()
    );
}
echo $this->Bs->endTable() .
 $this->Html2->btnCancel();
if ($is_deliberante) {
    echo $this->Bs->btn(
        $this->Bs->icon('clock') . ' ' . __('Reporter l\'ordre du jour'),
        [
        'action' => 'reportePositionsSeanceDeliberante', $seance_id],
        [
        'type' => 'success',
        'class' => 'pull-right',
        'name' => 'Retour',
        'escape' => false]
    );
}
