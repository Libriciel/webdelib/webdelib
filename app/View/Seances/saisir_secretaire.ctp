<?php

$this->Html->addCrumb(__('Séances à traiter'), ['controller' => 'seances', 'action' => 'index']);

$this->Html->addCrumb(__('Sélection Président/Secrétaire'));


echo $this->BsForm->create('Seances', ['url' => ['controller' => 'seances', 'action' => 'saisirSecretaire', $seance_id], 'type' => 'post']);

echo $this->BsForm->select('Acteur.president_id', $acteurs, ['label' => __('Président de séance'), 'class' => 'selectone', 'autocomplete' => 'off', 'data-placeholder'=> __('Sélectionner le président de séance'), 'data-allow-clear'=> true, 'value' => !empty($selectedPresident) ? $selectedPresident : null, 'empty' => true]);
echo $this->BsForm->select('Acteur.secretaire_id', $acteurs, ['label' => __('Secrétaire de séance'), 'class' => 'selectone', 'data-placeholder'=> __('Sélectionner le secrétaire de séance'), 'data-allow-clear'=> true, 'autocomplete' => 'off', 'value' => !empty($selectedActeurs) ? $selectedActeurs : null, 'empty' => true]);

echo $this->Html2->btnSaveCancel('', $previous);
echo $this->BsForm->end();
