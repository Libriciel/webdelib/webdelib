<div class="seances">
    <h2><?php echo __('Séances traitées'); ?></h2>
    <table class="table table-striped">
        <thead>
            <tr>
                <th><?php echo __('Type'); ?></th>
                <th><?php echo __('Date Séance'); ?></th>
                <th><?php echo __('Action'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $numLigne = 1;
            foreach ($seances as $seance):
                $rowClass = ($numLigne & 1) ? ['height' => '36px'] : ['height' => '36px', 'class' => 'altrow'];
                echo $this->Html->tag('tr', null, $rowClass);
                $numLigne++;
                ?>
            <td><?php echo $seance['Typeseance']['libelle']; ?></td>
            <td><?php echo $this->Time->i18nFormat($seance['Seance']['date'], '%d/%m/%Y à %k:%M'); ?></td>
            <td class="actions">
                <?php echo $this->Html->link(SHY, ['controller' => 'seances', 'action' => 'saisirDebatGlobal', $seance['Seance']['id']], ['class' => 'link_debat', 'escape' => false, 'title' => 'Saisir les débats généraux de la séance']); ?>
            </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
