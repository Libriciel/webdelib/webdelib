<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Séances à traiter'), [
    'controller' => 'seances', 'action' => 'index'
]);
if ($this->data['Typeseance']['action'] === 0) {
    $this->Html->addCrumb(
        $this->data['Typeseance']['libelle'] . ' '
        . __('du') . ' '
        . $this->Time->i18nFormat($this->data['Seance']['date'], '%d/%m/%Y'). __(' à ')
        . $this->Time->i18nFormat($this->data['Seance']['date'], '%kh%M'), [
            'controller' => 'votes',
            'action' => 'index',
            $this->data['Seance']['id']
        ]);
}
if ($this->data['Typeseance']['action'] === 1) {
    $this->Html->addCrumb(
        $this->data['Typeseance']['libelle'] . ' ' . __('du')
        . ' ' . $this->Time->i18nFormat($this->data['Seance']['date'], '%d/%m/%Y')
        . __(' à ')
        . $this->Time->i18nFormat($this->data['Seance']['date'], '%kh%M'), [
            'controller' => 'avis',
            'action' => 'index',
            $this->data['Seance']['id']
        ]
    );
}
if ($this->data['Typeseance']['action'] === 2) {
    $this->Html->addCrumb('Post-séances', [
            'controller' => 'votes',
            'action' => 'index',
            $this->data['Seance']['id']
        ]
    );
}

$this->Html->addCrumb(__('Importer les procès-verbaux'));
echo $this->Html->tag(
    'h3',
    __(
        'Importer les procès-verbaux de la séance : %s du %s à %s',
        $this->data["Typeseance"]['libelle'],
        $this->Time->i18nFormat($this->data['Seance']['date'], '%d/%m/%Y'),
        $this->Time->i18nFormat($this->data['Seance']['date'], '%kh%M')
    )
);

echo $this->BsForm->create('Seance', [
    'url' => [
        'controller' => 'seances', 'action' => 'saveProcesVerbaux', $this->data['Seance']['id']], 'type' => 'file'
    ]
);

echo $this->html->tag('em', __(
    'Note : L\'enregistrement des procès-verbaux fige automatiquement les documents (procès-verbaux, débats) à la cloture de la séance.'
), ['class' => 'help-block']);

echo $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
    $this->Html->tag('div', __('Pv sommaire'), ['class' => 'panel-heading'])
    .  $this->Html->tag('div', null, ['class' => 'panel-body']);

$pv_sommaire_as_file = strlen($this->data['Seance']['pv_sommaire']) > 0;

if ($pv_sommaire_as_file) {
    echo $this->Bs->div('media viewSeancePvsommaire') .
        $this->Bs->link($this->Bs->icon('file-alt', ['4x']), '#', [
                'class' => 'media-left', 'escape' => false]
        ) .
        $this->Bs->div('media-body') .
        $this->Bs->tag('h4', $this->data['Typeseance']['Modele_pvsommaire']['name'], ['class' => 'media-heading']) .
        $this->Bs->div('btn-group') .
        $this->Bs->btn($this->Bs->icon('trash') . ' ' . __('Supprimer'), '#', [
            'type' => 'danger',
            'size' => 'xs',
            'class' => 'media-left wd-pv-delete',
            'data-wd-file-id' => 'SeancePvsommaire',
            'escape' => false,
            'confirm' => __('Voulez-vous vraiment supprimer le document %s ?',
                $this->data['Typeseance']['Modele_pvsommaire']['name']
            )
        ]) .
        $this->Bs->btn($this->Bs->icon('download') . ' ' . __('Télécharger'), [
            'controller' => 'postseances',
            'action' => 'download',
            $this->data['Seance']['id'], 'sommaire'], [
            'type' => 'default',
            'size' => 'xs',
            'class' => 'media-left',
            'escape' => false,
        ]) .
        $this->Bs->close(3) . $this->Bs->tag('br/', null);
}
    echo $this->Bs->row([
        'class' => 'choisir_fichier_SeancePvsommaire',
        'style' => $pv_sommaire_as_file ? 'display:none' : ''
        ]) .
        $this->Bs->col('xs8') .
        $this->BsForm->input('Seance.pvsommaire', [
            'label' => $this->data['Typeseance']['Modele_pvsommaire']['name'],
            'type' => 'file',
            'accept' => "application/pdf",
            'data-btnClass' => 'btn-primary',
            'data-text' => __('Choisir un fichier'),
            'data-placeholder' => __('Pas de fichier'),
            'data-badge' => false,
            'disabled' => $pv_sommaire_as_file,
            'error' => false,
            'help' => __('Les modifications apportées ici ne prendront effet que lors de l\'enregistrement.'),
            'class' => 'filestyle']) . $this->Bs->close() .
        $this->Bs->col('xs4') .
        $this->Bs->div('btn-group btn-group-right') .
        $this->Bs->btn($this->Bs->icon('eraser') . ' ' . __('Effacer'), '#', [
            'type' => 'danger',
            'escapeTitle' => false,
            'class' => 'btn-danger-right wd-filestyle-clear',
            'data-wd-filestyle-id' => 'SeancePvsommaire',
        ]) .
        $this->Bs->close(3) . ($this->Form->isFieldError('Seance.pv_sommaire') ?
            $this->Form->error('Seance.pv_sommaire') : '');

echo $this->Bs->close(2);

echo $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
    $this->Html->tag('div', __('Pv détaillé'), ['class' => 'panel-heading'])
    .  $this->Html->tag('div', null, ['class' => 'panel-body']);
$pv_complet_as_file = strlen($this->data['Seance']['pv_complet']) > 0;

if ($pv_complet_as_file) {
    echo $this->Bs->div('media viewSeancePvcomplet') .
        $this->Bs->link($this->Bs->icon('file-alt', ['4x']), '#', [
                'class' => 'media-left', 'escape' => false]
        ) .
        $this->Bs->div('media-body') .
        $this->Bs->tag('h4', $this->data['Typeseance']['Modele_pvdetaille']['name'], ['class' => 'media-heading']) .
        $this->Bs->div('btn-group') .
        $this->Bs->btn($this->Bs->icon('trash') . ' ' . __('Supprimer'), '#', [
            'type' => 'danger',
            'size' => 'xs',
            'class' => 'media-left wd-pv-delete',
            'data-wd-file-id' => 'SeancePvcomplet',
            'escape' => false,
            'confirm' => __('Voulez-vous vraiment supprimer le document %s ?',
                $this->data['Typeseance']['Modele_pvdetaille']['name']
            )
        ]) .
        $this->Bs->btn($this->Bs->icon('download') . ' ' . __('Télécharger'), [
            'controller' => 'postseances',
            'action' => 'download',
            $this->data['Seance']['id'], 'complet'], [
            'type' => 'default',
            'size' => 'xs',
            'class' => 'media-left',
            'escape' => false,
        ]) .
        $this->Bs->close(3) . $this->Bs->tag('br/', null);
}
echo $this->Bs->row([
        'class' => 'choisir_fichier_SeancePvcomplet',
        'style' => $pv_complet_as_file ? 'display:none' : ''
    ]) .
    $this->Bs->col('xs8') .
    $this->BsForm->input('Seance.pvcomplet', [
        'label' => $this->data['Typeseance']['Modele_pvdetaille']['name'],
        'type' => 'file',
        'accept' => "application/pdf",
        'data-btnClass' => 'btn-primary',
        'data-text' => __('Choisir un fichier'),
        'data-placeholder' => __('Pas de fichier'),
        'data-badge' => false,
        'disabled' => $pv_complet_as_file,
        'error' => false,
        'help' => __('Les modifications apportées ici ne prendront effet que lors de l\'enregistrement.'),
        'class' => 'filestyle']) . $this->Bs->close() .
    $this->Bs->col('xs4') .
    $this->Bs->div('btn-group btn-group-right') .
    $this->Bs->btn($this->Bs->icon('eraser') . ' ' . __('Effacer'), '#', [
        'type' => 'danger',
        'escapeTitle' => false,
        'class' => 'btn-danger-right wd-filestyle-clear',
        'data-wd-filestyle-id' => 'SeancePvcomplet',
    ]) .
    $this->Bs->close(3) . ($this->Form->isFieldError('Seance.pv_complet') ?
        $this->Form->error('Seance.pv_complet') : '');

echo $this->Bs->close(2);

echo $this->BsForm->hidden('Seance.id');
echo $this->Html2->btnSaveCancel('', $previous);
echo $this->BsForm->end();

?>

<script type="text/javascript">
    require(['domReady'], function (domReady) {
        domReady(function () {
            $(".wd-pv-delete").on("click", function (){
                console.log($(this).attr("data-wd-file-id"));
                supprimerPv($(this).attr("data-wd-file-id"));
            });

            function supprimerPv(id) {
                $(".choisir_fichier_"+id).show();
                $(".view"+id).hide();
                $(".choisir_fichier_"+ id + " input:file").filestyle('disabled', false);
                $(".choisir_fichier_"+ id + " label").removeAttr('disabled');
            }
        });
    });
</script>
