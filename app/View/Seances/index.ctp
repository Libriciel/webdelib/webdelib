<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

if (!isset($this->request['render']) || $this->request['render'] != 'bannette') {
    if ($this->params['filtre'] != 'hide') {
        $this->BsForm->setLeft(0);
        $this->Html->addCrumb(__('Séances'));
        $this->Html->addCrumb(__('À traiter'));
        echo $this->Html->tag('h3', __('Séances à traiter'));

        /* echo $this->Bs->container(array('class'=>'-fluid')).
          $this->Bs->row().
          $this->Bs->col('xs12'). */
    }

    if (!empty($models) && !empty($seances)) {
        echo $this->BsForm->create('Seance', [
            'url' => ['controller' => 'seances',
                'action' => 'genereFusionMultiSeancesToClient'],
            'data-waiter-send' => 'XHR',
            'data-waiter-title' => __('Génération du document'),
            'data-waiter-type' => 'progress-bar',
            'data-waiter-callback' => 'getProgress',
            'id' => 'appSeance',
            'class' => 'waiter']);
    }

    if ($this->permissions->check('Seances', 'create')) {
        echo $this->Html->link($this->Bs->icon('plus') . ' ' . __('Créer une séance'), ["action" => "add"], ['class' => 'btn btn-primary',
                    'escape' => false,
                    'title' => __('Créer une séance'),
                    'style' => 'margin-top: 10px;']);
    }
}

if (isset($this->request['render']) && $this->request['render'] == 'bannette') {
    echo $this->Bs->div('panel panel-primary');
    echo $this->Bs->div(
        'panel-heading',
        $this->Bs->row() .
            $this->Bs->col('xs8') .
            $this->Bs->tag('h4', __('Séances à traiter')) .
            $this->Bs->close() .
            $this->Bs->col('xs4') .
            $this->Bs->tag('p', $this->Bs->btn(
                $this->Bs->icon('list-ul') .' '.
                __('Visualiser toutes les séances à traiter'),
                [
                        'controller' => $this->request['controller'],
                        'action' => $this->request['action']
                            ],
                [
                        'type' => 'default',
                        'escapeTitle' => false,
                            ]
            ), ['class' => 'text-right']) .
            $this->Bs->close(2)
    );
}

if (!isset($this->request['render']) || $this->request['render'] != 'bannette') {
    $titles[] = ['title' => $this->BsForm->checkbox(null, [
            'id' => 'masterCheckbox',
            'inline' => 'inline',
            'autocomplete' => 'off'
    ])];
} else {
    $titles[] = ['title' => ''];
}
$titles[] = ['title' => __('Type')];
$titles[] = ['title' => __('Date')];
$titles[] = ['title' => __('Associées')];
$titles[] = ['title' => __('Traitements')];
if (in_array('ExportGedSeances/read', $actions, true)) {
    $titles[] = ['title' => __('Export GED')];
}
$titles[] = ['title' => __('Actions')];
echo $this->Bs->table($titles, ['drop', 'hover', 'striped']);

if (!empty($seances)) {
    foreach ($seances as $seance) {
        $seance_date = __('%s à %s', ucfirst($this->Time->i18nFormat($seance['Seance']['date'], '%A %e %B %Y')), $this->Time->i18nFormat($seance['Seance']['date'], '%k:%M'));

        if (!isset($this->request['render']) || $this->request['render'] != 'bannette') {
            echo $this->Bs->cell(
                $this->Form->checkbox('Seance.id_' . $seance['Seance']['id'], ['checked' => false, 'class' => 'masterCheckbox_checkbox'])
            );
        } else {
            echo $this->Bs->cell('');
        }

        // icone
        echo $this->Bs->cell(
            $this->Bs->icon('tag', ['lg'], (!empty($seance['Typeseance']['color']) ? ['style' => 'color: ' . $seance['Typeseance']['color']] : []))
          . ' ' . $seance['Typeseance']['libelle']
        );
        echo $this->Bs->cell($seance_date);

        $data_content = '';
        if (!empty($seance['SeanceChildren'])) {
            foreach ($seance['SeanceChildren'] as $seanceChildren) {
                $data_content .= '- '.__(
                    '%s du %s à %s',
                    $seanceChildren['Typeseance']['libelle'],
                    CakeTime::i18nFormat($seanceChildren['date'], '%A %e %B %Y'),
                    CakeTime::i18nFormat($seanceChildren['date'], '%k:%M')
                ) . $this->Bs->tag('br /');
            }
        }

        if (!empty($seance['DeliberationSeance'])) {
            $projets = count($seance['DeliberationSeance']);
        } else {
            $projets = 0;
        }

        echo $this->Bs->cell(
            (!empty($seance['Seance']['parent_id']) ?
            $this->Bs->btn(
                $this->Bs->icon('link'),
                null,
                [
           'tag' => 'button',
           'type' => 'default',
           'option_type' => 'button',
           'title' => __('Séances Associées'),
           'data-toggle' => 'popover',
           'data-content' => __(
               '%s du %s à %s',
               $seance['SeanceParent']['Typeseance']['libelle'],
               CakeTime::i18nFormat($seance['SeanceParent']['date'], '%A %e %B %Y'),
               CakeTime::i18nFormat($seance['SeanceParent']['date'], '%k:%M')
           ),
           'data-placement' => 'right',
           'escapeTitle' => false
      ]
            ) : '').
            (!empty($seance['SeanceChildren']) ?
            $this->Bs->btn(
                $this->Bs->icon('link') . ' ' . '<span class="badge badge-counter">'.  count($seance['SeanceChildren']) . '</span>',
                null,
                [
                'tag' => 'button',
                'type' => 'default',
                'option_type' => 'button',
                'title' => __('Séances Associées'),
                'data-toggle' => 'popover',
                'data-content' => $data_content,
                'data-placement' => 'right',
                'escapeTitle' => false
           ]
            ): '')
        );


        // préparation de la séance
        $preparation = [];
        if (in_array('Typeseance/update', $seance['Actions'], true)) {
            $preparation[] = $this->Bs->link($this->Bs->icon('list-ol') . ' ' . __('Ordre des projets'). ' <span class="badge badge-project">' .  $projets . ' </span>', ['controller' => 'seances', 'action' => 'schedule', $seance['Seance']['id']], [
              'title' => __('Visualiser l\'ordre des projets de la séance du %s', $seance_date),
              'escapeTitle' => false
          ]);
        }

        $preparationEnvoyer = '';
        $preparationVisualiser = '<li>' .$this->Bs->link($this->Bs->icon('file-pdf', null, ['stylePrefix' => 'far']) . ' ' . $seance['Typeseance']['Modele_convocation']['name'], ['action' => 'genereFusionToClient', $seance['Seance']['id'], 'convocation'], [
            'class' => 'waiter',
            'tabindex' => '-1',
            'data-waiter-send' => 'XHR',
            'data-waiter-callback' => 'getProgress',
            'data-waiter-type' => 'progress-bar',
            'data-waiter-title' => __('Générer l\'aperçu d\'une convocation pour la séance du %s', $seance_date),
            'title' => __('Générer l\'aperçu d\'une convocation pour la séance du %s', $seance_date),
            'escapeTitle' => false,
        ]) . '</li>';
        if (in_array('Typeseance/update', $seance['Actions'], true)) {
            $preparationEnvoyer = '<li>'.$this->Bs->link($this->Bs->icon('paper-plane') . ' ' . $seance['Typeseance']['Modele_convocation']['name'], ['controller' => 'seances', 'action' => 'sendConvocations', $seance['Seance']['id'], $seance['Typeseance']['modele_convocation_id']], [
              'title' => __('Générer la liste des convocations pour la séance du %s', $seance_date),
              'escapeTitle' => false,
          ]) . '</li>';
        }
        $preparation[] = !empty($preparation) ? 'divider':'';
        $preparationVisualiser .= '<li>' .$this->Bs->link($this->Bs->icon('file-pdf', null, ['stylePrefix' => 'far']) . ' ' . $seance['Typeseance']['Modele_ordredujour']['name'], ['action' => 'genereFusionToClient', $seance['Seance']['id'], 'ordredujour'], [
            'class' => 'waiter',
            'tabindex' => '-1',
            'data-waiter-send' => 'XHR',
            'data-waiter-callback' => 'getProgress',
            'data-waiter-type' => 'progress-bar',
            'data-waiter-title' => __('Générer l\'aperçu de l\'ordre jour pour la séance du %s', $seance_date),
            'escapeTitle' => false,
            'title' => __('Générer l\'ordre du jour pour la séance du %s', $seance_date),
        ]) . '</li>';
        if (in_array('Typeseance/update', $seance['Actions'], true)) {
            $preparationEnvoyer .= '<li>'.$this->Bs->link($this->Bs->icon('paper-plane') . ' ' . $seance['Typeseance']['Modele_ordredujour']['name'], ['controller' => 'seances', 'action' => 'sendOrdredujour', $seance['Seance']['id'],
              $seance['Typeseance']['modele_ordredujour_id']], [
              'title' => __('Générer l\'ordre du jour détaillé pour la séance du %s', $seance_date),
              'escapeTitle' => false,
          ]) .'</li>';
        }

        if (in_array('sendToIdelibre/read', $actions, true)) {

            $preparation['idelibre'] = $this->Bs->link($this->Bs->icon('cloud-upload-alt')
                . ' ' . __('Envoyer à idelibre')
                ,
                [
                    'controller' => 'votes',
                    'action' => 'sendSeanceVotesToIdelibre',
                    $seance['Seance']['id']
                ],
                [
              'class' => 'waiter',
              'data-waiter-send' => 'XHR',
              'data-waiter-callback' => 'getProgress',
              'data-waiter-type' => 'progress-bar',
              'data-waiter-title' => __('Envoyer à idelibre la séance du  %s', $seance_date),
              'title' => __('Envoyer à idelibre la séance du  %s', $seance_date),
              'confirm' => !empty($seance['Seance']['idelibre_id'])
                  ? __('Cette séance a déjà été envoyée à idelibre !')
                  . "\n\n"
                  . __('Si vous souhaitez la renvoyer de nouveau')
                  . ",\n"
                  . __('Assurez-vous au préalable qu\'elle n\'est plus dans idelibre.')
                  . "\n\n"
                  . __('Voulez-vous continuer ?')
                  :false,
              'escapeTitle' => false
          ]);
            $preparation[] = 'divider';
        }

        $enCours = [];
        if (in_array('Typeseance/update', $seance['Actions'], true)) {
            // séance en cours
            $enCours[] = $this->Bs->link($this->Bs->icon('user') . ' ' . __('Sélection Président/Secrétaire'), ['action' => 'saisirSecretaire', $seance['Seance']['id']], [
              'title' => __('Choix du Président et du Secrétaire de la séance du  %s', $seance_date),
              'escapeTitle' => false,
          ]);

            $enCours[] = 'divider';
            if ($seance['Typeseance']['action'] == 0) {
                $enCours[] = $this->Bs->link($this->Bs->icon('gavel') . ' ' . __('Voter'),
                    [
                        'controller' => 'votes', $seance['Seance']['id']
                    ], [
                  'escape' => false,
                  'title' => __('Afficher les projets et voter pour la séance du  %s', $seance_date)
              ]);
            } elseif ($seance['Typeseance']['action'] === 1) {
                $enCours[] = $this->Bs->link($this->Bs->icon('hand-point-up') . ' ' . __('Donner un avis'),
                    [
                        'controller' => 'avis', $seance['Seance']['id']
                    ], [
                  'title' => __('Afficher les projets et donner un avis pour la séance du  %s', $seance_date),
                  'escapeTitle' => false,
              ]);
            } elseif ($seance['Typeseance']['action'] === 2) {
                $enCours[] = $this->Bs->link($this->Bs->icon('info') . ' ' . __('Afficher les projets'),
                    [
                        'controller' => 'votes', $seance['Seance']['id']
                    ], [
                  'escape' => false,
                  'title' => __('Afficher les projets pour la séance du  %s', $seance_date)
              ]);
            }
            $enCours[] = 'divider';
            $enCours[] = $this->Bs->link($this->Bs->icon('comment') . ' ' . __('Débats généraux'), ['action' => 'saisirDebatGlobal', $seance['Seance']['id']], [
              'title' => __('Saisir les débats généraux de la séance du  %s', $seance_date),
              'escapeTitle' => false,
          ]);
        }
        // finalisation
        $finalisation = [];
        if (in_array('sendDeliberationsToSignature', $seance['Actions'], true)) {
            $finalisation[] = $this->Bs->link($this->Bs->icon('award') . ' ' . __('Signatures'), [
                'controller' => 'signatures',
                'action' => 'index',
                $seance['Seance']['id']
            ], [
                'title' => __('Envoi des actes à la signature pour la séance du %s', $seance_date),
                'escapeTitle' => false
            ]);
            $finalisation[] = 'divider';
        }

        $finalisationVisualiser = '<li>' . $this->Bs->link($this->Bs->icon('file-pdf', null, ['stylePrefix' => 'far']) . ' ' . $seance['Typeseance']['Modele_pvsommaire']['name'], ['action' => 'genereFusionToClient', $seance['Seance']['id'], 'pvsommaire'], [
            'class' => 'waiter',
            'data-waiter-send' => 'XHR',
            'data-waiter-callback' => 'getProgress',
            'data-waiter-type' => 'progress-bar',
            'data-waiter-title' => __('Génération du PV sommaire pour la séance du %s', $seance_date),
            'title' => __('Générer le document de type PV sommaire pour la séance du %s', $seance_date),
            'escapeTitle' => false,
        ]) .'</li>';
        $finalisationVisualiser.= '<li>' .$this->Bs->link($this->Bs->icon('file-pdf', null, ['stylePrefix' => 'far']) . ' ' . $seance['Typeseance']['Modele_pvdetaille']['name'], ['action' => 'genereFusionToClient', $seance['Seance']['id'], 'pvdetaille'], [
            'class' => 'waiter',
            'data-waiter-send' => 'XHR',
            'data-waiter-callback' => 'getProgress',
            'data-waiter-type' => 'progress-bar',
            'data-waiter-title' => __('Génération du PV complet pour la séance du %s', $seance_date),
            'escapeTitle' => false,
            'title' => __('Générer le document de type PV complet pour la séance du %s', $seance_date),
        ]). '</li>';
        $finalisation["Visualiser"] = '
             <li class="dropdown-submenu visualiser">
               <a tabindex="-1" href="#visualiser"> '. $this->Bs->icon('cog', null, ['stylePrefix' => 'fa']) . ' ' .' Générer les procès-verbaux</a>
               <ul class="dropdown-menu">'.
            $finalisationVisualiser
            .'</ul>
             </li>';
            $finalisation[] = $this->Bs->link($this->Bs->icon('upload') . ' ' . __('Importer les procès-verbaux'), [
                'controller' => 'seances',
                'action' => 'saveProcesVerbaux',
                $seance['Seance']['id']
            ], [
                'title' => __('Envoi des actes à la signature pour la séance du %s', $seance_date),
                'escapeTitle' => false
            ]);

        $preparation["Visualiser"] = '
             <li class="dropdown-submenu visualiser">
               <a tabindex="-1" href="#visualiser"> '. $this->Bs->icon('cog', null, ['stylePrefix' => 'fa']) . ' ' .' Générer les convocations</a>
               <ul class="dropdown-menu">'.
                    $preparationVisualiser
               .'</ul>
             </li>';
        if (!empty($preparationEnvoyer)) {
            $preparation["Evoyer"] = '
             <li class="dropdown-submenu envoyer">
               <a tabindex="-1" href="#envoyer"> '. $this->Bs->icon('paper-plane') . ' ' .'Envoyer les convocations</a>
               <ul class="dropdown-menu">'.
                    $preparationEnvoyer
                .'</ul>
             </li>';
        }
        $preparationExport = '';
        if (
            $seance['Typeseance']['action'] === 0
            && in_array('downloadVotesElectroniqueJson', $actions, true)
        ) {
           $preparation[] = 'divider';

            $preparation["Export"] = '<li>'.$this->Bs->link($this->Bs->icon('file-export') . ' ' . __('Export vote électronique'), [
              'controller' => 'votes', 'action' => 'downloadVotesElectroniqueJson', $seance['Seance']['id']], [
              'title' => __('Export au format json pour le vote électronique de la séance séance du %s', $seance_date),
              'escapeTitle' => false,
          ]) . '</li>';
        }
        // if (!empty($preparationExport)) {
        //     $preparation["Export"] = '
        //      <li class="dropdown-submenu export">
        //        <a tabindex="-1" href="#export"> '. $this->Bs->icon('file-export') . ' ' .'Export CSV</a>
        //        <ul class="dropdown-menu">'.
        //             $preparationExport
        //         .'</ul>
        //      </li>';
        // }
        echo $this->Bs->cell(
            $this->Bs->div('btn-group') .
                $this->BForm->dropdownButton(__('Préparer') . ' ', $preparation, [
                    'type' => 'button',
                    'class' => 'btn dropdown-toggle',
                    'bootstrap-type' => 'primary',
                    'data-toggle' => 'dropdown', 'aria-expanded' => 'false']) .
                (!empty($enCours) ? $this->BForm->dropdownButton(__('Traiter') . ' ', $enCours, [
                    'data-toggle' => 'dropdown',
                    'bootstrap-type' => 'primary',
                    'aria-expanded' => 'false']) : '') .
                $this->BForm->dropdownButton(__('Finaliser') . ' ', $finalisation, ['bootstrap-type' => 'primary']) .
                $this->Bs->close()
        );

        if (in_array('ExportGedSeances/read', $actions, true)) {
            echo $this->Bs->cell(
                $this->Bs->div('btn-group') .
                $this->Bs->btn($this->Bs->icon('cloud-upload-alt') . ' ' . __('<span class="badge badge-counter"> %d</span>', $seance['Seance']['numero_depot']), ['controller' => 'ExportGedSeances', 'action' => 'sendToGed', $seance['Seance']['id']], [
                    'type' => 'primary',
                    'class' => 'waiter',
                    'data-waiter-send' => 'XHR',
                    'data-waiter-title' => __('Envoi de la séance à la GED'),
                    'data-waiter-type' => 'progress-bar',
                    'data-waiter-callback' => 'getProgress',
                    'escapeTitle' => false,
                    'title' => __('Envoyer la séance à la GED (nombre d\'envoi effectués: %s)', $seance['Seance']['numero_depot']),
                    'confirm' => __('Envoyer les documents à la GED ?')])
                . $this->Bs->close()
            );
        }

        // actions
        echo $this->Bs->cell(
            $this->Bs->div('btn-group') .
                $this->Bs->btn($this->Bs->icon('newspaper'), ['action' => 'genereFusionToClient', $seance['Seance']['id'], 'journal_seance'], [
                       'type' => 'default',
                       'class' => in_array('generer_journal', $seance['Actions'], true) ? 'waiter' : 'disabled',
                       'data-waiter-send' => 'XHR',
                       'data-waiter-callback' => 'getProgress',
                       'data-waiter-type' => 'progress-bar',
                       'data-waiter-title' => __('Génération du journal de la séance du  %s', $seance_date),
                       'title' => __('Génération du journal de la séance du  %s', $seance_date),
                       'escapeTitle' => false,
                           ]).
                (in_array('Typeseance/update', $seance['Actions'], true)  ?
                $this->Bs->btn($this->Bs->icon('lock'), ['action' => 'clore', $seance['Seance']['id']], [
                    'type' => 'primary',
                    'data-waiter-title' => __('Clore la séance du  %s', $seance_date),
                    'data-waiter-type' => 'default',
                    'class' => 'waiter',
                    'title' => __('Clore la séance du  %s', $seance_date),
                    'confirm' => __('Clore la séance du %s ?', $seance_date),
                    'escapeTitle' => false,
                ]) : '').
                ($this->permissions->check('Seances', 'update') && in_array('Typeseance/update', $seance['Actions'], true)  ?
                $this->Bs->btn($this->Bs->icon('pencil-alt'), ['controller' => 'seances', 'action' => 'edit', $seance['Seance']['id']], [
                    'type' => 'primary',
                    //'class' => 'bouton_modifier btn btn-primary',
                    'title' => __('Modifier la séance du  %s', $seance_date),
                    'escapeTitle' => false,
                ]) : '').
                ($this->permissions->check('Seances', 'delete') && in_array('Typeseance/delete', $seance['Actions'], true)  ?
                $this->Bs->btn($this->Bs->icon('trash'), ['controller' => 'seances', 'action' => 'delete', $seance['Seance']['id']], [
                    'type' => 'danger',
                    //'class' => 'bouton_supprimer btn btn-danger',
                    'title' => __('Supprimer la séance du  %s', $seance_date),
                    'confirm' => __('Confirmer la suppression de la séance du %s ?', $seance_date),
                    'escapeTitle' => false,
                ]) : '').
                $this->Bs->close()
        );
    }
} else {
    echo $this->Bs->cell(
        $this->Bs->tag('p', $this->Bs->icon('folder-open')
                    . ' ' . __('Aucune séance à afficher')),
        'text-center',
        ['colspan' => in_array('ExportGedSeances/read', $actions, true) ? 7 : 6]
    );
}

echo $this->Bs->endTable();

if ($this->request['render'] == 'bannette') {
    echo $this->Bs->close();
} else {
    if (!empty($models) && !empty($seances)) {
        $this->BsForm->setLeft(0);
        $this->BsForm->setRight(0);
        $this->BsForm->setFormType('inline');
        echo $this->Bs->row() .
        $this->Bs->col('xs6') .
        $this->BsForm->inputGroup('Seance.model_id', [
            'content' => $this->Bs->icon('cogs') . ' ' . __('Générer le document') . ' <span id = "nbSeancesChecked"></span>',
            'id' => 'generer_multi_seance',
            'type' => 'submit',
            'state' => 'primary',
            'side' => 'right'], [
                'data-placeholder'=> __('Sélectionner un modèle'),
                'class' => 'selectone',
                'data-allow-clear'=> true,
                'class' => 'selectone pull-left']) .
        $this->Bs->close() .
        $this->Bs->col('xs12') .
        $this->html->tag('em', __('Note : Pour générer un document multi-séances, cochez les séances souhaitées dans la liste, sélectionnez le modèle d\'édition, puis cliquez sur le bouton "Générer le document".'), ['class' => 'help-block']) .
        $this->Bs->close();
    }
}
echo $this->Bs->close();
echo $this->BsForm->end();
if ($this->request['render'] == 'bannette') {
    return;
}
