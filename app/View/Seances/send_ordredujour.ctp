<?php

$this->Html->addCrumb(__('Séances à traiter'), ['controller' => 'seances', 'action' => 'index']);
$this->Html->addCrumb(__('Envoi de l\'ordre du jour'));

echo $this->Bs->div('deliberations') .
 $this->Bs->tag('h3', __('Envoi de l\'ordre du jour')) .
 $this->Form->create('Seance', ['url' => ['controller' => 'seances', 'action' => 'sendOrdredujour', $seance_id, $model_id],
    'class' => 'waiter', 'data-waiter-title' => __('Envoi de l\'ordre du jour')]);

echo $this->Bs->div('boutons_generation_odj') .
 $this->Bs->div('btn-group') ;
 if ($send_mailsec===false) {
     echo $this->Bs->btn($this->Bs->icon('cog') . ' ' . __('Générer les ordres du jour'), [
    'controller' => 'seances',
    'action' => 'genereFusionToFiles',
    $seance_id,
    $model_id,
    'ordredujour'
        ], [
    'class' => 'btn btn-success waiter',
    'escapeTitle' => false,
    'title' => __('Générer les ordres du jour'),
    'data-waiter-send' => 'XHR',
    'data-waiter-type' => 'progress-bar',
    'data-waiter-callback' => 'getProgress',
    'data-waiter-title' => __('Génération des ordres du jour en cours'),
    'type' => 'primary']) ;
 }
 echo $this->Bs->btn($this->Bs->icon('download') . ' ' . __('Télécharger l\'archive'), ['controller' => 'seances',
    'action' => 'downloadZip',
    $seance_id,
    $model_id,
    'Ordredujour'
        ], [
    'class' => "btn btn-inverse",
    'escape' => false,
    'title' => __('Télécharger une archive contenant les ordres du jour'),
    'type' => 'primary']) .
    $this->Bs->close();
    if ($this->permissions->check('Seances', 'delete') && $send_mailsec===true) {
        echo $this->Bs->div('btn-group').
      $this->Bs->btn($this->Bs->icon('trash') . ' ' . __('Supprimer l\'envoi'), ['controller' => 'seances',
       'action' => 'deleteSendMail',
       $seance_id,
       'Ordredujour'
        ], [
       'class' => "btn btn-inverse",
       'escape' => false,
       'title' => __('Supprimer l\'envoi des ordres du jour'),
       'confirm' => __('Êtes-vous sûr de vouloir supprimer l\'envoi de l\'ordre du jour ?'),
       'type' => 'danger']);
    }
 echo $this->Bs->close(3);

echo $this->Bs->tag('br /');

//creation du tableau
$attribute = [];
$attribute['attributes']['name'] = 'tableListeActeur';
$this->Bs->lineAttributes(['class' => 'colonne_checkbox']);
$this->Bs->setTableNbColumn(4);
echo $this->Bs->table(
    [
    ['title' => $this->Form->checkbox(null, [
            'id' => 'masterCheckbox',
            'autocomplete' => 'off'])],
    ['title' => __('Élus')],
    ['title' => __('Document')],
    ['title' => __('Date d\'envoi')],
    ['title' => __('Statut')]
  ],
    ['hover', 'striped']
);
foreach ($acteurs as $acteur) {
    //cellule checkbox
    if (empty($acteur['Acteur']['email'])) {
        $cell_checkbox = $this->BsForm->checkbox('Acteur.id_' . $acteur['Acteur']['id'], [
            'label' => false,
            'inline' => true,
            'class' => 'masterCheckbox_checkbox',
            'disabled' => true,
            'title' => __('Envoi impossible, l\'adresse mail de l\'acteur n\'est pas renseigné')]);
    } elseif (empty($acteur['Acteur']['link'])) {
        $cell_checkbox = $this->BsForm->checkbox('Acteur.id_' . $acteur['Acteur']['id'], [
            'label' => false,
            'inline' => true,
            'disabled' => true,
            'title' => __("Impossible d'envoyer à cet acteur, l\'ordre du jour n'a pas encore été générée.")]);
    } elseif ($acteur['Acteur']['date_envoi'] == null) {
        $cell_checkbox = $this->BsForm->checkbox('Acteur.id_' . $acteur['Acteur']['id'], [
            'label' => false,
            'inline' => true,
            'class' => 'masterCheckbox_checkbox',
            'title' => __("Impossible d'envoyer à cet acteur, l\'ordre du jour n'a pas encore été générée.")]);
    } else {
        $cell_checkbox = '<i class="fa fa-check" title="' . __('Ordre du jour déjà envoyée') . '"></i>';
    }

    //cellule élu
    $cell_elu = $this->Html->link($acteur['Acteur']['prenom'] . ' ' . $acteur['Acteur']['nom'], ['controller' => 'acteurs', 'action' => 'view', $acteur['Acteur']['id']]);

    //cellule link
    if (isset($acteur['Acteur']['link'])) {
        $cell_link = $this->Bs->btn($this->Bs->icon('download'), $acteur['Acteur']['link'], [
            'escapeTitle' => false,
            'type' => 'default',
        ]);
    } else {
        $cell_link = __('Pas de document');
    }

    //cellule date envoi
    if ($acteur['Acteur']['date_envoi'] == null) {
        $cell_envoi = __('Non envoyé');
    } else {
        $cell_envoi = __('Envoyé le : ') . $this->Time->i18nFormat($acteur['Acteur']['date_envoi'], '%A %d %B %Y') . __(' à ') . $this->Time->i18nFormat($acteur['Acteur']['date_envoi'], '%k:%M');
    }
    //cellule reception
    $cell_reception = __('Pas d\'accusé de réception');
    if ($use_mail_securise===true){
        if ($acteur['Acteur']['date_reception'] == null) {
            if ($send_mailsec) {
                $cell_reception = __('Envoyé, mais non lu');
            } else {
                $cell_reception = __('Non envoyé');
            }
        } else {
            $cell_reception = __('Reçu le : ') . $this->Time->i18nFormat($acteur['Acteur']['date_reception'], '%A %d %B %Y') . __(' à ') . $this->Time->i18nFormat($acteur['Acteur']['date_reception'], '%k:%M');
        }
    }
    echo $this->Bs->cell($cell_checkbox) . $this->Bs->cell($cell_elu) . $this->Bs->cell($cell_link) . $this->Bs->cell($cell_envoi) . $this->Bs->cell($cell_reception);
}
echo $this->Bs->endTable();

//spacer
echo $this->Bs->div('spacer') . $this->Bs->close();
$this->BsForm->setLeft(0);
$this->BsForm->setRight(0);
echo $this->Bs->div('btn-group col-md-offset-' . $this->BsForm->getLeft(), null) .
 $this->Bs->btn($this->Bs->icon('times-circle') . ' ' . __('Annuler'), $previous, ['type' => 'default', 'escape' => false, 'title' => __('Annuler les modifications')]) .
 $this->Bs->btn($this->Bs->icon('paper-plane') . ' ' . __('Envoyer les ordres du jour'), null, ['tag' => 'button', 'type' => 'primary', 'id' => 'boutonValider', 'escape' => false, 'title' => __('Envoyer les ordres du jour')]) .
 $this->Bs->close();
$this->Form->end() . $this->Bs->close();
?>

<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {
        //Lors d'action sur une checkbox :
        $('input[type=checkbox]').change(selectionChange);
        selectionChange();

    function selectionChange() {
        var nbChecked = $('input[type=checkbox].masterCheckbox_checkbox:checked').length;
        //Apposer ou non la class disabled au bouton selon si des checkbox sont cochées (style)
        if (nbChecked > 0) {
            $('#boutonValider').removeClass('disabled');
            $("#boutonValider").prop("disabled", false);
        } else {
            $('#boutonValider').addClass('disabled');
            $("#boutonValider").prop("disabled", true);
        }
        $('#nbActeursChecked').text('(' + nbChecked + ')');
    }
    });
});
</script>
