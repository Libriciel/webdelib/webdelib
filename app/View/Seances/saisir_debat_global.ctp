<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

if (empty($this->data['Seance']['traitee'])) {
    $this->Html->addCrumb(__('Séances à traiter'), ['controller' => 'seances', 'action' => 'index']);
} else {
    $this->Html->addCrumb('Post-séances', ['controller' => 'postseances', 'action' => 'index']);
}
if ($this->data['Seance']['pv_figes']) {
  $this->Html->addCrumb(__('Consulter les débats généraux'));
  echo $this->Html->tag('h3', __('Consulter les débats généraux de la séance : %s du %s à %s',
      $this->data["Typeseance"]['libelle'],
      $this->Time->i18nFormat($this->data['Seance']['date'], '%d/%m/%Y'),
      $this->Time->i18nFormat($this->data['Seance']['date'], '%kh%M')));
}
else {
  $this->Html->addCrumb(__('Saisir les débats généraux'));
  echo $this->Html->tag('h3', __('Saisir les débats généraux de la séance : %s du %s à %s',
        $this->data["Typeseance"]['libelle'] ,
       $this->Time->i18nFormat($this->data['Seance']['date'], '%d/%m/%Y'),
       $this->Time->i18nFormat($this->data['Seance']['date'], '%kh%M')));
}

if($this->data['Seance']['pv_figes']) {
    echo $this->Bs->div('media') .
        $this->Bs->link($this->Bs->icon('file-alt', ['4x']), Router::url(['controller' => 'seances',
            'action' => 'download',
            $seance_id, 'debat_global']), ['class' => 'media-left', 'escape' => false]) .
        $this->Bs->div('media-body') .
        $this->Bs->tag('h4', $this->data['Seance']['debat_global_name'], ['class' => 'media-heading']) .
        $this->Bs->div('btn-group') ;
    echo $this->Bs->btn($this->Bs->icon('download') . ' ' . __('Télécharger'), ['controller' => 'seances',
            'action' => 'download',
            $seance_id, 'debat_global'], [
            'type' => 'default',
            'size' => 'xs',
            'class' => 'media-left',
            'escape' => false,
        ]);
    echo $this->Bs->close(3).$this->Bs->tag('br/', null);
    echo $this->Html2->btnCancel($previous);
    return;
}

echo $this->BsForm->create('Seances', ['url' => [
        'controller' => 'seances',
        'action' => 'saisirDebatGlobal', $this->data['Seance']['id']], 'type' => 'file']);

if ($this->data['Seance']['debat_global_size'] > 0 ) {

    echo $this->Bs->div('media') .
        $this->Bs->link($this->Bs->icon('file-alt', ['4x']), $file_debat_url, ['class' => 'media-left', 'escape' => false]) .
        $this->Bs->div('media-body') .
        $this->Bs->tag('h4', $this->data['Seance']['debat_global_name'], ['class' => 'media-heading']) .
        $this->Bs->div('btn-group') ;

    // Si les documents ne sont figés alors on autorise la modification et la suppression

   echo $this->Bs->btn($this->Bs->icon('trash') . ' ' . __('Supprimer'), ['controller' => 'seances',
       'action' => 'deleteDebatGlobal',
       $seance_id], [
       'type' => 'danger',
       'size' => 'xs',
       'class' => 'media-left',
       'escape' => false,
       'confirm' => __('Voulez-vous vraiment supprimer %s du projet ?', $this->data['Seance']['debat_global_name'])
   ]) ;

   echo $this->Bs->btn($this->Bs->icon('pencil-alt') . ' ' . __('Modifier'), $file_debat_url, [
           'type' => 'primary',
           'size' => 'xs',
           'class' => 'media-left',
           'escape' => false,
       ]);

    echo $this->Bs->btn($this->Bs->icon('download') . ' ' . __('Télécharger'), ['controller' => 'seances',
            'action' => 'download',
            $seance_id, 'debat_global'], [
            'type' => 'default',
            'size' => 'xs',
            'class' => 'media-left',
            'escape' => false,
        ]) .
        $this->Bs->close(3) . $this->BsForm->hidden('Seance.file_debat',['value'=>$file_debat]) . $this->Bs->tag('br', null);
} else {
    echo $this->Bs->link($this->Bs->icon('file-alt', ['4x']), 'javascript:void(0);', ['class' => 'media-left', 'escape' => false]) .
        $this->Bs->div('media-body') .
        $this->Bs->tag('h4', __('Créer un nouveau document'), ['class' => 'media-heading']) .
        $this->Bs->div('btn-group') .
        $this->Bs->btn($this->Bs->icon('plus') . ' ' . __('Nouveau document'), [
            'controller' => 'seances',
            'action' => 'saisirDebatGlobal',
            $seance_id, true], [
            'type' => 'default',
            'size' => 'xs',
            'class' => 'media-left',
            'escape' => false,
        ]) .
    $this->Bs->close(3) .

    $this->Bs->tag('br/', null).

    $this->BsForm->setLeft(0).
    $this->Bs->row() .
    $this->Bs->col('xs8') .
    $this->BsForm->input('Seance.texte_doc', [
        'label' => false,
        'type' => 'file',
        'accept'=> 'application/vnd.oasis.opendocument.text',
        'data-btnClass' => 'btn-primary',
        'data-text' => __('Choisir un fichier'),
        'data-badge' => false,
        'error' => false,
        'help' => __('Les modifications apportées ici ne prendront effet que lors de la sauvegarde.'),
        'data-placeholder' => __('Pas de fichier'),
        'class' => 'filestyle']) . $this->Bs->close() .
    $this->Bs->col('xs4') .
    $this->Bs->div('btn-group btn-group-right') .
    $this->Bs->btn($this->Bs->icon('eraser') . ' ' . __('Effacer'), 'javascript:void(0);', [
        'type' => 'danger',
        'class' => 'btn-danger-right wd-filestyle-clear',
        'escapeTitle' => false,
        'data-wd-filestyle-id' => 'SeanceTexteDoc'
    ]) .
    $this->Bs->close(3). ($this->Form->isFieldError('Seance.texte_doc') ?
        $this->Form->error('Seance.texte_doc') : '');
}

echo $this->BsForm->hidden('Seance.id');
echo $this->Html2->btnSaveCancel('', $previous);
echo $this->BsForm->end();
