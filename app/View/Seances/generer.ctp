<h2> <?php echo 'Liste des fichiers générés';?> </h2>
<script>
    $("#pourcentage").hide();
    $("#progrbar").hide();
    $("#affiche").hide();
    $("#contTemp").hide();
</script>
<?php
foreach ($listFiles as $path => $name)
    echo $this->Html->link($name, $path) . "<br />";

echo ("<br /><br /><a href='".Router::url(['controller' => 'seances', 'action' => 'index'])."'> ".__('Retour à la liste des séances')." </a>");
