<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Séances'), ['action' => 'index']);

if ($this->action == 'edit') {
    echo $this->Bs->tag('h3', __('Modifier la séance'));
    $this->Html->addCrumb(__('Modification d\'une séance'));
    echo $this->BsForm->create('Seance', ['url' => ['controller' => 'seances', 'action' => 'edit', $this->Html->value('Seance.id')], 'type' => 'file', 'name' => 'SeanceForm']);
} else {
    echo $this->Bs->tag('h3', __('Créer une séance'));
    $this->Html->addCrumb(__('Nouvelle séance'));
    echo $this->BsForm->create('Seance', ['url' => ['controller' => 'seances', 'action' => 'add'], 'type' => 'file', 'name' => 'SeanceForm']);
}

// $this->Html->tag('div', null, array('class' => 'panel panel-default')) .
//$this->Html->tag('div', 'Date', array('class' => 'panel-heading')) .
//$this->Html->tag('div', null, array('class' => 'panel-body')) .

echo $this->BsForm->select('Seance.type_id', $typeseances, [
    'label' => __('Type de séance'),
    'default' => ($this->Html->value('Seance.type_id') != null) ? $this->Html->value('Seance.type_id') : null,
    'class' => 'selectone',
    'required' => true,
    'autocomplete' => 'off',
    'data-placeholder' => __('Choisir le type de séance'),
    'empty' => $this->action == 'edit' ? false : true
    ]) ;
if (isset($this->validationErrors['Seance']['type_id'])) {
    echo $this->Html->tag('div', $this->validationErrors['Seance']['type_id'][0], ['class' => 'error-message']);
}
echo $this->BsForm->dateTimepicker(
    'Seance.date', //TODO
        ['language' => 'fr',
    'autoclose' => 'true',
    'format' => 'dd/mm/yyyy hh:ii',
    'pickerPosition' => 'bottom-right'],
    [
    'label' => 'Date',
    'title' => __('Choisissez la date et l\'heure de séance'),
    'style' => 'cursor:pointer;background-color: white;',
    'help' => __('Cliquez sur le champ ci-dessus pour choisir la date et l\'heure de la séance'),
    'required' => true,
    'value' => isset($date) ? $date : '']
);
echo $this->BsForm->input('SeanceChildren', [
   'options' => $seances,
   'label' => __('Seances associées'),
   'value' => !empty($selectedSeances) ? $selectedSeances : null,
   'multiple' => 'multiple',
   'class' => 'selectmultiple',
   'data-placeholder' => __('Sélectionner une ou plusieurs séances'),
   'empty' => false]) ;

echo $this->BsForm->input('Seance.commentaire', [
    'type' => 'textarea',
    'rows' => 3,
    'label' => __('Commentaire'),
    'maxlength' => '1000']);

echo $this->element('infosup_edit', ['infosupdefs' => $infosupdefs]);

echo $this->BsForm->hidden('Seance.id');
//$this->BsForm->setLeft(0);
echo $this->Html2->btnSaveCancel('', $previous, 'Ajouter la séance') .
 $this->BsForm->end();
