<?php
$this->Html->addCrumb(__('...'));
$this->Html->addCrumb(__('Gestion des listes d\'information supplémentaire'), $previous);

if ($this->Html->value('Infosuplistedef.id')) {
    echo $this->Bs->tag('h3', __('Modifier l\'élément de la liste : ') . $infosupdef['Infosupdef']['nom']);
    $this->Html->addCrumb($infosupdef['Infosupdef']['nom']);
    echo $this->BsForm->create('Infosuplistedef', ['url' => ['controller' => 'infosuplistedefs', 'action' => 'edit'], 'type' => 'post']);
} else {
    echo $this->Bs->tag('h3', __('Ajouter un élément à la liste : ') . $infosupdef['Infosupdef']['nom']);
    $this->Html->addCrumb($infosupdef['Infosupdef']['nom']);
    echo $this->BsForm->create('Infosuplistedef', ['url' => ['controller' => 'infosuplistedefs', 'action' => 'add', $infosupdef['Infosupdef']['id']], 'type' => 'post']);
}
?>

<div class="required">
    <?php echo $this->BsForm->input('Infosuplistedef.nom', ['label' => __('Nom') . ' <acronym title="obligatoire">*</acronym>', 'size' => '40', 'title' => __('Nom de l\'element')]); ?>
</div>
<?php
echo $this->BsForm->checkbox('Infosuplistedef.actif', ['label' => __('élément actif')]);
echo $this->Html->tag('div', '', ['class' => 'spacer']);
?>

<div class="submit">
    <?php
    echo $this->BsForm->hidden('Infosuplistedef.id');
    echo $this->BsForm->hidden('Infosuplistedef.infosupdef_id');
    echo $this->Html2->btnSaveCancel(null, ['controller' => 'infosuplistedefs', 'action' => 'index', $infosupdef['Infosupdef']['id']], ['controller' => 'infosuplistedefs', 'action' => 'index', $infosupdef['Infosupdef']['id']]
    );
    ?>
</div>

<?php
echo $this->BsForm->end();
