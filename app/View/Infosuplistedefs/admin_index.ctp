<?php
$this->Html->addCrumb(__('Informations supplémentaires'), $previous);
$this->Html->addCrumb($infosupdef['Infosupdef']['nom']);

echo $this->Bs->tag('h3', __('Liste : ') . $infosupdef['Infosupdef']['nom']) .
 $this->Html2->btnAdd(__('Ajouter un élément'), __('Ajouter'), ['action' => 'add', $infosupdef['Infosupdef']['id']]) .
 $this->element('filtre') .
 $this->Bs->table([['title' => __('Ordre')], ['title' => __('Libellé')], ['title' => __('Actif')], ['title' => __('Actions')]], ['hover', 'striped']);
foreach ($this->data as $infosuplistedef) {
    echo $this->Bs->tableCells([$infosuplistedef['Infosuplistedef']['ordre'] .
    $this->Bs->btn($this->Bs->icon('chevron-up'), ['controller' => 'infosuplistedefs', 'action' => 'changerOrdre', $infosuplistedef['Infosuplistedef']['id'], 0], ['escapeTitle' => false], false) .
    $this->Bs->btn($this->Bs->icon('chevron-down'), ['controller' => 'infosuplistedefs', 'action' => 'changerOrdre', $infosuplistedef['Infosuplistedef']['id']], ['escapeTitle' => false], false), $infosuplistedef['Infosuplistedef']['nom'], $Infosuplistedef->libelleActif($infosuplistedef['Infosuplistedef']['actif']), $this->Bs->div('btn-group') .
    $this->Bs->btn($this->Bs->icon('pencil-alt'), ['controller' => 'infosuplistedefs', 'action' => 'edit', $infosuplistedef['Infosuplistedef']['id']], ['type' => 'primary', 'escapeTitle' => false, 'title' => __('Modifier')]) .
    $this->Bs->btn($this->Bs->icon('trash'), ['controller' => 'infosuplistedefs', 'action' => 'delete', $infosuplistedef['Infosuplistedef']['id']], ['type' => 'danger', 'escapeTitle' => false, 'title' => __('Supprimer'), 'class' => !$Infosuplistedef->isDeletable($infosuplistedef['Infosuplistedef']['id']) ? 'disabled' : ''], __('Êtes-vous sûr de vouloir supprimer %s ?', $infosuplistedef['Infosuplistedef']['nom'])) .
    $this->Bs->close()]);
}
echo $this->Bs->endTable() .
 $this->Bs->div('btn-group col-md-offset-0', null) .
 $this->Html2->btnCancel($previous) .
 $this->Bs->close();
