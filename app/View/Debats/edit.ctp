<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

if (empty($seance['Seance']['traitee'])) {
    if ($seance['Typeseance']['action'] === 0) {
        $this->Html->addCrumb(__('Séances à traiter'), [
            'controller' => 'seances', 'action' => 'index'
        ]);
        $this->Html->addCrumb(
            $seance['Typeseance']['libelle'] . ' '
            . __('du') . ' '
            . $this->Time->i18nFormat($seance['Seance']['date'], '%d/%m/%Y'). __(' à ')
            . $this->Time->i18nFormat($seance['Seance']['date'], '%kh%M'), [
                'controller' => 'votes',
                'action' => 'index',
                $seance_id
            ]);
    }
    if ($seance['Typeseance']['action'] === 1) {
        $this->Html->addCrumb(__('Séances à traiter'), [
            'controller' => 'seances', 'action' => 'index'
        ]);
        $this->Html->addCrumb(
            $seance['Typeseance']['libelle'] . ' ' . __('du')
            . ' ' . $this->Time->i18nFormat($seance['Seance']['date'], '%d/%m/%Y')
            . __(' à ')
            . $this->Time->i18nFormat($seance['Seance']['date'], '%kh%M'), [
                'controller' => 'avis',
                'action' => 'index',
                $seance_id
            ]
        );
    }
    if ($seance['Typeseance']['action'] === 2) {
        $this->Html->addCrumb(__('Séances à traiter'), [
            'controller' => 'seances', 'action' => 'index'
        ]);
        $this->Html->addCrumb('Post-séances', [
                'controller' => 'votes',
                'action' => 'index',
                $seance_id
            ]
        );
    }
} else {
    $this->Html->addCrumb('Post-séances', ['controller' => 'postseances', 'action' => 'index']);
    $this->Html->addCrumb($seance['Typeseance']['libelle'] . ' ' . __('du') . ' ' . $this->Time->i18nFormat($seance['Seance']['date'], '%d/%m/%Y'). __(' à ') . $this->Time->i18nFormat($seance['Seance']['date'], '%kh%M'), ['controller' => 'postseances', 'action' => 'afficherProjets', $seance_id, 'debats']);
}

$this->Html->addCrumb(__('Saisir les débats'));
echo $this->Html->tag(
    'h3',
    __(
        'Saisir les débats du projet : %s (%s du %s à %s)',
        $this->data['Deliberation']['objet'],
        $seance["Typeseance"]['libelle'],
        $this->Time->i18nFormat($seance['Seance']['date'], '%d/%m/%Y'),
        $this->Time->i18nFormat($seance['Seance']['date'], '%kh%M')
    )
);

echo $this->BsForm->create('Votes', ['url' => ['controller' => 'debats', 'action' => 'edit', $delib_id, $seance_id], 'type' => 'file']);

if ($this->data['Deliberation']['debat_size'] > 0) {
    echo $this->Bs->div('media') .
        $this->Bs->link($this->Bs->icon('file-alt', ['4x']), $file_debat_url, ['class' => 'media-left', 'escape' => false]) .
    $this->Bs->div('media-body') .
    $this->Bs->tag('h4', $this->data['Deliberation']['debat_name'], ['class' => 'media-heading']) .
    $this->Bs->div('btn-group') .
    $this->Bs->btn($this->Bs->icon('pencil-alt') . ' ' . __('Modifier'), $file_debat_url, [
        'type' => 'primary',
        'size' => 'xs',
        'class' => 'media-left',
        'escape' => false,
    ]) .
    $this->Bs->btn($this->Bs->icon('trash') . ' ' . __('Supprimer'), [
        'controller' => 'debats',
        'action' => 'delete', $delib_id, $seance_id], [
        'type' => 'danger',
        'size' => 'xs',
        'class' => 'media-left',
        'escape' => false,
        'confirm' => __('Voulez-vous vraiment supprimer %s du projet ?', $this->data['Deliberation']['debat_name'])
    ]) .
   $this->Bs->btn($this->Bs->icon('download') . ' ' . __('Télécharger'), [
       'controller' => 'debats',
       'action' => 'download',
       $this->data['Deliberation']['id']], [
       'type' => 'default',
       'size' => 'xs',
       'class' => 'media-left',
       'escape' => false,
   ]) .
    $this->Bs->close(3) . $this->BsForm->hidden('Deliberation.file_debat', ['value' => $file_debat]) . $this->Bs->tag('br/', null);
} else {
    echo $this->Bs->div('media') .
        $this->Bs->link($this->Bs->icon('file-alt', ['4x']), '#', ['class' => 'media-left', 'escape' => false]) .
    $this->Bs->div('media-body') .
    $this->Bs->tag('h4', __('Créer un nouveau document'), ['class' => 'media-heading']) .
    $this->Bs->div('btn-group') .
    $this->Bs->btn($this->Bs->icon('plus') . ' ' . __('Nouveau document'), [
        'controller' => 'debats',
        'action' => 'edit',
        $delib_id, $seance_id, true], [
        'type' => 'default',
        'size' => 'xs',
        'class' => 'media-left',
        'escape' => false,
    ]) .
    $this->Bs->close(3) . $this->Bs->tag('br/', null);


    $this->BsForm->setLeft(0);
    echo $this->Bs->row() .
        $this->Bs->col('xs8') .
    $this->BsForm->input('Deliberation.texte_doc', [
        'label' => false,
        'type' => 'file',
        'accept'=>"application/vnd.oasis.opendocument.text",
        'data-btnClass' => 'btn-primary',
        'data-text' => __('Choisir un fichier'),
        'data-placeholder' => __('Pas de fichier'),
        'data-badge' => false,
        'error' => false,
        'help' => __('Les modifications apportées ici ne prendront effet que lors de la sauvegarde.'),
        'class' => 'filestyle']) . $this->Bs->close() .
    $this->Bs->col('xs4') .
    $this->Bs->div('btn-group btn-group-right') .
    $this->Bs->btn($this->Bs->icon('eraser') . ' ' . __('Effacer'), '#', [
        'type' => 'danger',
        'escapeTitle' => false,
        'class' => 'btn-danger-right wd-filestyle-clear',
        'data-wd-filestyle-id' => 'SeanceTexteDoc'
    ]) .
    $this->Bs->close(3) . ($this->Form->isFieldError('Deliberation.texte_doc') ?
        $this->Form->error('Deliberation.texte_doc') : '');
}
echo $this->BsForm->hidden('Deliberation.id');
echo $this->BsForm->hidden('Seance.id');
echo $this->Html2->btnSaveCancel('', $previous);
echo $this->BsForm->end();
