<?php
$nbProjets = isset($this->Paginator) ? $this->Paginator->counter('{:count}') : $nbProjets;
$titre = __('Autres actes envoyés au contrôle de légalité') . ' (' . $nbProjets . ' ' . ($nbProjets > 1 ? 'projets' : 'projet') . ')';

$this->Html->addCrumb(__('Tous les projets'));
$this->Html->addCrumb(__('Autres actes'));
$this->Html->addCrumb(__('Télétransmis'));

echo $this->Bs->tag('h3', $titre);

echo $this->element('filtre');

echo $this->Bs->div('alert alert-info', __('La Classification enregistrée date du') . ' ' . $this->Time->i18nFormat($dateClassification, '%A %d %B %Y')) .
    $this->Bs->div();

// formulaire
echo $this->BsForm->create('Deliberation', [
    'url' => ['controller' => 'ExportGed', 'action' => 'sendToGedAutreActe'],
    'id' => 'appAbandon',
]);

// en-têtes du tableau
$titles = [
    ['title' => $this->BsForm->checkbox(null, [
            'id' => 'masterCheckbox',
            'inline' => 'inline',
            'autocomplete' => 'off',
    ])],
    ['title' => $this->Paginator->sort('id', 'Id')],
    ['title' => $this->Paginator->sort(
        'num_delib',
        __('N° de l\'acte')
    )],
    ['title' => $this->Paginator->sort('objet_delib', __('Libellé de l\'acte'))],
    ['title' => $this->Paginator->sort(
        'Deliberation.date_acte',
        __('Date de décision')
    )],
    ['title' => __('Annexe(s)')],
    ['title' => $this->Paginator->sort('num_pref', __('Classification'))],
    ['title' => __('Type de pièce principale')],
    ['title' => __('Envoi complémentaire')],
    ['title' => $this->Paginator->sort(__('Statut'))],
    ['title' => __('Courriers Ministériels')],
    ['title' => $this->Paginator->sort(__('N° Dépot'))],
    ['title' => __('Actions')]
];

// tableau des résultats
echo $this->Bs->table($titles, ['hover', 'striped']);
$modals = '';

foreach ($deliberations as $deliberation) {
    // checkbox
    echo $this->Bs->cell(
        $this->BsForm->checkbox(
                'Deliberation.' . $deliberation['Deliberation']['id'] . '.send',
                [
                'label' => false,
                'autocomplete' => 'off',
                'inline' => 'inline',
                'checked' => false,
                'class' => 'masterCheckbox_checkbox',
            ]
            )
    );

    if (!empty($deliberation['Deliberation']['tdt_ar_annexes_status']) && $deliberation['Deliberation']['tdt_ar_annexes_status'] == 'danger') {
        echo $this->Bs->lineColor($deliberation['Deliberation']['tdt_ar_annexes_status']);
    }

    // id
    echo $this->Bs->cell($this->Html->link($deliberation['Deliberation']['id'], ['controller' => 'projets', 'action' => 'view', $deliberation['Deliberation']['id']]));

    // numéro de délibération
    echo $this->Bs->cell($this->Html->link($deliberation['Deliberation']['num_delib'], ['controller'=> 'actes', 'action' => 'getTampon', $deliberation['Deliberation']['id']]));

    // libellé
    echo $this->Bs->cell($deliberation['Deliberation']['objet_delib']);

    // date
    $date = '';
    if (!empty($deliberation['Deliberation']['date_acte'])) {
        $date = $this->Time->i18nFormat($deliberation['Deliberation']['date_acte'], '%d/%m/%Y');
    }
    echo $this->Bs->cell($date);

    // annexes
    $annexes = $this->DelibTdt->retourneAnnexes($deliberation);
    echo $this->Bs->cell($annexes);

    // classification
    echo $this->Bs->cell($deliberation['Deliberation']['num_pref_libelle']);
    echo $this->Bs->cell($deliberation['Deliberation']['typologiepiece_code_libelle']);
    echo $this->Bs->cell(!empty($deliberation['Deliberation']['tdt_document_papier']) ? __('Oui') : __('Non'));

    // statut TDT
    $etat=$this->DelibTdt->retourneEtat($deliberation);
    echo $this->Bs->cell($etat);

    // courriers ministériels
    $messages = $this->DelibTdt->retourneMessages($deliberation, $tdt, $tdt_host);
    echo $this->Bs->cell($messages);

    echo $this->Bs->cell($deliberation['Deliberation']['numero_depot']);

    $modal = "";
    $btnCancelTDT = "";

    if (isset($acl)) {
        $aclKeys = array_keys($acl);
        $cancelSendTTD = implode("", preg_grep('/(CancelSendTDT\z)/', $aclKeys));
    }

    // Si la délibération
    if (!empty($deliberation['Deliberation']['num_delib'])
        && (isset($acl[$cancelSendTTD]) && $acl[$cancelSendTTD])) {
        $modals .=
            $this->Bs->modal(
                in_array($deliberation['Deliberation']['tdt_status'], [-1,-3], true) ? "Changer l’état de votre acte afin de pouvoir le renvoyer vers le TDT" : 'Confirmer l\'annulation de votre acte dans le TDT',
                $this->element('Deliberations/formulaire_annulation_tdt', ['deliberation'=>$deliberation,'acl'=>$acl]),
                ['form' => true,
                    'id' => 'cancel-tdt-'.$deliberation['Deliberation']['id'],],
                [
                    'close' => $this->Bs->icon('times-circle') . ' Annuler',
                    'open' => [
                        'name' => $this->Bs->icon('share-square') . '</span>',
                        'class' => 'btn-danger class-wd-tdt-cancelForm',
                        'type'=>'danger',

                        'options'=>[
                            'tag'=>'button',
                            'option_type'=>'button',
                            'disabled' => array_key_exists('autresActesCancelSendTDT', $acl) && $acl['autresActesCancelSendTDT'] ? '' : 'disabled',
                            'title' => in_array($deliberation['Deliberation']['tdt_status'], [-1,-3], true) ? "Changer l’état de l’acte pour corriger l’erreur."  : "Confirmer l'annulation de télétransmission de l'acte : ",
                        ]
                    ],
                    'id' => 'cancelBtn-tdt-'.$deliberation['Deliberation']['id'],
                    'confirm' => ['name' => $this->Bs->icon('share-square') . ' ' .  (in_array($deliberation['Deliberation']['tdt_status'], [-1,-3], true) ?'Changer l\'état de l\'acte' : 'Confirmer l\'annulation'), 'class' => 'btn-danger', /*'link' => ['controller' => 'deliberations', 'action' => 'cancelSendToTDT']*/]
                ]
            );

        $btnCancelTDT = $this->Bs->btn('<span class="fa-1x">' . $this->Bs->icon('share-square') . '</span>', 'javascript:void(0);', [
            'type' => 'danger',
            'class' => 'class-wd-tdt-cancel',
            'data-wd-tdt-cancel-id'=> $deliberation['Deliberation']['id'],
            'data-toogle'=> 'modal',
            'tag'=>'button',
            'option_type'=>'button',
            'escapeTitle'=> false,
            'disabled' => $deliberation['Deliberation']['cancelSendTDT'] ? '' : 'disabled',
            'title' => in_array($deliberation['Deliberation']['tdt_status'], [-1,-3], true) ? "Changer l'état de l'acte pour corriger l'erreur" : "Confirmer l'annulation de télétransmission de l'acte",
        ]);
    }

    echo $this->Bs->cell($btnCancelTDT);
}

// fin de tableau
echo $this->Bs->endTable();
//paginate
echo $this->element('paginator', ['paginator' => $this->Paginator]);

if (!empty($deliberations)) {
    echo $this->element('Projet/traitement_lot', [
        'modeles' => isset($modeles) ? $modeles : null,
        'traitement_lot' => isset($traitement_lot) ? $traitement_lot : null,
        'actions_possibles'=>[
            'sendToGed' => __('Envoyer à la GED'),
            !$acl['autresActesAbandon'] ? null :'autresActesAbandon' => __('Abandonner')  ,
        ]]);
}
echo $this->BsForm->end();

echo $modals;

?>
<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {
        $('.class-wd-tdt-cancelForm').hide();
        $('#masterCheckbox').attr('checked', false);
        $('#sendToGed').addClass('disabled');
        $("#sendToGed").prop("disabled", true);
        $('input[type="checkbox"]').change(changeSelection);
        changeSelection();

        /**
         * Afficher/Masquer la sélection de circuit selon si la selection est vide ou non
         */
        function changeSelection() {
            if ($('input[type="checkbox"]:checked').length > 0) {
                $('#sendToGed').removeClass('disabled');
                $("#sendToGed").prop("disabled", false);
            } else {
                $('#sendToGed').addClass('disabled');
                $("#sendToGed").prop("disabled", true);
            }
        }

        $('.class-wd-tdt-cancel').on('click',function(){
            var target = $(this).attr("data-wd-tdt-cancel-id");
            $('*[data-target="#cancel-tdt-'+ target + '"]').click();
            }
        );
    });
});
</script>
