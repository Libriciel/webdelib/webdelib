<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$nbProjets = isset($this->Paginator) ? $this->Paginator->counter('{:count}') : $nbProjets;
$titre = $titreVue . ' (' . $nbProjets . ' ' . ($nbProjets > 1 ? 'projets' : 'projet') . ')';

if (!empty($crumbs)) {
    foreach ($crumbs as $crumb) {
        $this->Html->addCrumb($crumb);
    }
}

if (isset($message)) {
    echo $message;
}
echo $this->element('filtre');

$projet_etat = [
    0 => $this->Bs->icon('pause') . ' ' . __('En cours de rédaction'),
    1 => $this->Bs->icon('clock') . ' ' . __('Dans un circuit'),
    2 => $this->Bs->icon('check') . ' ' . __('Validé'),
    3 => $this->Bs->icon('thumbs-up') . ' ' . __('Adopté'),
    4 => $this->Bs->icon('thumbs-down') . ' ' . __('Refusé'),
    5 => $this->Bs->icon('university') . ' ' . __('Envoyé au TdT'),
];

echo $this->Bs->tag('h3', $titre);

$titres = [];
if ($this->action == "autresActesValides" && !empty($actes)) {
    echo $this->BsForm->create('Deliberation', [
        'url' => ['controller' => 'signatures', 'action' => 'sendAutresActesToSignature'],
        'class' => 'appAbandon waiter form-inline',
        'data-waiter-title' => __('Signature en cours'),

    ]);
    $titres[] = ['title' => $this->BsForm->checkbox(null, [
                'autocomplete' => 'off',
                'id' => 'masterCheckbox'])];
}

$titres[] = ['title' => 'Id.'];
if ($this->action == 'autresActesValides') {
    $titres[] = ['title' => __('N° Acte')];
}
$titres[] = ['title' => __('Type d\'acte')];
$titres[] = ['title' => __('Libellé de l\'acte')];
$titres[] = ['title' => __('Titre')];
$titres[] = ['title' => __('Circuit')];
if ($this->action == 'autresActesValides') {
    $titres[] = ['title' => __('Date signature')];
    $titres[] = ['title' => __('Statut signature')];
} else {
    $titres[] = ['title' => __('État')];
}
$titres[] = ['title' => __('Actions')];

echo $this->Bs->table($titres, ['hover', 'striped']);

foreach ($actes as $acte) {
    if ($this->action == 'autresActesValides') {
        $signature_etat = '';
        switch ($acte['Deliberation']['parapheur_etat']) {
            case -1:
                $signature_etat = $this->Bs->icon('exclamation-triangle') . ' ' . $acte['Deliberation']['parapheur_commentaire'] . '"></i>&nbsp;' . __('Refusé par le parapheur');
                break;
            case 1:
                $signature_etat = $this->Bs->icon('clock') . ' ' . __('En cours de signature');
                break;
            case 2:
                $signature = '';
                if (!empty($delib['Deliberation']['signee'])) {
                    if (!empty($delib['Deliberation']['signature_type']) && $delib['Deliberation']['signature_type']==='PAdES') {
                        $signature = __('(Signée PAdES)');
                    } elseif (!empty($delib['Deliberation']['signature'])) {
                        $signature = '('.$this->Html->link($this->Bs->icon('file-archive') . ' ' . __('Signée PKCS#7'), ['controller' => 'actes', 'action' => 'downloadSignature', $delib['Deliberation']['id']], [
                                            'title' => __('Télécharger les signatures'),
                                            'style' => 'text-decoration: none',
                                            'escapeTitle' => false]).')';
                    } else {
                        $signature = __('(Visa)');
                    }
                }
                $signature_etat = $this->Bs->icon('award') . ' ' . __('Signé électroniquement') . ' '. $signature;
                break;
            default: //0 ou null
                if (!empty($acte['Deliberation']['signee'])) {
                    $signature_etat = $this->Bs->icon('award') . ' ' . __('Signature manuscrite');
                } elseif ($acte['Deliberation']['etat']===10) {
                    $signature_etat = __('À renvoyer');
                } else {
                    $signature_etat = __('Non envoyé');
                }
        }
    }

    if ($this->action === "autresActesValides" && !empty($actes)) {
        if (
            empty($acte['Deliberation']['signee'])
            && in_array($acte['Deliberation']['parapheur_etat'], [null, 0, -1], true)
            && in_array($acte['Deliberation']['etat'], [2, 3, 10], true)
        ) {
            $options = ['class' => 'masterCheckbox_checkbox', 'autocomplete' => 'off'];
        } else {
            $options = ['disabled' => true, 'autocomplete' => 'off'];
        }

        echo $this->Bs->cell($this->Form->checkbox('Deliberation.' . $acte['Deliberation']['id'] . '.is_checked', $options));
    }

    echo $this->Bs->cell($this->Html->link($acte['Deliberation']['id'], ['controller'=> 'projets', 'action' => 'view', $acte['Deliberation']['id']]));
    if ($this->action === 'autresActesValides') {
        echo $this->Bs->cell(!empty($acte['Deliberation']['num_delib']) ? $acte['Deliberation']['num_delib'] : '');
    }
    echo $this->Bs->cell($acte['Typeacte']['name']);
    echo $this->Bs->cell($acte['Deliberation']['objet']);
    echo $this->Bs->cell($acte['Deliberation']['titre']);
    //////////////////////////////////
    echo $this->Bs->cell($acte['Circuit']['nom']);

    if ($this->action === 'autresActesValides') {
        echo $this->Bs->cell(
            !empty($acte['Deliberation']['parapheur_etat']) ? '':
            $this->BsForm->dateTimepicker(
            'Deliberation.' . $acte['Deliberation']['id'] . '.date_acte',
            [
                'language' => 'fr',
                'autoclose' => 'true',
                'minView' => '2',
                'fontAwesome' => true,
                'pickerPosition' => 'bottom-right'],
            [
                'label' => false,
                'title' => __('Choisissez la date de signature'),
                'style' => 'cursor:pointer;width:8em;background-color: white;',
                'readonly' => 'readonly',
                'autocomplete' => 'off',
                'value' => $acte['Deliberation']['date_acte']]
        ));
        echo $this->Bs->cell((!empty($signature_etat) ? $signature_etat : null));
    } else {
        echo $this->Bs->cell($projet_etat[$acte['Deliberation']['etat']]);
    }
    echo $this->element('Projet/actions', ['projet' => $acte, 'cell_class' => 'text-right']);
}
echo $this->Bs->endTable();
//paginate
echo $this->element('paginator', ['paginator' => $this->Paginator]);




if ($this->action == "autresActesValides" && !empty($actes)) {
    $actions = [
        'generation' => __('Génération de document(s)'),
        !$acl['sendAutresActesToSignature'] ? null :'generationNumerotation' => __('Génération de la numérotation'),
        !$acl['sendAutresActesToSignature'] ? null :'signatureManuscrite' => __('Signature manuscrite'),
        !$acl['autresActesAbandon'] ? null :'autresActesAbandon' => __('Abandonner')  ,
        (!isset($circuits) && empty($circuits)) || !$acl['sendAutresActesToSignature']  ? null : 'signatureElectronique' => __('Signature électronique'),
    ];

    echo $this->element('Projet/traitement_lot', [
        'modeles' => isset($modeles) ? $modeles : null,
        'traitement_lot' => isset($traitement_lot) ? $traitement_lot : null,
        'actions_possibles'=>$actions]);


    echo $this->Bs->close();

    echo $this->BsForm->end();

    echo $this->Html->tag('br');
}

echo $this->Html2->btnCancel($previous);
