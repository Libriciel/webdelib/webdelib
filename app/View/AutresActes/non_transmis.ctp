<?php
$nbProjets = isset($this->Paginator) ? $this->Paginator->counter('{:count}') : $nbProjets;
$titre = __('Autres actes non transmissibles au contrôle de légalité') . ' (' . $nbProjets . ' ' . ($nbProjets > 1 ? 'projets' : 'projet') . ')';

$this->Html->addCrumb(__('Tous les projets'));
$this->Html->addCrumb(__('Autres actes'));
$this->Html->addCrumb(__('Non transmissibles'));

echo $this->element('filtre');
echo $this->Bs->tag('h3', $titre);

echo $this->BsForm->create('Deliberation', [
    'id' => 'appSeance',
    'url' => ['controller' => 'ExportGed', 'action' => 'sendToGedAutreActe'],
]);

$title = [['title' => $this->BsForm->checkbox(null, ['id' => 'masterCheckbox', 'inline' => 'inline', 'autocomplete' => 'off'])], ['title' => $this->Paginator->sort('id', 'Id')], ['title' => $this->Paginator->sort('num_delib', __('N° de l\'acte'))], ['title' => $this->Paginator->sort('objet_delib', __('Libellé de l\'acte'))], ['title' => $this->Paginator->sort('Deliberation.date_acte', __('Date de signature'))], ['title' => __('Annexe(s)')], ['title' => __('N° Dépot')], ['title' => __('Actions')]];

echo $this->Bs->table($title, ['hover', 'striped']);

foreach ($this->data as $projet) {
    echo $this->Bs->cell($this->BsForm->checkbox('Deliberation.' . $projet['Deliberation']['id'] . '.send', ['label' => false, 'autocomplete' => 'off', 'class' => 'masterCheckbox_checkbox', 'inline' => 'inline', 'checked' => false]));
    echo $this->Bs->cell($this->Html->link($projet['Deliberation']['id'], ['controller' => 'projets', 'action' => 'view', $projet['Deliberation']['id']]));
    echo $this->Bs->cell($projet['Deliberation']['num_delib']);
    echo $this->Bs->cell($projet['Deliberation']['objet_delib']);
    echo $this->Bs->cell(ucfirst($this->Time->i18nFormat($projet['Deliberation']['date_acte'], '%A %d %B %Y')));
    $annexes = '';
    if (!empty($projet['Annexe'])) {
        //$annexes=$this->Bs->link('Annexes'.'<span class="badge">'.count($projet['Annexe']).'</span>', array(), array('escape'=>false));
        $annexes.='<div class="dropdown">'
                . '<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">'
                . __('Liste des annexes');
        if (!empty($projet['Deliberation']['tdt_ar_annexes_status'])) {
            $annexes.=' <span class="label label-' . $projet['Deliberation']['tdt_ar_annexes_status'] . ' label-as-badge"'
                    . 'title="' . $projet['Deliberation']['tdt_ar_annexes_status_libelle'] . '"'
                    . '>' . count($projet['Annexe']) . '</span>';
        } else {
            $annexes.=' <span class="label label-info label-as-badge">' . count($projet['Annexe']) . '</span>';
        }

        $annexes.=' <span class="caret"></span>'
                . '</button>'
                . '<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">';
        foreach ($projet['Annexe'] as $annexe) {
            $annexes.='<li role="presentation">' .
                    $this->Bs->btn($this->Bs->icon('download') . ' ' .$annexe['filename'], ['controller' => 'annexes', 'action' => 'download', $annexe['id']], ['escapeTitle' => false, 'role' => 'menuitem', 'tabindex' => -1])
                    . '</li>';
        }
        echo '</ul>'
        . '</div>';
    }
    echo $this->Bs->cell($annexes);

    echo $this->Bs->cell($projet['Deliberation']['numero_depot']);

    //Représente une liste d'actions dans une cellule du tableau
    echo $this->element('Deliberations/deliberation_actions_signature',['deliberation'=>$projet,'type'=>'autreActe']);
}

echo $this->Bs->endTable() .
 $this->Paginator->numbers(['before' => '<ul class="pagination">', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a', 'tag' => 'li', 'after' => '</ul><br />']);

if (!empty($projet)) {
    echo $this->element('Projet/traitement_lot', [
        'modeles' => isset($modeles) ? $modeles : null,
        'traitement_lot' => isset($traitement_lot) ? $traitement_lot : null,
        'actions_possibles'=>[
            'sendToGed' => __('Envoyer à la GED'),
        ]]);
}
echo $this->BsForm->end();
?>
<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {
        $('#masterCheckbox').attr('checked', false);
        $('#sendToGed').addClass('disabled');
        $("#sendToGed").prop("disabled", true);
        $('input[type="checkbox"]').change(changeSelection);
        changeSelection();

        /**
         * Afficher/Masquer la sélection de circuit selon si la selection est vide ou non
         */
        function changeSelection() {
            if ($('input[type="checkbox"]:checked').length > 0) {
                $('#sendToGed').removeClass('disabled');
                $("#sendToGed").prop("disabled", false);
            } else {
                $('#sendToGed').addClass('disabled');
                $("#sendToGed").prop("disabled", true);
            }
        }
    });
});
</script>
