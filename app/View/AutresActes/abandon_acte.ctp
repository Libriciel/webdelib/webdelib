<?php

    $formAbandonActe =
        $this->BsForm->create('Deliberation', [
            'type' => 'post',
            'url' => [
                'controller' => 'autres_actes', 'action' => 'autresActesAbandon'
            ]
            ]
        );
$this->BsForm->setLeft(3);
    $formAbandonActe .=
        $this->BsForm->input(
            'Deliberation.id',
            [
                'value' => $projet['Deliberation']['id'],
                'type'=>'hidden'
            ]
        ).
        $this->BsForm->input('Deliberation.commentaire', [
            'fieldset' => false,
            'legend' => false,
            'label' => __('Motivation'),
            'maxlength' => '1500',
            'value' => '',
            'help'=> __('Votre commentaire ne peut dépasser 1500 caractères.'),
            'required' => true,
            'type' => 'textarea']);
    $formAbandonActe .= $this->BsForm->end();

    echo $formAbandonActe;