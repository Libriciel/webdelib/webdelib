<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Types d\'acteur'), ['action' => 'index']);

if ($this->Html->value('Typeacteur.id')) {
    $this->Html->addCrumb(__('Modifier un type d\'acteur'));
    echo $this->Bs->tag('h3', __('Modifier un type d\'acteur : ') . $this->Html->value('Typeacteur.nom'));
    echo $this->BsForm->create('Typeacteur', ['url' => ['action' => 'edit', $this->Html->value('Typeacteur.id')], 'type' => 'post']);
} else {
    $this->Html->addCrumb(__('Ajouter un type d\'acteur'));
    echo $this->Bs->tag('h3', __('Ajouter un type d\'acteur'));
    echo $this->BsForm->create('Typeacteur', ['url' => ['action' => 'add'], 'type' => 'post']);
}
echo $this->BsForm->input('Typeacteur.nom', ['label' => __('Nom') . ' <abbr title="obligatoire">*</abbr>']) .
 $this->BsForm->input('Typeacteur.commentaire', ['label' => __('Commentaire')]);

echo $this->BsForm->radio('Typeacteur.elu', $eluNonElu, ['label' => __('Statut') . ' <abbr title="obligatoire">*</abbr>']);
if ($this->BsForm->isFieldError('Typeacteur.elu')) {
    echo $this->BsForm->error('Typeacteur.elu');
}
echo $this->BsForm->hidden('Typeacteur.id') .
 $this->Html2->btnSaveCancel('', $previous) .
 $this->BsForm->end();
