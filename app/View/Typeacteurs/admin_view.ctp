<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Type d\'acteur'), ['action' => 'index']);
echo $this->Bs->tag('h3', __('Type d\'acteur'));
?>
<div class="panel panel-default">
    <div class="panel-heading"><?php echo __('Fiche type d\'acteur: '); ?><?php echo $typeacteur['Typeacteur']['nom'] ?></div>
    <div class="panel-body">
        <dl>
            <div class="demi">
                <dt><?php echo __('Nom'); ?></dt>
                <dd>&nbsp;<?php echo $typeacteur['Typeacteur']['nom'] ?></dd>
            </div>
            <div class="demi">
                <dt><?php echo __('Commentaire'); ?></dt>
                <dd>&nbsp;<?php echo $typeacteur['Typeacteur']['commentaire'] ?></dd>
            </div>

            <div class="spacer"></div>

            <div class="demi">
                <dt><?php echo __('Statut'); ?></dt>
                <dd>&nbsp;<?php echo $typeacteur['Typeacteur']['elu'] ? __('élu') : __('non élu'); ?></dd>
            </div>

            <div class="spacer"></div>

            <div class="demi">
                <dt><?php echo __('Date de création'); ?></dt>
                <dd>&nbsp;<?php echo $typeacteur['Typeacteur']['created'] ?></dd>
            </div>
            <div class="demi">
                <dt><?php echo __('Date de modification'); ?></dt>
                <dd>&nbsp;<?php echo $typeacteur['Typeacteur']['modified'] ?></dd>
            </div>
        </dl>

        <br/>
        <?php
        echo $this->Bs->row() .
        $this->Bs->col('md4 of5');
        echo $this->Bs->div('btn-group', null, ['id' => "actions_fiche"]) .
        $this->Html2->btnCancel(),
        (
            $this->permissions->check('Typeacteurs', 'update')
                ? $this->Bs->btn($this->Bs->icon('pencil-alt') .' '. 'Modifier', ['action' => 'edit', $typeacteur['Typeacteur']['id']], ['type' => 'primary', 'escapeTitle' => false, 'title' => __('Modifier')])
                : ''
        ) .
        $this->Bs->close(6);
