<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Types d\'acteur'));
echo $this->Bs->tag('h3', __('Liste des types d\'acteur'));

if ($this->permissions->check('Typeacteurs', 'create')) {
    echo $this->Html2->btnAdd(__("Ajouter un type d'acteur"), __("Ajouter"));
}

echo $this->Bs->table([['title' => __('Libellé')], ['title' => __('Commentaire'), 'class'=>'text-center'], ['title' => __('Statut'), 'class'=>'text-center'], ['title' => __('Actions'), 'class'=>'text-center']], ['hover', 'striped']);
foreach ($typeacteurs as $typeacteur) {
    echo $this->Bs->cell($typeacteur['Typeacteur']['nom']);
    echo $this->Bs->cell($typeacteur['Typeacteur']['commentaire']);
    echo $this->Bs->cell($typeacteur['Typeacteur']['elu'] ? 'élu' : 'non élu', 'text-center');
    echo $this->Bs->cell(
            $this->Bs->div('btn-group') .
            (
                $this->permissions->check('Typeacteurs', 'read')
                    ? $this->Bs->btn($this->Bs->icon('eye', null, ['stylePrefix' => 'far']), ['controller' => 'typeacteurs', 'action' => 'view', $typeacteur['Typeacteur']['id']], ['type' => 'default', 'escapeTitle' => false, 'title' => __('Visualiser')])
                    : ''
            ) .
            (
                $this->permissions->check('Typeacteurs', 'update')
                    ? $this->Bs->btn($this->Bs->icon('pencil-alt'), ['controller' => 'typeacteurs', 'action' => 'edit', $typeacteur['Typeacteur']['id']], ['type' => 'primary', 'escapeTitle' => false, 'title' => __('Modifier')])
                    : ''
            ) .
            (
                $this->permissions->check('Typeacteurs', 'delete')
                    ? $this->Bs->btn($this->Bs->icon('trash'), ['controller' => 'typeacteurs', 'action' => 'delete', $typeacteur['Typeacteur']['id']], ['type' => 'danger', 'escapeTitle' => false, 'title' => __('Supprimer'), 'class' => empty($typeacteur['Typeacteur']['is_deletable']) ? 'disabled' : '', 'confirm' => __('Êtes-vous sûr de vouloir supprimer %s ?', $typeacteur['Typeacteur']['nom'])])
                    : ''
            ) .
            $this->Bs->close(), 'text-center'
    );
}
echo $this->Bs->endTable();
