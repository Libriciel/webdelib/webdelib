<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Informations sup.'));
$this->Html->addCrumb(__('Liste des informations supplémentaires'), ['action' => 'index', $this->Html->value('Infosupdef.model')]);

echo $this->Bs->tag('h3', $titre);
?>
<div class="panel panel-default">
    <div class="panel-heading"><?php echo __('Fiche') . ' : ' . $this->data['Infosupdef']['nom'] ?></div>
    <div class="panel-body">
        <dl>
            <div class="imbrique">
                <dt><?php echo __('Nom'); ?></dt>
                <dd><?php echo $this->data['Infosupdef']['nom']; ?></dd>
            </div>
            <div class="imbrique">
                <div class="gauche">
                    <dt><?php echo __('Date de création'); ?></dt>
                    <dd><?php echo $this->data['Infosupdef']['created'] ?></dd>
                </div>
                <div class="droite">
                    <dt><?php echo __('Date de modification'); ?></dt>
                    <dd><?php echo $this->data['Infosupdef']['modified'] ?></dd>
                </div>
            </div>
            <dt><?php echo __('Commentaire'); ?></dt>
            <dd><?php echo $this->data['Infosupdef']['commentaire']; ?></dd>
            <dt><?php echo __('Code'); ?></dt>
            <dd><?php echo $this->data['Infosupdef']['code']; ?></dd>
            <dt><?php echo __('Numéro d\'ordre'); ?></dt>
            <dd><?php echo $this->data['Infosupdef']['ordre']; ?></dd>
            <dt><?php echo __('Type'); ?></dt>
            <dd><?php echo $this->data['Infosupdef']['libelleType']; ?></dd>
            <dt><?php echo __('Valeur initiale'); ?></dt>
            <dd><?php echo $this->data['Infosupdef']['val_initiale']; ?></dd>
            <dt><?php echo __('Inclure dans la recherche'); ?></dt>
            <dd><?php echo $this->data['Infosupdef']['libelleRecherche']; ?></dd>
            <dt><?php echo __('Active'); ?></dt>
            <dd><?php echo $this->data['Infosupdef']['libelleActif']; ?></dd>
            <dt><?php echo __('API'); ?></dt>
            <dd><?php echo $this->data['Infosupdef']['api']; ?></dd>
        </dl> 

        <br/>
        <?php
        echo $this->Bs->row() .
        $this->Bs->col('md4 of5');
        echo $this->Bs->div('btn-group', null, ['id' => "actions_fiche"]) .
        $this->Html2->btnCancel($previous),
        $this->Bs->btn($this->Bs->icon('pencil-alt') .' '. __('Modifier'), ['controller' => 'infosupdefs', 'action' => 'edit', $this->data['Infosupdef']['id']], ['type' => 'primary', 'escapeTitle' => false, 'title' => __('Modifier')]) .
        $this->Bs->close(6);
