<?php

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Informations sup.'));
$this->Html->addCrumb($titre);
echo $this->Bs->tag('h3', $titre);
echo $this->Bs->btn($this->Bs->icon('plus-circle') .' '.__('Ajouter une information supplémentaire'), $lienAdd, [
    'id' => 'bouton_ajouter_infosup',
    'type' => "primary",
    'escapeTitle' => false,
    'title' => __('Ajouter une information supplémentaire')]);

echo $this->element('filtre');
$titres = [['title' => __('Ordre')],
['title' => __('Libellé')],
['title' => __('Code')],
['title' => __('Type')],
['title' => __('Recherche')],
['title' => __('Joindre à la fusion')]
];
if (!empty(Configure::read('USE_SAE'))) {
    $titres[]= ['title' => 'Joindre au SAE'];
}
if (!empty($modifiableApresSignature)) {
    $titres[]= ['title' => $modifiableApresSignature];
}
$titres[]= ['title' => __('Actions')];

echo $this->Bs->table($titres, ['hover', 'striped']);
$this->BsForm->setFormType('vertical');
foreach ($this->data as $infosupdef) {

    //Bouttons actions
    $actions = $this->Bs->div('btn-group') .
            $this->Bs->btn($this->Bs->icon('eye', null, ['stylePrefix' => 'far']), ['controller' => 'infosupdefs', 'action' => 'view', $infosupdef['Infosupdef']['id']], [
                'type' => 'default',
                'escapeTitle' => false,
                'title' => __('Visualiser')]) .
        $this->Bs->btn($this->Bs->icon(($infosupdef['Infosupdef']['actif']) ? 'toggle-on' : 'toggle-off'), [
            'controller' => 'infosupdefs',
            'action' => ($infosupdef['Infosupdef']['actif']) ? 'disable' : 'enable',
            $infosupdef['Infosupdef']['id'], $infosupdef['Infosupdef']['model']], [
            'type' => ($infosupdef['Infosupdef']['actif']) ? 'default' : 'success',
            'escape' => false,
            'title' => (($infosupdef['Infosupdef']['actif']) ? __('Désactiver') : __('Activer')), __('Êtes-vous sûr de vouloir %s %s ?', (($infosupdef['Infosupdef']['actif']) ? __('désactiver') : __('activer')), $infosupdef['Infosupdef']['nom'])]) ;
    if (in_array($infosupdef['Infosupdef']['type'], ['list', 'listmulti'], true)) {
        $actions .= $this->Bs->btn($this->Bs->icon('list'), ['controller' => 'infosuplistedefs', 'action' => 'index', $infosupdef['Infosupdef']['id']], [
            'type' => 'default',
            'escapeTitle' => false,
            'title' => __('Liste')]);
    }
    $actions .=
            $this->Bs->btn($this->Bs->icon('pencil-alt'), ['controller' => 'infosupdefs', 'action' => 'edit', $infosupdef['Infosupdef']['id']], [
                'type' => 'primary',
                'title' => __('Modifier'),
                'escapeTitle' => false]) .
            $this->Bs->btn($this->Bs->icon('trash'), ['controller' => 'infosupdefs', 'action' => 'delete', $infosupdef['Infosupdef']['id']], [
                'type' => 'danger',
                'title' => __('Supprimer'),
                'escapeTitle' => false,
                'class' => !$Infosupdef->isDeletable($infosupdef['Infosupdef']['id']) ? 'disabled' : ''], __('Êtes-vous sûr de vouloir supprimer %s ?', $infosupdef['Infosupdef']['nom'])) .
            $this->Bs->close();

    echo $this->Bs->cell(
        $this->BsForm->select('Infosupdefs.ordre', $aPosition, [
            'value' => $infosupdef['Infosupdef']['ordre'],
            'autocomplete' => 'off',
            'label' => false,
            'class' => 'input-sm selectone wd-change-order',
            'id' => 'InfosupOrdreId' . $infosupdef['Infosupdef']['id'],
            'data-minimum-results-for-search' => 10,
            'data-wd-infosup-id' => $infosupdef['Infosupdef']['id'],
            'data-wd-infosup-model' => $infosupdef['Infosupdef']['model'],
        ]) .
        $this->Bs->btn(
            null,
            ['controller' => 'infosupdefs',
            'action' => 'changeOrder',
            $infosupdef['Infosupdef']['id']],
            [  'style' => 'Display:none',
                    'id' => 'InfosupOrdreId' . $infosupdef['Infosupdef']['id'] . 'link']
        )
    );

    echo $this->Bs->cell($infosupdef['Infosupdef']['nom']);
    echo $this->Bs->cell($infosupdef['Infosupdef']['code']);
    echo $this->Bs->cell($Infosupdef->libelleType($infosupdef['Infosupdef']['type']));
    echo $this->Bs->cell($Infosupdef->libelleRecherche($infosupdef['Infosupdef']['recherche']), 'text-center');
    echo $this->Bs->cell($Infosupdef->libelleActif($infosupdef['Infosupdef']['joindre_fusion']), 'text-center');
    if(Configure::read('USE_SAE')){
        echo $this->Bs->cell($Infosupdef->libelleActif($infosupdef['Infosupdef']['joindre_sae']), 'text-center');
    }
    if(!empty($modifiableApresSignature)) {
        echo $this->Bs->cell($Infosupdef->libelleActif($infosupdef['Infosupdef']['edit_post_sign']), 'text-center');
    }
    echo $this->Bs->cell($actions, 'text-center');
}
echo $this->Bs->endTable();
echo $this->element('paginator', ['paginator' => $this->Paginator]);
?>

<script type="text/javascript">
    require(['domReady'], function (domReady) {
    domReady(function () {
        //Lors d'action sur un select wd-change-order:
        $('.wd-change-order').on('change',function() {
            document.location = "/infosupdefs/changeOrder/" + $( this ).attr('data-wd-infosup-id') + "/" + $(this).val() + '/' +$( this ).attr('data-wd-infosup-model');
        });
    });
});
</script>
