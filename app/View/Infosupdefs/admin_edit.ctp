<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Informations sup.'));

echo $this->Bs->tag('h3', $titre);

$this->Html->addCrumb(__('Liste des informations supplémentaires'), ['action' => 'index', $this->Html->value('Infosupdef.model')]);

$this->Html->addCrumb($titre);

$htmlAttributes['disabled'] = false;
$empty = false;
if ($this->request['action'] === 'admin_add') {
    echo $this->BsForm->create('Infosupdef', ['url' => [
        'action' => $this->request->action, $this->Html->value('Infosupdef.model')],
        'type' => 'post', 'name' => 'infoSupForm',
        'novalidate' => true]);
} else {
    echo $this->BsForm->create('Infosupdef', ['url' => [
        'action' => $this->request->action],
        'type' => 'post', 'name' => 'infoSupForm',
        'novalidate' => true]);
    $htmlAttributes['disabled'] = true;
    echo $this->BsForm->hidden('Infosupdef.type');
    $empty = true;
}
?>
<div class="required">
    <?php echo $this->BsForm->input('Infosupdef.nom', ['label' => __('Nom') . ' <abbr title="obligatoire">*</abbr>', 'size' => '40', 'title' => __('Nom affiché dans le formulaire d\'édition des projets')]); ?>
</div>
<div class="required">
    <?php echo $this->BsForm->input('Infosupdef.commentaire', ['label' => __('Commentaire'), 'size' => '80', 'title' => __('Bulle d\'information affichée dans le formulaire d\'édition des projets')]); ?>
</div>
<div class="required">
    <?php
    echo $this->BsForm->input('Infosupdef.code', [
            'label' => __('Code') . ' <abbr title="obligatoire">*</abbr>',
        'size' => '40',
        'id' => 'infosupdefCode',
        'title' => __('Code unique utilisé pour les éditions (pas d\'espace ni de caractère spécial)')
    ]);
    ?>
</div>
<div class="required">
    <?php
    echo $this->BsForm->select('Infosupdef.type', $types, [
        'class' => 'selectone',
        'data-placeholder'=> __('Sélectionner un type'),
        'data-allow-clear'=> $empty,
        'label' => __('Type') . ' <abbr title="obligatoire">*</abbr>',
        'id' => 'selectTypeInfoSup',
        'disabled' => $htmlAttributes['disabled'],
        'empty' => $empty,
        'help' => __('Note : la gestion des éléments de type liste est accessible à partir de la liste des informations supplémentaires.')]);
    ?>
</div>
<?php
echo $this->Bs->div(null, $this->BsForm->select('Infosupdef.typemime', $typemimes, [
    'class' => 'selectone',
    'label' => __('Typemime'),
    'width' => '100%',
    'empty' => false]), ['id'=>'val_typemime']);

echo $this->BsForm->input('Infosupdef.val_initiale', [
    'label' => __('Valeur initiale'),
    'size' => '80',
    'before' => '<div class="required" id="val_initiale">',
    'after' => '</div>',
    'title' => __('Valeur initiale lors de la création d\'un projet')]);

echo '<div class="required" id="val_initiale_boolean">' . $this->BsForm->select('Infosupdef.val_initiale_boolean', $listEditBoolean, [
    'class' => 'selectone',
    'label' => __('Valeur initiale'),
]) . '</div>';

echo $this->BsForm->datetimepicker(
    'Infosupdef.val_initiale_date',
    [
    'language' => 'fr',
    'autoclose' => 'true',
    'format' => 'dd/mm/yyyy',
    'minView' => 2],
    [
    'label' => __('Valeur initiale'),
    'title' => __('Valeur initiale lors de la création d\'un projet'),
    'style' => 'cursor:pointer;background-color: white;',
    'before' => '<div class="required" id="val_initiale_date">',
    'after' => '</div>',
    'help' => __('Cliquez sur le champ ci-dessus pour choisir la date'),
    'readonly' => 'readonly',
    'value' => $this->Html->value('Deliberation.date_limite')
        ]
);

echo $this->Bs->div(null, $this->BsForm->checkbox('Infosupdef.reactcomponent', [
    'label' => __('Ordonner selon la sélection de l\'utilisateur'),
    'before' => '',
    'after' => '</div>',
]), ['id'=>'react_component']);

if ($this->Html->value('Infosupdef.model') == 'Deliberation') {
    echo $this->Bs->div(null, $this->BsForm->checkbox('Infosupdef.recherche', ['label' => __('Inclure dans la recherche')]), ['id'=>'recherche']);
} else {
    echo $this->BsForm->hidden('Infosupdef.recherche', ['value' => false]);
}
if ($this->Html->value('Infosupdef.model') == 'Seance' && Configure::read('USE_SAE')) {
    echo $this->Bs->div(null, $this->BsForm->checkbox('Infosupdef.joindre_sae', ['label' => __('Joindre au SAE')]), ['id'=>'joindre_sae']);
} else {
    echo $this->BsForm->hidden('Infosupdef.joindre_sae', ['value' => false]);
}

echo $this->Bs->div(null, $this->BsForm->checkbox('Infosupdef.joindre_fusion', [
    'label' => __('Joindre à la fusion'),
    'before' => '',
    'after' => '</div>',
    ]), ['id'=>'val_joindre_fusion']);

if ($this->Html->value('Infosupdef.model') == 'Deliberation') {
    echo $this->Bs->div(null, $this->BsForm->checkbox('Infosupdef.edit_post_sign', ['label' => __('Modifiable après signature')]), ['id'=>'val_edit_post_sign']);
} else {
    echo $this->BsForm->hidden('Infosupdef.edit_post_sign', ['value' => false]);
}

if ($this->request['action'] === 'admin_add') {
    echo $this->BsForm->checkbox('Infosupdef.actif', [
    'label' => __('information active')]);
    echo $this->Html->tag('div', '', ['class' => 'spacer']);
}
echo $this->BsForm->select('Profil', $profils, [
    'class' => 'selectmultiple',
    'multiple' => true,
    'label' => __('Profils autorisés'),
    'title' => __('l\'information supplémentaire ne sera utilisable que pour les profils sélectionnés dans cette liste')
]);
?>

<div class="required" id="api_geoloc">
    <legend><?php echo __('Informations de l\'API'); ?></legend>
    <?php
        echo $this->BsForm->input('Infosupdef.api',
        [
            'label' => __('API') . ' <abbr title="obligatoire">*</abbr>',
            'size' => '200',
            'placeholder' => __($defaults['geojson']['api']),
            'value' => isset($this->request->data['Infosupdef']['api'])
                ? $this->request->data['Infosupdef']['api']
                : $defaults['geojson']['api']
        ]);
    ?>
</div>

<div class="required" id="apiType">
    <?php
        echo $this->BsForm->input('Infosupdef.api_type',
            [
                'label' => __('Type de résultat') . ' <abbr title="obligatoire">*</abbr>',
                'placeholder' => __($defaults['geojson']['api_type']),
                'value' => isset($this->request->data['Infosupdef']['api_type'])
                    ? $this->request->data['Infosupdef']['api_type']
                    : $defaults['geojson']['api_type']
            ]
        );
        echo $this->BsForm->hidden('Infosupdef.id');
    ?>
</div>

<div class="required" id="apiQuery">
    <?php
    echo $this->BsForm->input('Infosupdef.api_query',
        [
            'label' => __('Paramètre de recherche') . ' <abbr title="obligatoire">*</abbr>',
            'placeholder' => __($defaults['geojson']['api_query']),
            'value' => isset($this->request->data['Infosupdef']['api_query'])
                ? $this->request->data['Infosupdef']['api_query']
                : $defaults['geojson']['api_query']
        ]
    );
    ?>
</div>

<div class="required" id="apiLimit">
    <?php
    echo $this->BsForm->input('Infosupdef.api_limit',
        [
            'label' => __('Paramètre de limitation du nombre d\'élément') . ' <abbr title="obligatoire">*</abbr>',
            'placeholder' => __($defaults['geojson']['api_limit']),
            'value' => isset($this->request->data['Infosupdef']['api_limit'])
                ? $this->request->data['Infosupdef']['api_limit']
                : $defaults['geojson']['api_limit']
        ]
    );
    ?>
</div>

<fieldset id="info_sup">
    <legend><?php echo __('Tableau des concordances GeoJson'); ?></legend>
    <?php
    $data = isset($this->request->data['Infosupdef']['correlation'])
        ? json_decode($this->request->data['Infosupdef']['correlation'], true)
        : [];
    foreach ($geojsonLabels as $label) {
        $default = $defaults['geojson'][$label];
        echo $this->BsForm->input('Infosupdef.geojson.' . $label, [
            'type' => 'text',
            'label' => $label,
            'value' => isset($data[$label]) ? $data[$label] : $default,
            ]);
        }
    ?>
</fieldset>

<?php
echo $this->BsForm->hidden('Infosupdef.model');
echo $this->BsForm->hidden('Infosupdef.id');
echo $this->BsForm->hidden('Typeacte.id');
echo $this->Html2->btnSaveCancel('', $previous);
?>

<?php echo $this->BsForm->end(); ?>

<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {
    var extensionsFusion = <?php echo(!empty($typemimes_fusion) ? json_encode($typemimes_fusion) : '[]'); ?>;
        /*
         * Affiche ou masque les options en fonction du type d'info sup
         */
        $("#selectTypeInfoSup").change(changeSelection);
        changeSelection();

        $("#InfosupdefTypemime").change(function(){
            var extension = $(this).val();
            if (extensionsFusion[extension] == undefined) {
                $("#val_joindre_fusion").hide();
            } else {
                $("#val_joindre_fusion").show();
            }
            if (extension === 'application/pdf') {
                $("#joindre_sae").show();
            } else {
                $("#joindre_sae").hide();
            }
        });

    function changeSelection() {

            var val = $("#selectTypeInfoSup").val();

            /* On masque toutes les options */
            $("#val_initiale").hide();
            $("#val_initiale_boolean").hide();
            $("#val_initiale_date").hide();
            $("#recherche").hide();
            $("#gestionListe").hide();
            $("#val_typemime").hide();
            $("#val_joindre_fusion").hide();
            $("#react_component").hide();
            $("#info_sup").hide();
            $("#apiType").hide();
            $("#api_geoloc").hide();
            $("#apiQuery").hide();
            $("#apiLimit").hide();
            /* si le choix est vide : on sort */
            if ((val.length == 0) || (val == null))
                return;

            /* on affiche en fonction du type d'info sup */
            switch (val) {
                case "text":
                    $("#val_initiale").show();
                    if ($("#recherche"))
                        $("#recherche").show();
                    $("#val_joindre_fusion").show();
                    $("#joindre_sae").hide();
                    break;
                case "richText":
                    $("#val_initiale").show();
                    if ($("#recherche"))
                        $("#recherche").show();
                    $("#val_joindre_fusion").show();
                    $("#joindre_sae").hide();
                    break;
                case "date":
                    $("#val_initiale_date").show();
                    if ($("#recherche"))
                        $("#recherche").show();
                    $("#val_joindre_fusion").show();
                    $("#joindre_sae").hide();
                    break;
                case "boolean":
                    $("#val_initiale_boolean").show();
                    if ($("#recherche"))
                        $("#recherche").show();
                    $("#val_joindre_fusion").show();
                    $("#joindre_sae").hide();
                    break;
                case "list":
                    if ($("#recherche"))
                        $("#recherche").show();
                    $("#gestionListe").show();
                    $("#val_joindre_fusion").show();
                    $("#joindre_sae").hide();
                    break;
                case "listmulti":
                    if ($("#recherche"))
                        $("#recherche").show();
                    $("#gestionListe").show();
                    $("#val_joindre_fusion").show();
                    $("#react_component").show();
                    $("#joindre_sae").hide();
                    break;
                case "odtFile":
                    $("#val_joindre_fusion").show();
                    $("#joindre_sae").hide();
                    break;
                case "file":
                    $("#val_typemime").show();
                    var extension = $("#InfosupdefTypemime").val();
                    if (extensionsFusion[extension] == undefined) {
                        $("#val_joindre_fusion").hide();
                        $("#joindre_sae").hide();
                    } else {
                        $("#val_joindre_fusion").show();
                        $("#joindre_sae").show();
                    }
                    break;
                case "geojson":
                    $("#val_joindre_fusion").show();
                    $("#info_sup").show();
                    $("#apiType").show();
                    $("#apiQuery").show();
                    $("#apiLimit").show();
                    $("#api_geoloc").show();
                    break;
            }
    }
    });
});
</script>

