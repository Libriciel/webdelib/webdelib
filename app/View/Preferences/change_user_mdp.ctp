<?php
echo $this->Html->css('/js/components/dist/css/ls-jquery-password.css');

$this->Html->addCrumb(__('Préférences utilisateur'), ['controller' => 'preferences', 'action' => 'index']);

$this->Html->addCrumb(__('Changement du mot de passe'));

echo $this->Bs->tag('h3', __('Changer le mot de passe pour %s', $infoUser)) .
    $this->BsForm->create('User', [
            'url' => ['controller' => 'preferences', 'action' => 'changeUserMdp'],
            'type' => 'post', 'autocomplete' => 'off',
            'id' => 'appPassword'
        ]
    ) .
    $this->BsForm->input('User.oldPassword', [
        'id' => 'oldPassword',
        'type' => 'password',
        'label' => __('Ancien mot de passe') . ' <acronym title="obligatoire">*</acronym>',
        'value' => $this->Html->value('User.oldPassword'),
        'autocomplete' => 'Ancien mot de passe'
    ]).
    $this->BsForm->input( 'User.password', [
            'id' => 'password',
            'type' => 'password',
            'label' => __('Mot de passe') . ' <acronym title="obligatoire">*</acronym>',
            'value' => $this->Html->value('User.password'),
            'autocomplete' => 'Nouveau mot de passe',
            'data-wd-min-entropie' => $minEntropie,
        ]
    ) .
    $this->BsForm->input('User.passwordConfirm', [
        'id' => 'passwordConfirm',
        'type' => 'password',
        'label' => __('Confirmez le mot de passe') . ' <acronym title="obligatoire">*</acronym>',
        'autocomplete' => 'Confirmation du mot de passe',
        'value' => $this->Html->value('User.passwordConfirm'),
        'autocomplete' => 'off'
    ]) .
 $this->Html2->btnSaveCancel('', $previous, __("Enregistrer le mot de passe"), __("Enregistrer")) .
 $this->BsForm->end();
