<?php

echo $this->Bs->tag('h3', __('Changement de service émetteur'));
echo $this->BsForm->create(null, ['url' => ['admin' => false, 'prefix' => null, 'controller' => 'preferences', 'action' => 'changeServiceEmetteur']]);
echo $this->BsForm->select('User.ServiceEmetteur.id', $services, ['value' => $this->Html->value('User.ServiceEmetteur.id'), 'autocomplete' => 'off', 'empty' => false, 'label' => __('Service émetteur'), 'help' => __('Choix du service d\'émissions des projets'), 'onChange' => 'javascript: this.form.submit()']);
echo $this->BsForm->end();
