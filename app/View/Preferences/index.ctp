<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Préférences utilisateur'));

echo $this->Bs->tag('h3', __('Préférences utilisateur'));
echo $this->BsForm->create('Preferences', ['url' => ['controller' => 'preferences', 'action' => 'index'], 'type' => 'post']);

echo $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
    $this->Html->tag('div', __('Vos informations'), ['class' => 'panel-heading']) .
$this->Html->tag('div', null, ['class' => 'panel-body']) .
$this->Bs->row() . $this->Bs->col('xs4') . '<b>' . __('Nom') . ' :</b> ' . $this->Html->value('User.nom') . $this->Bs->close(2) .
$this->Bs->row() . $this->Bs->col('xs4') . '<b>' . __('Prénom') . ' :</b> ' . $this->Html->value('User.prenom') . $this->Bs->close(2) .
$this->Bs->row() . $this->Bs->col('xs4') . '<b>' . __('Téléphone fixe') . ' :</b> ' . $this->Html->value('User.telfixe') . $this->Bs->close(2) .
$this->Bs->row() . $this->Bs->col('xs4') . '<b>' . __('Téléphone mobile') . ' :</b> ' . $this->Html->value('User.telmobile') . $this->Bs->close(2) .
$this->Bs->row() . $this->Bs->col('xs4') . '<b>' . __('E-mail') . ' :</b> ' . $this->Html->value('User.email') . $this->Bs->close(2) .
$this->Bs->row() . $this->Bs->col('xs4') . '<b>' . __('Profil') . ' :</b> ' . $this->Html->value('Profil.name') . $this->Bs->close(2) .
$this->Bs->row() . $this->Bs->col('xs4') . '<b>' . __('Service(s)') . ' :</b> ';
echo '<ul>';
foreach ($this->Html->value('Service') as $service) {
    echo '<li>'.$service['name'].'</li>';
};
echo '</ul>';
echo  $this->Bs->close(2);
if ($this->permissions->check('changeUserMdp', 'read')) {
    echo $this->Bs->tag('br');
    echo $this->Bs->div('btn-group') .
        $this->Bs->btn(
            $this->Bs->icon('pencil-alt') . ' ' . __('Modifier mon mot de passe'),
            ['controller' => 'preferences', 'action' => 'changeUserMdp'],
            ['type' => 'primary', 'title' => __('Visualiser'), 'escapeTitle' => false]
        ) .
        $this->Bs->close();
    echo $this->Bs->tag('br');
}
echo $this->Bs->close(2);

echo $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
    $this->Html->tag('div', __('Votre configuration graphique'), ['class' => 'panel-heading']) .
$this->Html->tag('div', null, ['class' => 'panel-body']) .
$this->BsForm->select(
    'User.theme',
    $themes,
    [
  'label' => __('Thème'),
  'class' => 'selectone', 'default' => 0,
  'help' => __('Choix du nouveau thème à appliquer')]
);
echo $this->Bs->close(2);
$this->BsForm->setLeft(6);
echo $this->Html2->btnSaveCancel(null, $previous);
echo $this->BsForm->end();
?>


