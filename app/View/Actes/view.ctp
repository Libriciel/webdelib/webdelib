<?php
$this->Html->addCrumb(__('%s n° %s', ucfirst($projet['Typeacte']['name']), $projet['Deliberation']['id']));

// Initialisation des boutons action de la vue
$linkBarre = $this->Bs->div('btn-toolbar', null, ['role' => 'toolbar']);
$linkBarre .= $this->Bs->div('btn-group'); //
$linkBarre .= $this->Html2->btnCancel($previous);

if (!empty($versionsup)) {
    $linkBarre .= $this->Html->link(
        $this->Bs->icon('code-branch') . ' ' . __('Nouvelle version'),
        ['action' => 'view', $versionsup],
        ['escape' => false, 'class' => 'btn btn-danger', 'title' => __('Visualiser la fiche détaillée de la nouvelle version du projet')]
    );
}

if (in_array('generer', $projet['Actions'], true)) {
    $linkBarre .= $this->Bs->btn($this->Bs->icon('cog') . ' ' . __('Générer'), ['controller' => 'projets', 'action' => 'genereFusionToClient', $projet['Deliberation']['id']], ['type' => 'default',
        'escapeTitle' => false,
        'data-waiter-send' => 'XHR',
        'data-waiter-callback' => 'getProgress',
        'data-waiter-type' => 'progress-bar',
        'data-waiter-title' => __('Générer le document du projet : %s', $projet['Deliberation']['id']),
        'class' => 'waiter',
        'title' => __('Générer le document du projet : %s', $projet['Deliberation']['id'])
    ]);
}
if (in_array('telecharger', $projet['Actions'], true)) {
    $linkBarre.=$this->Bs->btn($this->Bs->icon('download') . ' ' . __('Télécharger'), ['controller' => 'actes', 'action' => 'download', $projet['Deliberation']['id']], ['type' => 'default',
        'escape' => false,
        'title' => view . ctp__('Visionner le document PDF du projet ') . $projet['Deliberation']['objet']
    ]);
}

if (in_array('editPostSign', $projet['Actions'], true)) {
    $linkBarre .= $this->Bs->close();
    $linkBarre .= $this->Bs->div('btn-group');
    //Document signé
    $linkBarre .= $this->Bs->btn($this->Bs->icon('file-code') . ' ' . __('Informations suplémentaires'), [
      'controller' => 'actes', 'action' => 'editPostSign', $projet['Deliberation']['id']], [
      'type' => 'primary',
      'escapeTitle' => false,
      'title' => __('Modifier les informations supplémentaires: %s', $projet['Deliberation']['id'])
    ]);
}

if (in_array('duplicate', $projet['Actions'], true)) {
    $linkBarre .= $this->Bs->close();
    $linkBarre .= $this->Bs->div('btn-group');
    //$projet['Deliberation']['etat'] -> 0 on verifiesi le projet en cour de rédaction

    $linkBarre.= $this->Bs->btn($this->Bs->icon('copy') . ' ' . __('Dupliquer'), ['controller' => 'duplicate', 'action' => 'duplicate', $projet['Deliberation']['id']], [
        'type' => 'default',
        //'class' => !$userCanadd ? 'disabled' : '' ,
        'confirm' => __('Confirmez-vous la duplication du projet "%s" ?', $projet['Deliberation']['objet']),
        'escapeTitle' => false,
        'title' => view . ctp__('Dupliquer le projet ') . $projet['Deliberation']['objet'],
    ]);
}
if (in_array('generer_bordereau', $projet['Actions'], true)) {
    $linkBarre .= $this->Bs->btn($this->Bs->icon('file-pdf', null, ['stylePrefix' => 'far']) . ' ' . __('Bordereau d\'activité'), ['controller' => 'actes', 'action' => 'getBordereau', $projet['Deliberation']['id']], ['type' => 'default',
        'escapeTitle' => false,
        'data-waiter-send' => 'XHR',
        'data-waiter-callback' => 'getProgress',
        'data-waiter-type' => 'progress-bar',
        'data-waiter-title' => __('Générer le Bordereau d\'activité du projet : %s', $projet['Deliberation']['id']),
        'class' => 'waiter',
        'title' => __('Générer le Bordereau d\'activité du projet : %s', $projet['Deliberation']['id'])
    ]);
}
$linkBarre .= $this->Bs->close(2);

// affichage  du titre
$title = '<span class="label label-' . ($projet['Deliberation']['etat'] == -1 ? 'danger' : 'default') . '" ' . (!empty($projet['listeSeances'][0]['color']) ? 'style="background-color: ' . $projet['listeSeances'][0]['color'] . '"' : '') . '>' . $projet['Deliberation']['id'] . '</span>';

if (!empty($projet['Multidelib'])) {
    $listeIds = $projet['Deliberation']['id'];
    foreach ($projet['Multidelib'] as $delibRattachee) {
        $listeIds .= ', ' . $delibRattachee['id'];
    }
    $title .= ' ' . __('Projet multi-délibérations n°') . $projet['Deliberation']['id'];
}



if (!empty($projet['User'])) {
    $title.=' ' . $this->Bs->icon(
        'users',
        '',
        [
                'data-toggle' => 'tooltip',
                'data-placement' => 'right',
                'escape' => false,
                'title' => __('Multi-rédacteurs')
            ]
    );
}
echo $this->Bs->div('panel panel-' . ($projet['Deliberation']['etat'] == -1 ? 'danger' : 'default'));
echo $this->Bs->div(
    'panel-heading',
    $this->Bs->row() .
    $this->Bs->col('xs8') .
    $this->Bs->div('media') .
    $this->Bs->div(
        null,
        $this->Bs->btn(
            $this->Bs->icon(
                $this->ProjetUtil->etat_icon($projet['Deliberation']['iconeEtat']['image']),
                [   'lg',
                    'iconType' =>  $this->ProjetUtil->etat_iconType($projet['Deliberation']['iconeEtat']['image'])]
            ),
            null,
            [
        'tag' => 'button',
        'type' => empty($projet['iconeEtat']['status']) ? $this->ProjetUtil->etat_icon_type($projet['Deliberation']['iconeEtat']['image']) : $projet['iconeEtat']['status'],
        'size' => 'lg',
        //'disabled'=>'disabled',
        'data-toggle' => 'popover',
        'data-content' => $this->ProjetUtil->etat_icon_help($projet['Deliberation']['iconeEtat']['image']),
        'data-placement' => 'right',
        'escapeTitle' => false,
        'title' => $projet['Deliberation']['iconeEtat']['titre'],
        'iconType' => $this->ProjetUtil->etat_iconType($projet['Deliberation']['iconeEtat']['image']),

        ]
        ),
        ['class' => 'media-left',]
    ) .
    $this->Bs->div('media-body') .
    $this->Bs->tag('h4', (isset($projet['Deliberation']['objet_delib']) && !empty($projet['Deliberation']['objet_delib']) ? $projet['Deliberation']['objet_delib'] : $projet['Deliberation']['objet']), ['class' => 'media-heading']) .
    $this->Bs->div(null, $title) .
    $this->Bs->close(2) .



//        $this->Bs->tag('p',
//                .' '. $this->Bs->tag('h3', $projet['Deliberation']['objet'], array('class' => 'text-right')), array('class' => 'text-left')) .
//
//        $title . ($projet['Deliberation']['etat']==-1?' '.__('(Versionné)'):'').
//
    $this->Bs->close() .
    $this->Bs->col('xs4') .
    $this->Bs->div('text-right', $this->Bs->btn(isset($projet['Deliberation']['num_delib']) ? ucfirst($projet['Typeacte']['name']) . ' n° ' . $projet['Deliberation']['num_delib']: ucfirst($projet['Typeacte']['name']), null, [
        'tag' => 'button',
        'type' => 'default',
    ])) .
    $this->Bs->close(2)
);

// panels
echo $this->Bs->div('panel-body');
//echo $this->Bs->tabPane($title);
echo $linkBarre;
echo $this->Bs->tag('br /');

// onglets
$aTab = [
    'infos' => __('Informations principales'),
    'circuits' => __('Circuit(s)')
];
if (!empty($commentaires)) {
    $aTab['commentaires'] = __('Commentaire(s)');
}
if (!empty($infosupdefs)) {
    $aTab['Infos_suppl'] = __('Information(s) supplémentaire(s)');
}
if (!empty($historiques)) {
    $aTab['historiques'] = __('Historique(s)');
}
if (!empty($projet['Multidelib'])) {
    $aTab['multidelib'] = __('Délibérations rattachée(s)');
}

echo $this->Bs->tab($aTab, [
        'active' => isset($nameTab) ? $nameTab : 'infos',
        'class' => '-justified']) .
    $this->Bs->tabContent();

// Informations principales
echo $this->Bs->tabPane('infos', ['class' => ((isset($nameTab) && $nameTab == 'infos') || (!isset($nameTab))) ? 'active' : '']);
echo $this->Bs->tag('br');
// ouvre l'élément projetInfo
echo $this->element('projetInfo', ['projet' => $projet]);

// multi-délibs
if (empty($projet['Multidelib']) && !empty($projet['Annexe'])) {
    echo $this->Bs->tag('br');
    $this->BsForm->setLeft(2);
    $this->BsForm->setRight(10);
    echo $this->Bs->row();
    echo $this->Bs->col('xs' . $this->BsForm->getLeft(), null, ['class' => 'text-right']) . $this->Html->tag('label', __('Annexe(s) :')) . $this->Bs->close().
    $this->Bs->col('xs'.$this->BsForm->getRight());
    echo $this->element('annexe_view', array_merge(['ref' => 'delibPrincipale'], ['annexes' => $projet['Annexe']]));
    echo $this->Bs->close(2);
}
echo $this->Bs->tabClose();

// Circuits
echo $this->Bs->tabPane('circuits', ['class' => (isset($nameTab) && $nameTab == 'circuits') ? 'active' : '']);
echo $this->Bs->tag('h4', __('Circuit(s)'));
echo $this->Bs->row();
echo $this->Bs->col('xs4') . '<b>' . __('Circuit') . ' :</b> ' . $projet['Circuit']['libelle'];
echo $this->Bs->close();
echo $this->Bs->col('xs6') . $visu;
echo $this->Bs->close(2);
echo $this->Bs->tabClose();

// Commentaires
if (!empty($commentaires)) {
    echo $this->Bs->tabPane('commentaires', ['class' => (isset($nameTab) && $nameTab == 'commentaires') ? 'active' : '']);
    echo $this->Bs->tag('h4', __('Commentaire(s)'));

    $sLis = '';
    foreach ($commentaires as $commentaire) {
        $sLis.=$this->Html->link(
            $this->Bs->tag('h5', '<span class="label label-info">' . $commentaire['User']['prenom'] . ' ' . $commentaire['User']['nom'] . '</span> '
                . $this->Time->i18nFormat($commentaire['Commentaire']['created'], '%d/%m/%Y à %kh%M'), ['class' => 'list-group-item-heading'])
            . $this->Bs->tag(
                'p',
                nl2br($commentaire['Commentaire']['texte']),
                ['class' => 'list-group-item-text']
            ),
            'javascript:void(0);',
            ['escapeTitle' => false, 'class' => 'list-group-item']
        );
    }
    echo $this->Bs->row();
    echo $this->Bs->col('xs12');
    echo $this->Bs->tag('div', $sLis, ['class' => 'list-group']);
    echo $this->Bs->close(2);
    echo $this->Bs->tabClose();
}

// Informations Supplémentaires
if (!empty($infosupdefs)) {
    echo $this->Bs->tabPane('Infos_suppl', ['class' => (isset($nameTab) && $nameTab == 'Infos_suppl') ? 'active' : '']);
    echo $this->Bs->tag('h4', __('Information(s) Supplémentaire(s)'));
    // ouvre l'élément projetInfoSupp
    echo $this->element('projetInfoSupp', [
        'infosupdefs' => $infosupdefs,
        'infosups' => (!empty($infosups) ? $infosups : null)
    ]);
    echo $this->Bs->tabClose();
}

// Historiques
if (!empty($historiques)) {
    echo $this->Bs->tabPane('historiques', ['class' => (isset($nameTab) && $nameTab == 'historiques') ? 'active' : '']);
    echo $this->Bs->tag('h4', __('Historique(s)'));
    echo $this->Bs->table([
        ['title' => __('Date')],
        ['title' => __('Utilisateur')],
        ['title' => __('Annotation / observation')],
        ['title' => __('Consultation'),'class'=>'text-right'],
    ], ['striped','hover','']);

    foreach ($historiques as $historique) {
        echo $this->Bs->cell($this->Time->i18nFormat($historique['Historique']['created'], '%d/%m/%Y %k:%M:%S'));
        echo $this->Bs->cell($historique['User']['prenom'] . ' ' . $historique['User']['nom']);
        echo $this->Bs->cell(nl2br($historique['Historique']['commentaire']));
        echo $this->Bs->cell($this->Bs->btn(
            $this->Bs->icon('eye', null, ['stylePrefix' => 'far']),
            ['controller'=>'versions',
                'action'=>'index',
                $historique['Historique']['revision_id']],
            [
                'tag' => empty($historique['Historique']['revision_id']) ? 'button' : null,
                'class'=>'pull-right',
                'type' => 'default',
                'disabled' => empty($historique['Historique']['revision_id']),
                'escapeTitle' => false,
                'title' => empty($historique['Historique']['revision_id'])?  __('Pas de révision disponible') : __('Consulter la révision')]
        ));
    }
    echo $this->Bs->endTable();
    echo $this->Bs->tabClose();
}


// Multidelib
if (!empty($projet['Multidelib'])) {
    echo $this->Bs->tabPane('multidelib', ['class' => (isset($nameTab) && $nameTab == 'multidelib') ? 'active' : '']);
    echo $this->element('Deliberations/Multidelib/view', [
        'delib' => $projet['Deliberation'],
        'annexes' => $projet['Annexe'],
        'natureLibelle' => $projet['Typeacte']['name']]);
    foreach ($projet['Multidelib'] as $delibRattachee) {
        echo $this->element('Deliberations/Multidelib/view', [
            'delib' => $delibRattachee,
            'annexes' => $delibRattachee['Annexe'],
            'natureLibelle' => $projet['Typeacte']['name']]);
    }
    echo $this->Bs->tabClose();
}

echo $this->Bs->tabPaneClose();

echo $this->Bs->tag('br /');
echo $this->Bs->tag('hr /');
echo $this->Bs->close(2);
?>
<script type="text/javascript">
    require(['domReady'], function (domReady) {
        domReady(function () {

            <?php if (isset($majDeleg)): ?>

            function afficheMAJ() {
                $("div.nomcourante").parent().append('<?php
                    echo $this->Html->tag('div', $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-repeat']) . __(' Mise à jour'), ['controller' => 'Signatures', 'action' => 'MajEtatParapheur', $projet['Deliberation']['id']], ['escape' => false, 'class' => 'btn btn-inverse']), ['class' => 'majDeleg', 'title' => __('Mettre à jour le statut des étapes de délégations')]);
                    ?>');
            }
            afficheMAJ();
            <?php
            endif;

            if (isset($visas_retard) && !empty($visas_retard)):
            foreach ($visas_retard as $visa):
            ?>
            $('#etape_<?php echo $visa['Visa']['numero_traitement']; ?> .delegation').before('<?php
                echo $this->Html->link(
                $this->Html->tag('i', '', ['class' => 'fa fa-repeat']),
                ['plugin' => 'cakeflow', 'controller' => 'traitements', 'action' => 'traiterDelegationsPassees', $visa['Visa']['traitement_id'], $visa['Visa']['numero_traitement'], 'traiter'],
                ['escape' => false, 'style' => 'text-decoration:none;margin-right:5px;', 'title' => __('Mettre à jour le statut de cette étape')]
            );
                ?>');
            <?php
            endforeach;
            endif;
            ?>
        });
    });
</script>
