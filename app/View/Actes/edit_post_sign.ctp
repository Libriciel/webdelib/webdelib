<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Modification des informations supplémentaires d\'un acte'));


$title =  __('Modifier les informations supplémentaires de l\'acte') . ' : ' . $this->Html->value('Deliberation.id') ;

echo $this->Bs->tag('h3', $title) .
 $this->BsForm->create(false, [
    'url' => [
        'controller' => 'actes',
        'action' => 'editPostSign', $this->Html->value('Deliberation.id')
      ],
    'type' => 'file',
    'novalidate' => true]);

$aTab = ['infos' => __('Informations principales')];

echo $this->element('infosup_edit', ['infosupdefs' => $infosupdefs]);

echo $this->Form->hidden('Deliberation.id');
echo $this->Html2->btnSaveCancel('', $previous);

echo $this->BsForm->end();
