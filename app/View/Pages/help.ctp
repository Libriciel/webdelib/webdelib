<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Aide'));

echo $this->Bs->tag('h3', __('Aide'));

echo $this->Bs->div(
    'well',
    __('Pour les clients Libriciel SCOP, la documentation est disponible en cliquant sur le lien ci-dessous :').
    $this->Bs->tag('br').
    $this->Bs->link(__('Accéder à la documentation'), 'https://www.libriciel.fr/portail-client/', ['target' => '_blank'])
);
