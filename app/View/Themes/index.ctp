<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Thèmes'));

echo $this->Bs->tag('h3', __('Liste des thèmes'));

$this->BsForm->setLeft(0);
$this->BsForm->setRight(6);
echo $this->Bs->div('btn-group btn-group-justified', null, ['role' => "group"]);
echo $this->BsForm->inputGroup('search_tree', [[
        'content' => $this->Bs->icon('filter'),
        'id' => 'search_tree_button',
        'type' => 'button',
        'state' => 'primary',
    ], [
        'content' => $this->Bs->icon('sliders-h') . ' <span class="caret"></span>',
        'class' => 'dropdown-toggle',
        'data-toggle' => 'dropdown',
        'after' => '<ul class="dropdown-menu dropdown-menu-right" role="menu">
            <li><a id="search_tree_erase_button" title="' . __('Remettre la recherche à zéro') . '">' . __('Effacer la recherche') . '</a></li>
            <li class="divider"></li>
            <li><a id="search_tree_plier_button" title="' . __('Replier tous les thèmes') . '">' . __('Tout replier') . '</i></a></li>
            <li><a id="search_tree_deplier_button" title="' . __('Déplier tous les thèmes') . '">' . __('Tout déplier') . '</a></li>
        </ul>',
        'type' => 'button',
        'state' => 'default']
        ], [
    'placeholder' => __('Filtrer par nom de thème'), //style="float: left; z-index: 2"
        ], ['multiple' => true, 'side' => 'right']);
echo $this->Bs->close();
$this->BsForm->setDefault();
echo $this->Bs->tag('br /');
echo $this->Bs->tag('br /');

echo $this->Bs->div('btn-toolbar', null, ['role' => "toolbar"]);
echo $this->Bs->div('btn-group', null, ['role' => "group"]);

if ($this->permissions->check('Themes', 'create')) {
    echo $this->Bs->btn($this->Bs->icon('plus-circle') . ' ' . __('Ajouter un thème'), ['action' => 'add'], [
      'escape' => false,
      'type' => 'primary',
      'id' => 'boutonAdd',
  ]);
}
echo $this->Bs->close();

if ($this->permissions->check('Themes', 'update')) {
    echo $this->Bs->div('btn-group', null, ['role' => "group"]);
    echo $this->Bs->btn($this->Bs->icon('pencil-alt') . ' ' . __('Modifier'), 'javascript:void(0);', ['escape' => false,
    'type' => 'warning',
    'id' => 'boutonEdit']);
    echo $this->Bs->close();
}

if ($this->permissions->check('Themes', 'delete')) {
    echo $this->Bs->div('btn-group', null, ['role' => "group"]);
    echo $this->Bs->btn($this->Bs->icon('trash') . ' ' . __('Supprimer'), 'javascript:void(0);', [
      'escape' => false,
      'type' => 'danger',
      'id' => 'boutonDelete',
      'confirm' => __('Voulez-vous vraiment supprimer le thème ?')
  ]);
}

echo $this->Bs->close(2);
echo $this->Bs->tag('/br');
echo $this->Bs->div(null, $this->Tree->generateIndex($data, 'Theme', ['id' => 'id', 'display' => 'libelle', 'order' => 'order']), ['id' => 'arbre']);
?>
<script>
require(['domReady'], function (domReady) {
    domReady(function () {

        function addAction() {
            window.location.href = $('a#boutonAdd').attr('href');
        }
        function editAction() {
            window.location.href = $('a#boutonEdit').attr('href');
        }
        function deleteAction() {
            window.location.href = $('a#boutonDelete').attr('href');
        }
        var jstreeconf = {
            /* Initialisation de jstree sur la liste des thèmes */
            "core": {//Paramétrage du coeur du plugin
                "multiple": false,
                "themes": {"stripes": true} //Une ligne sur deux est grise (meilleure lisibilité)
            },
            "checkbox": {//Paramétrage du plugin checkbox
                "three_state": false //Ne pas propager la séléction parent/enfants
            },
            "search": {//Paramétrage du plugin de recherche
                "fuzzy": false, //Indicates if the search should be fuzzy or not (should chnd3 match child node 3).
                "show_only_matches": true, //Masque les résultats ne correspondant pas
                "case_sensitive": false //Sensibilité à la casse
            },
            "types": {
                "level0": {
                    "icon": "ls-theme"
                },
                "default": {
                    "icon": "ls-theme"
                }
            },
            "contextmenu": {
                "items": contextMenu
            },
            "plugins": [
                "checkbox", //Affiche les checkboxes
                "wholerow", //Toute la ligne est surlignée
                "search", //Champs de recherche d'élément de la liste (filtre)
                "types", //Pour les icônes
                "contextmenu" //Menu clic droit
            ]
        };

        $('#arbre').jstree(jstreeconf);

        $("a#boutonEdit").hide();
        $("a#boutonDelete").hide();
        $('#arbre').on('changed.jstree', function (e, data) {
            /* Listener onChange qui fait la synchro jsTree/hiddenField */
            $("a#boutonEdit").hide().prop('href', '#');
            $("a#boutonDelete").hide().prop('href', '#');
            if (data.selected.length) {
                var node = data.instance.get_node(data.selected);
                $("a#boutonEdit").show().prop('href', '/<?php echo $this->params['prefix'] ?>/themes/edit/' + data.instance.get_node(data.selected).data.id)
                if ($('#' + node.id).hasClass('jstree-leaf')) { //Ne peux supprimer que les "feuilles"
                    $("a#boutonDelete").show().prop('href', '/<?php echo $this->params['prefix'] ?>/themes/delete/' + data.instance.get_node(data.selected).data.id)
                }
            }
        });

        /* Recherche dans la liste jstree */
        $('#search_tree_button').click(function () {
            $('#arbre').jstree(true).search($('#search_tree').val());
        });

        $('#search_tree_erase_button').click(function () {
            $('#search_tree').val('');
            $('#search_tree_button').click();
        });

        $('#search_tree').keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                $('#search_tree_button').click();
                return false;
            }
        });
        $("#search_tree_plier_button").click(function () {
            $('#arbre').jstree('close_all');
        });
        $("#search_tree_deplier_button").click(function () {
            $('#arbre').jstree('open_all');
        });

        function contextMenu(node) {
            // The default set of all items
            var items = {};

            if($('a#boutonAdd').length){
              items['add'] = {
                  "label": "Ajouter",
                  "action": addAction,
                  "icon": "fa fa-plus-circle",
                  "separator_after": true
              };
            }
            if($('a#boutonEdit').length){
              items['edit'] = {
                  "label": "Modifier",
                  "action": editAction,
                  "icon": "fa fa-pencil"
              };
            }
            if($('a#boutonDelete').length){
              items['delete'] = {
                  "label": "Supprimer",
                  "action": deleteAction,
                  "icon": "fa fa-trash-o"
              };
            }

            if ($('#' + node.id).hasClass("jstree-leaf") == false) {
                delete items.delete;
            }
            return items;
        }
    });
});
</script>
