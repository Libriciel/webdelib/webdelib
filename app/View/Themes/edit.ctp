<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Liste des thèmes'), ['action' => 'index']);
$this->Html->addCrumb(__('Modification d\'un thème'));

echo $this->Bs->tag('h3', __('Modifier le thème : ') . $this->Html->value('Theme.libelle'));

echo $this->BsForm->create('Theme', ['url' => ['prefix' => $this->params['prefix'], 'controller' => 'themes', 'action' => 'edit']]);

echo $this->BsForm->input('Theme.libelle', ['label' => __('Libellé'), 'maxlength' => '500']);
echo $this->BsForm->input('Theme.order', ['label' => __('Critère de tri')]);
echo $this->BsForm->select('Theme.parent_id', $themes, ['label' => __('Appartient à'), 'data-allow-clear' => true, 'data-placeholder' => __('Sélectionner un thème'), 'default' => $selectedTheme, 'class' => 'selectone', 'empty' => true, 'escape' => false]);

echo $this->BsForm->hidden('Theme.id');
echo $this->Html2->btnSaveCancel(null, $previous);
echo $this->BsForm->end();
