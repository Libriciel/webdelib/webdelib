<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Thèmes'), ['action' => 'index']);
$this->Html->addCrumb(__('Ajout d\'un thème'));

echo $this->Bs->tag('h3', __('Ajouter un thème'));


echo $this->BsForm->create('Theme', ['url' => ['prefix' => $this->params['prefix'], 'controller' => 'themes', 'action' => 'add']]);
echo $this->BsForm->input('Theme.libelle', ['label' => __('Libellé') . ' <abbr title="obligatoire">*</abbr>', 'maxlength' => '500']);
echo $this->BsForm->input('Theme.order', ['label' => __('Critère de tri')]);
echo $this->BsForm->select('Theme.parent_id', $themes, ['label' => __('Appartient à'), 'data-placeholder' => __('Choisir un thème'), 'class' => 'selectone', 'empty' => true, 'escape' => false]);
echo $this->Html2->btnSaveCancel(null, $previous);
echo $this->BsForm->end();
