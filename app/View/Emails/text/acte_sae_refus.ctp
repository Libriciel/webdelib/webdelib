Bonjour <?php echo $prenom; ?> <?php echo $nom; ?>,

Un acte que vous avez transmis au SAE a été refusé.

Acte : <?php echo $acte; ?>
Commentaire : <?php echo $sae_commentaire; ?>

Très cordialement.
