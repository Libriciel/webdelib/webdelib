Bonjour <?php echo $prenom; ?> <?php echo $nom; ?>,

Un projet que vous avez créé ou validé a été refusé

Objet : <?php echo $projet_objet; ?>
Séance : <?php echo $seance_deliberante; ?>
Nouvel identifiant : <?php echo $projet_identifiant; ?>
Dernier commentaire : <?php echo $sae_commentaire; ?>

Le projet peut à nouveau être édité à cette adresse : <?php echo $projet_url_modifier; ?>

Très cordialement.
