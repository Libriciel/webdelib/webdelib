Bonjour <?php echo $prenom; ?> <?php echo $nom; ?>,

L'erreur suivante a été identifiée lors de l'envoi d'une seance au SAE.

Séance : <?php echo $seance; ?>
Erreur : <?php echo $sae_error; ?>

Très cordialement.
