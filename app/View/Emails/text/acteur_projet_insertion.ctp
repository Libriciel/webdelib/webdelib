Bonjour <?php echo $civilite; ?> <?php echo $prenom; ?> <?php echo $nom; ?>,

Un projet vient d'être inséré dans un circuit de validation dont vous êtes le rapporteur.

Objet : <?php echo $projet_objet; ?>
Séance : <?php echo $seance_deliberante; ?>
Identifiant : <?php echo $projet_identifiant; ?>

Très cordialement.
