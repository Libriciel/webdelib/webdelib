Bonjour <?php echo $prenom; ?> <?php echo $nom; ?>,

Les erreurs suivantes ont été identifiées lors de l'envoi automatique des actes à télétransmettre:
<?php echo $message; ?>

Très cordialement.
