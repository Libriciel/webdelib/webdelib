Bonjour <?php echo $prenom; ?> <?php echo $nom; ?>,

L'erreur suivante a été identifiée lors de l'envoi d'un acte au SAE.

Acte : <?php echo $acte; ?>
Erreur : <?php echo $sae_error; ?>

Très cordialement.
