Bonjour <?php echo $prenom; ?> <?php echo $nom; ?>,

Un projet dont vous êtes le rapporteur vient d'être validé.

Objet : <?php echo $projet_objet; ?>
Séance : <?php echo $seance_deliberante; ?>

Très cordialement.
