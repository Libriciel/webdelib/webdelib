<p>Bonjour <?php echo $prenom; ?> <?php echo $nom; ?>,</p>
<p>Un acte que vous avez transmis au SAE a été refusé.</p>
<p>
    Acte : <?php echo $acte; ?><br />
    Commentaire : <?php echo $sae_commentaire; ?>
</p>

<p>Très cordialement.</p>
