<p>Bonjour <?php echo $prenom; ?> <?php echo $nom; ?>,</p>
<p>Les erreurs suivantes ont été identifiées lors de l'envoi automatique des actes à télétransmettre:</p>
<p>
<?php echo $message; ?>
</p>
<p>Très cordialement.</p>
