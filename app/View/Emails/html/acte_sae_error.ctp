<p>Bonjour <?php echo $prenom; ?> <?php echo $nom; ?>,</p>
<p>L'erreur suivante a été identifiée lors de l'envoi d'un acte au SAE.</p>
<p>
    Acte : <?php echo $acte; ?><br />
    Erreur : <?php echo $sae_error; ?>
</p>
<p>Très cordialement.</p>
