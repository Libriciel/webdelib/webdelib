<p>Bonjour <?php echo $prenom; ?> <?php echo $nom; ?>,</p>
<p>L'erreur suivante a été identifiée lors de l'envoi d'une seance au SAE.</p>
<p>
    Séance : <?php echo $seance; ?><br />
    Erreur : <?php echo $sae_error; ?>
</p>
<p>Très cordialement.</p>
