<p>Bonjour <?php echo $prenom; ?> <?php echo $nom; ?>,</p>
<p>Un projet dans votre bannette "Mes projets à traiter" est en retard et attend votre validation.</p>
<p>
    Objet : <?php echo $projet_objet; ?><br />
    Séance : <?php echo $seance_deliberante; ?><br />
    Identifiant : <?php echo $projet_identifiant; ?><br />
    Dernier commentaire : <?php echo $projet_dernier_commentaire; ?>
</p>
<p>Vous pouvez le traiter à l'adresse : <?php echo $this->Html->link('Modifier le projet', $projet_url_modifier); ?></p>
<p>Très cordialement.</p>