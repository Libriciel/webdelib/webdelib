<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Test du système'));

echo $this->Bs->tag('h3', __('Test du système'));

?><div class="row">
    <div class="span12">
        <div class="well well-small">
            <p><i class="icon-ok"></i> Versions</p>
            <?php   echo $this->Check->liste($verifVersionWebdelib);
                    echo $this->Check->liste($verifVersionOS);
                    echo $this->Check->liste($verifVersionCakePHP);
                    echo $this->Check->liste($verifVersionPHP);
                    echo $this->Check->liste($verifVersionApache);

                    ?>
        </div>
    </div>
</div>
<br/>

<div class="row">
    <div class="span12">
        <div class="well well-small">
            <p><i class="icon-user"></i> Permissions</p>
            <?php echo $this->Check->liste($verifPermissionsRepertoires); ?>
            <?php echo $this->Check->liste($verifConsoleCakePhp); ?>
        </div>
    </div>
</div>
<br/>


<div class="row">
    <div class="span12">
        <div class="well well-small">
            <p><i class="icon-th-large"></i> Modules apache</p>
            <?php //var_dump($apache_check_modules);
                  echo $this->Check->liste($apache_check_modules); ?>
        </div>
    </div>
</div>
<br/>

<div class="row">
    <div class="span12">
        <div class="well well-small">
            <p><i class="icon-th-list"></i> Extensions PHP</p>
            <?php echo $this->Check->liste($verifExtensionsPhp); ?>
        </div>
    </div>
</div>
<br/>

<div class="row">
    <div class="span12">
        <div class="well well-small">
            <p><i class="icon-code"></i> Librairies PHP</p>
            <?php echo $this->Check->liste($verifLibrairiesPhp); ?>
        </div>
    </div>
</div>
<br/>

<div class="row">
    <div class="span12">
        <div class="well well-small">
            <p><i class="icon-cog"></i> Programmes</p>
            <?php echo $this->Check->liste($verifBinaires); ?>
        </div>
    </div>
</div>
<br/>

<div class="row">
    <div class="span12">
        <div class="well well-small">
            <p><i class="icon-file"></i> Fichiers de configuration </p>
            <?php echo $this->Check->liste($verifPresenceFichiers); ?>
        </div>
    </div>
</div>
<br/>

<div class="row">
    <div class="span12">
        <div class="well well-small">
            <p><i class="icon-tasks"></i> Base de données </p>
            <?php echo $this->Check->liste($configDataBase);
                if($configDataBase[0]['okko']=='ok'){ ?>
                <div class='info'>Paramètres de connexion : </div>
                    <ul><li>host : <?php echo $InfosDataBase['host']; ?></li>
                    <li>login : <?php echo $InfosDataBase['login']; ?></li>
                    <li>database : <?php echo $InfosDataBase['database']; ?></li>
                    <li>schema : <?php echo $InfosDataBase['schema']; ?></li>
                    <li>port : <?php echo $InfosDataBase['port']; ?></li>
                    <li>datasource : <?php echo $InfosDataBase['datasource']; ?></li>
                    </ul>
            <?php echo $this->Check->liste($ConnectionDataBase); ?>
            <h5>Configuration de la base de donnée</h5>
            <?php echo $this->Check->liste($ConnectionDataBaseConfig); ?>
            <h5>Test de version du schéma webdelib</h5>
            <?php echo $this->Check->liste($verifcheckVersionSchema); ?>
<!--            <h5>Test d'intégrité du schéma webdelib</h5>-->
            <?php //echo $this->Check->liste($verifcheckSchema);
                }?>
        </div>
    </div>
</div>


<a name="smtp"> &nbsp;</a> <br/>

<div class="row" id="mail">
    <div class="span12">
        <div class="well well-small"><p><i class="icon-envelope"></i> Mails</p>
            <?php
                    echo $this->Check->liste($configmail);
            ?>
        </div>
    </div>
</div>

<div class="row" id="conversion">
    <div class="span12">
        <div class="well well-small">
            <p><i class="icon-resize-small"></i>Outil de conversion</p>
            <?php
                    echo $this->Check->liste($afficheInfosCloudoo);
                    echo $this->Check->liste($verifConversion);
                    echo $this->Check->liste($verifVariableNecessaireConversion);
                    echo $this->Check->liste($verifTypeConversion);?>
        </div>
    </div>
</div>
<br/>

<div class="row">
    <div class="span12">
        <div class="well well-small">
            <p><i class="icon-edit"></i> Outil de fusion</p>
            <?php
                    echo $this->Check->liste($verifVariableNecessaireFusion);
                    echo $this->Check->liste($veriftesterOdfGedooo);
                    echo $this->Check->liste($testerFusionGedoo);
                    ?>
        </div>
    </div>
</div>

<?php
if (!empty($getClassification)) {
    ?>
    <div class="row" id="s2low">
        <div class="span12">
            <div class="well well-small">
                <p><i class="icon-edit"></i>s²low</p>
                <?php echo $this->Check->liste($getClassification); ?>
            </div>
        </div>
    </div>
    <br/>

<?php
}

if (!empty($getCircuitsParapheur)) {
    ?>
    <div class="row" id="iparapheur">
        <div class="span12">
            <div class="well well-small">
                <i class="icon-edit"></i>
                <p>iparapheur</p>
                <?php echo $this->Check->liste($getCircuitsParapheur); ?>
            </div>
        </div>
    </div>
    <br/>
<?php
}

if (!empty($getPastellVersion)) {
    ?>
    <div class="row" id="pastell">
        <div class="span12">
            <div class="well well-small">
                <p><i class="icon-edit"></i>pastell</p>
               <?php echo $this->Check->liste($getPastellVersion); ?>
            </div>
        </div>
    </div>
    <br/>
<?php
}

if (!empty($checkGED)) {
    ?>
    <div class="row"  id="cmis">
        <div class="span12">
            <div class="well well-small">
                <p><i class="icon-book"></i>Connecteur CMIS GED</p>
                <?php echo $this->Check->liste($checkGED); ?>
            </div>
        </div>
    </div>
    <br/>
<?php
}

if (!empty($checkIdelibre)) {
    ?>
    <div class="row" id="i-delibre">
        <div class="span12">
            <div class="well well-small">
                <p><i class="icon-book"></i>Utilisation de idelibre
                <?php echo $this->Check->liste($checkIdelibre); ?>
                <?php echo $this->Check->liste($checkIdelibreVersion); ?>
                <?php echo $this->Check->liste($checkIdelibreConnexion); ?>
            </div>
        </div>
    </div>
    <br/>
<?php
}
if (!empty($checkLdap)) {
    ?>
    <div class="row">
        <div class="span12">
            <div class="well well-small">
                <p><i class="icon-group"></i>Connecteur LDAP</p>
                <?php echo $this->Check->liste($checkLDAPUser); ?>
                <?php echo $this->Check->liste($checkLdap); ?>
            </div>
        </div>
    </div>
    <br/>
<?php
}
?>
