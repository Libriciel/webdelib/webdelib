<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Utilisateurs'));
$this->Html->addCrumb(__('Liste des utilisateurs'));

echo $this->Bs->tag('h3', __('Liste des utilisateurs') . ' (' . $this->Paginator->counter('{:count}') . ')') ;
if ($this->permissions->check('Users', 'create')) {
    echo $this->Html2->btnAdd(__("Ajouter un utilisateur"), __("Ajouter"));
}
echo $this->element('filtre') .
 $this->Bs->table([['title' => $this->Paginator->sort('username', __('Login'))],
    ['title' => $this->Paginator->sort('nom', __('Nom'))],
    ['title' => $this->Paginator->sort('prenom', __('Prénom'))],
    ['title' => $this->Paginator->sort('Profil.name', __('Profil'))],
    ['title' => __('Service(s)')],
    ['title' => __('Type(s) d\'acte(s)')],
    ['title' => __('Actions'), 'class' => 'text-center'],
        ], ['hover', 'striped']);
foreach ($users as $user) {
    $services = '';
    $user_actif = $user['User']['active'];
    foreach ($user['Service'] as $service) {
        if (is_array($service)) {
            $services.=$service['name'] . $this->Html->tag(null, '<br />');
        }
    }
    if (!$user_actif) {
        echo $this->Bs->lineColor('danger');
    }
    echo $this->Bs->cell($user['User']['username']) .
    $this->Bs->cell($user['User']['nom']) .
    $this->Bs->cell($user['User']['prenom']) .
    $this->Bs->cell($user['Profil']['name']) .
    $this->Bs->cell($services) .
    $this->Bs->cell($this->Html->nestedList($user['Typeacte'])) .
    $this->Bs->cell(
        $this->Bs->div('btn-group') .
            (
                $this->permissions->check('Users', 'read')
                    ? $this->Bs->btn($this->Bs->icon('eye', null, ['stylePrefix' => 'far']), ['controller' => 'users', 'action' => 'view', $user['User']['id']], ['type' => 'default', 'title' => __('Visualiser'), 'escape' => false])
                    : ''
            ) .
            (
                $this->permissions->check('Users', 'update')
                    ? $this->Bs->btn($this->Bs->icon('lock'), ['controller' => 'users', 'action' => 'changeMdp', $user['User']['id']], ['type' => 'default', 'title' => __('Changer le mot de passe'), 'escape' => false]) .
                      $this->Bs->btn($this->Bs->icon(($user_actif) ? 'toggle-on' : 'toggle-off'), [
                          'controller' => 'users',
                          'action' => ($user_actif) ? 'disable' : 'enable',
                          $user['User']['id']], [
                          'type' => ($user_actif) ? 'default' : 'success',
                          'escape' => false,
                          'title' => (($user_actif) ? __('Désactiver') : __('Activer')), __('Êtes-vous sûr de vouloir %s %s ?', (($user_actif) ? __('désactiver') : __('activer')), $user['User']['username'])]) .
                      $this->Bs->btn($this->Bs->icon('ban'), ['controller' => 'users', 'action' => 'acl_edit', $user['User']['id']], ['type' => 'primary', 'title' => __('Gérer les droits'), 'escape' => false]) .
                      $this->Bs->btn($this->Bs->icon('pencil-alt'), ['controller' => 'users', 'action' => 'edit', $user['User']['id']], ['type' => 'primary', 'title' => __('Modifier  l\'utilisateur'), 'escape' => false])
                    : ''
            ) .
            (
                $this->permissions->check('Users', 'delete')
                    ? $this->Bs->btn($this->Bs->icon('trash'), ['controller' => 'users', 'action' => 'delete', $user['User']['id']], ['type' => 'danger', 'escape' => false, 'title' => __('Supprimer l\'utilisateur'), 'class' => !$user['User']['is_deletable'] ? 'disabled' : ''], __('Êtes-vous sûr de vouloir supprimer %s ?', $user['User']['username']))
                    : ''
            ) .
            $this->Bs->close(),
        'text-center'
    );
}
echo $this->Bs->endTable() .
$this->element('paginator', ['paginator' => $this->Paginator]);
