<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Utilisateurs'), ['action' => 'index']);

$this->Html->addCrumb(__('Gestion des droits d\'un utilisateur'));
echo $this->Bs->tag('h3', __('Gérer les droits "utilisateurs" de %s %s ', $this->Html->value('User.nom'), $this->Html->value('User.prenom')));

echo $this->BsForm->create('User', ['url' => ['controller' => 'users', 'action' => 'acl_edit', $this->Html->value('User.id')], 'type' => 'post', 'autocomplete' => 'off']);

$aTab = ['user' => __('Utilisateurs')];
if (!empty($acos_Typeacte)) {
    $aTab['typesactes'] = __('Types d\'actes');
}
if (!empty($acos_Typeacte)) {
    $aTab['typesseances'] = __('Types de seances');
}
if (!empty($acos_Circuit)) {
    $aTab['circuits'] = __('Circuits');
}
if (!empty($acos_Service)) {
    $aTab['services'] = __('Services');
}

echo $this->Bs->tab($aTab, ['active' => isset($nOngletCourant) ? $nOngletCourant : 'user', 'class' => '-justified']) .
 $this->Bs->tabContent();

echo $this->Bs->tabPane('user', ['class' => isset($nOngletCourant) ? $nOngletCourant : 'active']) .
 $this->Html->tag(null, '<br />') .
 $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
 $this->Html->tag('div', __('Table des droits'), ['class' => 'panel-heading']) .
 $this->Html->tag('div', null, ['class' => 'panel-body']);

echo $this->element('AuthManager.permissions', ['model' => 'User']);
echo $this->Bs->close(2);

echo $this->Bs->tabClose();

echo $this->Bs->tabPane('typesactes') .
 $this->Html->tag(null, '<br />') .
    $this->Html->tag('div', __('Cette interface permet de donner des droits aux administrateurs fonctionnels pour la modification de droits des utilisateurs'), ['class' => 'well well-sm']).
    $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
 $this->Html->tag('div', __('Table des droits'), ['class' => 'panel-heading']) .
 $this->Html->tag('div', null, ['class' => 'panel-body']);

echo $this->element('AuthManager.permissions', ['model' => 'Typeacte']);
echo $this->Bs->close(2);
echo $this->Bs->tabClose();

echo $this->Bs->tabPane('typesseances') .
 $this->Html->tag(null, '<br />') .
    $this->Html->tag('div', __('Cette interface permet de donner des droits aux administrateurs fonctionnels pour la modification de droits des utilisateurs'), ['class' => 'well well-sm']).
    $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
 $this->Html->tag('div', __('Table des droits'), ['class' => 'panel-heading']) .
 $this->Html->tag('div', null, ['class' => 'panel-body']);

echo $this->element('AuthManager.permissions', ['model' => 'Typeseance']);
echo $this->Bs->close(2);
echo $this->Bs->tabClose();

echo $this->Bs->tabPane('circuits') .
 $this->Html->tag(null, '<br />') .
 $this->Html->tag('div', __('Cette interface permet de donner des droits aux administrateurs fonctionnels pour la gestion des circuits'), ['class' => 'well well-sm']).
 $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
 $this->Html->tag('div', __('Table des droits'), ['class' => 'panel-heading']) .
 $this->Html->tag('div', null, ['class' => 'panel-body']);

echo $this->element('AuthManager.permissions', ['model' => 'Circuit']);
echo $this->Bs->close(2);
echo $this->Bs->tabClose();

echo $this->Bs->tabPane('services');
echo $this->Html->tag('br');
echo $this->Html->tag('div', __('Cette interface permet de donner des droits aux administrateurs fonctionnels pour la gestion des services et de leur utilisateurs'), ['class' => 'well well-sm']);
echo $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
 $this->Html->tag('div', __('Table des droits'), ['class' => 'panel-heading']) .
 $this->Html->tag('div', null, ['class' => 'panel-body']);
echo $this->element('AuthManager.permissions', ['model' => 'Service']);
echo $this->Bs->close(2);
echo $this->Bs->tabClose();

echo $this->Bs->tabPaneClose();

echo $this->Form->hidden('User.id');

$this->BsForm->setLeft(0);
echo $this->Html2->btnSaveCancel(null, ['action' => 'index']);
echo $this->BsForm->end();

echo $this->Html->scriptBlock("
require(['domReady'], function (domReady) {
    domReady(function () {
    // Checkbox -> masterCheckbox  data-aco
    $('.childCheckboxAuthManager').change(function () {
        $('.tableAco'
                + $(this).attr('data-model') + ' tr[data-aco_id='
                + $(this).attr('data-aco_id') + '] input[type=checkbox]')
                .not(':disabled')
                .prop('checked', $(this).prop('checked'));
        if (($(this).attr('data-model') === 'Service' || $(this).attr('data-model') === 'User') && $(this).is(':checked')) {

            if($(this).attr('id') != 'AcoUser1') {
                findChildren($(this).attr('data-aco_id'));
            }
        }
    });

    function findChildren(aco) {
        $('tr').each(function () {
            if ($(this).attr('data-parent_id') === aco) {
                $(this).find('input[type=checkbox]').prop('checked', true);
                findChildren($(this).attr('data-aco_id'));
            }
        });
    }

    });
});
");
