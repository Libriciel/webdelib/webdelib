<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Utilisateurs'), ['action' => 'index']);
$this->Html->addCrumb(__('%s %s', $user['User']['nom'], $user['User']['prenom']));

echo $this->Bs->tag('h3', __('Utilisateur : %s %s', $user['User']['nom'], $user['User']['prenom']));
?>
<div class="panel panel-default">
    <div class="panel-heading"><?php echo __('Fiche utilisateur : %s %s', $user['User']['nom'], $user['User']['prenom']); ?></div>
    <div class="panel-body">
        <dl>
            <div class="tiers">
                <dt><?php echo __('Login'); ?></dt>
                <dd><?php echo $user['User']['username'] ?></dd>
            </div>
            <div class="tiers">
                <dt><?php echo __('Nom'); ?></dt>
                <dd><?php echo $user['User']['nom'] ?></dd>
            </div>
            <div class="tiers">
                <dt><?php echo __('Prénom'); ?></dt>
                <dd><?php echo $user['User']['prenom'] ?></dd>
            </div>
            <div class="spacer"></div>

            <div class="tiers">
                <dt><?php echo __('Téléphone fixe'); ?></dt>
                <dd><?php echo $user['User']['telfixe'] ?></dd>
            </div>
            <div class="tiers">
                <dt><?php echo __('Téléphone mobile'); ?></dt>
                <dd><?php echo $user['User']['telmobile'] ?></dd>
            </div>
            <div class="tiers">
                <dt><?php echo __('E-mail'); ?></dt>
                <dd><a href="mailto:<?php echo $user['User']['email'] ?>"><?php echo $user['User']['email'] ?></a></dd>
            </div>
            <div class="spacer"></div>

            <div class="tiers">
                <dt><?php echo __('Notification par E-mail'); ?></dt>
                <dd><?php echo $user['User']['accept_notif'] ? 'Oui' : 'Non'; ?></dd>
            </div>
            <div class="tiers">
                <dt><?php echo __('Profil'); ?></dt>
                <dd><?php echo $user['Profil']['name'] ?></dd>
            </div>
            <div class="tiers">
                <dt><?php echo __('Service(s)'); ?></dt>
                <?php
                foreach ($user['Service'] as $service) {
                    echo '<dd>';
                    echo $service['name'] . '<br/>';
                    echo '</dd>';
                };
                ?>
            </div>
            <div class="spacer"></div>

            <div class="tiers">
                <dt><?php echo __('Circuit par défaut'); ?></dt>
                <dd><?php echo $circuitDefautLibelle ?></dd>
            </div>
            <div class="tiers">
                <dt><?php echo __('Note'); ?></dt>
                <dd><?php echo $user['User']['note'] ?></dd>
            </div>
            <div class="spacer"></div>

            <div class="tiers">
                <dt><?php echo __('Date de création'); ?></dt>
                <dd><?php echo $user['User']['created'] ?></dd>
            </div>
            <div class="tiers">
                <dt><?php echo __('Date de modification'); ?></dt>
                <dd><?php echo $user['User']['modified'] ?></dd>
            </div>
            <div class="spacer"></div>

        </dl>

        <br/>
        <?php
        echo $this->Bs->row() .
        $this->Bs->col('md4 of5');
        echo $this->Bs->div('btn-group', null, ['id' => "actions_fiche"]) .
        $this->Html2->btnCancel(),
        $this->Bs->close(6);
        