<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Utilisateurs'), ['action' => 'index']);
$this->Html->addCrumb(__('Changement du mot de passe'));

echo $this->Bs->tag('h3', __('Changer le mot de passe pour %s (%s %s)', $this->Html->value('User.username'), $this->Html->value('User.prenom'), $this->Html->value('User.nom'))) .
    $this->BsForm->create('User', [
        'url' => ['controller' => 'users', 'action' => 'changeMdp', $this->Html->value('User.id')],
        'type' => 'post' , 'autocomplete' => 'off', 'id'=>'appPassword']) .
    $this->BsForm->input('User.password', [
            'id' => 'password',
            'type' => 'password',
            'label' => __('Mot de passe') . ' <acronym title="obligatoire">*</acronym>',
            'value' => '',
            'autocomplete' => 'new-password',
            'data-wd-min-entropie' => $minEntropie,
//     'help' => __('8 caractères minimum.'),
        ]
    ) .
    $this->BsForm->input('User.passwordConfirm', [
        'id' => 'passwordConfirm',
        'type' => 'password',
        'label' => __('Confirmez le mot de passe') . ' <acronym title="obligatoire">*</acronym>',
        'value' => ''
    ]) .
    $this->BsForm->hidden('User.id') .
    $this->BsForm->hidden('User.username') .
    $this->Html2->btnSaveCancel("", $previous, __("Enregistrer le mot de passe"), __("Enregistrer")) .
    $this->BsForm->end();
