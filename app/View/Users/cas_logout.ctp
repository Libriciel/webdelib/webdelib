<?php

echo $this->Html->tag('br');
echo $this->Html->tag('br');
echo $this->Session->flash('auth');
if (!empty($errorMsg)) {
    echo $this->Bs->div('alert alert-danger alert-dismissible', null, ['role' => 'alert']);
    echo $this->Html->tag('button', $this->Html->tag('span', '&times;', ['aria-hidden' => true]) . $this->Html->tag('span', 'Close', ['class' => 'sr-only']), ['type' => 'button', 'class' => 'close', 'data-dismiss' => 'alert']);
    echo $this->Html->tag('strong', __('Erreur !')) . ' ' . $errorMsg;
    echo $this->Bs->close();
}

echo $this->Bs->div('login_form');
echo $this->Html->tag('br');
echo $this->Bs->div('row', $this->Bs->col('md10 of1') . $this->Html->tag('p', $this->Html->image('webdelib_petit.png', ['id' => 'logo', 'alt' => 'Webdelib'])) . $this->Bs->close()
);
echo $this->Bs->div('row', $this->Bs->col('md10 of1') .
        $this->Html->tag('p', $this->Html->tag('strong', __('Déconnexion effectuée avec succès !')) . ' ' . __('Veuillez vous reconnecter ou fermer la page.')) . $this->Bs->close()
);
$this->BsForm->setLeft(4);
$this->BsForm->setRight(7);
echo $this->Html->tag('br');
echo $this->BsForm->create('User', ['action' => 'caslogin', 'role' => 'form', 'type' => 'get', 'class' => 'form-horizontal', 'inputDefaults' => ['label' => false, 'div' => false]]);
echo $this->BsForm->end(['div' => false, 'type' => 'submit', 'class' => 'btn btn-primary', 'label' => __('Reconnexion')]);
echo $this->Bs->close();
