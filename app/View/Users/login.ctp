<ls-lib-login
        logo="/img/logo-webdelib/wd.svg"
        visual-configuration='<?php echo $loginConfig ?? ''; ?>'
        login-username-input-name="data[User][username]"
        login-password-input-name="data[User][password]"
        login-show-error="<?php echo $loginShowError ?? ''; ?>"
        login-show-ask-for-reset-password >
</ls-lib-login>
