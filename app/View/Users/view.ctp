<?php
$this->Html->addCrumb('Utilisateurs');

echo $this->Bs->tag('h3', 'Utilisateurs');
?>
<div class="panel panel-default">
    <div class="panel-heading"><?php echo __('Fiche utilisateur').' : '. $user['User']['nom'].' '.$user['User']['prenom'] ?></div>
    <div class="panel-body">
    <dl>
        <div>
            <dt><?php echo __('Nom'); ?></dt>
            <dd><?php echo $user['User']['nom'] ?></dd>
        </div>
        <div>
            <dt><?php echo __('Prénom'); ?></dt>
            <dd><?php echo $user['User']['prenom'] ?></dd>
        </div>
        <div>
            <dt><?php echo __('Téléphone fixe'); ?></dt>
            <dd><?php echo $user['User']['telfixe'] ?></dd>
        </div>
        <div>
            <dt><?php echo __('Téléphone mobile'); ?></dt>
            <dd><?php echo $user['User']['telmobile'] ?></dd>
        </div>
        <div>
            <dt><?php echo __('E-mail'); ?></dt>
            <dd><a href="mailto:<?php echo $user['User']['email'] ?>"><?php echo $user['User']['email'] ?></a></dd>
        </div>
        <div>
            <dt><?php echo __('Profil'); ?></dt>
            <dd><?php echo $user['Profil']['name'] ?></dd>
        </div>
        <div>
            <dt><?php echo __('Service(s)'); ?></dt>
            <?php
            foreach ($user['Service'] as $service) {
                echo '<dd>';
                echo $service['name'] . '<br/>';
                echo '</dd>';
            };
            ?>
        </div>
    </dl></ul>
</div></div>
    <br/>
 <?php
echo $this->Html2->btnCancel($previous);
