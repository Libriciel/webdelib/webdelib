<?php
    $this->Html->addCrumb(__('Administration'));
    $this->Html->addCrumb(
            __('Utilisateurs'),
            ['action' => 'index']
    );

    if ($this->Html->value('User.id')) {
        $this->Html->addCrumb(__('Modification d\'un utilisateur'));
        echo $this->Bs->tag(
                'h3',
                __('Modifier l\'utilisateur : %s %s',
                    $this->Html->value('User.prenom'),
                    $this->Html->value('User.nom')),
                ['id' => 'appUser']
        );
        echo $this->BsForm->create('User', ['url' => ['controller' => 'users', 'action' => 'edit', $this->Html->value('User.id')], 'type' => 'post', 'name' => 'userEdit', 'id' => 'userForm']);
    } else {
        echo $this->Bs->tag('h3', __('Ajouter un utilisateur'), ['id' => 'appUser']);
        $this->Html->addCrumb(__('Ajouter un utilisateur'));
        echo $this->BsForm->create('User', ['url' => ['controller' => 'users', 'action' => 'add'], 'type' => 'post', 'id' => 'userForm']);
    }

    // tabs
    echo $this->Bs->tab([
            'infos' => __('Informations principales'),
            'typesactes' => __('Types d\'actes'),
            'typesseances' => __('Types de seances'),
            'circuit_services' => __('Circuit & Services')
        ], ['active' => isset($nOngletCourant) ? $nOngletCourant : 'infos', 'class' => '-justified']) .
        $this->Bs->tabContent();

    // infos principales
    echo $this->Bs->tabPane('infos', ['class' => isset($nOngletCourant) ? $nOngletCourant : 'active']) .
        $this->Html->tag(null, '<br />') .
        $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
        $this->Html->tag('div', __('Identifiant de connexion'), ['class' => 'panel-heading']) .
        $this->Html->tag('div', null, ['class' => 'panel-body', 'id'=>'appPassword']) .
        $this->BsForm->input('User.username', [
            'label' => __('Login') . ' <abbr title="obligatoire">*</abbr>', 'required']
        );
    if (!$this->Html->value('User.id')) {
        echo $this->BsForm->input('User.password', [
            'type' => 'password',
            'id' => 'password',
            'label' => __('Mot de passe') . ' <abbr title="obligatoire">*</abbr>',
            'data-wd-min-entropie' => $minEntropie,
//            'help' => __('8 caractères minimum  ')
        ]);
        echo $this->BsForm->input('User.passwordConfirm', [
            'type' => 'password',
            'label' => __('Confirmez le mot de passe') . ' <abbr title="obligatoire">*</abbr>']);
    }
    echo $this->Bs->close(2) . $this->Html->tag(null, '<br />');

    // Identité et contacts
    echo $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
        $this->Html->tag('div', __('Identité et contacts'), ['class' => 'panel-heading']) .
        $this->Html->tag('div', null, ['class' => 'panel-body']) .
        $this->Bs->row() .
        $this->Bs->col('lg6') .
        $this->BsForm->input('User.nom', [
            'label' => __('Nom') . ' <abbr title="obligatoire">*</abbr>',
            'required',
            'autocomplete' => 'off'
        ]) .
        $this->BsForm->input('User.prenom', [
            'label' => __('Prénom') . ' <abbr title="obligatoire">*</abbr>',
            'required',
            'autocomplete' => 'off']) .
        $this->BsForm->input('User.telfixe', ['label' => __('Tél. fixe'),
            'autocomplete' => 'off'
        ]) .
        $this->BsForm->input('User.telmobile', ['label' => __('Tél. mobile'),
            'autocomplete' => 'off'
        ]) .
        $this->Bs->close() .
        $this->Bs->col('lg6') .
        $this->BsForm->input('User.email', ['label' => __('E-mail'), 'type' => 'email']) .
        $this->BsForm->input('User.note', ['type' => 'textarea', 'cols' => '30', 'rows' => '5']) .
        $this->Bs->close(4) . $this->Html->tag(null, '<br />');

    // Profil
    echo $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
        $this->Html->tag('div', __('Profil'), ['class' => 'panel-heading']) .
        $this->Html->tag('div', null, ['class' => 'panel-body']) .
        $this->BsForm->input('User.profil_id', [
            'label' => ['text' => __('Profil utilisateur') . ' <abbr title="obligatoire">*</abbr>', 'class' => 'label_autocomplete'],
            'options' => $profils,
            'empty' => false,
            'class' => 'autocomplete'
        ]) .
        $this->Bs->close(2) .
        $this->Bs->tabClose();

    // Types d'actes
    echo $this->Bs->tabPane('typesactes') .
        $this->Html->tag(null, '<br />') .
        $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
        $this->Html->tag('div', __('Table des droits'), ['class' => 'panel-heading']) .
        $this->Html->tag('div', null, ['class' => 'panel-body']);

    echo $this->element('AuthManager.permissions', ['model' => 'Typeacte']);
    echo $this->Bs->close(2);
    echo $this->Bs->tabClose();

    // Types d'actes
    echo $this->Bs->tabPane('typesseances') .
        $this->Html->tag(null, '<br />') .
        $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
        $this->Html->tag('div', __('Table des droits'), ['class' => 'panel-heading']) .
        $this->Html->tag('div', null, ['class' => 'panel-body']);

    echo $this->element('AuthManager.permissions', ['model' => 'Typeseance']);
    echo $this->Bs->close(2);
    echo $this->Bs->tabClose();
$this->BsForm->setDefault();
    // Circuits & Services
    echo $this->Bs->tabPane('circuit_services') .
        $this->Html->tag(null, '<br />') .
        $this->Bs->row() .
        $this->Bs->col('lg6') .
        $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
        $this->Html->tag('div', __('Circuits de validation'), ['class' => 'panel-heading']) .
        $this->Html->tag('div', null, ['class' => 'panel-body']) .
        $this->BsForm->select('Circuit.Circuit', $circuits, [
            'id' => 'all_circuits',
            'label' => __('Circuits visibles par l\'utilisateur'),
            'multiple' => true,
            'autocomplete' => 'off',
            'data-placeholder' => __('Sélectionner un ou des circuits visibles'),
            'class' => 'selectmultiple',
            'style' => 'width:100%',
        ]) .
        $this->BsForm->select('User.circuit_defaut_id', [], [
            'id' => 'default_circuit',
            'label' => __('Circuit par défaut'),
            'autocomplete' => 'off',
            'data-placeholder' => __('Sélectionner un circuit par défaut'),
            'empty' => true,
            'class' => 'selectone',
            'style' => 'width:100%',
        ]) .
        $this->Bs->close(3);

    echo $this->Bs->col('lg6') .
        $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
        $this->Html->tag('div', 'Services', ['class' => 'panel-heading']) .
        $this->Html->tag('div', null, ['class' => 'panel-body']);


    echo $this->Bs->div('btn-group', null, ['role' => "group"]);
    $this->BsForm->setLeft(0);
    $this->BsForm->setRight(12);
    echo $this->BsForm->inputGroup(
        'search_service',
        [
            [
                'content' => $this->Bs->icon('filter'),
                'id' => 'search_service_button',
                'title' => __('Rechercher un service'),
                'type' => 'button',
                'state' => 'primary',
            ],
            [
                'content' => $this->Bs->icon('sliders-h') . ' <span class="caret"></span>',
                'class' => 'dropdown-toggle',
                'title' => __('Option de recherche'),
                'data-toggle' => 'dropdown',
                'after' => '<ul class="dropdown-menu dropdown-menu-right" role="menu">
                <li><a id="search_service_erase_button" title="' . __('Remettre la recherche à zéro') . '">' . __('Effacer la recherche') . '</a></li>
                <li class="divider"></li>
                <li><a id="search_service_plier_button" title="' . __('Replier tous les services') . '">' . __('Tout replier') . '</i></a></li>
                <li><a id="search_service_deplier_button" title="' . __('Déplier tous les services') . '">' . __('Tout déplier') . '</a></li>
            </ul>',
                'type' => 'button',
                'state' => 'default']
        ],
        [
            'class' => 'form-control',
            'id' => 'search_service',
            'div' => false,
            'placeholder' => __('Filtrer par nom de service'),
        ],
        [
            'multiple' => true,
            'side' => 'right']
    );
    echo $this->Bs->close(1);

    $selectedServices = !empty($selectedServices) ? $selectedServices : [];
    echo $this->Bs->div(null, $this->Tree->generateIndex(
        $services,
        'Service',
        /* array('id' => 'id', 'display' => 'libelle', 'order' => 'order') */ $selectedServices
    ), ['id' => 'services']);

    //echo $this->Tree->generateList($services, 'Service', $selectedServices);
    $options = [];
    if (!empty($selectedServices)) {
        $options['value'] = implode(',', $selectedServices);
    } elseif ($this->Html->value('Service.Service')) {
        $options['value'] = implode(',', $this->Html->value('Service.Service'));
    }
    echo $this->BsForm->hidden('Service.Service', $options)
        . $this->BsForm->error('User.Service', __('Sélectionnez un ou plusieurs services')) .
        $this->Bs->close(4) .
        $this->Bs->tabClose();

    $this->Bs->tabPaneClose();
    //echo $this->Bs->tabPane('configuration_synthese');.
    //echo $this->Bs->tabClose();
    echo $this->Form->hidden('User.id');

    $this->BsForm->setDefault();
    echo $this->Html2->btnSaveCancel(null, ['action' => 'index']);
    echo $this->BsForm->end();
?>

<script>
    require(['domReady'], function (domReady) {
        domReady(function () {
            $("#default_circuit").val('<?php echo $this->Html->value('User.circuit_defaut_id') ?>').trigger("change");
        });
    });
</script>
