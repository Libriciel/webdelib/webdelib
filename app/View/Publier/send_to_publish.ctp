<?php

$nbProjets = isset($this->Paginator) ? $this->Paginator->counter('{:count}') : $nbProjets;
$titre = __('Publication') . ' (' . $nbProjets . ' ' . ($nbProjets > 1 ? 'projets' : 'projet') . ')';

if($this->action=='sendToPublish'){
    $this->Html->addCrumb(__('Tous les projets'));
    $this->Html->addCrumb(__('Autres actes'));
} else {
    $this->Html->addCrumb(__('Post-séances'));
}
$this->Html->addCrumb(__('Publication'));

echo $this->element('filtre');

echo $this->Bs->tag('h3', $titre);

$titles = [];

echo $this->BsForm->create('Deliberation', [
    'url' => ['controller' => 'publier', 'action' => 'publish'],
    'class' => 'waiter form-inline',
    'data-waiter-title'=>__('Publication en cours'),
]);

$titles[] = ['title' => $this->BsForm->checkbox(null, [
    'id' => 'masterCheckbox',
    'label' => '',
    'inline' => true
])];
$titles[] = ['title' => 'Id.'];
$titles[] = ['title' =>  $this->Paginator->sort('num_delib', __('Numéro'))];
$titles[] = ['title' => $this->Paginator->sort('objet_delib', __('Libellé'))];
$titles[] = ['title' => __('Date de publication')];
$titles[] = ['title' => __('Publié')];
$titles[] = ['title' => __('Actions')];

echo $this->Bs->table($titles, ['hover', 'striped']);

foreach ($deliberations as $delib) {

    $options = [];
    if (!$delib['Deliberation']['publier_etat']) {
        $options['checked'] = true;
    } else {
        $options['disabled'] = true;
    }
    echo $this->Bs->cell($this->Form->checkbox('Deliberation.' . $delib['Deliberation']['id'] . '.is_checked', $options));


    echo $this->Bs->cell($this->Html->link($delib['Deliberation']['id'], [
        'controller' => 'projets',
        'action' => 'view', $delib['Deliberation']['id']]
    ));
    echo $this->Bs->cell($this->Html->link($delib['Deliberation']['num_delib'], [
        'controller' => 'publier',
        'action' => 'download', $delib['Deliberation']['id']]
    ));
    echo $this->Bs->cell($delib['Deliberation']['objet_delib']);
    echo $this->Bs->cell($delib['Deliberation']['publier_date']?CakeTime::i18nFormat(strtotime($delib['Deliberation']['publier_date']), '%d/%m/%Y'):'-');
    echo $this->Bs->cell(
        empty($delib['Deliberation']['publier_etat'])?
            'En attente de publication':
            'Publié'.($delib['Deliberation']['publier_etat']===2?' manuellement':'')
    );

    echo $this->Bs->cell(
        $this->Bs->btn($this->Bs->icon('download'), [
        'controller' => 'publier',
            'action' => 'download', $delib['Deliberation']['id']], ['type' => 'default',
            'escapeTitle' => false,
            'title' => __('Télécharger le document:') . ' ' . $delib['Deliberation']['objet_delib']
        ])
    );
}
echo $this->Bs->endTable();

$this->BsForm->setLeft(0);
$this->BsForm->setRight(12);
$actions_possibles = [];
if(Configure::read('USE_PUBLICATION')){
    $actions_possibles['publier'] = __('Publier');
}
$actions_possibles['publier_manuellement'] = __('Déclarer une publication manuelle');

echo $this->element('Projet/publier', [
        'traitement_lot' => true ,
        'actions_possibles'=> $actions_possibles
    ]).

    $this->Bs->close();

echo $this->BsForm->end();

echo $this->Html->tag('br');

//paginate
echo $this->element('paginator', ['paginator' => $this->Paginator]);
