<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Utilisateurs'));
$this->Html->addCrumb(__('Profils'), ['action' => 'index']);

$this->Html->addCrumb(__('Ajout d\'un profil'));

echo $this->Bs->tag('h3', __('Ajouter un profil'));

echo $this->BsForm->create('Profil', ['url'=>['controller' => 'profils', 'action' => 'add'], 'type' => 'post']);
echo $this->BsForm->input('Profil.name', ['label' => __('Libellé') . ' <abbr title="obligatoire">(*)</abbr>']);
//echo $this->BsForm->select('Profil.parent_id', $profils, array(
//    'placeholder'=> __('Choisir un profil'),
//    'label' => __('Appartient à'),
//    'class' => 'selectone', 'empty' => true)); 

echo $this->BsForm->select('Profil.role_id', $roles, ['data-placeholder' => __('Choisir un type'), 'label' => __('Choisir un type'). ' <abbr title="obligatoire">(*)</abbr>', 'autocomplet' => 'off', 'class' => 'selectone', 'empty' => true]). ($this->Form->isFieldError('Profil.role_id') ?
        $this->Form->error('Profil.role_id') . $this->Bs->tag('br') : '');

if (Configure::read('AuthManager.Ldap.use')) {
    echo $this->BsForm->select('Profil.ldap_name', $profils, ['data-placeholder' => __('Choisir un groupe LDAP'), 'label' => __('Groupe LDAP'), 'autocomplet' => 'off', 'class' => 'selectone', 'empty' => true]);
}

echo $this->Html2->btnSaveCancel(null, $previous);
echo $this->BsForm->end();
