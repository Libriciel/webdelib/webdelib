<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Liste des profils'), ['action'=>'index']);

$this->Html->addCrumb(__('Envoi d\'un email'));

echo $this->Bs->tag('h3', __('Envoyer un email aux utilisateurs du profil : %s', $libelle_profil));

echo $this->BsForm->create(
    'Profil',
    [
    'url' => [
      'action' => 'notifier',
      $id
    ],
    'type' => 'post',
    'data-waiter-send' => 'XHR',
    'data-waiter-callback' => 'getProgress',
    'data-waiter-type' => 'progress-bar',
    'data-waiter-title' => __('Envoi de la notification aux utilisateurs'),
    'class' => 'waiter form-horizontal',
    ]
);

echo $this->BsForm->input('Profil.content', [
    'label' => __('Message à envoyer'),
    'class' => 'profil_content',
    'type' => 'textarea']);

echo $this->TinyMCE->load('profil_content');
echo $this->Bs->tag('br');
echo $this->Bs->tag('br');
echo $this->Bs->div('btn-group col-md-offset-' . $this->BsForm->getLeft(), null);
echo $this->Html2->btnCancelSend($previous).
$this->Bs->btn(
    $this->Bs->icon('paper-plane') . ' ' .__("Envoyer"),
    null,
    [
            'tag'=>'button',
            'type' => 'primary',
            'option_type'=> 'submit',
            'escapeTitle' => false,
            'title' => __('Envoyer le message aux utilisateurs de ce profil'),
            'confirm'=> __('Voulez-vous vraiment envoyer un mail vers les utilisateurs du profil ?')
        ]
)
.$this->Bs->close();

echo $this->BsForm->end();
