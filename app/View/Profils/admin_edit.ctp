<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Utilisateurs'));
$this->Html->addCrumb(__('Profils'), ['action' => 'index']);

$this->Html->addCrumb(__('Modification d\'un profil'));

echo $this->Bs->tag('h3', __('Modifier le profil : ') . $this->Html->value('Profil.name'));

echo $this->BsForm->create('Profil', ['url'=> ['controller' => 'profils', 'action' => 'edit'], 'type' => 'post']);

$aTab = ['infos' => __('Informations principales'), 'droits' => __('Droits')];

echo $this->Bs->tab($aTab, ['active' => 'infos', 'class' => '-justified']) .
 $this->Bs->tabContent();

echo $this->Bs->tabPane('infos', ['class' => isset($nameTab) ? $nameTab : 'active']);
echo $this->Html->tag('br /');
echo $this->BsForm->input('Profil.name', ['label' => ['text' => __('Libellé'), 'style' => 'padding-top:5px;']]);

//    echo $this->BsForm->select('Profil.parent_id', $profils, array(
//    'placeholder'=> __('Choisir un profil'),
//    'label' => __('Appartient à'),
//    'autocomplet'=>'off',
//    'class' => 'selectone', 'empty' => true));

echo $this->BsForm->select('Profil.role_id', $roles, ['label' => __('Choisir un type'), 'autocomplet' => 'off', 'class' => 'selectone']);

if (Configure::read('LdapManager.Ldap.use')) {
    echo $this->element('LdapManager.groups', ['model' => 'Profil']);
}
echo $this->Bs->tabClose();

echo $this->Bs->tabPane('droits');

if ($this->Html->value('Profil.id')) {
    //echo $this->element('LdapManager.permissions', array('model' => 'Typeacte'));
    echo $this->element('AuthManager.permissions', ['model' => 'Profil']);
    //echo $this->element('LdapManager.permissions', array('model' => 'User'));
} else {
    echo $this->Html->para(null, __('Sauvegardez puis éditez à nouveau l\'utilisateur pour modifier ses droits.', true));
    echo $this->Html->para(null, __('Les nouveaux utilisateurs héritent des droits des profils auxquels ils sont rattachés.', true));
}

echo $this->Bs->tabClose();
echo $this->Bs->tabPaneClose();
echo $this->BsForm->setLeft(3);
echo $this->BsForm->hidden('Profil.id');
echo $this->Html2->btnSaveCancel(null, $previous);
echo $this->BsForm->end();

echo $this->Html->scriptBlock("
    require(['domReady'], function (domReady) {
    domReady(function () {
    // Checkbox -> masterCheckbox  data-aco
    $('.childCheckboxAuthManager').change(function () {
        $('.tableAco'
                + $(this).attr('data-model') + ' tr[data-aco_id='
                + $(this).attr('data-aco_id') + '] input[type=checkbox]')
                .not(':disabled')
                .prop('checked', $(this).prop('checked'));
        if (($(this).attr('data-model') === 'Profil') && $(this).is(':checked')) {

            if($(this).attr('id') != 'AcoProfil1') {
                findChildren($(this).attr('data-aco_id'));
            }
        }
    });

    function findChildren(aco) {
        $('tr').each(function () {
            if ($(this).attr('data-parent_id') === aco) {
                $(this).find('input[type=checkbox]').prop('checked', true);
                findChildren($(this).attr('data-aco_id'));
            }
        });
    }
    });
});
");
