<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Utilisateurs'));
$this->Html->addCrumb(__('Liste des profils'));

echo $this->Bs->tag('h3', __('Liste des profils'));

$this->BsForm->setLeft(0);
$this->BsForm->setRight(6);
echo $this->Bs->div('btn-group btn-group-justified', null, ['role' => "group"]);
// search tree
echo $this->BsForm->inputGroup('search_tree', [[
        'content' => $this->Bs->icon('filter'),
        'id' => 'search_tree_button',
        'type' => 'button',
        'state' => 'primary',
    ], [
        'content' => $this->Bs->icon('sliders-h') . ' <span class="caret"></span>',
        'class' => 'dropdown-toggle',
        'data-toggle' => 'dropdown',
        'after' => '<ul class="dropdown-menu dropdown-menu-right" role="menu">
            <li><a id="search_tree_erase_button" title="' . __('Remettre à zéro la recherche') . '">' . __('Effacer la recherche') . '</a></li>
        </ul>',
        'type' => 'button',
        'state' => 'default']
        ], [
    'placeholder' => __('Filtrer par nom de profil'), //style="float: left; z-index: 2"
        ], ['multiple' => true, 'side' => 'right']);
echo $this->Bs->close();
echo $this->Bs->tag('br /');
echo $this->Bs->tag('br /');

echo $this->Bs->div('btn-toolbar', null, ['role' => "toolbar"]);
// nouveau
if ($this->permissions->check('Profils', 'create')) {
    echo $this->Bs->div('btn-group', null, ['role' => "group"]);
    echo $this->Bs->btn($this->Bs->icon('plus-circle') . ' ' . __('Ajouter un profil'), ['action' => 'add'], [
        'escapeTitle' => false,
        'type' => 'primary',
        'id' => 'boutonAdd',
        'onclick' => 'addAction();',
    ]);
    echo $this->Bs->close();
}
// modifier
if ($this->permissions->check('Profils', 'update')) {
    echo $this->Bs->div('btn-group', null, ['role' => "group"]);
    echo $this->Bs->btn($this->Bs->icon('pencil-alt') . ' ' . __('Modifier'), ['admin' => true, 'prefix' => 'admin', 'controller' => 'profils', 'action' => 'edit'], ['escapeTitle' => false,
        'type' => 'warning',
        'onclick' => 'editAction();',
        'id' => 'boutonEdit']);
    echo $this->Bs->close();
}
// supprimer
if ($this->permissions->check('Profils', 'delete')) {
    echo $this->Bs->div('btn-group', null, ['role' => "group"]);
    echo $this->Bs->btn($this->Bs->icon('trash') . ' ' . __('Supprimer'), ['admin' => true, 'prefix' => 'admin', 'controller' => 'profils', 'action' => 'delete'], [
        'escapeTitle' => false,
        'type' => 'danger',
        'id' => 'boutonDelete',
        'onclick' => 'deleteAction();',
        'confirm' => __('Voulez-vous vraiment supprimer le profil ?')
    ]);
    echo $this->Bs->close();
}
// recharger les droits
if ($this->permissions->check('Profils', 'update')) {
    echo $this->Bs->div('btn-group', null, ['role' => "group"]);
    echo $this->Bs->btn($this->Bs->icon('sync') . ' ' . __('Actualiser les droits'), [
        'admin' => true,
        'prefix' => 'admin',
        'controller' => 'profils',
        'action' => 'updateDroits'], [

        'escapeTitle' => false,
        'data-waiter-send' => 'XHR',
        'data-waiter-callback' => 'getProgress',
        'data-waiter-type' => 'progress-bar',
        'data-waiter-title' => __('Actualiser les droits'),
        'class' => 'waiter',
        'type' => 'success',
        'id' => 'boutonUpdate',
        //'onclick' => 'updateAction();',
    ]);
    echo $this->Bs->close();
}
// envoyer un mail
if ($this->permissions->check('Profils', 'read')) {
    echo $this->Bs->div('btn-group', null, ['role' => "group"]);
    echo $this->Bs->btn($this->Bs->icon('paper-plane') . ' ' . __('Envoyer un email'), ['admin' => true, 'prefix' => 'admin', 'controller' => 'profils', 'action' => 'notifier'], [
        'escapeTitle' => false,
        'type' => 'primary',
        'id' => 'boutonMail',
        'onclick' => 'mailAction();',
    ]);
    echo $this->Bs->close();
}
echo $this->Bs->close();

// tree
echo $this->Bs->tag('/br');
echo $this->Bs->div(null, $this->Tree->generateIndex($data, 'Profil', [
            'id' => 'id',
            'display' => 'name',
                /* 'order' => 'order' */
        ]), ['id' => 'arbre']);
?>
<script>
require(['domReady'], function (domReady) {
    domReady(function () {
        var jstreeconf = {
            /* Initialisation de jstree sur la liste des profils */
            "core": {//Paramétrage du coeur du plugin
                "multiple": false,
                "themes": {"stripes": true} //Une ligne sur deux est grise (meilleure lisibilité)
            },
            "checkbox": {//Paramétrage du plugin checkbox
                "three_state": false //Ne pas propager la séléction parent/enfants
            },
            "search": {//Paramétrage du plugin de recherche
                "fuzzy": false, //Indicates if the search should be fuzzy or not (should chnd3 match child node 3).
                "show_only_matches": true, //Masque les résultats ne correspondant pas
                "case_sensitive": false     //Sensibilité à la casse
            },
            "types": {
                "level0": {
                    "icon": "fa fa-sitemap"
                },
                "default": {
                    "icon": "fa fa-users"
                }
            },
            "contextmenu": {
                "items": contextMenu
            },
            "plugins": [
                "checkbox", //Affiche les checkboxes
                "wholerow", //Toute la ligne est surlignée
                "search", //Champs de recherche d'élément de la liste (filtre)
                "types",
                "contextmenu"
            ]
        };

        $('#arbre').jstree(jstreeconf);

        $("a#boutonEdit").hide();
        $("a#boutonDelete").hide();
        $("a#boutonUpdate").hide();
        $("a#boutonMail").hide();

        $('#arbre').on('changed.jstree', function (e, data) {
            /* Listener onChange qui fait la synchro jsTree/hiddenField */
            $("a#boutonEdit").hide().attr('data-link', '<?php echo Router::url(['admin' => true, 'prefix' => 'admin', 'controller' => 'profils', 'action' => 'edit']); ?>');
            $("a#boutonDelete").hide().attr('data-link', '<?php echo Router::url(['admin' => true, 'prefix' => 'admin', 'controller' => 'profils', 'action' => 'delete']); ?>');
            $("a#boutonUpdate").hide().attr('data-link', '<?php echo Router::url(['admin' => true, 'prefix' => 'admin', 'controller' => 'profils', 'action' => 'updateDroits']); ?>');
            $("a#boutonMail").hide().attr('data-link', '<?php echo Router::url(['admin' => true, 'prefix' => 'admin', 'controller' => 'profils', 'action' => 'notifier']); ?>');

            if (data.selected.length) {
                var node = data.instance.get_node(data.selected);
                $("a#boutonEdit").show().prop('href', $("a#boutonEdit").attr('data-link') + '/' + node.data.id);
                $("a#boutonMail").show().prop('href', $("a#boutonMail").attr('data-link') + '/' + node.data.id);
                $("a#boutonUpdate").show().prop('href', $("a#boutonUpdate").attr('data-link') + '/' + node.data.id);
                if ($('#' + node.id).hasClass('deletable')) {
                    $("a#boutonDelete").show().prop('href', $("a#boutonDelete").attr('data-link') + '/' + node.data.id);
                }
            }
        });

        /* Recherche dans la liste jstree */
        $('#search_tree_button').click(function () {
            $('#arbre').jstree(true).search($('#search_tree').val());
        });

        $('#search_tree_erase_button').click(function () {
            $('#search_tree').val('');
            $('#search_tree_button').click();
        });

        $('#search_tree').keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                $('#search_tree_button').click();
                return false;
            }
        });
        $("#search_tree_plier_button").click(function () {
            $('#arbre').jstree('close_all');
        });
        $("#search_tree_deplier_button").click(function () {
            $('#arbre').jstree('open_all');
        });

        function addAction() {
            window.location.href = $('a#boutonAdd').attr("href");
        }
        function editAction() {
            window.location.href = $('a#boutonEdit').attr("href");
        }
        function deleteAction() {
            window.location.href = $('a#boutonDelete').attr("href");
        }
        function updateAction() {
            $('a#boutonUpdate').click();
        }
        function mailAction() {
            window.location.href = $('a#boutonMail').attr("href");
        }

        function contextMenu(node) {
            // The default set of all items
            var items = {
                "add": {
                    "label": '<?php echo __("Nouveau"); ?>',
                    "action": addAction,
                    "icon": "fa fa-plus",
                    "separator_after": true
                },
                "edit": {
                    "label": '<?php echo __("Modifier"); ?>',
                    "action": editAction,
                    "icon": "fa fa-edit"
                },
                "delete": {
                    "label": '<?php echo __("Supprimer"); ?>',
                    "action": deleteAction,
                    "icon": "fa fa-trash-o"
                },
                "update": {
                    "separator_before": true,
                    "label": '<?php echo __("Recharger les droits"); ?>',
                    "action": updateAction,
                    "icon": "fa fa-retweet"
                },
                "mail": {
                    "separator_before": true,
                    "label": '<?php echo __("Envoyer un mail"); ?>',
                    "action": mailAction,
                    "icon": "fa fa-envelope"
                }
            };
            if ($('#' + node.id).hasClass("deletable") == false) {
                delete items.delete;
            }
            return items;
        }
    });
});
</script>
