<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

echo $this->Bs->row() .
  $this->Bs->col('xs12');
 echo $this->Bs->div('text-info', __('Vérifier ci-dessous les informations de classification et de typologie des pièces'));
 echo $this->Bs->close(2);
  echo $this->Bs->div('spacer', '&nbsp;');
 echo $this->Bs->row() .
  $this->Bs->col('xs12');
 echo $this->BsForm->input('Deliberation['.$acte_id.'].num_pref', [
             'type'=>'select',
             'options' => $nomenclatures,
             'label' => __('Classification'),
             'data-acte-code-classification' => $acte_id,
             'data-placeholder' => __('Sélectionner la classification'),
             'value' => !empty($acte['Deliberation']['num_pref']) ? $acte['Deliberation']['num_pref'] : null,
             'empty' => true,
             'autocomplete' => 'off',
             'class' => 'selectone',
             'escape' => false]);
 echo $this->Bs->close(2);

 echo $this->Bs->div('spacer', '&nbsp;');

 echo $this->Bs->row() .
  $this->Bs->col('xs12');
 echo $this->BsForm->input('Deliberation['.$acte_id.'].typologiepiece_code', [
             'type'=>'select',
             'options' => $typologiepieces,
             'label' => __('Type de pièce principale'),
             'data-typologiepiece-type' => 'Principal',
             'data-placeholder' => __('Sélectionner le type de pièce principale'),
             'empty' => true,
             'autocomplete' => 'off',
             'value' => !empty($acte['Deliberation']['typologiepiece_code']) ? $acte['Deliberation']['typologiepiece_code']: null,
             'class' => 'selectone',
             'escape' => false]);
echo $this->Bs->close(2);
echo $this->Bs->div('spacer', '&nbsp;');
echo $this->Bs->row() .
 $this->Bs->col('xs12');
echo $this->BsForm->checkbox('Deliberation['.$acte_id.'].tdt_document_papier', [
        'autocomplete' => 'off',
        'title' => __('Envoi de documents papiers complémentaires'),
        'checked' => !empty($acte['Deliberation']['tdt_document_papier']) ? true: false,
        'label' => __('Envoi de documents papiers complémentaires'),
        'help'=> __("Cocher la case ci-contre si vous souhaitez envoyer les actes au format papier.")]);
        echo $this->Bs->close(2);
        echo $this->Bs->div('spacer', '&nbsp;');
echo $this->Bs->row() .
 $this->Bs->col('xs12');
echo $this->Bs->btn($this->Bs->icon('download') .' '.__('Télécharger l\'acte N°%s', $acte['Deliberation']['num_delib']), [
                    'controller' => 'actes',
                    'action' => 'download', $acte['Deliberation']['id']], [
                    'type' => 'default',
                    'size' => 'sm',
                    'title' => __('Télécharger l\'acte'),
                    'escape' => false,
                ]);

                echo $this->Bs->close(2);
                echo $this->Bs->div('spacer', '&nbsp;');
if (!empty($acte['Annexe'])) {
    echo $this->Bs->row() .
  $this->Bs->col('xs12');
    $aTitles = [
   ['title' => __('Nom du fichier')],
   ['title' => __('Titre')],
   ['title' => __('Type de pièce')],
   ['title' => __('Actions')],
];
    echo $this->Bs->table($aTitles, ['hover', 'striped'], ['caption' => __('Liste des annexes'), 'class'=> ['wd-table-file']]);
    foreach ($acte['Annexe'] as $annexe) {
        echo $this->Bs->cell($annexe['filename'], 'wd-table-file-name');
        echo $this->Bs->cell($annexe['titre'], 'wd-table-file-titre');
        $this->BsForm->setLeft(0);
        $this->BsForm->setRight(12);
        echo $this->Bs->cell($this->BsForm->input('Deliberation['.$acte_id.']'.'Annexe['.$annexe['id'].'].typologiepiece_code', [
                'type'=>'select',
                'options' => $typologiepieceAnnexes,
                'label' => false,
                'value' => !empty($annexe['typologiepiece_code']) ? $annexe['typologiepiece_code'] : null,
                'data-typologiepiece-type' => 'Annexe',
                'data-typologiepiece-annexe-id' => $annexe['id'],
                'data-placeholder' => __('Sélectionner le type de pièce'),
                'class' => 'selectone',
                'empty' => true,
                'autocomplete' => false,
                'escape' => false]));
        $this->BsForm->setDefault();
        echo $this->Bs->cell($this->Bs->btn($this->Bs->icon('download'), [
                    'controller' => 'annexes',
                    'action' => 'download', $annexe['id']], [
                    'type' => 'default',
                    'size' => 'sm',
                    'title' => __('Télécharger l\'annexe'),
                    'escape' => false,
                ]), 'wd-table-file-action');
    }
    echo $this->Bs->endTable();

    echo $this->Bs->close(2);
}
echo $this->BsForm->input(
    'Deliberation['.$acte_id.'].id',
    [
        'data-acte-id' => $acte_id,
        'value' => $acte_id,
        'type'=>'hidden'
    ]
);
echo $this->BsForm->input(
    'Deliberation['.$acte_id.'].typeacte_id',
    [
        'data-acte-typeacte-id' => $acte['Deliberation']['typeacte_id'],
        'value' => $acte['Deliberation']['typeacte_id'],
        'type'=>'hidden'
    ]
);
echo $this->Bs->tag('h4', $acte['Deliberation']['objet_delib'], ['style'=>'display:none']);
