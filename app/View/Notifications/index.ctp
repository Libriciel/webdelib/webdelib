<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Réglages des notifications'));

echo $this->Bs->tag('h3', __('Réglages des notifications'));
echo $this->BsForm->create('Notifications', ['url' => ['controller' => 'notifications', 'action' => 'index'], 'type' => 'post']);

echo $this->element('notification_formulaire', [
    'left'=> 3 ,
    'right'=> 9,
    'form'=> 'notification_preference',
    'notificationsForm' => $this->data,
    'notif' => $notif,
    'not_accept_notif' => $not_accept_notif
]);
echo $this->Html2->btnSaveCancel(null, $previous);
echo $this->BsForm->end();
?>


