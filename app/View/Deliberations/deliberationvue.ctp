<h2><?php echo $this->Html->image('/img/icons/synthese.png') . __('Déliberation') . ' : '; ?></h2>

<div class="pave">
    <?php
    echo '<br />' . __('Nom fichier') . ' : ' . $deliberation['Deliberation']['deliberation_name'];
    echo '<br />' . __('Taille') . ' : ' . $deliberation['Deliberation']['deliberation_size'];
    echo '<br />' . $this->Html->link(__('Télécharger'), '/deliberations/download/' . $deliberation['Deliberation']['id'] . '/deliberation') . '<br /><br /><br />';
    ?>
</div>

<br/><br/>

<div class="optional">
    <?php if (!empty($annexes)) { ?>
        <?php echo $this->Html->image('/img/icons/bookmark.png') . 'Annexe(s) :'; ?>
        <?php
        foreach ($annexes as $annexe) :
            echo '<br />' . __('Nom fichier') . ' : ' . $annexe['Annexe']['filename'];
            echo '<br />' . __('Taille') . ' : ' . $annexe['Annexe']['size'];
            echo '<br />' . $this->Html->link(__('Télécharger'), '/annexes/download/' . $annexe['Annexe']['id']);
            ?><br/><br/>
        <?php
        endforeach;
    }
    ?>
</div>

<div class="actions">
<?php echo $this->Html->link('<i class="fa fa-arrow-left"></i> ' . __('Retour'), 'javascript:history.go(-1)', ['class' => 'btn', 'title' => __('Retour fiche'), 'escape' => false]); ?>
</div>
