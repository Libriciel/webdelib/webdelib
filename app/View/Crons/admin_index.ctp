<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Tâches automatiques'));

echo $this->Bs->tag('h3', __('Liste des tâches automatiques'));

echo $this->Bs->btn($this->Bs->icon('cogs') . ' ' . __('Exécuter toutes les tâches'), ['controller' => 'crons', 'action' => 'runCrons'], ['type' => 'primary',
    'escapeTitle' => false,
    'title' => __('Exécuter toutes les tâches planifiées maintenant')]);

echo $this->Bs->table(
    [
    ['title' => __('État')],
    ['title' => __('Nom')],
    ['title' => __('Date exécution prévue')],
    ['title' => __('Délai entre 2 exécutions')],
    ['title' => __('Date dernière exécution')],
    ['title' => __('Active')],
    ['title' => __('Actions')]
        ],
    ['hover', 'striped']
);

foreach ($this->data as $rownum => $rowElement) {
    if ($rowElement['Cron']['lock']) {
        $rowElement['Cron']['statusLibelle'] = $this->Bs->btn($this->Bs->icon('lock') . ' ' . __('Verrouillée'), [
            'action' => 'unlock', $rowElement['Cron']['id']], [
            'escapeTitle' => false,
            'class' => 'label label-warning',
            'title' => __("Déverrouiller la tâche : La tâche est verrouillée, ce qui signifie qu'elle est en cours d'exécution ou dans un état bloqué suite à une erreur")]);
    } else {
        switch ($rowElement['Cron']['last_execution_status']) {
            case 'LOCKED':
                $rowElement['Cron']['statusLibelle'] = '<span class="label label-info" title="' . __('La tâche est verrouillée, ce qui signifie qu\'elle est en cours d\'exécution ou dans un état bloqué suite à une erreur') . '"><i class="fa fa-lock"></i> ' . __('Verrouillée') . '</span>';
                break;
            case 'SUCCES':
                $rowElement['Cron']['statusLibelle'] = '<span class="label label-success" title="' . __('Opération exécutée avec succès') . '"><i class="fa fa-check"></i> ' . __('Exécutée avec succès') . '</span>';
                break;
            case 'WARNING':
                $rowElement['Cron']['statusLibelle'] = '<span class="label label-warning" title="' . __('Avertissement(s) détecté(s) lors de l\'exécution, voir les détails de la tâche') . '"><i class="fa fa-info"></i> ' . __('Exécutée, en alerte') . '</span>';
                break;
            case 'FAILED':
                $rowElement['Cron']['statusLibelle'] = '<span class="label label-danger" title="' . __('Erreur(s) détectée(s) lors de l\'exécution, voir les détails de la tâche') . '"><i class="fa fa-exclamation-triangle"></i> ' . __('Non exécutée : erreur') . '</span>';
                break;
            default:
                $rowElement['Cron']['statusLibelle'] = '<span class="label label-default" title="' . __('La tâche n\'a jamais été exécutée') . '">' . __('En attente') . '</span>';
        }
    }

    //cell date derniere execution
    if ($rowElement['Cron']['last_execution_start_time'] != null) {
        $date_last_exec = $this->Time->format("d/m/Y à H:i:s", $rowElement['Cron']['last_execution_start_time']);
    } else {
        $date_last_exec = __('Jamais');
    }

    //liste des actions
    $liste_bouton = $this->Bs->btn(
        $this->Bs->icon('info'),
        [
        'controller' => 'crons',
        'action' => 'view',
        $rowElement['Cron']['id']],
        [
        'type' => 'default',
        'escapeTitle' => false,
        'title' => __('Informations sur la tâche.')]
    );

    $liste_bouton .= $this->Bs->btn(
        $this->Bs->icon('clock'),
        [
        'controller' => 'crons',
        'action' => 'planifier',
        $rowElement['Cron']['id']],
        [
        'type' => 'primary',
        'escapeTitle' => false,
        'title' => __('Planifier la tâche')]
    );

    $liste_bouton .= $this->Bs->btn(
        $this->Bs->icon('cog'),
        [
        'controller' => 'crons',
        'action' => 'executer',
        $rowElement['Cron']['id']],
        [
        'type' => 'success',
        'escapeTitle' => false,
        'title' => __('Exécuter la tâche')]
    );

    echo
    $this->Bs->cell($rowElement['Cron']['statusLibelle']) .
    $this->Bs->cell($rowElement['Cron']['nom']) .
    $this->Bs->cell($this->Time->format("d/m/Y à H:i", $rowElement['Cron']['next_execution_time'])) .
    $this->Bs->cell($rowElement['Cron']['durationLibelle']) .
    $this->Bs->cell($date_last_exec) .
    $this->Bs->cell($rowElement['Cron']['activeLibelle']) .
    $this->Bs->cell($this->Bs->div('btn-group-vertical') . $liste_bouton . $this->Bs->close());
}

echo $this->Bs->endTable();
