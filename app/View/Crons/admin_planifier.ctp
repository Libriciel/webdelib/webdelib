<?php

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Tâches automatiques'), ['action' => 'index']);
$this->Html->addCrumb(__('Planification de la tâche'));

echo $this->Bs->tag('h3', __('Planification de la tâche') . ' : ' . $this->data['Cron']['nom']);

echo $this->BsForm->create('Crons', ['type' => 'post', 'url' => ['controller' => 'crons', 'action' => 'planifier'], 'class' => 'form-inline']);
echo $this->Html->tag('div', null, ['class' => 'panel panel-default']);
echo $this->Html->tag('div', __('Planification de la tâche'), ['class' => 'panel-heading']);
echo $this->Html->tag('div', null, ['class' => 'panel-body']);

// Date de la prochaine exécution
echo $this->Bs->row()
 . $this->Bs->col('xs3')
 . $this->Html->tag('label', __('Date de la prochaine exécution'))
 . $this->Bs->close()
 . $this->Bs->col('xs9')
 . $this->Form->input('Cron.next_execution_date', ['label' => false, 'type' => 'date', 'dateFormat' => 'DMY', 'minYear' => date('Y') - 0, 'maxYear' => date('Y') + 2, 'monthNames' => false, 'empty' => true])
 . $this->Bs->close(2);

// Heure de la prochaine exécution
echo $this->Bs->row()
 . $this->Bs->col('xs3')
 . $this->Html->tag('label', __('Heure de la prochaine exécution'))
 . $this->Bs->close()
 . $this->Bs->col('xs9')
 . $this->Form->input('Cron.next_execution_heure', ['label' => false, 'type' => 'time', 'timeFormat' => '24', 'interval' => 15])
 . $this->Bs->close(2);

// Délai entre deux exécutions
echo $this->Bs->row()
 . $this->Bs->col('xs3')
 . $this->Html->tag('label', __('Délai entre deux exécutions'))
 . $this->Bs->close()
 . $this->Bs->col('xs9')
 . $this->DurationPicker->picker('Cron.execution_duration', ['label' => false, 'empty' => true, 'value' => $this->data['Cron']['execution_duration']])
 . $this->Bs->close(2);

// Activation
echo $this->Bs->row()
 . $this->Bs->col('xs3')
 . $this->Html->tag('label', __('Activation') . '&nbsp;&nbsp;')
 . $this->Bs->close()
 . $this->Bs->col('xs9')
 . $this->BsForm->checkbox('Cron.active', ['autocompete' => false, 'title' => __('Activation'), 'inline' => 'inline', 'label' => false]
)
 . $this->Bs->close(2);

echo $this->Bs->endPanel();
echo $this->Html2->btnSaveCancel('', $previous);
echo $this->BsForm->hidden('Cron.id');
echo $this->BsForm->end();
