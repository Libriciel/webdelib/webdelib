<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Tâches automatiques'), ['action'=> 'index']);
$this->Html->addCrumb($contenuVue['titreVue']);
echo $this->Bs->tag('h3', $contenuVue['titreVue']);

// Affichage du contenu de chaque onglet
foreach ($contenuVue['onglets'] as $i => $onglet) {
    // Affichage des sections principales
    foreach ($onglet['sections'] as $section) {
        // affichage du titre de la section
        echo $this->Bs->panel($section['titre']);
        // Parcours des lignes de la section
        foreach ($section['lignes'] as $iLigne => $ligne) {
            echo '<b>' . $ligne[0]['libelle'] . ' : </b>' . $ligne[0]['valeur'] . '<br />';
        }
        echo $this->Bs->endPanel();
    }
}

echo $this->Bs->div('text-center') .
 $this->Bs->div('btn-group') .
 $this->Bs->btn($this->Bs->icon('arrow-left') . ' ' . __('Retour'), $previous, ['type' => 'default', 'escapeTitle' => false, 'title' => $contenuVue['lienRetour']['title']]);

if ($this->request->data['Cron']['lock']) {
    echo $this->Bs->btn($this->Bs->icon('unlock') . ' ' . __('Déverrouiller'), ['controller' => 'crons', 'action' => 'unlock', $this->request->data['Cron']['id']], ['escapeTitle'=> false, 'type' => 'danger', 'title' => __('Déverrouiller')]);
} else {
    echo $this->Bs->btn($this->Bs->icon('cog') . ' ' . __('Exécuter'), ['controller' => 'crons', 'action' => 'executer', $this->request->data['Cron']['id']], ['escapeTitle'=> false, 'type' => 'primary', 'title' => __('Exécuter')]);
}
echo $this->Bs->close(2);
