<!--nocache-->
<?php
echo $this->Bs->div('page-header');
echo $this->Bs->tag('h1', __('Accueil') . ' ' . $this->Bs->tag('small', __('Bienvenue sur webdelib')));
echo $this->Bs->close();
echo $this->Bs->tag('p', $this->Bs->icon('user') . ' Utilisateur : ' . $infoUser . $this->Bs->tag('br/') .
        $this->Bs->icon('sitemap') . ' Service : ' . $infoServiceEmeteur);
if ($this->permissions->check('Projets', 'create')) {
    echo $this->Bs->div('btn-group');
    echo $this->Bs->btn($this->Bs->icon('plus') . ' ' . __('Créer un projet'), ['controller' => 'projets', 'action' => 'add'], ['type' => 'primary', 'escapeTitle' => false, 'title' => __('Créer un nouveau projet')]);
    echo $this->Bs->close();
    echo $this->Bs->tag('br/') . $this->Bs->tag('br/');
}

if ($this->permissions->check('mesProjetsATraiter', 'read')) {
    echo $this->requestAction(
            ['admin' => false, 'prefix' => null, 'plugin' => null, 'controller' => 'projets', 'action' => 'mesProjetsATraiter'], ['return', 'render' => 'bannette']
    ) .
    $this->Bs->tag('br/') . $this->Bs->tag('br/');
}
if ($this->permissions->check('mesProjetsValidation', 'read')) {
    echo $this->requestAction(
            ['admin' => false, 'prefix' => null, 'plugin' => null, 'controller' => 'projets', 'action' => 'mesProjetsValidation'], ['return', 'render' => 'bannette']
    ) .
    $this->Bs->tag('br/') . $this->Bs->tag('br/');
}
if ($this->permissions->check('Projets', 'create')) {
    echo $this->requestAction(
            ['admin' => false, 'prefix' => null, 'plugin' => null, 'controller' => 'projets', 'action' => 'mesProjetsRedaction'], ['return', 'render' => 'bannette']) .
    $this->Bs->tag(null, '<br/><br/>');
}
if ($this->permissions->check('Seances', 'create')) {
    echo $this->requestAction(['admin' => false, 'prefix' => null, 'plugin' => null, 'controller' => 'seances', 'action' => 'index'], ['return', 'render' => 'bannette']);
}
?>
<!--/nocache-->
