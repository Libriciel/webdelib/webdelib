<?php

$nbProjets = isset($this->Paginator) ? $this->Paginator->counter('{:count}') : $nbProjets;
$titre = $titreVue . ' (' . $nbProjets . ' ' . ($nbProjets > 1 ? 'projets' : 'projet') . ')';

    if (!empty($crumbs)) {
        foreach ($crumbs as $crumb) {
            if(is_array($crumb)){
                $this->Html->addCrumb($crumb[0],$crumb[1]);  
            }else
            {
                $this->Html->addCrumb($crumb);  
            }
            
        }
    }
    echo $this->element('filtre');
    echo $this->Bs->tag('h3', $titre);

$titres = [];
$titres[] = ['title' => 'Id.'];
$titres[] = ['title' => __('Type d\'acte')];
$titres[] = ['title' => __('Libellé de l\'acte')];
$titres[] = ['title' => __('Circuit')];
$titres[] = ['title' => __('Etape courante')];
$titres[] = ['title' => __('Etape(s) suivante(s)')];
$titres[] = ['title' => __('Etat')];
$titres[] = ['title' => __('Actions')];

echo $this->Bs->table($titres, ['hover', 'striped']);

foreach ($this->data as $projet) {

    echo $this->Bs->cell($this->Html->link($projet['Deliberation']['id'], ['controller' => 'projets', 'action' => 'view', $projet['Deliberation']['id']]));
    echo $this->Bs->cell($projet['Typeacte']['name']);
    echo $this->Bs->cell($projet['Deliberation']['objet']);
    echo $this->Bs->cell($projet['Circuit']['nom']);
    $etape_encours='';
    foreach($projet['Deliberation']['traitement_encours'] as $etape) 
    {
        $etape_encours .= $etape.$this->Bs->tag('br');
    }
    
    echo $this->Bs->cell($etape_encours);
    $etapes_suivantes='';
    foreach($projet['Deliberation']['traitement_suivant'] as $etape) 
    {
        $etapes_suivantes .= $etape.$this->Bs->tag('br');
    }
    
    echo $this->Bs->cell($etapes_suivantes);
    echo $this->Bs->cell($projet['etat_libelle']);
    
    echo $this->element('Projet/actions', ['projet' => $projet, 'cell_class' => 'text-right']);
}
echo $this->Bs->endTable();
//paginate
echo $this->Paginator->numbers(['before' => '<ul class="pagination">', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a', 'tag' => 'li', 'after' => '</ul><br />']);
echo $this->Html2->btnCancel($previous).' '.
        $this->Bs->btn($this->Bs->icon('download') . ' ' . __('Export csv'), ['controller' => 'tableau_de_bord', 'action' => 'downloadCsv', $etat, $dashlet], ['type' => 'primary', 'escapeTitle' => false, 'title' => __('Télécharger la liste au format csv')]);