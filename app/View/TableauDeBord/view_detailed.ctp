<?php

//echo $this->Bs->modal($titre,$affichage);
//echo $titre;
//echo $affichage;
$id = 'view';
$out = '<div class="modal-dialog modal-lg">';
// Content
$out .= '<div class="modal-content">';
$out .= '<div class="modal-header">';
$out .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
$out .= '<h4 class="modal-title" id="' . $id . 'Label">' . $titreVue . '(' . $nbProjets . ')' . '</h4>';
$out .= '</div>';
$out .= '<div class="modal-body">';
$out .= $this->Bs->btn($this->Bs->icon('download') . ' ' . __('Télécharger'), ['controller' => 'tableau_de_bord', 'action' => 'downloadCsv', $projets], ['type' => 'default', 'escapeTitle' => false, 'title' => __('télécharger la liste présente au format csv')]);
$out .= $this->element('9cases', ['projets' => $this->data, 'traitement_lot' => isset($traitement_lot) ? $traitement_lot : null]
);
$out .= '</div>';
$out .= '</div>';
// End Content
$out .= '</div>';
echo $out;
