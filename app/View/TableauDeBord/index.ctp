<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

echo $this->Bs->scriptBlock(
    'require(["domReady"], function (domReady) {'
    . 'domReady(function () {'
        . 'Chart.defaults.responsive = true;'
        . 'legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"'
        . '});'
        . '});'
);

$this->Html->addCrumb(__('Tous les projets'));
$this->Html->addCrumb(__('Tableau de bord'));

echo $this->Bs->tag('h3', __('Tableau de bord des projets'));
echo $this->Bs->tag('br/');
echo $this->Bs->tag('br/');

foreach ($dashboards as $key_dashboard => $dashboard) {
    foreach (array_keys($dashboard['data']) as $key=>$dashboard_data) {
        ${'values_'.$dashboard_data} = [];
        //debug($dashboard['data'][$dashboard_data]);
        if (!empty($dashboard['data'][$dashboard_data])) {
            //debug($dashboard['data'][$dashboard_data]);
            if (is_array($dashboard['data'][$dashboard_data])) {
                //debug($dashboard['data'][$dashboard_data]);
                foreach ($dashboard['data'][$dashboard_data] as $key=>$data) {
                    ${'values_'.$dashboard_data}['labels'][]= $data['Dashboard']['name'] ? $data['Dashboard']['name'] : 'test';
                    ${'values_'.$dashboard_data}['datasets'][0]['data'][]= $data['Dashboard']['count'];
                    ${'values_'.$dashboard_data}['datasets'][0]['backgroundColor'][]= $colors[$key]['color'];
                }

                echo $this->Bs->scriptBlock('
                        var data_'.$key_dashboard.$dashboard_data.' = '. json_encode(${'values_'.$dashboard_data}).';
                    ');
            } else {
                echo $this->Bs->scriptBlock('
                        var data_'.$key_dashboard.$dashboard_data.' = \''. $dashboard['data'][$dashboard_data] .'\';
                    ');
            }
        } else {
            if ($dashboard['data'][$dashboard_data]==='0') {
                echo $this->Bs->scriptBlock('
                      var data_'.$key_dashboard.$dashboard_data.' = \'0\';
                  ');
            } else {
                ${'values_'.$dashboard_data}['labels'][] = '';
                ${'values_'.$dashboard_data}['datasets'][]['data'][]= '0.01';
                ${'values_'.$dashboard_data}['datasets'][]['backgroundColor'][]= $colors[0]['color'];
                //debug(${'values_'.$dashboard_data});
                echo $this->Bs->scriptBlock('
                      var data_'.$key_dashboard.$dashboard_data.' = '. json_encode(${'values_'.$dashboard_data}) . ';
                  ');
            }
        }
    }
}

 $expand = $this->Bs->btn($this->Bs->icon('expand-alt'), 'javascript:void(0);', [
            'type'=> 'info',
            'size'=> 'xs',
            'tag'=>'button',
            'title'=>__('Plein écran'),
          //  'class' => 'navbar-right',
            'escapeTitle' => false,
            'data-toggle'=> 'windows_expand']);

echo $this->element('TableauDeBord/dashboard_etat', ['colors'=> $colors, 'dashboards'=> $dashboards,'expand' => $expand], [/*'key' => AuthComponent::$sessionKey*/]);
echo $this->element('TableauDeBord/dashboard_process', ['colors'=> $colors, 'dashboards'=> $dashboards,'expand' => $expand], [/*'key' => AuthComponent::$sessionKey*/]);

echo $this->Bs->scriptBlock(
    'require(["domReady"], function (domReady) {'
    . 'domReady(function () {'
    . '$("button[data-toggle=\"windows_expand\"]").on("click", function () {
        var canvas, isFullScreen;

        canvas = $(this).closest( "li" ).get(0);
        $(this).attr("title", "Quitter le mode plein écran (Echap)");
        $(this)
        .find(\'[data-fa-i2svg]\')
        .toggleClass(\'fa-compress fa-expand\');

        if (canvas.requestFullscreen) {
          if(document.fullscreenElement) {
            document.exitFullscreen();
          }
          else{
            canvas.requestFullscreen();
          }

        } else if (canvas.webkitRequestFullscreen) {
          if(document.webkitFullscreenElement){
            document.webkitCancelFullScreen();
          }
          else{
            canvas.webkitRequestFullscreen();
          }
        }
  })'
  . '});'
        . '});'
);
