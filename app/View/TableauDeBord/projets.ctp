<?php

$nbProjets = isset($this->Paginator) ? $this->Paginator->counter('{:count}') : $nbProjets;
$titre = $titreVue . ' (' . $nbProjets . ' ' . ($nbProjets > 1 ? 'projets' : 'projet') . ')';

    if (!empty($crumbs)) {
        foreach ($crumbs as $crumb) {
            if(is_array($crumb)){
                $this->Html->addCrumb($crumb[0],$crumb[1]);  
            }else
            {
                $this->Html->addCrumb($crumb);  
            }
            
        }
    }
    echo $this->element('filtre');
    echo $this->Bs->tag('h3', $titre);

echo $this->element('9cases', ['projets' => $this->data, 'traitement_lot' => false]
);
//paginate
echo $this->Paginator->numbers(['before' => '<ul class="pagination">', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a', 'tag' => 'li', 'after' => '</ul><br />']);
echo $this->Html2->btnCancel($previous).' '.
        $this->Bs->btn($this->Bs->icon('download') . ' ' . __('Export csv'), ['controller' => 'tableau_de_bord', 'action' => 'downloadCsv', $etat, $dashlet], ['type' => 'primary', 'escapeTitle' => false, 'title' => __('Télécharger la liste au format csv')]);