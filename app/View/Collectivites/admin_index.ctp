<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Collectivité'));

echo $this->Bs->tag('h3', __('Informations de votre collectivité')) .
 $this->Bs->table([
   ['title' => __('Logo')],
    ['title' => __('Nom')],
    ['title' => __('Adresse')],
    ['title' => __('Telephone')],
    ['title' => __('Actions')],
        ], []) .
        $this->Bs->cell(
            $this->Html->image($logo_path, ['alt' => __('logo de la collectivité'), 'style' => 'max-width: 500px'])
        ) .
        $this->Bs->cell($collectivite['Collectivite']['nom']).
        $this->Bs->cell(
            $collectivite['Collectivite']['adresse']
           . ' ' . $collectivite['Collectivite']['CP']
           . ' ' . $collectivite['Collectivite']['ville']
        ) .
        $this->Bs->cell($collectivite['Collectivite']['telephone']).
 $this->Bs->cell(
     $this->Bs->div('btn-group') .
        $this->Bs->btn($this->Bs->icon('pencil-alt'), [
            'controller' => 'collectivites', 'action' => 'edit'], [
            'type' => 'primary', 'escapeTitle' => false, 'title' => __('Modifier la collectivité')]) .
     $this->Bs->btn($this->Bs->icon('user'), [
         'controller' => 'collectivites', 'action' => 'rgpd'], [
         'type' => 'primary', 'escapeTitle' => false, 'title' => __('Modifier le Responsable/DPO')]) .
    $this->Bs->btn($this->Bs->icon('sliders-h'), [
        'controller' => 'collectivites', 'action' => 'login'], [
        'type' => 'primary', 'escapeTitle' => false, 'title' => __('Paramétrer la page de connexion')]) .
    $this->Bs->close()
 ) .
 $this->Bs->endTable();
