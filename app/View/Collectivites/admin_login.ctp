<?php

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Collectivité'), ['controller' => 'collectivites', 'action' => 'index']);
$this->Html->addCrumb(__('Paramétrer la page de connexion'));

echo $this->Html->css('ls-composants-bootstrap-3.css');
echo $this->Html->script('ls-elements-bootstrap3.js');
echo $this->Html->script('components/font-awesome/js/all.min', ['data-auto-replace-svg'=>"nest"]);
?>
<ls-lib-login-config configuration='<?php echo $loginConfig; ?>' ></ls-lib-login-config>

<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {

      $('ls-lib-login-config')[0].addEventListener('save', function(e) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/admin/collectivites/login',
                data: e['detail'],
                complete : function(data) {
                    alert($.parseJSON(data.responseText).val);
                }
            });
        });

        $('ls-lib-login-config')[0].addEventListener('back', function(e) {
            window.location.href = "/admin/collectivites";
        });
      });
});
</script>
