<h2><?php echo __('Paramétrage des e-mails'); ?></h2>
<?php echo $this->Form->create('Collectivites', ['url' => $this->webroot . 'collectivites/setMails', 'type' => 'file']); ?>

<table>
    <tr> 
        <td> <?php echo __('Projet refusé'); ?> </td>
        <td> <?php echo $this->Form->input('Mail.refus', ['type' => 'file', 'size' => '40']) ?></td>
        <td> <?php echo $this->Html->link(SHY, $email_path . 'refus', ['class' => 'link_voir', 'title' => 'Visualiser'], false, false) ?></td>
    </tr>
    <tr>
        <td> <?php echo __('Projet à traiter'); ?>  </td>
        <td> <?php echo $this->Form->input('Mail.traiter', ['type' => 'file', 'size' => '40']) ?> </td>
        <td> <?php echo $this->Html->link(SHY, $email_path . 'traiter', ['class' => 'link_voir', 'title' => 'Visualiser'], false, false) ?></td>
    </tr>
    <tr>
        <td> <?php echo __('Notification d\'insertion dans le circuit'); ?>  </td>
        <td> <?php echo $this->Form->input('Mail.insertion', ['type' => 'file', 'size' => '40']) ?> </td>
        <td> <?php echo $this->Html->link(SHY, $email_path . 'insertion', ['class' => 'link_voir', 'title' => 'Visualiser'], false, false) ?></td>
    </tr>
    <tr>
        <td> <?php echo __('Convocation'); ?>  </td>
        <td> <?php echo $this->Form->input('Mail.convocation', ['type' => 'file', 'size' => '40']) ?></td>
        <td> <?php echo $this->Html->link(SHY, $email_path . 'convocation', ['class' => 'link_voir', 'title' => 'Visualiser'], false, false) ?></td>
    </tr>
</table>

<p> <br/><i><?php echo __('(au format .txt)'); ?></i> </p>                                                                      
<br/><br/>
<div class="submit">
    <?php $this->Html2->boutonsSaveCancel(); ?>
</div>

<?php
echo $this->Form->end();
