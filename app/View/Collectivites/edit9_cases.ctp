<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
/**
 * COLONE ETAT
 */
$etat = $this->Bs->div('spacer') . $this->Bs->close() .
        $this->Bs->div('spacer') . $this->Bs->close() .
        $this->Bs->btn($this->Bs->icon('pause'), null, ['tag' => 'button', 'type' => 'default', 'class' => 'btn-action-m', 'escapeTitle' => false, 'disabled' => 'disabled', 'data-toggle' => 'popover', 'data-placement' => 'right', 'title' => __('titre')]
        ) .
        $this->Bs->div('spacer') . $this->Bs->close() .
        $this->Bs->tag('h4', 'N°###', ['class' => 'label label-default', 'style' => 'background-color: grey']);

/**
 * COLONE ACTION BOUTTONS
 */
$actions = $this->Bs->div('spacer') . $this->Bs->close() .
        $this->Bs->div('spacer') . $this->Bs->close() .
        $this->Bs->div('btn-group-vertical') .
        $this->Bs->btn($this->Bs->icon('eye', null, ['stylePrefix' => 'far']), 'javascript:void(0);', ['type' => 'default', 'class'=>'btn-action-m', 'escapeTitle' => false, 'disabled' => 'disabled']) .
        $this->Bs->btn($this->Bs->icon('pencil-alt'), 'javascript:void(0);', ['type' => 'default', 'escapeTitle' => false, 'class'=>'btn-action-m', 'disabled' => 'disabled']) .
        $this->Bs->btn($this->Bs->icon('road'), 'javascript:void(0);', ['type' => 'default', 'escapeTitle' => false, 'class'=>'btn-action-m', 'disabled' => 'disabled']) .
        $this->Bs->btn($this->Bs->icon('trash'), 'javascript:void(0);', ['type' => 'default', 'escapeTitle' => false, 'class'=>'btn-action-m', 'disabled' => 'disabled']) .
        $this->Bs->btn($this->Bs->icon('cog'), 'javascript:void(0);', ['type' => 'default', 'escapeTitle' => false, 'class'=>'btn-action-m', 'disabled' => 'disabled']) .
        $this->Bs->close(3);

/**
 * COLONE VUE SYNTHETIQUE : Creation du tableau a partir du JSON 9cases
 */
$this->BsForm->setLeft(0);
$this->BsForm->setRight(12);
$caseList = $this->Bs->div('spacer') . $this->Bs->close() .
        $this->Bs->div('spacer') . $this->Bs->close() .
        $this->Bs->panel('') . $this->Bs->row();
//tableau de correpondance
foreach ($templateProject as $key => $case) {
    $caseList .= $this->Bs->col('xs4') .
            $this->BsForm->select('Case' . $key, $selecteurProject, ['label' => false, 'empty' => true, 'class' => 'selectone', 'data-allow-clear' => true, 'data-placeholder' => __('Sélectionner une information à afficher'), 'default' => $case, 'inline' => true, 'autocomplete' => 'off', 'escape' => false]) .
            $this->Bs->close();
    if ($key == 2 || $key == 5 || $key == 8) {
        $caseList .= $this->Bs->close();
        if ($key != 8) {
            $caseList .= $this->Bs->row();
        }
    }
}
$caseList .= $this->Bs->close(3);


/**
 * AFFICHAGE
 */
$this->Html->addCrumb(__('Modifier l\'affichage de la vue synthétique'));
echo $this->Bs->tag('h3', __('Modifier l\'affichage de la vue synthétique')) .
 $this->Bs->div('spacer') . $this->Bs->close() .
 $this->BsForm->create('Collectivites', ['url' => ['controller' => 'collectivites', 'action' => 'edit9Cases'], 'type' => 'post']) .
 $this->Bs->table([['title' => __('État')], ['title' => __('Vue synthétique'), ['width' => '100%']], ['title' => __('Actions')]], ['hover', 'striped', 'bordered']) .
 $this->Bs->setTableNbColumn(5) .
 $this->Bs->cell($etat) .
 $this->Bs->cell($caseList, 'max') .
 $this->Bs->cell($actions, 'text-right') .
 $this->Bs->endTable();

echo $this->Bs->btn($this->Bs->icon('undo').' '.__('Restaurer les valeurs par défaut'), ['controller' => 'collectivites', 'action' => 'edit9Cases', 'revertModification'], ['type' => 'default', 'escapeTitle' => false, 'name' => __('Clore'), 'title' => __('Restaurer les valeurs par défaut de la vue synthétique'), 'confirm'=> __('Êtes-vous sûr de vouloir restaurer la vue synthétique par défaut ?')]);

echo $this->Bs->div('text-center') .
 $this->Html2->btnSaveCancel('', $previous).
 $this->BsForm->end() .
 $this->Bs->close() .
 $this->Bs->div('spacer') . $this->Bs->close() .
 $this->Bs->div('text-center') .
 $this->Bs->close();
