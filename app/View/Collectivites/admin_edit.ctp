<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Collectivité'), ['controller' => 'collectivites', 'action' => 'index']);
$this->Html->addCrumb(__('Modifier les informations de la collectivité'));

echo $this->Bs->tag('h3', __('Modifier les informations de la collectivité'));
echo $this->BsForm->create('Collectivite', [
    'url' => ['action' => 'edit'],
    'type' => 'file',
//    'class ' => 'input-group colorpicker-component'
]);

echo $this->BsForm->input('Collectivite.nom', ['label' => __('Nom'). ' <abbr title="obligatoire">*</abbr>', 'readonly' => $use_pastell ? true : false]) .
    $this->BsForm->input('Collectivite.siret', ['label' => __('Siret'). ' <abbr title="obligatoire">*</abbr>']) .
    $this->BsForm->input('Collectivite.code_ape', ['label' => __('Code APE'). ' <abbr title="obligatoire">*</abbr>']) .
    $this->BsForm->input('Collectivite.adresse', ['label' => __('Adresse'). ' <abbr title="obligatoire">*</abbr>']) .
    $this->BsForm->input('Collectivite.CP', ['label' => __('Code Postal'). ' <abbr title="obligatoire">*</abbr>']) .
    $this->BsForm->input('Collectivite.ville', ['label' => __('Ville'). ' <abbr title="obligatoire">*</abbr>']) .
    $this->BsForm->input('Collectivite.telephone', ['label' => __('Téléphone'). ' <abbr title="obligatoire">*</abbr>']) .
    $this->BsForm->select('Collectivite.force',[
        1 => __('Très faible'),
        2 => __('Faible'),
        3 => __('Moyen'),
        4 => __('Forte'),
        5 => __('Très forte'),
    ], [
        'class' => 'selectone',
        'label' => __('Force des mots de passe utilisateurs'
        ). ' <abbr title="obligatoire">*</abbr>']) .
    $this->BsForm->input('Collectivite.email', ['label' => __('Adresse mail'). ' <abbr title="obligatoire">*</abbr>']) .
    $this->BsForm->input('Collectivite.logo', [
      'label' => __('Logo'),
      'help' => __('Format de fichier accepté: png ou jpeg.'),
      'type' => 'file',
      'data-btnClass' => 'btn-primary',
      'data-text' => __('Choisir un fichier'),
      'data-placeholder' => __('Pas de fichier'),
      'class' => 'filestyle']) .
    $this->Html2->btnSaveCancel('', $previous) .
    $this->BsForm->end();
