<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Politique de confidentialité'));

echo $this->Bs->tag('h1', __('Politique de confidentialité'));

echo $this->Html->tag('br');

echo $this->Bs->tag('p', $this->Html->tag('strong', __('DEFINITION :')));

echo $this->Bs->tag('p',
'Une donnée personnelle est une information se rapportant à une personne
physique identifiée ou identifiable. Les personnes concernées doivent être en mesure d\'en avoir la maîtrise.
Une personne physique peut être identifiée :
directement (exemple : nom et prénom)
indirectement (exemple : par un numéro de téléphone ou de plaque
d’immatriculation, un identifiant tel que le numéro de sécurité sociale,
une adresse postale ou courriel, mais aussi la voix ou l’image, un identifiant (ID) dans
une base de données).

L’identification d’une personne physique peut être réalisée :
à partir d’une seule donnée (exemple : nom),
à partir du croisement d’un ensemble de données (exemple : une femme, vivant
à une adresse définie, dont la date de naissance est citée et membre de telle ou telle association).

Par contre, des coordonnées d’entreprises ou de collectivité (par exemple,
l’entreprise « Compagnie A » avec son adresse postale, le numéro de téléphone
de son standard et un courriel de contact générique « compagnie1@email.fr »)
ne sont pas, en principe, des données personnelles.');

echo $this->Bs->tag('p', 'La création de votre compte utilisateur sur web-delib demande la
saisie de données personnelles vous concernant. Vous trouverez ci-dessous le
détail des informations collectées ainsi que les traitements effectués. Ces
données sont collectées uniquement dans le cadre de votre activité
professionnelle et pour l\'utilisation exclusive de
l\'application web-delib.
Le fonctionnement dans les conditions nominales et recommandées de
l\'application, ne permet pas de partager ou de diffuser à un tiers vos
informations personnelles. Vous pouvez demander à tout moment à l\'administrateur
de la plateforme de vous communiquer l\'ensemble des informations collectées pour
la création de votre compte.');
echo $this->Html->tag('br');

?>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    <?php
                        echo $this->Bs->tag('h4', __('Responsable des traitements'));
                    ?>
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <?php
                    echo $this->Bs->tag('p', $this->Html->tag('strong', __('Entité :')));
                    echo $this->Bs->tag('p', $collectivite['nom'], 'style=padding-left:3em;');
                    echo $this->Bs->tag('p', $collectivite['adresse'] . ', ' . $collectivite['CP'] . ' ' . $collectivite['ville'], 'style=padding-left:3em;');
                    echo $this->Bs->tag('p', 'n° SIRET : '. $collectivite['siret'], 'style=padding-left:3em;');
                    echo $this->Bs->tag('p', 'code APE : '. $collectivite['code_ape'], 'style=padding-left:3em;');
                    echo $this->Bs->tag('p', 'n° de téléphone : '. $collectivite['telephone'], 'style=padding-left:3em;');
                    echo $this->Bs->tag('p', 'email : '. $collectivite['email'], 'style=padding-left:3em;');
                    echo $this->Bs->tag('p', $this->Html->tag('strong', __('Responsable de la structure / Responsable des traitements :')));
                    echo $this->Bs->tag('p', $collectivite['responsable_nom'] . ' ' . $collectivite['responsable_prenom'], 'style=padding-left:3em;');
                    echo $this->Bs->tag('p', $collectivite['responsable_fonction'], 'style=padding-left:3em;');
                    echo $this->Bs->tag('p', $this->Html->tag('strong', __('Délégué à la protection des données (DPD / DPO) :')));
                    echo $this->Bs->tag('p', $collectivite['dpo_nom'] . ' ' . $collectivite['dpo_prenom'], 'style=padding-left:3em;');
                    echo $this->Bs->tag('p', $collectivite['dpo_email'], 'style=padding-left:3em;');
                ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    <?php
                        echo $this->Bs->tag('h4', __('Article 1 - Mentions légales'));
                    ?>
                </a>
            </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
                <?php
                    echo $this->Bs->tag('p',  $this->Html->tag('strong', __('1.1 Logiciel (ci-après « le logiciel ») :')));
                    echo $this->Bs->tag('p', 'webdelib', 'style=padding-left:3em;');

                    echo $this->Bs->tag('p',  $this->Html->tag('strong', __('1.2 Editeur (ci-après « l\'éditeur ») :')));

                    echo $this->Bs->tag('p', 'LIBRICIEL SCOP', 'style=padding-left:3em;');
                    echo $this->Bs->tag('p', '140 rue Aglaonice de Thessalie – 34170 Castelnau-le-lez', 'style=padding-left:3em;');
                    echo $this->Bs->tag('p', 'Numéro SIRET : 49101169800033', 'style=padding-left:3em;');
                ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <?php
                        echo $this->Bs->tag('h4', 'Article 2 - Hébergement');
                    ?>
                </a>
            </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
                <?php

                    if ($collectivite['saas']) {
                        echo $this->Bs->tag('h4', 'MODE SAAS PAR LIBRICIEL');

                        echo $this->Bs->tag('p', $this->Html->tag('strong', __('2.1 Maintenance du serveur :')));
                        echo $this->Bs->tag('p', 'Le serveur applicatif est maintenu par :');
                        echo $this->Bs->tag('p', 'Libriciel SCOP 140 rue Aglaonice de Thessalie – 34170 Castelnau-le-lez', 'style=padding-left:3em;');
                        echo $this->Bs->tag('p', 'Numéro SIRET : 49101169800033', 'style=padding-left:3em;');

                        echo $this->Html->tag('br');

                        echo $this->Bs->tag('p', $this->Html->tag('strong', __('2.2 Hébergeur :')));
                        echo $this->Bs->tag('p', 'Libriciel SCOP, assure l\'hébergement de vos données chez plusieurs de nos sous-traitants,
                    uniquement sur le territoire Français, en dehors des zones soumises au PATRIOT Act.');
                    } else {
                        echo $this->Bs->tag('h4', 'HEBERGEE PAR LE CLIENT (MONO-ENTITE)');
                        echo $this->Bs->tag('p', $this->Html->tag('strong', __('2.1 Hébergeur / maintenance du serveur :')));
                        echo $this->Bs->tag('p', 'Le serveur applicatif est maintenu par :');
                        echo $this->Bs->tag('p', $collectivite['nom'], 'style=padding-left:3em;');
                        echo $this->Bs->tag('p', $collectivite['adresse'] . ', ' . $collectivite['CP'] . ' ' . $collectivite['ville'], 'style=padding-left:3em;');
                        echo $this->Bs->tag('p', 'Représentée par : ' . $collectivite['responsable_nom'] . ' ' . $collectivite['responsable_prenom'] . ' en sa qualité de ' . $collectivite['responsable_fonction'], 'style=padding-left:3em;');
                        echo $this->Bs->tag('p', 'Numéro de SIRET : ' . $collectivite['siret'], 'style=padding-left:3em;');
                        echo $this->Bs->tag('p', 'Code APE : ' . $collectivite['code_ape'], 'style=padding-left:3em;');
                        echo $this->Bs->tag('p', 'N° de téléphone : ' . $collectivite['telephone'], 'style=padding-left:3em;');
                        echo $this->Bs->tag('p', 'Email : ' . $collectivite['email'], 'style=padding-left:3em;');

                        echo $this->Html->tag('br');
                    }
                ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingFour">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                    <?php
                        echo $this->Bs->tag('h4', 'Article 3 - Collecte');
                    ?>
                </a>
            </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
            <div class="panel-body">
                <?php
                    echo $this->Bs->tag('p', $this->Html->tag('strong', __('Création d\'un utilisateur:') , 'Lors de la création de votre compte utilisateur, les informations suivantes sont collectées obligatoirement :'));
                    echo $this->Bs->tag('li', 'Nom d\'utilisateur');
                    echo $this->Bs->tag('li', 'Prénom de l\'utilisateur');
                    echo $this->Bs->tag('li', 'Identifiant de connexion');
                    echo $this->Bs->tag('li', 'Consentement à être notifié par e-mail');
                    echo $this->Html->tag('br');
                    echo $this->Bs->tag('p', 'Lors de la création de votre compte utilisateur, les informations suivantes peuvent être collectées :');
                    echo $this->Bs->tag('li', 'Adresse de messagerie professionnelle');
                    echo $this->Bs->tag('li', 'Note');
                    echo $this->Bs->tag('li', 'Numéro de téléphone fixe');
                    echo $this->Bs->tag('li', 'Numéro de téléphone portable');
                    echo $this->Bs->tag('li', 'Service');
                ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingFive">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                    <?php
                        echo $this->Bs->tag('h4', 'Article 4 - Traitements');
                    ?>
                </a>
            </h4>
        </div>
        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
            <div class="panel-body">
                <?php
                    echo $this->Bs->tag('p', 'Vos informations personnelles sont utilisées pour le bon fonctionnement de webdelib dans les cas listés ci-après :');
                    echo $this->Bs->tag('li', 'lister les utilisateurs de l\'application : votre identifiant de connexion, nom, prénom,
                    adresse de messagerie professionnelle, numéro de téléphone fixe, numéro de téléphone portable, note et service apparaissent dans la liste.');
                    echo $this->Bs->tag('li', 'lister les utilisateurs dans un circuit de traitement : votre identifiant de connexion, nom, prénom apparaissent dans la liste.');
                    echo $this->Bs->tag('li', 'journal d\'événement d\'un traitement : votre identifiant de connexion, prénom et nom
                    apparaissent pour chaque action (rédaction, modification, validation, consultation) de votre part sur un traitement.');
                    echo $this->Bs->tag('li', 'information de projet : votre identifiant de connexion, nom, prénom apparaissent dans les informations de rédacteur du projet.');
                    echo $this->Bs->tag('li', 'dans la génération documentaire lorsque des variables correspondant à redacteur_nom (Nom),
                    redacteur_prenom (Prenom), redacteur_email (Email), redacteur_telfixe (Numéro de téléphone fixe), redacteur_telmobile
                    (Numéro de téléphone portable) et redacteur_note (Note) ces données sont utilisées.');
                ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingSix">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                    <?php
                        echo $this->Bs->tag('h4', 'Article 5 - Cookies');
                    ?>
                </a>
            </h4>
        </div>
        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
            <div class="panel-body">
                <?php
                    echo $this->Bs->tag('p', $this->Html->tag('strong', __('5.1  Cookies strictement nécessaires :')));
                    echo $this->Bs->tag('p', 'Ces cookies sont nécessaires au fonctionnement de l\'application et ne peuvent pas être désactivés.');
                    echo $this->Html->tag('br');
                    echo $this->Html->tag('p', 'Nous utilisons un cookie de session qui est utilisé pour stocker des informations de votre
                    la session en cours. Les informations, telles que les données de connexion ou les formulaires en ligne déjà remplis,
                    sont également conservées pendant la session. Toutefois, si vous mettez fin à votre session de navigation ou que votre
                    session expire (30 minutes par défaut), l’identifiant de session et toutes les autres données stockées seront effacés.');
                ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingSeven">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                    <?php
                        echo $this->Bs->tag('h4', 'Article 6 - Droit à l\'oubli');
                    ?>
                </a>
            </h4>
        </div>
        <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
            <div class="panel-body">
                <?php
                    echo $this->Bs->tag('p', 'Sur votre demande ou à la demande de l’administrateur de webdelib, votre compte utilisateur peut être supprimé définitivement.');
                    echo $this->Bs->tag('p', 'La suppression de votre compte laissera les traces de votre activité :');

                    echo $this->Bs->tag('li', 'dans le journal des évenements pendant une durée de 3 mois après la suppression de l\'utilisateur');
                    echo $this->Bs->tag('li', 'dans les documents PDF générés par l\'application, potentiellement');
                    echo $this->Bs->tag('li', 'conservation de l\'activité pour conservation légale 5 ans');
                ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingEight">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                    <?php
                        echo $this->Bs->tag('h4', 'Article 7 - Vos droits sur les données vous concernant');
                    ?>
                </a>
            </h4>
        </div>
        <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
            <div class="panel-body">
                <?php
                    echo $this->Bs->tag('p', 'Vous pouvez accéder et obtenir une copie des données vous concernant, vous
                    opposer au traitement de ces données, les faire rectifier ou les faire effacer.
                    Vous disposez également d\'un droit à la limitation du traitement de vos données.');

                    echo $this->Bs->link( __('https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles'));

                    echo $this->Bs->tag('h3', 'Exercer ses droits');
                    echo $this->Bs->tag('p', 'Pour exercer vos droits sur les données personnelles vous concernant, vous
                    pouvez contacter le Délégué à la Protection des Données (DPO), à l’adresse suivante :');
                    echo $this->Bs->tag('p', $collectivite['dpo_email']);

                    echo $this->Bs->tag('h3', 'Réclamation (plainte) auprès de la CNIL');
                    echo $this->Bs->tag('p', 'Si vous estimez, après nous avoir contacté, que vos droits sur vos données ne sont pas respectés, vous pouvez : ');
                    echo $this->Bs->link(__('adresser une réclamation (plainte) à la CNIL'), 'https://www.cnil.fr/fr/plaintes');
                ?>
            </div>
        </div>
    </div>
</div>


