<?php

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Collectivité'), ['controller' => 'collectivites', 'action' => 'index']);
$this->Html->addCrumb(__('Informations liées à la politique de confidentialité'));

echo $this->Bs->tag('h3', __('Informations liées à la politique de confidentialité'));

echo $this->BsForm->create('Collectivite',[
        'url' => [
            'controller' => 'collectivites',
            'action' => 'rgpd',
        ]
    ]
);

echo $this->Bs->div('panel panel-default') .
    $this->Bs->div('panel-heading', __('Responsable de l\'entité')) .
    $this->Bs->div('panel-body') .
    $this->BsForm->input('Collectivite.responsable_nom', [
            'label' => __('Nom'). ' <abbr title="obligatoire">*</abbr>',
            'type' => 'text']
    ) .
    $this->BsForm->input('Collectivite.responsable_prenom', [
            'label' => __('Prénom'). ' <abbr title="obligatoire">*</abbr>',
            'type' => 'text']
    ) .
    $this->BsForm->input('Collectivite.responsable_fonction', [
            'label' => __('Fonction'). ' <abbr title="obligatoire">*</abbr>',
            'type' => 'text']
    ) .
    $this->BsForm->input('Collectivite.responsable_email', [
            'label' => __('Email'). ' <abbr title="obligatoire">*</abbr>',
            'type' => 'text']
    ) .
    $this->Bs->close(2);

echo $this->Bs->div('panel panel-default') .
    $this->Bs->div('panel-heading', __('Délégué à la Protection des Données de l\'entité')) .
    $this->Bs->div('panel-body') .
    $this->BsForm->input('Collectivite.dpo_nom', [
            'label' => __('Nom'). ' <abbr title="obligatoire">*</abbr>',
            'type' => 'text']
    ) .
    $this->BsForm->input('Collectivite.dpo_prenom', [
            'label' => __('Prénom'). ' <abbr title="obligatoire">*</abbr>',
            'type' => 'text']
    ).
    $this->BsForm->input('Collectivite.dpo_email', [
            'label' => __('Email'). ' <abbr title="obligatoire">*</abbr>',
            'type' => 'text']
    ) .
    $this->Bs->close(2);

echo $this->Bs->div('panel panel-default') .
    $this->Bs->div('panel-heading', __('Hébergement')) .
    $this->Bs->div('panel-body') .
    $this->BsForm->radio('Collectivite.saas', ['1' => __('oui'), '0' => __('non')], ['label' => __('L\'application est-elle hébergée par LIBRICIEL SCOP ?'), 'div' => false, 'value' => $this->html->value('Collectivite.saas')]) .

    $this->Bs->close(2);

echo
    $this->Html2->btnSaveCancel('', $previous) .
    $this->BsForm->end();
