<?php

if (!empty($typeseances)) {
    echo $this->BsForm->select('Typeseance', $typeseances, ['class' => 'selectmultiple', 'label' => __('Types de séance'), 'placeholder' => __('Choisir un type de séance'), 'autocomplete' => 'off', 'multiple' => true]
    );
}
