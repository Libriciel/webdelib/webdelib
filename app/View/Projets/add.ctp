<?php

$this->Html->addCrumb(__('Mes projets'));
$this->Html->addCrumb(__('Nouveau'));

echo $this->Bs->tag('h3', __('Créer un projet'));
echo $this->BsForm->create(false, [
    'id'=> 'appProjet',
    'url' => ['controller' => 'projets', 'action' => 'add'], 'type' => 'post']);

$aTab = [
    'infos' => __('Informations principales'),
    'textes' => __('Textes'),
    'annexes' => __('Annexes')];

if ($infosupdefExist) {
    $aTab['Infos_suppl'] = __('Informations supplémentaires');
}
if (Configure::read('DELIBERATIONS_MULTIPLES')) {
    $aTab['multidelib'] = __('Délibérations rattachée(s)');
}

echo $this->Bs->tab($aTab, ['active' => isset($nameTab) ? $nameTab : 'infos',
    'class' => '-justified']) .
 $this->Bs->tabContent();

echo $this->Bs->tabPane('infos', ['class' => isset($nameTab) ? $nameTab : 'active']);
//$this->BsForm->setLeft(0);
echo $this->Html->tag(null, '<br />') .
 $this->Html->tag('div', null, ['class' => 'well well-lg']) .
 '<b><u>' . __('Rédacteur') . '</u></b> : <i>' . $projet['redacteur']['prenom'] . ' ' . $projet['redacteur']['nom'] . '</i>' .
 $this->Html->tag(null, '<br />') .
 '<b><u>' . __('Service émetteur') . '</u></b> : <i>' . $projet['service']['name'] . '</i>' .
 $this->Bs->close();

echo $this->Bs->row() .
 $this->Bs->col('xs6');
echo $this->BsForm->input('Deliberation.typeacte_id', [
    'type'=>'select',
    'label' => __('Type d\'acte') ,
    'escape' => false,
    'class' => 'selectone',
    'data-placeholder' => __('Sélectionner un type d\'acte'),
    'autocomplete' => 'off',
    'required' => true,
    'empty' => true,
]) .
$this->BsForm->input('Deliberation.objet', [
    'type' => 'textarea',
    'label' => __('Libellé'),
    'rows' => '2',
    'required' => true,
]) .
 $this->BsForm->input('Deliberation.titre', [
    'type' => 'textarea',
    'label' => __('Titre'),
    'rows' => '2'
]) .
 $this->BsForm->input('Deliberation.theme_id', [
    'type'=>'select',
    'label' => __('Thème'),
    'empty' => true,
    'data-placeholder' => __('Sélectionner un thème'),
    'class' => 'selectone',
    'autocomplete' => 'off',
    'required' => true,
    'escape' => false]) ;

//Télétransmissions
echo $this->BsForm->input('Deliberation.num_pref', [
    'type'=>'select',
    'options' => $nomenclatures,
    'label' => __('Classification'),
    'data-placeholder' => __('Sélectionner la classification'),
    'disabled' => empty($nomenclatures),
    'empty' => true,
    'autocomplete' => 'off',
    'class' => 'selectone',
    'escape' => false]).
$this->BsForm->input('Deliberation.typologiepiece_code', [
        'type'=>'select',
        'options' => $typologiepieces,
        'label' => __('Type de pièce principale'),
        'data-placeholder' => __('Sélectionner le type de pièce principale'),
        'disabled' => empty($typologiepieces),
        'empty' => true,
        'autocomplete' => 'off',
        'class' => 'selectone',
        'escape' => false]);

echo $this->BsForm->checkbox('Deliberation.tdt_document_papier', [
                'autocomplete' => 'off',
                'label' => __('Envoi de documents papiers complémentaires'),
                'help'=> __("Cocher la case ci-contre si vous souhaitez envoyer les actes au format papier."),
            ]);
echo $this->BsForm->input(
    'DeliberationUser',
    [
    'type' => 'select',
    'label' => __('Autre(s) rédacteur(s)'),
    'autocomplete' => 'off',
    'help' => __('Ajouter des rédacteurs ayant le droit de modifier et/ou d\'insérer le projet dans un circuit de validation'),
    'class' => 'selectmultiple',
    'data-placeholder'=>__('Sélectionner un ou des rédacteurs supplémentaires'),
    'data-allow-clear'=> true,
    'multiple' => true,
    'empty' => true
  ]
);

if (Configure::read('DELIBERATIONS_MULTIPLES')) {
    echo $this->BsForm->checkbox('Deliberation.is_multidelib', [
    'autocomplete' => 'off',
    'label' => 'Multi-Délibération',
]);
}
echo $this->Bs->close() .
 $this->Bs->col('xs6');

echo $this->BsForm->input('Deliberation.rapporteur_id', [
    'type' => 'select',
    'label' => __('Rapporteur'),
    'class' => 'selectone',
    'data-placeholder'=> __('Sélectionner un rapporteur'),
    'data-allow-clear'=> true,
    'empty' => true]);

echo $this->BsForm->datetimepicker('Deliberation.date_limite', [
    'language' => 'fr',
    'autoclose' => 'true',
    'format' => 'dd/mm/yyyy',
    'minView' => 2], [
    'label' => __('Date limite'),
    'title' => __('Sélectionner une date'),
    'style' => 'cursor:pointer;background-color: white;',
    'help' => __('Cliquez sur le champ ci-dessus pour sélectionner la date'),
    'readonly' => 'readonly'
]);
$selectTypeseances = $this->BsForm->input('DeliberationTypeseance', [
    'type' => 'select',
    'class' => 'selectmultiple',
    'label' => __('Types de séance'),
    //'data-wd-projet-id' => '',
    //'selected'=> $this->Html->value('Typeseance.Typeseance'),
    'data-placeholder' => __('Sélectionner un type de séance'),
    'autocomplete' => 'off',
    'multiple' => 'multiple']).
    ($this->Form->isFieldError('DeliberationTypeseance.DeliberationTypeseance') ? $this->Form->error('DeliberationTypeseance.DeliberationTypeseance') : '');

$selectDatesSeances = $this->BsForm->input('DeliberationSeance', [
    'type' => 'select',
    'class' => 'selectmultiple',
    'label' => __('Dates de séance'),
    //'data-wd-projet-id' => '',
    //'selected'=> $this->Html->value('Seance.Seance'),
    'data-placeholder' => __('Sélectionner une date de séance'),
    'autocomplete' => 'off',
    'multiple' => 'multiple']).
    ($this->Form->isFieldError('DeliberationSeance.DeliberationSeance') ? $this->Form->error('DeliberationSeance.DeliberationSeance') : '');

echo $this->Bs->div('', $selectTypeseances, [
  'id' => 'selectTypeseances',
  'style'=> 'display: none'
]);
echo $this->Bs->div('', $selectDatesSeances, ['
id' => 'selectDatesSeances',
'style'=> 'display: none'
]);
echo $this->Bs->close(2);
echo $this->Bs->tabClose() .
 $this->Bs->tabPaneClose();

echo  $this->Bs->div('btn-group col-md-offset-' . $this->BsForm->getLeft(), null) .
$this->Bs->btn($this->Bs->icon('times-circle') . ' ' . __('Annuler'), 'cancel', [
    'type' => 'default',
//                    'tag'=>'button',
    'id' => 'boutonAnnuler',
    'escapeTitle' => false,
    'title' => __('Annuler les modifications du projet')]) .
$this->Bs->btn($this->Bs->icon('save', null, ['stylePrefix' => 'far']) . ' ' . __('Enregistrer'), null, [
     'value' => 'Save',
     'tag' => 'button',
     'type' => 'primary',
     'option_type' => 'button',
     'class' => 'wd-boutonValider',
     'escapeTitle' => false,
     'title' => "Enregistrer le projet",
     'onclick' => '']).
$this->Bs->btn($this->Bs->icon('save', null, ['stylePrefix' => 'far']) . ' ' . __('Enregistrer et quitter'), null, [
   'value'=>'SaveAndQuit',
    'tag' => 'button',
    'type' => 'success',
    'option_type' => 'button',
    'class' => 'wd-boutonValider',
    'escapeTitle' => false,
    'title' => "Enregistrer et quitter le projet",
    'onclick' => '']) .

    $this->BsForm->hidden('Save', ['id'=>'buttonValueType','value'=>'']).
    $this->Bs->close().
    $this->BsForm->end();

?>

<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {
        $('.wd-boutonValider').on('mousedown',function(){
            $('#buttonValueType').val($(this).val());
        });
        window.app_data = new Object;
        window.app_data.allowedMulti = <?php echo(!empty($typesactemulti) ? json_encode($typesactemulti) : '[]'); ?>;

    });
});
</script>
