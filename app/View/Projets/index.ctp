<?php

$defaultModel=$this->Paginator->defaultModel();
$nbProjets = !empty($defaultModel) ? $this->Paginator->counter('{:count}') : $nbProjets;
$titre = $titreVue . ' (' . $nbProjets . ' ' . ($nbProjets > 1 ? 'projets' : 'projet') . ')';

if (!empty($crumbs)) {
    foreach ($crumbs as $crumb) {
        $this->Html->addCrumb($crumb);
    }
}
echo $this->element('filtre');
echo $this->Bs->tag('h3', $titre);

if (
    $this->permissions->check('Projets', 'create')
    && $this->params['action'] == 'mesProjetsRedaction'
) {
    echo $this->Bs->div('btn-group');
    echo $this->Bs->btn(
        $this->Bs->icon('plus') . ' '
       . __('Créer un projet'),
        ['controller' => 'projets', 'action' => 'add'],
        ['type' => 'primary', 'escapeTitle' => false, 'title' => __('Créer un nouveau projet')
        ]
    );
    echo $this->Bs->close();
}

if (isset($traitement_lot) && ($traitement_lot == true) && $nbProjets > 0) {
    echo $this->Form->create(
        'Deliberation',
        [
            'url' =>
                [
                    'controller' => 'TraitementParLot',
                    'action' => 'index'
                ],
            'type' => 'post',
            'class'=>'waiter'
        ]
    );
}

echo $this->element(
    '9cases',
    [
    'projets' => $this->data,
    'traitement_lot' => isset($traitement_lot) ? $traitement_lot : null
    ]
);

//paginate
echo $this->element('paginator', ['paginator' => $this->Paginator]);

echo $this->element(
    'Projet/traitement_lot',
    [
        'modeles' => isset($modeles) ? $modeles : null,
        'traitement_lot' => isset($traitement_lot) ? $traitement_lot : null
    ]
);
