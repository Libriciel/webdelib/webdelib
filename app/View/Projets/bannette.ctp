<?php

//echo $this->Html->tag('div', null, array('class' => 'ouvrable', 'id' => $titreVue));
//echo $this->Html->tag('h2', "$titreVue $nb");
$nbProjets = isset($this->Paginator) ? $this->Paginator->counter('{:count}') : $nbProjets;

echo $this->Bs->div('panel panel-primary');
echo $this->Bs->div('panel-heading', $this->Bs->row() .
        $this->Bs->col('xs8') .
        $this->Bs->tag('h4', $titreVue . ' (' . $nbProjets . ' ' . ($nbProjets > 1 ? 'projets' : 'projet') . ')'
        ) .
        $this->Bs->close() .
        $this->Bs->col('xs4') .
        $this->Bs->tag('p',
            $this->Bs->btn($this->Bs->icon('list-ul') .' '. __('Visualiser le contenu de la bannette'),
                [
                    'controller' => $this->request['controller'],
                    'action' => $this->request['action']
                ],
                [
                    'type' => 'default',
                    'escapeTitle' => false,
                ]
            ),
            ['class' => 'text-right']) .
        $this->Bs->close(2)
);

echo $this->element('9cases', ['projets' => $this->data]);
echo $this->Bs->close();