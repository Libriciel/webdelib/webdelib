<?php
//FIL D'ARIANE
$this->Html->addCrumb(__('Mes projets'));
$this->Html->addCrumb(__('Modification d\'un projet'));


$title =  __('Modifier le projet') . ' : ' . $this->Html->value('Deliberation.id') ;
echo $this->Bs->tag('h3', $title) .
 $this->BsForm->create(false, [
    'id'=> 'appProjet',
    'js-app' => 'appEditWopi',
    'url' => array_merge((!empty($nameTab) ? ['nameTab' => $nameTab] : []), [
        'controller' => 'projets',
        'action' => 'edit', $this->Html->value('Deliberation.id')]),
    'type' => 'file',
    'novalidate' => true]);

$aTab = ['infos' => __('Informations principales')];
if (!empty($projet_textes_active)) {
    $aTab['textes'] = __('Textes');
}
$aTab['annexes'] = __('Annexes');
if (!empty($infosupdefs)) {
    $aTab['Infos_suppl'] = __('Informations supplémentaires');
}
if (Configure::read('DELIBERATIONS_MULTIPLES')) {
    $aTab['multidelib'] = __('Délibérations rattachée(s)'); //'style' => 'display: none'
}

echo $this->Bs->tab($aTab, ['active' => isset($nameTab) ? $nameTab : 'infos',
    'class' => '-justified']) .
 $this->Bs->tabContent();

// information principales
echo $this->Bs->tabPane('infos', ['class' => isset($nameTab) ? $nameTab : 'active']);
//$this->BsForm->setLeft(0);
echo $this->Html->tag(null, '<br />') .
 $this->Html->tag('div', null, ['class' => 'well well-lg']) .
 $this->Bs->row() . $this->Bs->col('xs6') .
 '<b><u>' . __('Service émetteur') . '</u></b> : <i>' . $projet['service']['name'] . '</i>' .
 $this->Html->tag(null, '<br />') .
 '<b><u>' . __('Rédacteur') . '</u></b> : <i>' . $projet['redacteur']['prenom'] . ' ' . $projet['redacteur']['nom']. '</i>' .
 $this->Bs->close() .
 $this->Bs->col('xs6') .
 '<b><u>' . __('Date création') . '</u></b> : <i>' . $this->Time->i18nFormat($projet['created'], '%d/%m/%Y à %k:%M') . '</i>' .
 $this->Html->tag(null, '<br />') .
 '<b><u>' . __('Date de modification') . '</u></b> : <i>' . $this->Time->i18nFormat($projet['modified'], '%d/%m/%Y à %k:%M') . '</i>' .
 $this->Bs->close(3);


echo $this->Bs->row() .
 $this->Bs->col('xs6');
echo $this->BsForm->input('Deliberation.typeacte_id', [
    'type' => 'select',
    'label' => 'Type d\'acte',
    'autocomplete' => 'off',
    'escape' => false,
    //'disabled' => true,
    'class' => 'selectone',
    'required' => true,
    'empty' => false
]);
echo $this->BsForm->input('Deliberation.objet', [
    'type' => 'textarea',
    'label' => __('Libellé'),
    'maxlength' => '500',
    'data-wd-libelle' => 'deliberation',
    'rows' => '2',
    'required' => true,
])  .
 $this->BsForm->input('Deliberation.titre', [
    'type' => 'textarea',
    'label' => __('Titre'),
    'maxlength' => '1000',
    'rows' => '2'
]) .
 $this->BsForm->select('Deliberation.theme_id', $themes, [
    'label' => __('Thème'),
    'data-allow-clear' => false,
    'escape'=> false,
    'required' => true,
    'class' => 'selectone']);

echo $this->BsForm->select('Deliberation.num_pref', $nomenclatures, [
    'label' => __('Classification'),
    'data-placeholder' => __('Sélectionner la classification'),
    'disabled' => empty($nomenclatures),
    'autocomplete' => 'off',
    'value' => $this->Html->value('Deliberation.num_pref'),
    'empty' => true,
    'class' => 'selectone',
    'escape' => false]).
$this->BsForm->input('Deliberation.typologiepiece_code', [
            'type'=>'select',
            'options' => $typologiepieces,
            'label' => __('Type de pièce principale'),
            'data-placeholder' => __('Sélectionner le type de pièce principale'),
            //'data-allow-clear' => true,
            'disabled' => empty($typologiepieces),
            'empty' => true,
            'class' => 'selectone',
            'escape' => false]);

            echo $this->BsForm->checkbox('Deliberation.tdt_document_papier', [
                    'autocomplete' => 'off',
                    'label' => __('Envoi de documents papiers complémentaires'),
                    'help'=> __("Attention en cochant cette case la transaction ne sera alors acquittée par la Préfecture qu'à réception des documents papiers."),
                ]);

echo $this->BsForm->input(
    'DeliberationUser',
    [
    'type' => 'select',
    'label' => __('Autre(s) rédacteur(s)'),
    'autocomplete' => 'off',
    'value' => $this->Html->value('DeliberationUser'),
    'disabled'=> $redacteur_disable,
    'help' => __('Ajouter des rédacteurs ayant le droit de modifier et/ou d\'insérer le projet dans un circuit de validation'),
    'class' => 'selectmultiple',
    'data-placeholder' => __('Sélectionner un ou des rédacteurs supplémentaires'),
    'data-allow-clear' => true,
    'multiple' => true,
    'empty' => true
  ]
);

if (Configure::read('DELIBERATIONS_MULTIPLES')) {
    echo $this->BsForm->checkbox('Deliberation.is_multidelib', [
        'autocomplete' => 'off',
        'readonly' => !empty($this->data['Multidelib']) ? true : false,
        'label' => __('Multi-Délibération')
    ]);
}
echo $this->Bs->close() .
 $this->Bs->col('xs6');
echo $this->BsForm->select('Deliberation.rapporteur_id', $rapporteurs, [
    'label' => __('Rapporteur'),
    'class' => 'selectone',
    'data-placeholder'=>__('Sélectionner un rapporteur'),
    'data-allow-clear'=> true,
    'empty' => true]) .
 $this->BsForm->datetimepicker(
     'Deliberation.date_limite',
     [
  //  'addon' => $this->Bs->icon('trash'),
    'autoclose' => 'true',
    'format' => 'dd/mm/yyyy',
    'minView' => 2],
     [
    'label' => __('Date limite'),
    'title' => __('Sélectionner une date'),
    'style' => 'cursor:pointer;background-color: white;',
    'help' => __('Cliquez sur le champ ci-dessus pour sélectionner la date'),
    //'readonly' => 'readonly',
    'value' => $this->Html->value('Deliberation.date_limite')
        ]
 );

$selectTypeseances = $this->BsForm->input(
    'DeliberationTypeseance',
    [
    'type'=> 'select',
    'class' => 'selectmultiple',
    'label' => __('Types de séance'),
  //  'error' => ['typeseance_id' => __('Vous ne pouvez affecter le projet qu\'à un seule séance de type délibérante')],
    // 'errorMessage' => 'eee',
    //'name'=> 'Typeseance.id',
    'data-placeholder' => __('Sélectionner un type de séance'),
    'autocomplete' => 'off',
    'multiple' => 'multiple']
).($this->Form->isFieldError('DeliberationTypeseance.DeliberationTypeseance') ? $this->Form->error('DeliberationTypeseance.DeliberationTypeseance') : '');


$selectDatesSeances = $this->BsForm->input('DeliberationSeance', [
    'type'=> 'select',
    'class' => 'selectmultiple',
    'label' => __('Dates de séance'),
    'data-placeholder' => __('Sélectionner une date de séance'),
    'autocomplete' => 'off',
    'multiple' => 'multiple']).
    ($this->Form->isFieldError('DeliberationSeance.DeliberationSeance') ? $this->Form->error('DeliberationSeance.DeliberationSeance') : '');

echo $this->Bs->div('', $selectTypeseances, ['id' => 'selectTypeseances','style'=> 'display: none']);
echo $this->Bs->div('', $selectDatesSeances, ['id' => 'selectDatesSeances','style'=> 'display: none']);
echo $this->Bs->close(2);
echo $this->Bs->tabClose();

if (!empty($projet_textes_active)) {
    echo $this->Bs->tabPane('textes', ['class' => (isset($nameTab) && $nameTab == 'textes' ? 'active' : '')]);
    echo $this->Html->tag(null, '<br />');
    echo $this->element('texte', ['type' => 'texte_projet']);
    echo $this->element('texte', ['type' => 'texte_synthese']);
    echo '<div id="texteDelibOngletTextes"><div id="texteDeliberation">';
    echo $this->element('texte', ['type' => 'deliberation']);
    echo '</div></div>';
    echo $this->Html->tag('span', __('Note : les modifications apportées ici ne prendront effet que lors de la sauvegarde du projet.'), ['class' => 'help-block']);

    echo $this->Bs->tabClose();
}

echo $this->Bs->tabPane('annexes', ['class' => (isset($nameTab) && $nameTab == 'annexes' ? 'active' : '')]);
echo $this->Html->tag('br') .
 '<div id="DelibOngletAnnexes"><div id="DelibPrincipaleAnnexes">';
echo $this->element('annexe_edit', [
  'ref' => 'delibPrincipale',
  'id' => $this->Html->value('Deliberation.id'),
  'annexes' => $this->Html->value('Annexe')
]);
echo '</div></div>';
echo $this->Html->tag('span', __('Note : les modifications apportées ici ne prendront effet que lors de la sauvegarde du projet.'), ['class' => 'help-block']);

echo $this->Bs->tabClose();

// informatins supplémentaires
if (!empty($infosupdefs)) {
    echo $this->Bs->tabPane('Infos_suppl', ['class' => (isset($nameTab) && $nameTab == 'Infos_suppl' ? 'active' : '')]);
    echo $this->Html->tag('br');
    echo $this->element('infosup_edit', ['infosupdefs' => $infosupdefs]);
    echo $this->Bs->tabClose();
}

// DELIBERATIONS MULTIPLES
if (Configure::read('DELIBERATIONS_MULTIPLES')) {
    echo $this->Bs->tabPane('multidelib', ['class' => (isset($nameTab) && $nameTab == 'multidelib' ? 'active' : '')]);
    echo $this->Html->tag('br /');

    if (empty($this->data['Deliberation']['parent_id'])) {
        echo $this->element('Deliberations/Multidelib/index');
    } else {
        echo $this->Html->tag('strong', __('Délibération parent : ') . $this->data['Deliberation']['parent_id']);
        echo '<div class="btn-group">';
        echo $this->Html->link($this->Bs->icon('search'). ' ' . 'Visualiser', ['action' => 'view', $this->data['Deliberation']['parent_id']], ['escape' => false, 'class' => 'btn btn-mini']);
        echo $this->Html->link($this->Bs->icon('pencil-alt'). ' ' . 'Modifier', ['action' => 'edit', $this->data['Deliberation']['parent_id']], ['escape' => false, 'class' => 'btn btn-mini']);
        echo '</div>';
        echo '<div class="spacer"></div>';
    }
    echo $this->Bs->tabClose();
}

echo $this->Bs->tabPaneClose();
echo $this->Bs->close();
echo $this->Bs->tag('br');
echo $this->Form->hidden('Deliberation.id');
/*echo $this->Html2->btnSaveCancel('','cancel');*/
echo  $this->Bs->div('btn-group col-md-offset-' . $this->BsForm->getLeft(), null) .
                $this->Bs->btn($this->Bs->icon('times-circle') . ' ' . __('Annuler'), 'cancel', [
                    'type' => 'default',
//                    'tag'=>'button',
                    'id' => 'boutonAnnuler',
                    'escapeTitle' => false,
                    'title' => __('Annuler les modifications du projet')]) .
                $this->Bs->btn($this->Bs->icon('save') . ' ' . __('Enregistrer'), null, [
                    'name'=>'Save',
                    'value'=>'Save',
                     'tag' => 'button',
                     'type' => 'primary',
                     'option_type' => 'button',
                     'class' => 'wd-boutonValider',
                     'escapeTitle' => false,
                     'title' => "Enregistrer le projet",
                     'onclick' => '']) .
               $this->Bs->btn($this->Bs->icon('save') . ' ' . __('Enregistrer et quitter'), null, [
                   'name'=>'Save',
                   'value'=>'SaveAndQuit',
                    'tag' => 'button',
                    'type' => 'success',
                    'option_type' => 'button',
                    'class' => 'wd-boutonValider',
                    'escapeTitle' => false,
                    'title' => "Enregistrer et quitter le projet",
                    'onclick' => '']) .

                $this->Bs->close().
                $this->BsForm->hidden('Save', ['id'=>'buttonValueType','value'=>'']).
                $this->BsForm->end();

echo $this->BsForm->end();

echo $this->element('editTextWopiModal');
?>
<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {

        $('.wd-boutonValider').on('mousedown',function(){
            $('#buttonValueType').val($(this).val());
        });

    window.app_data = new Object;
    window.app_data.gabarits = <?php echo(!empty($gabarits_acte) ? json_encode($gabarits_acte) : '[]'); ?>;
    window.app_data.extensions = <?php echo(!empty($extensions) ? json_encode($extensions) : '[]'); ?>;
    window.app_data.extensionsFusion = <?php echo(!empty($extensionsFusion) ? json_encode($extensionsFusion) : '[]'); ?>;
    window.app_data.extensionsCtrl = <?php echo(!empty($extensionsCtrl) ? json_encode($extensionsCtrl) : '[]'); ?>;

    var current_gabarit_name;
        // Gestion des gabarits selon le type d'acte
        current_gabarit_name = window.app_data.gabarits[$('#listeTypeactesId').val()];
        $('#listeTypeactesId').change(function () {
            current_gabarit_name = window.app_data.gabarits[$('#listeTypeactesId').val()];
            //Le type d'acte possède un gabarit de texte d'acte
            if (current_gabarit_name != undefined) {
                $('#ajouteMultiDelib .gabarit_name_multidelib').text(current_gabarit_name);
                $('#ajouteMultiDelib .MultidelibGabaritBloc').show();
                $('#ajouteMultiDelib .texte_acte_multidelib').each(function () {
                    //Le formulaire d'upload est vide
                    if ($(this).val() == '') {
                        //Désactivation et masquage du champ d'upload
                        $(this).prop('disabled', true).hide();
                        $(this).closest('.delibRattachee').find('.MultidelibGabaritBloc').show();
                        $(this).closest('.delibRattachee').find('input.gabarit_acte_multidelib').prop('disabled', false);
                    } else { //Activation du formulaire d'upload non vide
                        $(this).prop('disabled', false).show();
                        $(this).closest('.delibRattachee').find('input.gabarit_acte_multidelib').prop('disabled', true);
                        $(this).closest('.delibRattachee').find('.MultidelibGabaritBloc').hide();
                    }
                });
            } else { // Pas de gabarit associé au type d'acte
                $('#ajouteMultiDelib .MultidelibGabaritBloc').hide();
                $('#ajouteMultiDelib .delibRattachee input.gabarit_acte_multidelib').prop('disabled', true);
                $('#ajouteMultiDelib .texte_acte_multidelib').prop('disabled', false).show();
            }
        });
    });
});
</script>
