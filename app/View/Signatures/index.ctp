<?php

$this->Html->addCrumb(__('Séances à traiter'), ['controller' => 'seances', 'action' => 'index']);
$this->Html->addCrumb(__('Signature des délibérations'));

echo $this->element('filtre');

if (!empty($seance_id) && !empty($deliberations)) {
    echo $this->BsForm->create('Deliberation', [
        'url' => ['controller' => 'signatures', 'action' => 'index', $seance_id],
        'class' => 'waiter form-inline',
        'data-waiter-title'=>__('Signature en cours'),
    ]);
}

$titles = [];
if (!empty($seance_id)) {
    echo $this->Bs->tag('h3', __('Signature des délibérations'));
    $titles[] = ['title' => $this->BsForm->checkbox(null, [
            'id' => 'masterCheckbox',
            'label' => '',
            'inline' => true
    ])];
} else {
    echo $this->Bs->tag('h3', __('Suivi des délibérations envoyées en signature'));
    $titles[] = ['title' => __('Date de signature')];
}
$titles[] = ['title' => 'Ordre'];
$titles[] = ['title' => 'Id.'];
$titles[] = ['title' => 'État'];
$titles[] = ['title' => 'N° Acte'];
$titles[] = ['title' => __('Libellé de l\'acte')];
$titles[] = ['title' => __('Bordereau')];
$titles[] = ['title' => __('État signature')];
$titles[] = ['title' => __('Actions')];

echo $this->Bs->table($titles, ['drop', 'hover', 'striped']);

foreach ($deliberations as $delib) {
    $options = [];
    if ($seance_id !== null) {
        if (empty($delib['Deliberation']['signee']) && in_array($delib['Deliberation']['parapheur_etat'], [null, 0, -1], true) && in_array($delib['Deliberation']['etat'], [3, 4,10], true)
        ) {
            $options['checked'] = true;
        } else {
            $options['disabled'] = true;
        }
        echo $this->Bs->cell($this->Form->checkbox('Deliberation.' . $delib['Deliberation']['id'] . '.is_checked', $options));
    }

    if (!empty($seance_id)) {
        echo $this->Bs->cell($delib['DeliberationSeance']['position']);
    } else {
        echo $this->Bs->cell(!empty($delib['Deliberation']['date_acte']) ? $this->Time->i18nFormat($delib['Deliberation']['date_acte'], '%e %B %Y') . __(' à ') . $this->Time->i18nFormat($delib['Deliberation']['date_acte'], '%kh%M') : '');
    }

    echo $this->Bs->cell($this->Html->link($delib['Deliberation']['id'], [
            'controller' => 'projets',
            'action' => 'view', $delib['Deliberation']['id']]
    ));

    $etat = $this->Bs->btn(
        $this->Bs->icon(
            $this->ProjetUtil->etat_icon($delib['Deliberation']['iconeEtat']['image']),
            ['xs','iconType' =>  $this->ProjetUtil->etat_iconType($delib['Deliberation']['iconeEtat']['image'])]
        ),
        null,
        [
            'tag' => 'button',
            'type' => empty($delib['Deliberation']['iconeEtat']['status']) ? $this->ProjetUtil->etat_icon_type($delib['Deliberation']['iconeEtat']['image']) : $delib['Deliberation']['iconeEtat']['status'],
            'size' => 'xs',
            'option_type' => 'button',
            //'disabled'=>'disabled',
            'data-toggle' => 'popover',
            'data-content' => $this->ProjetUtil->etat_icon_help($delib['Deliberation']['iconeEtat']['image']),
            'data-placement' => 'right',
            'escape' => false,
            'title' => $delib['Deliberation']['iconeEtat']['titre']
        ]
    );
    echo $this->Bs->cell($etat);

    echo $this->Bs->cell($delib['Deliberation']['num_delib']);

    echo $this->Bs->cell($delib['Deliberation']['objet_delib']);
    echo $this->Bs->cell(!empty($delib['Deliberation']['parapheur_bordereau']) ?
                       $this->Html->link($this->Bs->icon('file-pdf', null, ['stylePrefix' => 'far']) . ' ' . __('Bordereau de signature'), [
                           'controller' => 'actes','action' => 'downloadBordereau', $delib['Deliberation']['id']], [
                           'escapeTitle' => false,
                           'title' => __('Télécharger le bordereau de signature'),
                           'style' => 'text-decoration: none']) : __('Non disponible'));
    $status ="";
    switch ($delib['Deliberation']['parapheur_etat']) {
        case -1:
            $signature_etat = $this->Bs->icon('exclamation-triangle') . ' ' . $delib['Deliberation']['parapheur_commentaire'] . '"></i>&nbsp;' . __('Refusé par le parapheur');
            break;
        case 1:
            $signature_etat = $this->Bs->icon('clock') . ' ' . __('En cours de signature');
            break;
        case 2:
            $signature = '';
            if (!empty($delib['Deliberation']['signee'])) {
                if (!empty($delib['Deliberation']['signature_type']) && $delib['Deliberation']['signature_type']==='PAdES') {
                    $signature = __('(Signée PAdES)');
                } elseif (!empty($delib['Deliberation']['signature'])) {
                    $signature = '('.$this->Html->link($this->Bs->icon('file-archive') . ' ' . __('Signée PKCS#7'), ['controller' => 'actes', 'action' => 'downloadSignature', $delib['Deliberation']['id']], [
                            'title' => __('Télécharger les signatures'),
                            'style' => 'text-decoration: none',
                            'escapeTitle' => false]).')';
                } else {
                    $signature = __('(Visa)');
                }
            }
            $signature_etat = $this->Bs->icon('award') . ' ' . __('Signé électroniquement') . ' '. $signature;
            break;
        default: //0 ou null
            if (!empty($delib['Deliberation']['signee'])) {
                $signature_etat = $this->Bs->icon('award') . ' ' . __('Signature manuscrite');
            } elseif ($delib['Deliberation']['etat']===10) {
                $signature_etat = __('À renvoyer');
            } else {
                $signature_etat = __('Non envoyé');
            }
    }

    echo $this->Bs->cell($signature_etat);
    //Représente une liste d'actions dans une cellule du tableau
    echo $this->element('Deliberations/deliberation_actions_signature_sendToParapheur', [
      'type' => 'deliberation',
      'deliberation' => $delib
    ]);
}

echo $this->Bs->endTable();

if (!empty($seance_id) && !empty($deliberations)) {
    $this->BsForm->setLeft(0);
    $this->BsForm->setRight(12);
    $actions = [];

    isset($circuits['Standard']) ? $actions['signatureManuscrite'] = __('Signature manuscrite'):null;
    isset($circuits['Parapheur']) ? $actions['signatureElectronique'] = __('Signature électronique'):null;

    echo $this->element('Projet/traitement_lot', [
            'traitement_lot' => true,
            'actions_possibles' => $actions
        ]) .

        $this->Bs->close();

    echo $this->BsForm->end();

    echo $this->Html->tag('br');
}
echo $this->Html2->btnCancel($previous);
?>
<script type="text/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {
        $(":checkbox").change(function(){
            if ($('input[type="checkbox"]:checked').length > 0) {
                $('#CircuitsSignature').slideDown();
            } else {
                $('#CircuitsSignature').slideUp();
            }
        });
    });
});
</script>
