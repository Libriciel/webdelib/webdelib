<h2><?php echo __('Liste des fichiers générés'); ?></h2>
<script>
    $("#pourcentage").hide();
    $("#progrbar").hide();
    $("#affiche").hide();
    $("#contTemp").hide();
</script>
<?php
if (!empty($listFiles)) {
    foreach ($listFiles as $path => $name) {
        if ($name != 'Documents.zip') {
            $filename = end(explode('/', $path . '.' . $format));
            echo $name . ' : <strong>' . $filename . '</strong> ';
            if ($format == 'pdf') {
                echo $this->Html->link('[Visualiser]', $path . ".$format", ['target' => '_blank', 'style' => 'font-weight: bold', 'title' => __('Visualiser le document dans votre navigateur')]);
            }

            echo '&nbsp;';
            echo $this->Html->link(__('[Télécharger]'), $path . ".$format", ['download' => $filename, 'style' => 'font-weight: bold', 'title' => __('Télécharger le fichier sur votre disque')]);
        } else {
            echo $this->Html->link($name, $path);
        }
    }
    echo $this->Html->tag('br');
    echo $this->Html->tag('br');
    echo $this->Html->tag('p', __('Attention :lLe fichier généré deviendra inaccessible au prochain changement de page. Pensez à l\'enregistrer.'));
} else {
    echo '<strong>' . __('L\'accès au fichier généré a expiré ou une erreur s\'est produite, veuillez recommencer la génération.') . '</strong>';
}
echo '<br /><br />';
if (empty($urlRetour) || strpos($urlRetour, "multiodj")) {
    echo $this->Html->link('<i class="fa fa-arrow-left"></i> ' . __('Retour'), '/', ['class' => 'btn', 'escape' => false]);
} else {
    echo $this->Html->link('<i class="fa fa-arrow-left"></i> ' . __('Retour'), $urlRetour, ['class' => 'btn', 'escape' => false]);
}