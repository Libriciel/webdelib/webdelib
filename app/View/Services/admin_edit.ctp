<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Utilisateurs'));
$this->Html->addCrumb(__('Services'), ['action' => 'index']);

if ($this->request->prefix == 'admin') {
    $editAction = ['controller' => 'services', 'action' => 'admin_edit', 'type' => 'post'];
    $listAction = Router::url(['admin' => true, 'prefix' => 'admin', 'controller' => 'services', 'action' => 'index']);
} else {
    $editAction = ['controller' => 'services', 'action' => 'manager_edit', 'type' => 'post'];
    $listAction = Router::url(['controller' => 'services', 'action' => 'index']);
}

$this->Html->addCrumb(__('Modification d\'un service'));
echo $this->Bs->tag('h3', __('Modifier le service : %s', $this->Html->value('Service.name')));

echo $this->BsForm->create('Service', ['url' => $editAction]);
echo $this->BsForm->input('Service.name', ['label' => __('Libellé')]);

echo $this->BsForm->select('Service.parent_id', $services, [
    'label' => __('Appartient à'),
        'placeholder' => __('Choisir un service'),
        'empty' => true,
        'autocomplete' => 'off',
        'data-allow-clear' => true,
        'data-placeholder' => __('Sélectionner un service parent'),
        'escape' => false,
        'class' => 'selectone'
    ]
);

echo $this->BsForm->select('Service.circuit_defaut_id', $circuits, [
    'placeholder' => __('Choisir un circuit par défaut'),
        'label' => __('Circuit par défaut'),
        'data-allow-clear' => true,
        'data-placeholder' => __('Sélectionner un circuit par défaut'),
        'empty' => true,
        'autocomplete' => 'off', 'class' => 'selectone']
);
echo $this->BsForm->input('Service.order', ['label' => __('Critère de tri')]);

echo $this->BsForm->hidden('Service.id');
echo $this->Html2->btnSaveCancel(null, $previous);
echo $this->BsForm->end();