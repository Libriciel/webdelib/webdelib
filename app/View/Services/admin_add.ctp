<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Utilisateurs'));

if ($this->request->prefix == 'admin') {
    $addAction = ['controller' => 'services', 'action' => 'admin_add', 'type' => 'post'];
    $listAction = Router::url(['admin' => true, 'prefix' => 'admin', 'controller' => 'services', 'action' => 'index']);
} else {
    $addAction = ['controller' => 'services', 'action' => 'manager_add', 'type' => 'post'];
    $listAction = Router::url(['controller' => 'services', 'action' => 'index']);
}

$this->Html->addCrumb(__('Services'), $listAction);
$this->Html->addCrumb(__('Ajout d\'un service'));
echo $this->Bs->tag('h3', __('Ajouter un service'));

echo $this->BsForm->create('Service', ['url' => $addAction]);
echo $this->BsForm->input('Service.name', ['label' => __('Libellé') . ' <abbr title="obligatoire">*</abbr>']);
echo $this->BsForm->select('Service.parent_id', $services, [
        'label' => __('Appartient à'),
        'placeholder' => __('Choisir un service'),
        'data-allow-clear' => true,
        'data-placeholder' => __('Sélectionner un service parent'),
        'autocomplete' => 'off',
        'empty' => true,
        'escape' => false,
        'class' => 'selectone'
    ]
);

echo $this->BsForm->select('Service.circuit_defaut_id', $circuits, [
    'placeholder' => __('Choisir un circuit par défaut'),
        'label' => __('Circuit par défaut'),
        'data-placeholder' => __('Sélectionner un circuit par défaut'),
        'empty' => true,
        'class' => 'selectone'
    ]
);
echo $this->BsForm->input('Service.order', [
        'label' => __('Critère de tri')
    ]
);

echo $this->Html2->btnSaveCancel(null, $previous);
echo $this->BsForm->end();
