<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Utilisateurs'));
//FIX
if ($this->request->prefix == 'admin') {
    $editAction = Router::url(['admin' => true, 'prefix' => 'admin', 'controller' => 'services', 'action' => 'edit']);
    $fusionAction = Router::url(['admin' => true, 'prefix' => 'admin', 'controller' => 'services', 'action' => 'fusionner']);
    $deleteAction = Router::url(['admin' => true, 'prefix' => 'admin', 'controller' => 'services', 'action' => 'delete']);
    $addAction = Router::url(['admin' => true, 'prefix' => 'admin', 'controller' => 'services', 'action' => 'add']);
} else {
    $editAction = Router::url(['controller' => 'services', 'action' => 'edit']);
    $fusionAction = Router::url(['controller' => 'services', 'action' => 'fusionner']);
    $deleteAction = Router::url(['controller' => 'services', 'action' => 'delete']);
    $addAction = Router::url(['controller' => 'services', 'action' => 'add']);
}
$this->Html->addCrumb(__('Services'));

echo $this->Bs->tag('h3', __('Liste des services'));

echo $this->Bs->div('btn-group', null, ['role' => "group"]);
$this->BsForm->setLeft(0);
$this->BsForm->setRight(12);
//$this->BsForm->setFormType('inline');
// Rechercher
echo $this->BsForm->inputGroup('search_tree', [['content' => $this->Bs->icon('filter'), 'id' => 'search_tree_button', 'title' => __('Filtrer par nom de service'), 'type' => 'button', 'state' => 'primary'], ['content' => $this->Bs->icon('sliders-h') . ' <span class="caret"></span>', 'class' => 'dropdown-toggle', 'title' => __('Options de recherche'), 'data-toggle' => 'dropdown', 'after' => '<ul class="dropdown-menu dropdown-menu-right" role="menu">
            <li><a id="search_tree_erase_button" title="' . __('Remettre la recherche à zéro') . '">' . __('Effacer la recherche') . '</a></li>
            <li class="divider"></li>
            <li><a id="search_tree_plier_button" title="' . __('Replier tous les services') . '">' . __('Tout replier') . '</i></a></li>
            <li><a id="search_tree_deplier_button" title="' . __('Déplier tous les services') . '">' . __('Tout déplier') . '</a></li>
        </ul>', 'type' => 'submit', 'state' => 'default']], ['class' => 'form-control', 'div' => false, 'placeholder' => __('Filtrer par nom de service')], ['multiple' => true, 'side' => 'right']
);
echo $this->Bs->close();
echo $this->Bs->tag('br /');
echo $this->Bs->tag('br /');

echo $this->Bs->div('btn-toolbar', null, ['role' => "toolbar"]);
if ($this->permissions->check('Services', 'create')) {
    // Nouveau
    echo $this->Bs->div('btn-group', null, ['role' => "group"]);
    echo $this->Bs->btn($this->Bs->icon('plus-circle') . ' ' . __('Ajouter un service'), $addAction, ['escape' => false, 'title' => __('Ajouter un nouveau service'), 'type' => 'primary', 'id' => 'boutonAdd']);
    echo $this->Bs->close();
}

if ($this->permissions->check('Services', 'update')) {
    // Modifier
    echo $this->Bs->div('btn-group', null, ['role' => "group"]);
    echo $this->Bs->btn($this->Bs->icon('pencil-alt') . ' ' . __('Modifier'), 'javascript:void(0);', ['escape' => false, 'title' => __('Modifier le service'), 'type' => 'primary', 'id' => 'boutonEdit']);
    echo $this->Bs->close();

    // Fusionner
    echo $this->Bs->div('btn-group', null, ['role' => "group"]);
    echo $this->Bs->confirm('<span class="ls-fusion"></span>'.
        __(' Fusionner'), $fusionAction, ['type' => 'success', 'title' => __('Fusionner le service'), 'texte' => $this->BsForm->create(false, ['url' => $fusionAction, 'novalidate' => true])
        . $this->Bs->tag('p', __('Si vous ne souhaitez pas fusionner, fermez la boite de dialogue.'))
        . $this->Bs->tag('p', __('Avec quel service voulez-vous fusionner ?'))
        . $this->BsForm->select('Service.id', $allServices, ['data-placeholder' => __('Sélectionner un service'), 'label' => false, 'empty' => true, 'escape' => false, 'class' => 'app-selectService'])
        . $this->BsForm->hidden('service_a_fusionner')
        . $this->BsForm->end(), 'header' => __('Vous allez fusionner un service !'), 'style' => ['border-bottom-right-radius' => '4px', 'border-top-right-radius' => '4px'], 'id' => 'boutonFusion'], ['id' => 'modalFusion', 'form' => true]);
    echo $this->Bs->close();
}

if ($this->permissions->check('Services', 'delete')) {
    // Supprimer
    echo $this->Bs->div('btn-group', null, ['role' => "group"]);
    echo $this->Bs->btn($this->Bs->icon('trash') . ' ' . __('Supprimer'), $deleteAction, [
            'escape' => false,
        'title' => __('Supprimer le service'),
        'type' => 'danger',
        'id' => 'boutonDelete',
        'confirm' => __('Voulez-vous vraiment supprimer le service ?')]
    );
    echo $this->Bs->close(2);
}

// Tree
echo $this->Bs->tag('/br');
$selectedServices = !empty($selectedServices) ? $selectedServices : [];
echo $this->Bs->div(null, $this->Tree->generateIndex($services, 'Service', []), ['id' => 'arbre']);
$options = [];
if (!empty($selectedServices)) {
    $options['value'] = implode(',', $selectedServices);
} elseif ($this->Html->value('Service.Service')) {
    $options['value'] = implode(',', $this->Html->value('Service.Service'));
}
echo $this->BsForm->hidden('Service.Service', $options)
 . $this->BsForm->error('User.Service', __('Sélectionnez un ou plusieurs services')) .
 $this->Bs->close(4);
?>
<script>
require(['domReady'], function (domReady) {
    domReady(function () {


        $(".app-selectService").select2({
            dropdownParent: $("#modalFusion"),
            templateSelection: function (object) {
                // trim sur la sélection (affichage en arbre)
                return $.trim(object.text);
            }
        });

        var jstreeconf = {
            /* Initialisation de jstree sur la liste des services */
            "core": {//Paramétrage du coeur du plugin
                "multiple": false,
                "themes": {"stripes": true}     //Une ligne sur deux est grise (meilleure lisibilité)
            },
            "checkbox": {//Paramétrage du plugin checkbox
                "three_state": false            //Ne pas propager la séléction parent/enfants
            },
            "search": {//Paramétrage du plugin de recherche
                "fuzzy": false, //Indicates if the search should be fuzzy or not (should chnd3 match child node 3).
                "show_only_matches": true, //Masque les résultats ne correspondant pas
                "case_sensitive": false         //Sensibilité à la casse
            },
            "types": {
                "level0": {
                    "icon": "fa fa-sitemap"
                },
                "default": {
                    "icon": "fa fa-users"
                }
            },
            "contextmenu": {
                "items": contextMenu
            },
            "plugins": [
                "checkbox", //Affiche les checkboxes
                "wholerow", //Toute la ligne est surlignée
                "search", //Champs de recherche d'élément de la liste (filtre)
                "types",
                "contextmenu"
            ]
        };

        $('#arbre').jstree(jstreeconf);

        $("a#boutonEdit").hide();
        $("button#boutonFusion").hide();
        $("a#boutonDelete").hide();

        $('#arbre').on('changed.jstree', function (e, data) {
            /* Listener onChange qui fait la synchro jsTree/hiddenField */
            $("a#boutonEdit").hide().prop('href', '#');
            $("button#boutonFusion").hide().prop('href', '#');
            $("a#boutonDelete").hide().prop('href', '#');
            if (data.selected.length) {
                var node = data.instance.get_node(data.selected);
                if ($('#' + node.id).hasClass('isUpdatable')) {
                    $("a#boutonEdit").show().prop('href', '<?php echo $editAction; ?>' + '/' + node.data.id);
                    $("button#boutonFusion").show().prop('href', '<?php echo $fusionAction; ?>' + '/' + node.data.id);
                }
                $("#service_a_fusionner").val(node.data.id);
                if ($('#' + node.id).hasClass('isDeletable')) {
                    $("a#boutonDelete").show().prop('href', '<?php echo $deleteAction; ?>' + '/' + node.data.id);
                }
            }
        });

        /* Recherche dans la liste jstree */
        $('#search_tree_button').click(function () {
            $('#arbre').jstree(true).search($('#search_tree').val());
        });

        $('#search_tree_erase_button').click(function () {
            $('#search_tree').val('');
            $('#search_tree_button').click();
        });

        $('#search_tree').keydown(function (event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                $('#search_tree_button').click();
                return false;
            }
        });
        $("#search_tree_plier_button").click(function () {
            $('#arbre').jstree('close_all');
        });
        $("#search_tree_deplier_button").click(function () {
            $('#arbre').jstree('open_all');
        });

        function addAction() {
            window.location.href = $('a#boutonAdd').attr("href");
        }
        function editAction() {
            window.location.href = $('a#boutonEdit').attr("href");
        }
        function fusionService() {
            $('button#boutonFusion').click();
        }
        function deleteAction() {
            window.location.href = $('a#boutonDelete').attr("href");
        }

        function contextMenu(node) {
            // The default set of all items
            var items = {
                "add": {
                    "label": "Ajouter",
                    "action": addAction,
                    "icon": "fa fa-plus-circle",
                },
                "edit": {
                    "label": "Modifier",
                    "action": editAction,
                    "icon": "fa fa-pencil"
                },
                "delete": {
                    "label": "Supprimer",
                    "action": deleteAction,
                    "icon": "fa fa-trash-o"

                },
                "fusion": {
                    "label": "Fusionner",
                    "action": fusionService,
                    "icon": "icon-fusion",
                    "separator_before": true

                }
            };
            if ($('#' + node.id).hasClass("deletable") == false) {
                delete items.delete;
            }
            return items;
        }
    });
});
</script>
