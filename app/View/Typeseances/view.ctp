<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Types de séances'), ['action' => 'index']);
$this->Html->addCrumb($typeseance['Typeseance']['libelle']);

echo $this->Bs->tag('h3', __('Types de séance'));
?>
<div class="panel panel-default">
    <div class="panel-heading"><?php echo __('Fiche type de séance: ') . $typeseance['Typeseance']['libelle']; ?></div>
    <div class="panel-body">
        <dl>
            <div class="demi">
                <dt><?php echo __('Libellé'); ?></dt>
                <dd>&nbsp;<?php echo $typeseance['Typeseance']['libelle'] ?></dd>
            </div>
            <div class="demi">
                <dt><?php echo __('Nombre de jours avant retard'); ?></dt>
                <dd><?php echo $typeseance['Typeseance']['retard']; ?></dd>
            </div>
            <div class="spacer"></div>
            <div class="demi">
                <dt><?php echo __('Action'); ?></dt>
                <dd>&nbsp;<?php echo $typeseance['Typeseance']['action'] ? 'Avis' : 'Vote' ?></dd>
            </div>
            <div class="demi">
                <dt><?php echo __('Compteur'); ?></dt>
                <dd>&nbsp;<?php echo $typeseance['Compteur']['nom'] ?></dd>
            </div>
            <div class="spacer"></div>

            <div class="demi">
                <dt><?php echo __('Modèle de la convocation'); ?></dt>
                <dd>&nbsp;<?php echo $typeseance['Modele_convocation']['name']; ?></dd>
            </div>
            <div class="demi">
                <dt><?php echo __('Modèle de l\'ordre du jour'); ?></dt>
                <dd>&nbsp;<?php echo $typeseance['Modele_ordredujour']['name'] ?></dd>
            </div>
            <div class="spacer"></div>

            <div class="demi">
                <dt><?php echo __('Modèle du PV sommaire'); ?></dt>
                <dd>&nbsp;<?php echo $typeseance['Modele_pvsommaire']['name'] ?></dd>
            </div>
            <div class="demi">
                <dt><?php echo __('Modèle du PV complet'); ?></dt>
                <dd>&nbsp;<?php echo $typeseance['Modele_pvdetaille']['name'] ?></dd>
            </div>
            <div class="spacer"></div>

            <div class="demi">
                <dt><?php echo __('Date de création'); ?></dt>
                <dd>&nbsp;<?php echo $typeseance['Typeseance']['created'] ?></dd>
            </div>
            <div class="demi">
                <dt><?php echo __('Date de modification'); ?></dt>
                <dd>&nbsp;<?php echo $typeseance['Typeseance']['modified'] ?></dd>
            </div>
            <div class="spacer"></div>
        </dl>

        <br/>
        <?php
        echo $this->Bs->row() .
        $this->Bs->col('md4 of5');
        echo $this->Bs->div('btn-group', null, ['id' => "actions_fiche"]) .
        $this->Html2->btnCancel(),
        (
            $this->permissions->check('Typeseances', 'update')
                ? $this->Bs->btn($this->Bs->icon('pencil-alt') . ' ' . __('Modifier'), ['controller' => 'typeseances', 'action' => 'edit', $typeseance['Typeseance']['id']], ['type' => 'primary', 'escapeTitle' => false, 'title' => __('Modifier')])
                : ''
        ) .
        $this->Bs->close(6);
