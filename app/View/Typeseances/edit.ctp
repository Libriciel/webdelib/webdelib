<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Types de séances'), ['action' => 'index']);

if ($this->Html->value('Typeseance.id')) {
    $this->Html->addCrumb(__('Modification d\'un type de séance'));
    echo $this->Bs->tag('h3', $this->Html->value('Typeseance.libelle'));
    echo $this->BsForm->create('Typeseance', [
        'url' => ['controller' => 'typeseances', 'action' => 'edit', $this->Html->value('Typeseance.id')],
        'type' => 'post']);
} else {
    $this->Html->addCrumb(__('Ajouter un type de séance'));
    echo $this->Bs->tag('h3', __('Ajouter un type de séance'));
    echo $this->BsForm->create('Typeseance', [
        'url' => ['controller' => 'typeseances', 'action' => 'add'],
        'type' => 'post']);
}

echo $this->Bs->row();
echo $this->Bs->col('lg6') .
 $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
 $this->Html->tag('div', __('Informations générales'), ['class' => 'panel-heading']) .
 $this->Html->tag('div', null, ['class' => 'panel-body']) .
 $this->BsForm->input('Typeseance.libelle', [
   'label' => __('Libellé'),
   'required' => true]) .
 $this->BsForm->input('Typeseance.color', [
   'class' => 'cssTypeseanceColor',
   'label' => __('Couleur de fond')]) .
 $this->Bs->scriptBlock(//FIXME
         'require(["domReady"], function (domReady) {
            domReady(function () {'.
                '$(".cssTypeseanceColor").colorpicker();'
         .'});});'
         ) .
 $this->BsForm->input('Typeseance.retard', [
   'label' => __('Nombre de jours avant retard'),
   'required' => true,
   'default' => 0
 ]) .
 $this->BsForm->input('Typeseance.action', [
   'options' => $actions,
    'label' => __('Action en séance'),
    'default' => $this->Html->value('Typeseance.action'),
    'class' => 'selectone',
    'required' => true,
    'data-placeholder' => __('Sélectionner un type d\'action'),
    'empty' => !$this->Html->value('Typeseance.id')]) .
 $this->BsForm->input('Typeseance.compteur_id', [
    'options' => $compteurs,
    'label' => __('Compteur'),
    'class' => 'selectone',
    'required' => true,
     'data-placeholder' => __('Sélectionner un compteur'),
    'default' => $this->Html->value('Typeseance.compteur_id'),
    'empty' => (count($compteurs) > 1) && (!$this->Html->value('Typeseance.id'))]) .
 $this->BsForm->input('Typeacte.Typeacte', [
    'options' => $typeActes,
    'label' => __('Types d\'actes'),
    'default' => $selectedTypeActes,
    'multiple' => 'multiple',
    'class' => 'selectmultiple',
    'required' => true,
    'data-placeholder' => __('Sélectionner un ou plusieurs types d\'actes'),
    'empty' => false]) . ($this->Form->isFieldError('Typeacte') ? $this->Form->error('Typeacte') : '') .
  $this->BsForm->input('Typeseance.parent_id', [
       'options' => $typeSeances,
       'label' => __('Appartient à'),
       'default' => $selectedTypeSeances,
       'disabled' => $disabledTypeSeances,
       'class' => 'selectone',
       'data-allow-clear' => true,
       'data-placeholder' => __('Sélectionner un type de seance délibérante'),
       'empty' => true]) .
 $this->Bs->close(3);

echo $this->Bs->col('lg6');
echo $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
 $this->Html->tag('div', __('Modèles pour les éditions'), ['class' => 'panel-heading']) .
 $this->Html->tag('div', null, ['class' => 'panel-body']) .
 $this->BsForm->input('Typeseance.modele_projet_id', [
    'options' => $models_projet,
    'label' => __('Projet'),
    'class' => 'selectone',
    'required' => true,
    'data-placeholder' => !$this->Html->value('Typeseance.id') ? __('Sélectionner un modèle') : '',
    'default' => $this->Html->value('Typeseance.modele_projet_id'),
    'empty' => !$this->Html->value('Typeseance.id')]) .
 $this->BsForm->input('Typeseance.modele_deliberation_id', [
   'options' => $models_delib,
    'label' => __('Document final'),
    'class' => 'selectone',
    'required' => true,
    'data-placeholder' => !$this->Html->value('Typeseance.id') ? __('Sélectionner un modèle') : '',
    'default' => $this->Html->value('Typeseance.modele_deliberation_id'),
    'empty' => !$this->Html->value('Typeseance.id')]) .
 $this->BsForm->input('Typeseance.modele_convocation_id', [
   'options' => $models_convoc,
    'label' => __('Convocation'),
    'class' => 'selectone',
    'required' => true,
    'data-placeholder' => !$this->Html->value('Typeseance.id') ? __('Sélectionner un modèle') : '',
    'default' => $this->Html->value('Typeseance.modele_convocation_id'),
    'empty' => !$this->Html->value('Typeseance.id')]) .
 $this->BsForm->input('Typeseance.modele_ordredujour_id', [
   'options' => $models_odj,
    'label' => __('Ordre du jour'),
    'class' => 'selectone',
    'required' => true,
    'data-placeholder' => !$this->Html->value('Typeseance.id') ? __('Sélectionner un modèle') : '',
    'default' => $this->Html->value('Typeseance.modele_ordredujour_id'),
    'empty' => !$this->Html->value('Typeseance.id')]) .
 $this->BsForm->input('Typeseance.modele_pvsommaire_id', [
   'options' => $models_pvsommaire,
    'label' => __('PV sommaire'),
    'class' => 'selectone',
    'required' => true,
    'data-placeholder' => !$this->Html->value('Typeseance.id') ? __('Sélectionner un modèle') : '',
    'default' => $this->Html->value('Typeseance.modele_pvsommaire_id'),
    'empty' => !$this->Html->value('Typeseance.id')]) .
 $this->BsForm->input('Typeseance.modele_pvdetaille_id', [
   'options' => $models_pvdetaille,
    'label' => __('PV complet'),
    'class' => 'selectone',
    'required' => true,
    'data-placeholder' => !$this->Html->value('Typeseance.id') ? __('Sélectionner un modèle') : '',
    'default' => $this->Html->value('Typeseance.modele_pvdetaille_id'),
    'empty' => !$this->Html->value('Typeseance.id')]) .
  $this->BsForm->input('Typeseance.modele_journal_seance_id', [
    'options' => $models_journal_seance,
    'label' => __('Journal'),
    'class' => 'selectone',
    'data-allow-clear' => true,
    'data-placeholder' => __('Sélectionner un modèle'),
    'default' => $this->Html->value('Typeseance.modele_journal_seance_id'),
    'empty' => true]) .
 $this->Bs->close(4);

echo $this->Bs->row();
echo $this->Bs->col('lg12');
echo $this->Html->tag('div', null, ['class' => 'panel panel-default']) .
 $this->Html->tag('div', __('Convocations (union des deux sélections ci-dessous)'), ['class' => 'panel-heading']) .
 $this->Html->tag('div', null, ['class' => 'panel-body']) .
 $this->Bs->row() .
 $this->Bs->col('lg6') .
 $this->BsForm->select('Typeacteur', $typeacteurs, [
    'label' => __('Par type d\'acteur'),
    'default' => $selectedTypeacteurs,
    'class' => 'selectmultiple',
    'multiple' => 'multiple',
    'data-placeholder' => __('Sélectionner un type d\'acteur'),
    'empty' => true]) .
 $this->Bs->close() .
 $this->Bs->col('lg6') .
 $this->BsForm->select('Acteur', $acteurs, [
    'label' => __('Par acteur'),
    'default' => $selectedActeurs,
    'class' => 'selectmultiple',
    'data-placeholder' => __('Sélectionner un acteur'),
    'multiple' => 'multiple',
    'empty' => true]) .
 $this->Bs->close(6);

if (strpos($this->request->params['action'], 'edit') !== false) {
    echo $this->BsForm->hidden('Typeseance.id');
}

echo $this->Html2->btnSaveCancel('', $previous) .
 $this->BsForm->end();
