<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Types de séances'));

echo $this->Bs->tag('h3', __('Liste des types de séance'));
if ($this->permissions->check('Typeseances', 'create')) {
    echo $this->Html2->btnAdd(__("Ajouter un type de séance"), __("Ajouter"));
}
echo $this->Bs->table([['title' => __('Libellé')],
    ['title' => __('Nb jours avant retard')],
    ['title' => __('Action')],
    ['title' => __('Compteur')],
    ['title' => __('Modèles')],
    ['title' => __('Convocations')],
    ['title' => __('Actions')],
        ], ['hover', 'striped']);
foreach ($typeseances as $typeseance) {
    $Typeacteur = '';
    if (!empty($typeseance['Typeacteur'])) {
        $Typeacteur = __('Types d\'acteur :') . '<br/>';
        foreach ($typeseance['Typeacteur'] as $typeacteur) {
            $Typeacteur.= '&nbsp;&nbsp;' . $typeacteur['nom'] . $this->Html->tag(null, '<br />');
        }
    }
    $Acteur = '';
    if (!empty($typeseance['Acteur'])) {
        $Acteur = 'Acteurs :<br/>';
        foreach ($typeseance['Acteur'] as $acteur) {
            $Acteur.= '&nbsp;' . $acteur['prenom'] . ' ' . $acteur['nom'] . $this->Html->tag(null, '<br />');
        }
    }

    echo $this->Bs->cell($typeseance['Typeseance']['libelle']);
    echo $this->Bs->cell($typeseance['Typeseance']['retard']);
    echo $this->Bs->cell($typeseance['Typeseance']['action']);
    echo $this->Bs->cell($typeseance['Compteur']['nom']);
    echo $this->Bs->cell(__('Projet : ') . $typeseance['Modele_projet']['name'] .
            $this->Html->tag(null, '<br />') .
            __('Document final : ') . $typeseance['Modele_deliberation']['name'] .
            $this->Html->tag(null, '<br />') .
            __('Convocation : ') . $typeseance['Modele_convocation']['name'] .
            $this->Html->tag(null, '<br />') .
            __('Ordre du jour : ') . $typeseance['Modele_ordredujour']['name'] .
            $this->Html->tag(null, '<br />') .
            __('PV sommaire : ') . $typeseance['Modele_pvsommaire']['name'] .
            $this->Html->tag(null, '<br />') .
            __('PV complet : ') . $typeseance['Modele_pvdetaille']['name'] .
            (!empty($typeseance['Modele_journal_seance']['name'])?
            $this->Html->tag(null, '<br />') .
            __('Journal de séance : ') . $typeseance['Modele_journal_seance']['name']:''));
    echo $this->Bs->cell($Typeacteur . ' ' . $Acteur);

    $actions = $this->Bs->div('btn-group');
    if ($this->permissions->check('Typeseances', 'read')) {
        $actions.= $this->Bs->btn($this->Bs->icon('eye', null, ['stylePrefix' => 'far']), ['controller' => 'typeseances', 'action' => 'view', $typeseance['Typeseance']['id']], ['type' => 'default', 'escapeTitle' => false, 'title' => 'Visualiser']);
    }
    if ($this->permissions->check('Typeseances', 'update')) {
        $actions.= $this->Bs->btn($this->Bs->icon('pencil-alt'), ['controller' => 'typeseances', 'action' => 'edit', $typeseance['Typeseance']['id']], ['type' => 'primary', 'escapeTitle' => false, 'title' => 'Modifier']);
    }
    if ($this->permissions->check('Typeseances', 'delete')) {
        $actions.= $this->Bs->btn($this->Bs->icon('trash'), ['controller' => 'typeseances', 'action' => 'delete', $typeseance['Typeseance']['id']], ['type' => 'danger', 'escapeTitle' => false, 'title' => __('Supprimer'), 'class' => !$typeseance['Typeseance']['is_deletable'] ? 'disabled' : ''
            , 'confirm' => __('Êtes-vous sûr de vouloir supprimer %s ?', $typeseance['Typeseance']['libelle'])
        ]);
    }
    $actions.=$this->Bs->close();
    echo $this->Bs->cell($actions);
}
echo $this->Bs->endTable();
