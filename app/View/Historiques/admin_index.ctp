<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
//AFFICHAGE
$this->Html->addCrumb(__('Historiques'));
echo $this->element('filtre');

echo $this->Bs->tag('h3', __('Historiques') . ' (' . $this->Paginator->counter('{:count}') . ')') ;

//CONSTRUCTION DE L'AFFICHAGE
echo $this->Bs->row() .
 $this->Bs->col('lg12') .
 $this->Bs->table(
        [['title' => $this->Paginator->sort('Historique.created', __('Date'))], ['title' => $this->Paginator->sort('Deliberation.id', __('id'))], ['title' => __('User')], ['title' => __('Commentaire')]], ['striped']
);
foreach ($historiques as $data) {
    if (!empty($data['Historique']['commentaire'])) {
        echo $this->Bs->cell($data['Historique']['created']);
        if (!empty($data['Deliberation']['id'])) {
            echo $this->Bs->cell($this->Html->link($data['Deliberation']['id'], ['admin' => false, 'prefix' => null, 'controller' => 'projets', 'action' => 'view', $data['Deliberation']['id']], ['class' => 'btn', 'escape' => false, 'alt' => __('Nouvelle recherche parmi tous les projets'), 'title' => __('Nouvelle recherche parmi tous les projets')]));
        } else {
            echo $this->Bs->cell($data['Historique']['delib_id'] . __(' (' . $this->Bs->icon('times') . ')'));
        }
        echo $this->Bs->cell($data['User']['nom'] . ' ' . $data['User']['prenom']);
        echo $this->Bs->cell($data['Historique']['commentaire'], 'text-justified');
    }
}
echo $this->Bs->endTable() .
 //affichage des pages numéroté
$this->element('paginator', ['paginator' => $this->Paginator]) .
$this->Bs->close(2);