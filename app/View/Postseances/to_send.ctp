<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Post-séances'), ['controller' => 'postseances', 'action' => 'index']);
$this->Html->addCrumb(__('À télétransmettre'));

echo $this->Bs->tag('h3', __('Télétransmission des délibérations'));

echo $this->element('filtre');

$classification = '';
if (!empty($dateClassification)) {
    $classification = __('La Classification enregistrée date du') . ' ' . $this->Time->i18nFormat($dateClassification, '%A %d %B %Y');
    $this->Bs->div();
} else {
    $classification = $this->Bs->icon('warning') . __('Classification non téléchargée');
}

echo $this->Bs->div(
    'alert alert-info',
    $classification . ' ' .
        $this->Html->link(
            $this->Bs->icon('sync'),
            ['controller' => 'teletransmettre', 'action' => 'getClassification'],
            [
            'title' => __('Télécharger/Mettre à jour les données de classification'),
            'escapeTitle' => false]
        )
) .
 $this->Bs->div();

 $deploy = '<div class="dropdown">'
         . '<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">'
         . $this->Bs->icon('sliders-h') . ' <span class="caret"></span>'
         . '</button>'
         . '<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">';
     $deploy .= '<li role="presentation">' .
             $this->Bs->btn(__('Déplier toutes les annexes'), ['annexe_visu'=>'show'], [
                 'escapeTitle'=> false,
                 'role' => 'menuitem',
                 'tabindex' => -1
             ]) .'</li>';
      $deploy .= '<li role="presentation">' .
              $this->Bs->btn(__('Replier toutes les annexes'), ['annexe_visu'=>'hide'], [
                 'escapeTitle'=> false,
                 'role' => 'menuitem',
                 'tabindex' => -1
             ]) .'</li></ul></div>';
echo $deploy;

 echo $this->BsForm->create(
     'Deliberation',
     [
       'class' => 'appSendTdt appAbandon appTypologie waiter form-inline',
       'data-waiter-title'=>__('Télétransmission en cours'),
       'type' => 'file',
       'url' => ['controller' => 'postseances', 'action' => 'sendToTdt']
     ]
 );

echo $this->Bs->table([
    ['title' => $this->BsForm->checkbox(null, [
            'id' => 'masterCheckbox',
            'inline' => 'inline',
            'autocomplete' => 'off'
        ])],
    ['title' => __('id')],
    ['title' => __('N° Deliberation')],
    ['title' => __('Libellé de l\'acte')],
    ['title' => __('Titre')],
    ['title' => __('Annexe(s)')],
    ['title' => __('Classification')],
    ['title' => __('Type de pièce principale')],
    ['title' => __('Envoi complémentaire')],
    ['title' => __('Statut')],
    ['title' => __('Actions')],
        ], ['hover', 'striped']);

foreach ($deliberations as $acte) {
    $options = ['hiddenField' => false];
    if (in_array($acte['Deliberation']['etat'], [3,4,10], true) && $acte['Deliberation']['signee']) {
        $options['checked'] = true;
    } else {
        $options['disabled'] = true;
    }
    echo $this->Bs->cell($this->BsForm->checkbox('Deliberation.' . $acte['Deliberation']['id'] . '.send', array_merge($options, [
                'label' => false,
                'autocomplete' => 'off',
                'class' => 'masterCheckbox_checkbox',
                'inline' => 'inline',
    ])));
    echo $this->Bs->cell($this->Html->link($acte['Deliberation']['id'], [
                'controller' => 'projets',
                'action' => 'view', $acte['Deliberation']['id']]));
    echo $this->Bs->cell($this->Html->link($acte['Deliberation']['num_delib'], [
        'controller' => 'actes',
        'action' => 'download', $acte['Deliberation']['id']]));
    echo $this->Bs->cell($acte['Deliberation']['objet_delib']);
    echo $this->Bs->cell($acte['Deliberation']['titre']);

    $annexes = '';
    $hide = empty($this->request->params['named']['annexe_visu']);
    if (!empty($this->request->params['named']['annexe_visu']) && $this->request->params['named']['annexe_visu']==='hide') {
        $hide = true;
    }
    if (!empty($acte['Annexe']) && $hide) {
        $annexes = '<div class="dropdown">'
                . '<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">'
                . __('Liste des annexes') . ' <span class="label label-info label-as-badge">' . count($acte['Annexe']) . '</span>'
                . '<span class="caret"></span>'
                . '</button>'
                . '<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">';
        foreach ($acte['Annexe'] as $annexe) {
            $annexes.='<li role="presentation">' .
                    $this->Bs->btn($this->Bs->icon('download') . ' ' .$annexe['filename'] . ' | ' . (!empty($annexe['typologiepiece_code_libelle'])?$annexe['typologiepiece_code_libelle']:'<em>-- Type manquant --</em>'), [
                        'controller' => 'annexes', 'action' => 'download', $annexe['id']], [
                        'escapeTitle'=> false,
                        'role' => 'menuitem',
                        'tabindex' => -1
                    ]).'</li>';
        }

        echo '</ul>'
        . '</div>';
    }
    if (!empty($acte['Annexe']) && !empty($this->request->params['named']['annexe_visu']) && $this->request->params['named']['annexe_visu']==='show') {
        foreach ($acte['Annexe'] as $annexe) {
            $annexes.=  $this->Bs->link('<span style="white-space: nowrap;">' . $this->Bs->icon('download') . ' ' .$annexe['filename'] . '</span>', [
                        'controller' => 'annexes', 'action' => 'download', $annexe['id']], [
                        'escapeTitle'=> false,
                    ]). '<br /> <span style="white-space: nowrap;">Type : ' . (!empty($annexe['typologiepiece_code_libelle'])?$annexe['typologiepiece_code_libelle']:'<em>-- Type manquant --</em>').'</span>' . '<br />';
        }
    }
    echo $this->Bs->cell($annexes);

    echo $this->Bs->cell(!empty($acte['Deliberation']['num_pref_libelle']) ? $acte['Deliberation']['num_pref_libelle'] : '<em>-- Manquante --</em>');
    echo $this->Bs->cell(!empty($acte['Deliberation']['typologiepiece_code_libelle']) ? $acte['Deliberation']['typologiepiece_code_libelle'] : '<em>-- Type manquant --</em>');
    echo $this->Bs->cell(!empty($acte['Deliberation']['tdt_document_papier']) ? 'Oui' : 'Non');

    if ($acte['Deliberation']['etat'] == 5) {
        $statut = $this->Bs->icon('check-circle') .' '. __('Envoyé');
    } elseif ($acte['Deliberation']['tdt_status'] === 0) {
        $statut = __("À renvoyer");
    } else {
        $statut = __("Non envoyé");
    }
    echo $this->Bs->cell($statut);

    //Représente une liste d'actions dans une cellule du tableau
    echo $this->element('Deliberations/deliberation_actions_signature', [
      'type' => 'deliberation',
      'deliberation' => $acte
    ]);
}
echo $this->Bs->endTable();

//paginate
echo $this->element('paginator', ['paginator' => $this->Paginator]);

if (!empty($deliberations)) {
    echo $this->element('Projet/traitement_lot', [
        'traitement_lot' => isset($traitement_lot) ? $traitement_lot : null,
        'actions_possibles'=>[
            'sendToTdt' => 'Envoyer au TdT',
            'depotManuel' => 'Déclarer un dépôt manuel sur le TdT',
            // 'autresActesAbandon' => __('Abandonner') ,
            // !$acl['autresActesAbandon'] ? null :'autresActesAbandon' => __('Abandonner')  ,
        ]]);
}

if (isset($seance_id)) {
    echo $this->Form->hidden('Seance.id', ['value' => $seance_id]);
}

echo $this->BsForm->end();
