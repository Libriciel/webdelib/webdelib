<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Post-séances'), ['controller' => 'postseances', 'action' => 'index']);

$this->Html->addCrumb(__('Débats de la séance'));

echo $this->Html->tag(
    'h3',
    __(
        'Liste des débats de la séance : %s du %s à %s',
        $seance["Typeseance"]['libelle'],
        $this->Time->i18nFormat($seance['Seance']['date'], '%d/%m/%Y'),
        $this->Time->i18nFormat($seance['Seance']['date'], '%kh%M')
    )
);

$titles[] = ['title' => __('Num Delib')];
$titles[] = ['title' => __('Libellé de l\'acte')];
$titles[] = ['title' => __('Titre')];
$titles[] = ['title' => __('Thème')];
$titles[] = ['title' => __('Rapporteur')];
$titles[] = ['title' => __('Actions')];

echo $this->Bs->table($titles, ['hover', 'striped']);
foreach ($projets as $projet) {
    echo $this->Bs->tableCells(
        [
            $projet['Deliberation']['num_delib'],
            $projet['Deliberation']['objet_delib'],
            $projet['Deliberation']['titre'],
            $projet['Theme']['libelle'],
            $projet['Rapporteur']['nom'] . ' ' . $projet['Rapporteur']['prenom'],
            $this->Bs->div('btn-group-vertical') .
            ($pv_figes !== 1 ?
                    $this->Bs->btn($this->Bs->icon('comments'), ['controller' => 'debats', 'action' => 'edit', $projet['Deliberation']['id'], $seance_id], ['type' => 'primary',
                        'escapeTitle' => false,
                        'title' => __('Saisir les débats')]) : '') .
            $this->Bs->btn($this->Bs->icon('download'), ['controller' => 'debats', 'action' => 'download', $projet['Deliberation']['id']], ['type' => 'default',
                'escapeTitle' => false,
                'title' => __('Télécharger le débat')]) .
            $this->Bs->close()
        ]
    );
}
echo $this->Bs->endTable() .
 $this->Bs->div('btn-group') .
 $this->Html2->btnCancel() .
 $this->Bs->close() .
 ($pv_figes != 1 ?
         $this->Bs->div('btn-group') .
 $this->Bs->btn($this->Bs->icon('comments') . ' ' . __('Débats généraux'), [
        'controller' => 'seances',
        'action' => 'saisirDebatGlobal', $seance_id], [
        'type' => 'primary',
       // 'class' => 'pull-right',
        'name' => 'Retour',
        'title' => __('Saisir les débats généraux'),
        'escapeTitle' => false]). $this->Bs->close(): '').
 $this->Bs->close();
