<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Post-séances'), ['controller' => 'postseances', 'action' => 'index']);
$this->Html->addCrumb(__('Signatures'));

echo $this->element('filtre');

$titles = [];

    echo $this->BsForm->create('Deliberation', [
        'url' => ['controller' => 'signatures', 'action' => 'sendDeliberationsToSignature'],
        'class' => 'waiter form-inline appInfosup',
        'data-waiter-title'=>__('Signature en cours'),
    ]);

    echo $this->Bs->tag('h3', __('Suivi des délibérations envoyées en signature'));

    $titles[] = ['title' => $this->BsForm->checkbox(null, [
        'id' => 'masterCheckbox',
        'label' => '',
        'inline' => true
    ])];

$titles[] = ['title' => 'Id.'];
$titles[] = ['title' => 'N° Acte'];
$titles[] = ['title' => __('Date de signature')];
$titles[] = ['title' => __('Libellé de l\'acte')];
$titles[] = ['title' => __('État')];
$titles[] = ['title' => __('Actions')];



echo $this->Bs->table($titles, ['hover', 'striped']);

foreach ($deliberations as $delib) {
    $options = [];
    if (empty($delib['Deliberation']['signee']) && in_array($delib['Deliberation']['parapheur_etat'], [null, 0, -1], true) && in_array($delib['Deliberation']['etat'], [3, 4,10], true)
    ) {
        $options['checked'] = true;
    } else {
        $options['disabled'] = true;
    }
    echo $this->Bs->cell($this->Form->checkbox('Deliberation.' . $delib['Deliberation']['id'] . '.is_checked', $options));

    echo $this->Bs->cell($this->Html->link($delib['Deliberation']['id'], ['controller'=>'projets', 'action' => 'view', $delib['Deliberation']['id']]));
    echo $this->Bs->cell($delib['Deliberation']['num_delib']);
    echo $this->Bs->cell(!empty($delib['Deliberation']['signature_date']) ? $this->Time->i18nFormat($delib['Deliberation']['signature_date'], '%e %B %Y') : '');
    echo $this->Bs->cell($delib['Deliberation']['objet_delib']);

    switch ($delib['Deliberation']['parapheur_etat']) {
        case -1:
            $status = $this->Bs->icon('exclamation-triangle', null, ['title'=> $delib['Deliberation']['parapheur_commentaire']])  . '&nbsp;' . __('Retour parapheur : refusée');
            break;
        case 1:
            $status =  $this->Bs->icon('clock') .' '. __('En cours de signature');
            break;
        case 2:
            $signature = '';
            if (!empty($delib['Deliberation']['signee'])) {
                if (!empty($delib['Deliberation']['signature_type']) && $delib['Deliberation']['signature_type']==='PAdES') {
                    $signature = __('(Signée PAdES)');
                } elseif (!empty($delib['Deliberation']['signature'])) {
                    $signature = '('.
                                $this->Bs->link(
                                    $this->Bs->icon('file-archive') . ' ' .__('Signée PKCS#7'),
                                    ['controller'=>'actes',
                                        'action'=>'downloadSignature', $delib['Deliberation']['id']],
                                    [
                                            'escapeTitle' => false,
                                            'style' => 'text-decoration: none',
                                            'title'=> __('Télécharger la signature')]
                                ).')';
                } else {
                    $signature = __('(Visa)');
                }
            }
            $status = $this->Bs->icon('award') .' '. __('Signé électroniquement') . ' '. $signature;
            break;
        default: //0 ou null
            if (!empty($delib['Deliberation']['signee'])) {
                if (!empty($delib['Deliberation']['signature'])) {
                    if ($delib['Deliberation']['signature_type']!='PAdES') {
                        $status = $this->Bs->icon('award') . __('Signée') . ' '.
                                $this->Bs->link(
                                    $this->Bs->icon('download'),
                                    ['controller'=>'actes', 'action'=>'downloadSignature', $delib['Deliberation']['id']],
                                    [
                                            'escapeTitle' => false,
                                            'style' => 'text-decoration: none',
                                            'title'=> __('Télécharger la signature')]
                                );
                    } else {
                        $status = __('Signée');
                    }
                } else {
                    $status = $this->Bs->icon('award') .' '. __('Signée manuellement');
                }
            } else {
                switch ($delib['Deliberation']['etat']) {
                    case -1:
                        $status = $this->Bs->icon('times') .' '. __('Projet refusé');
                        break;
                    case 2:
                        $status = $this->Bs->icon('clock') .' '. __('A faire voter');
                        break;
                    case 3:
                        $status = $this->Bs->icon('thumbs-up') .' '. __('Projet voté');
                        break;
                    case 4:
                        $status = $this->Bs->icon('thumbs-down') .' '. __('Projet rejeté');
                        break;
                    case 5:
                        $status = $this->Bs->icon('award') .' '. __('Projet envoyé au TdT');
                        break;
                    case 10:
//                        $status = $delib['Deliberation']['vote_resultat'] == true ? $this->Bs->icon('thumbs-up') .' '. __('Projet voté')
//                            :  $this->Bs->icon('thumbs-down') .' '. __('Projet rejeté')
//                        ;
                        $status = $this->Bs->icon('award') .' '. __('À Renvoyer');
                        break;
                    default:
                        $status = $this->Bs->icon('pencil-alt') .' '. __('En cours d\'élaboration');
                }
            }
    }
    echo $this->Bs->cell($status);

    //Représente une liste d'actions dans une cellule du tableau
    echo $this->element('Deliberations/deliberation_actions_signature', ['deliberation'=>$delib,'type'=>'deliberation']);
}

echo $this->Bs->endTable();

$this->BsForm->setLeft(0);
$this->BsForm->setRight(12);
$actions = [];
if ($this->permissions->check('sendDeliberationsToSignature', 'read')) {
    isset($circuits['Standard']) ? $actions['signatureManuscrite'] = __('Signature manuscrite') : null;
    isset($circuits['Parapheur']) ? $actions['signatureElectronique'] = __('Signature électronique') : null;
}
echo $this->element('Projet/traitement_lot', [
        'traitement_lot' => true ,
        'actions_possibles' => $actions
    ]).

    $this->Bs->close();

echo $this->BsForm->end();

echo $this->Html->tag('br');

//paginate
echo $this->element('paginator', ['paginator' => $this->Paginator]);
