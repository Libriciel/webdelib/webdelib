<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Post-séances'));
$this->Html->addCrumb(__('Éditions'));

echo $this->element('filtre');

echo $this->Bs->tag('h3', __('Éditions des séances'));

$titles[] = ['title' => __('Type de séance')];
$titles[] = ['title' => __('Date')];
$titles[] = ['title' => __('Associées')];
$titles[] = ['title' => __('Débats')];
$titles[] = ['title' => __('Transmissions TdT')];
if (in_array('ged', $actions, true)) {
    $titles[] = ['title' => __('Export GED')];
}
if (in_array('sendToSaeDeliberation', $actions, true)) {
    $titles[] = ['title' => __('Archivage délibérations SAE')];
}
if (in_array('sendToSae', $actions, true)) {
    $titles[] = ['title' => __('Archivage dossier SAE')];
}
$titles[] = ['title' => __('Actions')];
echo $this->Bs->table($titles, ['drop', 'hover', 'striped']);

foreach ($seances as $seance) {
    $attributes = !empty($seance['Typeseance']['color']) ? ['style' => 'color: ' . $seance['Typeseance']['color']] : [];

    echo $this->Bs->cell($this->Bs->icon('tag', ['lg'], $attributes) . ' ' . $seance['Typeseance']['libelle']);
    echo $this->Bs->cell($this->Time->i18nFormat($seance['Seance']['date'], '%A %e %B %Y').' à '.$this->Time->i18nFormat($seance['Seance']['date'], '%k:%M'));

    $data_content = '';
    if (!empty($seance['SeanceChildren'])) {
        foreach ($seance['SeanceChildren'] as $seanceChildren) {
            $data_content .= '- '.__(
                '%s du %s à %s',
                $seanceChildren['Typeseance']['libelle'],
                CakeTime::i18nFormat($seanceChildren['date'], '%A %e %B %Y'),
                CakeTime::i18nFormat($seanceChildren['date'], '%k:%M')
            ) . $this->Bs->tag('br /');
        }
    }
    echo $this->Bs->cell(
        (!empty($seance['Seance']['parent_id']) ?
        $this->Bs->btn(
            $this->Bs->icon('link'),
            null,
            [
       'tag' => 'button',
       'type' => 'default',
       'option_type' => 'button',
       'title' => __('Séances Associées'),
       'data-toggle' => 'popover',
       'data-content' => __(
           '%s du %s à %s',
           $seance['SeanceParent']['Typeseance']['libelle'],
           CakeTime::i18nFormat($seance['SeanceParent']['date'], '%A %e %B %Y'),
           CakeTime::i18nFormat($seance['SeanceParent']['date'], '%k:%M')
       ),
       'data-placement' => 'right',
       'escapeTitle' => false
  ]
        ) : '').
        (!empty($seance['SeanceChildren']) ?
        $this->Bs->btn(
            $this->Bs->icon('link') . ' ' . '<span class="badge badge-counter">'.  count($seance['SeanceChildren']) . '</span>',
            null,
            [
            'tag' => 'button',
            'type' => 'default',
            'option_type' => 'button',
            'title' => __('Séances Associées'),
            'data-toggle' => 'popover',
            'data-content' => $data_content,
            'data-placement' => 'right',
            'escapeTitle' => false
       ]
        ): '')
    );

    echo $this->Bs->cell($this->Bs->div('btn-group') .
            $this->Bs->btn($this->Bs->icon('comments') . '<span class="badge badge-counter">'. $seance['nombreDeDebats'] . ' </span>', [
                'controller' => 'postseances',
                'action' => 'afficherProjets', $seance['Seance']['id'], 'debats'],
                [
                    'type' => 'default',
                    'escapeTitle' => false,
                    'class'=>'post_seance_button',
                    'title' => __('Liste les débats')
                ])
        .
        (!$seance['Seance']['hasGlobalDebat'] ?
            $this->Bs->btn($this->Bs->icon('comment'). '<span class="badge badge-counter">0</span>',
                (
                    $seance['Seance']['pv_figes'] ? 'javascript:void(0);' : [
                    'controller' => 'seances',
                    'action' => 'saisirDebatGlobal',
                    $seance['Seance']['id']]
                ),
                [
                'type' => 'default',
                'class'=>'post_seance_button',
                'disabled' =>  $seance['Seance']['pv_figes'] ?? 'disabled',
                'name' => 'Retour',
                'title' => __('Saisir les débats généraux'),
                'escapeTitle' => false]) :
            $this->Bs->btn($this->Bs->icon('comment'). '<span class="badge badge-counter"> 1</span>', ['controller' => 'seances',
                'action' => 'saisirDebatGlobal', $seance['Seance']['id']], [
                'type' => 'default',
                'class'=>'post_seance_button',
                'name' => 'Retour',
                'title' => __('Consulter les débats généraux'),
                'escapeTitle' => false])
        ).
        $this->Bs->close());

    if ($seance['Typeseance']['action'] === 1) {
        echo $this->Bs->cell('');
    } else {
        echo $this->Bs->cell(
            $this->Bs->div('btn-group') .
            (($use_tdt) ?
                $this->Bs->btn($this->Bs->icon('share-square'), ['controller' => 'postseances', 'action' => 'toSend', $seance['Seance']['id']], [
                    'type' => 'primary',
                    'escapeTitle' => false,
                    'title' => __('Envoyer au TdT')]) .
                $this->Bs->btn($this->Bs->icon('university') . '<span class="badge badge-counter">' . $seance['nombreDeliberationTdtOk'] . ' </span>', ['controller' => 'postseances', 'action' => 'transmit', $seance['Seance']['id']], [
                    'type' => 'default',
                    'class' => 'post_seance_button',
                    'escapeTitle' => false,
                    'title' => __('Délibérations envoyées au TdT')]) : '') . $this->Bs->close()
        );
    }

    if (in_array('ged', $actions, true)) {
        echo $this->Bs->cell(
            $this->Bs->div('btn-group') .
                $this->Bs->btn($this->Bs->icon('cloud-upload-alt') . ' ' . __('<span class="badge badge-counter"> %d</span>', $seance['Seance']['numero_depot']), [
                    'controller' => 'ExportGedSeances',
                    'action' => 'sendToGed', $seance['Seance']['id']
                ], [
                    'type' => 'primary',
                    'class' => 'waiter',
                    'data-waiter-send' => 'XHR',
                    'data-waiter-title' => __('Envoi de la séance à la GED'),
                    'data-waiter-type' => 'progress-bar',
                    'data-waiter-callback' => 'getProgress',
                    'escapeTitle' => false,
                    'title' => __('Envoyer la séance à la GED'),
                    'confirm' => __('Envoyer les documents à la GED ?')])
                .
                $this->Bs->close()
        );
    }
    if (in_array('sendToSaeDeliberation', $actions, true)) {
        if ($seance['Typeseance']['action'] > 0) {
            echo $this->Bs->cell('');
        } else {
            echo $this->Bs->cell(
                $this->Bs->div('btn-group') .
                $this->Bs->btn($this->Bs->icon('share-square'), [
                    'controller' => 'verserSae', 'action' => 'sendToSaeDeliberation', $seance['Seance']['id']], [
                    'type' => 'primary',
                    'escapeTitle' => false,
                    'title' => __('Verser au SAE')]) .
                $this->Bs->btn($this->Bs->icon('archive') . '<span class="badge badge-counter">' . $seance['nombreDeliberationSaeOk'] . ' </span>', [
                    'controller' => 'verserSae', 'action' => 'sendToSaeDeliberation', $seance['Seance']['id']], [
                    'type' => 'default',
                    'class' => 'post_seance_button',
                    'escapeTitle' => false,
                    'title' => __('Délibérations versées au SAE')]) .

                $this->Bs->close()
            );
        }
    }

    if (in_array('sendToSae', $actions, true)) {
        $disabled = 'disabled';
        if((!isset($seance['Seance']['sae_etat']) XOR (isset($seance['Seance']['sae_etat']) && $seance['Seance']['sae_etat'] !== SaeVersement::VERSER))
            && $seance['Seance']['pv_figes'] === true && $seance['Seance']['isSeanceOk'] === true) {
            $disabled = null;
        }
        echo $this->Bs->cell(
            $this->Bs->div('btn-group') .
            $this->Bs->btn(
                $this->Bs->icon('archive') . ' ' . __('<span class="badge badge-counter"> %d</span>', $seance['Seance']['sae_numero_versement'] ?: 0),
                ($disabled===null ?  [
                    'controller' => 'VerserSaeSeances',
                    'action' => 'sendToSae',
                    $seance['Seance']['id']]:'javascript:void(0);')
            , [
                'type' => 'primary',
                'class' =>  $disabled===null ? 'waiter':'',
                'disabled' => $disabled,
                'data-waiter-send' => 'XHR',
                'data-waiter-title' => __('Envoi de la séance au SAE'),
                'data-waiter-type' => 'progress-bar',
                'data-waiter-callback' => 'getProgress',
                'escapeTitle' => false,
                'title' => __('Verser au SAE le dossier de séance'),
                'confirm' => $disabled===null ? __('Verser le dossier de séance au SAE ?') : false])
            .
            $this->Sae->btnEtat($seance['Seance']['sae_etat'], $seance['Seance']['sae_commentaire'])
            . $this->Bs->close()
        );
    }


    if ($seance['Seance']['pv_figes'] === true) {
        $nestedList = [
            $this->Bs->link($seance['Typeseance']['Modele_pvsommaire']['name'], [
                'controller' => 'postseances', 'action' => 'download', $seance['Seance']['id'], 'sommaire'
            ], [
                'title' => __('Télécharger le document de type PV sommaire pour la séance du ') . $this->Time->i18nFormat($seance['Seance']['date'], '%d/%m/%Y à %k:%M'),
            ]),
            $this->Bs->link($seance['Typeseance']['Modele_pvdetaille']['name'], [
                'controller' => 'postseances', 'action' => 'download', $seance['Seance']['id'], 'complet'
            ], [
                'title' => __('Télécharger le document de type pv complet pour la séance du ') . $this->Time->i18nFormat($seance['Seance']['date'], '%d/%m/%Y à %k:%M')
            ])
        ];
        if(!empty($seance['Seance']['sae_atr'])){
            $nestedList[] = $this->Bs->link('Réponse du SAE', ['controller' => 'postseances', 'action' => 'download', $seance['Seance']['id'], 'sae_atr'], [
                'title' => __('Télécharger la réponse du SAE')
            ]);
        }

        echo $this->Bs->cell(
            $this->Bs->div('btn-group') .
                $this->Bs->btn($this->Bs->icon('list') . '<span class="badge badge-counter">'. $seance['nombreDeProjet']. ' </span>', ['controller' => 'postseances', 'action' => 'afficherProjets', $seance['Seance']['id']], ['type' => 'default',
                'escape' => false,
                'class'=>'post_seance_button',
                'title' => __('Visualiser les délibérations')]) .
                $this->Bs->btn($this->Bs->icon('check'), 'javascript:void(0);',
                    [
                        'type' => 'primary',
                        'disabled' => 'disabled',
                        'escapeTitle' => false,
                        'name' => 'Clore',
                        'title' => __('Les documents sont figés')
                ]) .
                $this->Bs->btn($this->Bs->icon('download') /*. ' ' . __('Télécharger', 'btn')*/ . ' <span class="caret"></span>', ['controller' => 'postseances', 'action' => 'afficherProjets'], ['type' => 'default',
                    'escapeTitle' => false,
                    'class' => 'dropdown-toggle',
                    'data-toggle' => 'dropdown']) .
                $this->Bs->nestedList($nestedList, ['class' => 'dropdown-menu', 'role' => 'menu']) .
                $this->Bs->close()
        );
    } else {
        //////////////////////
        echo $this->Bs->cell(
            $this->Bs->div('btn-group') .
                $this->Bs->btn($this->Bs->icon('list') . '<span class="badge badge-counter">'. $seance['nombreDeProjet']. ' </span>', ['controller' => 'postseances', 'action' => 'afficherProjets', $seance['Seance']['id']], ['type' => 'default',
                'escape' => false,
                'title' => __('Visualiser les délibérations')]) .
                $this->Bs->btn($this->Bs->icon('check'), [
                    'controller' => 'postseances',
                    'action' => 'changeStatus',
                    $seance['Seance']['id']], ['type' => 'primary',
                    'class' => 'waiter',
                    'data-waiter-title' => __('Figer les débats'),
                    'disabled' => $infoFormatSortie === 'odt' ? 'disabled' : null,
                    'escapeTitle' => false,
                    'confirm' => __('Voulez-vous figer les documents ?'),
                    'name' => 'Clore',
                    'title' => __('Figer les documents')]) .
                $this->Bs->btn($this->Bs->icon('cog') . ' <span class="caret"></span>', '#', ['type' => 'default',
                    'escapeTitle' => false, 'class' => 'dropdown-toggle',
                    'data-toggle' => 'dropdown']) .
                $this->Bs->nestedList([
                    $this->Bs->link($seance['Typeseance']['Modele_pvsommaire']['name'], ['controller' => 'seances', 'action' => 'genereFusionToClient', $seance['Seance']['id'], 'pvsommaire'], [
                        'title' => __('Générer le document de type  PV sommaire pour la séance du %s', $this->Time->i18nFormat($seance['Seance']['date'], '%d/%m/%Y à %k:%M')),
                        'class' => 'waiter',
                        'data-waiter-send' => 'XHR',
                        'data-waiter-type' => 'progress-bar',
                        'data-waiter-callback' => 'getProgress',
                        'data-waiter-title' => __('Génération du PV sommaire en cours')]),
                    $this->Bs->link($seance['Typeseance']['Modele_pvdetaille']['name'], ['controller' => 'seances', 'action' => 'genereFusionToClient', $seance['Seance']['id'], 'pvdetaille'], [
                        'title' => __('Générer le document de type PV complet pour la séance du %s', $this->Time->i18nFormat($seance['Seance']['date'], '%d/%m/%Y à %k:%M')),
                        'class' => 'waiter',
                        'data-waiter-send' => 'XHR',
                        'data-waiter-type' => 'progress-bar',
                        'data-waiter-callback' => 'getProgress',
                        'data-waiter-title' => __('Génération du PV complet en cours')])
                        ], ['class' => 'dropdown-menu', 'role' => 'menu']) .
                $this->Bs->close()
                //////////////////////////////
        );
    }
}
echo $this->Bs->endTable();

//paginate
echo $this->element('paginator', ['paginator' => $this->Paginator]);
