<?php

$nbProjets = isset($this->Paginator) ? $this->Paginator->counter('{:count}') : $nbProjets;
$titre = __('Suivi des délibérations télétransmises') . ' (' . $nbProjets . ' ' . ($nbProjets > 1 ? 'projets' : 'projet') . ')';

$this->Html->addCrumb(__('Post-séances'), ['controller' => 'postseances', 'action' => 'index']);
$this->Html->addCrumb(__('Télétransmises'));

echo $this->element('filtre');

echo $this->Bs->tag('h3', $titre);

echo $this->Bs->div('alert alert-info', __('La Classification enregistrée date du') . ' ' . $this->Time->i18nFormat($dateClassification, '%A %d %B %Y')) .
 $this->Bs->div();

$title = [
    ['title' => $this->Paginator->sort('id', 'Id')],
    ['title' => $this->Paginator->sort('num_delib', __('Numéro'))],
    ['title' => $this->Paginator->sort('objet_delib', __('Libellé'))],
    ['title' => __('Annexe(s)')],
    ['title' => $this->Paginator->sort('num_pref', __('Classification'))],
    ['title' => __('Type de pièce principale')],
    ['title' => __('Envoi complémentaire')],
    ['title' => $this->Paginator->sort('Deliberation.tdt_ar_date', __('Statut TDT'))],
    ['title' => __('Courriers Ministériels')],
    ['title' => __('Actions')]
];

echo $this->Bs->table($title, ['hover', 'striped']);
foreach ($deliberations as $deliberation) {

    echo $this->Bs->cell($this->Html->link($deliberation['Deliberation']['id'], ['controller' => 'projets', 'action' => 'view', $deliberation['Deliberation']['id']]));

    echo $this->Bs->cell($this->Html->link($deliberation['Deliberation']['num_delib'], ['controller' => 'actes', 'action' => 'getTampon', $deliberation['Deliberation']['id']]));
    echo $this->Bs->cell($deliberation['Deliberation']['objet_delib']);

    $annexes = $this->DelibTdt->retourneAnnexes($deliberation);
    echo $this->Bs->cell($annexes);

    echo $this->Bs->cell(!empty($deliberation['Deliberation']['num_pref_libelle']) ? $deliberation['Deliberation']['num_pref_libelle'] : '<em>-- Manquante --</em>');
    echo $this->Bs->cell(!empty($deliberation['Deliberation']['typologiepiece_code_libelle']) ? $deliberation['Deliberation']['typologiepiece_code_libelle'] : '<em>-- Type manquant --</em>');
    echo $this->Bs->cell(!empty($deliberation['Deliberation']['tdt_document_papier']) ? 'Oui' : 'Non');

    $etat=$this->DelibTdt->retourneEtat($deliberation);
    echo $this->Bs->cell($etat);

    $messages = $this->DelibTdt->retourneMessages($deliberation,$tdt,$tdt_host);
    echo $this->Bs->cell($messages);

    $modal = "";
    $cancelSignature='';
    //Logical grep for cancelSignature on specific type
    if (isset($acl)){
        $aclKeys = array_keys($acl);
        $cancelSendTTD = implode("",preg_grep('/(CancelSendTDT\z)/',$aclKeys));
    }
    // Si la délibération
    if (!empty( $deliberation['Deliberation']['num_delib'] )
        && (isset($acl[$cancelSendTTD]) && $acl[$cancelSendTTD])){
        $modal =
            $this->Bs->modal(
                in_array($deliberation['Deliberation']['tdt_status'],[-1,-3]) ? "Changer l’état de votre acte afin de pouvoir le renvoyer vers le TDT" : 'Confirmer l\'annulation de votre acte dans le TDT',
                $this->element('Deliberations/formulaire_annulation_tdt',['deliberation'=>$deliberation,'acl'=>$acl]),
                ['form' => true],
                [
                    'close' => $this->Bs->icon('times-circle') . ' Annuler',
                    'open' => [
                        'name' => $this->Bs->icon('share-square'),
                        'class' => 'btn-danger',
                        'type'=>'danger',
                        'options'=>[
                            'disabled' => $deliberation['Deliberation']['cancelSendTDT'] ? '' : 'disabled',
                            'title' => in_array($deliberation['Deliberation']['tdt_status'],[-1,-3]) ? "Changer l’état de l’acte pour corriger l’erreur."  : "Confirmer l'annulation de télétransmission de l'acte : " . $deliberation['Deliberation']['id'],
                        ]
                    ],
                    'confirm' => ['name' =>$this->Bs->icon('share-square') .' ' . (in_array($deliberation['Deliberation']['tdt_status'],[-1,-3]) ?'Changer l\'état de l\'acte' : 'Confirmer l\'annulation'), 'class' => 'btn-danger', /*'link' => ['controller' => 'deliberations', 'action' => 'cancelSendToTDT']*/]
                ]);
    }

    echo $this->Bs->cell($modal);

}
echo $this->Bs->endTable();
//paginate
echo $this->Paginator->numbers(['before' => '<ul class="pagination">', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a', 'tag' => 'li', 'after' => '</ul><br />']);
