<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Post-séances'), ['controller' => 'postseances', 'action' => 'index']);
$titre = __(
    'Liste des délibérations de la séance : %s du %s à %s',
    $seance["Typeseance"]['libelle'],
    $this->Time->i18nFormat($seance['Seance']['date'], '%d/%m/%Y'),
    $this->Time->i18nFormat($seance['Seance']['date'], '%kh%M')
);
$this->Html->addCrumb($titre);

echo $this->Html->tag(
    'h3',
    $titre
);

$titles[] = ['title' => __('Num Delib')];
$titles[] = ['title' => __('Libellé de l\'acte')];
$titles[] = ['title' => __('Titre')];
$titles[] = ['title' => __('Thème')];
$titles[] = ['title' => __('Rapporteur')];
$titles[] = ['title' => __('Signature')];
if (in_array('ged', $actions, true)) {
    $titles[] = ['title' => __('Export GED')];
}
$titles[] = ['title' => __('Actions')];

echo $this->Bs->table($titles, ['hover', 'striped']);
foreach ($projets as $projet) {
    $etatSignature =  __('Signée');
    if (empty($projet['Deliberation']['signee'] && $projet['Deliberation']['signee'] != false)
        || ($projet['Deliberation']['signee'] == false  && $projet['Deliberation']['etat']!=10)) {
        $etatSignature = __("Non signée");
    } elseif ($projet['Deliberation']['etat']==10) {
        $etatSignature = __("À renvoyer en signature");
    }


    echo $this->Bs->tableCells([
        $projet['Deliberation']['num_delib'],
        $projet['Deliberation']['objet_delib'],
        $projet['Deliberation']['titre'],
        $projet['Theme']['libelle'],
        $projet['Rapporteur']['nom'] . ' ' . $projet['Rapporteur']['prenom'],

        $etatSignature,
        (in_array('ged', $actions, true) ?
            $this->Bs->div('btn-group') .
                $this->Bs->btn($this->Bs->icon('cloud-upload-alt').' '.__('(%s)', $projet['Deliberation']['numero_depot']), [
                    'controller' => 'ExportGed',
                    'action' => 'sendToGedDeliberation', $projet['Deliberation']['id']], [
                    'type' => 'primary',
                    'title' => __('Envoyer en GED'),
                    'escapeTitle' => false])
                :'').$this->Bs->close()
        ,
        $this->Bs->div('btn-group-vertical') .
        $this->Bs->btn($this->Bs->icon('download'), ['controller' => 'actes', 'action' => 'download', $projet['Deliberation']['id']], ['type' => 'default',
            'escapeTitle' => false,
            'title' => __('Télécharger la délibération au format PDF')]) .
        $this->Bs->close()
    ]);
}
echo $this->Bs->endTable() .
 $this->Bs->div('btn-group') .
 $this->Html2->btnCancel() .
 $this->Bs->close() ;
