<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Séquences'), ['action' => 'index']);

if ($this->Html->value('Sequence.id')) {
    $this->Html->addCrumb(__('Modification d\'une séquence'));
    echo $this->Bs->tag('h3', __('Modifier la séquence')) .
    $this->BsForm->create('Sequence', [
        'prefix' => $this->params['prefix'],
        'url' => [
            'controller' => 'sequences',
            'action' => 'edit'
        ],
        'type' => 'post']);
} else {
    $this->Html->addCrumb(__('Ajout d\'une séquence'));
    echo $this->Bs->tag('h3', __('Ajouter une séquence')) .
    $this->BsForm->create('Sequence', [
        'prefix' => $this->params['prefix'],
        'url' => [
            'controller' => 'sequences',
            'action' => 'add'
        ],
        'type' => 'post']);
}

echo $this->Bs->div('required') .
 $this->BsForm->input('Sequence.nom', [
    'label' => __('Nom') . ' <acronym title="obligatoire">(*)</acronym>',
    'autocomplete' => 'off'
     ]) .
 $this->Bs->close() .
 $this->Bs->div('spacer') . $this->Bs->close() .
 $this->Bs->div('required') .
 $this->BsForm->input('Sequence.commentaire', [
    'label' => __('Commentaire')]) .
 $this->Bs->close() .
 $this->Bs->div('spacer') . $this->Bs->close() .
 $this->Bs->div('required');

if (Configure::read('INIT_SEQ') && ($this->action == 'admin_add' || $this->action == 'admin_edit')) {
    echo $this->BsForm->input(
            'Sequence.num_sequence',
        [
        'label' => __('Numéro de séquence'),
        'default' => 0]
    );
} elseif ($this->action == 'admin_edit') {
    echo $this->BsForm->input(
            'Sequence.num_sequence',
        [
        'label' => __('Numéro de séquence'),
        /*'disabled' => true*/]
    );
} elseif ($this->action == 'admin_add') {
    echo $this->BsForm->input(
            'Sequence.num_sequence',
        [
        'label' => __('Numéro de séquence'),
        'type' => 'number',
        'readonly' => true,
        'value' => 0]
    );
}
$this->BsForm->setLeft(5);
echo $this->Bs->close() .
 $this->Bs->div('spacer') . $this->Bs->close() .
 $this->Form->hidden('Sequence.id') .
 $this->Html2->btnSaveCancel('', $previous) .
 $this->Form->end();
