<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Séquences'));

echo $this->Bs->tag('h3', __('Liste des séquences'));
if ($this->permissions->check('Sequences', 'create')) {
    echo $this->Html2->btnAdd(__("Ajouter une séquence"), __("Ajouter"));
}

echo $this->Bs->table([['title' => __('Libellé')], ['title' => __('Commentaire')], ['title' => __('Numéro de séquence')], ['title' => __('Actions')]], ['hover', 'striped']);
foreach ($sequences as $sequence) {
    $actions = $this->Bs->div('btn-group');

    if ($this->permissions->check('Sequences', 'read')) {
        $actions .= $this->Bs->btn($this->Bs->icon('eye', null, ['stylePrefix' => 'far']), ['controller' => 'sequences', 'action' => 'view', $sequence['Sequence']['id']], ['type' => 'default', 'escape' => false, 'title' => __('Visualiser')]);
    }
    if ($this->permissions->check('Sequences', 'update')) {
        $actions .= $this->Bs->btn($this->Bs->icon('pencil-alt'), ['controller' => 'sequences', 'action' => 'edit', $sequence['Sequence']['id']], ['escape' => false, 'type' => 'primary', 'title' => __('Modifier')]);
    }
    if ($this->permissions->check('Sequences', 'delete')) {
        $actions .= $this->Bs->btn($this->Bs->icon('trash'), ['controller' => 'sequences', 'action' => 'delete', $sequence['Sequence']['id']], ['escape' => false, 'type' => 'danger', 'title' => __('Supprimer'), 'class' => !empty($sequence['Compteur']) ? 'disabled' : '', 'confirm' => __('Êtes-vous sûr de vouloir supprimer %s ?', $sequence['Sequence']['nom'])]);
    }

    $actions .= $this->Bs->close();

    echo $this->Bs->tableCells([$sequence['Sequence']['nom'], $sequence['Sequence']['commentaire'], $sequence['Sequence']['num_sequence'], $actions]);
}
echo $this->Bs->endTable();
