<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Séquences'), ['action' => 'index']);
$this->Html->addCrumb($sequence['Sequence']['nom']);

$panel_left = '<b>' . __('Libelle') . ' : </b>' . $sequence['Sequence']['nom'] . '<br />' .
        '<b>' . __('Numéro de la séquence') . ' : </b>' . $sequence['Sequence']['num_sequence'] . '<br />' .
        '<b>' . __('Date de création') . ' : </b>' . $sequence['Sequence']['created'] . '<br />';
$panel_right = '<b>' . __('Commentaire') . ' : </b>' . $sequence['Sequence']['commentaire'] . '<br />' .
        '<b>' . __('Date de modification') . ' : </b>' . $sequence['Sequence']['modified'] . '<br />';

echo $this->Bs->tag('h3', __('Séquence')) .
 $this->Bs->panel(__('Fiche séquence') . ' : ' . $sequence['Sequence']['nom']) .
 $this->Bs->row() .
 $this->Bs->col('xs6') . $panel_left .
 $this->Bs->close() .
 $this->Bs->col('xs6') . $panel_right .
 $this->Bs->close(2) .
 $this->Bs->endPanel() .
 $this->Bs->row() .
 $this->Bs->col('md4 of5') .
 $this->Bs->div('btn-group', null, ['id' => "actions_fiche"]) .
 $this->Html2->btnCancel() .
(
    $this->permissions->check('Sequences', 'update')
        ? $this->Bs->btn($this->Bs->icon('pencil-alt') . ' ' . __('Modifier'), ['controller' => 'sequences', 'action' => 'edit', $sequence['Sequence']['id']], ['type' => 'primary', 'escapeTitle' => false, 'title' => __('Modifier')])
        : ''
) .
 $this->Bs->close(6);
