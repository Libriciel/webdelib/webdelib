<?php

$this->Html->addCrumb(
    __('Séances à traiter'),
    [
        'controller' => 'seances',
        'action' => 'index'
    ]
);
$this->Html->addCrumb(
    __('Séance du ')
    . $this->Time->i18nFormat($date_seance, '%d/%m/%Y')
    . __(' à ')
    . $this->Time->i18nFormat($date_seance, '%kh%M')
);

$attribute = [];
$attribute['attributes']['id'] = 'tableDetailAvis';
$attribute['attributes']['name'] = 'tableDetailAvis';

echo $this->Bs->tag('h3', __('Liste des projets : ') . $seance['Typeseance']['libelle'] . __(' du ') . $this->Time->i18nFormat($date_seance, '%d/%m/%Y à %k:%M')) .
    $this->Bs->div('deliberations') .
 $this->Bs->table(
     [
        ['title' => __('Ordre')],
        ['title' => __('Id.')],
        ['title' => __('État')],
        ['title' => __('Theme')],
        ['title' => __('Service émetteur')],
        ['title' => __('Rapporteur')],
        ['title' => __('Libellé de l\'acte')],
        ['title' => __('Résultat')],
        ['title' => __('Actions')],
     ],
     ['striped'],
     $attribute
 );

foreach ($this->data as $projet) {
    //cellule avis
    $cellDelibAvis = '';
    if ($projet['Deliberation']['avis'] === true) {
        $cellDelibAvis = $this->Bs->icon('thumbs-up', ['2x'], [
          'title' => __('Avis favorable'),
          'class' => 'text-success text-center']);
    } elseif ($projet['Deliberation']['avis'] === false) {
        $cellDelibAvis = $this->Bs->icon('thumbs-down', ['2x'], [
          'title' => __('Avis défavorable'),
          'class' => 'text-danger text-center']);
    }
    //cellule actions
    //@Deprecated : $this->Bs->lineAttributes(array('class'=>'actions'));
    $actions = $this->Bs->div('btn-group-vertical');
    $actions.= $this->Bs->btn($this->Bs->icon('comments'), [
        'controller' => 'debats', 'action' => 'edit', $projet['Deliberation']['id'], $seance_id], [
        'type' => 'default',
        'escapeTitle' => false,
        'class'=>'btn-action-m',
        'title' => __('Saisir les debats du projet'),
    ]);
    $actions.= $this->Bs->btn($this->Bs->icon('hand-point-up'), [
        'controller' => 'avis', 'action' => 'edit',
        $projet['Deliberation']['id'], $seance_id], [
        'type' => 'primary',
        'escapeTitle' => false,
        'class'=>'btn-action-m',
        'title' => __('Donner un avis'),
    ]);

    if (is_bool($projet['Deliberation']['avis'])) {
        $actions .= $this->Bs->btn($this->Bs->icon('undo'), [
            'controller' => 'avis', 'action' => 'delete',
            $projet['Deliberation']['id'], $seance_id], [
            'type' => 'warning',
            'escapeTitle' => false,
            'class' => 'btn-action-m',
            'confirm' => __('Réinitialiser l\'avis pour le projet'),
            'title' =>  __('Réinitialiser l\'avis pour le projet'),
        ]);
    }
    if (in_array('genereFusionToClientByModelTypeNameAndSeanceId', $projet['btn_actions'], true)) {
        $actions.=$this->Bs->btn(
            $this->Bs->icon('cog'),
            [
            'controller' => 'projets', 'action' => 'genereFusionToClientByModelTypeNameAndSeanceId', ($projet['Deliberation']['avis'] !== null ? 'deliberation' : 'projet'), $seance_id, $projet['Deliberation']['id']
        ],
            [
                'type' => 'default',
                'class' => 'waiter  btn-action-m',
                'escapeTitle' => false,
                'data-waiter-send' => 'XHR',
                'data-waiter-callback' => 'getProgress',
                'data-waiter-type' => 'progress-bar',
                'data-waiter-title' => __('Générer le document du projet'),
                'title' => __('Générer le document du projet')
            ]
        );
    }

    $actions.= $this->Bs->close();

    echo $this->Bs->cell($projet['DeliberationSeance']['position']);
    echo $this->Bs->cell($this->Bs->link($projet['Deliberation']['id'], ['controller' => 'projets', 'action' => 'view', $projet['Deliberation']['id']]));

    if (!empty($projet['iconeEtat']['image'])) {
        if (in_array($projet['iconeEtat']['image'], ['encours', 'circuit_user_fini', 'attente_circuit', 'circuit_user_atraiter', 'circuit_redacteur_attente', 'circuit_attente'], true)) {
            $projet['iconeEtat']['status'] = 'warning';
        }
    }

    $etat = $this->Bs->btn(
        $this->Bs->icon($this->ProjetUtil->etat_icon($projet['iconeEtat']['image']), ['xs']),
        null,
        [
            'tag' => 'button',
            'type' => empty($projet['iconeEtat']['status']) ? $this->ProjetUtil->etat_icon_type($projet['iconeEtat']['image']) : $projet['iconeEtat']['status'],
            'size' => 'xs',
            'option_type' => 'button',
            //'disabled'=>'disabled',
            'data-toggle' => 'popover',
            'data-content' => $this->ProjetUtil->etat_icon_help($projet['iconeEtat']['image']),
            'data-placement' => 'right',
            'escape' => false,
            'title' => $projet['iconeEtat']['titre']
        ]
    );

    echo $this->Bs->cell($etat);
    echo $this->Bs->cell($projet['Theme']['libelle']) .
        $this->Bs->cell($projet['Service']['libelle']) .
    $this->Bs->cell($projet['Rapporteur']['nom'] . ' ' . $projet['Rapporteur']['prenom']) .
    $this->Bs->cell($projet['Deliberation']['objet_delib']) .
    $this->Bs->cell($cellDelibAvis) .
    $this->Bs->cell($actions);
}
echo $this->Bs->endTable() . $this->Bs->close();

//SPACER
echo $this->Bs->div('spacer') . $this->Bs->close() . $this->Html2->btnCancel($previous);
