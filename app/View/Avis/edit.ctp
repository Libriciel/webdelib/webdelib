<?php

$this->Html->addCrumb(__('Séances à traiter'), [
    'controller' => 'seances', 'action' => 'index']
);

$this->Html->addCrumb(
    __('Séance du ')
    . $this->Time->i18nFormat($seance['Seance']['date'], '%d/%m/%Y à %kh%M'),
    [
        'controller' => 'avis', 'action' => 'index',  $seance['Seance']['id']
    ]
);

echo $this->Bs->tag('h3', __('Donner un avis pour le projet :')
    . ' ' . $this->Html->value('Deliberation.objet')
);

echo $this->BsForm->create('Deliberation', [
    'url' => [
        'controller' => 'avis',
        'action' => 'edit',
        $this->Html->value('Deliberation.id'),
        $seance_id
    ],
    'type' => 'post']
);

echo $this->BsForm->radio('Deliberation.avis', $avis, ['fieldset' => false, 'legend' => false, 'label' => __('Donner un avis') . ' <abbr title="obligatoire">*</abbr>', 'type' => 'radio', 'class' => 'btn', 'style' => 'margin-left:5px;margin-right:5px;', 'value' => $avis_selected]);

echo $this->BsForm->input('Deliberation.commentaire', ['fieldset' => false, 'legend' => false, 'label' => __('Commentaire'), 'maxlength' => '1000', 'value' => $commentaire, 'type' => 'textarea']);

echo $this->BsForm->select('Deliberation.seance_id', $seances, ['label' => __('Attribuer une nouvelle séance'), 'class' => 'selectmultiple', 'multiple' => true, 'selected' => $seances_selected]);

echo $this->Form->hidden('Deliberation.id');
echo $this->Html2->btnSaveCancel('', $previous);
echo $this->Form->end();
