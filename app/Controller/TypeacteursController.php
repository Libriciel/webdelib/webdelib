<?php

/**
 * Contrôleur des Types d'acteurs
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       web-delib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 * @version     v4.3
 * @package     app.Controller
 */
class TypeacteursController extends AppController
{
    public $name = 'Typeacteurs';
    public $helpers = [];
    public $components = [
        'Auth' => [
            'mapActions' =>
                [
                    'create' => ['admin_add'],
                    'read' => ['admin_index', 'admin_view'],
                    'update' => ['admin_edit'],
                    'delete' => ['admin_delete'],
                ]
        ]
    ];

    /** Affiche la liste des types acteurs
     * @version 5.0
     * @since 4.3
     * @access public
     */
    public function admin_index() // phpcs:ignore
    {
        $typeacteurs = $this->Typeacteur->find('all', [
            'recursive' => -1,
            'order' => 'id ASC',
            'conditions' => ['Typeacteur.actif' => true],
        ]);
        for ($i = 0, $iMax = count($typeacteurs); $i < $iMax; $i++) {
            $typeacteurs[$i]['Typeacteur']['is_deletable'] =
                $this->Typeacteur->isDeletable($typeacteurs[$i]['Typeacteur']['id']);
        }
        $this->set('typeacteurs', $typeacteurs);
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     */
    public function admin_view($id) // phpcs:ignore
    {
        $typeacteur = $this->Typeacteur->find('first', ['conditions' => ['Typeacteur.id' => $id]]);
        if (empty($typeacteur)) {
            $this->Flash->set(
                __('ID invalide pour le type d\'acteur'),
                ['element' => 'growl', 'params'=>['type' => 'danger']]
            );
            $this->redirect(['action' => 'index']);
        } else {
            $this->set('typeacteur', $typeacteur);
        }
    }

    /**
     * @version 4.3
     * @access public
     */
    public function admin_add() // phpcs:ignore
    {
        if (!empty($this->request->data)) {
            $this->Typeacteur->create();
            if ($this->Typeacteur->save($this->request->data)) {
                $this->Flash->set(
                    __('Le type d\'acteur "%s" a été ajouté', $this->request->data['Typeacteur']['nom']),
                    ['element' => 'growl']
                );
                $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Veuillez corriger les erreurs ci-dessous.'),
                    ['element' => 'growl', 'params'=>['type' => 'danger']]
                );
            }
        }

        $this->set('eluNonElu', ['1' => 'élu', '0' => 'non élu']);

        $this->render('admin_edit');
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     */
    public function admin_edit($id = null) // phpcs:ignore
    {
        if ($this->request->isPost()) {
            if ($this->Typeacteur->save($this->request->data)) {
                $this->Flash->set(
                    __('Le type d\'acteur "%s" a été modifié', $this->request->data['Typeacteur']['nom']),
                    ['element' => 'growl']
                );

                $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Veuillez corriger les erreurs ci-dessous.'),
                    ['element' => 'growl', 'params'=>['type' => 'danger']]
                );
            }
        }
        $this->request->data = $this->Typeacteur->find('first', [
            'conditions'=> [
                'Typeacteur.id' => $id
            ],
            'recursive' => -1
        ]);

        if ($this->request->data == false) {
            $this->Flash->set(
                __('ID invalide pour le type d\'acteur'),
                ['element' => 'growl', 'params'=>['type' => 'danger']]
            );
            $this->redirect($this->previous);
        }

        $this->set('eluNonElu', ['1' => 'élu', '0' => 'non élu']);
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     */
    public function admin_delete($id = null) // phpcs:ignore
    {
        $typeacteur = $this->Typeacteur->read('id, nom', $id);

        if (empty($typeacteur)) {
            $this->Flash->set(
                __('ID invalide pour le type d\'acteur'),
                ['element' => 'growl', 'params'=>['type' => 'danger']]
            );
        } elseif (!$this->Typeacteur->isDeletable($id)) {
            $this->Flash->set(
                __(
                    'Le type d\'acteur "%s" est utilisé par un acteur. Suppression impossible.',
                    $typeacteur['Typeacteur']['nom']
                ),
                ['element' => 'growl', 'params'=>['type' => 'danger']]
            );
        } elseif ($this->Typeacteur->delete($id)) {
            $this->Flash->set(
                __('Le type d\'acteur "%s" a été supprimé', $typeacteur['Typeacteur']['nom']),
                ['element' => 'growl']
            );
        }
        $this->redirect($this->previous);
    }
}
