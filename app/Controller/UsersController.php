<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

use Libriciel\Utility\Password\PasswordStrengthMeterInterface;

App::import('Vendor', 'wdPasswordStrengthMeterAnssi', ['file' => 'wd_password_strength_meter_anssi.php']);

/**
 * [UsersController description]
 * @version 5.2.0
 * @since 1.0.0
 */
class UsersController extends AppController
{
    /**
     * $uses
     * @var [array]
     */
    public $uses = [
        'User',
        'ServiceUser',
        'Collectivite',
        'Service',
        'Cakeflow.Circuit',
        'Profil',
        'Typeacte',
        'Aro',
        'Aco'
    ];
    /**
     * $components
     * @var [array]
     */
    public $components = [
        'Auth' => [
            'mapActions' => [
                'read' => [
                    'getAdresse',
                    'getCP',
                    'getNom',
                    'getPrenom',
                    'getVille',
                    'admin_view',
                    'admin_index', 'manager_index', 'manager_view',
                ],
                'create' => [
                    'admin_add', 'manager_add'
                ],
                'update' => [
                    'admin_edit', 'manager_edit',
                    'admin_changeMdp', 'manager_changeMdp',
                    'admin_acl_edit', 'manager_acl_edit',
                    'admin_enable', 'manager_enable',
                    'admin_disable', 'manager_disable'
                ],
                'delete' => ['admin_delete', 'manager_delete'],
                'allow' => [
                    'login',
                    'logout',
                    'casLogin',
                    'casLogout',
                    'changeTheme',
                    'view'
                ]
            ]
        ],
        'AuthManager.AclManager',
        'Filtre',
        'Paginator',
        'TokensMethods',
        'RequestHandler'
    ];

    /**
     *
     * @version 4.3.0
     */
    public function admin_index() // phpcs:ignore
    {
        $this->index();
    }

    /**
     * @access public
     *
     * @version 4.3.0
     */
    public function manager_index() // phpcs:ignore
    {
        $this->index(['Service.id' => 'update', 'Role.id' => 1]);

        $this->render('admin_index');
    }

    //FIXME -- optimisation

    /**
     * Liste des utilisateurs
     *
     * @access private
     *
     * @param type $allow
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    private function index($allow = null)
    {
        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);

        if (!$this->Filtre->critereExists()) {
            //Définition d'un champ virtuel pour affichage complet des informations
            $this->User->virtualFields['name'] = 'User.prenom || \' \' || User.nom || \' (\' || User.username || \')\'';

            $users = $this->User->find('list', [
                'fields' => ['id', 'name'],
                'order' => 'username',
                'recursive' => -1,
                'allow' => $allow
            ]);
            $this->Filtre->addCritere('Utilisateur', [
                'field' => 'User.id',
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Nom complet'),
                    'title' => __('Recherche sur par nom, prénom et login'),
                    'data-placeholder' => __('Sélectionner un utilisateur'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'options' => $users],
                'column' => 2
            ]);

            $profils = $this->Profil->find('list', ['fields' => ['id', 'name']]);
            $this->Filtre->addCritere('Profil', [
                'field' => 'User.profil_id',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Profil'),
                    'data-placeholder' => __('Sélectionner un profil des utilisateurs recherchés'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'options' => $profils],
                'column' => 2
            ]);

            $this->Filtre->addCritere('Nom', [
                'field' => 'User.nom',
                'inputOptions' => [
                    'label' => __('Nom'),
                    'type' => 'text',
                    'title' => __('Filtre sur les noms des utilisateurs')],
                'column' => 2
            ]);

            $this->Filtre->addCritere('Prenom', [
                'field' => 'User.prenom',
                'retourLigne' => true,
                'class' => 'selectone',
                'inputOptions' => [
                    'label' => __('Prénom'),
                    'type' => 'text',
                    'title' => __('Filtre sur les prénoms des utilisateurs')],
                'column' => 2
            ]);

            $this->Filtre->addCritere('Login', [
                'field' => 'User.username',
                'inputOptions' => [
                    'label' => __('Login'),
                    'type' => 'text',
                    'title' => __('Filtre sur les logins des utilisateurs')],
                'column' => 2
            ]);

            $this->Filtre->addCritere('Actif', [
                'field' => 'User.active',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Actif'),
                    'options' => [0 => __('Non'), 1 => __('Oui')],
                    'data-placeholder' => __('Sélectionner un état'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'title' => __('Filtre sur les états des utilisateurs')],
                'column' => 2
            ]);

            //Affichage des utilisateurs actifs par défaut
            $this->Filtre->setCritere('Actif', 1);

            $this->Filtre->addCritere(
                'Service',
                [
                    'field' => 'Service.id',
                    'inputOptions' => [
                        'type' => 'select',
                        'label' => __('Services'),
                        'escape' => false,
                        'multiple' => true,
                        'title' => __('Service du(des) utilisateur(s) recherché(s)'),
                        'options' => $this->Service->generateTreeList(
                            ['Service.actif' => '1'],
                            null,
                            null,
                            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                        )
                    ],
                    'column' => 2]
            );

            $this->Filtre->addCritere(
                'SousService',
                [
                    'field' => 'SousService',
                    'retourLigne' => true,
                    'inputOptions' => [
                        'type' => 'select',
                        'label' => __('Sous services'),
                        'escape' => false,
                        'data-placeholder' => __('Inclure les sous-services dans la recherche ? '),
                        'title' => __('Sous service du service principal'),
                        'options' => [0 => __('Non'), 1 => __('Oui')]
                    ],
                    'column' => 2]
            );

            //Affichage des utilisateurs actifs par défaut
            $this->Filtre->setCritere('SousService', 1);
        }

        $conditions = $this->Filtre->conditions();

        //On doit ajouter la condition sur la classe associated
        // car le filtreComponent ne prend pas en charge les model associated
        //TODO : Refactoring into FiltreComponent
        $containServiceCondition = [];
        if (isset($conditions['AND']['Service.id'])) {
            $children = $this->Service->find('all', [
                'fields' => ['id'],
                'conditions' => ['Service.parent_id' => $conditions ['AND']['Service.id']],
                'recursive' => -1]);
            if ($conditions['AND']['SousService'][0] === 1 && count($children) > 0) {
                $childrenIds = Hash::extract($children, '{n}.Service.id');
                $conditions['AND']['Service.id'] = empty($childrenIds) ?:
                    array_merge($childrenIds, $conditions['AND']['Service.id']);
            }
            $subQuery = [
                'alias' => 'services_users',
                'fields' => ['services_users.user_id'],
                'conditions' => [
                    'services_users.user_id = User.id',
                    'services_users.service_id' => $conditions['AND']['Service.id']
                ]
            ];
            $sql = $this->User->ServicesUser->sql($subQuery);
            $containServiceCondition = [
                'conditions' => [
                    'Service.id' => $conditions['AND']['Service.id']],
                'order' => ['Service.lft' => 'ASC']
            ];
            //On unset car Service.id n'est pas directement accessible pour l'entité User
            unset($conditions['AND']['Service.id']);
            $conditions['AND'][] = ["User.id IN ( $sql )"];
        } else {
            $containServiceCondition = ['order' => ['Service.lft' => 'ASC']];
        }
        //TODO : rendre "disable" le select SousService si pas de service selectionné
        if (isset($conditions['AND']['SousService'])) {
            unset($conditions['AND']['SousService']);
        }

        $this->paginate = ['User' => [
            'countField' => 'DISTINCT User.id',
            'fields' => [
                'DISTINCT User.id',
                'User.username',
                'User.nom',
                'User.prenom',
                'User.telfixe',
                'User.telmobile',
                'User.active'
            ],
            'contain' => [
                'Service' => $containServiceCondition,
                'Profil.name'
            ],
            'conditions' => $conditions,
            'order' => ['User.username' => 'asc'],
            'limit' => 20,
            'recursive' => -1,
            'allow' => $allow
        ]
        ];
        $users = $this->Paginator->paginate('User');

        $i = 1;
        foreach ($users as &$user) {
            $user['Typeacte'] = $this->User->getTypeActes($user['User']['id']);
            // FIXME Optimiser pour diminuer le nombre de requêtes quand grosse bdd!!
            $user['User']['is_deletable'] = $this->isDeletable($user, $message);

            if ($i == 20) {
                break;
            }
            $i++;
        }

        $this->set('users', $users);
    }

    /**
     * Visualiser un utilisateur
     *
     * @access private
     *
     * @param type $id
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function view($id)
    {
        $user = $this->User->find('first', [
            'conditions' => ['User.id' => $id],
            'contain' => ['Service', 'Profil'],
            'recursive' => -1,
        ]);
        if (!$user) {
            $this->Flash->set(__('Utilisateur introuvable'), ['element' => 'growl']);
            $this->redirect($this->previous);
        } else {
            $this->set('user', $user);
            $this->set('circuitDefautLibelle', $this->User->circuitDefaut($id, 'nom'));
        }

        $this->render('view');
    }

    /**
     * Visualiser un utilisateur pour l'admin
     *
     * @access public
     *
     * @param type $id
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function admin_view($id) // phpcs:ignore
    {
        $this->view($id);

        $this->render('admin_view');
    }

    /**
     * Visualiser un utilisateur pour le manager
     *
     * @access public
     *
     * @param type $id
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function manager_view($id) // phpcs:ignore
    {
        $this->view($id);

        $this->render('admin_view');
    }

    /**
     * ajouter un utilisateur pour l'admin
     *
     * @access public
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function admin_add() // phpcs:ignore
    {
        $this->add_edit(null);
        $this->render('admin_edit');
    }

    /**
     * Ajouter un utilisateur pour le manager
     *
     * @access public
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function manager_add() // phpcs:ignore
    {
        $this->add_edit(null, ['Service.id' => 'create']);
        $this->render('admin_edit');
    }

    /**
     * Modifier un utilisateur pour l'admin
     *
     * @access public
     *
     * @param type $id
     *
     * @version 5.1.0
     * @version 4.3.0
     */
    public function admin_edit($id = null) // phpcs:ignore
    {
        $this->add_edit($id);
    }

    /**
     * Modifier un utilisateur pour le manager
     *
     * @access public
     *
     * @param type $id
     *
     * @version 5.1.0
     * @version 4.3.0
     */
    public function manager_edit($id = null) // phpcs:ignore
    {
        $this->add_edit($id, ['Service.id' => 'update', 'Role.id' => 1]);

        $this->render('admin_edit');
    }

    /**
     * Modifier un utilisateur
     *
     * @access private
     *
     * @param type $id
     * @param type $allow
     *
     * @version 5.1.0
     * @version 4.3.0
     */
    private function add_edit($id = null, $allow = null) // phpcs:ignore
    {
        $created= false;
        if (!empty($id)) {
            $user = $this->User->find('first', [
                'conditions' => ['User.id' => $id],
                'contain' => ['Service', 'Circuit'],
                'recursive' => -1,
                'allow' => $allow]);
            if (!$user) {
                $this->Flash->set(
                    __('ID invalide pour l\'utilisateur'),
                    ['element' => 'growl', 'params' => ['type' => 'warning']]
                );
                $this->redirect($this->previous);
            }
        }

        if ($this->request->is('Post')) {
            if (!empty($id)) {
                $this->User->id = $id;
                $this->User->recursive = -1;
                $this->User->validator()->remove('password')->remove('password2');
                $lastProfilId = $this->User->field('profil_id');
            } else {
                $this->User->create();
                $created= true;
            }

            //Transformation type de donnée
            if (!empty($this->request->data['Service']['Service'])) {
                $this->request->data['Service']['Service'] = $this->ServiceUser->setServiceUserPermissions(
                    explode(',', $this->request->data['Service']['Service']),
                    $this->request->param('manager'),
                    $id
                );
            }

            // Réinitialiser le circuit par defaut.
            //Si le circuit par defaut n'est pas posté alors il doit être mis à null
            $this->request->data['User']['circuit_defaut_id'] =
                !isset($this->request->data['User']['circuit_defaut_id']) ? null
                    : $this->request->data['User']['circuit_defaut_id'];

            if ($this->User->save($this->data, ['request' => true])) {
                $this->Aro->recursive = -1;
                $aroProfil = $this->Aro->findByForeignKeyAndModel($this->request->data['User']['profil_id'], 'Profil');
                $permissions = $this->Aro->Permission->find('all', [
                    'conditions' => [
                        'aro_id' => $aroProfil['Aro']['id']
                    ],
                    'joins' => [
                        $this->Aco->Permission->join(
                            'Aco',
                            ['type' => 'INNER', 'conditions' => [
                                'model' => null,
                            ]]
                        )
                    ],
                    'recursive' => -1]);

                if (empty($id)) {
                    $this->User->id = $this->User->getInsertID();

                    //Ajout des droits du profil
                    $aroUser = $this->Aro->findByForeignKeyAndModel($this->User->id, 'User');
                    foreach ($permissions as $permission) {
                        $this->Aro->Permission->create();
                        $acoAro['Permission']['aro_id'] = $aroUser['Aro']['id'];
                        $acoAro['Permission']['aco_id'] = $permission['Permission']['aco_id'];
                        foreach ($this->Aro->Permission->getAcoKeys($this->Aro->Permission->schema()) as $permKeys) {
                            $acoAro['Permission'][$permKeys] = $permission['Permission'][$permKeys];
                        }

                        $this->Aro->Permission->save($acoAro);
                        unset($acoAro);
                    }
                } elseif ((int)$this->request->data['User']['profil_id'] !== $lastProfilId) {
                    //Modification des droits utilisateur
                    $aroUser = $this->Aro->findByForeignKeyAndModel($this->User->id, 'User');
                    $db = $this->User->getDataSource();
                    $this->User->query(sprintf(
                        "
                        DELETE FROM %s WHERE aco_id IN (SELECT id FROM %s WHERE model IS NULL) AND aro_id='%s'",
                        $db->fullTableName('aros_acos'),
                        $db->fullTableName('acos'),
                        $aroUser['Aro']['id']
                    ));
                    foreach ($permissions as $permission) {
                        $this->Aro->recursive = -1;
                        $acoAro = $this->Aro->Permission->findByAcoIdAndAroId(
                            $permission['Permission']['aco_id'],
                            $aroUser['Aro']['id']
                        );
                        if (empty($acoAro)) {
                            $this->Aro->Permission->create();
                            $acoAro['Permission']['aro_id'] = $aroUser['Aro']['id'];
                            $acoAro['Permission']['aco_id'] = $permission['Permission']['aco_id'];
                        }

                        foreach ($this->Aro->Permission->getAcoKeys($this->Aro->Permission->schema()) as $permKeys) {
                            $acoAro['Permission'][$permKeys] = $permission['Permission'][$permKeys];
                        }

                        $this->Aro->Permission->save($acoAro);
                        unset($acoAro);
                    }
                }

                if (!empty($this->request->data['Aco']['Typeacte'])) {
                    $this->AclManager->setPermissionsTypeacte(
                        'User',
                        $this->User->id,
                        $this->request->data['Aco']['Typeacte']
                    );
                }
                if (!empty($this->request->data['Aco']['Typeseance'])) {
                    $this->AclManager->setPermissionsTypeseance(
                        'User',
                        $this->User->id,
                        $this->request->data['Aco']['Typeseance']
                    );
                }

                $this->Flash->set(
                    __(
                        'L\'utilisateur "%s" a été %s',
                        $this->data['User']['username'],
                        $created ? 'créé' : 'modifié'
                    ),
                    ['element' => 'growl']
                );

                $this->redirect($this->previous);
            } else {
                $this->Flash->set(__('Veuillez corriger les erreurs ci-dessous.'), [
                    'element' => 'growl',
                    'params' => ['type' => 'warning']]);
                $this->set('selectedServices', $this->data['Service']['Service']);
            }
        }

        if (!$this->request->data && !empty($id)) {
            $this->request->data = $user;
        }
        if (!empty($this->data['Service'])) {
            $this->set('selectedServices', Set::extract('/Service', $this->data['Service']));
        }

        $this->AclManager->permissionsTypeacte(
            $id,
            null,
            ['create', 'read', 'update', 'delete'],
            $this->request->param('prefix')
        );
        $this->AclManager->permissionsTypeseance(
            $id,
            null,
            ['create', 'read', 'update', 'delete'],
            $this->request->param('prefix')
        );

        // services pour l'admin
        if ($this->request->param('admin')) {
            $this->set('profils', $this->User->Profil->find('list'));

            $services = $this->User->Service->find('threaded', [
                'recursive' => -1,
                'order' => 'name ASC',
                'fields' => ['id', 'name', 'parent_id'],
                'conditions' => ['Service.actif' => 1],
            ]);
        } else { // services pour lesquels on a les droits uniquement
            $this->set('profils', $this->User->Profil->find('list', ['conditions' => ['role_id' => 1]]));

            $services = $this->User->Service->find('threaded', [
                'fields' => ['id', 'name', 'parent_id'],
                'recursive' => -1,
                'order' => ['name' => 'ASC'],
                'conditions' => ['Service.actif' => 1],
                'allow' => ['Service.id'],
            ]);
        }
        $this->set('notif', ['1' => __('oui'), '0' => __('non')]);
        $this->set('circuits', $this->Circuit->getList());
        $minEntropie = $this->entropyPassword();

        $this->set(compact(['minEntropie', 'services']));
    }

    /**
     * Dans le controleur car utilisé dans la vue index pour l'affichage
     *
     * @param type $user
     * @param type $message
     * @return boolean
     * @version 4.3
     * @access private
     */
    private function isDeletable($user, &$message)
    {
        $this->loadModel('Deliberation');
        if ($user['User']['id'] == 1) {
            $message = __(
                'L\'utilisateur "%s" ne peut pas être supprimé car il est protégé',
                $user['User']['username']
            );
            return false;
        } elseif ($user['User']['id'] == $this->Auth->user('id')) {
            $message = __('L\'utilisateur courant ("%s") ne peut pas être supprimé', $user['User']['username']);
            return false;
        } elseif ($this->Deliberation->find('count', [
            'conditions' => ['Deliberation.redacteur_id' => $user['User']['id']],
            'recursive' => -1])
        ) {
            $message = __(
                'L\'utilisateur "%s" ne peut pas être supprimé car il est l\'auteur de délibérations',
                $user['User']['username']
            );
            return false;
        } else {
            //Si l'utilisateur à des projets A traiter, ne pas permettre la suppression
            $this->loadModel('Cakeflow.Traitement');

            //A traiter
            $nbProjetsATraiter = count($this->Traitement->listeTargetId(
                $user['User']['id'],
                [
                    'etat' => 'NONTRAITE',
                    'traitement' => 'AFAIRE',
                    'joins' => [[
                        'table' => 'deliberations',
                        'alias' => 'Deliberation',
                        'type' => 'INNER',
                        'conditions' => [
                            'Traitement.target_id = Deliberation.id',
                            'Deliberation.etat' => 2,
                            'Deliberation.signee' => false
                        ]
                    ]]
                ]
            ));

            //En cours de validation
            $nbProjetsValidation = count($this->Traitement->listeTargetId(
                $user['User']['id'],
                [
                    'etat' => 'NONTRAITE',
                    'traitement' => 'NONAFAIRE',
                    'joins' => [[
                        'table' => 'deliberations',
                        'alias' => 'Deliberation',
                        'type' => 'INNER',
                        'conditions' => [
                            'Traitement.target_id = Deliberation.id',
                            'Deliberation.etat' => 2,
                            'Deliberation.signee' => false
                        ]
                    ]]
                ]
            ));

            $nbProjets = $nbProjetsATraiter + $nbProjetsValidation;

            if ($nbProjets > 0) {
                $message = __(
                    'L\'utilisateur "%s" ne peut pas être supprimé car il a des projets en cours',
                    $user['User']['username']
                );
                return false;
            }
        }
        return true;
    }

    /**
     * Supression d'un utilisateur pour un admin
     *
     * @access public
     *
     * @param type $id
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function admin_delete($id) // phpcs:ignore
    {
        $this->delete($id, null);
    }

    /**
     * Supression d'un utilisateur pour un manager
     *
     * @access public
     *
     * @param type $id
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function manager_delete($id) // phpcs:ignore
    {
        $this->delete($id, ['Service.id' => 'delete']);
    }

    /**
     * Supression d'un utilisateur
     *
     * @access private
     *
     * @param type $id
     * @param type $allow
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    private function delete($id, $allow = null)
    {
        $messageErreur = '';
        $user = $this->User->find('first', [
            'conditions' => ['User.id' => $id],
            'fields' => ['id', 'username'],
            'allow' => $allow,
            'recursive' => -1]);
        if (empty($user)) {
            $this->Flash->set(__('ID invalide pour l\'utilisateur'), ['element' => 'growl']);
        } elseif (!$this->isDeletable($user, $messageErreur)) {
            $this->Flash->set($messageErreur, ['element' => 'growl', 'params' => ['type' => 'danger']]);
        } elseif ($this->User->delete($id)) {
            $this->Flash->set(
                __('L\'utilisateur "%s" a été supprimé', $user['User']['username']),
                ['element' => 'growl', 'params' => ['type' => 'success']]
            );
        }
        $this->redirect($this->previous);
    }

    /**
     * Activer un compte utilisateur
     *
     * @access public
     * @param type $id
     */
    public function admin_enable($id = null) // phpcs:ignore
    {
        $this->enableUser($id, true);
    }

    /**
     * Désactiver un compte utilisateur
     *
     * @access public
     * @param type $id
     */
    public function manager_disable($id = null) // phpcs:ignore
    {
        $this->enableUser($id, false);
    }

    /**
     * Désactiver un compte utilisateur
     *
     * @access public
     * @param type $id
     */
    public function manager_enable($id = null) // phpcs:ignore
    {
        $this->enableUser($id, true);
    }

    /**
     * Désactiver un compte utilisateur
     *
     * @access public
     * @param type $id
     */
    public function admin_disable($id = null) // phpcs:ignore
    {
        $this->enableUser($id, false);
    }

    /**
     * Active ou desactive un compte utilisateur
     *
     * @access private
     * @param int $id
     * @param boolean $enabled
     * @version 5.1.0
     * @since 1.0.0
     */
    private function enableUser($id = null, $enabled = false)
    {
        $user = $this->User->find('first', [
            'conditions' => ['User.id' => $id],
            'fields' => ['id', 'username', 'User.active'],
            'recursive' => -1]);
        if (empty($user)) {
            $this->Flash->set(__('ID invalide pour l\'utilisateur'), ['element' => 'growl']);
        } else {
            $this->User->id = $id;
            if ($this->User->saveField('active', $enabled)) {
                $this->Flash->set(
                    __(
                        'L\'utilisateur "%s" a été %s',
                        $user['User']['username'],
                        ($enabled) ? 'activé' : 'désactivé'
                    ),
                    ['element' => 'growl', 'params' => ['type' => 'warning']]
                );
            }
        }

        $this->redirect($this->previous);
    }

    /**
     * @access public
     * @param type $id
     * @return string
     */
    public function getNom($id)
    {
        $condition = "User.id = $id";
        $fields = "nom";
        $dataValeur = $this->User->findAll($condition, $fields);
        if (isset($dataValeur['0'] ['User']['nom'])) {
            return $dataValeur['0'] ['User']['nom'];
        } else {
            return '';
        }
    }

    /**
     * @access public
     * @param type $id
     * @return string
     */
    public function getPrenom($id)
    {
        $condition = "User.id = $id";
        $fields = "prenom";
        $dataValeur = $this->User->findAll($condition, $fields);
        if (isset($dataValeur['0'] ['User']['prenom'])) {
            return $dataValeur['0'] ['User']['prenom'];
        } else {
            return '';
        }
    }

    /**
     * @access public
     * @param type $id
     * @return string
     */
    public function getAdresse($id)
    {
        $condition = "User.id = $id";
        $fields = "adresse";
        $dataValeur = $this->User->findAll($condition, $fields);
        if (isset($dataValeur['0'] ['User']['adresse'])) {
            return $dataValeur['0'] ['User']['adresse'];
        } else {
            return '';
        }
    }

    /**
     * @access public
     * @param type $id
     * @return string
     */
    public function getCP($id)
    {
        $condition = "User.id = $id";
        $fields = "CP";
        $dataValeur = $this->User->findAll($condition, $fields);
        if (isset($dataValeur['0'] ['User']['CP'])) {
            return $dataValeur['0'] ['User']['CP'];
        } else {
            return '';
        }
    }

    /**
     * @access public
     * @param type $id
     * @return string
     */
    public function getVille($id)
    {
        $condition = "User.id = $id";
        $fields = "ville";
        $dataValeur = $this->User->findAll($condition, $fields);
        if (isset($dataValeur['0'] ['User']['ville'])) {
            return $dataValeur['0'] ['User']['ville'];
        } else {
            return '';
        }
    }

    /**
     * @access public
     * @return type
     */
    public function casLogin()
    {
        if ($this->Auth->login()) {
            $this->getCommunity();
            return $this->infoUser();
        }

        return $this->redirect(['admin' => false, 'prefix' => false, 'controller' => 'user', 'action' => 'login']);
    }

    private function getCommunity()
    {
        $collectivite = $this->Collectivite->find('first', [
            'fields' => ['logo', 'nom', 'id_entity', 'config_login'],
            'conditions'=> [
                'Collectivite.id' => 1
            ],
            'recursive' => -1
        ]);

        $this->Session->write('Collectivite.nom', $collectivite['Collectivite']['nom']);

        App::uses('File', 'Utility');
        $file = new File(APP . WEBROOT_DIR . DS . 'files' . DS . 'image' . DS . 'logo');
        if (!empty($collectivite['Collectivite']['logo'])) {
            if (!$file->exists()) {
                $file->write($collectivite['Collectivite']['logo'], 'w', true);
            }
            $file->close();
            $this->set('logo_path', $this->base . "/files/image/logo");
        }

        if (!empty($collectivite['Collectivite']['config_login'])) {
            $this->set('loginConfig', $collectivite['Collectivite']['config_login']);
        }
    }

    /**
     * @return type
     * @version 4.3
     * @access public
     */
    public function login()
    {
        unset($this->Auth->authenticate['AuthManager.Cas']);
        $this->layout = 'login';

        $this->getCommunity();
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->TokensMethods->clear();
                return $this->infoUser();
            } else {
                $this->set('loginShowError', 1);
            }
        }

        if (!empty($this->Auth->user())) {
            return $this->eraseUserConnected();
        }

        //$this->History->reset();
        return $this->render();
    }

    /**
     * @return type
     * @throws Exception
     * @version 4.3
     * @access protected
     */
    protected function infoUser()
    {
        try {
            $this->User->Profil->recursive = -1;
            $userGroup = $this->User->Profil->findById($this->Auth->user('profil_id'));
            $this->Session->write('Auth.User.role', $userGroup['Profil']['role_id']);

            $this->User->Service->recursive = -1;
            $userServices = $this->ServiceUser->findAllByUserId($this->Auth->user('id'));
            //$user = $this->User->findByLogin($this->data['User']['username']);
            //services auquels appartient l'agent
            $services = [];
            foreach ($userServices as $service) {
                $services[$service['ServiceUser']['service_id']] =
                    $this->Service->doList($service['ServiceUser']['service_id']);
            }

            if (empty($services)) {
                throw new Exception('$services is empty');
            }

            $this->Session->write('Auth.User.formatSortie', 'pdf');
            $this->Session->write('Auth.User.Service', $services);
            $this->Session->write('Auth.User.ServiceEmetteur.id', key($services));
            $this->Session->write('Auth.User.ServiceEmetteur.name', $services[key($services)]);

            /**
             * Initialisation (filtre) des menus avec les droits utilisateurs
             */
            include(ROOT . DS . APP_DIR . DS . 'Config' . DS . 'menu.ini.php');
            $this->purgeMenuDroit($navbar);
            $this->purgeMenuHtml($navbar);

            $this->Session->write('Auth.Navbar', $navbar);

            $collectivite = $this->Collectivite->find('first', [
                'fields' => ['templateProject', 'id_entity'],
                'conditions'=> [
                    'Collectivite.id' => 1
                ],
                'recursive' => -1
            ]);

            //Mise en session du template des 9 cases
            if (!empty($collectivite['Collectivite']['templateProject'])) {
                $templateProject = json_decode($collectivite['Collectivite']['templateProject'], true);
            } else {
                $templateProject = json_decode($this->Collectivite->getJson9Cases(), true);
            }
            $this->Session->write('Collectivite.templateProject', $templateProject);

            // On stock la collectivite de l'utilisateur en cas de PASTELL
            if (Configure::read('USE_PASTELL')) {
                $this->Session->write('Collectivite.id_entity', $collectivite['Collectivite']['id_entity']);
            }

            $this->Flash->set(__('Bienvenue sur webdelib'), ['element' => 'growl']);

            if ($this->Session->check('Auth.loginRedirect')) {
                $pageRedirection = $this->Session->read('Auth.loginRedirect');
                $this->Session->delete('Auth.loginRedirect');
                return $this->redirect($pageRedirection);
            }

            return $this->redirect($this->Auth->redirectUrl());
        } catch (Exception $e) {
            $this->set(
                'loginShowError',
                __(
                    'Utilisateur non configuré (%s)! Contacter votre administrateur puis réessayez ultérieurement',
                    h($e->getMessage())
                )
            );
            return $this->render();
        }
    }

    /**
     * @access public
     * @return type
     */
    public function logout()
    {
        return $this->eraseUserConnected();
    }

    /**
     * [eraseUserConnected description]
     * @return [type] [description]
     */
    private function eraseUserConnected()
    {
        //on supprime les infos utilisateur de la session
        $this->Session->delete('Auth.Permissions');
        $this->Session->delete('Collectivite');
        $this->History->reset();

        $this->TokensMethods->clear();

        $return = $this->Auth->logout();
        $this->Session->destroy();

        return $this->redirect($return);
    }

    /**
     * @access public
     */
    public function casLogout()
    {
        $this->layout = 'login';
        $this->set('logo_path', $this->base . "/files/image/Libriciel_SCOP.png");
    }

    /**
     * Modify password user
     *
     * @access public
     *
     * @param type $id
     *
     * @version 5.1.0
     * @version 4.3.0
     */
    public function manager_changeMdp($id) // phpcs:ignore
    {
        $this->admin_changeMdp($id);

        $this->render('admin_changeMdp');
    }

    /**
     * @param type $id
     * @version 4.3
     * @access public
     */
    public function admin_changeMdp($id) // phpcs:ignore
    {
        if ($this->request->is('Post')) {
            $this->User->id = $id;

            $this->User->validator()->remove('nom');
            $this->User->validator()->remove('prenom');
            ;
            if ($this->User->save($this->data)) {
                $this->Flash->set(
                    __(
                        'Le mot de passe de l\'utilisateur \'%s\' a été modifié',
                        $this->User->field('username')
                    ),
                    ['element' => 'growl']
                );

                return $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Erreur lors de la saisie des mots de passe.'),
                    ['element' => 'growl', 'params' => ['type' => 'danger']]
                );
            }
        }

        $this->request->data = $this->User->find('first', [
            'conditions' => ['User.id' => $id],
            'recursive' => -1]);

        $minEntropie = $this->entropyPassword();

        $this->set(compact(['minEntropie']));
    }

    private function entropyPassword()
    {
        return $this->User->getMinUserPasswordEntropy();
    }

    /**
     * purgeSubMenuDroit
     *
     * @access private
     *
     * @param type $subMenu
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    private function purgeSubMenuDroit(&$subMenu)
    {
        foreach ($subMenu as $key => &$Menu) {
            switch ($Menu['html']) {
                case 'link':
                    $checkDroit = $Menu['check'][0];
                    // Mise par défaut pour le droit de lecture
                    $checkPermission = !empty($Menu['check'][1]) ? $Menu['check'][1] : 'read';

                    if (isset($Menu['url']['admin'])
                        && $Menu['url']['admin']
                        && !in_array($this->Auth->user('Profil.role_id'), [2, 3], true)
                    ) {
                        unset($subMenu[$key]);
                        break;
                    }
                    // Gestion du manager
                    if (isset($Menu['url']['admin'])
                        && $Menu['url']['admin']
                        && $this->Auth->user('Profil.role_id') === 3
                    ) {
                        if (isset($Menu['manager']) && $Menu['manager'] === false) {
                            unset($subMenu[$key]);
                            break;
                        }
                        $Menu['url']['manager'] = true;
                        $Menu['url']['prefix'] = 'manager';
                        unset($Menu['url']['admin']);
                    }
                    //Vérification du droit sur le lien
                    $aclAdapter = $this->Acl->adapter();
                    $aclAdapter->Permission->Behaviors->load('DynamicDbConfig');
                    $aclAdapter->Aco->Behaviors->load('DynamicDbConfig');
                    $aclAdapter->Aro->Behaviors->load('DynamicDbConfig');
                    $this->Acl->adapter($aclAdapter);
                    if (!$this->Acl->check(
                        ['User' => ['id' => $this->Auth->user('id')]],
                        $checkDroit,
                        $checkPermission
                    )) {
                        unset($subMenu[$key]);
                    }
                    break;
                case 'subMenu':
                    $this->purgeMenuDroit($Menu['content']);
                    break;

                default:
                    break;
            }
        }
    }

    /**
     * purgeMenuDroit
     *
     * @access private
     *
     * @param type $navbar
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    private function purgeMenuDroit(&$navbar)
    {
        //Purge menu droit
        foreach ($navbar as $key => &$menu) {
            if (!empty($menu['subMenu'])) {
                $this->purgeSubMenuDroit($menu['subMenu']);
            }
            if (empty($menu['subMenu'])) {
                unset($navbar[$key]);
            }
        }
    }

    /**
     * purgeSubMenuHtml
     *
     * @access private
     *
     * @param type $subMenu
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    private function purgeSubMenuHtml(&$subMenu)
    {
        $hasContent = false;
        foreach ($subMenu as $key => &$Menu) {
            if ($Menu['html'] != 'divider' && !$hasContent) {
                $hasContent = true;
            }
            if (!$hasContent && $Menu['html'] == 'divider') {
                unset($subMenu[$key]);
            }

            if (isset($previous_value) && $Menu['html'] == 'divider' && $previous_value == 'divider') {
                unset($subMenu[$key]);
            }
            if ($Menu['html'] == 'subMenu') {
                $this->purgeMenuHtml($Menu['content']);
                if (empty($Menu['content'])) {
                    unset($subMenu[$key]);
                }
            }
            $previous_value = $Menu['html'];
        }
        if (isset($previous_value) && $previous_value == 'divider') {
            array_pop($subMenu);
        }
        //Compter le nombre de composant html
        if (!array_key_exists('html', $subMenu)) {
            unset($subMenu);
        }
    }

    /**
     * purgeMenuHtml
     *
     * @access private
     *
     * @param type $navbar
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    private function purgeMenuHtml(&$navbar)
    {
        //Purge menu droit
        foreach ($navbar as $key => &$menu) {
            if (!empty($menu['subMenu'])) {
                $this->purgeSubMenuHtml($menu['subMenu']);
            }
            if (empty($menu['subMenu'])) {
                unset($navbar[$key]);
            }
        }
    }

    /**
     * admin_acl_edit
     *
     * @access public
     *
     * @param type $id
     * @param type $allow
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function admin_acl_edit($id, $allow = null) // phpcs:ignore
    {
        // Vérification de l'utilisateur
        $user = $this->User->find('first', [
            'conditions' => ['User.id' => $id],
            'contain' => [
                'Profil' => ['role_id'],
            ],
            'recursive' => -1,
            'allow' => $allow]);
        if (!$user) {
            $this->Flash->set(
                __('ID invalide pour l\'utilisateur'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            $this->redirect($this->previous);
        }

        if ($this->request->is('Post')) {
            if ($this->request->param('admin')) {
                if (!empty($this->request->data['Aco']['Service'])) {
                    $this->AclManager->setPermissionsService(
                        'User',
                        $user['User']['id'],
                        $this->request->data['Aco']['Service']
                    );
                }
                if (!empty($this->request->data['Aco']['Circuit'])) {
                    $this->AclManager->setPermissionsCircuit(
                        'User',
                        $user['User']['id'],
                        $this->request->data['Aco']['Circuit']
                    );
                }
                if (!empty($this->request->data['Aco']['Typeacte'])) {
                    $this->AclManager->setPermissionsTypeacte(
                        'User',
                        $user['User']['id'],
                        $this->request->data['Aco']['Typeacte']
                    );
                }
                if (!empty($this->request->data['Aco']['Typeseance'])) {
                    $this->AclManager->setPermissionsTypeseance(
                        'User',
                        $user['User']['id'],
                        $this->request->data['Aco']['Typeseance']
                    );
                }
            }

            if (!empty($this->request->data['Aco']['User'])) {
                $this->AclManager->setPermissionsUser('User', $user['User']['id'], $this->request->data['Aco']['User']);
            }

            $this->Flash->set(
                __('L\'utilisateur "%s" a été modifié', $user['User']['username']),
                ['element' => 'growl']
            );

            $this->redirect($this->previous);
        }

        if (!$this->request->data) {
            $this->request->data = $user;
        } else {
            $this->request->data = $this->User->find(
                'first',
                ['conditions' => ['User.id' => $id]]
            );
        }

        $crud = [];
        if ($this->request->param('admin') && in_array((int)$user['Profil']['role_id'], [3], true)) {
            $this->AclManager->permissionsTypeacte(
                $id,
                null,
                ['manager'],
                $this->request->param('prefix')
            );
            $this->AclManager->permissionsTypeseance(
                $id,
                null,
                ['manager'],
                $this->request->param('prefix')
            );
            $this->AclManager->permissionsService(
                $id,
                null,
                ['read', 'update', 'delete'],
                $this->request->param('prefix')
            );
            $this->AclManager->permissionsCircuit(
                $id,
                'Cakeflow',
                ['read', 'update', 'delete'],
                $this->request->param('prefix')
            );
            $crud[] = 'manager';
        }

        $filter = [
            'role' => array_search(
                (int)$user['Profil']['role_id'],
                ['user' => 1, 'admin' => 2, 'manager' => 3],
                true
            )
        ];

        $this->AclManager->permissionsUser(
            $id,
            null,
            array_merge(['create', 'read', 'update', 'delete'], $crud),
            $this->request->param('prefix'),
            $filter
        );

        $this->render('admin_acl_edit');
    }

    /**
     * manager_acl_edit
     *
     * @access public
     *
     * @param type $id
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function manager_acl_edit($id) // phpcs:ignore
    {
        $this->admin_acl_edit($id, ['Service.id' => 'update']);
    }
}
