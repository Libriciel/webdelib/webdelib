<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('File', 'Utility');
App::uses('AppTools', 'Lib');

/**
 * Connecteurs Controller
 *
 * @package app.Controller
 * @version 5.1.0
 * @since   4.0.0
 */
class ConnecteursController extends AppController
{
    public $uses = ['Collectivite'];

    public $components = [
        'RequestHandler',
        //'ConnectorManager.ConnectorManager',
        'Auth' => [
            'mapActions' => [
                'read' => [
                    'admin_edit', 'admin_index', 'admin_makeconf',
                    'admin_maintenance', 'listEntities',  'saveDataConnexionPastell',
                    'admin_fusion_download_data', 'admin_fusion_download_file', 'admin_fusion_download_serialize'
                ]
            ]
        ],
        'Pastell',
        'Portainer'
    ];
    private $connecteurs = [
        -1 => 'Modifier le fichier webdelib.inc',
        0 => 'Mode debug',
        1 => 'Génération des documents',
        2 => 'Configuration des mails',
        3 => 'Parapheur électronique (Signature)',
        4 => 'Tiers de Télétransmission (TdT)',
        5 => 'Idelibre',
        6 => 'Mail sécurisé',
        7 => 'Publication',
        8 => 'Gestion électronique de document (GED)',
        9 => 'Système d\'archivage électronique (SAE)',
        10 => 'LDAP',
        11 => 'Pastell',
        12 => 'Authentification (CAS, LDAP)',
    ];

    /**
     * @version 4.3
     * @access public
     */
    public function admin_index() // phpcs:ignore
    {
        unset($this->connecteurs[-1]);
        unset($this->connecteurs[0]);
        $this->set('connecteurs', $this->connecteurs);

        //$this->ConnectorManager->getConnector();
    }

    /**
     * @version 4.2
     * @access public
     * @param type $id
     * @return type
     */
    public function admin_edit($id) // phpcs:ignore
    {
        $this->set('titre', $this->connecteurs[$id]);
        switch ($id) {
            case -1:
                // Mode Config (texte)
                $this->set('content', file_get_contents(
                    DATA . $this->getTenantName() . DS .'Config' . DS . 'webdelib.inc'
                ));
                $this->render('all');
                break;
            case 0:
                // Mode Debug
                $this->render('debug');
                break;
            case 1:
                $convertTools = [
                    'ghostscript' =>'Ghostscript',
                  //  'pdftk-ghostscript' => 'PDFtk-Ghostscript',
                    'pdftk-imagick' => 'PDFtk-Imagick (défaut)',
                   // 'pdftk-pdftocairo' => 'PDFtk-PDFtoCairo',
                   // 'pdftk-pdftoppm' => 'PDFtk-Pdftoppm',
                   // 'pdftocairo' => 'PDFtoCairo',
                    //'pdftoppm' => 'Pdftoppm'
                ];
                $this->set('convertTools', $convertTools);
                $resolutions = [
                    '110' => '110 dpi',
                    '120' => '120 dpi',
                    '130' => '130 dpi',
                    '140' => '140 dpi',
                    '150' => '150 dpi (défaut)',
                    '160' => '160 dpi',
                    '170' => '170 dpi'
                ];
                $this->set('resolutions', $resolutions);

                $sizes = [
                    '5M'=>'5 Mo',
                    '10M' => '10 Mo',
                    '20M' => '20 Mo (défaut)',
                    '30M' => '30 Mo',
                    '40M' => '40 Mo',
                    '50M' => '50 Mo'
                ];

                $this->set('sizes', $sizes);
                $odfgedooo_etat = $cloudooo_etat = 'informations impossibles (maintenance non renseigné)';

                $host = Configure::read('portainer.host');
                $username = Configure::read('portainer.admin');
                $password = Configure::read('portainer.password');
                if (!empty($host) && !empty($username) && !empty($password)) {
                    $docker_services = array_flip(['flow','cloudooo','pdf-stamp','lspdf2odt']);
                    foreach ($docker_services as $service_name => &$docker_service) {
                        $docker_service = [
                            'state' => $this->Portainer->getDockerState($service_name),
                            'status' => $this->Portainer->getDockerStatus($service_name),
                            'log' => $this->Portainer->getLastLog($service_name)
                        ];
                    }
                    $this->set('docker_services', $docker_services);

                    $this->set('maintenance', true);
                }

                $this->set('FusionConv_app_fileOdt_convert', Configure::read('FusionConv.app.fileOdt.convert'));

                if (file_exists(Configure::read('PDFStamp.logo'))) {
                    $imgData = base64_encode(file_get_contents(Configure::read('PDFStamp.logo')));
                    $imgSrc = 'data: ' . mime_content_type(Configure::read('PDFStamp.logo')) . ';base64,' . $imgData;
                } else {
                    $imgData = base64_encode(file_get_contents(PDFSTAMP_LOGO));
                    $imgSrc = 'data: ' . mime_content_type(PDFSTAMP_LOGO) . ';base64,' . $imgData;
                }

                $this->set('imgSrc', $imgSrc);

                // Connecteur ODFGEDOOo et CLOUDOOo
                $this->render('conversion');
                break;
            case 2:
                // Connecteur mails
                $this->render('mail');
                break;
            case 3:
                // Configuration signature
                $protocoles = ['PASTELL' => 'Pastell', 'IPARAPHEUR' => 'Iparapheur'];
                if (Configure::read('PARAPHEUR') === 'IPARAPHEUR') {
                    if (file_exists(Configure::read('IPARAPHEUR_CLIENTCERT'))) {
                        $clientCert = openssl_x509_parse(
                            file_get_contents(Configure::read('IPARAPHEUR_CLIENTCERT'))
                        );
                        $this->set('clientCert', $clientCert);
                        //$this->set('client_validTo', date('Y-m-d H:i:s', $server_data['validTo_time_t']));
                    }
                }
                $this->set('protocoles', $protocoles);
                $this->set('pastell_parapheur_types', ['IPARAPHEUR' => 'iparapheur', 'FAST' => 'Fast parapheur']);
                $this->render('signature');
                break;
            case 4:
                // Configuration tdt
                $protocoles = ['PASTELL' => 'Pastell', 'S2LOW' => 'S²LOW'];
                if (!Configure::read('USE_PASTELL')) {
                    unset($protocoles['PASTELL']);
                }
                $this->set('protocoles', $protocoles);
                if (file_exists(Configure::read('S2LOW_PEM')) && Configure::read('TDT') == 'S2LOW') {
                    $clientCert = openssl_x509_parse(file_get_contents(Configure::read('S2LOW_PEM')));
                    $this->set('clientCert', $clientCert);
                }
                $this->render('tdt');
                break;
            case 5:
                // Connecteur idelibre
                $this->render('idelibre');
                break;
            case 6:
                // Configuration tdt
                $protocoles = ['PASTELL' => 'Pastell', 'S2LOW' => 'S²LOW'];
                if (!Configure::read('USE_S2LOW') && !Configure::read('MAILSEC_S2LOW_CACHE')) {
                    unset($protocoles['S2LOW']);
                }
                $this->set('protocoles', $protocoles);
                $this->render('mailsec');
                break;
            case 7:
                // Configuration tdt
                $protocoles = ['PASTELL' => 'Pastell'];
                $this->set('protocoles', $protocoles);
                $this->render('publication');
                break;
            case 8:
                // Connecteur CMIS
                $protocoles = ['PASTELL' => 'Pastell', 'CMIS' => 'CMIS'];
                if (!Configure::read('USE_PASTELL')) {
                    unset($protocoles['PASTELL']);
                }
                $this->set('protocoles', $protocoles);

                $this->render('ged');
                break;
            case 9:
                // Connecteur SAE
                $protocoles = ['PASTELL' => 'Pastell'/* , 'ASALAE' => 'as@lae' */];
                if (!Configure::read('USE_PASTELL')) {
                    unset($protocoles['PASTELL']);
                }
                $this->set('protocoles', $protocoles);
                $this->render('sae');
                break;
            case 10:
                // Connecteur LDAP
                $protocoles = ['ActiveDirectory' => 'Active Directory', 'OpenLDAP' => 'OpenLDAP'];
                $this->set('protocoles', $protocoles);
                $this->render('ldap');
                break;
            case 11:
                $collectivite = $this->Collectivite->find('first', [
                    'conditions'=> [
                        'Collectivite.id' => 1
                    ],
                    'recursive' => -1
                ]);

                $entities = [];
                $selected = null;
                if (!empty($collectivite['Collectivite']['id_entity'])) {
                    $entitie_id = $collectivite['Collectivite']['id_entity'];
                    $entities =  ["$entitie_id" => $collectivite['Collectivite']['nom']];
                    $selected = $collectivite['Collectivite']['id_entity'];
                }
                $this->set('entities', $entities);
                $this->set('selected', $selected);

                $this->set('flux_pastell', Configure::read('Pastell'));
                $this->render('pastell');
                break;
            case 12:
                // Connecteur Authentification
                $protocoles = ['cas' => 'CAS', 'ldap' => 'LDAP'];
                $this->set('protocoles', $protocoles);
                if (file_exists(Configure::read('AuthManager.Cas.cert_path'))
                    && Configure::read('AuthManager.Authentification.type') == 'CAS'
                ) {
                    $clientCert = openssl_x509_parse(
                        file_get_contents(Configure::read('AuthManager.Cas.cert_path'))
                    );
                    $this->set('clientCert', $clientCert);
                }
                $this->render('authentification');
                break;
            default:
                $this->Flash->set(
                    __('Ce connecteur n\'est pas valide'),
                    ['element' => 'growl', 'params' => ['type' => 'danger']]
                );

                return $this->redirect($this->previous);
        }
    }

    /**
     * @version 4.2
     * @access private
     * @param type $content
     * @param type $param
     * @param type $new_value
     * @return type
     */
    private function replaceValue($content, $param, $new_value)
    {
        if (is_bool(Configure::read($param))) {
            $valeur = Configure::read($param) === true ? 'true' : 'false';
        } else {
            $valeur = Configure::read($param);
        }

        $count = 0;
        if (is_array($new_value)) {
            $pattern = '/Configure::write\(\'' . str_replace('.', '\.', $param) . '\'\,[^);]*\);/i';
            $replacement = "Configure::write('$param', " . AppTools::varExportConfig($new_value, true) . ");";

            $content = preg_replace($pattern, $replacement, $content, -1, $count);
        }


        if ($count === 0) {
            $host_b = "Configure::write('$param', " . var_export((string)$valeur, true) . ");";
            $host_a = "Configure::write('$param', " . var_export((string)$new_value, true) . ");";

            $content = str_replace($host_b, $host_a, $content, $count);
        }

        if ($count === 0) {
            $host_b = "Configure::write('$param', " . $valeur . ");";
            $host_a = "Configure::write('$param', $new_value);";

            $content = str_replace($host_b, $host_a, $content, $count);
        }

        if ($count>1) {
            throw new Exception(__('Erreur doublon dans la configuration.'));
        }

        return $content;
    }

    /**
     * @version 4.3
     * @access public
     * @param type $type
     * @return type
     */
    public function admin_makeconf($type) // phpcs:ignore
    {
        try {
            $file = new File(DATA . $this->getTenantName() . DS .'config' . DS . 'webdelib.inc', true);
            $content = $file->read();
            switch ($type) {
                case 'signature':
                    $protocol = strtoupper($this->data['Connecteur']['signature_protocol']);
                    $content = $this->replaceValue($content, 'PARAPHEUR', $protocol);
                    $content = $this->replaceValue(
                        $content,
                        'PASTELL_PARAPHEUR_TYPE',
                        $this->data['Connecteur']['pastell_parapheur_type']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'PASTELL_VISA_TYPE',
                        $this->data['Connecteur']['pastell_visa_type']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'USE_PARAPHEUR',
                        $this->data['Connecteur']['use_signature']
                    );
                    $content = $this->replaceValue($content, 'IPARAPHEUR_URI', $this->data['Connecteur']['uri']);
                    $content = $this->replaceValue($content, 'IPARAPHEUR_HOST', $this->data['Connecteur']['host']);
                    $content = $this->replaceValue($content, 'IPARAPHEUR_LOGIN', $this->data['Connecteur']['login']);
                    $content = $this->replaceValue($content, 'IPARAPHEUR_PWD', $this->data['Connecteur']['pwd']);
                    $content = $this->replaceValue($content, 'IPARAPHEUR_TYPE', $this->data['Connecteur']['type']);
                    $content = $this->replaceValue(
                        $content,
                        'PASTELL_PARAPHEUR_MODE',
                        $this->data['Connecteur']['pastell_parapheur_mode']
                    );

                    if ($protocol == 'IPARAPHEUR') {
                        $content = $this->replaceValue($content, 'USE_IPARAPHEUR', 'true');
                    } else {
                        $content = $this->replaceValue($content, 'USE_IPARAPHEUR', 'false');
                    }
                    break;

                case 'tdt':
                    $protocol = strtoupper($this->data['Connecteur']['tdt_protocol']);
                    $content = $this->replaceValue($content, 'TDT', $protocol);
                    $content = $this->replaceValue(
                        $content,
                        'USE_TDT',
                        $this->data['Connecteur']['use_tdt']
                    );
                    if ($protocol == 'S2LOW') {
                        $content = $this->replaceValue($content, 'USE_S2LOW', 'true');
                        if (strpos($this->request->data['Connecteur']['host'], 'https://') === false) {
                            $this->request->data['Connecteur']['host'] =
                                'https://' . $this->request->data['Connecteur']['host'];
                        }
                        $content = $this->replaceValue(
                            $content,
                            'S2LOW_HOST',
                            $this->data['Connecteur']['host']
                        );
                        $content = $this->replaceValue(
                            $content,
                            'S2LOW_CERTPWD',
                            $this->data['Connecteur']['certpwd']
                        );
                        $content = $this->replaceValue(
                            $content,
                            'S2LOW_USEPROXY',
                            $this->data['Connecteur']['use_proxy']
                        );
                        $content = $this->replaceValue(
                            $content,
                            'S2LOW_PROXYHOST',
                            $this->data['Connecteur']['proxy_host']
                        );
                        if (file_exists($this->data['Connecteur']['clientcert']['tmp_name'])) {
                            $certs = [];
                            $path_dir_s2low = DATA . $this->getTenantName() . DS .'config' . DS . 'cert_s2low' . DS;
                            $pkcs12 = file_get_contents($this->data['Connecteur']['clientcert']['tmp_name']);
                            if (openssl_pkcs12_read(
                                $pkcs12,
                                $certs,
                                $this->data['Connecteur']['certpwd']
                            )
                            ) {
                                file_put_contents($path_dir_s2low . 'key.pem', $certs['pkey']);
                                file_put_contents($path_dir_s2low . 'client.pem', $certs['cert']);
                                file_put_contents($path_dir_s2low . 'ca.pem', $certs['extracerts'][0]);
                            } else {
                                $this->Flash->set(
                                    __('Le mot de passe du certificat est erroné'),
                                    ['element' => 'growl', 'params' => ['type' => 'danger']]
                                );
                                return $this->redirect($this->previous);
                            }
                        }
                        if ($this->data['Connecteur']['use_login']) {
                            $content = $this->replaceValue(
                                $content,
                                'S2LOW_USELOGIN',
                                $this->data['Connecteur']['use_login']
                            );
                            $content = $this->replaceValue(
                                $content,
                                'S2LOW_LOGIN',
                                $this->data['Connecteur']['s2low_login']
                            );
                            $content = $this->replaceValue(
                                $content,
                                'S2LOW_PWD',
                                $this->data['Connecteur']['s2low_pwd']
                            );
                        }
                    } elseif ($protocol == 'PASTELL') {
                        $content = $this->replaceValue($content, "USE_S2LOW", 'false');
                        $content = $this->replaceValue(
                            $content,
                            "S2LOW_BORDEREAU_EXIST",
                            $this->data['Connecteur']['s2low_bordereau_exist']
                        );
                        $content = $this->replaceValue(
                            $content,
                            "S2LOW_ACTE_TAMPON_EXIST",
                            $this->data['Connecteur']['s2low_acte_tampon_exist']
                        );
                    }
                    break;

                case 'idelibre':
                    $content = $this->replaceValue(
                        $content,
                        'USE_IDELIBRE',
                        $this->data['Connecteur']['use_idelibre']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'IDELIBRE_CONN',
                        $this->data['Connecteur']['idelibre_conn']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'IDELIBRE_LOGIN',
                        $this->data['Connecteur']['idelibre_login']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'IDELIBRE_PWD',
                        $this->data['Connecteur']['idelibre_pwd']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'IDELIBRE_USEPROXY',
                        $this->data['Connecteur']['use_proxy']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'IDELIBRE_PROXYHOST',
                        $this->data['Connecteur']['proxy_host']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'IDELIBRE_APPENDIX_FUSION',
                        $this->data['Connecteur']['remove_appendix_for_fusion']
                    );
//                    $content = $this->replaceValue(
//                        $content,
//                        'IDELIBRE_X_AUTH_TOKEN',
//                        $this->data['Connecteur']['x_auth_token']
//                    );
                    $content = $this->replaceValue(
                        $content,
                        'IDELIBRE_HOST',
                        $this->data['Connecteur']['idelibre_host']
                    );
                    break;

                case 'mailsec':
                    $protocol = strtoupper($this->data['Connecteur']['mailsec_protocol']);
                    $content = $this->replaceValue($content, 'MAILSEC', $protocol);
                    $content = $this->replaceValue($content, 'USE_MAILSEC', $this->data['Connecteur']['use_mailsec']);
                    $content = $this->replaceValue($content, 'USE_S2LOW_MAILSEC', 'false');
                    $content = $this->replaceValue($content, 'USE_PASTELL_MAILSEC', 'false');
                    if ($protocol == 'S2LOW') {
                        $content = $this->replaceValue($content, 'USE_S2LOW_MAILSEC', 'true');
                        $content = $this->replaceValue(
                            $content,
                            'S2LOW_MAILSEC_PWD_USE',
                            $this->data['Connecteur']['s2low_mailsec_pwd_use']
                        );
                        $content = $this->replaceValue(
                            $content,
                            'S2LOW_MAILSEC_PWD',
                            $this->data['Connecteur']['s2low_mailsec_pwd']
                        );
                    } elseif ($protocol == 'PASTELL') {
                        $content = $this->replaceValue(
                            $content,
                            'USE_PASTELL_MAILSEC',
                            'true'
                        );
                        $content = $this->replaceValue(
                            $content,
                            'PASTELL_MAILSEC_HOST',
                            $this->data['Connecteur']['mailsec_host']
                        );
                        $content = $this->replaceValue(
                            $content,
                            'PASTELL_MAILSEC_IDE',
                            $this->data['Connecteur']['mailsec_id_e']
                        );
                        $content = $this->replaceValue(
                            $content,
                            'PASTELL_MAILSEC_FLUX',
                            $this->data['Connecteur']['mailsec_type']
                        );
                        $content = $this->replaceValue(
                            $content,
                            'PASTELL_MAILSEC_LOGIN',
                            $this->data['Connecteur']['mailsec_login']
                        );
                        $content = $this->replaceValue(
                            $content,
                            'PASTELL_MAILSEC_PWD',
                            $this->data['Connecteur']['mailsec_pwd']
                        );
                    }
                    break;

                case 'publication':
                    $protocol = strtoupper($this->data['Connecteur']['publication_protocol']);
                    $content = $this->replaceValue($content, 'PUBLICATION', $protocol);
                    $content = $this->replaceValue(
                        $content,
                        'USE_PUBLICATION',
                        $this->data['Connecteur']['use_publication']
                    );
                    if ($protocol == 'PASTELL') {
                        $content = $this->replaceValue(
                            $content,
                            'PUBLICATION_HOST',
                            $this->data['Connecteur']['publication_host']
                        );
                        $content = $this->replaceValue(
                            $content,
                            'PUBLICATION_IDE',
                            $this->data['Connecteur']['publication_id_e']
                        );
                        $content = $this->replaceValue(
                            $content,
                            'PUBLICATION_FLUX',
                            $this->data['Connecteur']['publication_type']
                        );
                        $content = $this->replaceValue(
                            $content,
                            'PUBLICATION_LOGIN',
                            $this->data['Connecteur']['publication_login']
                        );
                        $content = $this->replaceValue(
                            $content,
                            'PUBLICATION_PWD',
                            $this->data['Connecteur']['publication_pwd']
                        );
                        $content = $this->replaceValue(
                            $content,
                            'PUBLICATION_PERMALIEN',
                            $this->data['Connecteur']['publication_permalien']
                        );
                        $content = $this->replaceValue(
                            $content,
                            'PUBLICATION_PREF_ID',
                            $this->data['Connecteur']['publication_pref_id']
                        );
                    }
                    break;

                case 'conversion':
                    $content = $this->replaceValue(
                        $content,
                        'App.convert.tool',
                        $this->data['Connecteur']['app_convert_tool']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'App.convert.resolution',
                        $this->data['Connecteur']['app_convert_resolution']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'App.convert.url',
                        $this->data['Connecteur']['app_convert_url']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'FusionConv.app.fileOdt.convert',
                        $this->data['Connecteur']['FusionConv_app_fileOdt_convert']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'App.upload_max_filesize',
                        $this->data['Connecteur']['app_upload_max_filesize']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'portainer.host',
                        $this->data['Connecteur']['Portainer_host']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'portainer.admin',
                        $this->data['Connecteur']['Portainer_admin']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'portainer.password',
                        $this->data['Connecteur']['Portainer_password']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'FusionConv.Gedooo.wsdl',
                        $this->data['Connecteur']['gedooo_url']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'FusionConv.cloudooo_host',
                        $this->data['Connecteur']['cloudooo_url']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'PDFStamp.host',
                        $this->data['Connecteur']['PDFStamp_host']
                    );
                    if (file_exists($this->data['Connecteur']['PDFStamp_logo']['tmp_name'])) {
                        file_put_contents(
                            DATA . Configure::read('Config.tenantName') . DS . 'config' . DS . 'PDFStamp_logo.png',
                            file_get_contents($this->data['Connecteur']['PDFStamp_logo']['tmp_name'])
                        );
                    }
                    break;

                case 'ged':
                    if (!empty($this->data['Connecteur']['ged_protocol'])) {
                        $protocol = strtoupper($this->data['Connecteur']['ged_protocol']);
                        $content = $this->replaceValue($content, 'USE_GED', $this->data['Connecteur']['use_ged']);
                    } else {
                        $protocol = '';
                    }
                    $content = $this->replaceValue($content, 'GED', $protocol);
                    $content = $this->replaceValue($content, 'CMIS_HOST', $this->data['Connecteur']['ged_url']);
                    $content = $this->replaceValue($content, 'CMIS_LOGIN', $this->data['Connecteur']['ged_login']);
                    $content = $this->replaceValue($content, 'CMIS_PWD', $this->data['Connecteur']['ged_passwd']);
                    $content = $this->replaceValue($content, 'CMIS_REPO', $this->data['Connecteur']['ged_repo']);
                    $content = $this->replaceValue(
                        $content,
                        'GED_XML_VERSION',
                        $this->data['Connecteur']['ged_xml_version']
                    );
                    break;

                case 'mail':
                    $content = $this->replaceValue($content, 'MAIL_FROM', $this->data['Connecteur']['mail_from']);
                    break;

                case 'sae':
                    if (!empty($this->data['Connecteur']['sae_protocol'])) {
                        $protocol = strtoupper($this->data['Connecteur']['sae_protocol']);
                        $content = $this->replaceValue($content, 'USE_SAE', $this->data['Connecteur']['use_sae']);
                    } else {
                        $protocol = '';
                    }
                    $content = $this->replaceValue($content, 'SAE', $protocol);
                    $content = $this->replaceValue(
                        $content,
                        "SAE_PASTELL_DOSSIER_TYPE_SEANCE",
                        $this->data['Connecteur']['sae_pastell_dossier_type_seance']
                    );
                    break;

                case 'ldap':
                    $content = $this->replaceValue(
                        $content,
                        'LdapManager.Ldap.type',
                        $this->data['Connecteur']['type']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'LdapManager.Ldap.use',
                        $this->data['Connecteur']['use']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'LdapManager.Ldap.host',
                        $this->data['Connecteur']['host']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'LdapManager.Ldap.host_fall_over',
                        $this->data['Connecteur']['host_fall_over']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'LdapManager.Ldap.port',
                        $this->data['Connecteur']['port']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'LdapManager.Ldap.basedn',
                        $this->data['Connecteur']['basedn']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'LdapManager.Ldap.login',
                        $this->data['Connecteur']['login']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'LdapManager.Ldap.password',
                        $this->data['Connecteur']['password']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'LdapManager.Ldap.tls',
                        $this->data['Connecteur']['tls']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'LdapManager.Ldap.version',
                        $this->data['Connecteur']['version']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'LdapManager.Ldap.account_suffix',
                        $this->data['Connecteur']['suffix']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'LdapManager.Ldap.filter',
                        $this->data['Connecteur']['filter']
                    );
                    $fields = Configure::read('LdapManager.Ldap.fields');
                    $fields_out = [];
                    foreach ($fields['User'] as $key => $value) {
                        $fields_out['User'][$key] = $this->data['Connecteur']['fields_user_' . $key];
                    }
                    $content = $this->replaceValue($content, 'LdapManager.Ldap.fields', $fields_out);
                    break;

                case 'pastell':
                    if (Configure::read('USE_PASTELL')
                        && !empty($this->data['Collectivite']['id_entity'])
                    ) {
                        try {
                            $this->Collectivite->id = 1;
                            $entities = $this->Pastell->listEntities();
                            $collectivite = [
                              'id_entity' => $this->data['Collectivite']['id_entity'],
                              'nom' => $entities[$this->data['Collectivite']['id_entity']]
                            ];
                            $this->Collectivite->save($collectivite);
                        } catch (Exception $e) {
                            CakeLog::error($e->getMessage(), 'connector');
                        }
                    } else {
                        $this->Collectivite->id = 1;
                        $collectivite = [
                            'id_entity' => null,
                        ];
                        $this->Collectivite->save($collectivite);
                    }
                    $content = $this->replaceValue(
                        $content,
                        'USE_PASTELL',
                        $this->data['Connecteur']['use_pastell']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'PASTELL_HOST',
                        $this->data['Connecteur']['host']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'PASTELL_LOGIN',
                        $this->data['Connecteur']['login']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'PASTELL_PWD',
                        $this->data['Connecteur']['pwd']
                    );

                    $content = $this->replaceValue(
                        $content,
                        'PASTELL_TYPE',
                        $this->data['Connecteur']['type']
                    );

                    break;

                case 'authentification':
                    $content = $this->replaceValue(
                        $content,
                        'AuthManager.Authentification.use',
                        $this->data['Connecteur']['authentification_use']
                    );
                    $content = $this->replaceValue(
                        $content,
                        'AuthManager.Authentification.type',
                        strtoupper($this->data['Connecteur']['authentification_protocol'])
                    );

                    if (strtoupper($this->data['Connecteur']['authentification_protocol']) === 'CAS') {
                        $content = $this->replaceValue(
                            $content,
                            'AuthManager.Cas.host',
                            $this->data['Connecteur']['cas_host']
                        );
                        $content = $this->replaceValue(
                            $content,
                            'AuthManager.Cas.port',
                            $this->data['Connecteur']['cas_port']
                        );
                        $content = $this->replaceValue(
                            $content,
                            'AuthManager.Cas.uri',
                            $this->data['Connecteur']['cas_uri']
                        );
                        if (file_exists($this->data['Connecteur']['cas_clientcert']['tmp_name'])) {
                            file_put_contents(
                                DATA . $this->getTenantName() . DS .'config' . DS . 'cert_cas' . DS . 'client.pem',
                                file_get_contents($this->data['Connecteur']['cas_clientcert']['tmp_name'])
                            );
                        }
                    }
                    break;

                case 'debug':
                    $content = $this->replaceValue($content, 'debug', $this->data['Connecteur']['debug']);
                    break;

                case 'all':
                    $content = $this->data['Connecteur']['all'];
                    break;

                default:
                    $this->Flash->set(
                        __('Ce connecteur n\'est pas valide'),
                        ['element' => 'growl', 'params' => ['type' => 'danger']]
                    );
                    return $this->redirect($this->previous);
            }

            if (!$file->writable()) {
                $this->Flash->set(
                    __('Impossible de modifier le fichier de configuration,'
                        .' veuillez donner les droits sur fichier webdelib.inc'),
                    ['element' => 'growl', 'params' => ['type' => 'danger']]
                );
            } else {
                //TODO : php_check_syntax
                $success = $file->open('w+');
                $success &= $file->append($content);
                $success &= $file->close();
                if ($success) {
                    $this->Flash->set(
                        __('La configuration du module "%s" a été enregistrée', $type),
                        ['element' => 'growl']
                    );
                } else {
                    $this->Flash->set(
                        __('Un problème est survenu lors de la modification du fichier de configuration webdelib.inc'),
                        ['element' => 'growl', 'params' => ['type' => 'danger']]
                    );
                }
            }
        } catch (Exception $e) {
            $this->Flash->set($e->getMessage(), ['element' => 'growl', 'params' => ['type' => 'danger']]);
        }

        return $this->redirect(
            ['admin' => true, 'prefix' => 'admin', 'controller' => 'Connecteurs', 'action' => 'index']
        );
    }

    public function admin_fusion_download_data() // phpcs:ignore
    {
        App::uses('Folder', 'Utility');
        $folder = new Folder(TMP . DS . 'files' . DS . $this->getTenantName() . DS . 'FusionConv');
        $file = new File($folder->pwd() . DS . 'Donnees.txt');

        if (!$file->exists()) {
            $this->Flash->set(
                __('Pas de fichier disponible'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            return $this->redirect(
                ['admin' => true, 'prefix' => 'admin', 'controller' => 'Connecteurs', 'action' => 'edit', 1]
            );
        }

        $this->response->disableCache();
        $this->response->body($file->read());
        $this->response->type('application/text');
        $this->response->download('Donnees.txt');

        return $this->response;
    }

    public function admin_fusion_download_serialize() // phpcs:ignore
    {
        App::uses('Folder', 'Utility');
        $folder = new Folder(TMP . DS . 'files' . DS . $this->getTenantName() . DS . 'FusionConv');
        $file = new File($folder->pwd() . DS . 'Serialize.txt');

        if (!$file->exists()) {
            $this->Flash->set(
                __('Pas de fichier disponible'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            return $this->redirect(
                ['admin' => true, 'prefix' => 'admin', 'controller' => 'Connecteurs', 'action' => 'edit', 1]
            );
        }

        $this->response->disableCache();
        $this->response->body($file->read());
        $this->response->type('application/vnd.webdelib.raw-serialize');
        $this->response->download('Serialize.txt');

        return $this->response;
    }


    public function admin_fusion_download_file() // phpcs:ignore
    {
        App::uses('Folder', 'Utility');
        $folder = new Folder(TMP . DS . 'files' . DS . $this->getTenantName() . DS . 'FusionConv');
        $file = new File($folder->pwd() . DS . 'Resultat.odt');

        if (!$file->exists()) {
            $this->Flash->set(
                __('Pas de fichier disponible'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            return $this->redirect(
                ['admin' => true, 'prefix' => 'admin', 'controller' => 'Connecteurs', 'action' => 'edit', 1]
            );
        }

        $this->response->disableCache();
        $this->response->body($file->read());
        $this->response->type('application/vnd.oasis.opendocument.text');
        $this->response->download('Resultat.odt');

        return $this->response;
    }


    public function admin_maintenance($connecteur, $action) // phpcs:ignore
    {
        try {
            $host = Configure::read('portainer.host');
            $username = Configure::read('portainer.admin');
            $password = Configure::read('portainer.password');
            if (empty($host) || empty($username) || empty($password)) {
                throw new \RuntimeException('Échec de la connexion');
            }
            $action_libelle = [
                'start' => 'démarré',
                'stop'  => 'arrêté',
                'restart' => 'redémarré'
            ];

            $action_libelle_is_ok ='Le service a été arrêté';
            $action_libelle_is_ko = 'Le service n\'a pas été arrêté';

            if (!empty($this->Portainer->getDockerCommand($connecteur, $action))) {
                throw new Exception(__('Le service n\'a pas été %s'), $action_libelle[$action]);
            }
            $this->Flash->set(
                __('Le service a été %s', $action_libelle[$action]),
                ['element' => 'growl', 'params' => ['type' => 'success']]
            );
            $this->redirect(['controllers' => 'connecteurs', 'action' => 'edit', 1]);
        } catch (Exception $e) {
            $this->Flash->set($e->getMessage(), ['element' => 'growl', 'params' => ['type' => 'danger']]);
        }
    }

    /**
     * Retourne la liste des entités Pastell
     * @return [type] [description]
     * @since 5.1.0
     */
    public function listEntities()
    {
        $this->autoRender = false;
        try {
            $entities = $this->Pastell->listEntities();

            $this->response->type(['json' => 'text/x-json']);
            $this->RequestHandler->respondAs('json');
            $this->response->body(json_encode($entities));
        } catch (Exception $e) {
            $this->response->statusCode(404);
        }

        return $this->response;
    }

    /**
     * Sauvegarde des informations de connexion de pastell
     */
    public function saveDataConnexionPastell()
    {
        $this->autoRender = false;
        try {
            if (empty($this->data['host'])
                || empty($this->data['login'])
                || empty($this->data['pwd'])
            ) {
                $this->response->type(['json' => 'text/x-json']);
                $this->RequestHandler->respondAs('json');
                $this->response->body(json_encode(
                    [
                        'error' => 'Les données ne sont pas toutes renseignées'
                    ],
                    JSON_THROW_ON_ERROR
                ));
                return $this->response;
            }
            $success = false;
            $file = new File(DATA . $this->getTenantName() . DS .'config' . DS . 'webdelib.inc', true);
            $content = $file->read();
            $content = $this->replaceValue(
                $content,
                'PASTELL_HOST',
                $this->data['host']
            );
            $content = $this->replaceValue(
                $content,
                'PASTELL_LOGIN',
                $this->data['login']
            );
            $content = $this->replaceValue(
                $content,
                'PASTELL_PWD',
                $this->data['pwd']
            );

            if (!$file->writable()) {
                throw new CakeException('Impossible d\écrire dans le fichier de configuration');
            } else {
                $success = $file->open('w+');
                $success &= $file->append($content);
                $success &= $file->close();
                if ($success) {
                    $this->response->statusCode(200);
                } else {
                    throw new CakeException('Impossible d\enregistrer dans le fichier de configuration');
                }
            }
            $this->response->type(['json' => 'text/x-json']);
            $this->RequestHandler->respondAs('json');
            $this->response->body(json_encode($success));
        } catch (Exception $e) {
            $this->response->statusCode($e->getCode());
        }

        return $this->response;
    }
}
