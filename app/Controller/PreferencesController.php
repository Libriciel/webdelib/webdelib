<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Class WebdavController
 *
 * @version 4.3
 * @package app.Controller
 */
class PreferencesController extends AppController
{
    /**
     * $uses
     * @var [array]
     */
    public $uses = [
        'User',
        'ServiceUser',
        'Collectivite',
        'Service',
        'Cakeflow.Circuit',
        'Profil',
        'Typeacte',
        'Aro',
        'Aco'
    ];


    /**
     * @version 4.3
     * @access public
     * @var type
     */
    public $components = [
        'Auth' => [
            'mapActions' => [
                'read' => [
                    'index',
                ],
                'changeFormatSortie',
                'changeUserMdp',
                'allow' => ['changeServiceEmetteur'],
            ],
          ]
    ];

    /**
     * [changeUserSetting Changement des préférences utilisateur]
     * @return [type] [description]
     * @version 5.2
     */
    public function index()
    {
        //var_dump($this->previous);exit;
        if ($this->request->is('Post')) {
            $this->User->id = $this->Auth->user('id');
            if ($this->User->save($this->request->data, false)) {
                $this->Session->write('Auth.User.theme', $this->data['User']['theme']);
                $this->Flash->set(
                    __('Modifications enregistrées avec succès'),
                    ['element' => 'growl']
                );
                $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Erreur lors de l\'enregistrement des modifications.'),
                    ['element' => 'growl', 'params' => ['type' => 'danger']]
                );
            }
        }

        $this->request->data = $this->User->find(
            'first',
            [
                'contain' => ['Service', 'Profil'],
                'conditions' => [
                    'User.id' => $this->Auth->user('id')
                ],
                'recursive' => -1
            ]
        );

        //Fix default
        if (empty($this->request->data['User']['theme'])) {
            $this->request->data['User']['theme'] = 'Normal';
        }

        $this->set('circuitDefautLibelle', $this->User->circuitDefaut($this->Auth->user('id'), 'nom'));
        $this->set('themes', [
            'Normal' => 'Normal',
            'lumen' => 'Lumen',
            'paper' => 'Paper',
            'sandstone' => 'Sandstone',
            'simplex' => 'Simplex',
            'spacelab' => 'Spacelab',
            'readable' => 'Readable',
        ]);
    }

    /**
     * changeFormatSortie
     * @param string $format
     */
    public function changeFormatSortie(string $format)
    {
        if ($this->request->is('Get')) {
            $this->Session->write('Auth.User.formatSortie', $format);

            $this->Flash->set(
                __(
                    'Génération au format ODT "%s"',
                    $format ? __('Activé') : __('Désactivé')
                ),
                ['element' => 'growl', 'params' => ['type' => 'warning']]
            );

            $this->redirect($this->previous);
        }
    }

    /**
     * @access public
     */
    public function changeUserMdp()
    {
        if ($this->request->is('Post')) {
            $this->User->id = $this->Auth->user('id');
            if ($this->User->savePassword($this->data)) {
                $this->Flash->set(__('Le mot de passe a été modifié avec succès'), ['element' => 'growl']);
                return $this->redirect($this->previous);
            } else {
                $this->request->data = $this->data;

                $this->Flash->set(
                    __('Erreur lors de l\'enregistrement du nouveau mot de passe.'),
                    ['element' => 'growl', 'params' => ['type' => 'danger']]
                );
            }
        }

        //Fix default
        $this->request->data['User']['password'] = '';

        $minEntropie = $this->entropyPassword();

        $this->set(compact(['minEntropie']));
    }

    /**
     * @access public
     */
    public function changeServiceEmetteur()
    {
        $services = $this->Auth->user('Service');

        if ($this->request->is('Post')) {
            $this->User->id = $this->Auth->user('id');

            $data['User']['service_defaut_id'] = $this->request->data['User']['ServiceEmetteur']['id'];

            $this->Session->write(
                'Auth.User.ServiceEmetteur.id',
                $this->request->data['User']['ServiceEmetteur']['id']
            );
            $this->Session->write(
                'Auth.User.ServiceEmetteur.name',
                $services[$this->request->data['User']['ServiceEmetteur']['id']]
            );

            $this->User->save($data, false);
            //redirection sur la page où on était avant de changer de service
            $this->redirect($this->previous);
        }
        $this->set('services', $services);

        $this->request->data['User']['ServiceEmetteur']['id'] = $this->Auth->User('ServiceEmetteur.id');
    }

    private function entropyPassword()
    {
        return $this->User->getMinUserPasswordEntropy();
    }
}
