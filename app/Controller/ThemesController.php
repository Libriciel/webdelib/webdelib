<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class ThemesController extends AppController
{
    public $name = 'Themes';
    public $helpers = ['Tree'];
    public $components = [
        'Auth' => [
            'mapActions' => [
                'create' => ['admin_add'],
                'read' => ['admin_index','getLibelle', 'isEditable'],
                'update' => [ 'admin_edit'],
                'delete' => ['admin_delete']
            ]
        ]
    ];

    /**
     * @access public
     * @param type $id
     * @return type
     */
    public function getLibelle($id = null)
    {
        $objCourant = $this->Theme->find('first', [
            'conditions' => ['Theme.id' => $id],
            'recursive' => -1,
            'fields' => ['libelle']]);
        return $objCourant['Theme']['libelle'];
    }

    /**
     * @version 4.3
     * @access public
     */
    public function admin_index() // phpcs:ignore
    {
        $this->_index();
    }

    /**
     * @version 4.3
     * @access private
     */
    private function _index() // phpcs:ignore
    {
        $themes = $this->Theme->find('threaded', [
            'conditions' => ['actif' => 1],
            'order' => 'Theme.order ASC',
            'recursive' => -1]);

        $this->set('data', $themes);

        $this->render('index');
    }

    /**
     * @access public
     */
    public function admin_add() // phpcs:ignore
    {
        $this->_add();
    }

    /**
     * @version 4.3
     * @access private
     * @return type
     */
    private function _add() // phpcs:ignore
    {
        $themes = $this->Theme->generateTreeListByOrder(['Theme.actif' => '1'], '&nbsp;&nbsp;&nbsp;&nbsp;');
        $this->set('themes', $themes);
        if (!empty($this->data)) {
            if (empty($this->data['Theme']['parent_id'])) {
                $this->request->data['Theme']['parent_id'] = 0;
            }
            $this->request->data['Theme']['actif'] = 1;
            if ($this->Theme->save($this->data)) {
                $this->Flash->set(__('Le thème a été sauvegardé'), ['element' => 'growl']);
                return $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Veuillez corriger les erreurs ci-dessous.'),
                    ['element' => 'growl','params'=>['type' => 'danger']]
                );
            }
        }
        $this->render('add');
    }

    /**
     * @access public
     * @param type $id
     */
    public function admin_edit($id) // phpcs:ignore
    {
        $this->_edit($id);
    }

    /**
     * @version 4.3
     * @access private
     * @param type $id
     * @return type
     */
    private function _edit($id) // phpcs:ignore
    {
        if (empty($id)) {
            $this->Flash->set(__('ID invalide pour le thème'), ['element' => 'growl','params'=>['type' => 'danger']]);
            $this->redirect($this->previous);
        }
        if (!empty($this->request->data)) {
            if (empty($this->request->data['Theme']['parent_id'])) {
                $this->request->data['Theme']['parent_id'] = 0;
            }
            if ($this->Theme->save($this->request->data)) {
                $this->Flash->set(__('Le thème a été modifié'), ['element' => 'growl']);
                return $this->redirect($this->previous);
            } else {
                $error_msg = 'Veuillez corriger les erreurs ci-dessous.';
                if (empty($this->Theme->validationErrors)) {
                    $error_msg = 'Impossible de déplacer ce thème';
                }
                $this->Flash->set($error_msg, ['element' => 'growl','params'=>['type' => 'danger']]);
            }
        }
        $this->data = $this->Theme->find('first', [
            'conditions'=> [
                'Theme.id' => $id
            ],
        ]);
        $themes = $this->Theme->generateTreeListByOrder(
            ['Theme.id <>' => $id, 'Theme.actif' => '1'],
            '&nbsp;&nbsp;&nbsp;&nbsp;'
        );
        $this->set('isEditable', $this->isEditable($id));
        $this->set('themes', $themes);
        $this->set('selectedTheme', $this->data['Theme']['parent_id']);
        $this->render('edit');
    }

    /**
     * @access public
     * @param type $id
     */
    public function admin_delete($id) // phpcs:ignore
    {
        $this->_delete($id);
    }

    /**
     * @access private
     * @param type $id
     */
    private function _delete($id) // phpcs:ignore
    {
        if (!$id) {
            $this->Flash->set(__('ID invalide pour le thème'), ['element' => 'growl','params'=> ['type' => 'danger']]);
            $this->redirect($this->previous);
        }
        $theme = $this->Theme->find('first', [
            'conditions'=> [
                'Theme.id' => $id
            ],
        ]);
        $theme['Theme']['actif'] = 0;
        if ($this->Theme->save($theme)) {
            $this->Flash->set(__('Le thème a été désactivé'), ['element' => 'growl','params'=> ['type' => 'warning']]);
            $this->redirect($this->previous);
        }
    }

    /**
     * @access public
     * @param type $id
     * @return type
     */
    public function isEditable($id)
    {
        $liste = $this->Theme->find("first", [
            'conditions' => ['Theme.parent_id' => $id],
            'recursive' => -1]);
        return empty($liste);
    }
}
