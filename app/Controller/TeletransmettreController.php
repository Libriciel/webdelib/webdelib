<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('CakeTime', 'Utility');
App::uses('AppTools', 'Lib');

class TeletransmettreController extends AppController
{
    public $helpers = ['TinyMCE', 'ProjetUtil','DelibTdt'];
    public $uses = [
        'Acteur',
        'Deliberation', 'Typeacte', 'User', 'Annexe', 'Typeseance', 'Seance',
        'TypeSeance', 'Commentaire', 'ModelOdtValidator.Modeltemplate', 'Theme',
        'Collectivite', 'Vote', 'Listepresence', 'Infosupdef', 'Infosup', 'Historique',
        'Cakeflow.Circuit', 'Cakeflow.Composition', 'Cakeflow.Etape', 'Cakeflow.Traitement',
        'Cakeflow.Visa', 'Nomenclature', 'DeliberationSeance', 'DeliberationTypeseance',
        'DeliberationUser', 'Service', 'Cron',
        'Nature','Typologiepiece'
    ];
    public $components = [
        'RequestHandler',
        'ProjetTools',
        'ModelOdtValidator.Fido',
        'SabreDav',
        'Wopi',
        'Email',
        'Acl',
        'Iparapheur',
        'Filtre',
        'Progress',
        'Conversion',
        'PDFStamp',
        'S2low',
        'Paginator',
        'Teletransmission',
        'Auth' => [
            'mapActions' => [
                'read' => [],
                'create' => [],
                'update' => [],
                'delete' => [],
                'autresActesCancelSendToTDT',
                'deliberationCancelSendToTDT',
                'allow' => [
                    'getClassification',
                    'getTypologiePiecesAjax',
                    'typologiePieceToTdtAjax',
                    'depotManuel',
                    'editInfoSendToTdt',
                    'sendToTdt',
                ]
            ]
        ]
    ];

    /**
     * Envoi par lot de deliberations au TdT (s2low ou pastell)
     *
     * @access public
     * @return type
     */
    public function sendToTdt()
    {
        if ($this->Acl->check(
            ['User' => ['id' => $this->Auth->user('id')]],
            'Postseances/toSend',
            'read'
        ) ||
            $this->Acl->check(
                ['User' => ['id' => $this->Auth->user('id')]],
                'AutresActes/autresActesAEnvoyer',
                'read'
            )
        ) {
            parent::sendToTdt();
        } else {
            $this->Flash->set(
                __(
                    "Vous ne pouvez pas télétransmettre.",
                ),
                [
                    'element' => 'growl',
                    'params' => ['type' => 'danger']]
            );
        }

        return $this->redirect($this->previous);
    }

    public function depotManuel()
    {
        parent::sendToTdt();

        return $this->redirect($this->previous);
    }

    /**
     * @access public
     * @return type
     */
    public function getClassification()
    {
        App::uses('Tdt', 'Lib');
        $this->Tdt = new Tdt();
        if ($this->Tdt->updateClassification()) {
            $this->Flash->set(
                __('Les données de classification sont à jour'),
                ['element' => 'growl']
            );

            return $this->redirect($this->previous);
        } else {
            $this->Flash->set(
                __('Erreur lors de la récupération de la classification '),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );

            return $this->redirect($this->previous);
        }
    }

    /**
     * @access public
     * @param type $typeacte_id
     */
    public function getTypologiePiecesAjax()
    {
        $this->autoRender = false;
        $this->response->type(['json' => 'text/x-json']);
        $this->RequestHandler->respondAs('json');
        $this->response->body(
            json_encode($this->Teletransmission->getTypologiePieces(
                $this->data['type'],
                $this->data['typeacte_id'],
                !empty($this->data['num_pref']) ? $this->data['num_pref'] : null,
                !empty($this->data['projet_id']) ? $this->data['projet_id'] : null,
                !empty($this->data['typologiepiece_code'])
                    ? $this->data['typologiepiece_code'] : null
            ))
        );
        return $this->response;
    }

    public function autresActesCancelSendToTDT($id = null)
    {
        $this->cancelSendToTDT($id);
    }

    public function deliberationCancelSendToTDT($id = null)
    {
        $this->cancelSendToTDT($id);
    }

    /**
     * [public description]
     * @var [type]
     *
     * @version 5.1.2
     * @since 5.1.1
     */
    public function typologiePieceToTdtAjax()
    {
        parent::typologiePieceToTdtAjax();
    }

    /**
     * Set a new deliberation number (num_delib is an intern unique number for the government entity)
     * @version 5.1
     */
    private function cancelSendToTDT($id = null)
    {
        if ($id) {
            $this->Deliberation->id = $id;
            $deliberation = $this->Deliberation->find('first', [
                'fields'=>['tdt_status'],
                'conditions'=> [
                        'Deliberation.id'=>$id
                    ],
                'recursive'=>-1,
            ]);
            $deliberation["Deliberation"]['signee']=$this->request->data['Deliberation']['signee'];
            $deliberation["Deliberation"]['num_delib']=$this->request->data['Deliberation']['num_delib'];
            $canceled = $this->Deliberation->isCancelableForTdt()
                ? $this->Deliberation->resetSendTDT($deliberation) : false;
            if ($canceled===true) {
                $this->Flash->set(__("L’annulation a été prise en compte."), [
                    'element' => 'growl',
                ]);
            } else {
                $message='';
                foreach ($canceled as $cancelErrorMessage) {
                    $message .= implode('\n', $cancelErrorMessage).'\n';
                }

                $this->Flash->set($message, [
                    'element' => 'growl',
                    'params' => ['type' => 'danger']
                ]);
            }
        }
        return $this->redirect($this->previous);
    }

    /**
     * modify informations for send to TDT (s2low ou pastell)
     *
     * @access public
     * @return type
     */
    public function editInfoSendToTdt()
    {
        parent::editInfoSendToTdt();
    }
}
