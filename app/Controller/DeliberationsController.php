<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('CakeTime', 'Utility');
App::uses('AppTools', 'Lib');


class DeliberationsController extends AppController
{
    public $helpers = ['TinyMCE', 'ProjetUtil','DelibTdt'];
    public $uses = [
        'Acteur',
        'Deliberation', 'Typeacte', 'User', 'Annexe', 'Typeseance', 'Seance',
        'TypeSeance', 'Commentaire', 'ModelOdtValidator.Modeltemplate', 'Theme',
        'Collectivite', 'Vote', 'Listepresence', 'Infosupdef', 'Infosup', 'Historique',
        'Cakeflow.Circuit', 'Cakeflow.Composition', 'Cakeflow.Etape', 'Cakeflow.Traitement',
        'Cakeflow.Visa', 'Nomenclature', 'DeliberationSeance', 'DeliberationTypeseance',
        'DeliberationUser', 'Service', 'Cron',
        'Nature','Typologiepiece',
        'ProjetEtat'
    ];
    public $components = [
        'RequestHandler',
        'ProjetTools',
        'Acl',
        'Filtre',
        'Progress',
        'Paginator',
        'Auth' => [
            'mapActions' => [
                'read' => [],
                'create' => [],
                'update' => [],
                'delete' => ['delete'],
                'tousLesProjetsSansSeance' => ['attribuerSeance'],
                'allow' => [
                    'getTypeseancesParTypeacteAjax',
                    'getSeancesParTypeseanceAjax',
                ]
            ]
        ]
    ];

    /**
     * Affiche la liste de tous les projets en cours de validation
     * Permet de valider en urgence un projet
     *
     * @access public
     *
     * @version 5.1.2
     * @since 4.3
     */
    public function tousLesProjetsValidation()
    {
        $this->set('actions_possibles', ['validerUrgence' => 'Valider en urgence']);

        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);
        $conditions = $this->ProjetTools->handleConditions($this->Filtre->conditions());
        //$conditions =  $this->Filtre->conditions();
        // lecture en base
        $conditions['Deliberation.etat'] = 1;
        $conditions['Deliberation.parent_id'] = null;

        $joins = array_merge(
            [
            $this->Deliberation->join(
                'Typeacte',
                ['type' => 'INNER', 'conditions' => ['Typeacte.deliberant' => true]]
            ),
            $this->Typeacte->join('Nature', ['type' => 'INNER']),
            ],
            $this->handleJoins()
        );

        $this->paginate = ['Deliberation' => [
                'countField' => 'DISTINCT Deliberation.id',
                'conditions' => $conditions,
                'fields' => ['DISTINCT Deliberation.id', 'Deliberation.objet', 'Deliberation.etat',
                    'Deliberation.signee',
                    'Deliberation.titre', 'Deliberation.date_limite', 'Deliberation.anterieure_id',
                    'Deliberation.num_pref', 'Deliberation.redacteur_id', 'Deliberation.rapporteur_id',
                    'Deliberation.circuit_id',
                    'Deliberation.typeacte_id', 'Deliberation.theme_id', 'Deliberation.service_id',
                    'Typeacte.name', 'Nature.code'],
                'joins' => $joins,
                'contain' => [
                    'Annexe' => ['fields' => ['id', 'filename'], 'order' => ['position']],
                    'Service' => ['fields' => ['name']],
                    'Theme' => ['fields' => ['libelle']],
                    'Circuit' => ['fields' => ['nom']],
                    'DeliberationTypeseance' => ['fields' => ['id'],
                        'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action'],
                        ]],
                    'DeliberationSeance' => ['fields' => ['id'],
                        'Seance' => ['fields' => ['id', 'date', 'type_id'],
                            'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]]]],
                'order' => ['Deliberation.id' => 'DESC'],
                'allow' => ['typeacte_id'],
                'recursive' => -1,
                'limit' => 10,
        ]];

        $projets = $this->Paginator->paginate('Deliberation');

        $this->ProjetTools->sortProjetSeanceDate($projets);
        $this->ajouterFiltre($projets);
        $this->set('crumbs', [__('Tous les projets'), __('Délibérations'), __('À valider')]);
        $this->afficheProjets(
            'traitement_lot',
            $projets,
            __('Projets en cours d\'élaboration et de validation')
        );
    }

    /**
     * Affiche la liste de tous les projets en cours de redaction, validation, validés sans séance
     * Permet de modifier un projet validé si l'utilisateur à les droits editerTous
     *
     * @access public
     *
     * @version 5.1.2
     * @since 4.3
     */
    public function tousLesProjetsSansSeance()
    {
        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);

        $conditions = $this->ProjetTools->handleConditions($this->Filtre->conditions());

        //$natures_id = array($conditions['Deliberation.typeacte_id']);

        $db = $this->Deliberation->getDataSource();
        $subQuery = [
            'fields' => [
                'DeliberationSeance.deliberation_id'
            ],
            'contain' => false,
            'joins' => [
                $this->DeliberationSeance->join('Deliberation', ['type' => 'INNER']),
            ],
            'conditions' => [
                'Deliberation.parent_id IS NULL',
                'Deliberation.etat' => [0, 1, 2]
            ]
        ];

        $subQuery = $this->DeliberationSeance->sql($subQuery);

        $subQuery = ' "' . $this->Deliberation->alias . '"."id" NOT IN (' . $subQuery . ') ';

        $conditions[] = $db->expression($subQuery);
        if (!isset($conditions['AND']['Deliberation.etat'])) {
            $conditions['AND']['Deliberation.etat'] = [0, 1, 2];
        }
        $conditions['AND']['Deliberation.parent_id'] = null;

        $joins = array_merge([
            $this->Deliberation->join('Typeacte', ['type' => 'INNER', 'conditions' => [
                'Typeacte.deliberant' => true
            ]]),
            $this->Typeacte->join('Nature', ['type' => 'INNER']),
        ], $this->handleJoins());

        $this->paginate = ['Deliberation' => [
                'countField' => 'DISTINCT Deliberation.id',
                'fields' => ['DISTINCT Deliberation.id', 'Deliberation.objet',
                    'Deliberation.etat', 'Deliberation.signee',
                    'Deliberation.titre', 'Deliberation.date_limite',
                    'Deliberation.anterieure_id', 'Deliberation.num_pref',
                    'Deliberation.redacteur_id', 'Deliberation.rapporteur_id', 'Deliberation.circuit_id',
                    'Deliberation.typeacte_id',
                    'Deliberation.theme_id', 'Deliberation.service_id', 'Typeacte.name', 'Nature.code'],
                'joins' => $joins,
                'contain' => [
                    'User',
                    'Annexe' => ['fields' => ['id', 'filename'], 'order' => ['position']],
                    'Service' => ['fields' => ['name']],
                    'Theme' => ['fields' => ['libelle']],
                    'Circuit' => ['fields' => ['nom']],
                    'DeliberationTypeseance' => ['fields' => ['id'],
                        'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action'],
                        ]],
                    'DeliberationSeance' => ['fields' => ['id'],
                        'Seance' => ['fields' => ['id', 'date', 'type_id'],
                            'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]]]],
                'conditions' => $conditions,
                'order' => ['Deliberation.id' => 'DESC'],
                'recursive' => -1,
                'limit' => 10,
                'allow' => ['typeacte_id']
        ]];

        $projets = $this->Paginator->paginate('Deliberation');

        $dateNow = date('Y-m-d 00:00:00');
        foreach ($projets as $keyProjet => $projet) {
            $projets[$keyProjet]['Typeacte']['Nature'] = $projets[$keyProjet]['Nature'];
            $projets[$keyProjet]['Seances'] =
                $this->Seance->generateList(
                    ['date >=' => $dateNow],
                    true,
                    $projet['Deliberation']['typeacte_id']
                );
        }

        $this->ajouterFiltre($projets);
        $this->Filtre->delCritere('DeliberationSeanceId');
        $this->Filtre->delCritere('DeliberationTypeseanceId');
        $this->set('crumbs', [__('Tous les projets'), __('Délibérations'), __('À attribuer')]);
        $this->afficheProjets(
            'index',
            $projets,
            __('Projets non associés à une séance'),
            ['attribuerSeance']
        );
    }

    /**
     * Affiche la liste de tous les projets validés liés à une séance
     *
     * @access public
     *
     * @version 5.1.2
     * @since 4.3
     */
    public function tousLesProjetsAFaireVoter()
    {
        $projets_id = [];

        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);
        $conditions = $this->ProjetTools->handleConditions($this->Filtre->conditions());

        $conditions['Deliberation.etat'] = 2;
        $conditions['Deliberation.parent_id'] = null;

        $projets = $this->DeliberationSeance->find('all', [
            'fields' => [
                'DeliberationSeance.deliberation_id',
                'DeliberationSeance.seance_id'
            ],
            'contain' => ['Deliberation.id', 'Seance.id'],
            'conditions' => $conditions,
            'joins' => $this->handleJoins(),
            'recursive' => -1,
            'order' => ['Deliberation.id DESC'],
        ]);
        if (!empty($projets)) {
            foreach ($projets as $projet) {
                $projets_id[] = $projet['DeliberationSeance']['deliberation_id'];
            }
        }
        $joins = array_merge([
            $this->Deliberation->join(
                'Typeacte',
                [
                    'type' => 'INNER', 'conditions' => ['Typeacte.deliberant' => true]
                ]
            ),
            $this->Typeacte->join('Nature', ['type' => 'INNER']),
                ], $this->handleJoins());


        $this->paginate = ['Deliberation' => [
                'countField' => 'DISTINCT Deliberation.id',
                'fields' => ['DISTINCT Deliberation.id', 'Deliberation.objet',
                    'Deliberation.etat', 'Deliberation.signee',
                    'Deliberation.titre', 'Deliberation.date_limite',
                    'Deliberation.anterieure_id', 'Deliberation.num_pref',
                    'Deliberation.redacteur_id' , 'Deliberation.rapporteur_id', 'Deliberation.circuit_id',
                    'Deliberation.typeacte_id',
                    'Deliberation.theme_id', 'Deliberation.service_id', 'Typeacte.name', 'Nature.code'],
                'joins' => $joins,
                'contain' => [
                    'User',
                    'Annexe' => ['fields' => ['id', 'filename'], 'order' => ['position']],
                    'Service' => ['fields' => ['name']],
                    'Theme' => ['fields' => ['libelle']],
                    'Circuit' => ['fields' => ['nom']],
                    'DeliberationTypeseance' => ['fields' => ['id'],
                        'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action'],
                        ]],
                    'DeliberationSeance' => ['fields' => ['id'],
                        'Seance' => ['fields' => ['id', 'date', 'type_id'],
                            'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]]]],
                'conditions' => ['Deliberation.id' => $projets_id],
                'order' => ['Deliberation.id' => 'DESC'],
                'recursive' => -1,
                'limit' => 10,
                'allow' => ['typeacte_id']
        ]];

        $projets = $this->Paginator->paginate('Deliberation');

        $this->ProjetTools->sortProjetSeanceDate($projets);
        $this->ajouterFiltre($projets);
        $this->set('crumbs', [
            __('Tous les projets'), __('Délibérations'), __('À faire voter')
        ]);
        $this->afficheProjets(
            'traitement_lot',
            $projets,
            __('Projets validés associés à une séance')
        );
    }

    /**
     * @version 5.1
     * @since 4.3
     * @access private
     * @param type $projets
     */
    private function ajouterFiltre(&$projets)
    {
        if (!$this->Filtre->critereExists() && !empty($projets)) {
            $seances = $this->Seance->find('all', [
                'fields' => ['Seance.id', 'Seance.type_id', 'Seance.date'],
                'conditions' => ['Seance.traitee' => 0],
                'contain' => ['Typeseance.libelle', 'Typeseance.retard'],
                'order' => ['Typeseance.libelle' => 'ASC', 'Seance.date' => 'ASC'],
            ]);

            $options_seances = [];
            foreach ($seances as $seance) {
                //Voir tous les projets ou tous les futurs dates avec un delais respecté
                $options_seances[$seance['Seance']['id']] =
                  $seance['Typeseance']['libelle'] .
                  ' : ' .
                  CakeTime::i18nFormat($seance['Seance']['date'], '%A %e %B %Y') .
                  ' à ' .
                  CakeTime::i18nFormat($seance['Seance']['date'], '%k:%M');
            }

            $this->Filtre->addCritere('DeliberationSeanceId', [
                'field' => 'DeliberationSeance_Filter.seance_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner une séance'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'label' => __('Séances'),
                    'options' => $options_seances]]);

            $typeseances = $this->Typeseance->find(
                'list',
                ['fields' => ['id', 'libelle'], 'recursive' => -1, 'order' => ['Typeseance.libelle' => 'ASC']]
            );
            $this->Filtre->addCritere('DeliberationTypeseanceId', [
                'field' => 'DeliberationTypeseance_Filter.typeseance_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un type de séance'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'label' => __('Type de séance'),
                    'options' => $typeseances]]);

            $typeactes = $this->Deliberation->Typeacte->find('list', [
                'recursive' => -1,
                'order' => ['Typeacte.name' => 'ASC'],
                'allow' => ['Typeacte.id' => 'read']]);

            $this->Filtre->addCritere('Typeacte', [
                'field' => 'Deliberation.typeacte_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Type d\'acte'),
                    'multiple' => true,
                    'data-placeholder' => __('Sélectionner un type d\'acte'),
                    'data-allow-clear' => true,
                    'options' => $typeactes
                ]]);

            $themes = $this->Deliberation->Theme->generateTreeListByOrder(
                ['Theme.actif' => '1'],
                '&nbsp;&nbsp;&nbsp;&nbsp;'
            );
            $this->Filtre->addCritere('ThemeId', [
                'field' => 'Deliberation.theme_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un thème'),
                    'data-allow-clear' => true,
                    'label' => __('Thème'),
                    'multiple' => true,
                    'options' => $themes,
                    'escape' => false
                ]]);
            $services = $this->Service->find('list', [
                'fields' => ['id', 'name'],
                'order' => ['Service.name' => 'ASC'],
                'conditions' => ['actif' => true], 'recursive' => -1]);
            $this->Filtre->addCritere('ServiceId', [
                'field' => 'Deliberation.service_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un service émetteur'),
                    'data-allow-clear' => true,
                    'label' => __('Service émetteur'),
                    'multiple' => true,
                    'options' => $services,
                ]]);
            $circuits = $this->Circuit->find('list', [
                'fields' => ['id', 'nom'],
                'order' => ['Circuit.nom' => 'ASC'],
                'conditions' => ['actif' => true], 'recursive' => -1]);
            $this->Filtre->addCritere('CircuitId', [
                'field' => 'Deliberation.circuit_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un circuit'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'label' => __('Circuit de validation'),
                    'options' => $circuits,
                ]]);

            $etats = $this->ProjetTools->generateListEtat($this->request->param('action'));
            if (!empty($etats)) {
                $this->Filtre->addCritere('Etat', [
                'field' => 'Deliberation.etat',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un état'),
                    'data-allow-clear' => true,
//                    ici le multi déconne
                    'label' => __('État du projet'),
//                    'empty' => __('Tous'),
                    'multiple' => true,
                    'options' => $etats
                ]]);
            }

            return true;
        }
        return false;
    }

    /**
     * Attribue une séance à un projet
     * @since 4.2
     * @version 5.0
     * @access public
     */
    public function attribuerSeance()
    {
        if (!empty($this->data['Deliberation']['seance_id'])) {
            $nbSeancesDeliberantes = 0;
            foreach ($this->data['Deliberation']['seance_id'] as $seance_id) {
                $seance = $this->Seance->find('first', [
                    'fields' => ['Seance.id', 'Seance.type_id'],
                    'contain' => ['Typeseance.action'],
                    'conditions' => ['Seance.id' => $seance_id],
                    'recursive' => -1
                ]);

                if ($seance['Typeseance']['action'] == 0) {
                    $nbSeancesDeliberantes++;
                }
            }
            if ($nbSeancesDeliberantes < 2) {
                foreach ($this->data['Deliberation']['seance_id'] as $seance_id) {
                    $seance = $this->Seance->find('first', [
                        'fields' => ['Seance.id', 'Seance.type_id'],
                        'conditions' => ['Seance.id' => $seance_id],
                        'recursive' => -1
                    ]);

                    $this->DeliberationTypeseance->create();
                    $this->DeliberationTypeseance->save(['deliberation_id' => $this->data['Deliberation']['id'],
                        'typeseance_id' => $seance['Seance']['type_id']]);

                    $this->Deliberation->DeliberationSeance->addDeliberationSeance(
                        $this->data['Deliberation']['id'],
                        $seance_id
                    );
                }
                $this->Flash->set(__('Séance(s) enregistrée(s)'), ['element' => 'growl']);
            } else {
                $this->Flash->set(__('Une seule séance délibérante par projet'), [
                    'element' => 'growl',
                    'params' => ['type' => 'danger']]);
            }
        } else {
            $this->Flash->set(__('Vous devez selectionner une séance'), [
                'element' => 'growl',
                'params' => ['type' => 'danger']]);
        }

        $this->redirect($this->previous);
    }

    /**
     * @access private
     * @param type $conditions
     * @return type
     */
    private function handleJoins()
    {
        $joins = [
            ['table' => 'deliberations_seances',
                'alias' => 'DeliberationSeance_Filter',
                'type' => 'LEFT',
                'conditions' => [
                    'Deliberation.id = DeliberationSeance_Filter.deliberation_id',
                ]
            ],
            ['table' => 'seances',
                'alias' => 'Seance_Filter',
                'type' => 'LEFT',
                'conditions' => [
                    'DeliberationSeance_Filter.seance_id = Seance_Filter.id'
                ]
            ],
            ['table' => 'deliberations_typeseances',
                'alias' => 'DeliberationTypeseance_Filter',
                'type' => 'LEFT',
                'conditions' => [
                    'Deliberation.id = DeliberationTypeseance_Filter.deliberation_id'
                ]
            ],
        ];

        return $joins;
    }

    /**
     * @access public
     * @param type $typeacte_id
     */
    public function getTypeseancesParTypeacteAjax()
    {
        $this->autoRender = false;
        $this->response->type(['json' => 'text/x-json']);
        $this->RequestHandler->respondAs('json');
        $this->response->body(json_encode(
            $this->ProjetTools->getTypeseancesParTypeacte(
                $this->data['typeacte_id'],
                !empty($this->data['projet_id']) ? $this->data['projet_id'] : null
            )
        ));
        return $this->response;
    }

    /**
     *
     * @return type
     */
    public function getSeancesParTypeseanceAjax()
    {
        $this->autoRender = false;
        $this->response->type(['json' => 'text/x-json']);
        $this->RequestHandler->respondAs('json');
        $options = $this->ProjetTools->getSeancesParTypeseance(
            $this->data['typeseances_ids'],
            !empty($this->data['projet_id']) ? $this->data['projet_id'] : null,
            !empty($this->data['seances_ids']) ? $this->data['seances_ids'] : null
        );
        $this->response->body(json_encode($options));
        return $this->response;
    }

    /**
     * @access public
     * @param type $anterieure_id
     * @param type $nb_recursion
     * @param type $listeAnterieure
     * @param type $action
     * @return type
     */
    private function chercherVersionAnterieure($anterieure_id, $nb_recursion, $listeAnterieure, $action)
    {
        if ($anterieure_id != 0) {
            $projet = $this->Deliberation->find('first', [
                'fields' => ['id', 'created', 'anterieure_id'],
                'conditions' => ["Deliberation.id" => $anterieure_id],
                'recursive' => -1,
            ]);

            $date_version = $projet['Deliberation']['created'];
            $listeAnterieure[$nb_recursion]['id'] = $anterieure_id;

            $listeAnterieure[$nb_recursion]['lien'] = Router::url(
                ['controller' => 'deliberations', 'action' => $action, $anterieure_id]
            );
            $listeAnterieure[$nb_recursion]['date_version'] = $date_version;
            //on stocke les id des delibs anterieures
            $listeAnterieure = $this->chercherVersionAnterieure(
                $projet['Deliberation']['anterieure_id'],
                $nb_recursion + 1,
                $listeAnterieure,
                $action
            );
        }
        return $listeAnterieure;
    }
}
