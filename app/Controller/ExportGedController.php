<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppController', 'Controller');
App::uses('AppTools', 'Lib');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('Ged', 'Lib');

/**
 * Controller export GED des autres actes
 *
 * @package app.Controller
 * @version 5.1.4
 * @since 5.1.0
 */
class ExportGedController extends AppController
{
    /** @var Array Contains a uses */
    public $uses = ['Deliberation','Historique'];
    /** @var Array Contains a components */
    public $components = [
        'RequestHandler',
        'Progress',
        'Conversion',
        'ExportGedCMIS',
        'Auth' => [
            'mapActions' => [
                'read' => [''],
                'sendToGedDeliberation',
                'sendToGedAutreActe',
            ],
        ]];

    public function sendToGedDeliberation($projet_id = null)
    {
        $this->sendToGed($projet_id);
    }

    public function sendToGedAutreActe($projet_id = null)
    {
        $this->sendToGed($projet_id);
    }
    /**
     * Envoyer à la GED
     *
     * @version 5.1
     * @access public
     * @return view
     */
    private function sendToGed($projet_id = null)
    {
        if (!Configure::read('USE_GED')) {
            $this->Flash->set(
                __('Erreur : GED désactivé. Pour activer ce service, veuillez contacter votre administrateur.'),
                ['element' => 'growl','params'=>['type' => 'danger']]
            );
            return $this->redirect($this->previous);
        }

        if ($this->request->is('post')) {
            if (!empty($this->request->data['Deliberation'])) {
                try {
                    foreach ($this->request->data['Deliberation'] as $id => $acte) {
                        if ((int)$acte['send'] === 1) {
                            if (Configure::read('GED') === 'PASTELL') {
                                $this->sendToGedPastell($id);
                            } else {
                                $this->sendToGedCmis($id);
                            }
                            sleep(1);
                        }
                    }
                    $this->Flash->set(
                        __('Envoi(s) effectué(s) avec succès'),
                        ['element' => 'growl', 'params' => ['type' => 'info']]
                    );
                } catch (Exception $e) {
                    $this->Flash->set(
                        __('Erreur (%s): %s', $e->getCode(), $e->getMessage()),
                        ['element' => 'growl', 'params' => ['type' => 'danger']]
                    );
                    return $this->redirect($this->previous);
                }
            } else {
                $this->Flash->set(
                    __('Vous devez sélectionner au moins un acte à envoyer.'),
                    ['element' => 'growl', 'params' => ['type' => 'danger']]
                );
                return $this->redirect($this->previous);
            }
        }

        if (!empty($projet_id) && $this->request->is('get') && Configure::read('GED') === 'PASTELL') {
            try {
                $this->sendToGedPastell($projet_id);
                $this->Flash->set(
                    __('Envoi effectué avec succès'),
                    ['element' => 'growl', 'params' => ['type' => 'info']]
                );
            } catch (Exception $e) {
                $this->Flash->set(
                    __('Erreur (%s): %s', $e->getCode(), $e->getMessage()),
                    ['element' => 'growl', 'params' => ['type' => 'danger']]
                );
            }
        }

        return $this->redirect($this->previous);
    }

    /**
     * Envoi pour une GED en protocole CMIS
     *
     * @version 5.1
     * @access private
     * @param int $acte_id
     */
    private function sendToGedCmis($acte_id)
    {
        $folderSend = new Folder(
            AppTools::newTmpDir(TMP . 'files' . DS . 'export' . DS),
            true,
            0777
        );

        try {
            CakeLog::info(__('[Ged][%s]cmisService: Start', __METHOD__), 'connector');
            $this->ExportGedCMIS->service($folderSend);

            CakeLog::info(
                __('[Ged][%s]Version d\'export v%s', __METHOD__, Configure::read('GED_XML_VERSION')),
                'connector'
            );
            $cmisFolder = $this->{'sendToGedVersion'
            . (Configure::read('GED_XML_VERSION')>3 ? Configure::read('GED_XML_VERSION') : 3)}($acte_id);

            $this->Progress->at(90, __('Envoi des fichiers'));
            $this->ExportGedCMIS->send($folderSend, $cmisFolder);

            $folderSend->delete();
            CakeLog::info(__('[Ged][%s]cmisService: Send ok', __METHOD__), 'connector');
        } catch (Exception $e) {
            echo var_export($e->getMessage(), true);
            $message = '';
            switch ($e->getCode()) {
                case 500:
                    $message = 'Erreur interne';
                    break;
                case 404:
                    $message = 'Ressource non trouvée';
                    break;
                case 409:
                    $message = 'Conflict, l\'acte existe déjà dans la GED';
                    break;
                default:
                    $message = strip_tags($e->getMessage());
                    break;
            }

            CakeLog::error(
                __('[Ged][%s]Send error(%s)', __METHOD__, $e->getCode()) . "\n" . $e->getMessage(),
                'connector'
            );
            throw new Exception(__('[Ged]') . ' ' . $message, $e->getCode());
        }
    }

    /**
     * Envoi pour en GED via pastell
     * @param int $acte_id
     * @throws Exception
     * @version 5.1
     */
    private function sendToGedPastell($acte_id)
    {
        $this->Deliberation->Behaviors->load(
            'Version',
            $this->Deliberation->actsAsVersionOptionsList['Version']
        );

        try {
            $this->Ged = new Ged();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        $this->Deliberation->id = $acte_id;

        $acte = $this->Deliberation->find('first', [
            'fields' => [
                'id', 'objet_delib', 'num_delib', 'pastell_id', 'typeacte_id', 'numero_depot',
                'delib_pdf'
            ],
            'conditions' => ['Deliberation.id' => $acte_id],
            'recursive' => -1,
        ]);
        //Récupération de la nature
        $nature = ClassRegistry::init('Nature');
        $acte['acte_nature_code'] = $nature->getNatureCodeByTypeActe($acte['Deliberation']['typeacte_id']);
        $documentPrincipal = $acte['Deliberation']['delib_pdf'];
        unset($acte['Deliberation']['delib_pdf']);
        try {
            $sent = $this->Ged->send(
                $acte,
                $documentPrincipal,
                $this->Deliberation->getAnnexesToSend($acte['Deliberation']['id'])
            );
            if ($sent) {
                $this->Deliberation->saveField('numero_depot', $acte['Deliberation']['numero_depot'] + 1);
                $this->Historique->enregistre(
                    $acte_id,
                    $this->Auth->user('id'),
                    'Acte envoyé à la GED'
                );
            }
        } catch (Exception $e) {
            CakeLog::error(__("Export GED: Erreur %s ! \n %s", $e->getCode(), $e->getMessage()));
            throw new Exception(
                __('Dépôt (%s) : Envoi à la GED échoué', $acte['Deliberation']['num_delib']) .
                "\n" . $e->getMessage(),
                $e->getCode()
            );
        }
    }

    /**
     * Création des fichiers pour l'export GED en version 2 et 3
     * @version 5.1.0
     * @param type $folderTmp
     * @param type $folderSend
     * @param type $acte_id
     * @return type
     * @throws Exception
     */
    private function sendToGedVersion3($acte_id)
    {
        try {
            $delib = $this->Deliberation->find('first', [
                'conditions' => ['Deliberation.id' => $acte_id],
                'fields' => ['Deliberation.id', 'Deliberation.num_delib',
                    'Deliberation.objet_delib', 'Deliberation.titre',
                    'Deliberation.delib_pdf', 'Deliberation.tdt_data_pdf',
                    'Deliberation.tdt_data_bordereau_pdf', 'Deliberation.deliberation',
                    'Deliberation.deliberation_size', 'Deliberation.signature',
                    'Deliberation.tdt_ar_date', 'Deliberation.tdt_ar', 'Deliberation.signee',
                    'numero_depot', 'date_acte', 'signature_type',
                    'Deliberation.parapheur_etat', 'Deliberation.tdt_id'],
                'contain' => [
                    'Service' => ['fields' => ['name']],
                    'Theme' => ['fields' => ['libelle']],
                    'Typeacte' => [
                        'fields' => ['name', 'modele_projet_id'],
                        'Nature' => ['fields' => ['name']]
                    ],
                    'Redacteur' => ['fields' => ['nom', 'prenom']],
                    'Rapporteur' => ['fields' => ['nom', 'prenom']],
                ]]);

            // Création du répertoire de séance
            $result = $this->ExportGedCMIS->getClient()->getFolderTree($this->ExportGedCMIS->getFolderId(), 1);
            $this->ExportGedCMIS->deletetoGed(
                Configure::read('CMIS_REPO') . '/' . $delib['Deliberation']['num_delib']
            );

            $zip = new ZipArchive();
            $zip->open($this->ExportGedCMIS->folderSend->pwd() . DS . 'documents.zip', ZipArchive::CREATE);
            // Création du dossier //NUM_delib
            $my_folder = $this->ExportGedCMIS->getClient()->createFolder(
                $this->ExportGedCMIS->getFolderId(),
                $delib['Deliberation']['num_delib']
            );

            $dom = new DOMDocument('1.0', 'utf-8');
            $dom->formatOutput = true;

            $idDepot = $delib['Deliberation']['numero_depot'] + 1;

            $dom_depot = $this->ExportGedCMIS->createElement($dom, 'depot', null, [
                'versionDepot' => 3,
                'idDepot' => $idDepot,
                'dateDepot' => date("Y-m-d H:i:s"),
                'xmlns:webdelibdossier' => 'http://www.adullact.org/webdelib/infodossier/1.0',
                'xmlns:xm' => 'http://www.w3.org/2005/05/xmlmime']);

            $doc = $this->ExportGedCMIS->createElement($dom, 'dossierActe', null, ['idActe' => $acte_id]);
            $doc->appendChild(
                $this->ExportGedCMIS->createElement($dom, 'libelle', $delib['Deliberation']['objet_delib'])
            );
            $doc->appendChild(
                $this->ExportGedCMIS->createElement($dom, 'titre', $delib['Deliberation']['titre'])
            );
            $doc->appendChild(
                $this->ExportGedCMIS->createElement($dom, 'natureACTE', $delib['Typeacte']['Nature']['name'])
            );
            $doc->appendChild(
                $this->ExportGedCMIS->createElement($dom, 'dateACTE', $delib['Deliberation']['date_acte'])
            );
            $doc->appendChild(
                $this->ExportGedCMIS->createElement($dom, 'numeroACTE', $delib['Deliberation']['num_delib'])
            );
            $doc->appendChild(
                $this->ExportGedCMIS->createElement($dom, 'themeACTE', $delib['Theme']['libelle'])
            );
            $doc->appendChild(
                $this->ExportGedCMIS->createElement($dom, 'emetteurACTE', $delib['Service']['name'])
            );
            $doc->appendChild(
                $this->ExportGedCMIS->createElement(
                    $dom,
                    'redacteurACTE',
                    $delib['Redacteur']['prenom'] . ' ' . $delib['Redacteur']['nom']
                )
            );
            $doc->appendChild(
                $this->ExportGedCMIS->createElement(
                    $dom,
                    'rapporteurACTE',
                    $delib['Rapporteur']['prenom'] . ' ' . $delib['Rapporteur']['nom']
                )
            );
            $doc->appendChild(
                $this->ExportGedCMIS->createElement(
                    $dom,
                    'dateAR',
                    CakeTime::i18nFormat($delib['Deliberation']['tdt_ar_date'], '%A %d %B %Y')
                )
            ); // utile ??
            //Infos supps de délibération
            $this->ExportGedCMIS->createElementInfosupsByZip(
                $zip,
                $dom,
                $doc,
                $delib_id,
                'Deliberation'
            );

            $aDocuments = [];
            $i = 1;
            //Noeud document[TexteActe]
            $aDocuments['TexteActe'] = $i++;
            $delib_filename = $acte_id . '-' . $delib['Deliberation']['num_delib'] . '.pdf';
            $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                'idDocument' => $aDocuments['TexteActe'],
                'nom' => $delib_filename, 'relName' => $delib_filename,
                'type' => 'TexteActe']);

            if (!empty($delib['Deliberation']['signature'])) {
                $document->appendChild(
                    $this->ExportGedCMIS->createElement(
                        $dom,
                        'signature',
                        true,
                        ['formatignature' => $delib['Deliberation']['signature'] == 'PAdES' ? 'PAdES' : 'p7s']
                    )
                );
            }
            $document->appendChild($this->ExportGedCMIS->createElement($dom, 'mimetype', 'application/pdf'));
            $document->appendChild($this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8'));
            $doc->appendChild($document);
            //Ajout au zip
            $zip->addFromString($delib_filename, $delib['Deliberation']['delib_pdf']);

            //Noeud document[TexteActe]
            if (!empty($delib['Deliberation']['tdt_data_pdf'])) {
                $aDocuments['ActeTampon'] = $i++;
                $delib_filename = $acte_id . '-' . $delib['Deliberation']['num_delib'] . '.pdf';
                $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                    'idDocument' => $aDocuments['ActeTampon'],
                    'nom' => $delib_filename, 'relName' => 'ActeTampon' . DS . $delib_filename,
                    'type' => 'ActeTampon']);
                $document->appendChild($this->ExportGedCMIS->createElement($dom, 'signature', 'false'));
                $document->appendChild(
                    $this->ExportGedCMIS->createElement($dom, 'mimetype', 'application/pdf')
                );
                $document->appendChild($this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8'));
                $doc->appendChild($document);
                //Ajout au zip
                $zip->addFromString('ActeTampon' . DS . $delib_filename, $delib['Deliberation']['tdt_data_pdf']);
            }

            //Noeud document[Rapport]
            if (!empty($delib['Deliberation']['deliberation_size'])) {
                $aDocuments['Rapport'] = $i++;
                $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                    'idDocument' => $aDocuments['Rapport'],
                    'nom' => $delib_filename,
                    'relName' => 'Rapports' . DS . $delib_filename,
                    'type' => 'Rapport']);
                $document->appendChild($this->ExportGedCMIS->createElement($dom, 'signature', 'false'));
                $document->appendChild(
                    $this->ExportGedCMIS->createElement($dom, 'mimetype', 'application/pdf')
                );
                $document->appendChild($this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8'));
                $doc->appendChild($document);
                // fusion du rapport et ajout au zip
                $zip->addFromString(
                    'Rapports' . DS . $delib_filename,
                    $this->Deliberation->fusion(
                        $delib['Deliberation']['id'],
                        'rapport',
                        $delib['Typeacte']['modele_projet_id']
                    )
                );
            }

            if (!empty($delib['Deliberation']['signature']) && $delib['Deliberation']['signature_type'] != 'PAdES') {
                //Ajout de la signature (XML+ZIP)
                $signatureName = $delib['Deliberation']['id'] . '-signature.zip';
                $aDocuments['Signature'] = $i++;
                //Création du noeud XML
                $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                    'idDocument' => $aDocuments['Signature'],
                    'refDocument' => $aDocuments['TexteActe'],
                    'nom' => $signatureName, 'relName' => 'Signatures' . DS . $signatureName,
                    'type' => 'Signature']);
                $document->appendChild(
                    $this->ExportGedCMIS->createElement($dom, 'mimetype', 'application/zip')
                );
                $doc->appendChild($document);
                //Ajout à l'archive
                $zip->addFromString('Signatures' . DS . $signatureName, $delib['Deliberation']['signature']);
            }

            //Ajout du bordereau (XML+ZIP)
            if (!empty($aDocuments['ActeTampon'])) {
                $bordereauName = $delib['Deliberation']['id'] . '-bordereau.pdf';
                $aDocuments['Bordereau'] = $i++;
                //Création du noeud XML
                $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                    'idDocument' => $aDocuments['Bordereau'],
                    'refDocument' => $aDocuments['ActeTampon'],
                    'nom' => $bordereauName,
                    'relName' => 'Bordereaux' . DS . $bordereauName,
                    'type' => 'Bordereau']);
                $document->appendChild(
                    $this->ExportGedCMIS->createElement($dom, 'mimetype', 'application/pdf')
                );
                $doc->appendChild($document);
                //Ajout à l'archive
                $zip->addFromString(
                    'Bordereaux' . DS . $bordereauName,
                    $delib['Deliberation']['tdt_data_bordereau_pdf']
                );
            }
            //Ajout des annexes
            $annexes_id = $this->Deliberation->Annexe->getAnnexesFromDelibId($acte_id);
            if (!empty($annexes_id)) {
                foreach ($annexes_id as $annexe_key => $annexe_id) {
                    $aDocuments['Annexe'] = $i++;
                    $annexe = $this->Deliberation->Annexe->getContentToGed($annexe_id['Annexe']['id']);
                    $annexes_id[$annexe_key]['refDocument'] = $aDocuments['Annexe'];
                    //Création du noeud XML <document> de l'annexe
                    $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                        'idDocument' => $aDocuments['Annexe'],
                        'nom' => $annexe['name'],
                        'relName' => 'Annexes' . DS . $annexe['filename'],
                        'type' => 'Annexe']);
                    $document->appendChild($this->ExportGedCMIS->createElement($dom, 'titre', $annexe['titre']));
                    $document->appendChild($this->ExportGedCMIS->createElement($dom, 'signature', 'false'));
                    if ($annexe['joindre_ctrl_legalite']) {
                        $document->appendChild(
                            $this->ExportGedCMIS->createElement($dom, 'transmiesprefecture', 'true')
                        );
                    }
                    $document->appendChild($this->ExportGedCMIS->createElement($dom, 'mimetype', $annexe['filetype']));
                    $document->appendChild($this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8'));
                    $doc->appendChild($document);
                    //Ajout du fichier annexe à l'archive
                    $zip->addFromString('Annexes' . DS . $annexe['filename'], $annexe['data']);
                }
            }

            if (!empty($annexes_id)) {
                foreach ($annexes_id as $annexe_id) {
                    if (empty($annexe_id['Annexe']['tdt_data_pdf'])) {
                        continue;
                    }

                    $aDocuments['AnnexeTampon'] = $i++;
                    $annexe = $this->Deliberation->Annexe->getContentToGed($annexe_id['Annexe']['id']);
                    //Création du noeud XML <document> de l'annexe
                    $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                        'idDocument' => $aDocuments['AnnexeTampon'],
                        'refDocument' => $annexe_id['refDocument'],
                        'nom' => $annexe['name'],
                        'relName' => 'AnnexesTampon' . DS . $annexe['filename'],
                        'type' => 'AnnexeTampon']);
                    $document->appendChild($this->ExportGedCMIS->createElement($dom, 'titre', $annexe['titre']));
                    $document->appendChild($this->ExportGedCMIS->createElement($dom, 'signature', 'false'));
                    if ($annexe['joindre_ctrl_legalite']) {
                        $document->appendChild($this->ExportGedCMIS->createElement($dom, 'transmisprefecture', 'true'));
                    }
                    $document->appendChild($this->ExportGedCMIS->createElement($dom, 'mimetype', $annexe['filetype']));
                    $document->appendChild($this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8'));
                    $doc->appendChild($document);
                    //Ajout du fichier annexe à l'archive
                    $zip->addFromString('AnnexesTampon' . DS . $annexe['filename'], $annexe['data_tampon']);
                }
            }

            //Ajout de la signature (XML+ZIP)
            //Création du noeud XML
            if (!empty($delib['Deliberation']['tdt_ar'])) {
                $aDocuments['ARacte'] = $i++;
                $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                    'idDocument' => $aDocuments['ARacte'],
                    'nom' => $delib['Deliberation']['id'] . '-' . 'ARacte.xml',
                    'relName' => 'ARacte/' . $delib['Deliberation']['id'] . '-' . 'ARacte.xml',
                    'type' => 'ARacte']);
                $document->appendChild(
                    $this->ExportGedCMIS->createElement($dom, 'mimetype', 'application/xml')
                );
                $document->appendChild($this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8'));
                $doc->appendChild($document);
                //Ajout à l'archive
                $zip->addFromString(
                    'ARacte' . DS . $delib['Deliberation']['id'] . '-' . 'ARacte.xml',
                    $delib['Deliberation']['tdt_ar']
                );
            }

            $messages = $this->ExportGedCMIS->getTdtMessageForGed($acte_id);
            foreach ($messages as $message) {
                $aDocuments['TdtMessage'] = $i++;
                $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                    'idDocument' => $aDocuments['TdtMessage'],
                    'nom' => $message['name'],
                    'relName' => $message['type'] . '/' . $message['relName'],
                    'type' => $message['type']]);
                $document->appendChild(
                    $this->ExportGedCMIS->createElement($dom, 'mimetype', 'application/pdf')
                );
                $document->appendChild($this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8'));
                $doc->appendChild($document);
                //Ajout à l'archive
                $zip->addFromString($message['type'] . '/' . $message['relName'], $message['content_pdf']);
                if (!empty($message['reponses'])) {
                    $aDocuments['TdtMessageReponse'] = $aDocuments['TdtMessage'];
                    foreach ($message['reponses'] as $reponse) {
                        $aDocuments['TdtMessageReponse'] = $i++;
                        $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                            'idDocument' => $aDocuments['TdtMessageReponse'],
                            'nom' => $reponse['name'],
                            'relName' => $reponse['type'] . '/' . $reponse['relName'],
                            'type' => $reponse['type'],
                            'refDocument' => $aDocuments['TdtMessage']]);
                        $document->appendChild(
                            $this->ExportGedCMIS->createElement($dom, 'mimetype', 'application/pdf')
                        );
                        $document->appendChild(
                            $this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8')
                        );
                        $doc->appendChild($document);
                        //Ajout à l'archive
                        $zip->addFromString($reponse['type'] . '/' . $reponse['relName'], $reponse['content_pdf']);
                    }
                    $aDocuments['TdtMessage'] = $aDocuments['TdtMessageReponse'];
                }
            }
            $dom_depot->appendChild($doc);

            $zip->close();
            $dom->appendChild($dom_depot);

            $file = new File($this->ExportGedCMIS->folderSend->pwd() . DS . 'XML_DESC.xml');
            $file->write($dom->saveXML());
            $file->close();

            $this->Deliberation->id = $acte_id;
            $this->Deliberation->saveField('numero_depot', $idDepot);
            $this->Flash->set(
                __('Le dossier "%s" a été ajouté (Depot n° %s)', $delib['Deliberation']['num_delib'], $idDepot),
                ['element' => 'growl', 'params' => ['type' => 'important']]
            );
        } catch (Exception $e) {
            $this->log(
                'Export CMIS: Erreur ' . $e->getCode() . "! \n File:" . $e->getFile() . ' Line:' . $e->getLine(),
                'connector'
            );
            throw $e;
        }

        return !empty($my_folder) ? $my_folder : false;
    }

    /**
     * Création des fichiers pour l'export GED en version 2 et 3
     * @version 5.1.0
     * @param type $folderTmp
     * @param type $folderSend
     * @param type $acte_id
     * @return type
     * @throws Exception
     */
    private function sendToGedVersion4($acte_id)
    {
        try {
            CakeLog::info(__('[Ged][%s]getSeanceForGed: search', __METHOD__), 'connector');
            $acte = $this->getActeForGed($acte_id);

            //Création du répertoire CMIS
            $acte_folder = $this->createActeFolderCmis($acte);

            $dom = new DOMDocument('1.0', 'utf-8');
            $dom->formatOutput = true;

            $idDepot = $acte['Deliberation']['numero_depot'] + 1;

            $dom_depot = $this->ExportGedCMIS->createElement($dom, 'depot', null, [
                'versionDepot' => 4,
                'idDepot' => $idDepot,
                'dateDepot' => date("Y-m-d H:i:s"),
                'xmlns:webdelibdossier' => 'http://www.adullact.org/webdelib/infodossier/1.0',
                'xmlns:xm' => 'http://www.w3.org/2005/05/xmlmime']);

            $this->ExportGedCMIS->getActeCmis($dom, $dom_depot, $acte_id);

            $dom->appendChild($dom_depot);

            $file = new File($this->ExportGedCMIS->folderSend->pwd() . DS . 'XML_DESC.xml');
            $file->write($dom->saveXML());
            $file->close();
            $this->Deliberation->id = $acte_id;
            $this->Deliberation->saveField('numero_depot', $idDepot);
            $this->Flash->set(
                __('Le dossier "%s" a été ajouté (Depot n° %s)', $acte['Deliberation']['num_delib'], $idDepot),
                ['element' => 'growl', 'params' => ['type' => 'important']]
            );
        } catch (Exception $e) {
            $this->log(
                'Export CMIS: Erreur ' . $e->getCode() . "! \n File:" . $e->getFile() . ' Line:' . $e->getLine(),
                'connector'
            );
            throw $e;
        }

        return !empty($acte_folder) ? $acte_folder : false;
    }

    /**
     * Récupération des informations de séance
     * @param type $seance_id
     * @return type
     */
    private function getActeForGed($acte_id)
    {
        $acte = $this->Deliberation->find(
            'first',
            [
            'fields' => ['num_delib', 'numero_depot'],
            'conditions' => ['Deliberation.id' => $acte_id],
            'recursive' => -1]
        );

        //Modification du libelle de séance pour la Ged
        $acte['Deliberation']['cmis_folder'] = $acte['Deliberation']['num_delib'];

        return $acte;
    }

    /**
     * Récupération des informations de séance
     * @param type $seance_id
     * @return type
     */
    private function createActeFolderCmis(&$acte)
    {
        $this->ExportGedCMIS->getClient()->getFolderTree($this->ExportGedCMIS->getFolderId(), 1);

        CakeLog::info(
            __('[Ged][%s]name folder cmis: %s', __METHOD__, $acte['Deliberation']['cmis_folder']),
            'connector'
        );
        $this->ExportGedCMIS->deletetoGed(Configure::read('CMIS_REPO') . '/' . $acte['Deliberation']['cmis_folder']);

        // Création du répertoire de séance
        return $this->ExportGedCMIS->getClient()->createFolder(
            $this->ExportGedCMIS->getFolderId(),
            $acte['Deliberation']['cmis_folder']
        );
    }
}
