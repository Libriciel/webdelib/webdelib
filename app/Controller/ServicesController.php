<?php

/**
 * Contrôleur des Services
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       web-delib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 * @version     v4.3
 * @package     app.Controller
 */
class ServicesController extends AppController
{
    public $helpers = ['Tree'];
    public $uses = ['Service', 'ServiceUser', 'Cakeflow.Circuit', 'Aro', 'Aco', 'Permission'];
    public $components = [
        'Auth' => [
            'mapActions' => [
                'read' => ['admin_index', 'manager_index', 'isEditable', 'autoComplete'],
                'update' => ['admin_edit', 'manager_edit', 'admin_fusionner', 'manager_fusionner'],
                'delete' => ['admin_delete', 'manager_delete'],
                'create' => ['admin_add', 'manager_add'],
            ]
        ],
    ];

    /**
     * @access public
     */
    public function admin_index() // phpcs:ignore
    {
        $this->index();
    }

    /**
     * if is deletable
     * @param Array $services
     * @param Array $services
     *
     */
    private function isDeletable(&$services, $droits)
    {
        foreach ($services as &$service) {
            if (!empty($service['children'])) {
                $service['Service']['isDeletable'] = false;
                $this->isDeletable($service['children'], $droits);
                continue;
            }

            if (empty($droits) || in_array($service['Service']['id'], $droits['Service/delete'], true)) {
                $service['Service']['isDeletable'] = true;
            }
        }
    }

    /**
     * if is deletable
     * @param Array $services
     * @param Array $services
     *
     */
    private function isUpdatable(&$services, $droits)
    {
        foreach ($services as &$service) {
            if (!empty($service['children'])) {
                $this->isUpdatable($service['children'], $droits);
            }

            if (empty($droits) || in_array($service['Service']['id'], $droits['Service/update'], true)) {
                $service['Service']['isUpdatable'] = true;
            }
        }
    }

    /**
     * @version 4.3
     * @access public
     */
    public function admin_add() // phpcs:ignore
    {
        $this->add_edit(null, null);
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     */
    public function admin_edit($id = null) // phpcs:ignore
    {
        $this->add_edit($id, null);
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     */
    public function admin_delete($id = null) // phpcs:ignore
    {
        $this->delete($id);
    }

    /**
     * if is editable
     *
     * @access public
     * @param type $id
     * @return type
     */
    public function isEditable($id)
    {
        return $this->Service->hasAny(['parent_id' => $id]);
    }

    /**
     * @version 4.3
     * @access public
     */
    public function autoComplete()
    {
        $this->layout = 'ajax';
        $data = $this->Service->find(
            'all',
            [
                'conditions' => ['Service.name LIKE' => $this->params['url']['q'] . '%'],
                'fields' => ['name', 'id']
            ]
        );
        $this->set('data', $data);
    }

    /**
     * @access public
     */
    public function admin_fusionner() // phpcs:ignore
    {
        $this->fusionner();
    }

    /**
     * @access public
     */
    public function manager_index() // phpcs:ignore
    {
        $this->index(['Service.id' => 'read']);
        $this->render('admin_index');
    }

    public function manager_add() // phpcs:ignore
    {
        $this->add_edit(null, ['Service.id' => 'read']);
        $this->render('admin_add');
    }

    /**
     * @access public
     * @param type $id
     */
    public function manager_delete($id = null) // phpcs:ignore
    {
        $this->delete($id);
    }

    /**
     * @access public
     */
    public function manager_fusionner() // phpcs:ignore
    {
        $this->fusionner();
        $this->render('admin_fusionner');
    }

    /**
     * @access public
     * @param type $id
     */
    public function manager_edit($id = null) // phpcs:ignore
    {
        $this->add_edit($id, ['Service.id' => 'read']);
        $this->render('admin_edit');
    }

    /**
     * @access private
     */
    private function treeOfServices($allow = null)
    {
        return $this->Service->find(
            'threaded',
            [
                'fields' => ['id', 'name', 'parent_id'],
                'recursive' => -1,
                'order' => 'name ASC',
                'conditions' => ['actif' => 1],
                'allow' => $allow]
        );
    }

    /**
     * @version 4.3
     * @access private
     * @param type $allow
     */
    private function index($allow = null) // phpcs:ignore
    {
        $services = $this->treeOfServices($allow);
        // chercher les services fils de l'utilisateur
        $sousServices = $this->Service->find('all', [
                'fields' => ['id'],
                'order' => ['name' => 'ASC'],
                'conditions' => ['actif' => 1],
                'recursive' => -1,
                'allow' => ['Service.id']
            ]);
        $dependentServices = Hash::extract($sousServices, '{n}.Service.id');

        // tous les services
        $allServices = $this->Service->generateTreeList(['Service.actif' => 1], null, null, '&nbsp;&nbsp;&nbsp;&nbsp;');

        if (!empty($allow)) { // si on n'est pas administrateur
            // on élimine les services ne dépendant pas de cet utilisateur
            foreach ($allServices as $srv => $label) {
                if (!in_array($srv, $dependentServices)) {
                    unset($allServices[$srv]);
                }
            }
            $droits = [];
            $droits['Service/update'] = $this->Service->find('list', [
                'fields' => ['id'],
                'recursive' => -1,
                'allow' => ['Service.id' => 'update']
            ]);
            $droits['Service/delete'] = $this->Service->find('list', [
                'fields' => ['id'],
                'recursive' => -1,
                'allow' => ['Service.id' => 'delete']
            ]);
        }

        $this->isUpdatable($services, $droits);
        $this->isDeletable($services, $droits);

        $this->set('services', $services);
        $this->set('allServices', $allServices);
    }

    /**
     * @version 4.3
     * @access private
     * @param type $id
     * @return type
     */
    private function delete($id)
    {
        if (empty($id)) {
            $this->Flash->set(
                __('ID invalide pour le service'),
                ['element' => 'growl', 'params'=>['type' => 'danger']]
            );
            return $this->redirect($this->previous);
        }

        if ($this->Service->find('first', [
            'conditions' => [
                'parent_id' => $id,
                'actif' => 1],
            'recursive' => -1])
        ) {
            $this->Flash->set(
                __('Impossible de supprimer ce service : il possède au moins un fils'),
                ['element' => 'growl', 'params'=>['type' => 'warning']]
            );
            $this->redirect($this->previous);
        }

        $this->Service->id = $id;
        if (!$this->Service->saveField('actif', false)) {
            $this->Flash->set(
                __('Impossible de supprimer ce service'),
                ['element' => 'growl', 'params'=>['type' => 'danger']]
            );
        } else {
            $this->purgePermissionServiceNotExist();
            $this->Flash->set(
                __('Le service a été supprimé'),
                ['element' => 'growl', 'params'=>['type' => 'success']]
            );
        }

        $this->redirect($this->previous);
    }

    /**
     * @version 4.3
     * @access private
     */
    private function fusionner()
    {
        if (empty($this->data['service_a_fusionner'])
            or empty($this->data['Service']['id'])) {
            $this->Flash->set(
                __('ID invalide pour fusionner le service'),
                ['element' => 'growl', 'params'=>['type' => 'danger']]
            );

            $this->redirect($this->previous);
        }

        try {
            $this->ServiceUser->fusion($this->data['service_a_fusionner'], $this->data['Service']['id']);
            $this->Flash->set(__('Le service a été fusionné'), ['element' => 'growl', 'params'=>['type' => 'success']]);
        } catch (Exception $e) {
            $this->Flash->set($e->getMessage(), ['element' => 'growl', 'params'=>['type' => 'danger']]);
        }

        $this->redirect($this->previous);
    }

    /**
     * @version 4.3
     * @access private
     * @param type $id
     * @param type $allow
     * @return \view
     */
    private function add_edit($id = null, $allow = null) // phpcs:ignore
    {
        $permission = [];
        if ($this->request->is(['post', 'put'])) {
            if (empty($this->data['Service']['parent_id'])) {
                $this->request->data['Service']['parent_id'] = 0;
            }
            if (empty($this->data['Service']['circuit_defaut_id'])) {
                $this->request->data['Service']['circuit_defaut_id'] = 0;
            }

            if ($this->Service->save($this->data, ['request' => true])) {
                if (empty($id)) {
                    $id = $this->Service->getInsertID();
                    $this->addNewUserPermission($id);
                    $this->Flash->set(
                        __('Le service a été sauvegardé'),
                        ['element' => 'growl', 'params'=>['type' => 'success']]
                    );
                } else {
                    $this->Flash->set(
                        __('Le service a été modifié'),
                        ['element' => 'growl', 'params'=>['type' => 'success']]
                    );
                }

                $this->set('isEditable', $this->isEditable($id));
                $this->set('selectedService', $this->data['Service']['parent_id']);

                return $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Veuillez corriger les erreurs ci-dessous.'),
                    ['element' => 'growl', 'params'=>['type' => 'danger']]
                );
            }
        }

        if (!empty($id)) {
            $this->data = $this->Service->find('first', [
                'conditions'=> [
                    'Service.id' => $id
                ],
            ]);
            if (empty($this->data)) {
                $this->Flash->set(
                    __('ID invalide pour le service'),
                    ['element' => 'growl', 'params'=>['type' => 'warning']]
                );
                $this->redirect($this->previous);
            }
            $conditions = ['Service.id <>' => $id, 'Service.actif' => 1];
        } else {
            $conditions = ['Service.actif' => 1];
        }

        // chercher les services fils de l'utilisateur
        $sousServices = $this->Service->find(
            'all',
            [
                'fields' => ['id'],
                'recursive' => -1,
                'order' => 'name ASC',
                'conditions' => [
                    'actif' => 1
                ],
                'allow' => ['Service.id' => 'read']
            ]
        );
        $dependentServices = Hash::extract($sousServices, '{n}.Service.id');

        // tous les services
        $allServices = $this->Service->generateTreeList($conditions, null, null, '&nbsp;&nbsp;&nbsp;&nbsp;');

        if (!empty($allow)) { // si on n'est pas administrateur
            // on élimine les services ne dépendant pas de cet utilisateur
            foreach ($allServices as $srv => $label) {
                if (!in_array($srv, $dependentServices)) {
                    unset($allServices[$srv]);
                }
            }
        }
        $this->set('services', $allServices);
        $this->set('circuits', $this->Circuit->find('list'));
    }

    private function addNewUserPermission($id)
    {
        // Mettre les droits pour les services qui lui sont affectés
        //Ajout des droits du profil
        $this->Aro->recursive = -1;
        $aro = $this->Aro->findByForeignKeyAndModel(
            $this->Auth->user('id'),
            'User',
            ['id']
        );

        $this->Aco->recursive = -1;
        $aco = $this->Aco->findByForeignKeyAndModel(
            $id,
            'Service',
            ['id']
        );

        $this->Permission->clear();
        $this->Permission->create();
        $permission['Permission']['aro_id'] = $aro['Aro']['id'];
        $permission['Permission']['aco_id'] = $aco['Aco']['id'];
        foreach ($this->Permission->getAcoKeys($this->Permission->schema()) as $permKeys) {
            $permission['Permission'][$permKeys] = 1;
        }
        $this->Permission->save($permission, false);


        // Purge des permissions dont les services n'existent plus
        $this->purgePermissionServiceNotExist();
    }

    private function getServiceAcos()
    {
        $this->Aco->Behaviors->attach('Database.DatabaseTable');
         return $this->Aco->find('all', [
            'fields' => ['id'],
            'joins' => [
                [
                    'table' => 'services',
                    'alias' => 'Service',
                    'type' => 'inner',
                    'conditions' => [
                        'Aco.foreign_key = Service.id'
                    ]
                ],
                [
                    'table' => 'services_users',
                    'alias' => 'ServiceUser',
                    'type' => 'inner',
                    'conditions' => [
                        'Service.id = ServiceUser.service_id'
                    ]
                ],
            ],
            'conditions' => [
                'Aco.model' => 'Service',
            ],
            'order' => ['Aco.id' => 'ASC'],
            'recursive' => -1]);
    }

    private function purgePermissionServiceNotExist()
    {
        $acos = $this->Aco->find('all', [
            'fields' => ['id'],
            'conditions' => [
                'Aco.model' => 'Service',
            ],
            'order' => ['Aco.id' => 'ASC'],
            'recursive' => -1]);

        $acoIds = Hash::extract($acos, '{n}.Aco.id');

        $serviceAcos = $this->getServiceAcos();
        $serviceIds = Hash::extract($serviceAcos, '{n}.Aco.id');

        $acoIds = array_diff($acoIds, $serviceIds);
        if (!empty($acoIds)) {
            $this->Permission->deleteAll([
                'aco_id IN' => $acoIds,
                'aro_id' => $aroUser['Aro']['id']
            ]);
        }
    }
}
