<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Inflector', 'Utility');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CakeTime', 'Utility');
App::uses('AppTools', 'Lib');

class SeancesController extends AppController
{
    /**
     * [public description]
     * @var [type]
     */
    public $name = 'Seances';
    /**
     * [public description]
     * @var [type]
     */
    public $helpers = ['TinyMCE'];
    /**
     * [public description]
     * @var [type]
     */
    public $uses = [
      'Deliberation',
      'Commentaire',
      'Cakeflow.Traitement',
      'DeliberationSeance',
      'DeliberationTypeseance',
      'Seance',
      'Acteurseance',
      'Historique',
      'User',
      'Collectivite',
      'Listepresence',
      'Vote',
      'ModelOdtValidator.Modeltemplate',
      'Annexe',
      'Typeseance',
      'Acteur',
      'Infosupdef',
      'Infosup',
      'Typeseance',
      'Cakeflow.Traitement',
    ];
    /**
     * [public description]
     * @var [type]
     */
    public $components = [
        'RequestHandler',
        'SeanceTools',
        'Email',
        'Conversion',
        'Progress',
        'MailSec',
        'ModelOdtValidator.Fido',
        'SabreDav',
        'Auth' => [
            'mapActions' => [
                'create' => ['add'],
                'read' => [
                    'index',
                    'genererConvoc',
                    'multiodj',
                    'getListActeurs',
                    'genereFusionToFiles',
                    'genereFusionMultiSeancesToClient',
                    'downloadZip',
                    'genereFusionToClient',
                    'download',
                    'downloadAttachedFileConvocation',
                    'downloadAttachedFileOrdredujour'
                ],
                'calendrier',
                'ExportGedSeances',
                'update' => [
                  'edit',
                  'clore',
                  'saveProcesVerbaux',
                  'sendOrdredujour',
                  'positionner',
                  'reportePositionsSeanceDeliberante',
                  'changePosition',
                  'changeRapporteur',
                  'saisirSecretaire',
                  'details',
                  'sortby',
                  'schedule',
                  'sendConvocations',
                  'saisirDebatGlobal',
                  'deleteDebatGlobal',
                  'deleteSendMail'
                ],
                'delete' => ['delete']
                ],
        ]];
    /**
     * [public description]
     * @var [type]
     */
    public $cacheAction = 0;

    /**
     * @version 4.3
     * @access public
     * @param type $timestamp
     */
    public function add($timestamp = null)
    {
        // initialisation
        $success = false;
        $dateSeance = '';

        if (empty($this->data)) {
            if (isset($timestamp)) {
                $dateSeance = date('d/m/Y H:i', $timestamp);
            }
        } else {
            $this->Seance->begin();
            $dateSeance = $this->data['Seance']['date'];
            $this->request->data['Seance']['date'] = CakeTime::format(
                str_replace('/', '-', $dateSeance) . ':00',
                '%Y-%m-%d %H:%M:00'
            );

            if ($success = $this->Seance->save($this->request->data)) {
                //Récupération de l'identifiant du nouveau projet
                $this->request->data['Seance']['id'] = $this->Seance->getLastInsertId();
                // sauvegarde des informations supplémentaires
                $success = $this->Infosup->saveInfosup($this->data, 'Seance', true);
                $this->Infosup->invalidFields();

                //FIXME :  Save has many
                if (!empty($this->data['Seance']['SeanceChildren'])) {
                    foreach ($this->data['Seance']['SeanceChildren'] as $seanceChildren) {
                        $this->Seance->SeanceChildren->id = $seanceChildren;
                        $success &= $this->Seance->SeanceChildren->save(['parent_id' =>$this->Seance->id], false);
                        $this->Seance->SeanceChildren->clear();
                    }
                }
            }

            if (!$success) {
                $this->Flash->set(
                    __('Corrigez les erreurs ci-dessous.'),
                    ['element' => 'growl','params'=>['type' => 'danger']]
                );
                $this->Seance->rollback();
            }
        }

        if ($success) {
            $this->Seance->commit();
            $this->Flash->set(__('La séance a été créée'), ['element' => 'growl']);
            $this->redirect($this->previous);
        } else {
            $this->set('date', $dateSeance);
            $this->set('typeseances', $this->Typeseance->find('list', [
              'allow'=> ['Typeseance.id' => 'create'],
              'order' => ['Typeseance.libelle' => 'ASC'],
              'recursive' => -1,
            ]));

            $seances_non_traitee = $this->Seance->find('all', [
              'fields' => [
                'id',
                'Typeseance.parent_id',
                'Typeseance.libelle',
                'date'
              ],
              'joins' => [
                $this->Seance->join('Typeseance', [
                        'type' => 'INNER', 'conditions' => ['Typeseance.parent_id IS NOT NULL']
                    ])
              ],
              'conditions' => [
                'Seance.parent_id IS NULL',
                'traitee' => 0,
                'Seance.id NOT' => $this->Seance->getSeancesDeliberantes()],
              'allow' => ['typeseance_id'],
              'order' => ['Seance.date' => 'ASC'],
              'recursive' => -1,
            ]);

            $seances = [];
            foreach ($seances_non_traitee as $seance) {
                $seances[$seance['Seance']['id']] = __(
                    '%s le %s à %s',
                    $seance['Typeseance']['libelle'],
                    CakeTime::i18nFormat($seance['Seance']['date'], '%A %e %B %Y'),
                    CakeTime::i18nFormat($seance['Seance']['date'], '%k:%M')
                );
            }
            $this->set('seance', null);
            $this->set('seances', $seances);
            $this->set('infosupdefs', $this->Infosupdef->find('all', [
                    'contain' => ['Profil.id'],
                    'conditions' => ['model' => 'Seance', 'actif' => true],
                    'order' => 'ordre',
                    'recursive' => -1,
            ]));
            $this->set('infosuplistedefs', $this->Infosupdef->generateListes('Seance'));
            $this->request->data['Infosup'] = $this->Infosupdef->valeursInitiales('Seance');
            $this->render('edit');
        }
    }

    /**
     * Modifier une séance
     *
     * @access public
     *
     * @param type $id
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function edit($id)
    {
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($id, 'update');

        if ($this->request->is('put')) {
            $success = true;
            $this->Seance->begin();
            $this->request->data['Seance']['date'] = CakeTime::format(
                str_replace('/', '-', $this->data['Seance']['date']) . ':00',
                '%Y-%m-%d %H:%M:00'
            );

            $success &= $this->Seance->save($this->data);

            if ($success) {
                // sauvegarde des informations supplémentaires //fix sauvegarde informations supplémentaire
                $infossupDefs = $this->Infosupdef->find('all', [
                    'recursive' => -1,
                    'fields' => ['id', 'code'],
                    'conditions' => [
                        'type' => 'odtFile',
                        'model' => 'Seance',
                        'actif' => true
                    ]]);
                foreach ($infossupDefs as $infossupDef) {
                    $infosup = $this->Infosup->find('first', [
                        'recursive' => -1,
                        'fields' => ['id', 'file_name', 'file_type','model','foreign_key'],
                        'conditions' => [
                            'foreign_key' => $id,
                            'model' => 'Seance',
                            'infosupdef_id' => $infossupDef['Infosupdef']['id']]
                        ]);
                    if (empty($infosup) || empty($infosup['Infosup']['file_name'])) {
                        continue;
                    }
                    if (!empty($this->data['Infosup'][$infossupDef['Infosupdef']['code']]['file_infosup'])) {
                        $file = $this->SabreDav->getFileDav(
                            $this->data['Infosup'][$infossupDef['Infosupdef']['code']]['file_infosup']
                        );
                        $infosup['Infosup']['content'] = $file->read();
                        $infosup['Infosup']['file_size'] = $file->size();
                        $this->Infosup->save($infosup);
                    } else {
                        $this->request->data['Infosup'][$infossupDef['Infosupdef']['code']]['delete'] = true;
                    }
                }
                if (array_key_exists('Infosup', $this->data)) {
                    $success = $this->Infosup->saveInfosup($this->request->data, 'Seance');
                }
                $seanceChildrens = $this->Seance->findAllByParentId($id, ['id']);
                foreach ($seanceChildrens as $seanceChildren) {
                    $this->Seance->SeanceChildren->id = $seanceChildren['Seance']['id'];
                    $success &= $this->Seance->SeanceChildren->save(['parent_id' => null], false);
                    $this->Seance->SeanceChildren->clear();
                }
                if (!empty($this->data['Seance']['SeanceChildren'])) {
                    foreach ($this->data['Seance']['SeanceChildren'] as $seanceChildren) {
                        $this->Seance->SeanceChildren->id = $seanceChildren;
                        $success &= $this->Seance->SeanceChildren->save(['parent_id' => $id], false);
                        $this->Seance->SeanceChildren->clear();
                    }
                }
            }

            if ($success) {
                $this->Seance->commit();
                $this->Flash->set(__('La séance a été sauvegardée'), ['element' => 'growl']);
                $this->redirect($this->previous);
            } else {
                $this->Seance->rollback();
                $msg_error = '';
                $infosupErrors = $this->Infosup->invalidFields();
                $msg_error = __('Corrigez les erreurs ci-dessous. \n');
                if (!empty($infosupErrors)) {
                    foreach ($infosupErrors as $infosupErrors) {
                        $msg_error .= 'Information supplémentaire : \n ';
                        foreach ($infosupErrors as $infosupCode => $infosupError) {
                            $msg_error .= '- ['.$this->Infosupdef->getLabel('Seance', $infosupCode).'] '
                                . $infosupError . '\n ';
                        }
                    }
                    $this->Flash->set(
                        $msg_error
                        . ' \n '
                        . __('Attention, les modifications ou ajouts des fichiers n\'ont pas été enregistrées'),
                        ['element' => 'growl','params'=>['type' => 'danger']]
                    );
                }
            }
        }

        if (!$this->request->data) {
            $this->request->data = $this->Seance->find('first', [
                'contain' => ['Infosup'],
                'conditions' => ['Seance.id' => $id]
            ]);

            if (empty($this->data)) {
                $this->Flash->set(
                    __('ID invalide pour la séance'),
                    ['element' => 'growl','params'=>['type' => 'danger']]
                );
                $this->redirect($this->previous);
            }
            if (!empty($this->data)) {
                $this->SabreDav->start();
                // création des fichiers des infosup de type odtFile
                foreach ($this->request->data['Infosup'] as $infosup_key => $infosup) {
                    $infoSupDef = $this->Infosupdef->find('first', [
                        'recursive' => -1,
                        'fields' => ['type'],
                        'conditions' => ['id' => $infosup['infosupdef_id'], 'model' => 'Seance', 'actif' => true]]);

                    if (!empty($infoSupDef['Infosupdef']['type'])
                        && $infoSupDef['Infosupdef']['type'] == 'odtFile'
                        && !empty($infosup['file_name'])
                        && !empty($infosup['content'])
                    ) {
                        $this->request->data['Infosup'][$infosup_key]['file_infosup'] =
                            $this->SabreDav->newFileDav($infosup['file_name'], $infosup['content']);
                        $this->request->data['Infosup'][$infosup_key]['file_infosup_url'] =
                            $this->SabreDav->newFileDavUrl();
                    }
                }

                $this->request->data['Infosup'] = $this->Seance->Infosup->compacte($this->request->data['Infosup']);
            }
        }

        $this->set('date', CakeTime::format($this->data['Seance']['date'], '%d/%m/%Y %H:%M'));
        $this->set('typeseances', $this->Typeseance->find('list'));

        $this->Seance->recursive = -1;
        $seance_type_id = $this->Seance->findById($id, ['Seance.type_id']);
        $seances_non_traitee = $this->Seance->find('all', [
          'fields' => [
            'id',
            'Typeseance.parent_id',
            'Typeseance.libelle',
            'date'
          ],
          'joins' => [
            $this->Seance->join('Typeseance', [
                'type' => 'INNER', 'conditions' => ['Typeseance.parent_id' => $seance_type_id['Seance']['type_id']]
                ])
          ],
          'conditions' => [
            'OR' => ['Seance.parent_id IS NULL', 'Seance.parent_id' => $id],
            'traitee' => 0,
            'Seance.id NOT' => $this->Seance->getSeancesDeliberantes()],
          'allow' => ['typeseance_id'],
          'order' => ['Seance.date' => 'ASC'],
          'recursive' => -1,
        ]);

        $seances = [];
        foreach ($seances_non_traitee as $seance) {
            $seances[$seance['Seance']['id']] = __(
                '%s du %s à %s',
                $seance['Typeseance']['libelle'],
                CakeTime::i18nFormat($seance['Seance']['date'], '%A %e %B %Y'),
                CakeTime::i18nFormat($seance['Seance']['date'], '%k:%M')
            );
        }
        $selectedSeances = $this->Seance->SeanceChildren->find('threaded', [
          'fields' => ['id'],
          'conditions' => ['parent_id' => $id],
          'recursive' => -1
        ]);

        $this->set('selectedSeances', Set::extract('/SeanceChildren/id', $selectedSeances));
        $this->set('seances', $seances);

        $this->set('infosuplistedefs', $this->Infosupdef->generateListes('Seance'));
        $this->set('infosupdefs', $this->Infosupdef->find('all', [
                'recursive' => -1,
                'conditions' => [
                  'model' => 'Seance',
                  'actif' => true],
                'order' => 'ordre',
                'contain' => ['Profil.id']]));
    }

    /**
     * Supression d'une séance
     *
     * @access public
     *
     * @param type $id
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function delete($id)
    {
        if (!$id) {
            $this->Flash->set(
                __('ID invalide pour la séance'),
                ['element' => 'growl','params'=>['type' => 'danger']]
            );
            $this->redirect($this->previous);
        }

        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($id, 'delete');

        $delibs = $this->Seance->getDeliberationsId($id);

        if (count($delibs) != 0) {
            $this->Flash->set(
                __('Cette séance contient des actes. Vous ne pouvez pas la supprimer.'),
                ['element' => 'growl','params'=>['type' => 'danger']]
            );
            $this->redirect($this->previous);
        }

        if ($this->Seance->removeFromTree($id, true)) {
            $this->Flash->set(
                __('La séance a été supprimée'),
                ['element' => 'growl','params'=> ['type' => 'success']]
            );
            $this->redirect($this->previous);
        } else {
            $this->Flash->set(
                __('ID invalide pour la séance'),
                ['element' => 'growl','params'=>['type' => 'danger']]
            );
            $this->redirect($this->previous);
        }
    }

    /**
     * Liste des séances
     *
     * @access public
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function index()
    {
        if (isset($this->params['render']) && ($this->params['render'] == 'bannette')) {
            $limit = Configure::read('LIMIT');
        } else {
            $limit = null;
        }
        $this->set('AFFICHE_CONVOCS_ANONYME', Configure::read('AFFICHE_CONVOCS_ANONYME'));
        $this->set('use_pastell', Configure::read('USE_PASTELL'));
        $canSign = $this->Acl->check(
            ['User' => ['id' => $this->Auth->user('id')]],
            'Signatures/sendDeliberationsToSignature',
            'read'
        );
        $canSendSeanceVotesToIdelibre = $this->Acl->check(
            ['User' => ['id' => $this->Auth->user('id')]],
            'Votes/sendSeanceVotesToIdelibre',
            'read'
        );
        $canSendSeanceAvisToIdelibre = $this->Acl->check(
            ['User' => ['id' => $this->Auth->user('id')]],
            'Votes/sendSeanceAvisToIdelibre',
            'read'
        );
        $canSendToGed = (Configure::read('USE_GED')
            && Configure::read('GED') =='CMIS'
            && Configure::read('GED_XML_VERSION') == 4) ? $this->Acl->check(
                ['User' => ['id' => $this->Auth->user('id')]],
                'ExportGedSeances',
                'read'
            ) : false;
        
        $canDownloadVotesElectroniqueJson = ($this->Acl->check(
            ['User' => ['id' => $this->Auth->user('id')]],
            'downloadVotesElectroniqueJson',
            'read'
        )
        ) ? 'downloadVotesElectroniqueJson' : false;

        $this->set('models', $this->Modeltemplate->find('list', [
                'fields' => ['name'],
                'recursive' => -1,
                'conditions' => ['modeltype_id' => [MODELE_TYPE_MULTISEANCE]],
        ]));

        //Choix du rendu à appliquer
        $rendu = (isset($this->params['endDiv'])) ? $this->params['endDiv'] : false;
        $this->set('endDiv', $rendu);

        if (empty($this->data)) {
            $seances = $this->Seance->find('all', [
                'fields' => ['id', 'Seance.date', 'type_id', 'idelibre_id', 'numero_depot', 'parent_id'],
                'contain' => [
                    'Typeseance'=>
                    [
                        'fields'=> [
                            'libelle',
                            'color',
                            'action',
                            'modele_convocation_id',
                            'modele_ordredujour_id',
                            'modele_pvsommaire_id',
                            'modele_pvdetaille_id',
                            'modele_journal_seance_id'
                        ],
                        'Modele_pvdetaille.name',
                        'Modele_pvsommaire.name',
                        'Modele_ordredujour.name',
                        'Modele_convocation.name',
                    ],
                    'SeanceChildren' => [
                      'fields'=> ['date'],
                      'Typeseance' => ['fields'=> ['libelle']]
                    ],
                    'SeanceParent' => [
                      'fields'=> ['date'],
                      'Typeseance' => ['fields'=> ['libelle']]
                    ],
                    'DeliberationSeance' => [
                        'fields' => ['deliberation_id']
                    ],
                ],
                'recursive' => -1,
                'conditions' => ['Seance.traitee' => 0],
                'limit' => $limit,
                'order' => ['Seance.date' => 'ASC'],
                'allow' => ['typeseance_id']
            ]);

            //Droit sur les types de projet
            $types_seances_actions = [];
            $types_seances_actions['Typeseance/delete'] = $this->Typeseance->find('list', [
                'fields' => ['id'],
                'recursive' => -1,
                'allow' => ['Typeseance.id' => 'delete']
            ]);
            $types_seances_actions['Typeseance/update'] = $this->Typeseance->find('list', [
                'fields' => ['id'],
                'recursive' => -1,
                'allow' => ['Typeseance.id' => 'update']
            ]);

            for ($i = 0; $i < count($seances); $i++) {
                $seances[$i]['Seance']['dateEn'] = $seances[$i]['Seance']['date'];
                $seances[$i]['Actions']=[];
                if ($seances[$i]['Typeseance']['action'] === 0 && $canSign) {
                    array_push($seances[$i]['Actions'], 'sendDeliberationsToSignature');
                }
                if (!empty($seances[$i]['Typeseance']['modele_journal_seance_id'])) {
                    array_push($seances[$i]['Actions'], 'generer_journal');
                }
                if (in_array(
                    $seances[$i]['Typeseance']['id'],
                    $types_seances_actions['Typeseance/update'],
                    true
                )
                ) {
                    array_push($seances[$i]['Actions'], 'Typeseance/update');
                }
                if (in_array(
                    $seances[$i]['Typeseance']['id'],
                    $types_seances_actions['Typeseance/delete'],
                    true
                )) {
                    array_push($seances[$i]['Actions'], 'Typeseance/delete');
                }
            }
            $actions = [];

            if ($canSendToGed) {
                array_push($actions, 'ExportGedSeances/read');
            }
            if ($canSendSeanceVotesToIdelibre) {
                array_push($actions, 'sendToIdelibre/read');
            }
            if ($canSendSeanceAvisToIdelibre) {
                array_push($actions, 'sendToIdelibre/read');
            }
            if ($canDownloadVotesElectroniqueJson) {
                array_push($actions, 'downloadVotesElectroniqueJson');
            }
            $this->set('actions', $actions);
            $this->set('seances', $seances);
        }
    }

    /**
     * Affichage du calendrier des séances
     *
     * @access public
     *
     * @param type $annee
     *
     * @version 4.3.0
     * @since 1.0.0
     */
    public function calendrier($annee = null)
    {
        // initialisations
        $seances_calendrier = [];
        $year = empty($annee) ? date('Y') : $annee;

        //lecture des séances non traitées en DB
        $seances = $this->Seance->find('all', [
            'fields' => ['Seance.id', 'Seance.date', 'Seance.type_id'],
            'contain' => ['Typeseance.libelle'],
            'conditions' => ['Seance.traitee' => 0],
            'order' => 'date ASC',
            'recursive' => -1,
            'allow' => ['Typeseance_id']
        ]);
        foreach ($seances as $seance) {
            $timestamp = CakeTime::fromString($seance['Seance']['date']);
            $seances_calendrier[] = [
                'id' => $seance['Seance']['id'],
                'libelle' => $seance['Typeseance']['libelle'],
                'timestamp' => $timestamp,
                'start' => $timestamp ,
                'end' => $timestamp + 7200 //2 heures de séance par défaut
            ];
        }

        $this->set('seances', $seances_calendrier);
        $this->set('annee', $year);
    }

    /**
     * Tri de l'ordre du jour
     *
     * @access public
     *
     * @param type $seance_id
     * @param type $sortby
     * @return type
     *
     * @version 5.1.0
     * @since 1.0.0
     */
    public function sortby($id, $sortby)
    {
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($id, 'update');

        $this->DeliberationSeance->ordonneSeanceByValue($id, $sortby);

        return $this->redirect($this->previous);
    }

    /**
     * Ordre du jour de la séance
     *
     * @access public
     *
     * @param  int $id     [description]
     * @param  string $sortby [description]
     *
     * @version 5.1.0
     * @since 1.0.0
     */
    public function schedule($id = null, $sortby = null)
    {
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($id, 'update');

        $this->set('lastPosition', $this->Seance->getLastPosition($id) - 1);

        // Critere de tri
        switch ($sortby) {
            case 'theme_id':
                $sortby_champ = 'Theme.order';
                break;
            case 'service_id':
                $sortby_champ = 'Service.order';
                break;
            case 'rapporteur_id':
                $sortby_champ = 'Rapporteur.nom';
                break;
            case 'titre':
                $sortby_champ = 'Deliberation.titre';
                break;
            default:
                $sortby_champ = 'DeliberationSeance.position';
                break;
        }

        $fields = [
            'Deliberation.id',
            'Deliberation.objet',
            'Deliberation.objet_delib',
            'Deliberation.etat',
            'Deliberation.signee',
            'Deliberation.titre',
            'Deliberation.date_limite',
            'Deliberation.anterieure_id',
            'Deliberation.num_pref',
            'Deliberation.rapporteur_id',
            'Deliberation.redacteur_id',
            'Deliberation.circuit_id',
            'Deliberation.typeacte_id',
            'Deliberation.theme_id',
            'Deliberation.date_acte',
            'Deliberation.tdt_ar_date',
            'Deliberation.service_id',
            'Deliberation.num_delib',
            'Deliberation.parapheur_etat',
            'Theme.libelle',
            'Theme.order',
            'DeliberationSeance.position'
        ];
        $projets = $this->Deliberation->find('all', [
            'fields' => $fields,
            'contain' => [
                'Theme' => ['fields' => ['order', 'libelle']],
                'User' => ['fields' => ['id']],
                'Annexe' => ['fields' => ['id', 'filename'], 'order' => ['position']],
                'Service' => ['fields' => ['id', 'order', 'name']],
                'Typeacte' => [
                    'fields' => ['name'],
                    'Nature' => ['fields' => ['code']]
                ],
                'Circuit' => ['fields' => ['nom']],
                'DeliberationTypeseance' => [
                    'fields' => ['id'],
                    'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]
                ],
                'DeliberationSeance' => [
                    'fields' => ['id'],
                    'Seance' => [
                        'fields' => ['id', 'date', 'type_id'],
                        'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]
                    ]
                ],
            ],
            'joins' => [$this->Deliberation->join('DeliberationSeance', ['type' => 'INNER'])],
            'conditions' => ['DeliberationSeance.seance_id' => $id, 'Deliberation.etat >=' => 0],
            'recursive' => -1,
            'order' => ["$sortby_champ"=>'ASC']
          ]);

        $this->set('seance_id', $id);
        $this->set('rapporteurs', $this->Acteur->generateListElus('Acteur.nom'));
        $this->set('date_seance', $this->Seance->getDate($id));

        $aPosition=[];
        foreach ($projets as $projet) {
            $aPosition[$projet['DeliberationSeance']['position']] = $projet['DeliberationSeance']['position'];
        }

        $this->set('aPosition', $aPosition);
        $this->set('is_deletable', true);
        $this->set('is_deliberante', $this->Seance->isDeliberante($id));
        $this->afficheProjets('schedule', $projets, null);
    }

    /**
     * Reporter les positions de la séance délibérante vers les commissions
     *
     * @access public
     *
     * @param type $seance_id
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function reportePositionsSeanceDeliberante($id)
    {
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($id, 'update');

        if ($this->Seance->DeliberationSeance->reportePositionsSeanceDeliberante($id)) {
            $this->Flash->set(__('Report effectué.'), ['element' => 'growl']);
        } else {
            $this->Flash->set(
                __('Report non effectué.'),
                ['element' => 'growl','params'=>['type' => 'danger']]
            );
        }

        $this->redirect('/seances/schedule/' . $id);
    }

    /**
     * Changer le rapporteur du projet
     *
     * @access public
     *
     * @param type $seance_id
     * @param type $newRapporteur
     * @param type $delib_id
     *
     * @version 5.1.0
     * @since 4.0.0
     */
    public function changeRapporteur($id, $delib_id, $newRapporteur = null)
    {
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($id, 'update');

        $this->Deliberation->id = $delib_id;
        if ($this->Deliberation->saveField('rapporteur_id', $newRapporteur)) {
            if (empty($newRapporteur)) {
                $this->Flash->set(__('Rapporteur supprimé.'), ['element' => 'growl']);
            } else {
                $this->Flash->set(__('Rapporteur modifié.'), ['element' => 'growl']);
            }
            $this->redirect(['action' => 'schedule', $id]);
        }
    }

    /**
     * @since 4.3
     * @access public
     * @param type $id
     * @param type $addFile
     * @return type
     */
    public function saisirDebatGlobal($id = null, $addFile = null)
    {
        $this->set('seance_id', $id);

        if ($this->request->isGet() && !empty($addFile) && $addFile == true) {
            $file = new File(APP . 'Config' . DS . 'OdtVide.odt', false);
            $this->request->data['Seance']['id'] = $id;
            $this->request->data['Seance']['texte_doc']['tmp_name'] = $file->pwd();
            $this->request->data['Seance']['texte_doc']['name'] = 'debat_global.odt';
            $this->request->data['Seance']['texte_doc']['size'] = $file->size();
            $this->request->data['Seance']['texte_doc']['type'] = $file->mime();
            $this->request->data['Seance']['texte_doc']['error'] = false;
            if ($this->Seance->saveDebatGen($this->data)) {
                $this->Flash->set(__('Débat global ajouté'), ['element' => 'growl']);
                return $this->redirect($this->previous);
            }

            $this->Flash->set(
                __('Problème lors de la création du document'),
                ['element' => 'growl','params'=>['type' => 'danger']]
            );

            return $this->redirect($this->previous);
        }
        if ($this->request->isPost()) {
            $this->request->data['Seance']['id'] = $id;
            if (!empty($this->data['Seance']['file_debat'])) {
                $file = $this->SabreDav->getFileDav($this->data['Seance']['file_debat']);
                $this->request->data['Seance']['debat_global'] = $file->read();
                $this->request->data['Seance']['debat_global_size'] = $file->size();
            } else {
                $this->request->data['Seance']['debat_global'] = '';
                $this->request->data['Seance']['debat_global_size'] = 0;
            }
            if ($this->Seance->saveDebatGen($this->data)) {
                $this->Flash->set(__('Débat global enregistré'), ['element' => 'growl']);
                return $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Veuillez corriger les erreurs ci-dessous'),
                    ['element' => 'growl','params'=>['type' => 'danger']]
                );
            }
        }

        $seance = $this->Seance->find('first', [
            'conditions' => ['Seance.id' => $id],
            'contain' => 'Typeseance.libelle',
            'recursive' => -1
        ]);

        $this->request->data = $seance;

        if (!empty($seance['Seance']['debat_global'])) {
            $this->SabreDav->start();
            $this->set('file_debat', $this->SabreDav->newFileDav(
                'DebatGlobal_' . $id . '.odt',
                $seance['Seance']['debat_global']
            ));
            $this->set('file_debat_url', $this->SabreDav->newFileDavUrl());
        }
    }

    /**
     * Suppression du débat global
     *
     * @access public
     *
     * @param type $id
     * @return type
     *
     * @version 5.1.0
     * @version 4.2.0
     */
    public function deleteDebatGlobal($id)
    {
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($id, 'delete');
        $this->Seance->id = $id;
        $data = [
            'debat_global' => '',
            'debat_global_name' => '',
            'debat_global_size' => 0,
            'debat_global_type' => ''
        ];

        if ($this->Seance->save($data, false)) {
            $this->Flash->set(__('Débat supprimé !'), ['element' => 'growl']);
            $this->redirect([
                    'admin' => false,
                    'prefix' => null,
                    'controller' => 'seances',
                    'action' => 'saisirDebatGlobal', $id]);
        } else {
            $this->Flash->set(
                __("Problème survenu lors de la suppression des débats généraux"),
                ['element' => 'growl','params'=>['type' => 'danger']]
            );
            $this->redirect($this->here);
        }
    }

    /**
     * Saisir le secrétaire de séance
     *
     * @access public
     *
     * @param type $seance_id
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function saisirSecretaire($id)
    {
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($id, 'update');

        $this->set('seance_id', $id);
        $seance = $this->Seance->find('first', [
            'conditions' => ['Seance.id' => $id],
            'recursive' => -1,
            'fields' => ['id', 'type_id', 'president_id', 'secretaire_id']]);
        //Récupération des acteurs convoqué pour la séance
        $acteursConvoques = $this->Seance->Typeseance->acteursConvoquesParTypeSeanceId(
            $seance['Seance']['type_id'],
            null,
            ['id', 'nom', 'prenom']
        );
        if (empty($acteursConvoques)) {
            $this->Flash->set(__('Aucun acteur convoqué.'), ['element' => 'growl','params'=>['type' => 'danger']]);
            $this->redirect($this->previous);
        }
        $tab = [];
        foreach ($acteursConvoques as $acteurConvoque) {
            $tab[$acteurConvoque['Acteur']['id']] =
                $acteurConvoque['Acteur']['prenom'] . ' ' . $acteurConvoque['Acteur']['nom'];
        }
        $this->set('acteurs', $tab);

        if (empty($this->data)) {
            $this->set('selectedPresident', $seance['Seance']['president_id']);
            $this->set('selectedActeurs', $seance['Seance']['secretaire_id']);
        } else {
            $this->Seance->id = $id;
            $this->Seance->saveField('president_id', $this->data['Acteur']['president_id']);
            if ($this->Seance->saveField('secretaire_id', $this->data['Acteur']['secretaire_id'])) {
                $this->redirect($this->previous);
            }
        }
    }

    /**
     * @version 4.3
     * @access public
     * @param type $seance_id
     * @param type $choixListe
     * @return type
     */
    public function getListActeurs($seance_id, $choixListe = 1)
    {
        $presents = [];
        $absents = [];
        $mandats = [];
        $mouvements = [];
        $tab = [];

        $delibs = $this->Deliberation->findAll("Deliberation.seance_id = $seance_id");
        $nb_delib = count($delibs);
        foreach ($delibs as $delib) {
            array_push($tab, $delib['Deliberation']['id']);
        }

        $conditions = "Listepresence.delib_id=";
        $conditions .= implode(" OR Listepresence.delib_id=", $tab);
        $presences = $this->Listepresence->findAll($conditions, null, 'Acteur.position');
        foreach ($presences as $presence) {
            $acteur_id = $presence['Listepresence']['acteur_id'];
            $tot_presents = $this->Listepresence->findAll(
                "Listepresence.acteur_id =  $acteur_id AND ($conditions) AND Listepresence.present=1"
            );
            $nb_presence = count($tot_presents);

            if ($nb_presence == $nb_delib) {
                array_push($presents, $acteur_id);
            } elseif ($nb_presence == 0) {
                $tmp = $this->Listepresence->findAll(
                    "Listepresence.acteur_id =  $acteur_id AND ($conditions) AND"
                    ." Listepresence.present=0 AND Listepresence.mandataire=0"
                );
                $nb_absence = count($tmp);
                if ($nb_absence == $nb_delib) {
                    array_push($absents, $acteur_id);
                } else {
                    $tmp2 = $this->Listepresence->findAll(
                        "Listepresence.acteur_id =  $acteur_id AND ($conditions) AND"
                        ." Listepresence.present=0 AND Listepresence.mandataire!=0"
                    );
                    foreach ($tmp2 as $mandat) {
                        if (!isset($mandat['Listepresence']['acteur_id'])) {
                            $mandat['Listepresence']['acteur_id'] = [];
                        }
                        $mandats[$mandat['Listepresence']['acteur_id']] = $mandat['Listepresence']['mandataire'];
                    }
                }
            } else {
                foreach ($tot_presents as $pres) {
                    if (!isset($mouvements[$acteur_id])) {
                        $mouvements[$acteur_id] = [];
                    }
                    $mouvements[$acteur_id] = $pres['Listepresence']['delib_id'];
                }
            }
        }

        if ($choixListe == 1) {
            return(array_unique($presents));
        } elseif ($choixListe == 2) {
            return(array_unique($absents));
        } elseif ($choixListe == 3) {
            return(array_unique($mandats));
        } elseif ($choixListe == 4) {
            return(array_unique($mouvements));
        }
    }

    /**
     * @access public
     *
     * @param int $id
     * @param string $file
     */
    public function download($id, $file)
    {
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($id, 'read');

        $this->autoRender = false;
        $fileType = $file . '_type';
        $fileSize = $file . '_size';
        $fileName = $file . '_name';
        $seance = $this->Seance->find('first', [
            'conditions' => ['Seance.id' => $id],
            'fields' => [$fileType, $fileSize, $fileName, $file],
            'recursive' => -1
        ]);
        $this->response->type($seance['Seance'][$fileType]);
        $this->response->download($seance['Seance'][$fileName]);
        $this->response->body($seance['Seance'][$file]);
    }

    /**
     * @access public
     * @param type $seance_id
     * @param type $model_id
     * @param type $acteur_id
     * @return type
     * @version 5.1.0
     */
    public function downloadAttachedFileConvocation($id, $model_id, $acteur_id)
    {
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($id, 'read');

        return $this->downloadAttachedFile($id, $model_id, $acteur_id);
    }

    /**
     * @access public
     *
     * @param type $seance_id
     * @param type $model_id
     * @param type $acteur_id
     * @return type
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function downloadAttachedFileOrdredujour($id, $model_id, $acteur_id)
    {
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($id, 'read');
        return $this->downloadAttachedFile($id, $model_id, $acteur_id);
    }

    /**
     * Télechargement des pieces jointes PDF
     *
     * @access private
     *
     * @param type $type
     * @param type $seance_id
     * @param type $model_id
     * @param type $acteur_id
     * @return type
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    private function downloadAttachedFile($seanceId, $modelId, $acteurId)
    {
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($seanceId, 'read');

        //refonte du chemin vers le pdf
        $folderActor = new Folder(
            DATA . Configure::read('Config.tenantName') . DS  . 'files'
            . DS . 'seances' . DS . $seanceId . DS . $modelId . DS . $acteurId,
        );

        $files = $folderActor->find('.*\.pdf');
        foreach ($files as $filename) {
            $file = new File($folderActor->pwd() . DS . $filename);
        }

        if ($file->exists()) {
            $this->response->disableCache();
            $this->response->body($file->read());
            $this->response->type('application/pdf');
            $this->response->download($filename);
            $file->close();

            return $this->response;
        }
    }

    /**
     * @access public
     * @version 5.1.0
     * @since 1.0.0
     * @param type $seance_id
     * @param type $new_position
     * @param type $delib_id
     */
    public function changePosition($seance_id, $new_position, $delib_id)
    {
        $delib = $this->Deliberation->DeliberationSeance->find('first', [
            'conditions' => ['deliberation_id' => $delib_id,
                'seance_id' => $seance_id],
            'fields' => ['id', 'DeliberationSeance.position'],
            'recursive' => -1]);

        $old_position = $delib['DeliberationSeance']['position'];
        if ($new_position < $old_position) {
            $delta = 1;
            $start = $new_position;
            $end = $old_position - 1;
        } else {
            $delta = -1;
            $start = $old_position + 1;
            $end = $new_position;
        }

        $this->Deliberation->DeliberationSeance->updateAll(
            ['DeliberationSeance.position' => "DeliberationSeance.position+$delta"],
            [
            "DeliberationSeance.position >= " => $start,
            "DeliberationSeance.position <= " => $end,
            "DeliberationSeance.seance_id" => $seance_id,
            "Deliberation.etat <> " => -1]
        );

        $this->Deliberation->DeliberationSeance->id = $delib['DeliberationSeance']['id'];
        $this->Deliberation->DeliberationSeance->saveField('position', $new_position);

        $this->Flash->set(
            __("Projet n° %s déplacé de position %s en position %s", $delib_id, $old_position, $new_position),
            ['element' => 'growl']
        );
        $this->redirect("/seances/schedule/$seance_id");
    }

    /**
     * Clôturer séance
     *
     * @access public
     * @param type $seance_id
     * @return type
     *
     * @version 5.1.0
     * @since 4.0.0
     */
    public function clore($seance_id)
    {
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($seance_id, 'update');

        $seance = $this->Seance->find('first', [
            'fields' => [
                'Seance.type_id',
                'Seance.date',
                'pv_sommaire',
                'pv_complet'
            ],
            'contain' => [
                'Typeseance.action',
                'Typeseance.id'
            ],
            'conditions' => ['Seance.id' => $seance_id],
            ]);
        $date_seance = strtotime($seance['Seance']['date']);
        $date_now = strtotime(date('Y-m-d H:i:s'));

        if ($date_seance > $date_now) {
            $this->Flash->set(
                __('Vous ne pouvez pas clôturer une séance future'),
                ['element' => 'growl','params'=>['type' => 'danger']]
            );
            return $this->redirect($this->previous);
        }

        $ids = $this->Seance->getDeliberationsId($seance_id);

        // nombre de signatures manquantes
        if ($seance['Typeseance']['action']  === 0) {
            $nbActesNonSigne = $this->Deliberation->find('count', [
                'conditions' => [
                    'Deliberation.id' => $ids,
                    'Deliberation.signee' => false
                ],
                'recursive' => -1
            ]);
            if ($nbActesNonSigne > 0) {
                $this->Flash->set(
                    __('Tous les actes ne sont pas signés.'),
                    ['element' => 'growl','params'=>['type' => 'danger']]
                );

                return $this->redirect($this->previous);
            }

            if (empty($seance['Seance']['pv_sommaire'])
                xor empty($seance['Seance']['pv_complet'])
            ) {
                $this->Flash->set(
                    __('Tous les procès-verbaux ne sont pas importés.'),
                    ['element' => 'growl','params'=>['type' => 'danger']]
                );

                return $this->redirect($this->previous);
            }
        }

        $this->Seance->id = $seance_id;
        // séances sans action
        if ($seance['Typeseance']['action'] === 2) {
            $this->cloreSeanceSansAction($seance_id);
        }
        if ($this->Seance->saveField('traitee', 1)) {
            $this->Flash->set(
                __("La séance %s a été clôturée", $seance_id),
                ['element' => 'growl','params'=>['type' => 'success']]
            );
        } else {
            $this->Flash->set(
                __("La séance %s n'a pas été clôturée", $seance_id),
                ['element' => 'growl','params'=>['type' => 'danger']]
            );
        }

        return $this->redirect($this->previous);
    }

    /**
     * @access public
     * @param type $seance_id
     * @param type $model_id
     * @return type
     *
     * @version 5.2.0
     * @since 4.3.0
     */
    public function sendConvocations($seanceId, $modelId)
    {
        $this->sendModeleMailSec('Convocation', $seanceId, $modelId);
    }

    /**
     * @access public
     * @param type $seance_id
     * @param type $model_id
     * @return type
     *
     * @version 5.2.0
     * @since 4.3.0
     */
    public function sendOrdredujour($seanceId, $modelId)
    {
        $this->sendModeleMailSec('Ordredujour', $seanceId, $modelId);
    }

    /**
     * [sendModeleMailSec description]
     * @param  [type] $typeModele [description]
     * @param  [type] $seanceId   [description]
     * @param  [type] $modelId    [description]
     * @return [type]             [description]
     *
     * @version 5.2.0
     */
    private function sendModeleMailSec($typeModele, $seanceId, $modelId)
    {

        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($seanceId, 'update');

        $folderTemplate = new Folder(
            DATA . Configure::read('Config.tenantName') . DS  . 'files'
            . DS . 'seances' . DS . $seanceId . DS . $modelId,
            true,
            0777
        );

        if ($this->request->is('post')) {
            $acteurs = [];
            foreach ($this->data['Acteur'] as $acteurKey => $bool) {
                if ($bool) {
                    $acteurs[] = substr($acteurKey, 3, strlen($acteurKey));
                }
            }
            if (empty($acteurs)) {
                $this->Flash->set(
                    __('Veuillez sélectionner au moins un acteur.'),
                    ['element' => 'growl','params'=>['type' => 'danger']]
                );
            }
            $this->sendMailSec($folderTemplate, $seanceId, $typeModele, $acteurs);
        }

        $seance = $this->Seance->find('first', [
            'fields' => ['id', 'date', 'type_id', 'date_convocation'],
            'contain' => [
                'Typeseance' => [
                    'libelle',
                    'action',
                    'modele_convocation_id',
                    'modele_ordredujour_id']
            ],
            'conditions' => ['Seance.id' => $seanceId],
            'order' => ['date ASC'],
            'recursive' => -1
        ]);

        $acteurs = $this->Typeseance->acteursConvoquesParTypeSeanceId($seance['Seance']['type_id']);

        foreach ($acteurs as &$acteur) {
            $dates = $this->Acteurseance->find('first', [
              'fields' => [
                  'Acteurseance.date_envoi',
                  'Acteurseance.date_reception'
                ],
                'conditions' => [
                    'Acteurseance.seance_id' => $seanceId,
                    'Acteurseance.model' => $typeModele,
                    'Acteurseance.acteur_id' => $acteur['Acteur']['id']],
                'recursive' => -1
            ]);

            // création du dossier contenant le(s) pdf(s) et test de la présence du pdf de convocation
            $folderActor = new Folder($folderTemplate->pwd() . DS . $acteur['Acteur']['id']);

            $files = $folderActor->find('.*\.pdf');
            foreach ($files as $filename) {
                $fileGen = new File($folderActor->pwd() . DS . $filename);
            }
            if (!empty($fileGen) && $fileGen->exists()) {
                $acteur['Acteur']['link'] = Router::url(
                    ['controller' => 'seances',
                            'action' => 'downloadAttachedFile'.$typeModele,
                            $seanceId,
                            $modelId,
                            $acteur['Acteur']['id']
                        ]
                );

                $fileGen->close();
            }

            $acteur['Acteur']['date_envoi'] =
                !empty($dates['Acteurseance']['date_envoi']) ? $dates['Acteurseance']['date_envoi'] : null;
            $acteur['Acteur']['date_reception'] =
                !empty($dates['Acteurseance']['date_reception']) ? $dates['Acteurseance']['date_reception'] : null;
        }

        $this->set('use_mail_securise', Configure::read('USE_MAILSEC'));
        $this->set('acteurs', $acteurs);
        $this->set('seance_id', $seanceId);
        $this->set('send_mailsec', $this->Acteurseance->isSendMailForMeeting($typeModele, $seanceId));
        $this->set('date_convocation', $seance['Seance']['date_convocation']);
        $this->set('model_id', $modelId);
    }

    /**
     * sendMailSec
     * @param $folder
     * @param $seanceId
     * @param $type
     * @param $acteurs
     * @return void
     */
    private function sendMailSec($folder, $seanceId, $type, $acteurs)
    {
        $seance = $this->Seance->find('first', [
            'fields' => ['id', 'date', 'type_id', 'date_convocation'],
            'contain' => [
                'Typeseance' => [
                    'libelle',
                    'action',
                    'modele_convocation_id',
                    'modele_ordredujour_id',
                    'modele_pvsommaire_id',
                    'modele_pvdetaille_id']
            ],
            'conditions' => ['Seance.id' => $seanceId],
            'order' => ['date' => 'ASC'],
            'recursive' => -1
        ]);

        $errors = '';
        $isSendMailActorPresent = $this->Acteurseance->isSendMailActorPresent(
            $seanceId,
            $seance['Typeseance']['modele_convocation_id']
        );
        if ($isSendMailActorPresent || Configure::read('MAILSEC')==='S2LOW') {
            $errors = $this->sendMailSecActors($type, $folder, $seance, $acteurs);
        } else {
            $errors = $this->sendMailSecAllActors($type, $folder, $seance, $acteurs);
        }

        if (!empty($errors)) {
            $this->Flash->set($errors, ['element' => 'growl','params'=>['type' => 'danger']]);
            return;
        }

        $this->Flash->set(__('Envoi des convocations effectué avec succès'), ['element' => 'growl']);
    }

    /**
     * [sendMailSecActors description]
     * @param  [type] $type    [description]
     * @param  [type] $folder  [description]
     * @param  [type] $seance  [description]
     * @param  [type] $acteurs [description]
     * @return [type]          [description]
     *
     * @version 5.2
     */
    private function sendMailSecActors($type, $folder, $seance, $acteurs)
    {
        $message = '';
        foreach ($acteurs as $acteur) {
            $acteurSend = $this->Acteur->find(
                'first',
                [
                'conditions' => ['Acteur.id' => $acteur],
                'recursive' => -1]
            );
            try {
                $Email = $this->sendMailSecReplaceValues($type, $folder, $seance, $acteur);
                $mailId = $this->MailSec->sendMail($Email);
                if ($mailId !== false) {
                    $this->sendMailSecSaveSend($type, $seance['Seance']['id'], $acteur, $mailId);
                }
            } catch (Exception $e) {
                CakeLog::error($e->getMessage(), 'connector');
                $message .= $acteurSend['Acteur']['prenom'] . ' ' . $acteurSend['Acteur']['nom']
                    . ' : Non envoyé' . "\n";
            }
            sleep(1);
        }

        return !empty($message) ? $message : '';
    }

    /**
     * [sendMailSecAllActors description]
     * @param  [type] $type    [description]
     * @param  [type] $folder  [description]
     * @param  [type] $seance  [description]
     * @param  [type] $acteurs [description]
     * @return [type]          [description]
     *
     * @version 5.2
     */
    private function sendMailSecAllActors($type, $folder, $seance, $acteurs)
    {
        $Email = $this->sendMailSecReplaceValues($type, $folder, $seance, $acteurs[0]);
        $acteursSend = $acteurs;
        unset($acteursSend[0]);
        $acteursMailSend = [];
        foreach ($acteursSend as $acteur) {
            $this->Acteur->id = $acteur;
            $acteursMailSend[] =  $this->Acteur->field('email');
            $this->Acteur->clear();
        }

        $Email->addTo($acteursMailSend);
        try {
            $mailId = $this->MailSec->sendMail($Email);
            if ($mailId !== false) {
                foreach ($acteurs as $acteur) {
                    $this->sendMailSecSaveSend($type, $seance['Seance']['id'], $acteur, $mailId);
                }
            } else {
                throw new \Exception("Error sendMailSecSaveSend", 1);
            }
        } catch (Exception $e) {
            CakeLog::error($e->getMessage());
            $message = '';
            foreach ($acteurs as $acteur) {
                $acteurSend = $this->Acteur->find(
                    'first',
                    [
                    'conditions' => ['Acteur.id' => $acteur],
                    'recursive' => -1]
                );
                $message .= $acteurSend['Acteur']['prenom'] . ' ' . $acteurSend['Acteur']['nom']
                    . ' : Non envoyé' . "\n";
            }
        }

        return !empty($message) ? $message : '';
    }

    /**
     * [sendMailSecReplaceValues description]
     * @param  [type] $type   [description]
     * @param  [type] $folder   [description]
     * @param  [type] $seance   [description]
     * @param  [type] $acteurId [description]
     * @return [type]           [description]
     *
     * @version 5.2
     */
    private function sendMailSecReplaceValues($type, $folder, $seance, $acteurId)
    {
        $acteur = $this->Acteur->find(
            'first',
            [
            'conditions' => ['Acteur.id' => $acteurId],
            'recursive' => -1]
        );

        $acteur_info = $this->Acteur->find('first', [
            'fields' => ['Acteur.id', 'Acteur.nom', 'Acteur.prenom', 'Acteur.position'],
            'conditions' => ['Acteur.id' => $acteur['Acteur']['id']],
            'recursive' => -1
        ]);

        //refonte du chemin vers le pdf
        $folderActor = new Folder($folder->pwd() . DS . $acteurId);

        $files = $folderActor->find('.*\.pdf');
        foreach ($files as $filename) {
            $file = new File($folderActor->pwd() . DS . $filename);
        }

        if (!$file->exists()) {
            throw new \Exception("Error sendMailSecReplaceValues File attachments", 1);
        }

        $searchReplace = [
          "#NOM#" => $acteur['Acteur']['nom'],
          "#PRENOM#" => $acteur['Acteur']['prenom'],
          "#SEANCE_TYPE#" => $seance['Typeseance']['libelle'],
          "#SEANCE_DATETIME#"=> CakeTime::i18nFormat($seance['Seance']['date'], '%A %d %B %Y à %k:%M')
        ];

        $subject =  str_replace(
            array_keys($searchReplace),
            array_values($searchReplace),
            $type == 'Convocation' ? Configure::read('MAILSEC_CONVOCATION_OBJET')
                : Configure::read('MAILSEC_ORDREDUJOUR_OBJET')
        );

        $Email = new CakeEmail();
        $Email->template('seance_' . strtolower($type), 'default')
        ->to($acteur['Acteur']['email'])
        ->subject($subject)
        ->viewVars([
              'nom' => $acteur['Acteur']['nom'],
              'prenom' => $acteur['Acteur']['prenom'],
              'seance_type' => $seance['Typeseance']['libelle'],
              'seance_datetime' => CakeTime::i18nFormat($seance['Seance']['date'], '%A %d %B %Y à %k:%M'),
          ])
        ->attachments([
          $file->name => [
            'file' => $file->pwd(),
            'mimetype' => 'application/pdf'
          ]
        ]);

        $fileTemplateHtml = new File(APP . __('Config/mails/html/seance_%s.ctp', strtoupper($type)));
        if ($fileTemplateHtml->exists()) {
            $Email->viewRender('Email')->template($fileTemplateHtml->pwd());
        }
        $fileTemplateTxt = new File(APP . __('Config/mails/text/seance_%s.ctp', strtoupper($type)));
        if ($fileTemplateTxt->exists()) {
            $Email->viewRender('Text')->template($fileTemplateTxt->pwd());
        }

        $file->close();

        return $Email;
    }

    /**
     * [sendMailSecReplaceValues description]
     * @param  [type] $seanceId [description]
     * @param  [type] $acteur   [description]
     * @param  [type] $mailId   [description]
     * @return [type]           [description]
     *
     * @version 5.2
     */
    private function sendMailSecSaveSend($type, $seanceId, $acteur, $mailId)
    {
        $this->Acteurseance->create();

        $acteurseance['seance_id'] = $seanceId;
        $acteurseance['acteur_id'] = $acteur;
        $acteurseance['mail_id'] = is_array($mailId) ? null : $mailId;
        $acteurseance['date_envoi'] = date("Y-m-d H:i:s", strtotime("now"));
        $acteurseance['date_reception'] = null;
        $acteurseance['model'] = Inflector::camelize($type);

        $this->Acteurseance->save($acteurseance);
    }

    /**
     * [deleteSendMail description]
     * @param  [type] $id         [description]
     * @param  [type] $typeFusion [description]
     * @return [type]             [description]
     *
     * @version 5.1.0
     */
    public function deleteSendMail($id, $typeFusion)
    {
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($id, 'delete');

        if ($this->Acteurseance->isSendMailForMeeting($typeFusion, $id)) {
            $this->Acteurseance->deleteSendMail($typeFusion, $id);
            $this->Seance->save(['id' => $id, 'date_convocation' => ''], false);
        }

        return $this->redirect($this->previous);
    }

    /**
     * @version 4.3
     * @access public
     * @param type $seance_id
     * @param type $model_id
     */
    public function downloadZip($seanceId, $modelId, $type)
    {
        $files_search = [];

        $seance = $this->Seance->find('first', [
            'fields' => ['id', 'type_id'],
            'conditions' => ['Seance.id' => $seanceId],
            'recursive' => -1
        ]);

        $acteurs = $this->Typeseance->acteursConvoquesParTypeSeanceId($seance['Seance']['type_id']);

        foreach ($acteurs as $acteur) {
            $folderActor = new Folder(
                DATA . Configure::read('Config.tenantName') . DS  . 'files'
                . DS . 'seances' . DS . $seanceId . DS . $modelId . DS . $acteur['Acteur']['id'],
            );
            $files = $folderActor->find('.*\.pdf');
            foreach ($files as $filename) {
                $files_search[] = $folderActor->pwd() . DS . $filename;
            }
        }

        $folder = new Folder(AppTools::newTmpDir(TMP . 'files' . DS . 'seances' . DS));
        $zip = new ZipArchive();

        if (file_exists($folder->pwd() . DS . $type . '.zip')) {
            unlink($folder->pwd() . DS . $type . '.zip');
        }
        try {
            $filezip = $zip->open($folder->pwd() . DS . $type . '.zip', ZIPARCHIVE::CREATE);
            if ($filezip) {
                foreach ($files_search as $file_search) {
                    $file = new File($file_search);
                    $zip->addFile($file->pwd(), $file->name);
                    $file->close();
                }
            }
            $zip->close();
        } catch (Exception $e) {
            $this->Flash->set(
                __('Une erreur est survenue lors de la génération de l\'archive'),
                ['element' => 'growl']
            );
        }

        $this->response->disableCache();
        $this->response->type('application/zip');
        $this->response->download($type . '.zip');
        $this->response->body(file_get_contents($folder->pwd() . DS . $type . '.zip'));

        return $this->response;
    }

    /**
     * Génération de la fusion d'un modèle pour le premier acteur convoqué et envoi du résultat vers le client
     *
     * @version 4.3
     * @access public
     * @param integer $id --> id de la séance
     * @param string $typeFusion --> type de la fusion à générer : conv, adj, ...
     * @param integer $cookieToken --> numéro de cookie du client pour masquer la fenêtre attendable
     * @return CakeResponse
     */
    public function genereFusionToClient($id, $typeFusion, $cookieTokenKey = null)
    {
        try {
            $this->Progress->start($cookieTokenKey);
            // vérification de l'existence de la séance
            if (!$this->Seance->hasAny(['id' => $id])) {
                throw new Exception(__('Séance id: %s non trouvée en base de données', $id));
            }
            $this->Progress->at(10, __('Récupération des données à générer'));

            // vérification des types de fusion
            $allowedFusionTypes = ['convocation', 'ordredujour', 'pvsommaire', 'pvdetaille', 'journal_seance'];
            if (!in_array($typeFusion, $allowedFusionTypes, true)) {
                throw new Exception(
                    __('le type de modèle d\'édition %s n\'est par autorisé', $typeFusion, $allowedFusionTypes)
                );
            }

            // fusion du document
            $this->Seance->Behaviors->load('OdtFusion', [
              'id' => $id,
              'modelOptions' => ['modelTypeName' => $typeFusion]
            ]);

            $this->Progress->at(40, __('Fusion du document'));
            $this->Seance->odtFusion();
            $filename = $this->Seance->fusionName();

            // selon le format d'envoi du document (pdf ou odt)
            if ($this->Auth->user('formatSortie') === 'pdf') {
                $this->Progress->at(70, __('Conversion du document'));
                $typemime = "application/pdf";
                $filename = $filename . '.pdf';
                $content = $this->Seance->getOdtFusionResult();
            } else {
                $this->Progress->at(70, __('Actualisation du sommaire'));
                $typemime = "application/vnd.oasis.opendocument.text";
                $filename = $filename . '.odt';
                $content = $this->Seance->getOdtFusionResult('odt');
            }
            unset($this->Deliberation->odtFusionResult);
            $this->Progress->at(90, __('Préparation du fichier de sortie'));
            $this->Progress->setFile($filename, $typemime, $content);

            $this->Progress->stop();
        } catch (Exception $e) {
            $this->Progress->error($e);
            //__('Erreur lors de la génération du document : %s', $e->getMessage())
            $this->log(
                'Fusion :' . $e->getMessage()
                . __(' Fichier ') . $e->getFile() . __(' Ligne ') . $e->getLine(),
                'error'
            );
        }

        $this->autoRender = false;
        $this->response->type(['json' => 'text/x-json']);
        $this->RequestHandler->respondAs('json');
        $this->response->body(json_encode($this->Session->read('Progress.Tokens')));
        return $this->response;
    }

    /**
     * Fonction de génération des convocations des acteurs convoqués à une séance et stockage sur file system
     * @since 4.3
     * @version 5.0
     * @access public
     * @param integer $id --> id de la séance
     * @param integer $modelTemplateId --> id du template de fusion
     * @param string $typeFusion --> type de la fusion à générer : conv, adj, ...
     * @param integer $cookieToken --> numéro de cookie du client pour masquer la fenêtre attendable
     * @return CakeResponse
     */
    public function genereFusionToFiles($id, $modelTemplateId, $typeFusion, $cookieTokenKey)
    {
        try {
            $this->Progress->start($cookieTokenKey);
            $this->Progress->at(10, __('Récupération des données à générer'));
            // vérification de l'existence de la séance
            if (!$this->Seance->hasAny(['id' => $id])) {
                throw new Exception(__('Séance id: %s non trouvée en base de données', $id));
            }

            // vérification des types de fusion
            $allowedFusionTypes = ['convocation', 'ordredujour'];
            if (!in_array($typeFusion, $allowedFusionTypes, true)) {
                throw new Exception(__('Le type de modèle d\'édition %s n\'est pas autorisé', $typeFusion));
            }

            // lecture de la liste des acteurs convoqués
            $typeSeanceId = $this->Seance->field('type_id', ['id' => $id]);
            $convoques = $this->Seance->Typeseance->acteursConvoquesParTypeSeanceId($typeSeanceId, null, ['id']);
            if (empty($convoques)) {
                throw new Exception(__('Aucun acteur(s) convoqué(s) pour la séance id:', $id));
            }

            // format de conversion
            //$formatConversion = $this->Auth->User('formatSortie') == 0 ? 'pdf' : 'odt';
            // initialisation du répertoire de destination des convocations
            App::uses('Folder', 'Utility');
            App::uses('File', 'Utility');
            $pathTemplate = DATA . Configure::read('Config.tenantName') . DS  . 'files'
                . DS . 'seances' . DS . $id . DS . $modelTemplateId;
            $folder = new Folder(
                $pathTemplate,
                true,
                0777
            );
            $folder->delete();

            //Suppression des fichiers déjà généré.
            // $files = $folder->find('.*\.pdf');
            // foreach ($files as $file) {
            //     $file = new File($folder->pwd() . DS . $file);
            //     $file->delete();
            //     $file->close();
            // }

            // chargement  du behavior de fusion du document
            $this->Seance->Behaviors->load('OdtFusion', [
              'id' => $id,
              'modelTemplateId' => $modelTemplateId,
              'modelOptions' => ['modelTypeName' => $typeFusion]]);
            // le modèle template possede-t-il des variables de fusion des acteurs
            $acteurPresentTemplate =
                $this->Seance->modelTemplateOdtInfos->hasUserFieldsDeclared(
                    'salutation_acteur',
                    'prenom_acteur',
                    'nom_acteur',
                    'titre_acteur',
                    'position_acteur',
                    'email_acteur',
                    'telmobile_acteur',
                    'telfixe_acteur',
                    'adresse1_acteur',
                    'adresse2_acteur',
                    'cp_acteur',
                    'ville_acteur',
                    'note_acteur'
                );
            // traitement différent en fonction de la présence de variables acteur dans le template
            //FIX les convocations sont automatiquement regénérées au lieu d'être gardées lorsqu'elle sont envoyées
            //Seance -> type seance -> libelle
            $conditions = [];
            switch ($typeFusion) {
                case 'convocation':
                    $conditions[] = ['Typeseance.modele_convocation_id' => $modelTemplateId];
                    break;
                case 'ordredujour':
                    $conditions[] = ['Typeseance.modele_ordredujour_id' => $modelTemplateId];
                    break;
                default:
                    break;
            }
            $conditions[] = ['Seance.id' => $id];
            $seance = $this->Seance->find('first', [
                'conditions' => $conditions,
                'fields' => ['Seance.id', 'Seance.date'],
                'contain' => ['Typeseance.libelle'],
                'recursive' => -1
            ]);

            $date = CakeTime::i18nFormat($seance['Seance']['date'], '%d-%m-%Y');
            if ($acteurPresentTemplate) {
                $cpt=11;
                $nb_convoques = (count($convoques)+20)+10;
                foreach ($convoques as $acteur) {
                    $folderActor = new Folder($pathTemplate . DS . $acteur['Acteur']['id'], true, 0777);
                    $acteur_info = $this->Acteur->find('first', [
                        'fields' => ['Acteur.id', 'Acteur.nom', 'Acteur.prenom', 'Acteur.position'],
                        'conditions' => ['Acteur.id' => $acteur['Acteur']['id']],
                        'recursive' => -1
                    ]);
                    //FIX progression
                    $this->Progress->at(
                        100 - round((($nb_convoques-$cpt)/$nb_convoques * 100)),
                        __(
                            'Fusion et conversion du document pour l\'acteur : %s %s...',
                            $acteur_info['Acteur']['nom'],
                            $acteur_info['Acteur']['prenom']
                        )
                    );
                    $cpt++;
                    $this->Seance->odtFusion(['modelOptions' => ['acteurId' => $acteur['Acteur']['id']]]);
                    $filename = $this->Seance->fusionName([
                      'modelTemplateFileOutputFormatValues'=> [
                        'id' => $acteur['Acteur']['id'],
                        'acteurLastName'=> $acteur_info['Acteur']['nom'],
                        'acteurFirstName'=> $acteur_info['Acteur']['prenom'],
                        'sequence' => $acteur_info['Acteur']['position']
                      ]
                    ]);
                    $content = $this->Seance->getOdtFusionResult();
                    $file = new File($folderActor->pwd() . DS . $filename . '.pdf', true, 0777);
                    $file->write($content);
                    $file->close();

                    unset($content);
                }
            } else {
                $this->Progress->at(40, __('Fusion du document'));
                $this->Seance->odtFusion();
                $this->Progress->at(70, __('Conversion du document'));
                $content = $this->Seance->getOdtFusionResult();

                foreach ($convoques as $acteur) {
                    $folderActor = new Folder($pathTemplate . DS . $acteur['Acteur']['id'], true, 0777);
                    $acteur_info = $this->Acteur->find('first', [
                        'fields' => ['Acteur.id', 'Acteur.nom', 'Acteur.prenom', 'Acteur.position'],
                        'conditions' => ['Acteur.id' => $acteur['Acteur']['id']],
                        'recursive' => -1
                    ]);
                    $filename = $this->Seance->fusionName([
                      'modelTemplateFileOutputFormatValues'=> [
                        'id' => $acteur['Acteur']['id'],
                        'acteurLastName'=> $acteur_info['Acteur']['nom'],
                        'acteurFirstName'=> $acteur_info['Acteur']['prenom'],
                        'sequence' => $acteur_info['Acteur']['position']
                      ]
                    ]);
                    $file = new File($folderActor->pwd() . DS . $filename . '.pdf', true, 0777);
                    $file->write($content);
                    $file->close();
                }
                unset($content);
            }
            // mise à jour de la date de génération des convocations
            if ($typeFusion==='convocation') {
                $this->Seance->save(['id' => $id, 'date_convocation' => date("Y-m-d H:i:s", strtotime("now"))], false);
            }
        } catch (Exception $e) {
            $this->log('Fusion :' . $e->getMessage() . ' File:' . $e->getFile() . ' Line:' . $e->getLine(), 'error');
            $this->Progress->error($e);
            $this->Flash->set(
                __('Erreur lors de la génération du document %s', $e->getMessage()),
                ['element' => 'growl','params'=>['type' => 'danger']]
            );
        }

        $this->Progress->at(90, __('Fin des générations de fichiers'));
        $this->Progress->redirect($this->previous);
        $this->Progress->stop();

        $this->autoRender = false;
        $this->response->type(['json' => 'text/x-json']);
        $this->RequestHandler->respondAs('json');
        $this->response->body(json_encode($this->Session->read('Progress.Tokens')));
        return $this->response;
    }

    /**
     * Génération de la fusion pour plusieurs séances : l'id du
     * modèle de fusion et les séances a fusionner sont passés dans les données du formulaire
     *
     * @version 4.3
     * @access public
     * @return CakeResponse
     * @throws Exception
     */
    public function genereFusionMultiSeancesToClient($cookieTokenKey)
    {
        try {
            $this->Progress->start($cookieTokenKey);
            // initialisation de l'id du modèle de fusion
            $modelTemplateId = $this->request->data['Seance']['model_id'];
            unset($this->request->data['Seance']['model_id']);

            // initialisation de la liste des séances sélectionnées
            $seancesIds = [];
            foreach ($this->request->data['Seance'] as $seanceId => $selected) {
                if ($selected) {
                    $seanceId = explode('_', $seanceId);
                    $seancesIds[] = $seanceId[1];
                }
            }
            if (empty($seancesIds)) {
                throw new Exception(__('aucune séance sélectionnée'));
            }
            $this->Progress->at(30);

            // fusion du document
            $this->Seance->Behaviors->load(
                'OdtFusion',
                [
                    'modelTemplateId' => $modelTemplateId,
                    'modelOptions' => ['modelTypeName' => 'multiseances']
                ]
            );
            $this->Seance->odtFusion(['modelOptions' => ['seanceIds' => $seancesIds]]);
            $filename = $this->Seance->fusionName();
            $this->Progress->at(60);

            // selon le format d'envoi du document (pdf ou odt)
            if ($this->Auth->User('formatSortie') === 'pdf') {
                $typemime = "application/pdf";
                $filename .= '.pdf';
                $content = $this->Seance->getOdtFusionResult();
            } else {
                $typemime = "application/vnd.oasis.opendocument.text";
                $filename .= '.odt';
                $content = $this->Seance->getOdtFusionResult('odt');
            }
            $this->Seance->deleteOdtFusionResult();
            $this->Progress->at(90);
            $this->Progress->setFile($filename, $typemime, $content);

            // envoi au client
            $this->Progress->stop();
        } catch (Exception $e) {
            $this->Progress->error($e);
            $this->log(
                'Fusion :' . $e->getMessage() . ' '
                . __('Fichier') . ' ' . $e->getFile() . ', ' . __('Ligne') . ' ' . $e->getLine(),
                'error'
            );
        }

        $this->autoRender = false;
        $this->response->type(['json' => 'text/x-json']);
        $this->RequestHandler->respondAs('json');
        $this->response->body(json_encode($this->Session->read('Progress.Tokens')));
        return $this->response;
    }

    /**
     *
     * @access private
     * @param type $seance_id
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    private function cloreSeanceSansAction($seance_id)
    {
        if (!$seance_id) {
            $this->Flash->set(
                __('ID invalide pour la séance'),
                ['element' => 'growl','params'=>['type' => 'danger']]
            );
            $this->redirect($this->previous);
        }
        $actes = $this->DeliberationSeance->find(
            'all',
            [
            'fields' => [
                'DeliberationSeance.seance_id',
                'DeliberationSeance.deliberation_id',
                'Seance.id',
                'Seance.date',
                'Seance.type_id',
                'Deliberation.id',
                'Typeseance.compteur_id'
            ],
            'conditions' => [
                'DeliberationSeance.seance_id' => $seance_id,
                'Deliberation.etat >=' => 0,
                'Deliberation.num_delib' => null,
                'Deliberation.signee' => false

            ],
            'joins' => [
                $this->DeliberationSeance->join('Deliberation', ['type' => 'LEFT']),
                $this->DeliberationSeance->join('Seance', ['type' => 'LEFT']),
                $this->Seance->join('Typeseance', ['type' => 'LEFT']),
            ],
            'recursive' => -1,
            'order' => 'DeliberationSeance.position ASC',
            ]
        );

        foreach ($actes as $acte) {
            $seance_deliberante_id = $this->Deliberation->getSeanceDeliberanteId($acte['Deliberation']['id']);
            if (empty($seance_deliberante_id)) {
                $this->Deliberation->id = $acte['Deliberation']['id'];
                $this->Deliberation->saveField('etat', 3);
                $this->Deliberation->saveField('signee', true);
                $this->Deliberation->saveField('date_acte', $acte['Seance']['date']);
                $this->Deliberation->saveField(
                    'num_delib',
                    $this->Seance->Typeseance->Compteur->genereCompteur($acte['Typeseance']['compteur_id'])
                );
                $this->Deliberation->saveField(
                    'delib_pdf',
                    $this->Deliberation->fusion(
                        $acte['Deliberation']['id'],
                        null,
                        $acte['Typeseance']['modele_deliberation_id']
                    )
                );
                $this->Deliberation->clear();
            }
        }
    }

    public function saveProcesVerbaux($id)
    {
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($id, 'update');

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Seance->id = $id;
            if (!empty($this->data['Seance']['pvsommaire'])) {
                $pvsommaire = null;
                if ($this->data['Seance']['pvsommaire']['tmp_name']) {
                    $pvsommaire =
                        file_get_contents($this->data['Seance']['pvsommaire']['tmp_name']);
                }
                $this->request->data['Seance']['pv_sommaire'] = $pvsommaire;
            }
            if (!empty($this->data['Seance']['pvcomplet'])) {
                $pvcomplet = null;
                if ($this->data['Seance']['pvcomplet']['tmp_name']) {
                    $pvcomplet =
                        file_get_contents($this->data['Seance']['pvcomplet']['tmp_name']);
                }
                $this->request->data['Seance']['pv_complet'] = $pvcomplet;
            }
            if (empty($this->request->data['Seance']['pv_complet'])
                && empty($this->request->data['Seance']['pv_sommaire'])) {
                $this->request->data['Seance']['pv_figes']  = 0;
            } else {
                $this->request->data['Seance']['pv_figes']  = 1;
            }
            if ($this->Seance->save($this->data, true, ['pv_sommaire','pv_complet', 'pv_figes'])) {
                $this->Flash->set(__('Procès-verbaux enregistrés'), ['element' => 'growl']);
                return $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Veuillez corriger les erreurs ci-dessous'),
                    ['element' => 'growl','params'=>['type' => 'danger']]
                );
            }
        }

        $this->data = $this->Seance->find('first', [
            'contain' => [
                'Typeseance' => [
                    'libelle',
                    'action',
                    'modele_pvsommaire_id',
                    'modele_pvdetaille_id',
                    'Modele_pvdetaille.name',
                    'Modele_pvsommaire.name',
                ],
            ],
            'conditions' => ['Seance.id' => $id],
            'recursive' => -1
        ]);
    }
}
