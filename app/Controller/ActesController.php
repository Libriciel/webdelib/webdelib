<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppController', 'Controller');

/**
 * Controller Actes
 *
 * @package app.Controller
 * @version 5.2.0
 */
class ActesController extends AppController
{
    /** @var Array Contains a uses */
    public $uses = [
        'Deliberation',
        'Infosupdef',
        'Infosup',
        'Historique'
    ];

    /** @var Array Contains a components */
    public $components = [
        'RequestHandler',
        'Progress',
        'SabreDav',
        'Auth' => [
            'mapActions' => [
                'editPostSign',
                'allow' => [
                    'getTampon',
                    'getBordereau',
                    'getBordereauTdt',
                    'generationNumerotation',
                    'downloadTdtMessage',
                    'downloadBordereau',
                    'download',
                    'downloadSignature',
                    'downloadTdtTampon',
                ]
            ],
        ],
    ];

    /**
     * [edit description]
     * @return [type] [description]
     */
    public function editPostSign($id)
    {
        $this->Deliberation->Behaviors->load('Version', $this->Deliberation->actsAsVersionOptionsList['Version']);

        if (!$id) {
            throw new NotFoundException(__('Id de déliberation invalide'));
        }

        $deliberation = $this->Deliberation->find('first', [
          'fields' => [
              'id', 'parent_id',
              'objet', 'objet_delib', 'titre',
              'is_multidelib',
              'created',
              'modified',
            ],
          'contain' => [
              'Infosup',
              'DeliberationUser',
              'DeliberationTypeseance' => ['fields' => ['typeseance_id']],
              'DeliberationSeance'=> ['fields' => ['seance_id']],
              'Redacteur' => ['fields' => ['id', 'nom', 'prenom']]
          ],
          'conditions' => ['Deliberation.id' => $id],
          'recursive' => -1
        ]);

        if ($this->request->is(['post', 'put'])) {
            try {
                $success = $this->Infosup->saveInfosup($this->request->data, 'Deliberation', false);

                $this->Flash->set(__('Informations supplémentaires modifiées'), ['element' => 'growl']);
                $this->Historique->enregistre(
                    $id,
                    $this->Auth->user('id'),
                    __('Informations supplémentaires modifiées')
                );

                return $this->redirect($this->previous);
            } catch (Exception $e) {
                // $this->Flash->set(__('Veuillez corriger les erreurs ci-dessous :'), [
                //   'element' => 'growl','params' => [
              //         'type' => 'danger',
              //         'settings' => ['delay' => 10000]]]);
            }
        }

        $this->SabreDav->start($id);

        if (!$this->request->data) {
            $this->request->data = $deliberation;

            // création des fichiers des infosup de type odtFile
            foreach ($this->request->data['Infosup'] as $infosup_key => $infosup) {
                $infoSupDef = $this->Infosupdef->find('first', [
                'fields' => ['type'],
                'conditions' => [
                  'id' => $infosup['infosupdef_id'],
                  'model' => 'Deliberation',
                  'actif' => true],
                'recursive' => -1,
                ]);

                //FIX
                if (!empty($infoSupDef['Infosupdef']['type'])
                    && $infoSupDef['Infosupdef']['type'] == 'odtFile'
                    && !empty($infosup['file_name']) && !empty($infosup['content'])) {
                    $this->request->data['Infosup'][$infosup_key]['file_infosup'] =
                        $this->SabreDav->newFileDav($infosup['file_name'], $infosup['content']);
                    $this->request->data['Infosup'][$infosup_key]['file_infosup_url'] =
                        $this->SabreDav->newFileDavUrl();
                }
            }

            // création des fichiers des infosup de type odtFile
            foreach ($this->request->data['Infosup'] as $infosup_key => $infosup) {
                $infoSupDef = $this->Infosupdef->find('first', [
                            'fields' => ['type'],
                            'conditions' => [
                              'id' => $infosup['infosupdef_id'],
                              'model' => 'Deliberation',
                              'actif' => true],
                            'recursive' => -1,
                          ]);
            }
        }
        //Gestion des informations supplémentaires
        $this->request->data['Infosup'] = $this->Deliberation->Infosup->compacte($this->request->data['Infosup']);

        $this->set('infosuplistedefs', $this->Infosupdef->generateListes('Deliberation'));
        $this->set('infosupdefs', $this->Infosupdef->find('all', [
                    'conditions' => ['model' => 'Deliberation',
                    'edit_post_sign' => true,
                    'actif' => true],
                    'order' => 'ordre',
                    'contain' => ['Profil.id']]));
    }

    /**
     * @param int $delib_id
     * @return CakeResponse
     */
    public function download($delib_id, $isRevision = false)
    {
        if (!$isRevision) {
            $this->Deliberation->id = $delib_id;
            $delib_pdf = $this->Deliberation->field('delib_pdf');
            $num_delib = $this->Deliberation->field('num_delib');
        } else {
            $this->Deliberation->Behaviors->load(
                'Version',
                $this->Deliberation->actsAsVersionOptionsList['Version']
            );

            $delib = $this->Deliberation->version($isRevision);
            $delib_pdf = $delib['deliberations_versions']['delib_pdf'];
            $num_delib = $delib['deliberations_versions']['data_version']['Deliberation']['num_delib'];
        }

        if (!empty($num_delib)) {
            $filename = $num_delib . '.pdf';
        } else {
            $filename = 'projet_' . $delib_id . '.pdf';
        }
        // envoi au client
        $this->response->disableCache();
        $this->response->body($delib_pdf);
        $this->response->type('application/pdf');
        $this->response->download($filename);
        return $this->response;
    }

    /**
     * @version 5.1.0
     * @since 4.3
     * @access public
     * @param type $delib_id
     * @return type
     */
    public function downloadBordereau($id, $type = null, $isRevision = false)
    {
        $this->Deliberation->id = $id;

        switch ($type) {
            default:
                if (!$isRevision) {
                    $bordereau = $this->Deliberation->field('parapheur_bordereau');
                    $num_delib = $this->Deliberation->field('num_delib');
                } else {
                    $this->Deliberation->Behaviors->load(
                        'Version',
                        $this->Deliberation->actsAsVersionOptionsList['Version']
                    );

                    $delib = $this->Deliberation->version($isRevision);
                    $bordereau = $delib['deliberations_versions']['parapheur_bordereau'];
                    $num_delib = $delib['deliberations_versions']['data_version']['Deliberation']['num_delib'];
                }

                break;
        }

        // envoi au client
        $this->response->disableCache();
        $this->response->body($bordereau);
        $this->response->type('application/pdf');
        $this->response->download('bordereau_signature_' . $num_delib . '.pdf');
        return $this->response;
    }
    /**
     * @version 5.1.0
     * @access public
     * @param type $delib_id
     * @return type
     */
    public function downloadSignature($delib_id, $isRevision = false)
    {
        if (!$isRevision) {
            $this->Deliberation->id = $delib_id;
            $signature = $this->Deliberation->field('signature');
            $num_delib = $this->Deliberation->field('num_delib');
        } else {
            $this->Deliberation->Behaviors->load(
                'Version',
                $this->Deliberation->actsAsVersionOptionsList['Version']
            );

            $delib = $this->Deliberation->version($isRevision);
            $signature = $delib['deliberations_versions']['signature'];
            $num_delib = $delib['deliberations_versions']['data_version']['Deliberation']['num_delib'];
        }

        // envoi au client
        $this->response->disableCache();
        $this->response->body($signature);
        $this->response->type('application/zip');
        $this->response->download($num_delib . '_signature.zip');
        return $this->response;
    }



    /**
     * @version 5.1.0
     * @access public
     * @param type $delib_id
     * @return type
     */
    public function downloadTdtTampon($id, $type = null, $isRevision = false)
    {
        $this->Deliberation->id = $id;

        switch ($type) {
            case 'edition_data':
                if (!$isRevision) {
                    $content = $this->Deliberation->field('tdt_data_edition');
                    $filename = __('Tdt_tampon_edition_data_%s.odt', $this->Deliberation->field('num_delib'));
                    $typemime = 'application/vnd.oasis.opendocument.text';
                } else {
                    $this->Deliberation->Behaviors->load(
                        'Version',
                        $this->Deliberation->actsAsVersionOptionsList['Version'
                        ]
                    );

                    $delib = $this->Deliberation->version($isRevision);
                    $num_delib = $delib['deliberations_versions']['data_version']['Deliberation']['num_delib'];
                    $content = $delib['deliberations_versions']['tdt_data_edition'];
                    $filename = __('Tdt_tampon_edition_data_%s.odt', $num_delib);
                    $typemime = 'application/vnd.oasis.opendocument.text';
                }

                break;
            default:
                if (!$isRevision) {
                    $content = $this->Deliberation->field('tdt_data_pdf');
                    $filename = __('Tdt_tampon_%s.pdf', $this->Deliberation->field('num_delib'));
                    $typemime = 'application/pdf';
                } else {
                    $this->Deliberation->Behaviors->load(
                        'Version',
                        $this->Deliberation->actsAsVersionOptionsList['Version']
                    );

                    $delib = $this->Deliberation->version($isRevision);
                    $num_delib = $delib['deliberations_versions']['data_version']['Deliberation']['num_delib'];
                    $content = $delib['deliberations_versions']['tdt_data_pdf'];
                    $filename = __('Tdt_tampon_edition_data_%s.pdf', $num_delib);
                    $typemime = 'application/pdf';
                }

                break;
        }
        $this->response->disableCache();
        $this->response->body($content);
        $this->response->type($typemime);
        $this->response->download($filename);

        return $this->response;
    }

    /**
     * @access public
     * @param type $tdt_id
     * @return type
     * @throws Exception
     */
    public function downloadTdtMessage($tdt_id = null)
    {
        try {
            if (empty($tdt_id)) {
                throw new Exception(__('Merci d\indiquer l\'identifiant du message.'));
            }

            $data = $this->Deliberation->TdtMessage->find('first', [
                'fields' => ['tdt_data'],
                'conditions' => ['tdt_id' => $tdt_id],
                'recursive' => -1
            ]);

            if (empty($data['TdtMessage']['tdt_data'])) {
                throw new Exception(__('Le message est indiponible.'));
            }
            $tdt_data = $this->Deliberation->TdtMessage->recupMessagePdfFromTar($data['TdtMessage']['tdt_data']);
            // envoi au client
            $this->response->disableCache();
            $this->response->body($tdt_data['content']);
            $this->response->type('application/pdf');
            $this->response->download($tdt_data['filename']);
            return $this->response;
        } catch (Exception $e) {
            $this->Flash->set($e->getMessage(), ['element' => 'growl', 'params' => ['type' => 'warning']]);
            return $this->redirect($this->previous);
        }
    }

    /**
     * @access public
     * @param type $delib_id
     * @return type
     */
    public function getTampon($delib_id)
    {
        $delib = $this->Deliberation->find('first', [
            'conditions' => ['id' => $delib_id],
            'fields' => ['num_delib', 'tdt_data_pdf'],
            'recursive' => -1
        ]);

        if (empty($delib['Deliberation']['tdt_data_pdf'])) {
            $this->Flash->set(
                __('L\'acte tamponné n\'est pas encore disponible'),
                ['element' => 'growl', 'params' => ['type' => 'info']]
            );
            $this->redirect($this->previous);
        }
        // envoi au client
        $this->response->disableCache();
        $this->response->body($delib['Deliberation']['tdt_data_pdf']);
        $this->response->type('application/pdf');
        $this->response->download('tampon_tdt_' . $delib['Deliberation']['num_delib'] . '.pdf');
        return $this->response;
    }

    /**
     * @access public
     * @param int $delib_id
     * @return CakeResponse|string
     */
    public function getBordereau($id, $cookieTokenKey)
    {
        try {
            $this->Progress->start($cookieTokenKey);

            // vérification de l'existence du projet/délibération en base de données
            if (!$this->Deliberation->hasAny(['id' => $id])) {
                throw new Exception(
                    __('Projet/délibération # %s non trouvé(e) en base de données', $id)
                );
            }
            $projet = $this->Deliberation->find('first', [
                'fields' => ['Deliberation.id'],
                'contain' => [
                    'Typeacte' => ['fields' => ['modele_bordereau_projet_id']]
                ],
                'conditions' => ['Deliberation.id' => $id],
                'recursive' => -1
            ]);

            if (empty($projet['Typeacte']['modele_bordereau_projet_id'])) {
                throw new Exception(
                    __('modele_bordereau_projet_id %s non trouvé(e) en base de données', $id)
                );
            }
            // initialisation de l'id du modèle de fusion
            $this->Progress->at(10, __('Récupération des données à générer'));
            // fusion du document
            $this->Deliberation->Behaviors->load('OdtFusion', [
                'id' => $id,
                'modelTemplateId' => $projet['Typeacte']['modele_bordereau_projet_id']  //Model de rapport
            ]);

            $this->Progress->at(40, __('Fusion du document'));
            $this->Deliberation->odtFusion();
            $filename = $this->Deliberation->fusionName();

            // selon le format d'envoi du document (pdf ou odt)
            if ($this->Auth->user('formatSortie') == 'pdf') {
                $this->Progress->at(70, __('Conversion du document'));
                $typemime = "application/pdf";
                $filename = $filename . '.pdf';
                $content = $this->Deliberation->getOdtFusionResult();
            } else {
                $this->Progress->at(70, __('Actualisation du sommaire'));
                $typemime = "application/vnd.oasis.opendocument.text";
                $filename = $filename . '.odt';
                $content = $this->Deliberation->getOdtFusionResult('odt');
            }
            unset($this->Deliberation->odtFusionResult);
            $this->Progress->at(90, __('Préparation du fichier de sortie'));
            $this->Progress->setFile($filename, $typemime, $content);

            $this->Progress->stop();
        } catch (Exception $e) {
            $this->Progress->error($e);
            $this->log(
                'Fusion :' . $e->getMessage() . __(' Fichier ')
                . $e->getFile() . __(' Ligne ') . $e->getLine(),
                'error'
            );
        }

        $this->autoRender = false;
        $this->response->type(['json' => 'text/x-json']);
        $this->RequestHandler->respondAs('json');
        $this->response->body(json_encode($this->Session->read('Progress.Tokens')));
        return $this->response;
    }

    /**
     * @access public
     * @param int $delib_id
     * @return CakeResponse|string
     */
    public function getBordereauTdt($delib_id)
    {
        $delib = $this->Deliberation->find('first', [
            'conditions' => ['id' => $delib_id],
            'fields' => ['num_delib', 'tdt_data_bordereau_pdf'],
            'recursive' => -1
        ]);

        if (empty($delib['Deliberation']['tdt_data_bordereau_pdf'])) {
            $this->Flash->set(
                __('le bordereau n\'est pas encore disponible'),
                ['element' => 'growl', 'params' => ['type' => 'warning']]
            );
            $this->redirect($this->previous);
        }
        // envoi au client
        $this->response->disableCache();
        $this->response->body($delib['Deliberation']['tdt_data_bordereau_pdf']);
        $this->response->type('application/pdf');
        $this->response->download('bordereau_tdt_' . $delib['Deliberation']['num_delib'] . '.pdf');
        return $this->response;
    }

    public function generationNumerotation()
    {
        $this->versioningEnable('Deliberation');

        if ($this->request->is('post')) {
            if (!empty($this->request->data['genereNumActe'])) {
                try {
                    $message = '';
                    foreach ($this->data['Deliberation'] as $id => $acte) {
                        if (!empty($acte['is_checked'])) {
                            $this->Deliberation->id = $id;
                            if (!empty($acte['num_pref'])) {
                                $this->Deliberation->saveField(
                                    'num_pref',
                                    str_replace('_', '.', $acte['num_pref'])
                                );
                            }

                            $generee = $this->Deliberation->genereNumeroActe($id, $this->Auth->user('id'), true);
                            $message .= __('Acte n° %s : ', $id) . ($generee
                                    ? __('numéro généré correctement')
                                    : __('le numéro existe déjà')) . '\n';
                        }
                    }

                    $this->Flash->set($message, ['element' => 'growl', 'params' => ['type' => 'info']]);
                    return $this->redirect($this->previous);
                } catch (Exception $e) {
                    $this->Flash->set(
                        $e->getMessage(),
                        ['element' => 'growl', 'params' => ['type' => 'danger']]
                    );
                    return $this->redirect($this->previous);
                }
            }
            $this->Flash->set(__('Veuillez sélectionner une action'), [
                'element' => 'growl',
                'params' => ['type' => 'danger']
            ]);
        }
    }
}
