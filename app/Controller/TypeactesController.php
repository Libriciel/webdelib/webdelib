<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [TypeactesController description]
 * @version 5.1.0
 */
class TypeactesController extends AppController
{
    /**
     * [public description]
     * @var [type]
     */
    public $name = 'Typeactes';
    /**
     * [public description]
     * @var [type]
     */
    public $uses = ['Typeacte', 'ModelOdtValidator.Modeltemplate', 'Compteur', 'Nature'];
    /**
     * [public description]
     * @var [type]
     */
    public $components = ['Email', 'Conversion', 'Progress', 'S2low', 'ModelOdtValidator.Fido', 'SabreDav', 'Paginator',
        'Auth' => [
            'mapActions' => [
                'create' => ['admin_add'],
                'read' => ['admin_index', 'admin_view', 'admin_downloadGabarit'],
                'update' => ['admin_edit', 'admin_deleteGabarit'],
                'delete' => ['admin_delete'],
            ]
        ]
    ];

    /**
     * @version 4.3
     * @access public
     */
    public function admin_index() // phpcs:ignore
    {
        $this->index();
    }

    /**
     * @version 5.1
     * @since 4.3
     * @access private
     * @param string $allow
     */
    private function index($allow = null)
    {
        $this->paginate = [
            'countField' => 'DISTINCT Typeacte.id',
            'fields' => ['Typeacte.id', 'Typeacte.name', 'Typeacte.teletransmettre'],
            'contain' => [
                'Nature' => ['fields' => ['name']],
                'Compteur' => ['fields' => ['nom']],
                'Modelprojet' => ['fields' => ['name']],
                'Modeldeliberation' => ['fields' => ['name']],
                'Modelbordereau' => ['fields' => ['name']]],
            'order' => ['Typeacte.name' => 'ASC'],
            'limit' => 20,
            'recursive' => -1,
            'allow' => $allow,
        ];

        $typeactes = $this->Paginator->paginate('Typeacte');

        for ($i = 0; $i < count($typeactes); $i++) {
            $typeactes[$i]['Typeacte']['is_deletable'] = $this->Typeacte->isDeletable($typeactes[$i]['Typeacte']['id']);
        }
        $this->set('typeactes', $typeactes);

        $this->render('index');
    }

    /**
     * @version 4.3
     * @access public
     */
    public function admin_view($id) // phpcs:ignore
    {
        $this->view($id);
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     * @param type $allow
     */
    private function view($id = null, $allow = null)
    {
        $typeacte = $this->Typeacte->find('first', ['conditions' => ['Typeacte.id' => $id],
            'contain' => ['Nature.name',
                'Compteur.nom',
                'Modelprojet.name',
                'Modeldeliberation.name'],
            'recursive' => -1,
            'allow' => $allow
        ]);
        if (empty($typeacte)) {
            $this->Flash->set(
                __('ID invalide pour le type d\'acte.'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            $this->redirect($this->previous);
        }
        $this->set('typeacte', $typeacte);

        $this->render('view');
    }

    /**
     * @version 4.3
     * @access public
     */
    public function admin_add() // phpcs:ignore
    {
        $this->add();
    }

    /**
     * @version 4.3
     * @access private
     */
    private function add()
    {
        if (!empty($this->data)) {
            $this->Typeacte->set($this->request->data);
            if ($this->Typeacte->validates()) {
                if (isset($this->request->data['Typeacte']['gabarit_projet_upload'])) {
                    $this->gabaritProjetUpload($this->request->data['Typeacte']['gabarit_projet_upload']);
                }
                if (isset($this->request->data['Typeacte']['gabarit_synthese_upload'])) {
                    $this->gabaritSyntheseUpload($this->request->data['Typeacte']['gabarit_synthese_upload']);
                }
                if (isset($this->request->data['Typeacte']['gabarit_acte_upload'])) {
                    $this->gabaritActeUpload($this->request->data['Typeacte']['gabarit_acte_upload']);
                }

                if (empty($this->Typeacte->validationErrors) && $this->Typeacte->save($this->request->data)) {
                    $this->Flash->set(
                        __('Le type d\'acte \' %s \' a été sauvegardé', $this->data['Typeacte']['name']),
                        ['element' => 'growl']
                    );
                    $this->redirect($this->previous);
                } else {
                    $this->Flash->set(
                        __('Veuillez corriger les erreurs ci-dessous.'),
                        ['element' => 'growl', 'params' => ['type' => 'danger']]
                    );
                }
            }
        }
        $this->set('compteurs', $this->Typeacte->Compteur->find('list'));
        $this->set('models_projet', $this->Modeltemplate->getModels(MODELE_TYPE_PROJET, true));
        $this->set(
            'models_docfinal',
            $this->Modeltemplate->getModels([MODELE_TYPE_PROJET, MODELE_TYPE_DELIBERATION], true)
        );
        $this->set('models_bordereau', $this->Modeltemplate->getModels(MODELE_TYPE_BORDEREAU_PROJET, true));
        $this->set('natures', $this->generateList('Nature.name'));
        $this->set('selectedNatures', null);

        $this->render('edit');
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     */
    public function admin_edit($id) // phpcs:ignore
    {
        $this->edit($id);
    }

    /**
     * @version 4.3
     * @access private
     * @param type $id
     * @param type $allow
     */
    private function edit($id, $allow = null)
    {
        //FIX
        $typeacte = $this->Typeacte->find('first', [
                'conditions' => ['Typeacte.id' => $id],
                'allow' => $allow,
            'recursive' => -1,
                ]);

        if (empty($typeacte)) {
            $this->Flash->set(
                __('Type d\'acte introuvable.'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            $this->redirect($this->previous);
        }

        if ($this->request->is('Put')) {
            if ($this->Typeacte->validates()) {
                if (isset($this->request->data['Typeacte']['gabarit_projet_upload'])) {
                    $this->gabaritProjetUpload($this->request->data['Typeacte']['gabarit_projet_upload']);
                }
                if (isset($this->request->data['Typeacte']['gabarit_synthese_upload'])) {
                    $this->gabaritSyntheseUpload($this->request->data['Typeacte']['gabarit_synthese_upload']);
                }
                if (isset($this->request->data['Typeacte']['gabarit_acte_upload'])) {
                    $this->gabaritActeUpload($this->request->data['Typeacte']['gabarit_acte_upload']);
                }

                foreach (['projet', 'synthese', 'acte'] as $gabarit) {
                    if (!empty($this->data['Typeacte']['file_gabarit_' . $gabarit])) {
                        $file = $this->SabreDav->getFileDav($this->data['Typeacte']['file_gabarit_' . $gabarit]);
                        $this->request->data['Typeacte']['gabarit_' . $gabarit] = $file->read();
                    }
                }

                if (empty($this->Typeacte->validationErrors) && $this->Typeacte->save($this->data)) {
                    $this->Flash->set(
                        __('Le type d\'acte \' %s \' a été modifié', $this->data['Typeacte']['name']),
                        ['element' => 'growl']
                    );
                    $this->redirect($this->previous);
                } else {
                    $this->Flash->set(
                        __('Veuillez corriger les erreurs ci-dessous.'),
                        ['element' => 'growl', 'params' => ['type' => 'danger']]
                    );
                    if (array_key_exists('Typeacteur', $this->data)) {
                        $this->set('selectedTypeacteurs', $this->data['Typeacteur']['Typeacteur']);
                        $this->set('selectedActeurs', $this->data['Acteur']['Acteur']);
                    } else {
                        $this->set('selectedTypeacteurs', null);
                        $this->set('selectedActeurs', null);
                    }
                }
            }
        }
        if (!$this->request->data) {
            $this->request->data = $typeacte;
        } else {
            $this->request->data['Typeacte']['gabarit_projet_name'] = $typeacte['Typeacte']['gabarit_projet_name'];
            $this->request->data['Typeacte']['gabarit_projet'] = $typeacte['Typeacte']['gabarit_projet'];
            $this->request->data['Typeacte']['gabarit_synthese'] = $typeacte['Typeacte']['gabarit_synthese'];
            $this->request->data['Typeacte']['gabarit_acte'] = $typeacte['Typeacte']['gabarit_acte'];
        }
        $this->set('selectedNatures', $this->request->data['Typeacte']['nature_id']);
        $this->Typeacte->recursive = -1;
        $this->set('compteurs', $this->Typeacte->Compteur->find('list'));
        $this->set('models_projet', $this->Modeltemplate->getModels(MODELE_TYPE_PROJET, true));
        $this->set(
            'models_docfinal',
            $this->Modeltemplate->getModels([MODELE_TYPE_PROJET, MODELE_TYPE_DELIBERATION], true)
        );
        $this->set('models_bordereau', $this->Modeltemplate->getModels(MODELE_TYPE_BORDEREAU_PROJET, true));
        $this->set('natures', $this->generateList('Nature.name'));

        $this->set('typeacte_id', $id);
        if (!empty($this->request->data['Typeacte']['gabarit_projet'])) {
            $this->SabreDav->start();
            $this->set(
                'file_gabarit_projet',
                $this->SabreDav->newFileDav(
                    $this->request->data['Typeacte']['gabarit_projet_name'],
                    $this->request->data['Typeacte']['gabarit_projet']
                )
            );
            $this->set('file_url_gabarit_projet', $this->SabreDav->newFileDavUrl());
        }
        if (!empty($this->request->data['Typeacte']['gabarit_synthese'])) {
            $this->SabreDav->start();
            $this->set(
                'file_gabarit_synthese',
                $this->SabreDav->newFileDav(
                    $this->request->data['Typeacte']['gabarit_synthese_name'],
                    $this->request->data['Typeacte']['gabarit_synthese']
                )
            );
            $this->set('file_url_gabarit_synthese', $this->SabreDav->newFileDavUrl());
        }
        if (!empty($this->request->data['Typeacte']['gabarit_acte'])) {
            $this->SabreDav->start();
            $this->set(
                'file_gabarit_acte',
                $this->SabreDav->newFileDav(
                    $this->request->data['Typeacte']['gabarit_acte_name'],
                    $this->request->data['Typeacte']['gabarit_acte']
                )
            );
            $this->set('file_url_gabarit_acte', $this->SabreDav->newFileDavUrl());
        }

        $this->render('edit');
    }

    /**
     * @access public
     * @param type $id
     */
    public function admin_delete($id) // phpcs:ignore
    {
        $this->delete($id);
    }

    /**
     * @version 4.3
     * @access private
     * @param type $id
     */
    private function delete($id = null)
    {
        $typeacte = $this->Typeacte->find('first', [
            'fields' => ['id', 'name'],
            'conditions'=> [
                'Typeacte.id' => $id
            ],
        ]);
        if (empty($typeacte)) {
            $this->Flash->set(
                __('Type d\'acte introuvable'),
                ['element' => 'growl','params'=>['type' => 'danger']]
            );
        } elseif (!$this->Typeacte->isDeletable($id)) {
            $this->Flash->set(
                __(
                    'Le type d\'acte \'%s\' ne peut pas être supprimé car il est utilisé par un acte',
                    $typeacte['Typeacte']['name']
                ),
                ['element' => 'growl','params' => ['type' => 'danger']]
            );
        } elseif ($this->Typeacte->delete($id)) {
            $this->Flash->set(
                __('Le type d\'acte \'%s\' a été supprimé', $typeacte['Typeacte']['name']),
                ['element' => 'growl','params' => ['type' => 'success']]
            );
        } else {
            $this->Flash->set(
                __(
                    'Erreur lors de la tentative de suppression du type d\'acte %s',
                    $typeacte['Typeacte']['name']
                ),
                ['element' => 'growl','params' => ['type' => 'danger']]
            );
        }

        $this->redirect($this->previous);
    }

    /**
     * @access public
     * @param type $id
     * @param type $type
     */
    public function admin_deleteGabarit($id, $type = null) // phpcs:ignore
    {
        $this->deleteGabarit($id, $type);
    }

    /**
     * @version 4.3
     * @access private
     * @param type $id
     * @param string $type
     * @return type
     */
    private function deleteGabarit($id, $type = null)
    {
        $type = 'gabarit_' . $type;
        $this->Typeacte->id = $id;
        $typeacte = $this->Typeacte->find('first', ['recursive' => -1,
            'conditions' => ['Typeacte.id' => $id],
            'fields' => ['Typeacte.name']
        ]);
        $data = ['name' => $typeacte['Typeacte']['name'],
            $type => '',
            $type . '_name' => ''
            ];
        if ($this->Typeacte->save($data, false)) {
            $this->Flash->set(__('Fichier supprimé !'), ['element' => 'growl']);
            return $this->redirect($this->previous);
        } else {
            $this->Flash->set(
                'Problème survenu lors de la suppression des débats généraux',
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            return $this->redirect($this->here);
        }
    }

    /**
     * @access public
     * @param int $id
     * @param int $type
     */
    public function admin_downloadGabarit($id, $type = null) // phpcs:ignore
    {
        return $this->downloadGabarit($id, $type);
    }

    /**
     * @version 4.3
     * @access private
     * @param type $id
     * @param string $type
     * @return type
     */
    private function downloadGabarit($id, $type = null)
    {
        $type = 'gabarit_' . $type;
        $typeacte = $this->Typeacte->find('first', ['recursive' => -1,
            'conditions' => ['Typeacte.id' => $id],
            'fields' => ['Typeacte.' . $type, 'Typeacte.' . $type . '_name']
        ]);
        if (!empty($typeacte['Typeacte'][$type])) {
            $this->response->disableCache();
            $this->response->body($typeacte['Typeacte'][$type]);
            $this->response->type('application/vnd.oasis.opendocument.text');
            $this->response->download($typeacte['Typeacte'][$type . '_name']);
            return $this->response;
        } else {
            $this->Flash->set(__('Type d\'acte introuvable'), ['element' => 'growl']);
            return $this->redirect(['controller' => 'typeactes', 'action' => 'edit', $id]);
        }
    }

    /**
     * Retourne la liste des natures [id]=>[libelle] pour utilisation html->selectTag
     *
     * @version 4.3
     * @access private
     * @param string $order_by
     * @return array
     * @access private
     */
    private function generateList($order_by = null)
    {
        $generateList = [];

        if ($order_by == null) {
            $natures = $this->Nature->find('all', [
                'fields' => 'id, name',
                'recursive' => -1
                ]);
        } else {
            $natures = $this->Nature->find('all', [
                'fields' => 'id, name',
                'order' => $order_by . ' DESC',
                'recursive' => -1]);
        }

        foreach ($natures as $nature) {
            $generateList[$nature['Nature']['id']] = $nature['Nature']['name'];
        }
        return $generateList;
    }

    /**
     * @access private
     * @param type $gabarit_projet_upload
     */
    private function gabaritProjetUpload($gabarit_projet_upload)
    {
        if (!empty($gabarit_projet_upload) && $gabarit_projet_upload['error'] !== 4) {
            if (strlen($gabarit_projet_upload['name']) > 75) {
                $this->Typeacte->invalidate(
                    'gabarit_projet_upload',
                    __('Nom de fichier invalide : maximum 75 caractères')
                );
            }
            $this->request->data['Typeacte']['gabarit_projet'] = file_get_contents($gabarit_projet_upload['tmp_name']);
            $this->request->data['Typeacte']['gabarit_projet_name'] = $gabarit_projet_upload['name'];
        }
    }

    /**
     * @access public
     * @param type $gabarit_synthese_upload
     */
    private function gabaritSyntheseUpload($gabarit_synthese_upload)
    {
        if (!empty($gabarit_synthese_upload) && $gabarit_synthese_upload['error'] !== 4) {
            if (strlen($gabarit_synthese_upload['name']) > 75) {
                $this->Typeacte->invalidate(
                    'gabarit_synthese_upload',
                    __('Nom de fichier invalide : maximum 75 caractères')
                );
            }
            $this->request->data['Typeacte']['gabarit_synthese'] =
                file_get_contents($gabarit_synthese_upload['tmp_name']);
            $this->request->data['Typeacte']['gabarit_synthese_name'] = $gabarit_synthese_upload['name'];
        }
    }

    /**
     * @version 4.3
     * @access public
     * @param type $gabarit_acte_upload
     */
    private function gabaritActeUpload($gabarit_acte_upload)
    {
        if (!empty($gabarit_acte_upload) && $gabarit_acte_upload['error'] != 4) {
            if (strlen($gabarit_acte_upload['name']) > 75) {
                $this->Typeacte->invalidate(
                    'gabarit_acte_upload',
                    __('Nom de fichier invalide : maximum 75 caractères')
                );
            }
            $this->request->data['Typeacte']['gabarit_acte'] = file_get_contents($gabarit_acte_upload['tmp_name']);
            $this->request->data['Typeacte']['gabarit_acte_name'] = $gabarit_acte_upload['name'];
        }
    }
}
