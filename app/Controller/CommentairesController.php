<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class CommentairesController extends AppController
{
    /**
     * @param int $delibId
     */
    public function add($delibId)
    {
        if (isset($this->request->data['forceReturnAction'])
            && $this->request->data['forceReturnAction'] === 'traiter'
        ) {
            $this->previous = ['controller' => 'validations', 'action' => 'traiter', $delibId];
        }
        if (empty($delibId)) {
            $this->Flash->set(__('Identifiant de délibération introuvable.'), ['element' => 'growl']);
            $this->redirect($this->previous);
        }
        $this->set('delib_id', $delibId);
        if (!empty($this->request->data)) {
            $this->Commentaire->create();
            $this->request->data['Commentaire']['agent_id'] = $this->Auth->user('id');
            $this->request->data['Commentaire']['commentaire_auto'] = 0;
            if ($this->Commentaire->save($this->request->data)) {
                $this->Flash->set(
                    __('Le commentaire a été enregistré.'),
                    ['element' => 'growl']
                );
                $this->redirect($this->previous);
            }
        }
        $this->Flash->set(
            __('Le commentaire est vide.'),
            ['element' => 'growl','params'=>['type' => 'danger']]
        );

        $this->redirect($this->previous);
    }

    /**
     * @version 4.2
     * @access public
     * @param type $id
     * @param type $delib_id
     * @return type
     */
    public function edit($id = null, $delib_id = null)
    {
        if (empty($this->request->data)) {
            if (empty($id)) {
                $this->Flash->set(__('ID invalide pour le commentaire'), ['element' => 'growl']);
                return $this->redirect($this->previous);
            }
            $this->set('delib_id', $delib_id);
            $this->request->data = $this->Commentaire->find('first', [
                'conditions'=> [
                    'Commentaire.id' => $id
                ],
            ]);
        } else {
            $this->request->data['Commentaire']['agent_id'] = $this->Session->read('user.User.id');
            if ($this->Commentaire->save($this->request->data)) {
                return $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Veuillez corriger les erreurs ci-dessous.'),
                    ['element' => 'growl','params'=>['type' => 'danger']]
                );
            }
        }
    }

    /**
     * @param int $id
     * @param int $delibId
     */
    public function delete($id, $delibId)
    {
        if (!$id) {
            $this->Flash->set(__('Invalide id pour le commentaire'), ['element' => 'growl']);
            $this->redirect(['controller' => 'validations', 'action' => 'traiter', $delibId]);
        }
        if ($this->Commentaire->delete($id)) {
            $this->Flash->set(__('Commentaire supprimé !'), ['element' => 'growl']);
            $this->redirect(['controller' => 'validations', 'action' => 'traiter', $delibId]);
        }
    }

    /**
     * @version 4.2
     * @access public
     * @param int $id
     * @param int $delib_id
     */
    public function prendreEnCompte($id, $delibId)
    {
        if (!$id) {
            $this->Flash->set(__('Invalide id pour le commentaire'), ['element' => 'growl']);
            $this->redirect(['controller' => 'validations', 'action' => 'traiter', $delibId]);
        }
        $this->request->data = $this->Commentaire->find('first', [
            'conditions'=> [
                'Commentaire.id' => $id
            ],
        ]);
        $this->request->data['Commentaire']['pris_en_compte'] = 1;
        if ($this->Commentaire->save($this->request->data)) {
            $this->redirect(['controller' => 'validations', 'action' => 'traiter', $delibId]);
            $this->Flash->set(__('Commentaire pris en compte'), ['element' => 'growl']);
        }
    }
}
