<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class TypeseancesController extends AppController
{
    public $name = 'Typeseances';
    public $uses = ['Typeseance', 'Typeacte', 'ModelOdtValidator.Modeltemplate', 'Compteur'];
    public $components = [
        'Auth' => [
            'mapActions' => [
                'create' => ['admin_add'],
                'read' => ['admin_index', 'admin_view'],
                'update' => ['admin_edit'],
                'delete' => ['admin_delete'],
            ]
        ]
    ];

    /**
     * @version 4.3
     * @access public
     */
    public function admin_index() // phpcs:ignore
    {
        $typeseances = $this->Typeseance->find('all', [
            'contain' => ['Modele_pvdetaille.name', 'Modele_pvdetaille.id',
                'Modele_pvsommaire.name', 'Modele_pvsommaire.id',
                'Modele_ordredujour.name', 'Modele_ordredujour.id',
                'Modele_convocation.name', 'Modele_convocation.id',
                'Modele_deliberation.name', 'Modele_deliberation.id',
                'Modele_journal_seance.name', 'Modele_journal_seance.id',
                'Modele_projet.name', 'Modele_projet.id',
                'Compteur.id', 'Compteur.nom', 'Acteur',
                'Typeacteur', 'Typeacte'
            ],
            'order' => ['libelle' => 'ASC']
        ]);
        for ($i = 0, $iMax = count($typeseances); $i < $iMax; $i++) {
            $typeseances[$i]['Typeseance']['is_deletable'] =
                $this->Typeseance->isDeletable($typeseances[$i]['Typeseance']['id']);
            $typeseances[$i]['Typeseance']['action'] =
                $this->Typeseance->libelleAction($typeseances[$i]['Typeseance']['action'], true);
        }
        $this->set('typeseances', $typeseances);

        $this->render('index');
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     */
    public function admin_view($id) // phpcs:ignore
    {
        $typeseance = $this->Typeseance->find('first', ['conditions' => ['Typeseance.id' => $id],
            'contain' => ['Modele_pvdetaille.name', 'Modele_pvdetaille.id',
                'Modele_pvsommaire.name', 'Modele_pvsommaire.id',
                'Modele_ordredujour.name', 'Modele_ordredujour.id',
                'Modele_convocation.name', 'Modele_convocation.id',
                'Modele_deliberation.name', 'Modele_deliberation.id',
                'Modele_journal_seance.name', 'Modele_journal_seance.id',
                'Modele_projet.name', 'Modele_projet.id',
                'Compteur.id', 'Compteur.nom', 'Acteur',
                'Typeacteur', 'Typeacte']]);
        $this->set('typeseance', $typeseance);

        $this->render('view');
    }

    /**
     * @version 4.3
     * @access public
     */
    public function admin_add() // phpcs:ignore
    {
        if (!empty($this->data)) {
            if ($this->Typeseance->save($this->data)) {
                $this->Flash->set(
                    __('Le type de séance "%s" a été sauvegardé', $this->data['Typeseance']['libelle']),
                    ['element' => 'growl']
                );
                return $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Veuillez corriger les erreurs ci-dessous.'),
                    ['element' => 'growl','params'=>['type' => 'danger']]
                );
            }
        }

        $this->set('compteurs', $this->Typeseance->Compteur->find('list'));

        //Modèles
        $this->set('models_projet', $this->Modeltemplate->getModels(MODELE_TYPE_PROJET, true));
        $this->set(
            'models_delib',
            $this->Modeltemplate->getModels([MODELE_TYPE_PROJET, MODELE_TYPE_DELIBERATION], true)
        );
        $this->set('models_convoc', $this->Modeltemplate->getModels(MODELE_TYPE_CONVOCATION, true));
        $this->set('models_odj', $this->Modeltemplate->getModels(MODELE_TYPE_ORDREDUJOUR, true));
        $this->set('models_pvdetaille', $this->Modeltemplate->getModels(MODELE_TYPE_PV_DETAILLE, true));
        $this->set('models_pvsommaire', $this->Modeltemplate->getModels(MODELE_TYPE_PV_SOMMAIRE, true));
        $this->set('models_journal_seance', $this->Modeltemplate->getModels(MODELE_TYPE_JOURNAL_SEANCE, true));

        $this->set('actions', [0 => $this->Typeseance->libelleAction(0, true),
            1 => $this->Typeseance->libelleAction(1, true),
            2 => $this->Typeseance->libelleAction(2, true)]);
        $this->set('typeacteurs', $this->Typeseance->Typeacteur->find('list', [
            'conditions' => ['actif' => true]
        ]));
        $this->set('selectedTypeacteurs', null);
        $this->set('acteurs', $this->Typeseance->Acteur->generateList('Acteur.nom'));
        $this->set('selectedActeurs', null);
        $this->set('typeActes', $this->Typeacte->find('list', [
            'fields' => ['Typeacte.name'],
            'conditions' => ['Typeacte.deliberant' => true]
        ]));
        $this->set('selectedTypeActes', null);
        $this->set('typeSeances', $this->Typeseance->find('list', [
            'fields' => ['Typeseance.libelle'],
            'conditions' => [
                'Typeseance.action' => 0,
            ]
        ]));
        $this->set('selectedTypeSeances', null);
        $this->set('disabledTypeSeances', null);

        $this->render('edit');
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     */
    public function admin_edit($id) // phpcs:ignore
    {
        if (!$id) {
            throw new NotFoundException(__('Id de type de séance invalide'));
        }

        $typeseance = $this->Typeseance->find('first', [
            'contain' => [
                'Modele_pvdetaille.name', 'Modele_pvdetaille.id',
                'Modele_pvsommaire.name', 'Modele_pvsommaire.id',
                'Modele_ordredujour.name', 'Modele_ordredujour.id',
                'Modele_convocation.name', 'Modele_convocation.id',
                'Modele_deliberation.name', 'Modele_deliberation.id',
                'Modele_journal_seance.name', 'Modele_journal_seance.id',
                'Modele_projet.name', 'Modele_projet.id',
                'Compteur.id', 'Compteur.nom',
                'Acteur',
                'Typeacteur',
                'Typeacte',
                'TypeseanceTypeseance'
            ],
            'conditions' => ['Typeseance.id' => $id],
            'recursive' => -1
        ]);

        if (!$typeseance) {
            $this->Flash->set(
                __("Le type de séance n° %s est introuvable !", $id),
                ['element' => 'growl']
            );
            $this->redirect($this->previous);
        }

        if ($this->request->is('Post')) {
            $this->Typeseance->id = $id;
            if ($this->Typeseance->save($this->data)) {
                $this->Flash->set(
                    __('Le type de séance "%s" a été modifié', $this->data['Typeseance']['libelle']),
                    ['element' => 'growl']
                );

                return $this->redirect($this->previous);
            } else {
                $this->Flash->set(__('Veuillez corriger les erreurs ci-dessous.'), [
                    'element' => 'growl',
                    'params'=>['type' => 'danger']]);
            }
        }
        if (!$this->request->data) {
            $this->request->data = $typeseance;
        }

        $this->set(
            'selectedTypeacteurs',
            $this->selectedArray(
                $this->data['Typeacteur']['Typeacteur'] ?? $this->data['Typeacteur']
            )
        );
        $this->set(
            'selectedActeurs',
            $this->selectedArray(
                $this->data['Acteur']['Acteur'] ?? $this->data['Acteur']
            )
        );
        $this->set(
            'selectedTypeActes',
            $this->selectedArray(
                $this->data['Typeacte']['Typeacte'] ?? $this->data['Typeacte']
            )
        );
        $this->set(
            'selectedTypeSeances',
            $this->data['Typeseance']['parent_id'] ?: null
        );

        // Si la séance n'est pas délibérante on ne peut pas sélectionner de types de séance liés
        $this->set('disabledTypeSeances', $this->data['Typeseance']['action'] === 0 ? true : null);

        $this->set('compteurs', $this->Typeseance->Compteur->find('list'));
        $this->set(
            'actions',
            [
                0 => $this->Typeseance->libelleAction(0, true),
                1 => $this->Typeseance->libelleAction(1, true),
                2 => $this->Typeseance->libelleAction(2, true)]
        );
        $this->set('typeacteurs', $this->Typeseance->Typeacteur->find('list'));
        $this->set('acteurs', $this->Typeseance->Acteur->generateList('Acteur.nom'));
        $this->set('typeActes', $this->Typeacte->find('list', [
            'fields' => ['Typeacte.name'],
            'conditions' => ['Typeacte.deliberant' => true]
        ]));

        $this->set('typeSeances', $this->Typeseance->find('list', [
            'fields' => ['Typeseance.libelle'],
            'conditions' => [
                'Typeseance.action' => 0,
            ]
        ]));


        //Modèles
        $this->set('models_projet', $this->Modeltemplate->getModels(MODELE_TYPE_PROJET, true));
        $this->set('models_delib', $this->Modeltemplate->getModels(
            [MODELE_TYPE_PROJET, MODELE_TYPE_DELIBERATION],
            true
        ));
        $this->set('models_convoc', $this->Modeltemplate->getModels(MODELE_TYPE_CONVOCATION, true));
        $this->set('models_odj', $this->Modeltemplate->getModels(MODELE_TYPE_ORDREDUJOUR, true));
        $this->set('models_pvdetaille', $this->Modeltemplate->getModels(MODELE_TYPE_PV_DETAILLE, true));
        $this->set('models_pvsommaire', $this->Modeltemplate->getModels(MODELE_TYPE_PV_SOMMAIRE, true));
        $this->set('models_journal_seance', $this->Modeltemplate->getModels(MODELE_TYPE_JOURNAL_SEANCE, true));


        $this->render('edit');
    }

    /**
     * @access public
     * @param type $id
     */
    public function admin_delete($id) // phpcs:ignore
    {
        $this->Typeseance->recursive = -1;
        $typeseance = $this->Typeseance->read('id, libelle', $id);
        if (empty($typeseance)) {
            $message = 'Type de séance introuvable';
        } elseif (!$this->Typeseance->isDeletable($id)) {
            $message =
                'Le type de séance \'' . $typeseance['Typeseance']['libelle']
                . '\' ne peut pas être supprimé car il est utilisé par une séance';
        } elseif ($this->Typeseance->delete($id)) {
            $message = 'Le type de séance \'' . $typeseance['Typeseance']['libelle'] . '\' a été supprimé';
        } else {
            $message =
                'Erreur lors de la tentative de suppression du type de séance '
                . $typeseance['Typeseance']['libelle'];
        }
        $this->Flash->set($message, ['element' => 'growl']);

        return $this->redirect($this->previous);
    }
}
