<?php

/**
 * Contrôleur des Listes de définition des Informations supplémentaires
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       web-delib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 * @version     v4.3
 * @package     app.Controller
 */
class InfosuplistedefsController extends AppController
{
    public $components = [
        'Filtre',
        'Auth' => [
            'mapActions' => [
                'read' => ['admin_index', 'admin_add', 'admin_edit', 'admin_delete', 'admin_view', 'admin_changerOrdre']
            ]
        ]
    ];

    /**
     *
     */
    public function beforeFilter()
    {
        $this->set('Infosuplistedef', $this->Infosuplistedef);
        parent::beforeFilter();
    }

    /**
     * Liste des éléments de la liste d'une information supplémentaire de type 'list'
     *
     * @param type $infosupdefId
     * @since   4.3
     * @access public
     * @version 5.1
     */
    public function admin_index($infosupdefId) // phpcs:ignore
    {
        $sortie = false;
        // lecture de l'infosup
        $infosupdef = $this->{$this->modelClass}->Infosupdef->find(
            'first',
            ['recursive' => -1, 'conditions' => ['id' => $infosupdefId]]
        );
        if (empty($infosupdef)) {
            $this->Flash->set(
                __('ID invalide pour l\'information supplémentaire : édition impossible'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            $sortie = true;
        } elseif (!in_array($infosupdef['Infosupdef']['type'], ['list', 'listmulti'])) {
            $this->Flash->set(
                __('Cette information supplémentaire n\'est pas de type liste : édition impossible'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            $sortie = true;
        }
        if ($sortie) {
            $this->redirect(['controller' => 'infosupdefs', 'action' => 'index']);
        } else {
            $this->set('infosupdef', $infosupdef);
            $this->Filtre->initialisation(
                $this->name . ':' . $this->request->action,
                $this->request->data,
                ['url' => ['controller' => $this->params['controller'], 'action' => $this->action, $infosupdefId]]
            );

            $conditions = $this->Filtre->conditions();
            $conditions['infosupdef_id'] = $infosupdefId;
            $this->request->data = $this->{$this->modelClass}->find(
                'all',
                ['recursive' => -1, 'conditions' => $conditions, 'order' => 'ordre']
            );
            if (!$this->Filtre->critereExists()) {
                $this->Filtre->addCritere(
                    'Actif',
                    [
                        'field' => 'Infosuplistedef.actif',
                        'inputOptions' => [
                            'label' => __('Actif'),
                            'empty' => __('Tous'),
                            'options' => [1 => __('Oui'), 0 => __('Non')]
                        ]
                    ]
                );
                $this->Filtre->setCritere('Actif', '');
            }
        }
    }

    /**
     * Ajoute un éléments à la liste de l'info. sup.
     *
     * @param type $infosupId
     * @since 4.3
     * @access public
     * @version 5.1
     */
    public function admin_add($infosupId = 0) // phpcs:ignore
    {
        if (empty($this->data)) {
            // recherche de l'infosupdef
            $infosupdef = $this->{$this->modelClass}->Infosupdef->find(
                'first',
                ['conditions' => ['Infosupdef.id' => $infosupId], 'fields' => ['id', 'type', 'nom'], 'recursive' => -1]
            );
            if (empty($infosupdef)) {
                $this->Flash->set(
                    __('ID invalide pour l\'information supplémentaire : édition impossible'),
                    ['element' => 'growl', 'params' => ['type' => 'danger']]
                );
                $redirect = ['controller' => 'infosupdefs', 'action' => 'index'];
                $sortie = true;
            } elseif (!in_array($infosupdef['Infosupdef']['type'], ['list', 'listmulti'])) {
                $this->Flash->set(
                    __('Cette information supplémentaire n\'est pas de type liste : édition impossible'),
                    ['element' => 'growl', 'params' => ['type' => 'danger']]
                );
                $this->redirect($this->previous);
            } else {
                // initialisations
                $this->request->data['Infosuplistedef']['infosupdef_id'] = $infosupId;
                $this->request->data['Infosuplistedef']['actif'] = true;
            }
        } else {
            $nb_liste = $this->Infosuplistedef->find(
                'count',
                ['conditions' => ['Infosuplistedef.infosupdef_id' => $infosupId], 'recursive' => -1]
            );
            $this->request->data['Infosuplistedef']['ordre'] = $nb_liste + 1;
            if ($this->Infosuplistedef->save($this->data['Infosuplistedef'])) {
                $this->Flash->set(
                    __('L\'élément "%s" a été ajouté.', $this->data['Infosuplistedef']['nom']),
                    ['element' => 'growl']
                );
                $this->redirect($this->previous);
            } else {
                $infosupdef = $this->{$this->modelClass}->Infosupdef->findById(
                    $this->data['Infosuplistedef']['infosupdef_id'],
                    null,
                    null,
                    -1
                );
                $this->Flash->set(
                    __('Veuillez corriger les erreurs ci-dessous.'),
                    ['element' => 'growl', 'params' => ['type' => 'danger']]
                );
            }
        }

        $this->set('infosupdef', $infosupdef);
        $this->render('admin_edit');
    }

    /**
     * Edition de l'élément $id de la liste d'une info supplémentaire
     *
     * @param type $id
     * @version 4.3
     * @access public
     */
    public function admin_edit($id = 0) // phpcs:ignore
    {
        if (empty($this->data)) {
            // lecture de l'infosuplistedef
            $this->data = $this->{$this->modelClass}->findById($id, null, null, -1);
            if (empty($this->data)) {
                $this->Flash->set(
                    __('ID invalide pour l\'élément de l\'information supplémentaire : édition impossible'),
                    ['element' => 'growl', 'params' => ['type' => 'danger']]
                );
                $this->redirect($this->previous);
            }
        } else {
            if ($this->{$this->modelClass}->save($this->data)) {
                $this->Flash->set(
                    __('L\'élément "%s" a été modifié', $this->data['Infosuplistedef']['nom']),
                    ['element' => 'growl']
                );
                $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Veuillez corriger les erreurs ci-dessous.'),
                    ['element' => 'growl', 'params' => ['type' => 'danger']]
                );
            }
        }

        $this->set('infosupdef', $this->{$this->modelClass}->Infosupdef->findById(
            $this->data['Infosuplistedef']['infosupdef_id'],
            null,
            null,
            -1
        ));
    }

    /**
     * Supprime l'élément $id de la liste d'une info supplémentaire
     *
     * @param int $id
     * @version 4.3
     * @access public
     */
    public function admin_delete($id = 0) // phpcs:ignore
    {
        $data = $this->{$this->modelClass}->find('first', ['recursive' => -1, 'conditions' => ['id' => $id]]);
        if (empty($data)) {
            $this->Flash->set(
                __('ID invalide pour l\'élément de l\'information supplémentaire : suppression impossible'),
                ['element' => 'growl']
            );
        } elseif (!$this->{$this->modelClass}->isDeletable($id)) {
            $this->Flash->set(
                __('Cet élément de l\'information supplémentaire ne peut pas être supprimé'),
                ['element' => 'growl']
            );
        } elseif ($this->{$this->modelClass}->delete($id)) {
            $this->{$this->modelClass}->reOrdonne($data['Infosuplistedef']['infosupdef_id']);
            $this->Flash->set(
                __('L\'information supplémentaire "%s" a été supprimée', $data['Infosuplistedef']['nom']),
                ['element' => 'growl', 'params' => ['type' => 'success']]
            );
        } else {
            $this->Flash->set(
                __('Erreur lors de la suppression.'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
        }

        $this->redirect($this->previous);
    }

    /**
     * Intervertit l'élément de la liste $id avec son suivant ou son précédent
     *
     * @param type $id
     * @param type $suivant
     * @version 4.3
     * @access public
     */
    public function admin_changerOrdre($id = null, $suivant = true) // phpcs:ignore
    {
        $data = $this->{$this->modelClass}->find('first', ['recursive' => -1, 'conditions' => ['id' => $id]]);
        if (empty($data)) {
            $this->Flash->set(
                __('ID invalide : déplacement impossible.'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
        } else {
            $this->{$this->modelClass}->invert($id, $suivant);
        }

        $this->redirect(
            ['controller' => 'infosuplistedefs', 'action' => 'index', $data['Infosuplistedef']['infosupdef_id']]
        );
    }
}
