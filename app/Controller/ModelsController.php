<?php

/**
 * Contrôleur des Modèles
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       web-delib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 * @version     v4.3
 * @package     app.Controller
 */
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('AppTools', 'Lib');

class ModelsController extends AppController
{
    public $uses = ['Modeltemplate', 'Modeledition'];

    /**
     * @version 4.3
     * @access public
     * @var type
     */
    public $components = [
        'RequestHandler',
        'Progress',
        'Email',
        'Auth' => [
            'mapActions' => [
                'read' => ['changeStatus'],
                'allow' => ['genereToken', 'getProgress', 'paramMails', 'index', 'getProgressDocument']
            ]
        ]
    ];

    /**
     * @version 4.3
     * @access public
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow();
    }

    /**
     * @param type $id
     * @return false|string|type
     * @access private
     */
    private function _getFileName($id = null) // phpcs:ignore
    {
        $objCourant = $this->Modeltemplate->find(
            'first',
            ['conditions' => ['Modeltemplate.id' => $id], 'recursive' => -1, 'fields' => 'filename']
        );
        return utf8_encode($objCourant['Modeltemplate']['filename']);
    }

    /**
     * @access private
     * @param type $id
     * @return type
     */
    private function _getSize($id = null) // phpcs:ignore
    {
        $objCourant = $this->Modeltemplate->find(
            'first',
            ['conditions' => ['Modeltemplate.id' => $id], 'recursive' => -1, 'fields' => 'filesize']
        );
        return $objCourant['Modeltemplate']['filesize'];
    }

    /**
     * @version 4.3
     * @access private
     * @param type $id
     * @return type
     */
    private function _getModel($id = null) // phpcs:ignore
    {
        $objCourant = $this->Modeltemplate->find(
            'first',
            ['conditions' => ['Modeltemplate.id' => $id], 'recursive' => -1, 'fields' => 'content']
        );
        return $objCourant['Modeltemplate']['content'];
    }

    /**
     * @version 4.3
     * @access public
     */
    public function genereToken()
    {
        if ($this->request->is('ajax')) {
            $json = ['downloadToken' => $this->Session->read('Generer.downloadToken')];

            $this->autoRender = false;
            $this->response->type(['json' => 'text/x-json']);
            $this->RequestHandler->respondAs('json');
            $this->response->body(json_encode($json));
            return $this->response;
        }
    }

    /**
     * @version 4.4
     * @access public
     */
    public function getProgressDocument($cookieTokenKey)
    {
        $this->Progress->clear($cookieTokenKey);
        $tokens = $this->Session->read('Progress.Tokens');

        if (empty($cookieTokenKey)) {
            CakeLog::error(__('Impossible de recupérer le fichier, le "cookieTokenKey" est vide'));
            $this->response->statusCode(404);
            return $this->response;
        }

        // fusion du document
        $folderTmp = new Folder(TMP . 'files' . DS . 'progress' . DS . $cookieTokenKey);
        $file_generation = new File($folderTmp->pwd() . DS . $tokens[$cookieTokenKey]['filename']);
        $file_generation->close();

        $this->response->disableCache();
        $this->response->body($file_generation->read());
        $this->response->type($tokens[$cookieTokenKey]['typemime']);
        $this->response->download($tokens[$cookieTokenKey]['filename']);

        //Suppression du dossier temporaire
        if (!$folderTmp->delete()) {
            CakeLog::error(
                __('Impossible de supprimer le répertoire du "Token" de génération :')
                . "\n"
                . var_export($folderTmp->messages(), true)
            );
        }

        return $this->response;
    }

    /**
     * @version 4.3
     * @access public
     */
    public function getProgress($key)
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;

            $this->response->type(['json' => 'text/x-json']);
            $this->RequestHandler->respondAs('json');
            $tokens = $this->Session->read('Progress.Tokens');
            $json = [];
            if (!empty($tokens) && array_key_exists($key, $tokens)) {
                $json = [$key => $tokens[$key]];
            } else {
                $json = [
                    $key => [
                        'status'=> ProgressComponent::STATUS_FAILED,
                        'debug' => var_export($tokens, true),
                        'error'=> ['code' => 404, 'message' => 'Ressource non trouvée']
                    ]
                ];
            }

            $this->autoRender = false;
            $this->response->type(['json' => 'text/x-json']);
            $this->RequestHandler->respondAs('json');
            $this->response->body(json_encode($json));
            return $this->response;
        }
    }

    /**
     * @access private
     * @param type $acteur
     * @param type $fichier
     * @param type $path
     */
    private function _sendDocument($acteur, $fichier, $path) // phpcs:ignore
    {
        if ($this->Auth->User('formatSortie') === 'pdf') {
            $format = ".pdf";
        } else {
            $format = ".odt";
        }
        if ($acteur['email'] != '') {
            if (Configure::read("SMTP_USE")) {
                $this->Email->smtpOptions = [
                    'port' => Configure::read("SMTP_PORT"),
                    'timeout' => Configure::read("SMTP_TIMEOUT"),
                    'host' => Configure::read("SMTP_HOST"),
                    'username' => Configure::read("SMTP_USERNAME"),
                    'password' => Configure::read("SMTP_PASSWORD"),
                    'client' => Configure::read("SMTP_CLIENT")
                ];
                $this->Email->delivery = 'smtp';
            } else {
                $this->Email->delivery = 'mail';
            }

            $this->Email->from = Configure::read("MAIL_FROM");
            $this->Email->to = $acteur['email'];
            $this->Email->charset = 'UTF-8';

            $this->Email->subject = utf8_encode("Vous venez de recevoir un document de Webdelib ");
            $this->Email->sendAs = 'text';
            $this->Email->layout = 'default';
            $this->Email->template = 'convocation';
            $this->set('data', $this->paramMails('convocation', $acteur));

            $this->Email->attachments = [$path . $fichier . $format];
            $this->Email->send();
        }
    }

    /**
     * @param type $type
     * @param type $acteur
     * @return false|string|type
     * @access public
     */
    public function paramMails($type, $acteur)
    {
        $handle = fopen(APP . DS . 'Config' . DS . 'emails/' . $type . '.txt', 'r');
        $content = fread($handle, filesize(APP . DS . 'Config' . DS . 'emails/' . $type . '.txt'));
        $searchReplace = ["#NOM#" => $acteur['nom'], "#PRENOM#" => $acteur['prenom']];
        return utf8_encode(
            nl2br((str_replace(array_keys($searchReplace), array_values($searchReplace), $content)))
        );
    }

    /**
     * @param type $field
     * @param type $id
     * @access public
     */
    public function changeStatus($field, type $id)
    {
        $data = $this->Modeledition->find(
            'first',
            ['conditions' => ["Modeledition.id" => $id], 'recursive' => -1, 'fields' => ["$field"]]
        );
        $this->Modeledition->id = $id;
        if ($data['Modeledition'][$field] == 0) {
            $this->Modeledition->saveField($field, 1);
        } elseif ($data['Modeledition'][$field] == 1) {
            $this->Modeledition->saveField($field, 0);
        }
        $this->Flash->set(__('Modification prise en compte'), ['element' => 'growl']);
        $this->redirect('/models/index');
    }
}
