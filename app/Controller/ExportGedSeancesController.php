<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppController', 'Controller');
App::uses('AppTools', 'Lib');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Controller export GED des séances
 *
 * @package app.Controller
 * @version 5.1.0
 */
class ExportGedSeancesController extends AppController
{
    /** @var Array Contains a uses */
    public $uses = [
        'Seance',
        'Deliberation',
        'Nature'
    ];

    /** @var Array Contains a components */
    public $components = [
        'RequestHandler',
        'Progress',
        'Conversion',
        'ExportGedCMIS',
        'Auth' => [
            'mapActions' => [
                'allow' => ['sendToGed']
            ],
        ]];

    /**
     * Envoi pour une GED en protocole CMIS
     *
     * @version 5.1
     * @access public
     * @param type $seance_id
     */
    public function sendToGed($seance_id, $cookieTokenKey)
    {
        $this->Progress->start($cookieTokenKey);
        $folderSend = new Folder(
            AppTools::newTmpDir(TMP . 'files' . DS . 'export' . DS),
            true,
            0777
        );

        try {
            CakeLog::info(__('[Ged][%s]cmisService: Start', __METHOD__), 'connector');
            $this->ExportGedCMIS->service($folderSend);

            CakeLog::info(
                __(
                    '[Ged][%s]Version d\'export v%s',
                    __METHOD__,
                    Configure::read('GED_XML_VERSION')
                ),
                'connector'
            );
            $cmisFolder = $this->{'sendToGedVersion' . Configure::read('GED_XML_VERSION')}($seance_id);

            $this->Progress->at(90, __('Envoi des fichiers'));
            $this->ExportGedCMIS->send($folderSend, $cmisFolder);

            $folderSend->delete();
            CakeLog::info(__('[Ged][%s]cmisService: Send ok', __METHOD__), 'connector');
            sleep(3);
            $this->Progress->redirect($this->previous);
            $this->Progress->stop(__('Opération terminée'));
        } catch (Exception $e) {
            $message = '';
            switch ($e->getCode()) {
                case 500:
                    $message = 'Erreur interne';
                    break;
                case 404:
                    $message = 'Ressource non trouvée';
                    break;
                case 409:
                    $message = 'Conflict, la séance existe déjà dans la GED';
                    break;
                default:
                    $message = strip_tags($e->getMessage());
                    break;
            }

            CakeLog::error(
                __(
                    '[Ged][%s]Send error(%s)',
                    __METHOD__,
                    $e->getCode()
                ) . "\n" . $e->getMessage(),
                'connector'
            );
            $this->Progress->error(new Exception(__('[Ged]') . ' ' . $message, $e->getCode()));
        }

        $this->autoRender = false;
        $this->response->type(['json' => 'text/x-json']);
        $this->RequestHandler->respondAs('json');
        $this->response->body(json_encode($this->Session->read('Progress.Tokens')));
        return $this->response;
    }

    /**
     * @version 5.1
     * @access private
     * @param type $seance_id
     * @return type
     */
    private function sendToGedVersion1($seance_id)
    {
        // Création du répertoire de séance
        $result = $this->ExportGedCMIS->client->getFolderTree($this->ExportGedCMIS->folder->id, 1);

        $this->Progress->at(0, __('Création du répertoire de séance...'));

        $seance = $this->Seance->find('first', [
            'conditions' => ['Seance.id' => $seance_id],
            'contain' => ['Typeseance.libelle', 'Typeseance.modele_projet_id']]);

        $this->Progress->at(5, __('Recherche de la séance en base de données...'));

        $date_seance = $seance['Seance']['date'];
        $date_convocation = $seance['Seance']['date_convocation'];
        $type_seance = $seance['Typeseance']['libelle'];
        $libelle_seance =
            $seance['Typeseance']['libelle'] . " " . CakeTime::i18nFormat(
                $seance['Seance']['date'],
                '%A %d %B %Y à %k h %M'
            );

        $this->ExportGedCMIS->deletetoGed(Configure::read('CMIS_REPO') . '/' . $libelle_seance);

        $zip = new ZipArchive();
        $zip->open($this->ExportGedCMIS->folderSend->pwd() . DS . 'documents.zip', ZipArchive::CREATE);
        $this->Progress->at(15, __('Création des dossiers...'));
        // Création des dossiers
        $my_seance_folder = $this->ExportGedCMIS->client->createFolder(
            $this->ExportGedCMIS->folder->id,
            $libelle_seance
        );
        // Création des dossiers vides
        $zip->addEmptyDir('Rapports');
        $zip->addEmptyDir('Annexes');

        $delibs_id = $this->Seance->getDeliberationsId($seance_id);
        //$this->log(var_export($delibs_id, true), 'debug');

        $this->Progress->at(20, 'Création du fichier XML...');
        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->formatOutput = true;
        $idDepot = $seance['Seance']['numero_depot'] + 1;

        $dom_depot = $this->ExportGedCMIS->createElement($dom, 'depot', null, [
            'idDepot' => $idDepot,
            'xmlns:webdelibdossier' => 'http://www.adullact.org/webdelib/infodossier/1.0',
            'xmlns:xm' => 'http://www.w3.org/2005/05/xmlmime']);

        $dom_seance = $this->ExportGedCMIS->createElement(
            $dom,
            'seance',
            null,
            ['idSeance' => $seance_id]
        );
        $dom_seance->appendChild($this->ExportGedCMIS->createElement($dom, 'typeSeance', $type_seance));
        $dom_seance->appendChild($this->ExportGedCMIS->createElement($dom, 'dateSeance', $date_seance));
        $dom_seance->appendChild($this->ExportGedCMIS->createElement($dom, 'dateConvocation', $date_convocation));

        //Noeud document[convocation]
        $this->Progress->at(25, __('Génération de la convocation...'));
        $document = $this->ExportGedCMIS->createElement(
            $dom,
            'document',
            null,
            ['nom' => 'convocation.pdf', 'type' => 'convocation']
        );
        $document->appendChild($this->ExportGedCMIS->createElement($dom, 'mimetype', 'application/pdf'));
        $document->appendChild($this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8'));
        $dom_seance->appendChild($document);
        // fusion du document de convocation
        $zip->addFromString('convocation.pdf', $this->Seance->fusion($seance_id, 'convocation'));

        //Noeud document[odj]
        $this->Progress->at(40, __('Génération de l\'ordre du jour...'));
        $document = $this->ExportGedCMIS->createElement(
            $dom,
            'document',
            null,
            ['nom' => 'odj.pdf', 'type' => 'odj']
        );
        $document->appendChild($this->ExportGedCMIS->createElement($dom, 'mimetype', 'application/pdf'));
        $document->appendChild($this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8'));
        $dom_seance->appendChild($document);
        // fusion du document de ordre du jour
        $zip->addFromString('odj.pdf', $this->Seance->fusion($seance_id, 'ordredujour'));

        //Noeud document[pv_sommaire]
        if (!empty($seance['Seance']['pv_sommaire'])) {
            $this->Progress->at(50, __('Ajout du PV sommaire...'));
            $document = $this->ExportGedCMIS->createElement(
                $dom,
                'document',
                null,
                ['nom' => 'pv.pdf', 'type' => 'pv_sommaire']
            );
            $document->appendChild($this->ExportGedCMIS->createElement($dom, 'mimetype', 'application/pdf'));
            $document->appendChild($this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8'));
            $dom_seance->appendChild($document);
            $zip->addFromString('pv.pdf', $seance['Seance']['pv_sommaire']);
        }

        //Noeud document[pv_complet]
        $this->Progress->at(55, 'Ajout du PV complet...');
        if (!empty($seance['Seance']['pv_complet'])) {
            $document = $this->ExportGedCMIS->createElement(
                $dom,
                'document',
                null,
                ['nom' => 'pvcomplet.pdf', 'type' => 'pv_complet']
            );
            $document->appendChild($this->ExportGedCMIS->createElement($dom, 'mimetype', 'application/pdf'));
            $document->appendChild($this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8'));
            $dom_seance->appendChild($document);
            $zip->addFromString('pvcomplet.pdf', $seance['Seance']['pv_complet']);
        }
        //Infos supps
        $this->Progress->at(60, __('Ajout des informations supplémentaires de séance...'));
        $this->ExportGedCMIS->createElementInfosupsByZip(
            $zip,
            $dom,
            $dom_seance,
            $seance_id,
            'Seance'
        );

        //Insertion du noeud xml seance
        $dom_depot->appendChild($dom_seance);

        $this->Progress->at(66, __('Ajout des délibérations...'));
        foreach ($delibs_id as $delib_id) {
            $doc = $this->ExportGedCMIS->createElement(
                $dom,
                'dossierActe',
                null,
                ['idActe' => $delib_id, 'refSeance' => $seance_id]
            );

            $delib = $this->Deliberation->find('first', [
                'conditions' => ['Deliberation.id' => $delib_id],
                'fields' => ['Deliberation.id', 'Deliberation.num_delib',
                    'Deliberation.objet_delib', 'Deliberation.titre',
                    'Deliberation.delib_pdf', 'Deliberation.tdt_data_pdf',
                    'Deliberation.tdt_data_bordereau_pdf', 'Deliberation.deliberation',
                    'Deliberation.deliberation_size', 'Deliberation.signature', 'Deliberation.tdt_ar_date'],
                'contain' => [
                    'Service' => ['fields' => ['name']],
                    'Theme' => ['fields' => ['libelle']],
                    'Typeacte' => ['fields' => ['nature_id']],
                    'Redacteur' => ['fields' => ['nom', 'prenom']],
                    'Rapporteur' => ['fields' => ['nom', 'prenom']],
                ]]);

            $nature = $this->Nature->find('first', [
                'fields' => ['name'],
                'conditions' => ['Nature.id' => $delib['Typeacte']['nature_id']]]);

            $doc->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'natureACTE',
                $nature['Nature']['name']
            ));
            $doc->appendChild($this->ExportGedCMIS->createElement($dom, 'dateACTE', $date_seance));
            $doc->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'numeroACTE',
                $delib['Deliberation']['num_delib']
            ));
            $doc->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'themeACTE',
                $delib['Theme']['libelle']
            ));
            $doc->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'emetteurACTE',
                $delib['Service']['name']
            ));
            $doc->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'redacteurACTE',
                $delib['Redacteur']['prenom'] . ' ' . $delib['Redacteur']['nom']
            ));
            $doc->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'rapporteurACTE',
                $delib['Rapporteur']['prenom'] . ' ' . $delib['Rapporteur']['nom']
            ));
            $doc->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'typeseanceACTE',
                $type_seance
            ));
            $doc->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'dateAR',
                $delib['Deliberation']['tdt_ar_date']
            )); // utile ??
            //<listeCommissions>
            $seances_id = $this->Deliberation->getSeancesid($delib_id);

            $listeCommissions = '';
            foreach ($seances_id as $commission_id) {
                if (!$this->Deliberation->Seance->isDeliberante($commission_id)) {
                    $typeSeance = $this->Deliberation->Seance->Typeseance->getLibelle(
                        $this->Deliberation->Seance->getType($commission_id)
                    );
                    $listeCommissions .=
                        $typeSeance . ' : ' . $this->Deliberation->Seance->getDate($commission_id) . ', ';
                }
            }

            $doc->appendChild($this->ExportGedCMIS->createElement($dom, 'listeCommissions', $listeCommissions));
            //</listeCommissions>
            //Infos supps de délibération
            $this->ExportGedCMIS->createElementInfosupsByZip($zip, $dom, $doc, $delib_id, 'Deliberation');

            //Noeud document[TexteActe]
            $delib_filename = $delib_id . '-' . $delib['Deliberation']['num_delib'] . '.pdf';
            $document = $this->ExportGedCMIS->createElement(
                $dom,
                'document',
                null,
                ['nom' => $delib_filename, 'relname' => $delib_filename, 'type' => 'TexteActe']
            );
            $document->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'titre',
                $delib['Deliberation']['objet_delib']
            ));
            $document->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'description',
                $delib['Deliberation']['titre']
            ));
            $document->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'mimetype',
                'application/pdf'
            ));
            $document->appendChild($this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8'));
            $doc->appendChild($document);
            //Ajout au zip
            $zip->addFromString(
                $delib_filename,
                (!empty($delib['Deliberation']['tdt_data_pdf']) ? $delib['Deliberation']['tdt_data_pdf']
                    : $delib['Deliberation']['delib_pdf'])
            );

            //Noeud document[Rapport]
            if (!empty($delib['Deliberation']['deliberation_size'])) {
                $document = $this->ExportGedCMIS->createElement(
                    $dom,
                    'document',
                    null,
                    ['nom' => $delib_filename, 'relname' => $delib_filename, 'type' => 'Rapport']
                );
                $document->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'titre',
                    $delib['Deliberation']['objet_delib']
                ));
                $document->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'description',
                    $delib['Deliberation']['titre']
                ));
                $document->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'mimetype',
                    'application/pdf'
                ));
                $document->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'encoding',
                    'utf-8'
                ));
                $doc->appendChild($document);
                // fusion du rapport et ajout au zip
                $zip->addFromString(
                    'Rapports' . DS . $delib_filename,
                    $this->Deliberation->fusion(
                        $delib['Deliberation']['id'],
                        'rapport',
                        $seance['Typeseance']['modelprojet_id']
                    )
                );
            }

            if (!empty($delib['Deliberation']['signature']) && $delib['Deliberation']['signature_type'] != 'PAdES') {
                //Ajout de la signature (XML+ZIP)
                $signatureName = $delib['Deliberation']['id'] . '-signature.zip';
                //Création du noeud XML
                $document = $this->ExportGedCMIS->createElement(
                    $dom,
                    'document',
                    null,
                    ['nom' => $signatureName, 'relname' => $signatureName, 'type' => 'Signature']
                );
                $document->appendChild(
                    $this->ExportGedCMIS->createElement($dom, 'mimetype', 'application/zip')
                );
                $doc->appendChild($document);
                //Ajout à l'archive
                $zip->addFromString('Annexes' . DS . $signatureName, $delib['Deliberation']['signature']);
            }

            //Ajout du bordereau (XML+ZIP)
            $bordereauName = $delib['Deliberation']['id'] . '-bordereau.pdf';
            //Création du noeud XML
            $document = $this->ExportGedCMIS->createElement(
                $dom,
                'document',
                null,
                ['nom' => $bordereauName, 'relname' => $bordereauName, 'type' => 'Bordereau']
            );
            $document->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'mimetype',
                'application/pdf'
            ));
            $doc->appendChild($document);
            //Ajout à l'archive
            $zip->addFromString(
                'Annexes' . DS . $bordereauName,
                $delib['Deliberation']['tdt_data_bordereau_pdf']
            );


            //Ajout des annexes
            $annexes_id = $this->Deliberation->Annexe->getAnnexesFromDelibId($delib_id, true);
            if (!empty($annexes_id)) {
                foreach ($annexes_id as $annex_id) {
                    $annex_id = $annex_id['Annexe']['id'];
                    $annex = $this->Deliberation->Annexe->find('first', [
                        'conditions' => ['Annexe.id' => $annex_id],
                        'fields' => [
                            'Annexe.id', 'Annexe.titre', 'Annexe.filename',
                            'Annexe.filetype', 'Annexe.data_pdf', 'Annexe.data'
                        ],
                        'recursive' => -1]);

                    switch ($annex['Annexe']['filetype']) {
                        case 'application/pdf':
                            $annexe_content = $annex['Annexe']['data'];
                            $annexe_filetype = 'application/pdf';
                            $annexe_filename = $annex['Annexe']['filename'];
                            break;

                        case 'application/vnd.oasis.opendocument.text':
                            $annexe_content = $this->Conversion->convertirFlux(
                                $annex['Annexe']['data'],
                                'odt',
                                'pdf'
                            );
                            $annexe_filetype = 'application/pdf';
                            $annexe_filename = str_replace('odt', 'pdf', $annex['Annexe']['filename']);
                            break;

                        default:
                            $annexe_content = $annex['Annexe']['data'];
                            $annexe_filetype = $annex['Annexe']['filetype'];
                            $annexe_filename = $annex['Annexe']['filename'];
                            break;
                    }
                    //Création du noeud XML <document> de l'annexe
                    $document = $this->ExportGedCMIS->createElement(
                        $dom,
                        'document',
                        null,
                        ['nom' => $annexe_filename, 'relname' => $annex['Annexe']['id'] . '.pdf', 'type' => 'Annexe']
                    );
                    $document->appendChild($this->ExportGedCMIS->createElement(
                        $dom,
                        'titre',
                        $annex['Annexe']['titre']
                    ));
                    $document->appendChild($this->ExportGedCMIS->createElement(
                        $dom,
                        'mimetype',
                        $annexe_filetype
                    ));
                    $document->appendChild($this->ExportGedCMIS->createElement(
                        $dom,
                        'encoding',
                        'utf-8'
                    ));
                    $doc->appendChild($document);
                    //Ajout du fichier annexe à l'archive
                    $zip->addFromString('Annexes' . DS . $annex['Annexe']['id'] . '.pdf', $annexe_content);
                }
            }
            $dom_depot->appendChild($doc);
        }

        $zip->close();
        $dom->appendChild($dom_depot);

        $file = new File($this->ExportGedCMIS->folderSend->pwd() . DS . 'XML_DESC_' . $seance_id . '.xml', true, 0777);
        $file->write($dom->saveXML());
        $file->close();

        $this->Seance->id = $seance_id;
        $this->Seance->saveField('numero_depot', $idDepot);
        $this->Progress->at(
            90,
            __(
                'Le dossier "%s" est préparé pour l\'envoi (Dépôt n°%s)',
                $libelle_seance,
                $idDepot
            )
        );

        return !empty($my_seance_folder) ? $my_seance_folder : false;
    }

    /**
     * Création des fichiers pour l'export GED en version 2 et 3
     *
     * @version 5.1
     * @access private
     * @param type $seance_id
     * @param type $message_tdt
     * @return type
     * @throws Exception
     */
    private function sendToGedVersion2($seance_id, $version = 2, $message_tdt = false)
    {
        try {
            // Création du répertoire de séance
            $result = $this->ExportGedCMIS->client->getFolderTree($this->ExportGedCMIS->getFolderId(), 1);

            $this->Progress->at(1, __('Création du répertoire de séance...'));

            $seance = $this->Seance->find('first', [
                'conditions' => ['Seance.id' => $seance_id],
                'contain' => ['Typeseance.libelle', 'Typeseance.modele_projet_id']]);

            $this->Progress->at(5, __('Recherche de la séance en base de données...'));

            $date_seance = $seance['Seance']['date'];
            $date_convocation = $seance['Seance']['date_convocation'];
            $type_seance = $seance['Typeseance']['libelle'];
            $libelle_seance = $seance['Typeseance']['libelle']
                . " "
                . CakeTime::i18nFormat($seance['Seance']['date'], '%A %d %B %Y à %k h %M');

            $this->ExportGedCMIS->deletetoGed(Configure::read('CMIS_REPO') . '/' . $libelle_seance);

            $zip = new ZipArchive();
            $zip->open($this->ExportGedCMIS->folderSend->pwd() . DS . 'documents.zip', ZipArchive::CREATE);

            $this->Progress->at(15, __('Création des dossiers...'));

            // Création des dossiers
            $my_seance_folder = $this->ExportGedCMIS->client->createFolder(
                $this->ExportGedCMIS->getFolderId(),
                $libelle_seance
            );
            // Création des dossiers vides
            $zip->addEmptyDir('Rapports');
            $zip->addEmptyDir('Annexes');

            $delibs_id = $this->Seance->getDeliberationsId($seance_id);

            $this->Progress->at(20, __('Création du fichier XML...'));

            $dom = new DOMDocument('1.0', 'utf-8');
            $dom->formatOutput = true;
            $idDepot = $seance['Seance']['numero_depot'] + 1;

            $dom_depot = $this->ExportGedCMIS->createElement($dom, 'depot', null, [
                'versionDepot' => $version,
                'idDepot' => $idDepot,
                'dateDepot' => date("Y-m-d H:i:s"),
                'xmlns:webdelibdossier' => 'http://www.adullact.org/webdelib/infodossier/1.0',
                'xmlns:xm' => 'http://www.w3.org/2005/05/xmlmime']);

            $dom_seance = $this->ExportGedCMIS->createElement(
                $dom,
                'seance',
                null,
                ['idSeance' => $seance_id]
            );
            $dom_seance->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'typeSeance',
                $type_seance
            ));
            $dom_seance->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'dateSeance',
                $date_seance
            ));
            $dom_seance->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'dateConvocation',
                $date_convocation
            ));

            //Noeud document[convocation]
            $this->Progress->at(25, __('Génération de la convocation...'));
            $document = $this->ExportGedCMIS->createElement(
                $dom,
                'document',
                null,
                ['nom' => 'convocation.pdf', 'type' => 'convocation']
            );
            $document->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'mimetype',
                'application/pdf'
            ));
            $document->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'encoding',
                'utf-8'
            ));
            $dom_seance->appendChild($document);
            // fusion du document de convocation
            $zip->addFromString('convocation.pdf', $this->Seance->fusion($seance_id, 'convocation'));

            //Noeud document[odj]
            $this->Progress->at(40, __('Génération de l\'ordre du jour...'));
            CakeLog::info(__('[Ged][%s]Génération: ordre du jour: Start', __METHOD__), 'connector');
            $document = $this->ExportGedCMIS->createElement(
                $dom,
                'document',
                null,
                ['nom' => 'odj.pdf', 'type' => 'odj']
            );
            $document->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'mimetype',
                'application/pdf'
            ));
            $document->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'encoding',
                'utf-8'
            ));
            $dom_seance->appendChild($document);
            // fusion du document de ordre du jour
            $zip->addFromString('odj.pdf', $this->Seance->fusion($seance_id, 'ordredujour'));

            //Noeud document[pv_sommaire]
            if (!empty($seance['Seance']['pv_sommaire'])) {
                $this->Progress->at(50, __('Ajout du PV sommaire...'));
                CakeLog::info(__('[Ged][%s]Add: PV sommaire', __METHOD__), 'connector');
                $document = $this->ExportGedCMIS->createElement(
                    $dom,
                    'document',
                    null,
                    ['nom' => 'pv.pdf', 'type' => 'pv_sommaire']
                );
                $document->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'mimetype',
                    'application/pdf'
                ));
                $document->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'encoding',
                    'utf-8'
                ));
                $dom_seance->appendChild($document);
                //Ajout au zip
                if (!empty($seance['Seance']['pv_sommaire'])) {
                    $zip->addFromString('pv.pdf', $seance['Seance']['pv_sommaire']);
                }
            }

            //Noeud document[pv_complet]
            if (!empty($seance['Seance']['pv_complet'])) {
                $this->Progress->at(55, __('Ajout du PV complet...'));
                CakeLog::info(__('[Ged][%s]Add: PV complet', __METHOD__), 'connector');
                $document = $this->ExportGedCMIS->createElement(
                    $dom,
                    'document',
                    null,
                    ['nom' => 'pvcomplet.pdf', 'type' => 'pv_complet']
                );
                $document->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'mimetype',
                    'application/pdf'
                ));
                $document->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'encoding',
                    'utf-8'
                ));
                $dom_seance->appendChild($document);
                if (!empty($seance['Seance']['pv_complet'])) {
                    $zip->addFromString('pvcomplet.pdf', $seance['Seance']['pv_complet']);
                }
            }

            //Infos supps
            $this->Progress->at(60, __('Ajout des informations supplémentaires de séance...'));
            CakeLog::info(__('[Ged][%s]Add: Infosups', __METHOD__), 'connector');
            $this->ExportGedCMIS->createElementInfosupsByZip(
                $zip,
                $dom,
                $dom_seance,
                $seance_id,
                'Seance'
            );

            //Insertion du noeud xml seance
            $dom_depot->appendChild($dom_seance);

            $this->Progress->at(66, __('Ajout des délibérations...'));
            foreach ($delibs_id as $delib_id) {
                CakeLog::info(__('[Ged][%s]Add: Acte n°%s', __METHOD__, $delib_id), 'connector');
                $doc = $this->ExportGedCMIS->createElement(
                    $dom,
                    'dossierActe',
                    null,
                    [
                      'idActe' => $delib_id,
                      'refSeance' => $seance_id
                    ]
                );
                $delib = $this->Deliberation->find(
                    'first',
                    [
                    'fields' => [
                      'Deliberation.id', 'Deliberation.num_delib', 'Deliberation.objet_delib',
                        'Deliberation.titre',
                      'Deliberation.delib_pdf', 'Deliberation.tdt_data_pdf',
                        'Deliberation.tdt_data_bordereau_pdf', 'Deliberation.deliberation',
                      'Deliberation.deliberation_size', 'Deliberation.signature',
                        'signature_type',
                      'Deliberation.tdt_ar_date', 'Deliberation.tdt_ar', 'Deliberation.signee',
                      'Deliberation.parapheur_etat', 'Deliberation.tdt_id'
                    ],
                    'contain' => [
                        'Service' => ['fields' => ['name']],
                        'Theme' => ['fields' => ['libelle']],
                        'Typeacte' => ['fields' => ['nature_id']],
                        'Redacteur' => ['fields' => ['nom', 'prenom']],
                        'Rapporteur' => ['fields' => ['nom', 'prenom']],
                    ],
                    'conditions' => ['Deliberation.id' => $delib_id]
                    ]
                );

                $nature = $this->Nature->find(
                    'first',
                    [
                    'fields' => ['name'],
                    'conditions' => ['Nature.id' => $delib['Typeacte']['nature_id']]
                    ]
                );

                $doc->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'libelle',
                    $delib['Deliberation']['objet_delib']
                ));
                $doc->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'titre',
                    $delib['Deliberation']['titre']
                ));
                $doc->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'natureACTE',
                    $nature['Nature']['name']
                ));
                $doc->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'dateACTE',
                    $date_seance
                ));
                $doc->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'numeroACTE',
                    $delib['Deliberation']['num_delib']
                ));
                $doc->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'themeACTE',
                    $delib['Theme']['libelle']
                ));
                $doc->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'emetteurACTE',
                    $delib['Service']['name']
                ));
                $doc->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'redacteurACTE',
                    $delib['Redacteur']['prenom'] . ' ' . $delib['Redacteur']['nom']
                ));
                $doc->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'rapporteurACTE',
                    $delib['Rapporteur']['prenom'] . ' ' . $delib['Rapporteur']['nom']
                ));
                $doc->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'typeseanceACTE',
                    $type_seance
                ));
                $doc->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'dateAR',
                    CakeTime::i18nFormat($delib['Deliberation']['tdt_ar_date'], '%A %d %B %Y')
                )); // utile ??

                //<listeCommissions>
                $seanceIds= $this->Deliberation->getSeancesid($delib_id);

                //Noeud listeCommissions
                $document = $this->ExportGedCMIS->createElement($dom, 'listeCommissions', null);
                CakeLog::info(
                    __(
                        '[Ged][%s]Add: Acte n°%s (Commissions)',
                        __METHOD__,
                        $delib_id
                    ),
                    'connector'
                );
                foreach ($seanceIds as $commissionId) {
                    if (!$this->Deliberation->Seance->isDeliberante($commissionId)) {
                        $commission = $this->ExportGedCMIS->createElement(
                            $dom,
                            'commission',
                            null,
                            ['idCommission' => $commissionId]
                        );
                        $commission->appendChild($this->ExportGedCMIS->createElement(
                            $dom,
                            'typeSeance',
                            $this->Deliberation->Seance->Typeseance->getLibelle(
                                $this->Deliberation->Seance->getType($commissionId)
                            )
                        ));
                        $commission->appendChild($this->ExportGedCMIS->createElement(
                            $dom,
                            'dateSeance',
                            $this->Deliberation->Seance->getDate($commissionId)
                        ));
                        $document->appendChild($commission);
                    }
                }
                if (!empty($document)) {
                    $doc->appendChild($document);
                }
                //Infos supps de délibération
                $this->ExportGedCMIS->createElementInfosupsByZip(
                    $zip,
                    $dom,
                    $doc,
                    $delib_id,
                    'Deliberation'
                );

                $aDocuments = [];
                $i = 1;
                //Noeud document[TexteActe]
                CakeLog::info(
                    __(
                        '[Ged][%s]Add: Acte n°%s (TexteActe)',
                        __METHOD__,
                        $delib_id
                    ),
                    'connector'
                );
                $aDocuments['TexteActe'] = $i++;
                $delib_filename = $delib_id . '-' . $delib['Deliberation']['num_delib'] . '.pdf';
                $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                    'idDocument' => $aDocuments['TexteActe'],
                    'nom' => $delib_filename, 'relName' => $delib_filename,
                    'type' => 'TexteActe']);

                if (!empty($delib['Deliberation']['signature'])) {
                    $document->appendChild($this->ExportGedCMIS->createElement(
                        $dom,
                        'signature',
                        true,
                        ['formatSignature' => $delib['Deliberation']['signature'] == 'PAdES' ? 'PAdES' : 'p7s']
                    ));
                }
                $document->appendChild($this->ExportGedCMIS->createElement(
                    $dom,
                    'mimetype',
                    'application/pdf'
                ));
                $document->appendChild($this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8'));
                $doc->appendChild($document);
                //Ajout au zip
                $zip->addFromString($delib_filename, $delib['Deliberation']['delib_pdf']);

                //Noeud document[TexteActe]
                if (!empty($delib['Deliberation']['tdt_data_pdf'])) {
                    $aDocuments['ActeTampon'] = $i++;
                    $delib_filename = $delib_id . '-' . $delib['Deliberation']['num_delib'] . '.pdf';
                    $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                        'idDocument' => $aDocuments['ActeTampon'],
                        'nom' => $delib_filename, 'relName' => 'ActeTampon' . DS . $delib_filename,
                        'type' => 'ActeTampon']);
                    $document->appendChild($this->ExportGedCMIS->createElement(
                        $dom,
                        'signature',
                        'false'
                    ));
                    $document->appendChild($this->ExportGedCMIS->createElement(
                        $dom,
                        'mimetype',
                        'application/pdf'
                    ));
                    $document->appendChild($this->ExportGedCMIS->createElement(
                        $dom,
                        'encoding',
                        'utf-8'
                    ));
                    $doc->appendChild($document);
                    //Ajout au zip
                    $zip->addFromString('ActeTampon' . DS . $delib_filename, $delib['Deliberation']['tdt_data_pdf']);
                }

                //Noeud document[Rapport]
                $aDocuments['Rapport'] = $i++;
                $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                    'idDocument' => $aDocuments['Rapport'],
                    'nom' => $delib_filename,
                    'relName' => 'Rapports' . DS . $delib_filename,
                    'type' => 'Rapport']);
                $document->appendChild($this->ExportGedCMIS->createElement($dom, 'signature', 'false'));
                $document->appendChild($this->ExportGedCMIS->createElement($dom, 'mimetype', 'application/pdf'));
                $document->appendChild($this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8'));
                $doc->appendChild($document);
                // fusion du rapport et ajout au zip
                $zip->addFromString(
                    'Rapports' . DS . $delib_filename,
                    $this->Deliberation->fusion(
                        $delib['Deliberation']['id'],
                        'rapport',
                        $seance['Typeseance']['modele_projet_id']
                    )
                );

                if (!empty($delib['Deliberation']['signature'])
                    && $delib['Deliberation']['signature_type'] != 'PAdES'
                ) {
                    //Ajout de la signature (XML+ZIP)
                    $signatureName = $delib['Deliberation']['id'] . '-signature.zip';
                    $aDocuments['Signature'] = $i++;
                    //Création du noeud XML
                    $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                        'idDocument' => $aDocuments['Signature'],
                        'refDocument' => $aDocuments['TexteActe'],
                        'nom' => $signatureName, 'relName' => 'Signatures' . DS . $signatureName,
                        'type' => 'Signature']);
                    $document->appendChild($this->ExportGedCMIS->createElement($dom, 'mimetype', 'application/zip'));
                    $doc->appendChild($document);
                    //Ajout à l'archive
                    $zip->addFromString('Signatures' . DS . $signatureName, $delib['Deliberation']['signature']);
                }

                //Ajout du bordereau (XML+ZIP)
                if (!empty($aDocuments['ActeTampon'])) {
                    $bordereauName = $delib['Deliberation']['id'] . '-bordereau.pdf';
                    $aDocuments['Bordereau'] = $i++;
                    //Création du noeud XML
                    $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                        'idDocument' => $aDocuments['Bordereau'],
                        'refDocument' => $aDocuments['ActeTampon'],
                        'nom' => $bordereauName,
                        'relName' => 'Bordereaux' . DS . $bordereauName,
                        'type' => 'Bordereau']);
                    $document->appendChild($this->ExportGedCMIS->createElement($dom, 'mimetype', 'application/pdf'));
                    $doc->appendChild($document);
                    //Ajout à l'archive
                    $zip->addFromString(
                        'Bordereaux' . DS . $bordereauName,
                        $delib['Deliberation']['tdt_data_bordereau_pdf']
                    );
                }

                //Ajout des annexes
                $annexes_id = $this->Deliberation->Annexe->getAnnexesFromDelibId($delib_id);
                if (!empty($annexes_id)) {
                    foreach ($annexes_id as $annexe_key => $annexe_id) {
                        $aDocuments['Annexe'] = $i++;
                        $annexes_id[$annexe_key]['refDocument'] = $aDocuments['Annexe'];
                        $annexe = $this->Deliberation->Annexe->getContentToGed($annexe_id['Annexe']['id']);
                        //Création du noeud XML <document> de l'annexe
                        $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                            'idDocument' => $aDocuments['Annexe'],
                            'nom' => $annexe['name'],
                            'relName' => 'Annexes' . DS . $annexe['filename'],
                            'type' => 'Annexe']);
                        $document->appendChild($this->ExportGedCMIS->createElement($dom, 'titre', $annexe['titre']));
                        $document->appendChild($this->ExportGedCMIS->createElement($dom, 'signature', 'false'));
                        if ($annexe['joindre_ctrl_legalite']) {
                            $document->appendChild(
                                $this->ExportGedCMIS->createElement($dom, 'transmisprefecture', 'true')
                            );
                        }
                        $document->appendChild(
                            $this->ExportGedCMIS->createElement($dom, 'mimetype', $annexe['filetype'])
                        );
                        $document->appendChild($this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8'));
                        $doc->appendChild($document);
                        //Ajout du fichier annexe à l'archive
                        $zip->addFromString('Annexes' . DS . $annexe['filename'], $annexe['data']);
                    }
                }

                if (!empty($annexes_id) && $version > 2) {
                    foreach ($annexes_id as $annexe_id) {
                        if (empty($annexe_id['Annexe']['tdt_data_pdf'])) {
                            continue;
                        }

                        $aDocuments['AnnexeTampon'] = $i++;
                        $annexe = $this->Deliberation->Annexe->getContentToGed($annexe_id['Annexe']['id']);
                        //Création du noeud XML <document> de l'annexe
                        $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                            'idDocument' => $aDocuments['AnnexeTampon'],
                            'refDocument' => $annexe_id['refDocument'],
                            'nom' => $annexe['name'],
                            'relName' => 'AnnexesTampon' . DS . $annexe['filename'],
                            'type' => 'AnnexeTampon']);
                        $document->appendChild($this->ExportGedCMIS->createElement($dom, 'titre', $annexe['titre']));
                        $document->appendChild($this->ExportGedCMIS->createElement($dom, 'signature', 'false'));
                        if ($annexe['joindre_ctrl_legalite']) {
                            $document->appendChild(
                                $this->ExportGedCMIS->createElement($dom, 'transmisprefecture', 'true')
                            );
                        }
                        $document->appendChild(
                            $this->ExportGedCMIS->createElement($dom, 'mimetype', $annexe['filetype'])
                        );
                        $document->appendChild($this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8'));
                        $doc->appendChild($document);
                        //Ajout du fichier annexe à l'archive
                        $zip->addFromString('AnnexesTampon' . DS . $annexe['filename'], $annexe['data_tampon']);
                    }
                }

                //Ajout de la signature (XML+ZIP)
                //Création du noeud XML
                if (!empty($delib['Deliberation']['tdt_ar'])) {
                    $aDocuments['ARacte'] = $i++;
                    $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                        'idDocument' => $aDocuments['ARacte'],
                        'nom' => $delib['Deliberation']['id'] . '-' . 'ARacte.xml',
                        'relName' => 'ARacte/' . $delib['Deliberation']['id'] . '-' . 'ARacte.xml',
                        'type' => 'ARacte']);
                    $document->appendChild($this->ExportGedCMIS->createElement($dom, 'mimetype', 'application/xml'));
                    $document->appendChild($this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8'));
                    $doc->appendChild($document);
                    //Ajout à l'archive
                    $zip->addFromString(
                        'ARacte' . DS . $delib['Deliberation']['id'] . '-' . 'ARacte.xml',
                        $delib['Deliberation']['tdt_ar']
                    );
                }
                if ($message_tdt) {
                    $messages = $this->ExportGedCMIS->getTdtMessageForGed($delib_id);

                    foreach ($messages as $message) {
                        $aDocuments['TdtMessage'] = $i++;
                        $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                            'idDocument' => $aDocuments['TdtMessage'],
                            'nom' => $message['name'],
                            'relName' => $message['type'] . '/' . $message['relname'],
                            'type' => $message['type']]);
                        $document->appendChild(
                            $this->ExportGedCMIS->createElement($dom, 'mimetype', 'application/pdf')
                        );
                        $document->appendChild($this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8'));
                        $doc->appendChild($document);
                        //Ajout à l'archive
                        $zip->addFromString(
                            $message['type'] . '/' . $message['relname'],
                            $message['content_pdf']
                        );
                        if (!empty($message['reponses'])) {
                            $aDocuments['TdtMessageReponse'] = $aDocuments['TdtMessage'];
                            foreach ($message['reponses'] as $reponse) {
                                $aDocuments['TdtMessageReponse'] = $i++;
                                $document = $this->ExportGedCMIS->createElement($dom, 'document', null, [
                                    'idDocument' => $aDocuments['TdtMessageReponse'],
                                    'nom' => $reponse['name'],
                                    'relName' => $reponse['type'] . '/' . $reponse['relname'],
                                    'type' => $reponse['type'],
                                    'refDocument' => $aDocuments['TdtMessage']]);
                                $document->appendChild($this->ExportGedCMIS->createElement(
                                    $dom,
                                    'mimetype',
                                    'application/pdf'
                                ));
                                $document->appendChild($this->ExportGedCMIS->createElement($dom, 'encoding', 'utf-8'));
                                $doc->appendChild($document);
                                //Ajout à l'archive
                                $zip->addFromString(
                                    $reponse['type'] . '/' . $reponse['relname'],
                                    $reponse['content_pdf']
                                );
                            }
                            $aDocuments['TdtMessage'] = $aDocuments['TdtMessageReponse'];
                        }
                    }
                }

                $dom_depot->appendChild($doc);
            }

            $zip->close();
            $dom->appendChild($dom_depot);

            $file = new File($this->ExportGedCMIS->folderSend->pwd() . DS . 'XML_DESC_' . $seance_id . '.xml');
            $file->write($dom->saveXML());
            $file->close();

            $this->Seance->id = $seance_id;
            $this->Seance->saveField('numero_depot', $idDepot);
            $this->Progress->at(
                90,
                __(
                    'Le dossier "%s" est préparé pour l\'envoi (Dépôt n°%s)',
                    $libelle_seance,
                    $idDepot
                )
            );
        } catch (Exception $e) {
            throw $e;
        }

        return !empty($my_seance_folder) ? $my_seance_folder : false;
    }

    /**
     * Création des fichiers pour l'export GED en version 3
     * @version 5.1
     * @access private
     * @param type $seance_id
     * @return type
     */
    private function sendToGedVersion3($seance_id)
    {
        return $this->sendToGedVersion2($seance_id, 3, true);
    }

    /**
     * Création des fichiers pour l'export GED en version 3
     * @version 5.1
     * @access private
     * @param type $seance_id
     * @return type
     */
    private function sendToGedVersion4($seance_id)
    {
        try {
            $this->Progress->at(1, __('Recherche de la séance en base de données...'));
            CakeLog::info(__('[Ged][%s]getSeanceForGed: search', __METHOD__), 'connector');
            $seance = $this->getSeanceForGed($seance_id);

            //Création du répertoire CMIS
            $seance_folder = $this->createSeanceFolderCmis($seance);

            //Création du fichier XML
            $this->Progress->at(20, __('Création du fichier XML...'));

            CakeLog::info(__('[Ged][%s]Create DOMDocument', __METHOD__), 'connector');
            $dom = new DOMDocument('1.0', 'utf-8');
            $dom->formatOutput = true;
            $idDepot = $seance['Seance']['numero_depot'] + 1;

            $dom_depot = $this->ExportGedCMIS->createElement($dom, 'depot', null, [
                'versionDepot' => 4,
                'idDepot' => $idDepot,
                'dateDepot' => date("Y-m-d H:i:s")
              ]);

            $dom_seance = $this->ExportGedCMIS->createElement($dom, 'seance', null, ['idSeance' => $seance_id]);
            $dom_seance->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'typeSeance',
                $seance['Typeseance']['libelle']
            ));
            $dom_seance->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'dateSeance',
                $seance['Seance']['date']
            ));
            $dom_seance->appendChild($this->ExportGedCMIS->createElement(
                $dom,
                'dateConvocation',
                $seance['Seance']['date_convocation']
            ));

            CakeLog::info(__('[Ged][%s]Convocation: read', __METHOD__), 'connector');
            $this->Progress->at(25, __('Génération de la convocation...'));
            $this->ExportGedCMIS->createElementConvocation($dom, $dom_seance, $seance);

            CakeLog::info(__('[Ged][%s]Odj: read', __METHOD__), 'connector');
            $this->Progress->at(35, __('Génération de l\'ordre du jour...'));
            $this->ExportGedCMIS->createElementOdj($dom, $dom_seance, $seance);

            if (!empty($seance['Seance']['pv_sommaire'])) {
                CakeLog::info(__('[Ged][%s]PV sommaire: read', __METHOD__), 'connector');
                $this->Progress->at(40, __('Ajout du PV sommaire...'));
                $this->ExportGedCMIS->createElementPvSommaire($dom, $dom_seance, $seance);
            }

            if (!empty($seance['Seance']['pv_complet'])) {
                CakeLog::info(__('[Ged][%s]PV complet: read', __METHOD__), 'connector');
                $this->Progress->at(45, __('Ajout du PV complet...'));
                $this->ExportGedCMIS->createElementPvComplet($dom, $dom_seance, $seance);
            }

            //Création du noeud des informations supplémentaires de séance
            CakeLog::info(__('[Ged][%s]Infosups: read', __METHOD__), 'connector');
            $this->Progress->at(50, __('Ajout des informations supplémentaires de séance...'));
            $this->ExportGedCMIS->createElementInfosups($dom, $dom_seance, $seance_id, 'Seance');

            //Insertion du noeud xml seance
            $dom_depot->appendChild($dom_seance);

            CakeLog::info(__('[Ged][%s]getDeliberationsId: read', __METHOD__), 'connector');
            $this->Progress->at(60, __('Ajout des délibérations...'));

            //Récupération des identifiants des délibérations
            $delibs_id = $this->Seance->getDeliberationsId($seance_id);

            foreach ($delibs_id as $delib_id) {
                $this->ExportGedCMIS->getActeCmis($dom, $dom_depot, $delib_id, $seance);
            }

            $dom->appendChild($dom_depot);

            //Mise en place du fichier xml de description
            $file = new File($this->ExportGedCMIS->folderSend->pwd() . DS . 'XML_DESC_' . $seance_id . '.xml');
            $file->write($dom->saveXML());
            $file->close();

            CakeLog::info(__('[Ged][%s]Sauvegarde du numéro de dépot: %s', __METHOD__, $idDepot), 'connector');
            $this->Seance->id = $seance_id;
            $this->Seance->saveField('numero_depot', $idDepot);
        } catch (Exception $e) {
            throw $e;
        }

        return !empty($seance_folder) ? $seance_folder : false;
    }

    /**
     * Récupération des informations de séance
     * @param type $seance_id
     * @return type
     */
    private function getSeanceForGed($seance_id)
    {
        $seance = $this->Seance->find(
            'first',
            [
            'contain' => ['Typeseance.libelle', 'Typeseance.modele_projet_id'],
            'conditions' => ['Seance.id' => $seance_id],
            'recursive' => -1]
        );

        //Modification du libelle de séance pour la Ged
        $seance['Seance']['cmis_folder'] = $this->ExportGedCMIS->inflectorNameForFolder(
            __(
                "%s %s",
                $seance['Typeseance']['libelle'],
                CakeTime::i18nFormat($seance['Seance']['date'], '%A %d %B %Y à %k h %M')
            )
        );

        return $seance;
    }

    /**
     * Récupération des informations de séance
     * @param type $seance_id
     * @return type
     */
    private function createSeanceFolderCmis(&$seance)
    {
        $this->Progress->at(10, __('Création du répertoire d\'export de séance dans la ged...'));

        $this->ExportGedCMIS->client->getFolderTree($this->ExportGedCMIS->getFolderId(), 1);

        CakeLog::info(__('[Ged][%s]name folder cmis: %s', __METHOD__, $seance['Seance']['cmis_folder']), 'connector');
        $this->ExportGedCMIS->deletetoGed(Configure::read('CMIS_REPO') . '/' . $seance['Seance']['cmis_folder']);

        // Création du répertoire de séance
        return $this->ExportGedCMIS->client->createFolder(
            $this->ExportGedCMIS->getFolderId(),
            $seance['Seance']['cmis_folder']
        );
    }
}
