<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppController', 'Controller');

/**
 * Class VotesController
 *
 * @package app.Controller
 */
class DuplicateController extends AppController
{
    /**
     * [public description]
     * @var [type]
     */
    public $uses = [
        'Deliberation',
        'Seance',
        'Typeseance',
        'Acteur',
        'Listepresence',
        'Vote',
        'Historique',
    ];
    /**
     * [public description]
     * @var [type]
     */
    public $components = [
        'RequestHandler',
        'SeanceTools',
        'SabreDav',
        'Auth' => [
            'mapActions' => [
                'allow' => [ 'duplicate'],
            ],
        ]];

    /**
     * Duplique le projet d'on l'id est passé, duplique les
     * informations de la table délibération et infosup, un petit ménage est fait
     *
     * @access public
     * @param type $id --> id de la délibération
     */
    public function duplicate($id = null)
    {
        $this->Deliberation->Behaviors->load(
            'Version',
            $this->Deliberation->actsAsVersionOptionsList['Version']
        );

        $user_id = $this->Auth->user('id');
        $service_id = $this->Auth->user('ServiceEmetteur.id');
        $newId = $this->Deliberation->duplicate($id, $user_id, $service_id);
        $this->Flash->set(
            __('Le projet a été dupliqué.'),
            ['element' => 'growl', 'params' => ['type' => 'info']]
        );

        $this->redirect(['controller' => 'projets', 'action' => 'edit', $newId]);
    }
}
