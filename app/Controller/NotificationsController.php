<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Class WebdavController
 *
 * @version 4.3
 * @package app.Controller
 */
class NotificationsController extends AppController
{
    /**
     * $uses
     * @var [array]
     */
    public $uses = [
        'User',
    ];

    public $components = [
        'Auth' => [
            'mapActions' => [
                'allow' => ['index','notification']
            ],
          ]
    ];

    /**
     * [changeUserSetting Changement des préférences utilisateur]
     * @return [type] [description]
     * @version 5.2
     */
    public function index()
    {
        if ($this->request->is('Post')) {
            $this->User->id = $this->Auth->user('id');
            $this->request->data['User']['preference_first_login_active'] = true;
            $this->request->data['User']['accept_notif'] =
                ($this->request->data['User']['not_accept_notif'] != true);
            if ($this->User->save($this->request->data, false)) {
                $this->Flash->set(
                    __('Modifications enregistrées avec succès'),
                    ['element' => 'growl']
                );
                $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Erreur lors de l\'enregistrement des modifications.'),
                    ['element' => 'growl', 'params' => ['type' => 'danger']]
                );
            }
        }

        $this->request->data = $this->User->find(
            'first',
            [
                'conditions' => [
                    'User.id' => $this->Auth->user('id')
                ],
                'recursive' => -1
            ]
        );
        $this->set('notif', ['1' => __('oui'), '0' => __('non')]);
        $this->set('not_accept_notif', $this->data['User']['accept_notif'] === true ? false : true);
    }

    /**
     * [changeUserSetting Changement des préférences utilisateur]
     * @return [type] [description]
     * @version 7.0
     */
    public function notification($later = false)
    {
        if ($later) {
            $this->Session->write('Auth.User.preference_first_login_later', true);
            return $this->redirect($this->previous);
        }

        if ($this->request->is('Post')) {
            $this->User->id = $this->Auth->user('id');
            $this->request->data['User']['preference_first_login_active'] = true;
            $this->request->data['User']['accept_notif'] =
                $this->request->data['User']['not_accept_notif']==true ? false : true;
            if ($this->User->save($this->request->data, false)) {
                $this->Session->write('Auth.User.preference_first_login_active', true);


                $this->Flash->set(
                    __('Réglages de notifications utilisateur enregistrées avec succès'),
                    ['element' => 'growl']
                );
                return $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Erreur lors de l\'enregistrement des réglages de notifications.'),
                    ['element' => 'growl', 'params' => ['type' => 'danger']]
                );
            }
        }
    }
}
