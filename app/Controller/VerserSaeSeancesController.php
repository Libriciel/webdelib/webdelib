<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppController', 'Controller');
App::uses('AppTools', 'Lib');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('SaeVersement', 'Model');

/**
 * Controller export GED des séances
 *
 * @package app.Controller
 * @version 5.1.0
 */
class VerserSaeSeancesController extends AppController
{
    /** @var Array Contains a uses */
    public $uses = [
        'Seance',
        'Deliberation',
        'Nature',
        'SaeVersement'
    ];

    /** @var Array Contains a components */
    public $components = [
        'RequestHandler',
        'Progress',
        'Conversion',
        'VersementSae',
    ];

    public const PREPARATION =  '1_PREPARATION';
    public const DELIBERATIONS =  '2_DELIBERATIONS';
    public const POST_SEANCE =  '3_POST_SEANCE';
    public const COMPLEMENTS =  '4_COMPLEMENTS';

    /**
     * Envoi pour une GED en protocole CMIS
     *
     * @param int $seance_id
     * @throws JsonException
     * @version 5.1
     * @access public
     */
    public function sendToSae($seanceId, $cookieTokenKey)
    {
        $this->Progress->start($cookieTokenKey);
        $folderSend = new Folder(AppTools::newTmpDir(TMP . 'files' . DS . 'connectors'), true);
        try {
            $this->log(
                __('[Sae][%s]VerserService: Start', __METHOD__),
                LOG_INFO,
                'connector'
            );
            $this->VersementSae->verserService($folderSend);

            $this->log(
                __('[Sae][%s]Version d\'export v%s', __METHOD__, 1),
                LOG_INFO,
                'connector'
            );
            $this->sendToSaeVersion($seanceId);

            $this->Progress->at(90, __('Envoi des fichiers'));
            $idD = $this->VersementSae->send($folderSend);

            $this->Seance->recursive = -1;
            $this->Seance->id = $seanceId;
            $seance = $this->Seance->read(['sae_numero_versement']);
            $this->Seance->save([
                'sae_pastell_id' => $idD,
                'sae_numero_versement' => $seance['Seance']['sae_numero_versement'] + 1,
                'sae_etat' => SaeVersement::VERSER,
                'sae_commentaire' => null,
                'sae_atr' => null,
                'sae_date' => date('Y-m-d H:i:s', time()),
            ], false);

            $this->log(
                __('[Sae][%s]VerserService: Send ok', __METHOD__),
                LOG_INFO,
                'connector'
            );
            sleep(3);
            $this->Progress->redirect($this->previous);
            $this->Progress->stop(__('Opération terminée'));
        } catch (CakeException | Exception $e) {
            $message = '';
            switch ($e->getCode()) {
                case 500:
                    $message = 'Erreur interne';
                    break;
                case 404:
                    $message = 'Ressource non trouvée';
                    break;
                case 413:
                    $message = 'La séance est trop volumineuse pour la configuration actuelle de Pastell.'
                        .' Veuillez faire une demande d\'augmentation des capacités avant de renvoyer la séance.';
                    break;
                default:
                    $message = strip_tags($e->getMessage());
                    break;
            }
            $this->log(
                __(
                    '[Sae][%s]Send error(%s)',
                    __METHOD__,
                    $e->getCode()
                ) . "\n" . $e->getMessage(),
                LOG_ERR,
                'connector'
            );
            $this->Progress->error(new Exception(__('[Sae]') . ' ' . $message, $e->getCode()));
        }
        $folderSend->delete();

        $this->autoRender = false;
        $this->response->type(['json' => 'text/x-json']);
        $this->RequestHandler->respondAs('json');
        $this->response->body(json_encode($this->Session->read('Progress.Tokens'), JSON_THROW_ON_ERROR));

        return $this->response;
    }

    /**
     * Création des fichiers pour le versement GED en version 1
     *
     * @version 7.1
     * @access private
     * @param int $seance_id
     * @return void
     * @throws Exception
     */
    private function sendToSaeVersion($seanceId)
    {
        $this->Progress->at(5, __('Recherche de la séance en base de données...'));

        $seance = $this->Seance->find('first', [
            'contain' => [
                'Typeseance.libelle',
                'Typeseance.modele_projet_id',
                'Secretaire.nom',
                'Secretaire.prenom',
                'President.nom',
                'President.prenom'
            ],
            'conditions' => ['Seance.id' => $seanceId],
            'recursive' => -1
        ]);
        $idVersement = $seance['Seance']['sae_numero_versement'] + 1;

        $zip = new ZipArchive();
        $this->VersementSae->setZip($zip);
        $zipName = __(
            '%s-%s_versement_%s',
            Inflector::slug($seance['Typeseance']['libelle'], '_'),
            CakeTime::i18nFormat($seance['Seance']['date'], '%Y-%m-%d-%Hh%M'),
            $idVersement
        );
        $zipCreated = $zip->open(
            $this->VersementSae->getFolderSend()->pwd() . DS . $zipName . '.zip',
            ZipArchive::CREATE
        );
        if (!$zipCreated) {
            throw new CakeException(
                __(
                    'Impossible de créer le zip avec le nom : %s.zip (%s)',
                    $zipName,
                    $zipCreated
                )
            );
        }

        $this->Progress->at(10, __('Création du fichier XML...'));

        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->formatOutput = true;


        $dom_depot = $this->VersementSae->createElement($dom, 'versement', null, [
            'versionVersement' => 1,
            'numeroVersement' => $idVersement,
            'dateVersement' => date("Y-m-d H:i:s"),
            'xmlns:xm' => 'http://www.w3.org/2005/05/xmlmime']);

        $dom_seance = $this->VersementSae->createElement($dom, 'seance', null, ['idSeance' => $seanceId]);
        $dom_seance->appendChild($this->VersementSae->createElement(
            $dom,
            'typeSeance',
            $seance['Typeseance']['libelle']
        ));
        $dom_seance->appendChild($this->VersementSae->createElement(
            $dom,
            'dateSeance',
            CakeTime::i18nFormat($seance['Seance']['date'], '%Y-%m-%d %H:%M')
        ));
        $dom_seance->appendChild($this->VersementSae->createElement(
            $dom,
            'presidentSeance',
            trim(__('%s %s', $seance['President']['prenom'], $seance['President']['nom']))
        ));
        $dom_seance->appendChild($this->VersementSae->createElement(
            $dom,
            'secretaireSeance',
            trim(__('%s %s', $seance['Secretaire']['prenom'], $seance['Secretaire']['nom']))
        ));

        //Noeud document[convocation]
        $this->Progress->at(20, __('Génération de la convocation...'));
        CakeLog::info(__('[Sae][%s]Génération: convocation: Start', __METHOD__), 'connector');
        $dom_seance->appendChild($this->VersementSae->createElement(
            $dom,
            'dateConvocation',
            CakeTime::i18nFormat($seance['Seance']['date_convocation'], '%Y-%m-%d')
        ));
        $zip->addFromString(
            self::PREPARATION . DS . $this->nameFileSeance($seance, 'convocation.pdf'),
            $this->Seance->fusion($seanceId, 'convocation')
        );

        //Noeud document[odj]
        $this->Progress->at(30, __('Génération de l\'ordre du jour...'));
        CakeLog::info(__('[Sae][%s]Génération: ordre du jour: Start', __METHOD__), 'connector');
        // fusion du document de l'ordre du jour
        $zip->addFromString(
            self::PREPARATION . DS . $this->nameFileSeance($seance, 'ordre_du_jour.pdf'),
            $this->Seance->fusion($seanceId, 'ordredujour')
        );

        //Noeud document[pv_complet]
        $this->Progress->at(40, __('Ajout du PV sommaire...'));
        CakeLog::info(__('[Sae][%s]Add: PV sommaire', __METHOD__), 'connector');
        $zip->addFromString(
            self::POST_SEANCE . DS . $this->nameFileSeance($seance, 'liste_deliberations.pdf'),
            $seance['Seance']['pv_sommaire']
        );

        //Noeud document[pv_complet]
        $this->Progress->at(50, __('Ajout du PV complet...'));
        CakeLog::info(__('[Sae][%s]Add: PV complet', __METHOD__), 'connector');
        $zip->addFromString(
            self::POST_SEANCE . DS . $this->nameFileSeance($seance, 'pv.pdf'),
            $seance['Seance']['pv_complet']
        );
        //Infos supps
        $this->Progress->at(60, __('Ajout des informations supplémentaires de séance...'));
        $this->VersementSae->createElementInfosupsByZip($seanceId, 'Seance');

        $this->Progress->at(70, __('Ajout des délibérations...'));
//            //Récupération des identifiants des délibérations
        $delibs_id = $this->Seance->getDeliberationsId($seanceId);
        $domDeliberations = $this->VersementSae->createElement(
            $dom,
            'delibs',
            null,
        );
        foreach ($delibs_id as $delib_id) {
            $this->VersementSae->getDeliberation($dom, $domDeliberations, $delib_id, $seance);
        }
        $dom_seance->appendChild($domDeliberations);

        //Insertion du noeud xml seance
        $dom_depot->appendChild($dom_seance);

        $zip->close();
        $dom->appendChild($dom_depot);

        $file = new File($this->VersementSae->folderSend->pwd() . DS . $zipName. '.xml');
        $file->write($dom->saveXML());
        $file->close();

        $this->Progress->at(90, __('Le dossier est préparé pour versement (n°%s)', $idVersement));
    }

    private function nameFileSeance(&$seance, $nameFile)
    {
        return __(
            '%s_%s_%s',
            $seance['Typeseance']['libelle'],
            CakeTime::i18nFormat($seance['Seance']['date'], '%Y-%m-%d_%Hh%M'),
            $nameFile
        );
    }
}
