<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Cron', 'Model');
App::uses('CronJob', 'Model');

/**
 * Application: webdelib / Adullact.
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 * Date: 13/01/14
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       web-delib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 * @version     v4.3
 * @package     app.Controller.Component
 * @author      Florian Ajir
 */
class CronsComponent extends Component
{
    /**
     *
     * @param type $controller
     */
    public function startup($controller)
    {
        //Chargement du Model "Cron"
        $this->Cron = ClassRegistry::init('Cron');
        parent::startup($controller);
    }

    /**
     * fonction d'exécution d'un cron par son id (actif ou non)
     * @param integer $id id du cron a exécuter
     * @return bool|string
     */
    public function runCronId($id)
    {
        try {
            // initialisation date
            $lastExecutionStartTime = date(Cron::FORMAT_DATE);

            // lecture du cron à exécuter
            $cron = $this->Cron->find('first', [
                'recursive' => -1,
                'conditions' => [
                    'id' => $id,
                    'active' => true,
                    'lock' => false
                ]]);

            // Sortie si tâche non trouvée
            if (empty($cron)) {
                return;
            }
            $this->Cron->id = $id;

            //Verrouille la tâche pour éviter les exécutions parallèles
            $this->Cron->saveField('lock', true);

            // Chargement de la classe
            $caller = ucfirst($cron['Cron']['model']);
            if (!empty($cron['Cron']['plugin'])) {
                $caller = ucfirst($cron['Cron']['plugin']) . '.' . $caller;
            }
            $this->{$cron['Cron']['model']} = ClassRegistry::init($caller);

            // Exécution
            if (!empty($cron['Cron']['has_params'])) {
                $output = $this->{$cron['Cron']['model']}->{$cron['Cron']['action']}(
                    explode(',', $cron['Cron']['params'])
                );
            } else {
                $output = $this->{$cron['Cron']['model']}->{$cron['Cron']['action']}();
            }
        } catch (Exception $e) {
            $output = Cron::MESSAGE_FIN_EXEC_ERROR . "Exception levée : \n" . $e->getMessage();
            $this->log($e->getMessage() . PHP_EOL . $e->getTraceAsString(), 'error');
        }

        // initialisation du rapport d'exécution
        $rappExecution = str_replace(
            [Cron::MESSAGE_FIN_EXEC_SUCCES, Cron::MESSAGE_FIN_EXEC_WARNING, Cron::MESSAGE_FIN_EXEC_ERROR],
            '',
            $output
        );

        // mise à jour du cron
        $cron['Cron']['lock'] = false;
        $cron['Cron']['last_execution_start_time'] = $lastExecutionStartTime;
        if (strpos($output, Cron::MESSAGE_FIN_EXEC_SUCCES) !== false) {
            $cron['Cron']['last_execution_status'] = Cron::EXECUTION_STATUS_SUCCES;
            $cron['Cron']['next_execution_time'] = $this->Cron->calcNextExecutionTime(
                $cron['Cron']['execution_duration'],
                $cron['Cron']['next_execution_time']
            );
        } elseif (strpos($output, Cron::MESSAGE_FIN_EXEC_WARNING) !== false) {
            $cron['Cron']['last_execution_status'] = Cron::EXECUTION_STATUS_WARNING;
            $cron['Cron']['next_execution_time'] = $this->Cron->calcNextExecutionTime(
                $cron['Cron']['execution_duration'],
                $cron['Cron']['next_execution_time']
            );
        } else {
            $cron['Cron']['last_execution_status'] = Cron::EXECUTION_STATUS_FAILED;
        }
        $cron['Cron']['last_execution_end_time'] = date(Cron::FORMAT_DATE);
        $cron['Cron']['last_execution_report'] = $rappExecution;
        if ($this->Cron->save($cron)) {
            return $output;
        } else {
            return Cron::EXECUTION_STATUS_FAILED . $output;
        }
    }

    /**
     * Exécute toutes les tâches planifiée en attente
     * Vérifie si la date/time d'execution prévue est inférieur à la date/time du jour
     * OU si le délais entre 2 exécutions est dépassé
     *
     * @return string
     */
    public function runPending()
    {
        $rapport = date(Cron::FORMAT_DATE) . "\n";
        // lecture des crons à exécuter
        $crons = $this->Cron->find('all', [
            'recursive' => -1,
            'fields' => ['id', 'nom'],
            'conditions' => [
                'OR' => [
                    'next_execution_time <= ' => date(Cron::FORMAT_DATE),
                    'next_execution_time' => null,
                ],
                'active' => true,
                'lock' => false
            ],
            'order' => ['next_execution_time ASC']]);
        if (!empty($crons)) {
            // exécutions
            foreach ($crons as $cron) {
                $rapport .= $cron['Cron']['id'] . '-' . $cron['Cron']['nom'] . " : \n"
                    . $this->runCronId($cron['Cron']['id']) . "\n";
            }
        } else {
            $rapport .= __("Aucune tâche planifiée à exécuter");
        }
        return $this->info($rapport);
    }

    /**
     *
     */
    public function runAll()
    {
        // lecture des crons à exécuter
        $crons = $this->Cron->find('all', [
            'recursive' => -1,
            'fields' => ['id'],
            'conditions' => [
                'active' => true,
                'lock' => false
            ],
            'order' => ['next_execution_time ASC']]);

        // exécutions
        if (!empty($crons)) {
            foreach ($crons as $cron) {
                $this->runCronId($cron['Cron']['id']);
            }
        }
    }
}
