<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Deliberation', 'Model');
App::uses('Seance', 'Model');
App::uses('Nomenclature', 'Model');
App::uses('DeliberationTypeseance', 'Model');
App::uses('DeliberationSeance', 'Model');
App::uses('User', 'User');
App::uses('Cakeflow.Circuit', 'Model');
App::uses('Service', 'Model');

/**
 *
 */
class ProjetToolsComponent extends Component
{
    public $components = [
        'Session',
        'Auth',
        'Acl',
        'TokensMethods',
        'Filtre',
        'AuthManager.Permissions',
    ];

    /**
     * Request object
     *
     * @var CakeRequest
     */
    public $request;

    /**
     * Response object
     *
     * @var CakeResponse
     */
    public $response;
    /**
     * @var AppModel|bool|Deliberation|Model|object|null
     */
    private $Deliberation;
    /**
     * @var AppModel|bool|Model|object|Seance|null
     */
    private $Seance;
    /**
     * @var AppModel|bool|DeliberationSeance|Model|object|null
     */
    private $DeliberationSeance;
    /**
     * @var AppModel|bool|Model|Nomenclature|object|null
     */
    private $Nomenclature;
    /**
     * @var AppModel|bool|DeliberationTypeseance|Model|object|null
     */
    private $DeliberationTypeseance;

    private $classInitialized = false;

    /**
     *
     * @param Controller $controller
     */
    public function initialize(Controller $controller)
    {
        $this->request = $controller->request;
        $this->response = $controller->response;

        $this->initializeClass();

        parent::initialize($controller);
    }

    /**
     *
     * @param Controller $controller
     */
    public function initializeClass()
    {
        if ($this->classInitialized === true) {
            return;
        }

        $this->Deliberation = ClassRegistry::init('Deliberation');
        $this->Seance = ClassRegistry::init('Seance');
        $this->Nomenclature = ClassRegistry::init('Nomenclature');
        $this->DeliberationTypeseance = ClassRegistry::init('DeliberationTypeseance');
        $this->DeliberationSeance = ClassRegistry::init('DeliberationSeance');
        $this->User = ClassRegistry::init('User');
        $this->Circuit = ClassRegistry::init('Cakeflow.Circuit');
        $this->Service = ClassRegistry::init('Service');

        $this->classInitialized = true;
    }

    /**
     * Génére le tableau des 9 cases de chaque projet de délibération
     *
     * @param $deliberation_id : Id du projet de deliberation
     * @return array();
     */
    public function get9casesData($deliberation_id)
    {
        $caseProject = [];
        foreach ($this->Session->read('Collectivite.templateProject') as $field) {
            $explodeField = explode('.', $field);
            $case = [];
            $case['model'] = !empty($explodeField[0]) ? $explodeField[0] : '';
            $case['fields'] = !empty($explodeField[1]) ? $explodeField[1] : '';
            $case['id'] = !empty($explodeField[2]) ? $explodeField[2] : '';

            if (isset($case['model']) && $case['model'] == 'Infosupdef') {
                $caseProject[] = $this->combineInfosup($deliberation_id, $case['id']);
            } else {
                $caseProject[] = $case;
            }
        }
        return $caseProject;
    }

    /**
     * Retourne un tableau array('image'=>, 'titre'=>) pour l'affichage de l'icône dans les listes en fonction de :
     *
     * @param int $id identifiant du projet
     * @param $etat état du projet ou de la délibération
     * @param $editerTous droit d'éditer les projets validés
     *
     */
    public function iconeEtat(
        $id,
        $etat,
        $nature_code = null,
        $editerTous = false,
        $estDansCircuit = false,
        $estRedacteur = false,
        $tourDansCircuit = 0
    ) {
        App::uses('ProjetEtat', 'Model');
        $projetEtat = ClassRegistry::init('ProjetEtat');

        switch ($etat) {
            case -3: // refusé etat non disponible en base
                return [
                    'image' => 'abandonné',
                    'titre' => $projetEtat->libelleEtat($etat, $nature_code)];
            case -2: // refusé etat non disponible en base
                return [
                    'image' => 'nouvelle_version',
                    'titre' => $projetEtat->libelleEtat($etat, $nature_code)];

            case -1: // versionne
                return [
                    'image' => 'refuse',
                    'titre' => $projetEtat->libelleEtat($etat, $nature_code)];
            case 0: // en cours de rédaction
                return [
                    'image' => 'encours',
                    'titre' => $projetEtat->libelleEtat($etat, $nature_code)];
            case 1: // en cours de validation
                if ($estDansCircuit) {
                    if ($tourDansCircuit === false) {
                        return [
                            'image' => 'attente_circuit',
                            'titre' => $projetEtat->libelleEtat($etat, $nature_code) . ' (en attente d\'insertion)'];
                    }
                    if ($tourDansCircuit == -1) {
                        return [
                            'image' => 'circuit_user_fini',
                            'titre' => $projetEtat->libelleEtat($etat, $nature_code)
                                .' ('. __('Traitement effectué') . ')'];
                    } elseif ($tourDansCircuit == 0) {
                        App::uses('Traitement', 'Cakeflow.Model');
                        $traitement = ClassRegistry::init('Traitement');
                        return [
                            'image' => 'circuit_user_atraiter',
                            'titre' => $projetEtat->libelleEtat($etat, $nature_code) . ' (à traiter)',
                            'status' => $traitement->isDelayStatus($id)
                        ];
                    } elseif ($estRedacteur) {
                        return [
                            'image' => 'circuit_redacteur_attente',
                            'titre' => $projetEtat->libelleEtat($etat, $nature_code) . ' (Rédacteur)'];
                    } else {
                        return [
                            'image' => 'circuit_attente',
                            'titre' => $projetEtat->libelleEtat($etat, $nature_code) . ' (en attente)'];
                    }
                } else {
                    if ($estRedacteur) {
                        return [
                            'image' => 'circuit_attente',
                            'titre' => $projetEtat->libelleEtat($etat, $nature_code) . ' (Rédacteur)'];
                    } else {
                        return [
                            'image' => 'circuit_attente',
                            'titre' => $projetEtat->libelleEtat($etat, $nature_code)];
                    }
                }
                break;
            case 2: // validé
                if ($editerTous) {
                    return [
                        'image' => 'valide_editable',
                        'titre' => $projetEtat->libelleEtat($etat, $nature_code)];
                } else {
                    return [
                        'image' => 'fini',
                        'titre' => $projetEtat->libelleEtat($etat, $nature_code)];
                }
                break;
            case 3: // voté et adopté
                return [
                    'image' => 'voter_pour',
                    'titre' => $projetEtat->libelleEtat($etat, $nature_code)];
            case 4: // voté et rejeté
                return [
                    'image' => 'voter_contre',
                    'titre' => $projetEtat->libelleEtat($etat, $nature_code)];
            case 5: // transmis via connecteur web-delib au contrôle de légalité
                return [
                    'image' => 'transmis',
                    'titre' => $projetEtat->libelleEtat($etat, $nature_code)];
            case 10: // Signature annulée
                return [
                    'image' => 'a_renvoyer_signature',
                    'titre' => $projetEtat->libelleEtat($etat, $nature_code)];
            case 11: // transmis manuellement au contrôle de légalité
                return [
                    'image' => 'transmis_manuellement',
                    'titre' => $projetEtat->libelleEtat($etat, $nature_code)];
        }
    }

    /** Retourne la matière par rapport à une clé donnée en paramètre
     *
     * @param type $key
     * @return String
     */
    public function getMatiereByKey($key)
    {
        $this->initializeClass();

        if (!empty($key)) {
            $nomenclature = $this->Nomenclature->findByName($key, ['libelle']);
            if (!empty($nomenclature['Nomenclature']['libelle'])) {
                return $nomenclature['Nomenclature']['libelle'];
            }
        }

        return '';
    }

    /** Retourne la liste ds Natures de Projets
     *
     * @return boolean
     *
     * @version 5.1.1
     * @since 4.3
     */
    public function getNatureListe()
    {
        $this->initializeClass();

        $nomenclatures = $this->Nomenclature->find(
            'all',
            [
              'fields'=>['id', 'libelle'],
              'recursive' => -1
            ]
        );

        return Hash::combine($nomenclatures, '{n}.Nomenclature.id', '{n}.Nomenclature.libelle');
    }

    /**
     * Tri pour les dates de séance
     *
     * @param type $projets
     */
    public function sortProjetSeanceDate(&$projets)
    {
        foreach ($projets as $keyProjet => $projet) {
            if (!empty($projets[$keyProjet]['DeliberationTypeseance'])) {
                $projets[$keyProjet]['DeliberationTypeseance'] =
                    Hash::sort($projet['DeliberationTypeseance'], '{n}.Typeseance.action', 'asc');
            }
            if (!empty($projets[$keyProjet]['DeliberationSeance'])) {
                $projets[$keyProjet]['DeliberationSeance'] =
                    Hash::sort($projet['DeliberationSeance'], '{n}.Seance.date', 'asc');
                $projets[$keyProjet]['DeliberationSeance'] =
                    Hash::sort($projets[$keyProjet]['DeliberationSeance'], '{n}.Seance.Typeseance.action', 'asc');
            }
        }
    }

    /**
     * Combine la liste de toutes les informations supplémentaires
     *
     * @param $deliberation_id : Id du projet de deliberation
     * @param $infosupdef_id : Id de l'information supplémentaire
     * @return array();
     */
    private function combineInfosup($deliberation_id, $infosupdef_id)
    {
        $combineInfosup = [];
        App::uses('Infosupdef', 'Model');
        App::uses('Infosup', 'Model');
        $Infosupdef = ClassRegistry::init('Infosupdef');
        $Infosup = ClassRegistry::init('Infosup');

        $infosupdef = $Infosupdef->find('first', [
            'conditions' => ['id' => $infosupdef_id, 'actif' => true, 'model' => 'Deliberation'],
            'fields' => ['code', 'type', 'nom'],
            'recursive' => -1]);
        $infosup = $Infosup->find('first', [
            'recursive' => -1,
            'conditions' => [
                'foreign_key' => $deliberation_id,
                'infosupdef_id' => $infosupdef_id]]);
        if (!empty($infosup) && !empty($infosupdef)) {
            $combineInfosup['compacte'] = $Infosup->compacte($infosup, false);
            $combineInfosup['model'] = 'Infosupdef';
            $combineInfosup = array_merge($combineInfosup, $infosupdef['Infosupdef']);
        }
        return $combineInfosup;
    }

    /**
     * [locked description]
     * @param  [type] $projet [description]
     * @return [type]         [description]
     *
     */
    public function locked(&$projet): void
    {
        //On cherche à savoir si la délibération est verrouillée par un utilisateur
        $lock = $this->TokensMethods->locked([
            'controller' => 'projets',
            'action' => 'edit',
            'entity_id' => $projet['id']
        ]);
        $projet['isLocked'] = $lock !== false;
        if (!empty($lock)) {
            $this->User->virtualFields['name'] = 'User.nom || \' \' || User.prenom';
            $lock_user_name = $this->User->find('first', [
                'fields' => ['name'],
                'conditions' => ['User.id' => $lock['user_id']],
                'recursive' => -1
            ]);

            $projet['lock_user_name'] = $lock_user_name['User']['name'];
            $projet['lock_last_time'] = CakeTime::i18nFormat($lock['last_time'], '%A %e %B %Y')
                . ' à '
                . CakeTime::i18nFormat($lock['last_time'], '%k:%M');
        }
    }

    /**
     * Récupération de liste des droits par projet
     *
     * @access protected
     *
     * @param array $projet
     * @param type $droits
     * @param type $listeActions
     *
     * @version 5.1.0
     * @version 4.3.0
     *
     */
    public function actionDisponible(&$projet, $droits, $listeActions = [])
    {
        $actions = [];
        if (!empty($droits['Projets/read'])) {
            array_push($actions, 'view');
            array_push($actions, 'generer');
        }
        if (!empty($droits['duplicate/read'])) {
            array_push($actions, 'duplicate');
        }

        switch ($projet['Deliberation']['etat']) {
            case 0: //En cours de rédaction
                //Pour les rédacteurs
                if (isset($projet['User']) && !is_null($projet['User'])) {
                    $users = Hash::extract($projet['User'], '{n}.id');
                    array_push($users, $projet['Deliberation']['redacteur_id']);
                    if ((!empty($droits['Projets/update'])
                        && in_array($this->Auth->user('id'), $users, true))
                    ) {
                        if (isset($droits['Typeacte/update'])
                            && in_array($projet['Deliberation']['typeacte_id'], $droits['Typeacte/update'], true)
                        ) {
                            array_push($actions, 'edit');
                        }
                    }

                    if ((!empty($droits['attribuerCircuit/read']) && in_array($this->Auth->user('id'), $users, true))) {
                        array_push($actions, 'attribuerCircuit');
                    }
                }
                //Pour les administrateurs
                if (!in_array('edit', $actions, true)
                    && !empty($droits['editerTous/read'])
                    && !empty($projet['Deliberation']['typeacte_id'])
                    && !empty($droits['Typeacte/update'])
                ) {
                    if (in_array($projet['Deliberation']['typeacte_id'], $droits['Typeacte/update'], true)) {
                        array_push($actions, 'edit');
                    }
                }

                if (($projet['Deliberation']['redacteur_id'] == $this->Auth->user('id')
                    && !empty($droits['Projets/delete']))
                ) {
                    array_push($actions, 'delete');
                }
                if (!in_array('delete', $actions, true)
                    && !empty($droits['supprimerTous/read'])
                    && !empty($projet['Deliberation']['typeacte_id'])
                    && !empty($droits['Typeacte/delete'])
                ) {
                    if (in_array($projet['Deliberation']['typeacte_id'], $droits['Typeacte/delete'], true)) {
                        array_push($actions, 'delete');
                    }
                }
                break;
            case 1: //Dans un circuit
                //utilisateur de l'étape
                $users = $this->Deliberation->Traitement->whoIs($projet['Deliberation']['id'], 'current', 'RI');
                if (in_array($this->Auth->user('id'), $users, true)) {
                    array_push($actions, 'traiter');
                }
                if (!empty($droits['goNext/read'])) {
                    array_push($actions, 'goNext');
                }
                if (!empty($droits['validerEnUrgence/read'])) {
                    array_push($actions, 'validerEnUrgence');
                }
                if (!empty($droits['editerTous/read'])
                    && !empty($projet['Deliberation']['typeacte_id'])
                    && !empty($droits['Typeacte/update'])
                ) {
                    if (in_array($projet['Deliberation']['typeacte_id'], $droits['Typeacte/update'], true)) {
                        array_push($actions, 'edit');
                    }
                }
                if (!empty($droits['supprimerTous/read'])
                    && !empty($projet['Deliberation']['typeacte_id'])
                    && !empty($droits['Typeacte/delete'])
                ) {
                    if (in_array($projet['Deliberation']['typeacte_id'], $droits['Typeacte/delete'], true)) {
                        array_push($actions, 'delete');
                    }
                }
                break;
            case 2: //Validé
                if (!empty($droits['editerTous/read'])
                    && !empty($projet['Deliberation']['typeacte_id'])
                    && !empty($droits['Typeacte/update'])
                ) {
                    array_push($actions, 'attribuerCircuit');
                    if (in_array($projet['Deliberation']['typeacte_id'], $droits['Typeacte/update'], true)) {
                        array_push($actions, 'edit');
                    }
                }
                // Pouvoir ajouter un circuit de validation
                if (!empty($droits['reattribuerTous/read'])) {
                    array_push($actions, 'attribuerCircuit');
                }
                if (!empty($droits['supprimerTous/read'])
                    && !empty($projet['Deliberation']['typeacte_id'])
                    && !empty($droits['Typeacte/delete'])
                ) {
                    if (in_array($projet['Deliberation']['typeacte_id'], $droits['Typeacte/delete'], true)) {
                        array_push($actions, 'delete');
                    }
                }
                break;
            case 3: //Validé ou voter pour
            case 4: //voter contre
                if (empty($projet['Deliberation']['signee'])
                    && in_array($projet['Deliberation']['parapheur_etat'], [null, 0, -1], true)
                    && !empty($droits['editerTous/read'])
                ) {
                    // Pouvoir éditer un projet voté si on a les droits sur le type d'acte
                    if (in_array($projet['Deliberation']['typeacte_id'], $droits['Typeacte/update'], true)) {
                        array_push($actions, 'edit');
                    }
                } elseif (!empty($projet['Deliberation']['signee'])) {
                    //Si le projet est en cours de signature ou signé alors on télécharge le document
                    array_push($actions, 'telecharger');
                    // Pouvoir editer les actes post signature
                    if (!empty($droits['editPostSign/read'])) {
                        array_push($actions, 'editPostSign');
                    }
                    unset($actions[array_search('generer', $actions, true)]);
                }
                if (!empty($droits['toSend/read']) || !empty($droits['autresActesAEnvoyer/read'])) {
                    array_push($actions, 'sendToTdt');
                }
                break;
            case 10: //Renvoyer en signature
                if (!empty($droits['editerTous/read'])
                    && !empty($projet['Deliberation']['typeacte_id'])
                    && !empty($droits['Typeacte/update'])
                ) {
                    array_push($actions, 'attribuerCircuit');
                    if (in_array($projet['Deliberation']['typeacte_id'], $droits['Typeacte/update'], true)) {
                        array_push($actions, 'edit');
                    }
                }
                // Pouvoir ajouter un circuit de validation
                if (!empty($droits['reattribuerTous/read'])) {
                    array_push($actions, 'attribuerCircuit');
                }
                break;
            default:
                if (!empty($projet['Deliberation']['signee'])) {
                    array_push($actions, 'telecharger');
                    unset($actions[array_search('generer', $actions, true)]);
                    // Pouvoir editer les actes post signature
                    if (!empty($droits['editPostSign/read'])) {
                        array_push($actions, 'editPostSign');
                    }
                }
                break;
        }

        //Ajout des actions optionnnels
        foreach ($listeActions as $action_disponible) {
            if (!in_array($action_disponible, $actions, true)) {
                array_push($actions, $action_disponible);
            }
        }

        //Gestion des multidélibération
        if (!empty($projet['Deliberation']['parent_id'])) {
            $delete_actions = [
                'edit',
                'duplicate',
                'goNext',
                'validerEnUrgence',
                'delete',
                'attribuerCircuit',
                'traiter'
            ];
            foreach ($actions as $key => $action) {
                if (in_array($action, $delete_actions, true)) {
                    unset($actions[$key]);
                }
            }
        }

        $projet['Actions'] = $actions;
    }

    /**
     * @access private
     * @param type $conditions
     * @return type
     *
     * @version 5.1.2
     * @since 4.3
     */
    public function handleConditions($conditions)
    {
        $projet_type_ids = [];
        $projet_seance_ids = [];

        if (isset($conditions['DeliberationTypeseance.typeseance_id'])) {
            $type_id = $conditions['DeliberationTypeseance.typeseance_id'];
            $typeseances = $this->DeliberationTypeseance->find(
                'all',
                [
                    'conditions' => ['DeliberationTypeseance.typeseance_id' => $type_id],
                    'recursive' => -1
                ]
            );
            foreach ($typeseances as $typeseance) {
                $projet_type_ids[] = $typeseance['DeliberationTypeseance']['deliberation_id'];
            }
            unset($conditions['DeliberationTypeseance.typeseance_id']);
        }
        if (isset($conditions['DeliberationSeance.seance_id'])) {
            $projet_seance_ids = $this->Seance->getDeliberationsId($conditions['DeliberationSeance.seance_id']);
            unset($conditions['DeliberationSeance.seance_id']);
        }

        if (isset($conditions['Deliberation.id'])) {
            $conditions['Deliberation.id'] = array_intersect($conditions['Deliberation.id'], $result);
        }

        if (isset($conditions['AND']['Deliberation.etat'])) {
            if ($conditions['AND']['Deliberation.etat'] === 6) {
                $conditions['NOT']['Deliberation.num_delib'] = null;
                unset($conditions['AND']['Deliberation.etat']);
            }
        }

        return ($conditions);
    }

    /**
     * @version 4.3
     * @access private
     * @param type $projets
     */
    public function ajouterFiltreSeance(&$projets)
    {
        if (!$this->Filtre->critereExists() && !empty($projets)) {
            $typeactes = $this->Deliberation->Typeacte->find('list', [
                'recursive' => -1,
                'allow' => ['Typeacte.id' => 'read']]);
            $this->Filtre->addCritere('Typeacte', [
                'field' => 'Deliberation.typeacte_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Type d\'acte'),
                    'data-placeholder' => __('Sélectionner un type d\'acte'),
                    'data-allow-clear' => true,
                    'options' => $typeactes
                ]]);

            $themes = $this->Deliberation->Theme->generateTreeListByOrder(
                ['Theme.actif' => '1'],
                '&nbsp;&nbsp;&nbsp;&nbsp;'
            );

            $this->Filtre->addCritere('ThemeId', [
                'field' => 'Deliberation.theme_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un thème'),
                    'data-allow-clear' => true,
                    'label' => __('Thème'),
                    'options' => $themes,
                    'escape' => false
                ]]);

            $services = $this->Service->find('list', [
                'fields' => ['id', 'name'],
                'conditions' => ['actif' => true], 'recursive' => -1]);
            $this->Filtre->addCritere('ServiceId', [
                'field' => 'Deliberation.service_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un service émetteur'),
                    'data-allow-clear' => true,
                    'label' => __('Service émetteur'),
                    'multiple' => true,
                    'options' => $services
                ]]);

            $circuits = $this->Circuit->find('list', [
                'fields' => ['id', 'nom'],
                'conditions' => ['actif' => true], 'recursive' => -1]);
            $this->Filtre->addCritere('CircuitId', [
                'field' => 'Deliberation.circuit_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un circuit'),
                    'data-allow-clear' => true,
                    'label' => __('Circuit de validation'),
                    'empty' => __('Tous'),
                    'options' => $circuits
                ]]);
        }
    }

    /**
     * [checkTypeSeance description]
     * @return [type] [description]
     */
    public function check($id, $crud)
    {
        if (!$this->Acl->check(
            ['User' => ['id' => $this->Auth->user('id')]],
            ['Deliberation' => ['id' => $id]],
            $crud
        )
        ) {
            $this->Flash->set(
                __(
                    "Vous ne pouvez pas %s la seance '%s' en raison de son type de seance.",
                    ($crud === 'update' ? __('éditer') : ($crud === 'delete' ? __('supprimer') : '')),
                    $id
                ),
                [
                    'element' => 'growl',
                    'params' => ['type' => 'danger']]
            );

            return $this->redirect($this->previous);
        }
    }

    /**
     * Génère la liste des états
     *
     * @param $action String Permet de spécifier une action (utilisé pour les filtres)
     * @return array
     * @version 5.1
     * @since 4.3
     * @access public
     */
    public function generateListEtat($action = "default")
    {
        App::uses('ProjetEtat', 'Model');
        $projetEtat = ClassRegistry::init('ProjetEtat');

        $labels = [];
        switch ($action) {
            case 'mesProjetsRedaction':
            case 'mesProjetsValidation':
            case 'mesProjetsATraiter':
                break;
            case 'tousLesProjetsValidation':
                $labels = [1 => __('En cours d\'élaboration et de validation')];
                break;
            case 'tousLesProjetsSansSeance':
                $labels = [
                    0 => __('En cours de rédaction'),
                    1 => __('En cours d\'élaboration et de validation'),
                    2 => __('Validé')];
                break;
            case 'tousLesProjetsAFaireVoter':
                $labels = [2 => _('Validé')];
                break;
            case 'autresActesValides':
                $labels = [
                    2 => __('Validé (non numéroté)'),
                    6 => __('Numéroté'),
                ];

                break;
            case 'autresActesAValider':
                $labels = [
                    0 => __('En cours de rédaction'),
                    1 => __('En cours d\'élaboration et de validation'),
                    -3 => __('Abandonné'),
                ];
                break;
            case 'autresActesAEnvoyer':
                $labels = [
                    7 => __('Signé'),
                    -3 => __('Abandonné'),
                ];
                break;
            case 'autresActesEnvoyes':
                $labels = [
                    5 => __('Transmis au contrôle de légalité.'),
                    11 => __('Transmis manuellement au contrôle de légalité.'),
                    -3 => __('Abandonné'),
                ];
                break;
            case 'nonTransmis':
                $labels = [
                    3 => __('Voté et adopté'),
                    4 => __('Voté et rejeté'),
                    7 => __('Signé')];
                break;
            case 'projetsMonService':
                $labels = [
                    0 => __('En cours de rédaction'),
                    1 => __('En cours d\'élaboration et de validation'),
                    2 => __('Validé')];
                break;
            default:
                $labels = $projetEtat->getLibellesEtat();
                //FIX
                unset($labels[100]);
                break;
        }
        return $labels;
    }

    /**
     *     //Fix $selectedTypeseanceIds
     * @param type $typeacte_id
     * @param type $projet_id
     * @param type $selectedTypeseanceIds
     * @return array
     */
    public function getTypeseancesParTypeacte(
        $typeacte_id = null,
        $projet_id = null,
        $selectedTypeseanceIds = null,
        $selectedSeanceIds = null
    ) {

        //Gestion des droits : editer des projets Validés
        $canEditAll = $this->Acl->check(['User' => ['id' => $this->Auth->user('id')]], 'editerTous', 'read');

        $deliberanteId = !empty($projet_id) ? $this->Deliberation->getSeanceDeliberanteId($projet_id) : false;

        if ($typeacte_id == null) {
            $typeseances = null;
        } else {
            App::uses('TypeseancesTypeacte', 'Model');
            $typeseancesTypeacte = ClassRegistry::init('TypeseancesTypeacte');
            App::uses('Typeseance', 'Model');
            $typeseance = ClassRegistry::init('Typeseance');

            $typeseance_ids = $typeseancesTypeacte->getTypeseanceByTypeActe($typeacte_id);
            $typeseances = $typeseance->find('all', [
                'fields' => ['Typeseance.id', 'Typeseance.libelle', 'Typeseance.retard'],
                'conditions' => ['Typeseance.id' => $typeseance_ids],
                'order' => ['Typeseance.libelle' => 'ASC'],
                'recursive' => -1,
                'allow' => ['Typeseance.id']
            ]);
        }

        $options = [];
        if (!empty($typeseances)) {
            if (Set::check($selectedSeanceIds, '0.seance_id')) {
                $selectedSeanceIds = Set::extract('/seance_id', $selectedSeanceIds);
            }
            foreach ($typeseances as $typeseance) {
                $contain = null;
                if (!empty($projet_id)) {
                    $contain = [
                        'DeliberationSeance' => [
                            'conditions' => ['deliberation_id' => $projet_id]
                        ]];
                }
                $seances = $this->Seance->find('all', [
                    'fields' => ['Seance.id', 'Seance.type_id', 'Seance.date'],
                    'conditions' => [
                        'Seance.type_id' => $typeseance['Typeseance']['id'],
                        'Seance.traitee' => 0],
                    'contain' => $contain,
                    'recursive' => -1
                ]);

                if ($projet_id !== null) {
                    $deliberationTypeseances = $this->DeliberationTypeseance->find('all', [
                        'fields' => ['deliberation_id', 'typeseance_id'],
                        'conditions' => [
                            'typeseance_id' => $typeseance['Typeseance']['id'],
                            'deliberation_id' => $projet_id],
                        'recursive' => -1]);
                }

                $disabled = true;
                $selected = false;
                $selectedSeance = false;
                if (!empty($selectedTypeseanceIds)) {
                    //Dans le cas d'un find
                    if (Set::check($selectedTypeseanceIds, '0.typeseance_id')) {
                        $selectedTypeseanceIds = Set::extract('/typeseance_id', $selectedTypeseanceIds);
                    }
                    //Si l'on sélectionne le type de séance lors d'un post en erreur
                    if (in_array($typeseance['Typeseance']['id'], $selectedTypeseanceIds, false)) {
                        $selected = true;
                        $disabled = false;
                        //Si l'on sélectionne une séance lors d'un post en erreur
                        if (!empty($selectedSeanceIds)) {
                            foreach ($seances as $seance) {
                                if (in_array($seance['Seance']['id'], $selectedSeanceIds, false)) {
                                    $selectedSeance = true;
                                    $disabled = true;
                                    $selected = true;
                                }
                            }
                        }
                        goto options;
                    }
                }

                foreach ($seances as $seance) {
                    $dateSeanceTime = strtotime($seance['Seance']['date']);
                    $retardNbrJour = !empty($seance['Typeseance']['retard']) ? $seance['Typeseance']['retard'] : 0;
                    $retardTime = mktime(
                        0,
                        0,
                        0,
                        date('m', $dateSeanceTime),
                        (date('d', $dateSeanceTime) - $retardNbrJour),
                        date('Y', $dateSeanceTime)
                    );

                    if ($projet_id !== null) {
                        $selected = Hash::check(
                            $seances,
                            '{n}.DeliberationSeance.{n}[deliberation_id=' . $projet_id . '][seance_id='
                            . $seance['Seance']['id'] . ']'
                        );
                    }
                    if (!$selected && !$canEditAll && (time() > $retardTime)) {
                        continue;
                    } elseif ($canEditAll && $selected) {
                        $disabled = false;
                    } elseif (!$selected && (empty($deliberanteId) || !empty($seance['Typeseance']['action']))) {
                        $disabled = false;
                    }
                    if (!$disabled) {
                        break;
                    }
                }

                //Si pas de sélection en POST ni en base de donnée
                if ($disabled && (empty($deliberationTypeseances) || empty($seances))) {
                    $disabled = false;
                }

                options:
                $options[] = [
                    'name' => $typeseance['Typeseance']['libelle'],
                    'value' => $typeseance['Typeseance']['id'],
                    'disabled' => $disabled,
                    'selected' => $selected
                ];
            }
        }
        return $options;
    }

    /**
     *
     * @param array $selectedTypeseanceIds
     * @param int $projet_id
     * @param array $selectedSeanceIds
     * @return boolean
     */
    public function getSeancesParTypeseance($selectedTypeseanceIds, $projet_id = null, $selectedSeanceIds = null)
    {

        //Gestion des droits : editer des projets Validés
        $canEditAll = $this->Acl->check(
            ['User' => ['id' => $this->Auth->user('id')]],
            'editerTous',
            'read'
        );
        $options = [];
        $seances_ids = [];
        $deliberanteId = !empty($projet_id) ? $this->Deliberation->getSeanceDeliberanteId($projet_id) : false;
        if ($selectedTypeseanceIds != 'null') {
            if (!empty($projet_id)) {
                $seances_ids = $this->DeliberationSeance->find('list', [
                    'fields' => ['DeliberationSeance.seance_id'],
                    'conditions' => ['DeliberationSeance.deliberation_id' => $projet_id],
                    'allow' => ['Typeseance.id']
                ]);
            }
            if (Set::check($selectedTypeseanceIds, '0.typeseance_id')) {
                $selectedTypeseanceIds = Set::extract('/typeseance_id', $selectedTypeseanceIds);
            }
            if (!empty($selectedSeanceIds) && Set::check($selectedSeanceIds, '0.seance_id')) {
                $selectedSeanceIds = Set::extract('/seance_id', $selectedSeanceIds);
            }

            $seances = $this->Seance->find('all', [
                'fields' => ['Seance.id', 'Seance.type_id', 'Seance.date', 'Seance.traitee'],
                'conditions' => [
                    'Seance.type_id' => $selectedTypeseanceIds,
                    'AND' => [
                        'OR' => [
                            'Seance.traitee' => 0,
                            'Seance.id' => $seances_ids]
                    ],
                ],
                'contain' => [
                    'Typeseance' => [
                        'fields' => ['libelle', 'retard', 'action']],
                    'DeliberationSeance' => [
                        'fields' => ['deliberation_id']],],
                'order' => [
                    'Typeseance.libelle' => 'ASC',
                    'Seance.date' => 'ASC'],
                'recursive' => -1,
                'allow' => ['typeseance_id']
            ]);

            foreach ($seances as $seance) {
                $dateSeanceTime = strtotime($seance['Seance']['date']);
                $retardNbrJour = !empty($seance['Typeseance']['retard']) ? $seance['Typeseance']['retard'] : 0;
                $traitee = $seance['Seance']['traitee'];
                $retardTime = mktime(
                    0,
                    0,
                    0,
                    date('m', $dateSeanceTime),
                    (date('d', $dateSeanceTime) - $retardNbrJour),
                    date('Y', $dateSeanceTime)
                );
                $selected = false;
                $disabled = true;

                //Si l'on sélectionne un séance lors d'un post en erreur
                if (!empty($selectedSeanceIds)) {
                    if (is_array($selectedSeanceIds) && in_array($seance['Seance']['id'], $selectedSeanceIds, false)) {
                        $selected = true;
                    } elseif ($seance['Seance']['id']===$selectedSeanceIds) {
                        $selected = true;
                    }
                } elseif (!empty($projet_id)) {
                    $selected = Hash::check(
                        $seances,
                        '{n}.DeliberationSeance.{n}[deliberation_id='
                        . $projet_id . '][seance_id=' . $seance['Seance']['id'] . ']'
                    );
                }

                if (!$selected && !$canEditAll && (time() > $retardTime)) {
                    continue;
                } elseif ($selected && ($canEditAll || (time() < $retardTime)) && !$traitee) {
                    $disabled = false;
                } elseif (!$selected
                    && (empty($deliberanteId) || !empty($seance['Typeseance']['action']))
                    && !$traitee
                ) {
                    $disabled = false;
                }
                $options[] = [
                    'name' => $seance['Typeseance']['libelle'] . ' : ' .
                        CakeTime::i18nFormat($seance['Seance']['date'], '%A %e %B %Y') .
                        ' à ' . CakeTime::i18nFormat($seance['Seance']['date'], '%k:%M'),
                    'value' => $seance['Seance']['id'],
                    'data-wd-typeseance-id' => $seance['Typeseance']['id'],
                    'data-wd-typeseance-action' => $seance['Typeseance']['action'],
                    'disabled' => $disabled,
                    'selected' => $selected
                ];
            }
        }

        return $options;
    }

    /**
     * Retourne un ensemble d'id de délibération si elles ont été cochées
     * @return array|CakeResponse|null
     */
    public function traitementParLotVerification()
    {
        $deliberationIds= [];

        if (isset($this->request->data['Deliberation']['check'])) {
            foreach ($this->request->data['Deliberation']['check'] as $tmp_id => $bool) {
                if ($bool) {
                    $delib_id = substr($tmp_id, 3, strlen($tmp_id));
                    $deliberationIds[] = $delib_id;
                }
            }
        } else {
            foreach ($this->request->data['Deliberation'] as $projet_id => $projet) {
                if (!empty($projet['is_checked']) && $projet['is_checked']) {
                    $deliberationIds[] = $projet_id;
                }
            }
        }
        if (!isset($deliberationIds) || (isset($deliberationIds) && count($deliberationIds) == 0)) {
            $this->Flash->set(
                __('Veuillez sélectionner un projet.'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            return $this->redirect($this->previous);
        }
        return $deliberationIds;
    }

    /**
     * Récupération des permissions de l'utilisateur permettant de calculer
     * les actions disponibles pour un projet donné.
     *
     * @see ProjetToolsComponent::actionDisponible
     *
     * @param array $filter Les permissions que l'on souhaite récupérer. Laisser à vide pour récupérer toutes les
     *  permssions nécessaires.
     * @return array
     */
    public function getDroits(array $filter = [])
    {
        $available = [
            'Projets' => ['read', 'update', 'delete'],
            'Projets/editerTous' => 'read',
            'Projets/supprimerTous' => 'read',
            'Projets/duplicate' => 'read',
            'AutresActes/autresActesAEnvoyer' => 'read',
            'Actes/editPostSign' => 'read',
            'Validations/attribuerCircuit' => 'read',
            'Validations/reattribuerTous' => 'read',
            'Validations/goNext' => 'read',
            'Validations/validerEnUrgence' => 'read',
            'Postseances/toSend' => 'read',
        ];
        $aliases = [];
        foreach (array_keys($available) as $key) {
            $tokens = explode('/', $key);
            if (count($tokens) > 1) {
                $aliases[$tokens[count($tokens) - 1]] = $key;
            }
        }

        $droits = [];
        $errors = [];
        if (!empty($filter)) {
            foreach ($filter as $aco => $cruds) {
                if (array_key_exists($aco, $aliases)) {
                    $aco = $aliases[$aco];
                }
                if (array_key_exists($aco, $available)) {
                    $droits[$aco] = [];
                    foreach ((array)$cruds as $crud) {
                        if (in_array($crud, (array)$available[$aco])) {
                            $droits[$aco][] = $crud;
                        } else {
                            $format = 'La valeur CRUD "%s" pour l\'Aco "%s" passée en filtre de la méthode %s n\'est '
                                . 'pas disponible.';
                            $errors[] = sprintf($format, $crud, $aco, __METHOD__);
                        }
                    }
                } else {
                    $format = 'La valeur de l\'Aco "%s" passée en filtre de la méthode %s n\'est pas disponible.';
                    $errors[] = sprintf($format, $aco, __METHOD__);
                }
            }
        } else {
            $droits = $available;
        }

        if (!empty($errors)) {
            $droits = $available;
            $this->log(str_repeat('-', 120));
            $format = 'Erreur lors de la récupération des droits pour l\'utilisateur %s à l\'adresse /%s';
            $this->log(sprintf($format, $this->Session->read('Auth.User.username'), $this->request->url));
            foreach ($errors as $error) {
                $this->log($error);
            }
            $this->log(str_repeat('-', 120));
        }

        $result = [];

        foreach ($droits as $aco => $cruds) {
            $tokens = explode('/', $aco);
            $last = $tokens[count($tokens) - 1];
            foreach ((array)$cruds as $crud) {
                $result[$last . '/' . $crud] = $this->Permissions->check($aco, $crud);
            }
        }

        $Typeacte = ClassRegistry::init('Typeacte');

        //Droit sur les types de projet
        $result['Typeacte/delete'] = $Typeacte->find('list', [
            'fields' => ['id'],
            'recursive' => -1,
            'allow' => ['Typeacte.id' => 'delete']
        ]);
        $result['Typeacte/update'] = $Typeacte->find('list', [
            'fields' => ['id'],
            'recursive' => -1,
            'allow' => ['Typeacte.id' => 'update']
        ]);

        return $result;
    }
}
