<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('AppTools', 'Lib');
App::uses('ConnectorS2low', 'Lib');


/**
 * [S2lowComponent description]
 */
class S2lowComponent extends Component
{
    private $s2low;

    /*
     * @version v5.1.0
     */
    public function __construct(ComponentCollection $collection, $settings = [])
    {
        parent::__construct($collection, $settings);
        $this->s2low = new ConnectorS2low();
    }

    public function saveClassification($reponse)
    {
        return $this->s2low->saveClassification($reponse);
    }

    /**
     * @version 5.1.0
     * @param type $acte
     * @return type
     */
    public function send($acte)
    {
        return $this->s2low->send($acte);
    }

    /**
   * @version 5.1.0
   * @return boolean
   */
    public function testLogin()
    {
        return $this->s2low->testLogin();
    }

    /**
     * @version 5.1.0
     * @return boolean
     */
    public function getClassification()
    {
        return $this->s2low->getClassification();
    }

    /**
     * @version 5.1.0
     * @param type $tdt_id
     * @return type
     */
    public function getFluxRetour($tdt_id)
    {
        return $this->s2low->getFluxRetour($tdt_id);
    }

    /**
     * @version 5.1.0
     */
    public function getActeTampon($tdt_id)
    {
        return $this->s2low->getActeTampon($tdt_id);
    }

    /**
     * @version 5.1.0
     */
    public function getCourrier($tdt_id)
    {
        return $this->s2low->getCourrier($tdt_id);
    }

    /**
     *
     * @version 5.1.0
     * @param type $tdt_id
     * @param type $ordre
     * @return type
     */
    public function getActeAnnexesTampon($tdt_id, $ordre)
    {
        return $this->s2low->getActeAnnexesTampon($tdt_id, $ordre);
    }

    /**
     * @version 5.1.0
     * @param type $tdt_id
     * @return type
     */
    public function getActeBordereau($tdt_id)
    {
        return $this->s2low->getActeBordereau($tdt_id);
    }

    /**
     * @version 5.1.0
     * @param type $mail_id
     * @return type
     */
    public function checkMail($mail_id)
    {
        return $this->s2low->checkMail($mail_id);
    }
    /**
     * @param type $fluxRetour
     * @return type
     */
    public function getDateAR($fluxRetour)
    {
        // +21 Correspond a la longueur du string : actes:DateReception"
        return substr($fluxRetour, strpos($fluxRetour, 'actes:DateReception') + 21, 10);
    }

    /**
     * @return type
     */
    public function getIDActe($fluxRetour)
    {
        // Utilisé ?
        //$fluxRetour;
        // +21 Correspond a la longueur du string : actes:DateReception"
        return substr($fluxRetour, strpos($fluxRetour, 'actes:DateReception') + 21, 10);
    }
    /**
     * @version 5.1.0
     * @param type $tdt_id
     * @return type
     */
    public function getNewFlux($tdt_id)
    {
        return $this->s2low->getNewFlux($tdt_id);
    }
    /**
     * @version 5.1.0
     * @param type $document_id
     * @return boolean
     * @throws Exception
     */
    public function getDocument($document_id)
    {
        return $this->s2low->getDocument($document_id);
    }

    /**
     *
     * @param type $delib_id
     * @param type $type
     * @param type $reponse
     * @param type $message_id
     * @return type
     */
    public function isNewMessage($delib_id, $type, $reponse, $message_id)
    {
        $message = $this->TdtMessage->find('first', ['conditions' =>
            ['TdtMessage.delib_id' => $delib_id,
                'TdtMessage.type_message' => $type,
                'TdtMessage.reponse' => $reponse,
                'TdtMessage.message_id' => $message_id]]);
        return (empty($message));
    }
}
