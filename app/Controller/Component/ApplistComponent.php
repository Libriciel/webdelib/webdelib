<?php

/**
 * Composant Applist
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       web-delib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 * @version     v4.3
 * @package     app.Controller.Component
 *
 * @author Florian Ajir
 */
class ApplistComponent extends Component
{
    public $components = [];

    /**
     *
     * @param type $type
     * @return type
     */
    public function get($type)
    {
        return App::objects($type);
    }

    /**
     *
     * @param type $plugin
     * @return type
     */
    public function getPluginControllers($plugin)
    {
        return App::objects('Controller', App::pluginPath($plugin) . 'Controller' . DS, false);
    }

    /**
     *
     * @param type $plugin
     * @return type
     */
    public function getPluginModels($plugin)
    {
        return App::objects('Model', App::pluginPath($plugin) . 'Model' . DS, false);
    }

    /**
     *
     * @param type $controller
     * @param type $plugin
     * @return type
     */
    public function getControllerMethods($controller, $plugin = null)
    {
        // Load the controller
        if ($plugin == null) {
            App::import('Controller', str_replace('Controller', '', $controller));
        } else {
            App::import('Controller', $plugin . '.' . str_replace('Controller', '', $controller));
        }
        // Load its methods / actions
        $aMethods = get_class_methods($controller);

        foreach ($aMethods as $idx => $method) {
            if ($method{0} == '_') {
                unset($aMethods[$idx]);
            }
        }
        // Load the ApplicationController (if there is one)
        App::import('Controller', 'AppController');
        $parentActions = get_class_methods('AppController');

        $methods = array_diff($aMethods, $parentActions);

        return $methods;
    }

    /**
     *
     * @param type $model
     * @param type $plugin
     * @return type
     */
    public function getModelMethods($model, $plugin = null)
    {
        // Load the controller
        if ($plugin == null) {
            App::import('Model', $model);
        } else {
            App::import('Model', $plugin . '.' . $model);
        }
        // Load its methods / actions
        $aMethods = get_class_methods($model);

        foreach ($aMethods as $idx => $method) {
            if ($method{0} == '_') {
                unset($aMethods[$idx]);
            }
        }
        // Load the ApplicationController (if there is one)
        App::import('Model', 'AppModel');
        $parentActions = get_class_methods('AppModel');

        $methods = array_diff($aMethods, $parentActions);

        return $methods;
    }

    /**
     *
     * @return type
     */
    public function construireArbreController()
    {
        $plugin_ctrl_method = [];
        foreach ($this->get('plugins') as $plugin) {
            foreach ($this->getPluginControllers($plugin) as $ctrl) {
                $plugin_ctrl_method[$plugin][$ctrl] = $this->getControllerMethods($ctrl, $plugin);
            }
        }
        foreach ($this->get('controller') as $ctrl) {
            $plugin_ctrl_method[''][$ctrl] = $this->getControllerMethods($ctrl);
        }
        return $plugin_ctrl_method;
    }

    /**
     *
     * @return type
     */
    public function construireArbreModel()
    {
        $plugin_ctrl_method = [];
        foreach ($this->get('plugins') as $plugin) {
            foreach ($this->getPluginModels($plugin) as $model) {
                $plugin_ctrl_method[$plugin][$model] = $this->getModelMethods($model, $plugin);
            }
        }
        foreach ($this->get('model') as $model) {
            $plugin_ctrl_method[''][$model] = $this->getModelMethods($model);
        }
        return $plugin_ctrl_method;
    }
}
