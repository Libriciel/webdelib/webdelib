<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('HttpSocketResponse', 'Network/Http');
App::uses('HttpSocket', 'Network/Http');
App::uses('AppTools', 'Lib');

/**
 * [PDFStampComponent description]
 * @package     app.Controller.Component
 * @version     v5.1
 * @since       v4.3.0
 */
class PDFStampComponent extends Component
{
    public function getHttpSocket($config)
    {
        if (!is_null($this->httpSocket)) {
            return $this->httpSocket;
        }
        return $this->httpSocket = new HttpSocket($config);
    }

    /**
     * [__construct description]
     */
    public function setHttpSocket($httpSocket)
    {
        if ($httpSocket instanceof HttpSocket) {
            $this->httpSocket = $httpSocket;
        }
    }

    /**
     * @return HttpSocket
     */
    private function getClient()
    {
        return $this->getHttpSocket([
            'request' => [
                'uri' =>  Configure::read('PDFStamp.host'),
            ],
            'timeout' => AppTools::env('APP_PDFSTAMP_CONFIG_TIMEOUT', 600),
            'ssl_verify_peer' => false,
            'ssl_allow_self_signed' => false
        ]);
    }

    /**
     * @throws Exception
     */
    public function stamp($fileStream, $dataAR)
    {
        $identifiant = $dateReception = $dateAquittement = null;
        if (!empty($dataAR['tdt_ar']) && $this->checkXML($dataAR['tdt_ar'])) {
            $dateReception = $dateAquittement = substr(
                $dataAR['tdt_ar'],
                strpos(
                    $dataAR['tdt_ar'],
                    'actes:DateReception='
                ) + 21,
                10
            );
            $xml = simplexml_load_string($dataAR['tdt_ar']);
            $identifiant = ((array)$xml->xpath('/actes:ARActe/@actes:IDActe')[0])['@attributes']['IDActe'];
        }

        $date_affichage = "";
        if (!empty($dataAR['publier_date'])) {
            $date_affichage = $this->getDateFr($dataAR['publier_date']);
        }

        $data = [
            'opacity'=> 0.8,
            'fontSize' => 7,
            'position' => [
                'width' => 190,
                'height' => 55,
                'x'=> 10,
                'y'=>10
            ],
            'rows' => []
        ];
        if (!empty($dateReception)) {
            $data['rows'][]= [
                'title' => 'Envoyé en préfecture le',
                'value' => $this->getDateFr($dateReception)
            ];
        }
        if (!empty($dateAquittement)) {
            $data['rows'][] =                 [
                'title' => 'Reçu en préfecture le',
                'value' => $this->getDateFr($dateAquittement),
            ];
        }

        if (file_exists(Configure::read('PDFStamp.logo'))) {
            $imgData = base64_encode(file_get_contents(Configure::read('PDFStamp.logo')));
        } else {
            $imgData = base64_encode(file_get_contents(PDFSTAMP_LOGO));
        }
        $data['rows'][] = [
            'title' => 'Publié le',
            'value' => $date_affichage,
            'logo' => [
                'data' => $imgData,
                "width" => 60,
                "marginRight" => 30
            ]
        ];
        if (!empty($identifiant)) {
            $data['rows'][] = [
                'title' => 'ID :',
                'value' => $identifiant
            ];
        }

        $metadataJSON = json_encode($data, JSON_THROW_ON_ERROR);

        // Définir la boundary pour le corps de la requête
        $boundary = '----WebKitFormBoundary' . md5(time());
        // Construire le corps de la requête
        $body = "--$boundary\r\n";
        // Ajout de metadata
        $body .= 'Content-Disposition: form-data; name="metadata"' . "\r\n";
        $body .= "Content-Type: application/json\r\n\r\n";
        $body .= $metadataJSON . "\r\n";
        // Ajout du fichier
        $body .= "--$boundary\r\n";
        $body .= 'Content-Disposition: form-data; name="file"; filename="PDFStamp.pdf' . "\"\r\n";
        $body .= "Content-Type: application/pdf\r\n\r\n";
        $body .= $fileStream . "\r\n";
        $body .= "--$boundary--\r\n";

        $response = $this->getClient()->post('/pdf-stamp/', $body, [
            'header' => [
                'Content-Type' => 'multipart/form-data; boundary=' . $boundary,
                'Content-Length' => strlen($body)
            ]
        ]);

        if (!$response->isOk()) {
            throw new Exception('Erreur PDFStamp code:' . $response->code);
        }

        return $response;
    }

    private function checkXML($xmlstr)
    {
        libxml_use_internal_errors(true);
        $doc = simplexml_load_string($xmlstr);
        if (!$doc) {
            $errors = libxml_get_errors();
            if (count($errors)) {
                libxml_clear_errors();
                return false;
            }
        }
        return true;
    }

    private function getDateFr($date)
    {
        return date('d/m/Y', strtotime($date));
    }
}
