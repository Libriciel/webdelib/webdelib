<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('MethodCurl', 'Lib');
App::uses('DockerRawStream', 'Lib');

use http\Encoding\Stream;
use Psr\Http\Message\StreamInterface;

/**
 * [PDFStampComponent description]
 * @package     app.Controller.Component
 * @version     v5.1
 * @since       v4.3.0
 */
class PortainerComponent extends Component
{
    private $portainer_url;
    private $portainer_user;
    private $portainer_password;
    private $portainer_jwt;
    private $portainer_environment_id;
    private $curl = null;
    /**
     * @var mixed
     */
    private $docker_state;

    /**
     * @param type $controller
     * @throws JsonException
     * @version 5.1
     */
    public function initialize(Controller $controller)
    {
        $this->portainer_url = Configure::read('portainer.host');
        $this->portainer_user = Configure::read('portainer.admin');
        $this->portainer_password = Configure::read('portainer.password');
        try {
            $this->login();
            $this->curl = null;
            $this->environment();
            $this->getStatusAll();
        } catch (Exception $e) {
        }
    }

    /**
     *  "Lazy loading" de la classe WebdelibCurl.
     * Effectue la requête pour récupérer le nounce si l'authentification se fait par certificat + login
     * @version 5.1.4
     * @since 5.1.0
     * @return WebdelibCurl
     */
    public function curl()
    {
        if (null === $this->curl) {
            $options["CurlOptions"] = [
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_RETURNTRANSFER => true

            ];

            if (!empty($this->portainer_jwt)) {
                $options["CurlOptions"][CURLOPT_HTTPAUTH] = CURLAUTH_BEARER;
                $options["CurlOptions"][CURLOPT_XOAUTH2_BEARER] =  $this->portainer_jwt;
            }

            $this->curl = new MethodCurl($this->portainer_url. '/api', $options);
        }

        return $this->curl;
    }

    /**
     * @throws JsonException
     */
    public function login()
    {
        $json = $this->curl()->doRequest(
            '/auth',
            [
                "PostParameters" => json_encode([
                "username" => $this->portainer_user,
                "password" => $this->portainer_password
                    ], JSON_THROW_ON_ERROR)
            ]
        );

        $this->portainer_jwt = json_decode($json, true, 512, JSON_THROW_ON_ERROR)['jwt'];
    }

    /**
     * @throws JsonException
     */
    public function environment()
    {
        $json = $this->curl()->doRequest(
            '/endpoints',
            [
                "GetParameters" => [
                    "name" => "webdelibapp",
                ]
            ]
        );

        $json = json_decode(trim($json), true, 512, JSON_THROW_ON_ERROR);

        if (is_array($json) && count($json) == 0) {
            $this->createEnvironment();
            return;
        }

        $this->portainer_environment_id = $json[0]['Id'];
    }

    public function createEnvironment()
    {
        $json = $this->curl()->doRequest(
            '/endpoints',
            [
                "PostParameters" => [
                    "Name" => "webdelibapp",
                    "EndpointCreationType" => 1
                ]
            ]
        );

        $this->portainer_environment_id = json_decode(
            trim($json),
            true,
            512,
            JSON_THROW_ON_ERROR
        )[0]['Id'];
    }


    public function status()
    {
        return $this->curl()->doRequest('/status');
    }

    public function version()
    {
        return $this->curl()->doRequest('/status/version');
    }

    /**
     * @throws Exception
     */
    public function getAll($query = ['all'=>'true'])
    {
        return $this->curl()->doRequest(
            '/endpoints/'.$this->portainer_environment_id.'/docker/containers/json',
            ["GetParameters" =>$query]
        );
    }

    public function getFilterStackName(string $name)
    {
        return $this->getAll(['all'=>1, 'filters' =>'{"label":["com.docker.compose.service='.$name.'"]}']);
    }

    public function getFilterDockerId(string $id, string $query)
    {
        return $this->curl()->doRequest(
            '/endpoints/'.$this->portainer_environment_id.'/docker/containers/'.$id.'/json',
            ["GetParameters" =>$query]
        );
    }
    public function getDockerState(string $name)
    {
        $state = null;

        if ($this->docker_state !== null) {
            foreach ($this->docker_state as $docker_state) {
                if (!empty(Hash::extract($docker_state, 'Labels[com.docker.compose.service='.$name.']'))) {
                    $state = $docker_state;
                }
            }
        }

        return $state !== null ? $state['State'] : null;
    }

    public function getDockerStatus(string $name)
    {
        $status = null;

        if ($this->docker_state !== null) {
            foreach ($this->docker_state as $docker_state) {
                if (!empty(Hash::extract($docker_state, 'Labels[com.docker.compose.service=' . $name . ']'))) {
                    $status = $docker_state;
                }
            }
        }

        return $status !== null ? $status['Status'] : null;
    }

    public function getDockerCommand(string $name, $command)
    {
        return $this->curl()->doRequest(
            '/endpoints/'
            . $this->portainer_environment_id.'/docker/containers/'
            . $this->getDockerId($name)
            . '/'
            . $command,
            ["PostParameters" => true]
        );
    }

    public function getStatusAll()
    {
        $this->docker_state = json_decode($this->getAll(), true, 512, JSON_THROW_ON_ERROR);
    }
    public function getDockerId(string $name)
    {
        $docker = null;
        if ($this->docker_state !== null) {
            foreach ($this->docker_state as $docker_state) {
                if (!empty(Hash::extract($docker_state, 'Labels[com.docker.compose.service=' . $name . ']'))) {
                    $docker = $docker_state;
                }
            }
        }

        return $docker !== null ? $docker['Id'] : null;
    }


    public function getLastLog(string $name)
    {
        $curl_return = $this->curl()->doRequest(
            '/endpoints/'.$this->portainer_environment_id.'/docker/containers/' . $this->getDockerId($name) . '/logs',
            ["GetParameters" => [
                "stderr" => 1,
                "stdout" => 1,
                "tail" => 100,
                "since" => 0,
                "timestamps" => 0
            ]
            ],
            false
        );

        return nl2br(preg_replace('/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F-\x9F]+/', '', $curl_return));
    }
}
