<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('AppTools', 'Lib');
App::uses('HttpSocketResponse', 'Network/Http');
App::uses('HttpSocket', 'Network/Http');

require_once 'XML/RPC2/Client.php';

use function Cake\Core\env as env;

/**
 * Conversion Component
 *
 * @package     app.Controller.Component
 * @version 5.1.0
 * @since 4.3.0
 */
class ConversionComponent extends Component
{
    private string $host;

    public function __construct(ComponentCollection $collection, $settings = [])
    {
        {
            parent::__construct($collection, $settings);

            $this->host = Configure::read('FusionConv.cloudooo_host');
        }
    }

    public function getHttpSocket($config)
    {
        if (!is_null($this->httpSocket)) {
            return $this->httpSocket;
        }
        return $this->httpSocket = new HttpSocket($config);
    }

    /**
     * [__construct description]
     */
    public function setHttpSocket($httpSocket)
    {
        if ($httpSocket instanceof HttpSocket) {
            $this->httpSocket = $httpSocket;
        }
    }

    /**
     * @return HttpSocket
     */
    private function getClient()
    {
        return $this->getHttpSocket([
            'request' => [
                'uri' =>  Configure::read('App.convert.url'),
            ],
            'timeout' => AppTools::env('APP_CONVERSION_CONFIG_TIMEOUT', 3600),
            'ssl_verify_peer' => false,
            'ssl_allow_self_signed' => false
        ]);
    }

    /**
     * Conversion de format du fichie $fileUri vers le format $format
     * @param $pathFile
     * @param $dataExtention
     * @param $dataSortieExtention
     * @return array|bool|string tableau de réponse composé comme suit :
     *      'resultat' => boolean
     *      'info' => string
     *      'convertedFileUri' => nom et chemin du fichier converti
     *      @version 5.1.0
     *      @since 4.3.1
     */
    public function convertirFichier($pathFile, $dataExtention, $dataSortieExtention, $actualisation = true)
    {
        return $this->convertir(file_get_contents($pathFile), $dataExtention, $dataSortieExtention, $actualisation);
    }

    /**
     * Conversion de format du fichie $fileUri vers le format $format
     * @param $sData
     * @param $dataExtention
     * @param $dataSortieExtention
     * @return array|bool|string tableau de réponse composé comme suit :
     *      'resultat' => boolean
     *      'info' => string
     *      'convertedFileUri' => nom et chemin du fichier converti
     *      @version 5.1.0
     *      @since 4.3.1
     */
    public function convertirFlux($sData, $dataExtention, $dataSortieExtention, $actualisation = true)
    {
        return $this->convertir($sData, $dataExtention, $dataSortieExtention, $actualisation);
    }

    public function getClientRPC()
    {
        if (!is_null($this->client)) {
            return $this->client;
        }
        return $this->client = XML_RPC2_Client::create(
            $this->host,
            [
                'uglyStructHack' => true
            ]
        );
    }

    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @param $data
     * @param $dataExtention
     * @param $dataSortieExtention
     * @return array|bool|string
     */
    protected function convertir($data, $dataExtention, $dataSortieExtention, $actualisation)
    {
        try {
            $result = $this->getClientRPC()->convertFile(
                base64_encode($data),
                $dataExtention,
                $dataSortieExtention,
                false,
                $actualisation
            );
            return base64_decode($result, true);
        } catch (XML_RPC2_FaultException $e) {
            CakeLog::error('Exception #' . $e->getFaultCode() . ' : ' . $e->getFaultString());
            return false;
        }
    }

    /**
     * @param $model
     * @param $id
     * @param $field
     * @param $content
     * @return string
     */
    public function odt2txt($model, $id, $field, $content)
    {
        $output = [];
        $dir = TMP . "/$model" . '_' . "$id/";
        $odtFile = $dir . "$field" . '_' . "$id";
        if (!file_exists($dir)) {
            if (!mkdir($dir) && !is_dir($dir)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
            }
        }
        file_put_contents($odtFile . ".odt", $content);
        $commande = Configure::read('odt2txt_EXEC') . " $odtFile" . 'odt';
        exec($commande, $output, $return_value);
        return file_get_contents("$odtFile.txt");
    }

    /**
     * @param File $file_source
     * @param File $file
     * @return File
     */
    public function concatFilePDF($file_source, $file)
    {
        if (empty($file_source) || empty($file)) {
            return false;
        }
        if (!file_exists(Configure::read('PDFTK_EXEC'))) {
            return false;
        }
        $command = Configure::read('PDFTK_EXEC') . ' ' . $file_source->pwd() .' '. $file->pwd()
            .' output '. $file_source->pwd() .' 2>&1';
        shell_exec($command);
        CakeLog::debug($command);

        return $file_source;
    }

    /**
     * @param $folder
     * @param array $files
     * @param string $name File output
     * @return String
     */
    public function concatFilesPDF(&$folder, $files, $name = null)
    {
        if (empty($folder) || empty($files) || empty($name)) {
            return false;
        }
        if (!file_exists(Configure::read('PDFTK_EXEC'))) {
            return false;
        }
        $file_output = new File($folder->pwd() . DS . $name);
        $files_concat = '';
        foreach ($files as $file) {
            $file = new File($folder->pwd() . DS . $file);
            $files_concat .= $file->pwd() . ' ';
            $file->close(); // Assurez-vous de fermer le fichier quand c'est fini
        }
        //pdftk *.pdf cat output combined.pdf
        $command = Configure::read('PDFTK_EXEC'). ' '. $files_concat .' cat output '. $file_output->pwd() .' 2>&1';
        shell_exec($command);

        return $file_output->pwd();
    }

    /**
     * @param $sData
     * @param $stypeMime
     * @return mixed
     * @version 5.1.0
     * @since 4.3.0
     */
    public function toOdt($sData, $stypeMime)
    {
        $DOC_TYPE = Configure::read('DOC_TYPE');

        $folder = new Folder(AppTools::newTmpDir(TMP . 'files' . DS . 'conversion' . DS), true, 0777);
        //Si le fichier n'est pas un pdf on le converti en pdf
        if ($DOC_TYPE[$stypeMime]['extension'] !== 'pdf') {
            $sDataPdf = $this->convertirFlux($sData, $DOC_TYPE[$stypeMime]['extension'], 'pdf', false);
        }
        $file = new File($folder->pwd() . DS . '_origine.pdf', true, 0777);

        if (!empty($DOC_TYPE[$stypeMime]['extension'])
            && $DOC_TYPE[$stypeMime]['extension'] === 'pdf'
        ) {
            if (@is_file($sData)) {
                $file->append(file_get_contents($sData));
            } else {
                $file->append($sData);
            }
        } else {
            $file->append($sDataPdf);
        }

        if (env('APP_CONV_ODT_OLD')===true) {
            $return = $this->pdftoOdt($folder, $file);
        } else {
            // Définir la boundary pour le corps de la requête
            $boundary = '----WebKitFormBoundary' . md5(time());
            // Construire le corps de la requête
            $body = "--$boundary\r\n";
            // Ajout de la méthode
            $body .= 'Content-Disposition: form-data; name="method"' . "\r\n\r\n";
            $body .= Configure::read('App.convert.tool'). "\r\n";
            // Ajout de la résolution
            $body .= "--$boundary\r\n";
            $body .= 'Content-Disposition: form-data; name="resolution"' . "\r\n\r\n";
            $body .= Configure::read('App.convert.resolution') . "\r\n";
            // Ajout du fichier
            $body .= "--$boundary\r\n";
            $body .= 'Content-Disposition: form-data; name="file"; filename="'.$file->name.'.pdf' . "\"\r\n";
            $body .= "Content-Type: " . $file->mime() . "\r\n\r\n";
            $body .= $file->read() . "\r\n";
            $body .= "--$boundary--\r\n";

            $response = $this->getClient()->post('/api/v1/pdf2odt', $body, [
                'header' => [
                    'Content-Type' => 'multipart/form-data; boundary=' . $boundary,
                    'Content-Length' => strlen($body)
                ]
            ]);

            if (!$response->isOk()) {
                $message = sprintf('Erreur Lspdf2odt code: %d - %s', $response->code, $response->body);
                throw new InternalErrorException($message);
            }

            $return = $response;
        }

        $folder->delete();

        return $return;
    }

    /**
     * @param $folder
     * @param $fileOrigine
     * @return mixed
     * @throws InternalErrorException
     * @throws ODTException
     * @throws ImagickException
     * @version 5.1.0
     * @since 4.3.0
     */
    protected function pdftoOdt(&$folder, &$fileOrigine)
    {

        //Preparation de la commande ghostscript
        $command =  Configure::read('PDFINFO_EXEC') . ' '
            . $fileOrigine->pwd() . ' | grep -a Pages: | sed -e "s/ *Pages: *//g"';
        $nbrPage = trim(shell_exec($command));
        if ($nbrPage > 0) {
            for ($i = 1; $i <= $nbrPage; $i++) {
                $pageName = 'page_' . sprintf('%04d', $i);
                $command = Configure::read('PDFTK_EXEC') . ' ' . $fileOrigine->pwd() . ' cat ' . $i . ' output '
                . $folder->pwd() . DS . $pageName . '.pdf-orign 2>&1';
                shell_exec($command);
                $command = Configure::read('PDFTK_EXEC') . ' ' . $folder->pwd() . DS . $pageName
                . '.pdf-orign dump_data output ' . $folder->pwd() . DS . $pageName . '.txt 2>&1';
                shell_exec($command);
                $command = Configure::read('PDFTK_EXEC') . ' ' . $folder->pwd() . DS . $pageName
                . '.pdf-orign update_info ' . $folder->pwd() . DS . $pageName . '.txt output '
                . $folder->pwd() . DS . $pageName . '.pdf 2>&1';
                shell_exec($command);
            }
        } else {
            return '';
        }
        $fileOrigine->delete();

        $files = $folder->find('.*\.pdf', true);

        $i = 1;
        foreach ($files as $file) {
            $file = new File($folder->pwd() . DS . $file);

            $imagick = new Imagick();
            $imagick->setResolution(
                Configure::read('GS_RESOLUTION'),
                Configure::read('GS_RESOLUTION')
            );
            $imagick->readImage($file->pwd() . '[0]');
            $imagick->setImageFormat('png');
            $imagick->writeImage($folder->pwd() . DS . $i . '.png');

            if ($imagick->getImageHeight() > $imagick->getImageWidth()) {
                $orientation = 'portrait';
            } else {
                $orientation = 'landscape';
            }

            $pageParam[$i] = ['path' => $folder->pwd() . DS . $i . '.png',
                'name' => $i . '.png',
                'orientation' => $orientation];

            CakeLog::debug('page ' . $i . '| orientation=' . $orientation);
            $imagick->clear();
            $file->close();

            $i++;
        }

        //génération du fichier ODT
        if (empty($pageParam)) {
            throw new InternalErrorException('Impossible de convertir le fichier : paramètres manquants');
        }

        $this->generateOdtFileWithImages($folder, $pageParam);

        $file = new File($folder->pwd() . DS . 'result.odt');
        $return = $file->read();
        $file->close();

        return $return;
    }

    /**
     * @param $folder
     * @param $fileOrigine
     * @return mixed
     * @throws InternalErrorException
     * @version 5.1.0
     * @since 4.3.0
     */
    protected function pdftoOdtGs(&$folder, &$fileOrigine)
    {

        //Preparation de la commande ghostscript
        $PDFINFO_EXEC = Configure::read('PDFINFO_EXEC');
        $GS_RESOLUTION = Configure::read('GS_RESOLUTION');

        $command = $PDFINFO_EXEC . ' ' . $fileOrigine->pwd() . ' | grep Pages: | sed -e "s/ *Pages: *//g"';
        $nbrPage = trim(shell_exec($command));

        for ($i = 0; $i < $nbrPage; $i++) {
            $imagick = new Imagick();
            $imagick->setResolution($GS_RESOLUTION, $GS_RESOLUTION);
            $imagick->readImage($fileOrigine->pwd() . '[' . $i . ']');
            $imagick->setImageFormat('png');
            $imagick->writeImage($folder->pwd() . DS . $i . '.png');

            if ($imagick->getImageHeight() > $imagick->getImageWidth()) {
                $orientaion = 'portrait';
            } else {
                $orientaion = 'landscape';
            }
            $pageParam[$i] = ['path' => $folder->pwd() . DS . $i . '.png',
                'name' => $i . '.png',
                'orientation' => $orientaion];

            CakeLog::Debug('page ' . ($i + 1) . '| orientation=' . $orientaion);
            $imagick->clear();
        }

        //génération du fichier ODT
        if (empty($pageParam)) {
            throw new InternalErrorException('Impossible de convertir le fichier : paramètres manquants');
        }
        $this->generateOdtFileWithImages($folder, $pageParam);

        $file = new File($folder->pwd() . DS . 'result.odt');
        $return = $file->read();
        $file->close();

        return $return;
    }

    /**
     * @param $folder
     * @param $aPagePng
     * @throws ODTException
     */
    public function generateOdtFileWithImages(&$folder, $aPagePng)
    {
        App::import('Vendor', 'phpodt/phpodt');
        $odt = ODT::getInstance(true, $folder->pwd() . DS . 'result.odt');
        $pageStyleP = new PageStyle('myPageStylePortrait', 'Standard');
        $pageStyleP->setOrientation(StyleConstants::PORTRAIT);
        $pageStyleP->setHorizontalMargin('0cm', '0cm');
        $pageStyleP->setVerticalMargin('0cm', '0cm');
        $pStyleP = new ParagraphStyle('myPStyleP', 'Standard');
        $pStyleP->setBreakAfter(StyleConstants::PAGE);
        $pageStyleL = new PageStyle('myPageStyleLandscape', 'Landscape');
        $pageStyleL->setOrientation(StyleConstants::LANDSCAPE);
        $pageStyleL->setHorizontalMargin('0cm', '0cm');
        $pageStyleL->setVerticalMargin('0cm', '0cm');
        $pStyleL = new ParagraphStyle('myPStyleL', 'Landscape');

        $pStyleL->setBreakBefore(StyleConstants::PAGE);

        foreach ($aPagePng as $page) {
            if ($page['orientation'] === 'landscape') {
                $p = new Paragraph($pStyleL);
                $p->addImage(
                    $page['path'],
                    '29.7cm',
                    '21cm',
                    true,
                    $page['name'],
                    'paragraph'
                );
            } else {
                $p = new Paragraph($pStyleP);
                $p->addImage(
                    $page['path'],
                    '21cm',
                    '29.7cm',
                    true,
                    $page['name'],
                    'paragraph'
                );
            }
        }
        $odt->output();
    }
}
