<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('AppTools', 'Lib');
App::uses('HttpSocket', 'Network/Http');

/**
 * [SabreDavComponent description]
 * @package     app.Controller.Component
 * @version     v5.1
 * @since       v4.3.0
 */
class IdelibreComponent extends Component
{
    public $components = ['Session', 'TokensMethods'];
    private bool $use_idelibre;
    private string $host;

    /**
     * @param Controller $controller
     * @version 5.1
     */
    public function initialize(Controller $controller)
    {
        $this->use_idelibre = (bool)Configure::read('USE_IDELIBRE');
        $this->host = Configure::read('IDELIBRE_HOST');
    }

    public function isUsed()
    {
        return $this->use_idelibre;
    }

    public function getHttpSocket($config)
    {
        if (!is_null($this->httpSocket)) {
            return $this->httpSocket;
        }
        return $this->httpSocket = new HttpSocket($config);
    }

    /**
     * [__construct description]
     */
    public function setHttpSocket($httpSocket)
    {
        $this->httpSocket = $httpSocket;
    }

    /**
     * @return HttpSocket
     */
    private function getClient($auth = true)
    {
        $client = $this->getHttpSocket([
            'host' => Configure::read('App.fullBaseUrl'),
            'request' => [ 'uri' =>  $this->host,
            ],
            'timeout' => AppTools::env('APP_IDELIBRE_CONFIG_TIMEOUT', 600),
            'ssl_verify_peer' => false,
            'ssl_allow_self_signed' => true
        ]);
        if ($auth) {
            $client->configAuth('Idelibre', 'X-AUTH-TOKEN', Configure::read('IDELIBRE_X_AUTH_TOKEN'));
        }

        return $client;
    }

    /** Ping idelibre api v2
     * @return bool
     */
    public function ping(): bool
    {
        try {
            $client = $this->getClient(false);
            $response = $client->get('/api/v2/ping');

            $ping = json_decode($response->body(), true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException|SocketException $e) {
            return false;
        }

        return $response->isOk() && $ping['message'] === 'success';
    }

    /** Ping idelibre api v2
     * @return array|false
     */
    public function auth(): array
    {
        try {
            $client = $this->getClient();
            $response = $client->get('/api/v2/me');
            $json = json_decode($response->body(), true, 512, JSON_THROW_ON_ERROR);
            if (!$response->isOk()) {
                throw new CakeException($json['message'], $response->code);
            }
        } catch (JsonException|SocketException|CakeException $e) {
            throw new CakeException(__(
                'Erreur de récupération de l\'identifiant de structure depuis idelibre (code: %s, message: %s)',
                $e->getCode(),
                $e->getMessage()
            ));
        }

        if (!isset($json['structure']['id'])) {
            throw new CakeException(
                __(
                    'Erreur de structure de données d\'authentification',
                ),
                500
            );
        }

        return $json;
    }

    /** Ping idelibre api v2
     * @return array
     * @throws exception
     */
    private function sittingResults($settingId): array
    {
        try {
            $auth = $this->auth();
            $client = $this->getClient();
            $response = $client->get(
                "/api/v2/structures/{$auth['structure']['id']}/vote/sitting/{$settingId}"
            );
            if (!$response->isOk()) {
                if ($response->body() !== '') {
                    $json = json_decode($response->body(), true, 512, JSON_THROW_ON_ERROR|JSON_UNESCAPED_UNICODE);
                }

                throw new CakeException(
                    __(
                        'Erreur de récupération de la séance impossible depuis idelibre (code: %s%s)',
                        $response->code,
                        isset($json['message']) ? __(', message: %s', $json['message']):'',
                    )
                );
            }
            $json = json_decode($response->body(), true, 512, JSON_THROW_ON_ERROR|JSON_UNESCAPED_UNICODE);
        } catch (JsonException|SocketException $e) {
            throw new CakeException(__(
                'Récupération de la séance impossible (code: %s, message: %s)',
                $response->code,
                $e->getMessage()
            ));
        }
        return $json;
    }

    /**
     * @param array $results
     * @return true
     * @throws Exception
     */
    public function processingSaveImportResults($seanceId)
    {
        App::uses('Seance', 'Model');
        $seance = ClassRegistry::init('Seance');

        $warnings = [];
        $deliberations = $seance->getDeliberationsId($seanceId);
        if (empty($deliberations)) {
            throw new CakeException(__('Pas de délibérations disponible dans la séance'));
        }

        try {
            $seance->recursive = -1;
            $seanceData = $seance->findById($seanceId, ['idelibre_id']);

            $results = $this->sittingResults($seanceData['Seance']['idelibre_id']);

            foreach ($deliberations as $delibId) {
                try {
                    $result = $this->searchResultForProjectByIdAndSeanceId($results, $delibId, $seanceId);

                    $this->completeListePresence($results['actors'], $result, $delibId, $seanceId, $warnings);

                    $this->completeVotes($results['actors'], $result, $delibId, $seanceId, $warnings);
                } catch (CakeException $e) {
                    if (in_array($e->getCode(), [207, 404], true)) {
                        $warnings[] = [
                            'error' => $e->getCode(),
                            'delibId' => $delibId,
                            'errorMessage' => $e->getMessage()
                        ];
                        continue;
                    }
                }
            }
        } catch (CakeException $e) {
                $warnings[] = [
                    'error' => $e->getCode(),
                    'errorMessage' => $e->getMessage()
                ];
        }
        $seance->id = $seanceId;
        $seance->save(
            [
                'idelibre_votes_warning' => json_encode($warnings, JSON_THROW_ON_ERROR)
            ],
            false,
            ['idelibre_votes_warning']
        );

        return empty($warnings);
    }

    private function searchResultForProjectByIdAndSeanceId($results, $delibId, $seanceId)
    {
        App::uses('DeliberationSeance', 'Model');
        $deliberationSeance = ClassRegistry::init('DeliberationSeance');
        App::uses('Deliberation', 'Model');
        $deliberation = ClassRegistry::init('Deliberation');

        $deliberationSeance->recursive = -1;
        $deliberationSeance = $deliberationSeance->find('first', [
            'fields' => [
                'position',
            ],
            'conditions' => [
                'deliberation_id' => $delibId,
                'seance_id' => $seanceId
            ]
        ]);

        $deliberation->recursive = -1;
        $deliberation = $deliberation->find('first', [
            'fields' => [
                'Deliberation.objet',
                'Deliberation.objet_delib',
            ],
            'conditions' => ['Deliberation.id' => $delibId],
            'recursive' => -1
        ]);

        $project = Hash::extract(
            $results,
            "projects.{n}[rank={$deliberationSeance['DeliberationSeance']['position']}]"
        );
        if (count($project) > 1) {
            throw new CakeException(__('Projet en double trouvé'), 207);
        }

        if (empty($project) || $deliberation['Deliberation']['objet'] !== $project[0]['name']) {
            throw new CakeException(__('Projet non trouvé'), 404);
        }

        return $project[0];
    }

    private function searchActorByIdentifier($actors, $identifier)
    {
        App::uses('Acteur', 'Model');
        $acteur = ClassRegistry::init('Acteur');

        $actor = Hash::extract(
            $actors,
            "{n}[identifier={$identifier}]"
        );
        $acteur->recursive = -1;
        $actor = $acteur->findByNomAndPrenom($actor[0]['lastName'], $actor[0]['firstName']);

        if (empty($actor)) {
            throw new CakeException(__('Pas de correspondance pour \'acteur'), 404);
        }
        return $actor['Acteur']['id'];
    }

    private function completeListePresence($actors, $results, $delibId, $seanceId, &$warnings)
    {
        App::uses('Deliberation', 'Model');
        $deliberation = ClassRegistry::init('Deliberation');
        App::uses('Listepresence', 'Model');
        $listepresence = ClassRegistry::init('Listepresence');
        $listepresence->recursive = -1;
        $listPresents = $listepresence->find(
            'all',
            [
                'conditions' => ['Listepresence.delib_id' => $delibId],
                'order' => ["Acteur.position ASC"],
                'contain' => ['Acteur', 'Acteur.Typeacteur']
            ]
        );
        if (empty($listPresents)) {
            $listPresents = $deliberation->buildFirstList($delibId, $seanceId);
        }
        foreach ($listPresents as $present) {
            try {
                $actor = Hash::extract(
                    $actors,
                    "{n}[lastName={$present['Acteur']['nom']}]"
                );
                if (empty($actor)) {
                    throw new CakeException(__('Acteur non trouvé dans la liste des acteurs'), 404);
                }
                $attendance = Hash::extract(
                    $results,
                    "attendances.{n}[actor={$actor[0]['identifier']}]"
                );
                if (empty($attendance)) {
                    throw new CakeException(__('L\'Acteur n\'est pas dans la liste de présence'), 404);
                }
                $listepresence->id = $present['Listepresence']['id'];
                $data = [];
                switch ($attendance[0]['status']) {
                    case 'remote':
                    case 'present':
                        $data['Listepresence']['present'] = true;
                        break;
                    case 'absent_deputy':
                        $data['Listepresence']['present'] = true;
                        $data['Listepresence']['suppleant_id'] =
                            $this->searchActorByIdentifier($actors, $attendance[0]['deputy']);
                        break;
                    case 'absent_mandator':
                        $data['Listepresence']['present'] = false;
                        $data['Listepresence']['mandataire'] =
                            $this->searchActorByIdentifier($actors, $attendance[0]['mandator']);
                        break;
                    case 'absent':
                        $data['Listepresence']['present'] = false;
                        break;
                    default:
                        throw new CakeException(
                            __(
                                'Erreur sur la récupération du statut de vote de l\'acteur: %s %s',
                                $present['Acteur']['nom'],
                                $present['Acteur']['prenom']
                            ),
                            500
                        );
                }
                $listepresence->save($data);
            } catch (CakeException $e) {
                $warnings[] = [
                    'error' => $e->getCode(),
                    'delibId' => $delibId,
                    'errorMessage' => $e->getMessage()
                ];
                continue;
            }
        }
    }
    private function completeVotes($actors, $results, $delibId, $seanceId, &$warnings)
    {
        App::uses('Votes', 'Model');
        $vote = ClassRegistry::init('Votes');
        $vote->deleteAll(['delib_id' => $delibId], false);
        switch ($results['typeVote']) {
            case 'details':
                $this->completeVotesDetails($actors, $results, $delibId, $seanceId, $warnings);
                break;
            case 'priseActe':
                $this->completeVotesPrendreActe($results, $delibId, $warnings);
                break;
        }
    }
    private function completeVotesDetails($actors, $results, $delibId, $seanceId, &$warnings)
    {
        App::uses('Deliberation', 'Model');
        $deliberation = ClassRegistry::init('Deliberation');
        App::uses('Listepresence', 'Model');
        $listepresence = ClassRegistry::init('Listepresence');
        App::uses('Votes', 'Model');
        $vote = ClassRegistry::init('Votes');

        $listepresence->recursive = -1;
        $listPresents = $listepresence->find(
            'all',
            [
                'conditions' => ['Listepresence.delib_id' => $delibId],
                'order' => ["Acteur.position ASC"],
                'contain' => ['Acteur', 'Acteur.Typeacteur']
            ]
        );
        foreach ($listPresents as $present) {
            try {
                $actor = Hash::extract(
                    $actors,
                    "{n}[lastName={$present['Acteur']['nom']}]"
                );
                if (empty($actor)) {
                    throw new CakeException(__('Acteur non trouvé dans la liste des acteurs'), 404);
                }
                $result = Hash::extract(
                    $results,
                    "results.{n}[mandate={$actor[0]['identifier']}]"
                );
                if (empty($result)) {
                    throw new CakeException(__('L\'Acteur n\'a pas été dans la liste de présence'), 404);
                }
                $data = [];
                $data['acteur_id'] = $this->searchActorByIdentifier($actors, $result[0]['mandate']);
                $data['delib_id'] = $delibId;
                $vote->create();
                switch ($result[0]['choice']) {
                    case 'yes':
                        $data['resultat'] = 3;
                        break;
                    case 'no':
                        $data['resultat'] = 2;
                        break;
                    case 'abstention':
                        $data['resultat'] = 4;
                        break;
                    case 'absent':
                        continue 2;
                        break;
                    case 'nppv':
                        $data['resultat'] = 5;
                        break;
                    default:
                        throw new CakeException(
                            __(
                                'Erreur sur la récupération du résultat de vote de l\'acteur: %s %s',
                                $present['Acteur']['nom'],
                                $present['Acteur']['prenom']
                            ),
                            500
                        );
                }
                $vote->save($data);
            } catch (CakeException $e) {
                $warnings[] = [
                    'error' => $e->getCode(),
                    'delibId' => $delibId,
                    'errorMessage' => $e->getMessage()
                ];
                continue;
            }
        }
    }

    private function completeVotesPrendreActe($results, $delibId, &$warnings)
    {
        App::uses('Deliberation', 'Model');
        $deliberation = ClassRegistry::init('Deliberation');


        if (empty($results['results']['priseActe'])) {
            throw new CakeException(__('Récupération de la valeur de prendre acte impossible'), 500);
        }
        $deliberation->id = $delibId;
        $deliberation->save([
            'vote_prendre_acte' => $results['results']['priseActe']
        ]);
    }
}
