<?php

/**
 * Composant iparapheur
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       webdelib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 * @version     v4.3
 * @package     app.Controller.Component
 */
App::uses('AppTools', 'Lib');

class IparapheurComponent extends Component
{
    public $responseMessage;
    public $responseMessageStr;
    public $wsto;
    public $wsdl;
    public $userpwd;
    public $boundary;

    /**
     *
     */
    public function __construct()
    {
        $this->setWsto(
            Configure::read('IPARAPHEUR_HOST') . '/' . Configure::read('IPARAPHEUR_URI'),
            Configure::read('IPARAPHEUR_WSDL')
        );
        $this->setLogin(
            Configure::read('IPARAPHEUR_LOGIN'),
            Configure::read('IPARAPHEUR_PWD'),
        );
        $this->boundary = "5eca3d4a-35d8-1e01-32da-005056b32ce6";
    }

    /**
     *
     * @param string $wsto
     * @param string $wsdl
     */
    public function setWsto($wsto, $wsdl)
    {
        $this->wsdl = $wsdl;
        $this->wsto = $wsto;
    }

    /**
     *
     * @param type $login
     * @param type $passwd
     */
    public function setLogin($login, $passwd)
    {
        $this->userpwd = $login . ":" . $passwd;
    }

    /**
     *
     * @param type $requestPayloadString
     * @param type $params
     * @return string
     */
    public function SOAPMessage($requestPayloadString, $params)
    {
        if (!isset($params["attachments"])) {
            $soap = '<?xml version="1.0" encoding="utf-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" '
                .'xmlns:ns="http://www.adullact.org/spring-ws/iparapheur/1.0">
   <soapenv:Header/>
   <soapenv:Body>';
            $soap .= $requestPayloadString;
            $soap .= '</soapenv:Body></soapenv:Envelope>';
        } else {
            //
            // Le message contient des pièces jointes
            //
            $soap = "\n--MIMEBoundary" . $this->boundary . "
Content-Type: application/xop+xml;charset=UTF-8;type=\"text/xml\"
Content-Transfer-Encoding: 8bit
Content-ID: <i-Parapheur-query@adullact.org>

<?xml version='1.0' ?>
<SOAP-ENV:Envelope
xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">
<SOAP-ENV:Body>";
            // Ajout des pieces jointes
            $soap .= $requestPayloadString;
            $soap .= "</SOAP-ENV:Body></SOAP-ENV:Envelope>\n\n--MIMEBoundary" . $this->boundary . "\n";

            $attachments = $params["attachments"];
            $count = count($attachments);
            $idx = 0;
            foreach ($attachments as $key => $content) {
                $idx++;
                $soap .= "Content-Type: " . $content[1] .
                    "\nContent-Transfer-Encoding: " . $content[2] .
                    "\nContent-id: <" . $key . ">\n\n";
                $soap .= $content[0] . "\n--MIMEBoundary" . $this->boundary . ($idx === $count ? "--\n" : "\n");
            }
        }
        return $soap;
    }

    /**
     *
     * @param type $request
     * @param type $attachments
     * @return boolean
     */
    public function lancerRequeteCurl($request, $attachments = null)
    {
        $ch = curl_init($this->wsto);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_USERPWD, $this->userpwd);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL, $this->wsto);

        if ($attachments !== null) {
            curl_setopt(
                $ch,
                CURLOPT_HTTPHEADER,
                [
                    "Content-type:  Multipart/Related; boundary=MIMEBoundary"
                    . $this->boundary
                    . "; type=\"application/xop+xml\"; charset=utf-8; start=\"<i-Parapheur-query@adullact.org>\"",
                    'SOAPAction: ""'
                ]
            );
            $params = ["to" => $this->wsto, "attachments" => $attachments];
        } else {
            $params = ["to" => $this->wsto];
            curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-type:text/xml; charset=utf-8", 'SOAPAction: ""']);
        }
        $soap = $this->SOAPMessage($request, $params);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $soap);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $respons = curl_exec($ch);

        if ($respons === false) {
            CakeLog::error(curl_error($ch), 'parapheur');
            return false;
        }

        if (strpos($respons, '<?xml')!==0) {
            $lines = explode("\n", $respons);

            $ideb = 0;
            foreach ($lines as $line) {
                $ideb++;
                if ($line == "\r" || $line == "") {
                    break;
                }
            }
            $xmlLines = array_slice($lines, $ideb, count($lines) - $ideb - 1, true);
            $this->responseMessageStr = implode("\n", $xmlLines);
        } else {
            $this->responseMessageStr = $respons;
        }

        curl_close($ch);

        return true;
    }

    /**
     *
     * @return type
     */
    public function echoWebservice()
    {
        $request = '<ns:echoRequest xmlns:ns="http://www.adullact.org/spring-ws/iparapheur/1.0">'
            .'coucou marie claude repondit l echo</ns:echoRequest>';
        $this->lancerRequeteCurl($request, null, null);
        return $this->responseMessageStr;
    }

    /**
     *
     * @return type
     */
    public function getListeTypesWebservice()
    {
        $request = '<ns:GetListeTypesRequest xmlns:ns="http://www.adullact.org/spring-ws/iparapheur/1.0">'
            .'</ns:GetListeTypesRequest>';
        $this->lancerRequeteCurl($request);
        return array_merge($this->traiteXMLTypeTechnique(), $this->traiteXMLMessageRetour());
    }

    /**
     *
     * @param type $type
     * @return type
     */
    public function getListeSousTypesWebservice($type)
    {
        $request = '<ns:GetListeSousTypesRequest xmlns:ns="http://www.adullact.org/spring-ws/iparapheur/1.0">'
            . $type
            . '</ns:GetListeSousTypesRequest>';
        $this->lancerRequeteCurl($request);
        $soustypes = $this->traiteXMLSousType();
        if (!empty($soustypes)) {
            return array_merge($soustypes, $this->traiteXMLMessageRetour());
        } else {
            return $this->traiteXMLMessageRetour();
        }
    }

    /**
     *
     * @param type $typetech
     * @param type $soustype
     * @return type
     */
    public function getCircuit($typetech, $soustype)
    {
        $request = '<ns:GetCircuitRequest xmlns:ns="http://www.adullact.org/spring-ws/iparapheur/1.0">
								         <ns:TypeTechnique>' . $typetech . '</ns:TypeTechnique>
								         <ns:SousType>' . $soustype . '</ns:SousType>
								      </ns:GetCircuitRequest>';
        $this->lancerRequeteCurl($request);
        return $this->traiteXMLCircuit();
    }

    /**
     *
     * @param type $nom_dossier
     * @return type
     */
    public function getHistoDossierWebservice($nom_dossier)
    {
        $request = '<ns:GetHistoDossierRequest xmlns:ns="http://www.adullact.org/spring-ws/iparapheur/1.0">'
            . $nom_dossier
            . '</ns:GetHistoDossierRequest>';
        $this->lancerRequeteCurl($request);
        return array_merge($this->traiteXMLLogDossier(), $this->traiteXMLMessageRetour());
    }

    /**
     *
     * @param type $typetech
     * @param type $soustype
     * @param type $status
     * @param type $nbdossiers
     * @param type $dossierid
     * @return type
     */
    public function rechercherDossierWebservice(
        $typetech = '',
        $soustype = '',
        $status = '',
        $nbdossiers = '',
        $dossierid = ''
    ) {
        $request = '<ns:RechercherDossiersRequest xmlns:ns="http://www.adullact.org/spring-ws/iparapheur/1.0">
		                                   <ns:TypeTechnique>' . $typetech . '</ns:TypeTechnique>
				                   <ns:SousType>' . $soustype . '</ns:SousType>
				                   <ns:Status>' . $status . '</ns:Status>
				                   <ns:NombreDossiers>' . $nbdossiers . '</ns:NombreDossiers>
				                   <ns:DossierID>' . $dossierid . '</ns:DossierID>
				               </ns:RechercherDossiersRequest>';
        $this->lancerRequeteCurl($request);
        if ($this->traiteXMLLogDossier()) {
            return array_merge($this->traiteXMLLogDossier(), $this->traiteXMLMessageRetour());
        }
    }

    /**
     *
     * @param type $nom_dossier
     * @param type $typearchivage
     * @return type
     */
    public function archiverDossierWebservice($id_dossier, $typearchivage = "ARCHIVER")
    {
        $request = '<ns:ArchiverDossierRequest xmlns:ns="http://www.adullact.org/spring-ws/iparapheur/1.0">
                    <ns:DossierID>' . $id_dossier . '</ns:DossierID>
                    <ns:ArchivageAction>' . $typearchivage . '</ns:ArchivageAction>
                    </ns:ArchiverDossierRequest>';
        $this->lancerRequeteCurl($request);
        return array_merge($this->traiteXMLArchiverDossier(), $this->traiteXMLMessageRetour());
    }

    /**
     *
     * @param type $nom_dossier
     * @return type
     */
    public function effacerDossierRejeteWebservice($id_dossier)
    {
        $request = '<ns:EffacerDossierRejeteRequest xmlns:ns="http://www.adullact.org/spring-ws/iparapheur/1.0">'
                    . $id_dossier
                    . '</ns:EffacerDossierRejeteRequest>';
        $this->lancerRequeteCurl($request);
        return $this->traiteXMLMessageRetour();
    }

    /**
     *
     * @param type $nom_dossier
     * @return type
     */
    public function exercerDroitRemordWebservice($id_dossier)
    {
        $request = '<ns:ExercerDroitRemordDossierRequest xmlns:ns="http://www.adullact.org/spring-ws/iparapheur/1.0">'
                    . $id_dossier
                    . '</ns:ExercerDroitRemordDossierRequest>';
        $this->lancerRequeteCurl($request);
        return $this->traiteXMLMessageRetour();
    }

    /**
     *
     * @param type $titre
     * @param type $typetech
     * @param type $soustype
     * @param type $visibilite
     * @param type $pdf
     * @param type $docsannexes
     * @param type $datelim
     * @param type $annotpub
     * @param type $annotpriv
     * @param type $metas
     * @return type
     */
    public function creerDossierWebservice(
        $titre,
        $typetech,
        $soustype,
        $visibilite,
        $pdf,
        $docsannexes = [],
        $datelim = '',
        $annotpub = '',
        $annotpriv = '',
        $metas = []
    ) {
        $attachments = ['fichierPDF' => [$pdf, 'application/pdf', 'binary', 'document.pdf']];
        $request =
            '<ns:CreerDossierRequest xmlns:ns="http://www.adullact.org/spring-ws/iparapheur/1.0"'
            .' xmlns:xm="http://www.w3.org/2005/05/xmlmime">
								         <ns:TypeTechnique>' . $typetech . '</ns:TypeTechnique>
								         <ns:SousType>' . $soustype . '</ns:SousType>
								         <ns:DossierID></ns:DossierID>
								         <ns:DossierTitre>'
            . AppTools::xmlEntityEncode(AppTools::reformatNameForIparapheur($titre))
            . '</ns:DossierTitre>
								         <ns:DocumentPrincipal xm:contentType="application/pdf">
								         	<xop:Include xmlns:xop="http://www.w3.org/2004/08/xop/include"'
            .' href="cid:fichierPDF"></xop:Include>
								         </ns:DocumentPrincipal>';
        if (isset($metas) && !empty($metas)) {
            $request .= '<ns:MetaData>';
            foreach ($metas as $nom => $valeur) {
                $request .= "<ns:MetaDonnee><ns:nom>$nom</ns:nom><ns:valeur>$valeur</ns:valeur></ns:MetaDonnee>";
            }
            $request .= '</ns:MetaData>';
        }

        $request .= '<ns:DocumentsAnnexes>';
        for ($i = 0, $iMax = count($docsannexes); $i < $iMax; $i++) {
            $encoding = !empty($docsannexes[$i]['encoding']) ? $docsannexes[$i]['encoding'] : 'UTF-8';
            $request .= '<ns:DocAnnexe>
		               <ns:nom>' . AppTools::xmlEntityEncode($docsannexes[$i]['filename']) . '</ns:nom>
		               <ns:fichier xm:contentType="' . $docsannexes[$i]['mimetype'] . '">
		               <xop:Include xmlns:xop="http://www.w3.org/2004/08/xop/include" href="cid:annexe_'
                . $i
                . '"></xop:Include>
		               </ns:fichier>
		               <ns:mimetype>' . $docsannexes[$i]['mimetype'] . '</ns:mimetype>
		               <ns:encoding>' . $encoding . '</ns:encoding>
		            </ns:DocAnnexe>';
            $attachments = array_merge(
                $attachments,
                [
                    'annexe_' . $i => [$docsannexes[$i]['content'],
                    $docsannexes[$i]['mimetype'], 'binary', $docsannexes[$i]['filename']]
                ]
            );
        }
        $request .= '</ns:DocumentsAnnexes>';
        $request .= '<ns:XPathPourSignature></ns:XPathPourSignature>
                    <ns:AnnotationPublique>' . $annotpub . '</ns:AnnotationPublique>
                    <ns:AnnotationPrivee>' . $annotpriv . '</ns:AnnotationPrivee>
                    <ns:Visibilite>' . $visibilite . '</ns:Visibilite>
                    <ns:DateLimite>' . $datelim . '</ns:DateLimite>
                    </ns:CreerDossierRequest>';
        $this->lancerRequeteCurl($request, $attachments);


        return $this->traiteXMLMessageRetour();
    }

    /**
     *
     * @param type $nom_dossier
     * @return type
     */
    public function getDossierWebservice($nom_dossier)
    {
        $request = '<ns:GetDossierRequest xmlns:ns="http://www.adullact.org/spring-ws/iparapheur/1.0">'
            . $nom_dossier . '</ns:GetDossierRequest>';
        $this->lancerRequeteCurl($request);
        return array_merge($this->traiteXMLGetDossier(), $this->traiteXMLMessageRetour());
    }

    /**
     *
     * @param type $nom_dossier
     * @return type
     */
    public function envoyerDossierTdTWebservice($nom_dossier)
    {
        $request = '<ns:EnvoyerDossierTdTRequest xmlns:ns="http://www.adullact.org/spring-ws/iparapheur/1.0">'
            . $nom_dossier . '</ns:EnvoyerDossierTdTRequest>';
        $this->lancerRequeteCurl($request);
        return $this->traiteXMLMessageRetour();
    }

    /**
     *
     * @param type $nom_dossier
     * @return type
     */
    public function getStatutTdTWebservice($nom_dossier)
    {
        $request = '<ns:GetStatutTdTRequest xmlns:ns="http://www.adullact.org/spring-ws/iparapheur/1.0">'
            . $nom_dossier . '</ns:GetStatutTdTRequest>';
        $this->lancerRequeteCurl($request);
        return array_merge($this->traiteXMLLogDossier(), $this->traiteXMLMessageRetour());
    }

    /**
     *
     * @param type $nom_dossier
     * @param type $codetransit
     * @param type $annotpub
     * @param type $annotpriv
     * @return type
     */
    public function forcerEtapeWebservice($nom_dossier, $codetransit, $annotpub = '', $annotpriv = '')
    {
        $request = '<ns:ForcerEtapeRequest xmlns:ns="http://www.adullact.org/spring-ws/iparapheur/1.0">
								         <ns:DossierID>' . $nom_dossier . '</ns:DossierID>
								         <ns:CodeTransition>' . $codetransit . '</ns:CodeTransition>
								         <ns:AnnotationPublique>' . $annotpub . '</ns:AnnotationPublique>
								         <ns:AnnotationPrivee>' . $annotpriv . '</ns:AnnotationPrivee>
								      </ns:ForcerEtapeRequest>';
        $this->lancerRequeteCurl($request);
        return array_merge($this->traiteXMLLogDossier(), $this->traiteXMLMessageRetour());
    }

    /**
     *
     * @return string
     * @throws Exception
     */
    public function traiteXMLMessageRetour()
    {
        $xml = simplexml_load_string($this->responseMessageStr);
        if ($xml !== false) {
            $result = $xml->xpath('S:Body/S:Fault');
            if (!empty($result)) {
                $response['messageretour'] = [
                    'coderetour' => -1,
                    'message' => 'Erreur soap : Veuillez contacter votre administrateur',
                    'severite' => 'grave'
                ];
                $this->log($result[0]->faultstring, 'parapheur');
                return $response;
            }
        }

        $dom = new DomDocument();
        try {
            if (empty($this->responseMessageStr)) {
                throw new Exception("Aucune réponse du parapheur");
            }
            $dom->loadXML($this->responseMessageStr);
            $codesretour = $dom->documentElement->getElementsByTagName('codeRetour');
            $coderetour = @$codesretour->item(0)->nodeValue;
            $messages = $dom->documentElement->getElementsByTagName('message');
            $message = @$messages->item(0)->nodeValue;
            $severites = $dom->documentElement->getElementsByTagName('severite');
            $severite = @$severites->item(0)->nodeValue;
            $dossierIDs = $dom->documentElement->getElementsByTagName('DossierID');
            $dossierID = @$dossierIDs->item(0)->nodeValue;
            $response['messageretour'] = ["coderetour" => $coderetour, "message" => $message, "severite" => $severite];
            $response['dossierID'] = $dossierID;
        } catch (Exception $e) {
            $response['messageretour'] = [
                "coderetour" => -1,
                "message" => "Erreur de connexion au parapheur: " . $e->getMessage(),
                "severite" => "grave"
            ];
        }
        return $response;
    }

    /**
     *
     * @return type
     */
    public function traiteXMLLogDossier()
    {
        $response = [];
        $dom = new DomDocument();
        $dom->loadXML($this->responseMessageStr);
        $dataset = $dom->getElementsByTagName("LogDossier");
        foreach ($dataset as $row) {
            $timestamps = $row->getElementsByTagName("timestamp");
            $timestamp = $timestamps->item(0)->nodeValue;

            $noms = $row->getElementsByTagName("nom");
            $nom = $noms->item(0)->nodeValue;

            $status = $row->getElementsByTagName("status");
            $statu = $status->item(0)->nodeValue;

            $annotations = $row->getElementsByTagName("annotation");
            $annotation = $annotations->item(0)->nodeValue;

            $accessibles = $row->getElementsByTagName("accessible");
            $accessible = $accessibles->length > 0 ? $accessibles->item(0)->nodeValue : "";

            $response['logdossier'][] = [
                "timestamp" => $timestamp,
                "nom" => $nom,
                "status" => $statu,
                "annotation" => $annotation,
                "accessible" => $accessible
            ];
        }
        return $response;
    }

    /**
     *
     * @return type
     */
    public function traiteXMLTypeTechnique()
    {
        $dom = new DomDocument();
        $dom->loadXML($this->responseMessageStr);
        $dataset = $dom->getElementsByTagName("TypeTechnique");
        $response = [];
        foreach ($dataset as $row) {
            $response['typetechnique'][] = $row->nodeValue;
        }
        return $response;
    }

    /**
     *
     * @return type
     */
    public function traiteXMLSousType()
    {
        $dom = new DomDocument();
        $response = [];
        if ($this->responseMessageStr != null) {
            $dom->loadXML($this->responseMessageStr);
            $dataset = $dom->getElementsByTagName("SousType");
            foreach ($dataset as $row) {
                $response['soustype'][] = $row->nodeValue;
            }
        }
        return $response;
    }

    /**
     *
     * @return type
     */
    public function traiteXMLCircuit()
    {
        $dom = new DomDocument();
        $dom->loadXML($this->responseMessageStr);
        $dataset = $dom->getElementsByTagName("EtapeCircuit");
        $response = [];
        foreach ($dataset as $row) {
            $parapheurs = $row->getElementsByTagName("Parapheur");
            $parapheur = $parapheurs->item(0)->nodeValue;

            $prenoms = $row->getElementsByTagName("Prenom");
            $prenom = $prenoms->item(0)->nodeValue;

            $noms = $row->getElementsByTagName("Nom");
            $nom = $noms->item(0)->nodeValue;

            $roles = $row->getElementsByTagName("Role");
            $role = $roles->item(0)->nodeValue;

            $response['etapecircuit'][] = [
                "Parapheur" => $parapheur,
                "Prenom" => $prenom,
                "Nom" => $nom,
                "Role" => $role
            ];
        }
        return $response;
    }

    /**
     *
     * @return type
     */
    public function traiteXMLArchiverDossier()
    {
        $response = [];
        $dom = new DomDocument();
        $dom->loadXML($this->responseMessageStr);
        $url = $dom->documentElement->getElementsByTagName('URL');
        if ($url->length > 0) {
            $response['URL'] = $url->item(0)->nodeValue;
        }
        return $response;
    }

    /**
     *
     * @return array
     */
    public function traiteXMLGetDossier()
    {
        $dom = new DomDocument();
        //$this->log($this->responseMessageStr,'debug');
        $dom->loadXML($this->responseMessageStr);
        $signdocprinc = '';
        $datelim = '';

        $typestech = $dom->documentElement->getElementsByTagName('TypeTechnique');
        $typetech = $typestech->item(0)->nodeValue;
        $soustypes = $dom->documentElement->getElementsByTagName('SousType');
        $soustype = $soustypes->item(0)->nodeValue;
        $dossiersid = $dom->documentElement->getElementsByTagName('DossierID');
        $dossierid = $dossiersid->item(0)->nodeValue;
        $annotspub = $dom->documentElement->getElementsByTagName('AnnotationPublique');
        $annotpub = $annotspub->item(0)->nodeValue;
        $annotspriv = $dom->documentElement->getElementsByTagName('AnnotationPrivee');
        $annotpriv = $annotspriv->item(0)->nodeValue;
        $visus = $dom->documentElement->getElementsByTagName('Visibilite');
        $visu = $visus->item(0)->nodeValue;
        $dateslim = $dom->documentElement->getElementsByTagName('DateLimite');
        if ($dateslim->length > 0) {
            $datelim = $dateslim->item(0)->nodeValue;
        }
        $docsprinc = $dom->documentElement->getElementsByTagName('DocPrincipal');
        $docprinc = $docsprinc->item(0)->nodeValue;
        $nomsdocprinc = $dom->documentElement->getElementsByTagName('NomDocPrincipal');
        $nomdocprinc = $nomsdocprinc->item(0)->nodeValue;

        $signsdocprinc = $dom->documentElement->getElementsByTagName('SignatureDocPrincipal');
        if ($signsdocprinc->length > 0) {
            $signdocprinc = $signsdocprinc->item(0)->nodeValue;
        }

        $annexesNode = $dom->documentElement->getElementsByTagName('DocAnnexe');
        if ($annexesNode->length > 0) {
            $bordereau = $annexesNode->item(($annexesNode->length)-1)
                ->getElementsByTagName("fichier")
                ->item(0)
                ->nodeValue;
        }

        $nodeListMetaDonnees = $dom->documentElement->getElementsByTagName('MetaDonnees');
        if (!empty($nodeListMetaDonnees)) {
            $nodeMetaDonnee = $nodeListMetaDonnees->item(0);
            $nodeListMetaDonnee = $nodeMetaDonnee->getElementsByTagName('MetaDonnee');
            for ($key = 0; $key<$nodeListMetaDonnee->length; $key++) {
                if ($nodeListMetaDonnee->item($key)
                        ->getElementsByTagName('nom')
                        ->item(0)->nodeValue === 'ph:signatureFormat') {
                    $signatureFormat =
                        $nodeListMetaDonnee->item($key)->getElementsByTagName('valeur')->item(0)->nodeValue;
                }
            }
        }

        $response['getdossier'] = [
            'type' => $typetech,
            'soustype' => $soustype,
            'dossierid' => $dossierid,
            'annotpub' => $annotpub,
            'annotpriv' => $annotpriv,
            'visu' => $visu,
            'datelim' => $datelim,
            'docprinc' => $docprinc,
            'nomdocprinc' => $nomdocprinc,
            'signatureformat' => !empty($signatureFormat) ? $signatureFormat : null,
            'signature' => $signdocprinc, 'bordereau' => !empty($bordereau) ? $bordereau : ''
        ];

        return $response;
    }
}
