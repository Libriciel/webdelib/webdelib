<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('AppTools', 'Lib');
App::uses('Deliberation', 'Model');
App::uses('Typeseance', 'Model');
App::uses('Seance', 'Model');
App::uses('Nature', 'Model');
App::uses('SaeVersement', 'Model');
App::uses('Sae', 'Lib');

/**
 * Component ExportGed
 *
 * @package app.Controller.Component
 * @version 5.1.0
 */
class VersementSaeComponent extends Component
{
    /** @var Object Contains a folder temp for send */
    public $folderSend;

    /** @var Array Contains a components */
    public $components = [
        'AuthComponent',
        'Conversion',
        'Progress',
        'ProjetTools',
        'PDFStamp'
    ];

    /** @var Array Contains a pluralize terms */
    private $pluralizes = [
        'TexteActe' => 'TextesActes',
        'Annexe' => 'Annexes',
        'Rapport' => 'Rapports',
        'Signature' => 'Signatures',
        'BordereauProjet' => 'BordereauxProjet',
        'BordereauTdt' => 'BordereauxTdt',
        'BordereauParapheur' => 'BordereauxParapheur',
        'ArActe' => 'ArActes',
        'AnnexeTampon' => 'AnnexesTampons',
        'ActeTampon' => 'ActesTampons',
        'CourrierSimple' => 'CourriersSimple',
        'LettreObservation' => 'LettresObservation',
        'PieceComplementaire' => 'PiecesComplementaire',
        'DefereTribunalAdministratif' => 'DeferesTribunalAdministratif',
        'Annulation' => 'Annulations',
    ];
    private $zip;

    public const PREPARATION =  '1_PREPARATION';
    public const DELIBERATIONS =  '2_DELIBERATIONS';
    public const POST_SEANCE =  '3_POST_SEANCE';
    public const COMPLEMENTS =  '4_COMPLEMENTS';
    public const DELIBERATIONS_RAPPORTS =  '1_RAPPORTS';
    public const DELIBERATIONS_DELIBERATION =  '2_DELIBERATIONS';
    public const DELIBERATIONS_CONTROLE_LEGALITE =  '3_CONTROLE_LEGALITE';
    public const DELIBERATIONS_PUBLICATION =  '4_PUBLICATION';
    public const DELIBERATIONS_COMPLEMENT =  '5_COMPLEMENT';

    public const PDF_TYPE_MIME='application/pdf';


    public function initialize(Controller $controller)
    {
        parent::initialize($controller);

        $this->Deliberation = ClassRegistry::init('Deliberation');
        $this->Typeseance = ClassRegistry::init('Typeseance');
        $this->Seance = ClassRegistry::init('Seance');
        $this->Nature = ClassRegistry::init('Nature');
        $this->Typologiepiece = ClassRegistry::init('Typologiepiece');
        $this->Theme = ClassRegistry::init('Theme');
        $this->Historique = ClassRegistry::init('Historique');
    }

    public function startup(Controller $controller)
    {
        $this->Sae = $this->getSae();

        parent::startup($controller);
    }

    public function getFolderSend()
    {
        return $this->folderSend;
    }
    public function getSae()
    {
        if (!is_null($this->Sae)) {
            return $this->Sae;
        }
        return $this->Sae = new Sae();
    }

    public function setSae($sae)
    {
        $this->Sae = $sae;
    }

    /**
     * @param Folder $folderSend
     * @version 5.1.0
     */
    public function verserService(&$folderSend)
    {
        $this->folderSend = &$folderSend;
    }

    /**
     * @param Folder $folderSend
     * @version 5.1.0
     */
    public function setZip(&$zip)
    {
        $this->zip = $zip;
    }

    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Create element for Ged
     * @param DOMDocument $dom
     * @param string $tag_name
     * @param string $value
     * @return DOMElement $element
     * @throws Exception
     * @version 5.1.0
     */
    public function createElement($dom, $tag_name, $value = null, $attributes = null)
    {
        try {
            $element = ($value !== null) ? $dom->createElement(
                $tag_name,
                AppTools::xmlEntityEncode($value)
            ) : $dom->createElement($tag_name);

            if ($attributes !== null) {
                foreach ($attributes as $attr => $val) {
                    $element->setAttribute($attr, $val);
                }
            }
        } catch (Exception $e) {
            CakeLog::error(
                __(
                    '[Sae][%s]Create error(%s) tag name: %s',
                    __METHOD__,
                    $e->getCode(),
                    $tag_name
                ) . "\n" . $e->getMessage(),
                'connector'
            );
            throw $e;
        }
        return $element;
    }

    /**
     * @param int $id
     * @param string $model
     */
    public function createElementInfosupsByZip($id, $model)
    {
        App::uses('Infosup', 'Model', true);
        CakeLog::info(__('[Sae][%s]Add: Infosups', __METHOD__), 'connector');
        $this->Infosup = ClassRegistry::init('Infosup');

        $infosups = $this->Infosup->export($id, $model, true);
        if (!empty($infosups)) {
            foreach ($infosups as $code => $infosup) {
                if (!empty($infosup['content'])) {
                    CakeLog::info(
                        __('[Sae][%s]Add: Infosup %s', __METHOD__, $code),
                        'connector'
                    );
                    if ($model==='Deliberation') {
                        $this->addFileToZip(
                            self::COMPLEMENTS,
                            __('%s_%s', $code, AppTools::getNameFile($infosup['file_name'])),
                            'pdf',
                            $infosup['content']
                        );
                    } else {
                        $zipName = __('%s_%s.pdf', $code, $infosup['file_name']);
                        CakeLog::info(
                            __('[Sae][%s]Acte: add file %s', __METHOD__, $zipName),
                            'connector'
                        );
                        $this->zip->addFromString(
                            self::COMPLEMENTS . DS . $zipName,
                            $infosup['content']
                        );
                    }
                }
            }
        }
    }

    /**
     * Récupération des informations de l'acte
     * @param int $id
     * @return array
     */
    private function getActe($id)
    {
        $acte = $this->Deliberation->find(
            'first',
            [
                'conditions' => ['Deliberation.id' => $id],
                'fields' => [
                    'Deliberation.id', 'Deliberation.num_delib', 'Deliberation.objet_delib', 'Deliberation.titre',
                    'Deliberation.num_pref', 'typologiepiece_code', 'tdt_document_papier',
                    'Deliberation.deliberation', 'Deliberation.deliberation_size',
                    'Deliberation.delib_pdf', 'Deliberation.signature', 'signature_type',
                    'Deliberation.parapheur_etat', 'Deliberation.signee', 'Deliberation.parapheur_bordereau',
                    'Deliberation.tdt_data_pdf', 'Deliberation.tdt_data_bordereau_pdf',
                    'Deliberation.tdt_ar_date', 'Deliberation.tdt_id', 'Deliberation.tdt_ar',
                    'Deliberation.tdt_status',
                    'Deliberation.date_acte',
                    'Deliberation.publier_date',
                    'Deliberation.publier_etat',
                    'Deliberation.signature_date'
                ],
                'contain' => [
                    'Service' => ['fields' => ['name']],
                    'Theme' => ['fields' => ['libelle']],
                    'Typeacte' => [
                        'fields' => ['name', 'modele_projet_id', 'modele_bordereau_projet_id', 'nature_id'],
                        'Nature' => ['fields' => ['name']]
                    ],
                    'Redacteur' => ['fields' => ['nom', 'prenom']],
                    'President' => ['fields' => ['nom', 'prenom']],
                    'Rapporteur' => ['fields' => ['nom', 'prenom']],
                ]
            ]
        );

        $nature = $this->Nature->find('first', [
            'fields' => ['name'],
            'conditions' => [
                'Nature.id' => $acte['Typeacte']['nature_id']]
        ]);

        return $acte + $nature;
    }

    /**
     * @param $dom
     * @param $dom_depot
     * @param $acte_id
     * @param $seance
     * @return void
     * @throws Exception
     */
    public function getDeliberation(&$dom, &$dom_depot, $acte_id, &$seance = null)
    {
        CakeLog::info(
            __('[Sae][%s]Acte(%s): create elements for DOMDocument', __METHOD__, $acte_id),
            'connector'
        );

        $attributes = ['idActe' => $acte_id];
        if (!empty($seance['Seance']['id'])) {
            $attributes = array_merge($attributes, ['refSeance' => $seance['Seance']['id']]);
        }

        $doc = $this->createElement($dom, 'delib', null, $attributes);

        CakeLog::info(
            __('[Sae][%s]Acte(%s): search informations', __METHOD__, $acte_id),
            'connector'
        );
        $acte = $this->getActe($acte_id);
        $name = $acte['Deliberation']['num_delib'];

        $doc->appendChild($this->createElement($dom, 'libelleProjet', $acte['Deliberation']['objet_delib']));
        $doc->appendChild($this->createElement($dom, 'numDelib', $acte['Deliberation']['num_delib']));
        $this->createElementThemesForActe($dom, $doc, $acte['Theme']['id']);
        $doc->appendChild($this->createElement(
            $dom,
            'serviceEmetteur',
            $acte['Service']['name']
        ));
        $doc->appendChild($this->createElement(
            $dom,
            'rapporteur',
            !empty($acte['Rapporteur']) ? __('%s %s', $acte['Rapporteur']['prenom'], $acte['Rapporteur']['nom']) : null
        ));
        $doc->appendChild($this->createElement(
            $dom,
            'presidentVote',
            !empty($acte['President']) ? __('%s %s', $acte['President']['prenom'], $acte['President']['nom']) : null
        ));

        $doc->appendChild($this->createElement(
            $dom,
            'numeroODJ',
            $this->Deliberation->getPosition($acte_id, $seance['Seance']['id'])
        ));
        $doc->appendChild($this->createElement(
            $dom,
            'redacteur',
            !empty($acte['Redacteur']) ? __('%s %s', $acte['Redacteur']['prenom'], $acte['Redacteur']['nom']) : null
        ));
        $doc->appendChild($this->createElement(
            $dom,
            'dateSignature',
            $acte['Deliberation']['signature_date']
        ));
        $doc->appendChild($this->createElement(
            $dom,
            'datePublication',
            $acte['Deliberation']['publier_date']
        ));
        $doc->appendChild($this->createElement(
            $dom,
            'dateAcquit',
            !empty($acte['Deliberation']['tdt_status']) ? $acte['Deliberation']['tdt_ar_date'] : null
        ));

        $doc->appendChild($this->createElement(
            $dom,
            'signatureManuel',
            empty($acte['Deliberation']['parapheur_etat']) ? 'oui': 'non'
        ));
        $doc->appendChild($this->createElement(
            $dom,
            'envoiTdTManuel',
            empty($acte['Deliberation']['tdt_status']) ? 'oui': 'non'
        ));
        $doc->appendChild($this->createElement(
            $dom,
            'publicationManuel',
            $acte['Deliberation']['publier_etat']===2 ? 'oui': 'non'
        ));

        //Récupération du rapport de projet
        CakeLog::info(
            __('[Sae][%s]Acte(%s): add to zip file "Rapport"', __METHOD__, $acte_id),
            'connector'
        );
        $this->addFileToZip(
            $name . DS . self::DELIBERATIONS_RAPPORTS,
            __('%s_rapport', $acte['Deliberation']['num_delib']),
            'pdf',
            $this->Deliberation->fusion(
                $acte['Deliberation']['id'],
                'rapport',
                !empty($seance['Seance']['id']) ? $seance['Typeseance']['modele_projet_id']
                    : $acte['Typeacte']['modele_projet_id']
            )
        );

        if (!empty($acte['Deliberation']['delib_pdf'])) {
            CakeLog::info(
                __('[Sae][%s]Acte(%s): add to zip file "DeliberationSignee"', __METHOD__, $acte_id),
                'connector'
            );
            $this->addFileToZip(
                $name . DS . self::DELIBERATIONS_DELIBERATION,
                __('%s_delib', $acte['Deliberation']['num_delib']),
                'pdf',
                $acte['Deliberation']['delib_pdf']
            );
        }

        if (!empty($acte['Deliberation']['parapheur_bordereau'])) {
            CakeLog::info(
                __('[Sae][%s]Acte(%s): add to zip file "BordereauParapheur"', __METHOD__, $acte_id),
                'connector'
            );
            $this->addFileToZip(
                $name . DS . self::DELIBERATIONS_DELIBERATION,
                __('%s_suivi_parapheur', $acte['Deliberation']['num_delib']),
                'pdf',
                $acte['Deliberation']['parapheur_bordereau']
            );
        }

        //Récupération de l'acte tamponné
        if (!empty($acte['Deliberation']['tdt_data_pdf'])) {
            //Noeud document[Bordereau]
            CakeLog::info(
                __('[Sae][%s]Acte(%s): add to zip file "ActeTdtTamponnee"', __METHOD__, $acte_id),
                'connector'
            );
            $this->addFileToZip(
                $name . DS . self::DELIBERATIONS_CONTROLE_LEGALITE,
                __('%s_deliberation_tamponnee', $name),
                'pdf',
                $acte['Deliberation']['tdt_data_pdf']
            );
        }
        //Récupération du bordereau de transmission
        if (!empty($acte['Deliberation']['tdt_data_bordereau_pdf'])) {
            //Noeud document[Bordereau]
            CakeLog::info(
                __('[Sae][%s]Acte(%s): add to zip file "BordereauTdt"', __METHOD__, $acte_id),
                'connector'
            );
            $this->addFileToZip(
                $name . DS . self::DELIBERATIONS_CONTROLE_LEGALITE,
                __('%s_bordereau_acquittement', $name),
                'pdf',
                $acte['Deliberation']['tdt_data_bordereau_pdf']
            );
        }
        if (!empty($acte['Deliberation']['delib_pdf'])) {
            CakeLog::info(
                __('[Sae][%s]Acte(%s): add to zip file "DeliberationPublication"', __METHOD__, $acte_id),
                'connector'
            );
            $this->addFileToZip(
                $name . DS . self::DELIBERATIONS_PUBLICATION,
                __('%s_deliberation_publiee', $acte['Deliberation']['num_delib']),
                'pdf',
                $this->PDFStamp->stamp($acte['Deliberation']['delib_pdf'], $acte['Deliberation'])
            );
        }

        //Récupération des ARacte de transmission
        if (!empty($acte['Deliberation']['tdt_ar']) && !empty($acte['Deliberation']['tdt_status'])) {
            CakeLog::info(
                __('[Sae][%s]Acte(%s): add to zip files "ARacte"', __METHOD__, $acte_id),
                'connector'
            );
            $this->addFileToZip(
                $name . DS . self::DELIBERATIONS_CONTROLE_LEGALITE,
                __('%s-ar-actes', $acte['Deliberation']['num_delib']),
                'xml',
                $acte['Deliberation']['tdt_ar']
            );
        }

        //Ajout de toutes les annexes du projets
        $annexes = $this->Deliberation->Annexe->getAnnexesFromDelibId($acte_id);
        CakeLog::info(
            __('[Sae][%s]Acte(%s): add to zip files "Annexes"', __METHOD__, $acte_id),
            'connector'
        );

        if (!empty($annexes)) {
            foreach ($annexes as $annexe) {
                //Récupération des informations des annexes de rapport
                if ($annexe['Annexe']['joindre_ctrl_legalite'] || $annexe['Annexe']['joindre_fusion']) {
                    $this->addFileToZip(
                        $name . DS . self::DELIBERATIONS_RAPPORTS,
                        __(
                            '%s_annexe_%s_%s',
                            $acte['Deliberation']['num_delib'],
                            $annexe['Annexe']['position'],
                            AppTools::getNameFile($annexe['Annexe']['filename'])
                        ),
                        AppTools::getExtensionFile($annexe['Annexe']['filename']),
                        $annexe['Annexe']['data']
                    );
                }

                if ($annexe['Annexe']['joindre_ctrl_legalite']) {
                    if (!empty($annexe['Annexe']['tdt_data_pdf'])) {
                        $this->addFileToZip(
                            $name . DS . self::DELIBERATIONS_CONTROLE_LEGALITE,
                            __(
                                '%s_annexe_tamponnee_%s_%s',
                                $acte['Deliberation']['num_delib'],
                                $annexe['Annexe']['position'],
                                AppTools::getNameFile($annexe['Annexe']['filename'])
                            ),
                            'pdf',
                            $annexe['Annexe']['tdt_data_pdf']
                        );
                    }
                    $this->addFileToZip(
                        $name . DS . self::DELIBERATIONS_PUBLICATION,
                        __(
                            '%s_annexe_publiee_%s_%s',
                            $acte['Deliberation']['num_delib'],
                            $annexe['Annexe']['position'],
                            AppTools::getNameFile($annexe['Annexe']['filename'])
                        ),
                        'pdf',
                        $this->PDFStamp->stamp(
                            $annexe['Annexe']['filetype']==='application/pdf' ? $annexe['Annexe']['data']
                                : $annexe['Annexe']['data_pdf'],
                            $acte['Deliberation']
                        )
                    );
                }
            }
        }
        if (!empty($acte['Deliberation']['delib_pdf'])) {
            CakeLog::info(
                __(
                    '[Sae][%s]Acte(%s): add to zip file "DeliberationPublication"',
                    __METHOD__,
                    $acte_id
                ),
                'connector'
            );
            $this->addFileToZip(
                $name . DS . self::DELIBERATIONS_PUBLICATION,
                __('%s_deliberation_publiee', $acte['Deliberation']['num_delib']),
                'pdf',
                $this->PDFStamp->stamp($acte['Deliberation']['delib_pdf'], $acte['Deliberation'])
            );
        }

        CakeLog::info(__('[Sae][%s]Acte(%s): add to DOMDocument', __METHOD__, $acte_id), 'connector');
        $dom_depot->appendChild($doc);
    }

    /**
     *
     */
    public function addFileToZip($dirName, $filename, $ext, $contents)
    {
        CakeLog::info(__('[Sae][%s]Acte: add file %s', __METHOD__, $dirName . DS . $filename), 'connector');
        $this->zip->addFromString(self::DELIBERATIONS . DS . $dirName . DS . $filename . '.' . $ext, $contents);
    }

    /**
     * @param $folderSend
     * @return string
     */
    public function send()
    {
        CakeLog::info(__('[Sae][%s]Find folder and files', __METHOD__), 'connector');
        $zipFiles = $this->folderSend->find('.*\.zip');
        $fileZip = new File($this->folderSend->pwd() . DS . $zipFiles[0], true, 0644);
        CakeLog::info(__('[Sae][%s]File send %s (%s)', __METHOD__, $fileZip->name, $fileZip->mime()), 'connector');
        $xmlFiles = $this->folderSend->find('.*\.xml');
        $fileXml = new File($this->folderSend->pwd() . DS . $xmlFiles[0], true, 0644);
        CakeLog::info(__('[Sae][%s]File send %s (%s)', __METHOD__, $fileXml->name, $fileXml->mime()), 'connector');

        return $this->Sae->sendDossierSeance($fileZip, $fileXml);
    }

    public function sendActe($acteId)
    {
        $data = [];
        $data['Deliberation']['id'] = $acteId;


        $acte = $this->Deliberation->find('first', [
            'fields' => [
                'num_delib','pastell_id','typeacte_id'
            ],
            'conditions' => ['Deliberation.id' => $acteId],
            'recursive' => -1
        ]);

        if (empty($acte['Deliberation']['pastell_id'])) {
            throw new CakeException(
                __(
                    'L\'acte "%s" n\'a pas été versé au SAE car il n\'a pas été télétransmis via pastell.',
                    $acte['Deliberation']['num_delib'],
                )
            );
        }
        $data['Deliberation']['pastell_id'] = $acte['Deliberation']['pastell_id'];

        if (empty($data['Deliberation']['pastell_id'])) {
            $data['Deliberation']['num_delib'] = $acte['Deliberation']['num_delib'];
            //Récupération de la nature
            $nature = ClassRegistry::init('Nature');
            $data['acte_nature_code'] = $nature->getNatureCodeByTypeActe($acte['Deliberation']['typeacte_id']);

            $this->Sae->send($data, $this->Deliberation->getAnnexesToSend($acteId));
        }
        $sent = $this->Sae->send($data);

        if ($sent) {
            $this->Deliberation->id = $acteId;
            $this->Deliberation->saveField('sae_etat', SaeVersement::VERSER);
            $this->Deliberation->setHistorique(
                __('Acte envoyé au SAE'),
                $acteId,
                null,
                AuthComponent::user('id')
            );
        } else {
            throw new CakeException(
                __(
                    'L\'acte "%s" n\'a pas été versé au SAE.',
                    $acte['Deliberation']['num_delib']
                )
            );
        }
    }

    /**
     *
     */
    public function createElementThemesForActe(&$dom, &$doc, $themeId)
    {
        $libelleThemesLevel = [];

        $theme = $this->Theme->find('first', [
            'recursive' => -1,
            'fields' => ['libelle', 'order', 'lft', 'rght'],
            'conditions' => ['Theme.id' => $themeId]]);

        $themes = $this->Theme->find('all', [
            'recursive' => -1,
            'fields' => ['libelle'],
            'conditions' => ['lft <=' => $theme['Theme']['lft'], 'rght >=' => $theme['Theme']['rght']],
            'order' => ['lft']]);
        foreach ($themes as $i => $theme) {
            $libelleThemesLevel['niveau' . ($i + 1)] = $theme['Theme']['libelle'];
        }

        $document = $this->createElement($dom, 'themeProjet', null);
        foreach ($libelleThemesLevel as $libelleThemeLevelKey => $libelleThemeLevel) {
            $theme = $this->createElement($dom, $libelleThemeLevelKey, $libelleThemeLevel);
            $document->appendChild($theme);
        }

        $doc->appendChild($document);
    }
}
