<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class SeanceToolsComponent extends Component
{
    public $components = [
        'Session',
        'Acl',
        'Auth',
        'ProjetTools',
        'Flash'
    ];

    /**
     * Request object
     *
     * @var CakeRequest
     */
    public $request;

    /**
     * Response object
     *
     * @var CakeResponse
     */
    public $response;

    /**
     * Response object
     *
     * @var CakeResponse
     */
    public $controller;

    /**
     *
     * @param Controller $controller
     */
    public function initialize(Controller $controller)
    {
        $this->controller =& $controller;
        $this->request = $controller->request;
        $this->response = $controller->response;

        parent::initialize($controller);
    }

    /**
     * Fonction générique pour afficher les projets sour forme d'index
     * @param string $render
     * @param array $projets
     * @param string $titreVue
     * @param array $listeActions
     * @param array $listeLiens
     * @param int|null $nbProjets
     * @throws Exception
     */
    public function afficheProjets(
        string $render = 'index',
        array  &$projets = [],
        string $titreVue = null,
        array  $listeActions = [],
        array  $listeLiens = [],
        int $nbProjets = null
    ): void {
        App::uses('Typeseance', 'Model');
        $typeseanceModel = ClassRegistry::init('Typeseance');

        // initialisation de l'utilisateur connecté et des droits
        $this->controller->set('typeseances', $typeseanceModel->find('list', ['recursive' => -1]));

        $this->request->data = $projets;

        //récupération des drois poour bouton
        $droits = $this->ProjetTools->getDroits([
            'Projets' => ['read', 'update', 'delete'],
            'reattribuerTous' => 'read',
            'editerTous' => 'read',
            'supprimerTous' => 'read',
            'duplicate' => 'read',
            'goNext' => 'read',
            'validerEnUrgence' => 'read',
            'editPostSign' => 'read']);

        // Gestion par lot
        if ($render === 'traitement_lot') {
            $this->controller->set('traitement_lot', true);
            $list_modeles = $this->Modeltemplate->find('list', [
                'recursive' => -1,
                'fields' => ['Modeltemplate.name'],
                'conditions' => ['modeltype_id' => [MODELE_TYPE_RECHERCHE]]
            ]);
            $generation = ['generation' => __('Générer un document des projets sélectionnés')];
            if (isset($this->viewVars['actions_possibles'])) {
                $this->viewVars['actions_possibles']['generation'] = __('Générer un document des projets sélectionnés');
            } elseif (!empty($list_modeles)) {
                $this->controller->set('actions_possibles', $generation);
            }
            $this->controller->set('modeles', $list_modeles);
            $render = 'index';
        }

        /* initialisation pour chaque projet ou délibération */
        if (!empty($this->request->data)) {
            foreach ($this->request->data as $i => $projet) {
                // initialisation des icônes
                if (isset($projet[0]) && $nbProjets === 1) {
                    $projet['Deliberation'] = $projet[0];
                }
                //On cherche à savoir si la délibération est vérrouillée par un utilisateur
                $this->ProjetTools->locked($this->request->data[$i]['Deliberation']);

                $this->getRapporteur($this->request->data[$i], $projet);

                $this->getLastVisa($this->request->data[$i], $projet);

                $this->request->data[$i]['Deliberation']['num_pref'] =
                    !empty($this->request->data[$i]['Deliberation']['num_pref']) ? $this->ProjetTools->getMatiereByKey(
                        $this->request->data[$i]['Deliberation']['num_pref']
                    ) : null;

                $this->getLastCommentaire($this->request->data[$i], $projet);
                //recupération de la liste des annexes
                if (!empty($projet['Annexe'])) {
                    foreach ($projet['Annexe'] as $key => $annexe) {
                        $this->request->data[$i]['Deliberation']['annexes'][$key]['filename'] = $annexe['filename'];
                        $this->request->data[$i]['Deliberation']['annexes'][$key]['id'] = $annexe['id'];
                    }
                }

                if ($projet['Deliberation']['etat'] == 0 && $projet['Deliberation']['anterieure_id'] != 0) {
                    $this->request->data[$i]['iconeEtat'] = $this->ProjetTools->iconeEtat(
                        $projet['Deliberation']['id'],
                        -2,
                        $projet['Nature']['code'] ?? $projet['Typeacte']['Nature']['code']
                    );
                } elseif ($projet['Deliberation']['etat'] === 1) {
                    App::uses('Traitement', 'Cakeflow.Model');
                    $traitementModel = ClassRegistry::init('Cakeflow.Traitement');

                    //Recherche si l'auteur est dans le circuit de validation du projet
                    $estDansCircuit = $traitementModel->triggerDansTraitementCible(
                        $this->Auth->user('id'),
                        $projet['Deliberation']['id']
                    );

                    $tourDansCircuit = $estDansCircuit ? $traitementModel->positionTrigger(
                        $this->Auth->user('id'),
                        $projet['Deliberation']['id']
                    ) : 0;
                    $estRedacteur = ($this->Auth->user('id') == $projet['Deliberation']['redacteur_id']);
                    $this->request->data[$i]['iconeEtat'] = $this->ProjetTools->iconeEtat(
                        $projet['Deliberation']['id'],
                        $projet['Deliberation']['etat'],
                        $projet['Nature']['code'] ?? $projet['Typeacte']['Nature']['code'],
                        false,
                        $estDansCircuit,
                        $estRedacteur,
                        $tourDansCircuit
                    );
                } else {
                    $this->request->data[$i]['iconeEtat'] = $this->ProjetTools->iconeEtat(
                        $projet['Deliberation']['id'],
                        $projet['Deliberation']['etat'],
                        $projet['Nature']['code'] ?? $projet['Typeacte']['Nature']['code'],
                        $droits['editerTous/read']
                    );
                }
                $this->getLibelleEtat($this->request->data[$i], $projet);
                // initialisation des séances
                $listeTypeSeance = [];
                $this->request->data[$i]['listeSeances'] = [];
                if (!empty($projet['DeliberationSeance'])) {
                    foreach ($projet['DeliberationSeance'] as $seance) {
                        if (!empty($seance['Seance']['id'])) {
                            $this->request->data[$i]['listeSeances'][] = ['seance_id' => $seance['Seance']['id'],
                                'type_id' => $seance['Seance']['type_id'],
                                'color' => !empty($seance['Seance']['Typeseance']['color']) ?
                                    $seance['Seance']['Typeseance']['color'] : '',
                                'action' => $seance['Seance']['Typeseance']['action'],
                                'libelle' => $seance['Seance']['Typeseance']['libelle'],
                                'date' => $seance['Seance']['date']];
                            $listeTypeSeance[] = $seance['Seance']['type_id'];
                        }
                    }
                }
                if (!empty($projet['DeliberationTypeseance'])) {
                    foreach ($projet['DeliberationTypeseance'] as $typeseance) {
                        if (!in_array($typeseance['Typeseance']['id'], $listeTypeSeance, true)) {
                            $this->request->data[$i]['listeSeances'][] = ['seance_id' => null,
                                'type_id' => $typeseance['Typeseance']['id'],
                                'color' => !empty($typeseance['Typeseance']['color']) ?
                                    $typeseance['Typeseance']['color'] : '',
                                'action' => $typeseance['Typeseance']['action'],
                                'libelle' => $typeseance['Typeseance']['libelle'],
                                'date' => null];
                        }
                    }
                }

                $this->request->data[$i]['listeSeances'] = Hash::sort(
                    $this->request->data[$i]['listeSeances'],
                    '{n}.action',
                    'asc'
                );

                $this->ProjetTools->actionDisponible($this->request->data[$i], $droits, $listeActions);

                //remplissage des 9 cases depuis le json, pour chaque projet
                $this->request->data[$i]['fields'] = $this->ProjetTools->get9casesData($projet['Deliberation']['id']);

                // initialisation des dates, modèle et service
                //$seances_id = array();

                $this->getGenerateModel($this->request->data[$i], $projet);

                if (isset($this->data[$i]['Service']['id'])) {
                    $this->request->data[$i]['Service']['name'] =
                        $this->Deliberation->Service->doList($projet['Service']['id']);
                }
                if (isset($this->data[$i]['Deliberation']['date_limite'])) {
                    $this->request->data[$i]['Deliberation']['date_limite'] = $projet['Deliberation']['date_limite'];
                }
            }
        }
        $this->controller->set('titreVue', $titreVue);
        $this->controller->set('listeLiens', $listeLiens);
        if ($nbProjets === null) {
            $nbProjets = count($projets);
        }
        $this->controller->set('nbProjets', $nbProjets);

        $this->controller->render($render);
    }

    private function getLastVisa(&$data, $projet): void
    {
        App::uses('Traitement', 'Cakeflow.Model');
        $traitementModel = ClassRegistry::init('Cakeflow.Traitement');

        $lastVisa = $traitementModel->getLastVisaTrigger($projet['Deliberation']['id']);
        //Récupère l'étape courante
        $data['Circuit']['current_step'] =
            $projet['Deliberation']['etat'] !== 0 ? $traitementModel->getEtapeCouranteName(
                $projet['Deliberation']['id']
            ) : _('En cours de rédaction');

        if (!empty($lastVisa)) {
            $data['last_viseur'] = $lastVisa['last_viseur'];
            $data['last_viseur_date'] = $lastVisa['date'];
        }
    }

    private function getRapporteur(&$data, $projet): void
    {
        App::uses('Acteur', 'Model');
        $acteurModel = ClassRegistry::init('Acteur');
        // Récupération du rapporteur principal
        $acteurModel->virtualFields['name'] = 'Acteur.nom || \' \' || Acteur.prenom';
        $acteur = $acteurModel->find('first', [
            'fields' => ['name'],
            'conditions' => ['Acteur.id' => $projet['Deliberation']['rapporteur_id']],
            'recursive' => -1
        ]);
        $data['Deliberation']['rapporteur_principal'] =
            $acteur['Acteur']['name'] ?? _('');
    }

    private function getLastCommentaire(&$data, $projet)
    {
        App::uses('Commentaire', 'Model');
        $commentaire = ClassRegistry::init('Commentaire');

        //recuperation des derniers commentaires
        $data['Deliberation']['commentaires'] = $commentaire->find('all', [
            'fields' => ['Commentaire.texte', 'Commentaire.created'],
            'contain' => ['User.nom', 'User.prenom'],
            'conditions' => [
                'Commentaire.delib_id' => $projet['Deliberation']['id'],
                'Commentaire.pris_en_compte' => 0
            ],
            'order' => ['Commentaire.created' => 'DESC'],
            'recursive' => -1,
            'limit' => 3
        ]);
    }
    private function getLibelleEtat(&$data, $projet)
    {
        App::uses('ProjetEtat', 'Model');
        $projetEtat = ClassRegistry::init('ProjetEtat');

        $data['etat_libelle'] = $projetEtat->libelleEtat(
            $projet['Deliberation']['etat'],
            $projet['Nature']['code'] ?? $projet['Typeacte']['Nature']['code']
        );
    }

    private function getGenerateModel(&$data, $projet)
    {
        App::uses('Typeseance', 'Model');
        $typeseance = ClassRegistry::init('Typeseance');
        App::uses('Typeacte', 'Model');
        $typeacte = ClassRegistry::init('Typeacte');

        if (!empty($data['listeSeances'])
        ) {
            foreach ($data['listeSeances'] as $seance) {
                if ($seance['action'] === 0) {
                    $data['Modeltemplate']['id'] =
                        $typeseance->modeleProjetDelibParTypeSeanceId(
                            $seance['type_id'],
                            $projet['Deliberation']['etat']
                        );
                    break;
                }
            }
        }
        if (!isset($data['Modeltemplate']['id'])) {
            $data['Modeltemplate']['id'] =
                $typeacte->getModelId(
                    $projet['Deliberation']['typeacte_id'],
                    'modele_projet_id'
                );
        }
    }

    /**
     * [checkTypeSeance description]
     * @return [type] [description]
     */
    public function checkTypeSeance($id, $crud)
    {
        App::uses('Seance', 'Model');
        $seance = ClassRegistry::init('Seance');

        if (!$this->Acl->check(
            ['User' => ['id' => $this->Auth->user('id')]],
            ['Typeseance' => ['id' => $seance->getType($id)]],
            $crud
        )
        ) {
            $this->Flash->set(
                __(
                    "Vous ne pouvez pas %s la seance '%s' en raison de son type de seance.",
                    ($crud === 'update' ? __('éditer') : ($crud === 'delete' ? __('supprimer') : '')),
                    $id
                ),
                [
                    'element' => 'growl',
                    'params' => ['type' => 'danger']]
            );

            return $this->redirect($this->previous);
        }
    }
}
