<?php

/**
 * Préparation du contenu des vues détaillées (view) dans les contrôleurs.
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       web-delib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 * @version     v4.3
 * @package     app.Controller.Component
 */
class VueDetailleeComponent extends Component
{
    private $contenuVue = [];
    private $iOnglet;
    private $iSection;
    private $iLigne;

    /**
     *
     * @param type $titreVue
     * @param type $lienRetourTitle
     * @param type $lienRetourUrl
     * @param type $actions
     */
    public function __construct(
        $titreVue = 'Vue détaillée',
        $lienRetourTitle = 'Retour',
        $lienRetourUrl = ['action' => 'index'],
        $actions = []
    ) {
        $this->contenuVue['titreVue'] = $titreVue;
        $this->contenuVue['lienRetour'] = ['title' => $lienRetourTitle, 'url' => $lienRetourUrl];
        $this->contenuVue['actions'] = $actions;
        $this->contenuVue['onglets'] = [];
        $this->iOnglet = 0;
    }

    /**
     * Ajoute un onglet
     *
     * @param type $nom
     */
    public function ajouteOnglet($nom = '')
    {
        $this->iOnglet++;
        $this->iSection = 0;
        $this->contenuVue['onglets'][$this->iOnglet] = ['titre' => $nom, 'sections' => []];
    }

    /**
     * Ajoute une section
     *
     * @param type $nom
     * @param type $options
     */
    public function ajouteSection($nom = '', $options = [])
    {
        // Initialisation des valeurs par défaut
        $defaut = ['tag' => 'h4', 'htmlAttributes' => []];
        $options = array_merge($defaut, $options);

        if (!$this->iOnglet) {
            $this->ajouteOnglet();
        }
        $this->iSection++;
        $this->iLigne = 0;
        $this->contenuVue['onglets'][$this->iOnglet]['sections'][$this->iSection] = [
            'titre' => $nom,
            'tag' => $options['tag'],
            'htmlAttributes' => $options['htmlAttributes'],
            'lignes' => []
        ];
    }

    /**
     * ajoute une nouvelle ligne à la dernière section
     *
     * @param type $libelle
     * @param type $valeur
     * @param type $ddClasse
     */
    public function ajouteLigne($libelle, $valeur = '', $ddClasse = '')
    {
        if (!$this->iSection) {
            $this->ajouteSection();
        }
        $this->iLigne++;
        $this->contenuVue['onglets'][$this->iOnglet]['sections'][$this->iSection]['lignes'][$this->iLigne][] =
            ['libelle' => $libelle, 'valeur' => $valeur, 'ddClasse' => $ddClasse];
    }

    /**
     * ajoute un nouvel élément à la dernière ligne de la dernière section
     *
     * @param type $libelle
     * @param type $valeur
     * @param type $ddClasse
     */
    public function ajouteElement($libelle, $valeur = '', $ddClasse = '')
    {
        if (!$this->iLigne) {
            $this->ajouteLigne($libelle, $valeur);
        } else {
            $this->contenuVue['onglets'][$this->iOnglet]['sections'][$this->iSection]['lignes'][$this->iLigne][] =
                ['libelle' => $libelle, 'valeur' => $valeur, 'ddClasse' => $ddClasse];
        }
    }

    /**
     * retourne le contenue de la vue
     *
     * @return type
     */
    public function getContenuVue()
    {
        return $this->contenuVue;
    }
}
