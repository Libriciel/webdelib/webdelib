<?php
App::uses('Signature', 'Lib');

class SignaturesComponent extends Component
{
    public function getCircuits()
    {
        $circuits = [];
        try {
            $Signature = new Signature();
            $circuits = $Signature->printCircuits();
        } catch (\Throwable $e) {
            $controller = $this->_Collection->getController();
            $controller->Flash->set($e->getMessage(), [
                'element' => 'growl',
                'params' => ['type' => 'danger']
            ]);
        }

        // Si erreur de connexion au parapheur
        $circuits['Standard'] = ['-1' => 'Signature manuscrite'];

        return $circuits;
    }

    public function updateAll()
    {
        try {
            $Signature = new Signature();
            /** @noinspection PhpParamsInspection ne pas avertir de la non-existence
             * de la fonction (passage par __call())
             */
            $ret = $Signature->updateAll();
            $ret = preg_replace(
                '/\s+/',
                ' ',
                nl2br(htmlspecialchars(trim($ret), ENT_QUOTES))
            );
            $this->_Collection->getController()->Flash->set(
                $ret,
                ['element' => 'growl', 'params' => ['type' => 'info']]
            );
        } catch (\Throwable $e) {
            $this->_Collection->getController()->Flash->set(
                $e->getMessage(),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
        }
    }
}
