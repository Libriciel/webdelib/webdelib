<?php
/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * Component History
 *
 * @package app.Controller.Component
 */
class HistoryComponent extends Component
{
    /**
     * Other components utilized by AuthComponent
     *
     * @var array
     */
    public $components = ['Session', 'RequestHandler'];

    /**
     * The session key name where the record of the current user is stored. Default
     * key is "Auth.User". If you are using only stateless authenticators set this
     * too false to ensure session is not started.
     *
     * @var string
     */
    public static $sessionKey = 'History';

    /**
     * Link for which user History is not required.
     *
     * @var array
     * @see HistoryComponent::deny()
     */
    public $unAuthorize = [];

    /**
     * Controller actions for which user History is not required.
     *
     * @var array
     * @see HistoryComponent::deny()
     */
    public $unAllowedActions = [];

    /**
     * A URL (defined as a string or array) to the controller action that handles
     * logins. Defaults to `/users/login`.
     *
     * @var mixed
     */
    public $loginAction = [
        'controller' => 'users',
        'action' => 'login',
        'plugin' => null
    ];

    /**
     * Request object
     *
     * @var CakeRequest
     */
    public $request;

    /**
     * Response object
     *
     * @var CakeResponse
     */
    public $response;

    /**
     * Method list for bound controller.
     *
     * @var array
     */
    private $methods = [];

    /**
     * @var array
     */
    public $previous = [];

    /**
     * @var array
     */
    public $here = [];
    /**
     * Initializes AuthComponent for use in the controller.
     *
     * @param Controller $controller A reference to the instantiating controller object
     * @return void
     */
    public function initialize(Controller $controller)
    {
        $this->request = $controller->request;
        $this->response = $controller->response;
        $this->methods = $controller->methods;
    }

    /**
     * Called automatically after controller beforeFilter
     * @param Controller $controller A reference to the controller object
     */
    public function startup(Controller $controller)
    {
        if ($this->Session->check(self::$sessionKey)) {
            $session = $this->Session->read(self::$sessionKey);
        }
        $list = !empty($session['data']) ? $session['data'] : [];
        $controller->here = !empty($session['here']) ? $session['here'] : [];
        $controller->previous = !empty($session['previous']) ? $session['previous'] : [];

        if (!$this->unauthorized($controller) && !empty($controller->request->params)) {
            $route = $this->purgeRequest($controller->request);
            //Ajoute l'url courante au début du tableau
            if (empty($list) || $list[0] !== $route) {
                //Insère la route courante en début de tableau (indice 0)
                array_unshift($list, $route);
            }

            if (count($list) > 2 && $list[0] === $list[2]) {
                array_shift($list);
                array_shift($list);
            }

            //Ne garder que 6 éléments dans l'historique
            if (count($list) > 6) {
                array_pop($list);
            }

            $controller->here = $list[0];
            $controller->previous = count($list) === 1 ? $list[0] : $list[1];

            $this->Session->write(self::$sessionKey, [
                'data' => $list,
                'here' => $controller->here,
                'previous' => $controller->previous
            ]);
        } elseif (empty($list)) { // Lorsque la liste est vide et la page non authorisé pour l'historique
            $controller->previous = $controller->here = $controller->Auth->loginRedirect;
        } else { // Lorsque le page est purgé le retour doit être fait sur le previous
            $controller->here = $this->purgeRequest($controller->request);
            $controller->previous = $list[0];
        }

        if (!empty($controller->here)) {
            $controller->set('here', $controller->here);
        }

        if (!empty($controller->previous)) {
            $controller->set('previous', $controller->previous);
        }
    }

    /**
     *
     */
    public function reset()
    {
        if ($this->Session->check(self::$sessionKey)) {
            $this->Session->delete(self::$sessionKey);
        }
    }

    /**
     * Handle unauthorized access attempt
     *
     * @param Controller $controller A reference to the controller object
     * @return boolean Returns false
     */
    protected function unauthorized(Controller $controller)
    {
        if (!empty($controller->request->params['requested'])) {
            return true;
        }

        if ($this->RequestHandler->isAjax()) {
            return true;
        }

        array_unshift($this->unAuthorize, Router::normalize($this->loginAction));

        foreach ($this->unAuthorize as $value) {
            if (stripos(strtolower(Router::normalize($controller->request->url)), strtolower($value)) !== false) {
                return true;
            }
        }

        return $this->isNotAllowed($controller);
    }

    /**
     * Takes a list of actions in the current controller for which History is not required, or
     * no parameters to allow all actions.
     *
     * You can use allow with either an array, or var args.
     *
     * `$this->History->deny(array('edit', 'add'));` or
     * `$this->History->deny('edit', 'add');` or
     * `$this->History->deny();` to allow all actions
     *
     * @param string|array $action Controller action name or array of actions
     * @return void
     */
    public function deny($action = null)
    {
        $args = func_get_args();
        if (empty($args) || $action === null) {
            $this->unAllowedActions = $this->methods;
            return;
        }
        if (isset($args[0]) && is_array($args[0])) {
            $args = $args[0];
        }
        $this->unAllowedActions = array_merge($this->unAllowedActions, $args);
    }

    /**
     * Checks whether current action is accessible without authentication.
     *
     * @param Controller $controller A reference to the instantiating controller object
     * @return boolean True if action is accessible without authentication else false
     */
    protected function isNotAllowed(Controller $controller)
    {
        $action = strtolower($controller->request->params['action']);
        if (in_array($action, array_map('strtolower', $this->unAllowedActions), true)) {
            return true;
        }

        return false;
    }

    /**
     * Purge du Request pour l'histoique des liens
     * @param Controller $controller A reference to the instantiating controller object
     * @return array tableau de paramêtre de l'action
     */
    protected function purgeRequest(CakeRequest $params)
    {
        if (!($params instanceof CakeRequest)) {
            throw Exception(__('this request is not instance of CakeRequest'), 500);
        }
        $params = $params->params;

        if (empty($params['prefix'])) {
            $prefixes = Configure::read('Routing.prefixes');
            foreach ($prefixes as $value) {
                $params[$value] = false;
            }
        }

        $pass = isset($params['pass']) ? $params['pass'] : [];
        $named = isset($params['named']) ? $params['named'] : [];

        unset(
            $params['pass'],
            $params['named'],
            $params['paging'],
            $params['models'],
            $params['url'],
            $params['autoRender'],
            $params['bare'],
            $params['requested'],
            $params['return'],
            $params['_Token']
        );

        return array_merge($params, $pass, $named);
    }

    /**
     *  Retourner en arrière après un lien précis
     * @param string $url
     */
    public function goBackBefore($url)
    {
        if ($this->Session->check(self::$sessionKey)) {
            $session = $this->Session->read(self::$sessionKey);
            $list = !empty($session['data']) ? $session['data'] : [];
            for ($i = 0; $i <= 10; $i++) {
                $route = $list[$i];
                unset($list[$i]);
                if (stripos(strtolower(Router::normalize($route)), strtolower($url)) !== false) {
                    break;
                }
            }
            $list = array_values($list);

            $this->Session->write(self::$sessionKey, [
                'data' => $list,
                'here' => $route,
                'previous' => count($list) === 1 ? $route : $list[0]
            ]);

            return $list[0];
        }
    }
}
