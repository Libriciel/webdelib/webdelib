<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Component', 'Controller');
App::uses('LockedActionException', 'Lib/Exceptions');
App::uses('TokenMethod', 'Model');

/**
 * Fichier source de la classe TokensMethodsComponent.
 *
 * @package app.Controller.Component
 */
class TokensMethodsComponent extends Component
{
    /**
     * Controller using this component.
     *
     * @var Controller
     */
    public $Controller = null;

    /**
     * @var array
     */
    public $components = [ 'Session' ];

    /**
     * On initialise le modèle TokenMethod si Configure::write( 'TokensMethods.disabled' ) n'est pas à true.
     *
     * @param Controller $controller Controller with components to initialize
     * @return void
     */
    public function initialize(Controller $controller)
    {
        $settings = $this->settings;
        parent::initialize($controller, $settings);
        $this->Controller = $controller;

        $this->TokenMethod = ClassRegistry::init('TokenMethod');
    }

    /**
     * @return boolean
     */
    public function get($params = [])
    {
        if (Configure::read('TokensMethods.disabled')) {
            return true;
        }
        if (AuthComponent::user('Profil.role_id') === 2) {
            if (isset($params['entity_id'])) {
                $this->clearByEntityId($params['entity_id']);
            }
        }

        if (!isset($params['controller']) && !isset($params['action'])) {
            $params = Hash::merge(
                [
                    'controller' => mb_strtolower($this->Controller->name),
                    'action' => 'edit'
                ],
                $params
            );
        }

        $controllerName = mb_strtolower($params['controller']);
        $actionName = mb_strtolower($params['action']);
        $user_id = $params['user_id'] ?? $this->Session->read('Auth.User.id');
        unset($params['controller'], $params['action'], $params['user_id']);

        $params = serialize(!empty($params) ? (array)$params : null);

        $this->TokenMethod->begin();

        $sq = $this->TokenMethod->sql(
            [
                'alias' => 'tokensmethods',
                'fields' => [
                    'tokensmethods.id',
                    'tokensmethods.php_sid',
                    'tokensmethods.user_id',
                    'tokensmethods.modified',
                ],
                'conditions' => [
                    'tokensmethods.controller' => $controllerName,
                    'tokensmethods.action' => $actionName,
                    'tokensmethods.params' => $params,
                    // INFO: si on get et que l'on release dans la même page, il ne fera pas le second
                    // SELECT même si cacheQueries est à false.
                    '( \''.microtime(true).'\' IS NOT NULL )'
                ],
                'recursive' => -1
            ]
        );

        $sq = "{$sq} FOR UPDATE";
        $result = $this->TokenMethod->query($sq);

        if ($result === false) {
            $this->TokenMethod->rollback();
            $this->Controller->cakeError('error500');
            return;
        }

        $fonctionNonVerrouillee = (
            empty($result)
            || ($result[0]['tokensmethods']['user_id'] ==  AuthComponent::user('id'))
            || (strtotime($result[0]['tokensmethods']['modified']) < strtotime('-'.self::readTimeout().' seconds'))
        );

        if ($fonctionNonVerrouillee) {
            $jetonfonction = [
                'TokenMethod' => [
                    'controller' => $controllerName,
                    'action' => $actionName,
                    'params' => $params,
                    'php_sid' => !empty($result) ? $result[0]['tokensmethods']['php_sid'] : $this->Session->id(),
                    'user_id' => $user_id,
                ]
            ];

            if (!empty($result) && !empty($result[0]['tokensmethods']['id'])) {
                $jetonfonction['TokenMethod']['id'] = $result[0]['tokensmethods']['id'];
            }

            $this->TokenMethod->create($jetonfonction);
            if (!$this->TokenMethod->save(null, [ 'atomic' => false ])) {
                $this->TokenMethod->rollback();
                $this->Controller->cakeError('error500');
            }

            $this->TokenMethod->commit();
        } else {
            $this->TokenMethod->rollback();
            return false ;
        }

        return true;
    }

    /**
     * On relache un (ensemble de) jeton(s).
     *
     * @param mixed $dossiers Un id de dossier ou un array d'ids de dossiers.
     * @return boolean
     */
    public function release($params = [])
    {
        $params = Hash::merge(
            [
              'controller' => $this->Controller->name,
              'action' => $this->Controller->action
            ],
            $params
        );
        $controllerName = mb_strtolower($params['controller']);
        $actionName = mb_strtolower($params['action']);
        unset($params['controller'], $params['action']);

        $params = serialize(!empty($params) ? (array)$params : null);

        $this->TokenMethod->begin();

        $conditions = [
            'tokensmethods.controller' => $controllerName,
            'tokensmethods.action' => $actionName,
            'tokensmethods.params' => $params,
            // INFO: si on get et que l'on release dans la même page, il ne fera pas le second
            // SELECT même si cacheQueries est à false.
            //'( \''.microtime( true ).'\' IS NOT NULL )'
        ];
        $sql = $this->TokenMethod->sql(
            [
                'alias' => 'tokensmethods',
                'fields' => [
                    'tokensmethods.id',
                    'tokensmethods.php_sid',
                    'tokensmethods.user_id',
                    'tokensmethods.modified',
                ],
                'conditions' => $conditions,
                'recursive' => -1
            ]
        );

        $sql = "{$sql} FOR UPDATE";

        $results = $this->TokenMethod->query($sql);
        if ($results === false) {
            $this->TokenMethod->rollback();
            die('Erreur de mise à jour en base ');
            return false;
        }

        if ($this->TokenMethod->deleteAll($conditions, false, false) == false) {
            $this->TokenMethod->rollback();
            die('Erreur de mise à jour en base ');
            return false;
        }

        $this->TokenMethod->commit();

        return true;
    }

    /**
     * Retourne une condition concernant l'instant pivot en-dessous duquel les connections sont
     * considérées comme étant expirées.
     *
     * @deprecated
     * @return string
     */
    protected function conditionsValid()
    {
        return [ 'modified >=' => strftime('%Y-%m-%d %H:%M:%S', strtotime('-'.self::readTimeout().' seconds')) ];
    }

    /**
     * Vérifie si une pour un certain contrôleur, une certaine action et d'éventuels paramètres, un autre
     * utilisateur a déjà bloqué l'accès à la fonction.
     *
     * @param mixed $params
     * @return boolean
     */
    public static function locked($params = [])
    {
        if (Configure::read('TokensMethods.disabled')) {
            return false;
        }

        $controllerName = mb_strtolower($params['controller']);
        $actionName = mb_strtolower($params['action']);

        unset($params['controller'], $params['action']);

        $params = serialize(!empty($params) ? (array)$params : null);

        $conditions = [
            'tokensmethods.controller' => $controllerName,
            'tokensmethods.action' => $actionName,
            'tokensmethods.params' => $params,
            // INFO: si on get et que l'on release dans la même page, il ne fera pas le second
            // SELECT même si cacheQueries est à false.
            // '( \''.microtime( true ).'\' IS NOT NULL )'
        ];

        $tokenMethodModel = ClassRegistry::init('TokenMethod');
        $sql = $tokenMethodModel->sql(
            [
                'alias' => 'tokensmethods',
                'fields' => [
                    'tokensmethods.id',
                    'tokensmethods.php_sid',
                    'tokensmethods.user_id',
                    'tokensmethods.modified',
                ],
                'conditions' => $conditions,
                'recursive' => -1
            ]
        );

        $token = $tokenMethodModel->query($sql);

        if (empty($token)
            || ($token[0]['tokensmethods']['user_id'] ==  AuthComponent::user('id'))
            || (strtotime($token[0]['tokensmethods']['modified']) < strtotime('-'.self::readTimeout().' seconds'))) {
            return false;
        }

        return [
          'user_id' => $token[0]['tokensmethods']['user_id'],
          'last_time' => $token[0]['tokensmethods']['modified']
        ];
    }

    /**
     * [haveToken description]
     * @param  array  $params [description]
     * @return [type]         [description]
     *
     * @version 5.1.1
     * @since 5.1
     */
    public function haveToken($params = [])
    {
        //Bypass
        if (Configure::read('TokensMethods.disabled')) {
            return true;
        }

        $controllerName = mb_strtolower($params['controller']);
        $actionName = mb_strtolower($params['action']);

        unset($params['controller'], $params['action']);


        $params = serialize(!empty($params) ? (array)$params : null);

        $conditions = [
            'tokensmethods.controller' => $controllerName,
            'tokensmethods.action' => $actionName,
            'tokensmethods.params' => $params,
            // INFO: si on get et que l'on release dans la même page, il ne fera pas le second
            // SELECT même si cacheQueries est à false.
            //              '( \''.microtime( true ).'\' IS NOT NULL )'
        ];

        $tokenMethodModel = ClassRegistry::init('TokenMethod');
        $sq = $tokenMethodModel->sql(
            [
                'alias' => 'tokensmethods',
                'fields' => [
                    'tokensmethods.id',
                    'tokensmethods.php_sid',
                    'tokensmethods.user_id',
                    'tokensmethods.modified',
                    'tokensmethods.params',
                ],
                'conditions' => $conditions,
                'recursive' => -1
            ]
        );
        $result = $tokenMethodModel->query($sq);
        if ($result === false) {
            die('Erreur de mise à jour en base ');
            return false;
        }
        return !empty($result[0]['tokensmethods'])
            && $result[0]['tokensmethods']['user_id'] === AuthComponent::user('id')
            && $result[0]['tokensmethods']['params'] === $params;
    }

    /**
     * [clear description]
     * @param  [type] $user_id    [description]
     * @param  [type] $session_id [description]
     * @return [type]             [description]
     *
     * @version 5.1
     */
    public function clear($user_id = null, $session_id = null)
    {
        $Controller = $this->_Collection->getController();

        $user_id = (null === $user_id)
            ? $this->Session->read('Auth.User.id')
            : $user_id;

        $session_id = (null === $session_id)
            ? $this->Session->id()
            : $session_id;

        // Conditions pour la suppression
        $conditions = [ 'user_id' => $user_id ];
        if (Configure::read('Utilisateurs.multilogin')) {
            $conditions['php_sid'] = $session_id;
        }

        // Suppression des jetons sur les fonctions
        if (!Configure::read('TokensMethods.disabled')) {
            $Controller->loadModel('TokenMethod');
            //CakeLog::debug(__('[Token][%s]deleteAll: %s', __METHOD__, var_export($conditions, true)));
            $Controller->TokenMethod->deleteAll($conditions);
        }
    }

    /**
     * @version 5.1
     * @param $entityId
     */
    public function clearByEntityId($entityId)
    {
        $this->TokenMethod->deleteAll(
            [
                'TokenMethod.params LIKE'=>"%i:$entityId%",
                'TokenMethod.controller' => mb_strtolower($this->Controller->name)
            ]
        );
    }

    /**
     * Called before Controller::redirect().  Allows you to replace the url that will
     * be redirected to with a new url. The return of this method can either be an array or a string.
     *
     * If the return is an array and contains a 'url' key.  You may also supply the following:
     *
     * - `status` The status code for the redirect
     * - `exit` Whether or not the redirect should exit.
     *
     * If your response is a string or an array that does not contain a 'url' key it will
     * be used as the new url to redirect to.
     *
     * @param Controller $controller Controller with components to beforeRedirect
     * @param string|array $url Either the string or url array that is being redirected to.
     * @param integer $status The status code of the redirect
     * @param boolean $exit Will the script exit.
     * @return array|null Either an array or null.
     * @link @link http://book.cakephp.org/2.0/en/controllers/components.html#Component::beforeRedirect
     */
    public function beforeRedirect(Controller $controller, $url, $status = null, $exit = true)
    {
        return [ 'url' => $url, 'status' => $status, 'exit' => $exit ];
    }

    /**
     * [readTimeout description]
     * @return [type] [description]
     *
     * @version 5.1.2
     */
    private function readTimeout()
    {
        App::uses('CakeSession', 'Model/Datasource');
        return (CakeSession::$sessionTime - CakeSession::$time);
    }
}
