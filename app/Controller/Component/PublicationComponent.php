<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Component', 'Controller');
App::uses('AppTools', 'Lib');
App::uses('File', 'Utility');
App::uses('Folder', 'Utility');

use Http\Adapter\Guzzle6\Client as GuzzleClient;
use PastellClient\Client;
use Psr\Http\Client\ClientExceptionInterface;
use PastellClient\Api\DocumentsRequester as PastellDocumentsRequester;

/**
 * [MailsecComponent description]
 *
 * @version 5.2.0
 */
class PublicationComponent extends Component
{
    /**
     * [protected description]
     * @var [type]
     */
    protected $HttpRequest;
    /**
     * [protected description]
     * @var [type]
     */
    protected $documentId;

    /**
     * @param Controller $controller
     * @version 5.1
     */
    public function initialize(Controller $controller)
    {
        $this->usePublication = Configure::read('USE_PUBLICATION');

        if ($this->usePublication) {
            $this->config = [
                'host' => Configure::read('PUBLICATION_HOST'),
                'username' => Configure::read('PUBLICATION_LOGIN'),
                'password' => Configure::read('PUBLICATION_PWD'),
                'pastell_ide' => Configure::read('PUBLICATION_IDE'),
                'pastell_flux' => Configure::read('PUBLICATION_FLUX'),
                'ssl_allow_self_signed' => Configure::read('PUBLICATION_SSL_ALLOW_SELF_SIGNED'),
            ];

            $this->client = $this->getInsecurePastellClient();
        }
    }

    /**
     * @return Client
     */
    private function getInsecurePastellClient(): Client
    {
        $httpClient = GuzzleClient::createWithConfig([
            'attendre' => $this->proxyContinueDisabled,
            'verify' => false
        ]);
        $client = Client::createWithHttpClient($httpClient);
        $client->setUrl(Configure::read('PASTELL_HOST'));
        $client->authenticate(Configure::read("PASTELL_LOGIN"), Configure::read("PASTELL_PWD"));

        return $client;
    }

    /**
     * [sendMail description]
     * @param  [type] $data [description]
     * @return [type]       [description]
     *
     * @version 5.2.0
     */
    public function send($data)
    {
        if ($this->usePublication) {
            $this->createDocument();
            $this->updateDocument($data);
            $this->sendDocument();

            return $this->documentId;
        }
    }

    /**
     * [createDocument description]
     * @return [type] [description]
     */
    private function createDocument()
    {
        $documentsRequester = new PastellDocumentsRequester($this->client);
        $document = $documentsRequester->create($this->config['pastell_ide'], $this->config['pastell_flux']);

        $this->documentId = $document->getIdD();
    }

    /**
     * [updateDocument description]
     * @param  CakeEmail $email [description]
     * @return [type]           [description]
     */
    private function updateDocument($dataPost)
    {
        try {
            $folderTmp = new Folder(AppTools::newTmpDir(TMP . 'files' . DS . 'publier'), true, 0777);

            $documentsRequester = new PastellDocumentsRequester($this->client);
            $document = $documentsRequester->show($this->config['pastell_ide'], $this->documentId);
            $document->documentData->data['numero_de_lacte'] = $dataPost['numero_de_lacte'];

            $document = $documentsRequester->update($document);

            $filePdf = new File($folderTmp->pwd() . DS . $dataPost['numero_de_lacte'] . '.pdf');
            $filePdf->append($dataPost['document_acte']);
            $document = $documentsRequester->uploadFile(
                $document,
                'document_acte',
                $filePdf->pwd(),
                $filePdf->name
            );
            $filePdf->close();

            $fileJson = new File($folderTmp->pwd() . DS . $dataPost['numero_de_lacte'] . '.json');
            $fileJson->append($dataPost['document_json']);
            $document = $documentsRequester->uploadFile(
                $document,
                'document_json',
                $fileJson->pwd(),
                $fileJson->name
            );
            $fileJson->close();

            $fileCsv = new File($folderTmp->pwd() . DS . $dataPost['numero_de_lacte'] . '.csv');
            $fileCsv->append($dataPost['document_csv']);
            $document = $documentsRequester->uploadFile(
                $document,
                'document_csv',
                $fileCsv->pwd(),
                $fileCsv->name
            );
            $fileCsv->close();

            if (!empty($dataPost['document_annexes'])) {
                $documentsRequester = new PastellDocumentsRequester($this->client);
                $document = $documentsRequester->show($this->config['pastell_ide'], $this->documentId);

                $fileNumber = 0;
                foreach ($dataPost['document_annexes'] as $annexe) {
                    $annexeFile = new File($folderTmp->path . DS . $annexe['filename'], true, 0777);
                    $annexeFile->write($annexe['content']);
                    $document = $documentsRequester->uploadFile(
                        $document,
                        'document_annexes',
                        $annexeFile->path,
                        $annexe['filename'],
                        $fileNumber++
                    );
                    $annexeFile->close();
                }
            }

            $folderTmp->delete();
        } catch (ClientExceptionInterface $e) {
            $folderTmp->delete();
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * [sendDocument description]
     * @return [type] [description]
     * @throws ClientExceptionInterface
     */
    private function sendDocument()
    {
        $documentsRequester = new PastellDocumentsRequester($this->client);
        $document = $documentsRequester->show($this->config['pastell_ide'], $this->documentId);
        $returnAction = $documentsRequester->triggerAction($document, 'orientation');

        return get_object_vars($returnAction);
    }
}
