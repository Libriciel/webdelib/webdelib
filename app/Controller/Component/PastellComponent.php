<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Component', 'Controller');
App::uses('AppTools', 'Lib');
App::uses('File', 'Utility');
App::uses('Folder', 'Utility');
App::uses('CakeTime', 'Utility');

// phpcs:disable PSR1.Files.SideEffects
use GuzzleHttp\Exception\ClientException;
use Http\Adapter\Guzzle6\Client as GuzzleClient;
use PastellClient\Client;
use PastellClient\Api\Version as PastellVersion;
use PastellClient\Api\EntitesRequester as EntitesRequester;
use Psr\Http\Client\ClientExceptionInterface;
use PastellClient\Api\ConnectorsRequester as PastellConnectorsRequester;
use PastellClient\Api\DocumentsRequester as PastellDocumentsRequester;
use PastellClient\Api\ModuleAssociationsRequester as PastellModuleAssociationsRequester;

/**
 *[PastellComponent description]
 * @package app.Controller.Component
 * @version 7.0.0
 * @since 4.3.0
 */
class PastellComponent extends Component
{
    public $components = ['ProjetTools','Session'];

    /**
     * [private description]
     * @var [type]
     */
    private $host;
    /**
     * [private description]
     * @var [type]
     */
    private $login;
    /**
     * [private description]
     * @var [type]
     */
    private $pwd;
    /**
     * [private description]
     * @var [type]
     */
    private $parapheurType;
    /**
     * [private description]
     * @var [type]
     */
    private $parapheurVisaType;
    /**
     * [private description]
     * @var [type]
     */
    private $parapheurMode;
    /**
     * [private description]
     * @var [type]
     */
    private $pastellType;
    /**
     * [private description]
     * @var [type]
     */
    private $config;
    /**
     * @var array|int[]|mixed
     */
    private $proxyContinueDisabled;

    private Client $client;

    /**
     * [__construct description]
     */
    public function __construct(ComponentCollection $collection, $settings = [])
    {
        parent::__construct($collection, $settings);

        $this->use_pastell = (bool)Configure::read('USE_PASTELL');
        if (empty(Configure::read('PASTELL_HOST'))
            xor empty(Configure::read("PASTELL_LOGIN"))
            xor empty(Configure::read("PASTELL_PWD"))) {
            CakeLog::error(
                'Informations de connexion manquantes (désactivation du connecteur)',
                'pastell'
            );
            $this->use_pastell = false;
        }
        $this->host = Configure::read('PASTELL_HOST');
        $this->login = Configure::read('PASTELL_LOGIN');
        $this->pwd = Configure::read('PASTELL_PWD');
        $this->pastellType = Configure::read('PASTELL_TYPE');
        $this->parapheurMode = Configure::read('PASTELL_PARAPHEUR_MODE');
        $this->parapheurType = Configure::read('PASTELL_PARAPHEUR_TYPE');
        $this->parapheurVisaType = Configure::read('PASTELL_VISA_TYPE');
        $this->proxyContinueDisabled = Configure::read('PASTELL_PROXY_CONTINUE_DISABLED');
        //clés de champs
        $pastell_config = Configure::read('Pastell');
        if (isset($pastell_config[$this->pastellType])) {
            $this->config = $pastell_config[$this->pastellType];
        }
        if ($this->use_pastell===true) {
            $this->client = $this->getInsecurePastellClient();
        }
    }

    /**
     * Initializes AuthComponent for use in the controller.
     *
     * @param Controller $controller A reference to the instantiating controller object
     * @return void
     */
    public function initialize(Controller $controller)
    {
    }

    /**
     * @return Client
     */
    private function getInsecurePastellClient(): Client
    {
        $httpClient = GuzzleClient::createWithConfig([
            'attendre' => $this->proxyContinueDisabled,
            'verify' => false
        ]);
        $client = Client::createWithHttpClient($httpClient);
        $client->setUrl(Configure::read('PASTELL_HOST'));
        $client->authenticate(Configure::read("PASTELL_LOGIN"), Configure::read("PASTELL_PWD"));

        return $client;
    }

    /**
     * Retourne la liste des circuits du PASTELL
     * La liste est enregistrée en session
     * pour économiser du traffic réseau entre WD et pastell lors des appels suivants
     *
     * @param int $idE identifiant de la collectivité
     * @return array
     * @throws ClientExceptionInterface
     */
    public function getCircuits($idE)
    {
        if (!$this->Session->check('user.Pastell.circuits')) {
            $documentsRequester = new PastellDocumentsRequester($this->client);
            $document = $documentsRequester->create($idE, $this->pastellType);
            $document->documentData->data[$this->config['field']['envoi_signature']] = true;
            $circuits = $documentsRequester->getExternalData(
                $document,
                $this->config['field']['iparapheur_sous_type']
            );
            if (!empty($circuits)) {
                $this->Session->write('user.Pastell.circuits', $circuits);
            }
            $documentsRequester->triggerAction($document, $this->config['action']['supression']);
        } else {
            $circuits = $this->Session->read('user.Pastell.circuits');
        }
        return $circuits;
    }

    /**
     * Retourne la liste des circuits du PASTELL
     * La liste est enregistrée en session
     * pour économiser du traffic réseau entre WD et pastell lors des appels suivants
     *
     * @param int $idE identifiant de la collectivité
     * @return array
     * @throws ClientExceptionInterface
     */
    public function getVisaCircuits($idE)
    {
        if ($this->parapheurMode ==='FAST') {
            return null;
        }

        if (!$this->Session->check('user.Pastell.visas')) {
            $documentsRequester = new PastellDocumentsRequester($this->client);
            $document = $documentsRequester->create($idE, $this->parapheurVisaType);
            $document->documentData->data[$this->config['field']['envoi_signature']] = true;
            $visas = $documentsRequester->getExternalData($document, $this->config['field']['iparapheur_sous_type']);

            if (!empty($visas)) {
                $this->Session->write('user.Pastell.visas', $visas);
            }
            $documentsRequester->triggerAction($document, $this->config['action']['supression']);
        } else {
            $visas = $this->Session->read('user.Pastell.visas');
        }
        return $visas;
    }

    /**
     * @param string $page script php
     * @param array $data
     * @param bool $file_transfert attente d'un fichier en retour ?
     * @return bool|mixed|string retour du webservice
     */
    public function execute($page, $data = [], $file_transfert = false)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, $this->login . ":" . $this->pwd);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $api = $this->host . "/api/$page";
        curl_setopt($curl, CURLOPT_URL, $api);
        if ($this->proxyContinueDisabled) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['Expect:']);
        }
        //Hack pastell pour champ multiple en api V1
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                if (is_array($value)) {
                    $data[$key] = json_encode($value);
                }
            }
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }

        if ($file_transfert) {
            $folder = new Folder(AppTools::newTmpDir(
                TMP . 'files' . DS . 'Pastell'
            ), true, 0777);
            $file = new File($folder->path . DS . 'WD_PASTELL_DOC', true, 0777);
            $fp = fopen($file->path, 'w');
            curl_setopt($curl, CURLOPT_FILE, $fp);
        }

        $response = curl_exec($curl);

        if ($response === false) {
            // if you want the headers sent by CURL
            $sentHeaders = curl_getinfo($curl, CURLINFO_HEADER_OUT);
            $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
            curl_close($curl);
            $headers = substr($data, 0, $headerSize);
            $body = substr($data, $headerSize);
            CakeLog::error(curl_error($curl), 'Pastell');
            CakeLog::error("==Sent Headers==\n$sentHeaders\n==End Sent Headers==\n", 'Pastell');
            CakeLog::error("==Response Headers==\n$headers\n==End Response Headers==\n", 'Pastell');
            CakeLog::error("==Response Body==\n$body\n==End Body==", 'Pastell');

            throw new Exception(curl_error($curl));
        }
        curl_close($curl);

        if ($file_transfert) {
            fclose($fp);
            $content = $file->read();
            $folder->delete();
        } else {
            $content = null;
        }

        return $this->parseRetour($page, $data, $response, $content);
    }

    /**
     * Fonction de parse des réponses de pastell
     * @param string $page script ws
     * @param array $data paramètres
     * @param array $retourWS reponse ws
     * @return bool false si en erreur
     */
    private function parseRetour($page, $data, $retourWS, $content)
    {
        //CakeLog::info(var_export($retourWS, true), 'pastell');
        $result = json_decode($retourWS, true);

        // Test si le retour est un fichier ou un message json (erreur)
        //CakeLog::info(print_r(['Request' => $page, 'Data' => $data], true), 'pastell');

        if (!empty($result['message'])) {
            return $result['message'];
            //$this->log('Message : ' . $result['message'], 'pastell');
        }
        if (!empty($result['error-message'])) {
            CakeLog::error('Error : ' . $result['error-message'], 'pastell');
            return false;
        }

        if (!empty($result) && is_array($result)) {
            CakeLog::info('Récupération du json Ok', 'pastell');
            return $result;
        }

        $contentJson = json_decode($content, true);
        if (!empty($contentJson)) {
            CakeLog::error('Error : ' . $contentJson['error-message'], 'pastell');
            return false;
        }

        if (!empty($content)) {
            CakeLog::info('Récupération du json Ok', 'pastell');
            return $content;
        }
        if ($page === 'action.php' && empty($result)) {
            CakeLog::info('Action effectuée', 'pastell');
            return true;
        }
        if (@simplexml_load_string($retourWS) !== false && empty($result)) {
            CakeLog::info('fichier correctement récupéré', 'pastell');
            return $retourWS;
        }

        CakeLog::error(print_r([
          'Request' => $page,
          'Data' => $data ,
          'reponse' => var_export($retourWS, true),
          'reponseData' => $content], true), 'pastell');

        throw new Exception('Aucune réponse du serveur distant', 500);
    }

    /**
     * Permet d'obtenir la version de la plateforme. pastell assure une compatibilité ascendante
     * entre les différents numéro de révision.
     * @return array(
     *      version => Numéro de version commerciale,
     *      revision => Numéro de révision du dépôt de source officiel
     *      de pastell  (https://adullact.net/scm/viewvc.php/?root=pastell),
     *      version_complete => Version affiché sur la l'interface web de la plateforme
     * )
     */
    public function getVersion(): array
    {
        return (new PastellVersion($this->client))->show();
    }


    /**
     * Listes des entités
     *
     * Liste l'ensemble des entités sur lesquelles l'utilisateur a des droits. Liste également les entités filles.
     *
     * @param bool $details si on veut afficher toutes les infos des entités
     * si $details :
     * @return array( array*(
     *  id_e => Identifiant numérique de l'entité
     *  denomination => Libellé de l'entité (ex : Saint-Amand-les-Eaux)
     *  siren => Numéro SIREN de l'entité (si c'est une collectivité ou un centre de gestion)
     *  centre_de_gestion => Identifiant numérique (id_e) du CDG de la collectivité
     *  entite_mere => Identifiant numérique (id_e) de l'entité mère de l'entité (par exemple pour un service)
     * ))
     * sinon
     * @return array( id_e => denomination )
     * @throws ClientExceptionInterface
     */
    public function listEntities(bool $details = false): array
    {
        $entityApi = new EntitesRequester($this->client);
        $listEntity = $entityApi->all();

        $entities = [];
        if ($details) {
            foreach ($listEntity as $keyEntity => $entity) {
                $entities[$keyEntity] = ['id_e' => $entity->getId()] + get_object_vars($entity);
            }
            return $entities;
        }

        foreach ($listEntity as $entity) {
            $entities[$entity->getId()] = $entity->denomination;
        }
        return $entities;
    }

    /**
     * Détail sur un document
     *
     * Récupère l'ensemble des informations sur un document Liste également les entités filles.
     *
     * @param int $idE Identifiant de l'entité (retourné par list-entite)
     * @param int $idD Identifiant unique du document (retourné par list-document)
     * @return array
     * Format de sortie :
     * [] => {
     *  info => Reprend les informations disponible sur list-document.php
     *  data => Données issue du formulaire (voir document-type-info.php
     *  pour savoir ce qu'il est possible de récupérer)
     *  action_possible * => Liste des actions possible (exemple : modification, envoi-tdt, ...)
     * }
     */
    public function detailDocument($idE, $idD)
    {
        return $this->client->get(sprintf('/api/v2/entite/%d/document/%s', $idE, $idD));
    }

    /**
     * @param int $idE identifiant de la collectivité
     * @param string $type type de flux (pastell)
     * @return integer id_d Identifiant unique du document crée.
     * @throws Exception|ClientExceptionInterface Si erreur lors de la création
     */
    public function createDocument($idE, $type = null): string
    {
        if ($type === null) {
            $type = $this->pastellType;
        }

        $documentsRequester = new PastellDocumentsRequester($this->client);
        $document = $documentsRequester->create($idE, $type);
        if (!empty($document->getIdD())) {
            return $document->getIdD();
        }
    }

    /**
     * FIXME : 'error-message' => 'Type action-possible introuvable'
     *
     * Récupération des choix possibles pour un champs spécial du document : external-data
     *
     * Récupère les valeurs possible d'un champs.
     * En effet, certaine valeur sont « externe » a pastell : classification Actes, classification CDG, etc..
     * Ce script permet de récupérer l'ensemble de ces valeurs.
     * Ce script est utilisable sur tous les champs qui dispose d'une propriétés « controler »
     *
     * @param int $idE identifiant de la collectivité
     * @param int $idD identifiant du dossier pastell
     * @param string $field le nom d'un champ du document
     * @return array
     * valeur_possible * => Information supplémentaire sur la valeur possible
     * (éventuellement sous forme de tableau associatif)
     * @throws ClientExceptionInterface
     */
    public function getInfosField($idE, $idD, $field)
    {
        $documentsRequester = new PastellDocumentsRequester($this->client);
        $document = $documentsRequester->show($idE, $idD);

        return $documentsRequester->getExternalData($document, $field);
    }

    /**
     * Modification d'un document
     * @param int $idE identifiant de la collectivité
     * @param int $idD identifiant du dossier pastell
     * @param array $delib
     * @param string $document (Pdf)
     * @param array $annexes
     * @return bool|string
     * @throws ClientExceptionInterface
     * @version 5.0
     * result : ok - si l'enregistrement s'est bien déroulé
     * formulaire_ok : 1 si le formulaire est valide, 0 sinon
     * message : Message complémentaire
     *
     * À noter que pour connaître la liste et les intitulés exacts des champs modifiables,
     * il convient d'utiliser la fonction document-type-info.php, en lui précisant le type concerné.
     * @since 4.2
     */
    public function modifDocument($idE, $idD, $acte, $actePDF, $annexes = [])
    {
        if (empty($acte) || empty($acte['acte_nature_code']) || empty($acte['Deliberation']['objet_delib'])) {
            return;
        }

        $documentName = $acte['Deliberation']['id'];
        if (!empty($acte['Deliberation']['num_delib'])) {
            $documentName =  $acte['Deliberation']['num_delib'];
        }

        if ($this->parapheurMode ==='FAST') {
            $documentName .= time();
        }

        $folder = new Folder(AppTools::newTmpDir(TMP . 'files' . DS . 'pastell' . DS), true, 0777);
        $file = new File($folder->path . DS . $documentName.".pdf", true, 0777);
        $file->write($actePDF);

        $documentsRequester = new PastellDocumentsRequester($this->client);
        $document = $documentsRequester->show($idE, $idD);

        $document->documentData->data[$this->configField('acte_nature')] = $acte['acte_nature_code'];
        $document->documentData->data[$this->configField('classification')] =
            !empty($acte['Deliberation']['num_pref']) ? $this->ProjetTools->getMatiereByKey(
                $acte['Deliberation']['num_pref']
            ) : null;
        $document->documentData->data[$this->configField('document_papier')] =
            !empty($acte['Deliberation']['tdt_document_papier']) ? $acte['Deliberation']['tdt_document_papier'] : null;
        $document->documentData->data[$this->configField('objet')] = AppTools::reformatNameForPastell(
            $acte['Deliberation']['objet_delib']
        );
        $document->documentData->data[$this->configField('numero_de_lacte')] =
            !empty($acte['Deliberation']['num_delib']) ? $acte['Deliberation']['num_delib']
                : $acte['Deliberation']['id'];
        $document->documentData->data[$this->configField('date_de_lacte')] =
            !empty($acte['date_acte']) ? CakeTime::i18nFormat($acte['date_acte'], '%Y-%m-%d') : date("Y-m-d");
        $document = $documentsRequester->update($document);

        $document = $documentsRequester->uploadFile(
            $document,
            'arrete',
            $file->pwd(),
            $file->name
        );

        if (!empty($annexes)) {
            $fileNumber = 0;
            foreach ($annexes as $annex) {
                $annexeFile = new File($folder->path . DS . $annex['filename'], true, 0777);
                $annexeFile->write($annex['content']);
                $document = $documentsRequester->uploadFile(
                    $document,
                    'autre_document_attache',
                    $annexeFile->path,
                    $annex['filename'],
                    $fileNumber++
                );
                $annexeFile->close();
            }
        }

        if (empty($acte['Deliberation']['typologiepiece_code'])) {
            $nature = ClassRegistry::init('Nature');
            $acte['Deliberation']['typologiepiece_code'] =
                $nature->getNatureDefaultCodeByTypeActe($acte['Deliberation']['typeacte_id']);
        }

        $type_pj = [$acte['Deliberation']['typologiepiece_code']];

        if (empty($acte['Deliberation']['annexe_typologiepiece_code'])) {
            $type_pj_annexe = [];
            for ($i = 0, $iMax = count($annexes); $i < $iMax; $i++) {
                $type_pj_annexe[] = $acte['Deliberation']['typologiepiece_code'];
            }
            $acte['Deliberation']['annexe_typologiepiece_code'] = $type_pj_annexe;
        }
        $type_pj = array_merge($type_pj, $acte['Deliberation']['annexe_typologiepiece_code']);


        $document = $documentsRequester->setExternalData(
            $document,
            'type_piece',
            [$this->configField('type_pj') => $type_pj]
        );

        $file->close();
        $folder->delete();
    }

    public function modifDocumentSaeDossierSeance($idE, $idD, File &$zip, File &$xml): void
    {
        $documentsRequester = new PastellDocumentsRequester($this->client);
        $document = $documentsRequester->show($idE, $idD);

        $document = $documentsRequester->uploadFile(
            $document,
            'dossier',
            $zip->pwd(),
            $zip->name
        );
        $zip->close();
        $document = $documentsRequester->uploadFile(
            $document,
            'metadonnees',
            $xml->pwd(),
            $xml->name
        );
        $xml->close();
    }

    /**
     * Execute une action sur un document
     *
     * @param int $idE Identifiant de l'entité (retourné par list-entite)
     * @param int $idD Identifiant unique du document (retourné par list-document)
     * @param string $action Nom de l'action (retourné par detail-document, champs action-possible)
     * @return array
     * [] =>
     *  result : 1 si l'action a été correctement exécute. Sinon, une erreur est envoyé
     *  message : Message complémentaire en cas de réussite
     *
     * @throws ClientExceptionInterface
     */
    public function action($idE, $idD, $action): array
    {
        $documentsRequester = new PastellDocumentsRequester($this->client);
        $document = $documentsRequester->show($idE, $idD);
        $returnAction = $documentsRequester->triggerAction($document, $action);

        return get_object_vars($returnAction);
    }

    /**
     * Supprime un dossier dans pastell
     * Attention: l'exécution de cette commande peut necessiter beaucoup de temps
     * @param int $idE identifiant d'entité (collectivité)
     * @param int $idD identifiant de dossier
     * @return bool|array
     */
    public function delete($idE, $idD)
    {
        return $this->action($idE, $idD, $this->configAction('supression'));
    }

    /**
     * Récupère le contenu d'un fichier
     * @param int $idE Identifiant de l'entité (retourné par list-entite)
     * @param int $idD Identifiant unique du document (retourné par list-document)
     * @param string $field le nom d'un champ du document
     * @param int $num le numéro du fichier, s'il s'agit d'un champ fichier multiple
     * @return string fichier c'est le fichier qui est renvoyé directement
     */
    public function getFile($idE, $idD, $field, $num = 0)
    {
        $documentsRequester = new PastellDocumentsRequester($this->client);
        $document = $documentsRequester->show($idE, $idD);

        return  $documentsRequester->getFile($document, $field, $num);
    }

    /**
     * Récupérer le journal
     * @param int $idE identifiant de la collectivité
     * @param int $idD identifiant du dossier pastell
     * @return bool|array résultat
     */
    public function journal($idE, $idD)
    {
        return $this->client->get(sprintf('/api/v2/journal?id_e=%d&id_d=%s', $idE, $idD));
    }

    /**
     * Déclare à pastell que l'acte doit être envoyé en signature
     * Attention: le document doit être inséré dans un circuit avant !
     * @param int $idE identifiant de la collectivité
     * @param int $idD identifiant du dossier pastell
     * @param string $classification
     * @param bool $value
     * @return bool|array résultat
     */
    public function envoiSignature($idE, $idD, $classification = null, $value = true)
    {
        $documentsRequester = new PastellDocumentsRequester($this->client);
        $document = $documentsRequester->show($idE, $idD);
        $document->documentData->data[$this->config['field']['envoi_signature']] = true;

        if (!empty($classification)) {
            $document->documentData->data[$this->config['field']['classification']] = $classification;
        }
        $document = $documentsRequester->update($document);

        return $document->documentData->data[$this->config['field']['envoi_signature']] === true;
    }

    /**
     * Mise à jour des informations de télétransmission à pastell
     *
     * @param int $idE identifiant de la collectivité
     * @param int $idD identifiant du dossier pastell
     * @param array $acte
     *
     * @return bool|array résultat
     *
     * @version 5.1.1
     */
    public function updateTdt($idE, $idD, $acte)
    {
        $documentsRequester = new PastellDocumentsRequester($this->client);
        $document = $documentsRequester->show($idE, $idD);
        unset($document->documentData->data);
        $document->documentData->data[$this->configField('envoi_tdt')] = true;
        $document->documentData->data[$this->configField('classification')] =
            !empty($acte['Deliberation']['num_pref']) ? $this->ProjetTools->getMatiereByKey(
                $acte['Deliberation']['num_pref']
            ) : null;
        $document->documentData->data[$this->configField('date_de_lacte')] =
            CakeTime::i18nFormat($acte['Deliberation']['date_acte'], '%Y-%m-%d');
        $document->documentData->data[$this->configField('document_papier')] =
            !empty($acte['Deliberation']['tdt_document_papier']) ? $acte['Deliberation']['tdt_document_papier'] : '';

        $document = $documentsRequester->update($document);

        if (!empty($acte['annexe_typologiepiece_code'])) {
            $document = $documentsRequester->setExternalData(
                $document,
                $this->configField('type_piece'),
                [$this->configField('type_pj') => array_merge(
                    [$acte['Deliberation']['typologiepiece_code']],
                    $acte['annexe_typologiepiece_code']
                )]
            );
        }

        return $document->documentData->data;
    }

    /**
     * Mise à jour des informations de télétransmission à pastell
     *
     * @param int $idE identifiant de la collectivité
     * @param int $idD identifiant du dossier pastell
     * @param array $acte
     *
     * @return bool|array résultat
     *
     * @version 5.1.1
     */
    public function updateVisa($entiteId, $documentId, $acte, $document, $annexes = [])
    {
        $success = true;
        $folder = new Folder(AppTools::newTmpDir(TMP . 'files' . DS), true, 0777);
        $file = new File($folder->path . DS . 'projet.pdf', true, 0777);

        $file->write($document);

        $fields = $this->config['field'];
        $data = [
            'id_e' => $entiteId,
            'id_d' => $documentId,
            'envoi_signature' => true,
            'libelle' => $acte['Deliberation']['objet'],
            'document' => curl_file_create($file->pwd(), 'application/pdf', $file->name),
        ];
        //filtre les eléments vide
        $data = array_filter($data);

        if ($this->execute('modif-document.php', $data) == false) {
            $success = false;
        }

        $folder->delete();

        return $success;
    }

    /**
     * Déclare à pastell que l'acte doit être envoyé au sae
     * Attention: le document doit être inséré dans un circuit avant !
     * @param int $idE identifiant de la collectivité
     * @param int $idD identifiant du dossier pastell
     * @return bool|array résultat
     */
    public function envoiSae($idE, $idD)
    {
        $documentsRequester = new PastellDocumentsRequester($this->client);
        $document = $documentsRequester->show($idE, $idD);
        unset($document->documentData->data);
        $document->documentData->data[$this->config['field']['envoi_sae']] = true;

        $document = $documentsRequester->update($document);

        return $document->documentData->data[$this->config['field']['envoi_sae']] === true;
    }

    /**
     * Déclare à pastell que l'acte doit être envoyé à la GED
     * Attention: le document doit être inséré dans un circuit avant !
     * @param int $idE identifiant de la collectivité
     * @param int $idD identifiant du dossier pastell
     * @return bool|array résultat
     */
    public function envoiGed($idE, $idD)
    {
        $documentsRequester = new PastellDocumentsRequester($this->client);
        $document = $documentsRequester->show($idE, $idD);
        unset($document->documentData->data);
        $document->documentData->data[$this->config['field']['envoi_ged']] = true;

        $document = $documentsRequester->update($document);

        return $document->documentData->data[$this->config['field']['envoi_ged']] === true;
    }


    /**
     * Envoi à Pastell l'information sur le circuit du parapheur à emprunter
     * @param int $idE identifiant de la collectivité
     * @param int $idD identifiant du dossier pastell
     * @param string $sous_type
     * @throws ClientExceptionInterface
     */
    public function selectCircuit($idE, $idD, $sous_type)
    {
        $documentsRequester = new PastellDocumentsRequester($this->client);
        $document = $documentsRequester->show($idE, $idD);
        unset($document->documentData->data);
        $document->documentData->data[$this->configField('iparapheur_type')] = $this->parapheurType;
        //selection best parapheur
        $document->documentData->data[$this->configField('envoi_signature_check')] = true;

        if ($this->parapheurMode ==='FAST') {
            $document->documentData->data[$this->configField('fast_parapheur_circuit')] = $sous_type;
        } else {
            $document->documentData->data[$this->configField('iparapheur_sous_type')] = $sous_type;
        }
        $document = $documentsRequester->update($document);
    }


    /**
     * Récupère le tableau de classifications
     * @param int $idE identifiant de la collectivité
     * @param int $idD identifiant du dossier pastell
     * @return array
     *
     * @version 5.1.1
     */
    public function getClassification($idE)
    {
        $moduleAssociationsRequester = new PastellModuleAssociationsRequester($this->client);
        $moduleAssociation = $moduleAssociationsRequester->all($idE, $this->pastellType, 'TdT');
        // Il ne doit y avoir qu'un seul connecteur
        if (count($moduleAssociation) !== 1) {
            throw new Exception('Il ne doit y avoir qu\'un seul connecteur');
        }
        $connectorsRequester = new PastellConnectorsRequester($this->client);
        $connector = $connectorsRequester->show($idE, $moduleAssociation[0]['id_ce']);

        return $connectorsRequester->getFile($connector, 'classification_file')->getContents();
    }

    public function configField($field)
    {
        return $this->config['field'][$field] ?? $field;
    }

    public function configAction($field)
    {
        return $this->config['action'][$field] ?? $field;
    }
}
