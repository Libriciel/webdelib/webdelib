<?php

/**
 * Gestion des filtres dans les vues index
 * Stockage des listes des options des filtres dans la session
 * Passage des critères du filtre au contrôleur par $this->data
 * Utilise la variable de session Filtre qui a la structure suivante :
 * - nom : non du filtre courant
 * - Criteres('nomCritere') : liste des ciritères qui ont la structure suivante :
 *        -'field' : nom du champ à filtrer (ex : User.name)
 *    -'inputOptions' : liste des options du select du formulaire du filtre
 *        -'retourLigne' : booléen qui indique si il faut ajouter un div spacer
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       web-delib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 * @version     v4.3
 * @package     app.Controller.Component
 */
class FiltreComponent extends Component
{
    /**
     * Other components utilized by AuthComponent
     *
     * @var array
     */
    public $components = ['Session'];

    /**
     * Request object
     *
     * @var CakeRequest
     */
    public $request;

    /**
     * Response object
     *
     * @var CakeResponse
     */
    public $response;

    /**
     *
     * @param Controller $controller
     * @param type $settings
     */
    public function initialize(Controller $controller)
    {
        $this->request = $controller->request;
        $this->response = $controller->response;
    }

    /**
     * Called automatically after controller beforeFilter
     * @param Controller $controller A reference to the controller object
     */
    public function startup(Controller $controller)
    {
        $this->defaultOptions = ['url' => $this->purgeRequest($controller->request)];
    }

    /**
     * Initialisation du filtre : création et sauvegarde des valeurs des critères saisis dans la vue
     * @param string $name : nom du filtre
     * @param string $dataFiltre : data du formulaire de saisie des critères du filtre
     * @param array $options tableau des parmètres optionnels :
     *        'url' : array, optionnel, url du formulaire du filtre
     */
    public function initialisation($name, $dataFiltre, $options = [])
    {
        // Initilisations
        $filtreActif = false;
        if (empty($name)) {
            $name = $this->defaultOptions;
        }
        $options = array_merge($this->defaultOptions, $options);
        // Si on a déjà un filtre en session et qu'il est différent alors on supprime l'ancien filtre
        if ($this->Session->check('Filtre') && $this->Session->read('Filtre.nom') != $name) {
            $this->Session->delete('Filtre');
        }
        // Initialisation
        if ($this->Session->check('Filtre')) {
            if (!empty($dataFiltre['Critere'])) {
                // Sauvegarde des valeurs des critères sélectionnés dans la vue
                foreach ($dataFiltre['Critere'] as $nomCritere => $valCritere) {
                    if ($this->Session->read('Filtre.Criteres.' . $nomCritere . '.inputOptions.type') ==
                        'date'
                    ) {
                        // critère de type date
                        $dateVide = ['day' => '', 'month' => '', 'year' => ''];
                        // initialisation de la valeur en session
                        $valSessionSelected = $this->Session->read(
                            'Filtre.Criteres.' . $nomCritere . '.inputOptions.selected'
                        );
                        if (empty($valSessionSelected)) {
                            $this->Session->write(
                                'Filtre.Criteres.' . $nomCritere . '.inputOptions.selected',
                                $dateVide
                            );
                        }
                        // Si la date est incomplete : on garde la valeur précédente en session
                        if ((!empty($valCritere['day']) || !empty($valCritere['month']) || !empty($valCritere['year']))
                            && (empty($valCritere['day']) || empty($valCritere['month']) || empty($valCritere['year']))
                        ) {
                            $this->Session->write('Filtre.Criteres.' . $nomCritere . '.changed', false);
                            $valCritere = $this->Session->read(
                                'Filtre.Criteres.' . $nomCritere . '.inputOptions.selected'
                            );
                        } else {
                            if ($this->Session->read('Filtre.Criteres.' . $nomCritere . '.inputOptions.selected') ===
                                $valCritere) {
                                $this->Session->write('Filtre.Criteres.' . $nomCritere . '.changed', false);
                            } else {
                                $this->Session->write(
                                    'Filtre.Criteres.' . $nomCritere . '.inputOptions.selected',
                                    $valCritere
                                );
                                $this->Session->write('Filtre.Criteres.' . $nomCritere . '.changed', true);
                            }
                        }
                        $filtreActif = $filtreActif || ($valCritere != $dateVide);
                    } else {
                        if ($this->Session->read('Filtre.Criteres.' . $nomCritere . '.inputOptions.type') ==
                            'text') {
                            $this->Session->write('Filtre.Criteres.' . $nomCritere . '.value', $valCritere);
                        }
                        if ($this->Session->read('Filtre.Criteres.' . $nomCritere . '.inputOptions.selected') ===
                            $valCritere) {
                            $this->Session->write('Filtre.Criteres.' . $nomCritere . '.changed', false);
                        } else {
                            $this->Session->write(
                                'Filtre.Criteres.' . $nomCritere . '.inputOptions.selected',
                                $valCritere
                            );
                            $this->Session->write('Filtre.Criteres.' . $nomCritere . '.changed', true);
                        }
                        $filtreActif = $filtreActif || ($valCritere !== '');
                    }
                }
                // Sauvegarde des valeurs du fonctionnement du filtre
                $this->Session->write('Filtre.Fonctionnement.affiche', $dataFiltre['filtreFonc']['affiche']);
                $this->Session->write('Filtre.Fonctionnement.actif', $filtreActif);
            }
        } else {
            $this->Session->write('Filtre.nom', $name);
            $this->Session->write('Filtre.Fonctionnement.affiche', false);
            $this->Session->write('Filtre.Fonctionnement.actif', false);
            $this->Session->write('Filtre.url', $options['url']);
        }
    }

    /**
     * Test l'existence du filtre $name
     * @param string $name : nom du filtre à tester
     * @return bool true si il existe, false dans le cas contraire
     */
    public function exists($name)
    {
        return ($this->Session->check('Filtre') && $this->Session->read('Filtre.nom') == $name);
    }

    /**
     * Test l'existence d'un critère du filtre
     * @param string $name : nom du critere du filtre à tester, si vide test l'existence de la présence de critères
     * @return bool true si il existe, false dans le cas contraire
     */
    public function critereExists($name = null)
    {
        if (empty($name)) {
            return ($this->Session->check('Filtre.Criteres'));
        } else {
            return $this->Session->check('Filtre.Criteres.' . $name);
        }
    }

    /**
     * Ajoute un critere au filtre courant (en session)
     * @param string $nomCritere : nom du critère
     * @param array $params paramètres de la fonction sous forme de tableau avec les entrées suivantes :
     *    - string $field : nom du model.champ a filtrer (ex : User.name)
     *    - array $inputOptions : options du select affiché dans le formulaire
     *    'label' : texte affiché devant le select
     *    'options' : liste des options du select, ....
     *    - booléen $retourLigne indique si il faut ajouter un div spacer
     */
    public function addCritere($nomCritere, $params)
    {
        // Initialisation des valeurs par défaut
        $defaut = ['retourLigne' => false];
        $params = array_merge($defaut, $params);
        // Initialisation des valeurs par défaut des options du select
        $inputOptionsDefaut = [];
        if (array_key_exists('type', $params['inputOptions'])
            && $params['inputOptions']['type'] === 'select'
        ) {
            $inputOptionsDefaut = ['empty' => ''];
        }
        $params['inputOptions'] = array_merge($inputOptionsDefaut, $params['inputOptions']);

        $this->Session->write('Filtre.Criteres.' . $nomCritere, $params);
        $this->Session->write('Filtre.Criteres.' . $nomCritere . '.inputOptions.selected', '');
        $this->Session->write('Filtre.Criteres.' . $nomCritere . '.changed', false);
    }

    /**
     * Supprime un critere au filtre courant (en session)
     * @param string $nomCritere : nom du critère à supprimer
     */
    public function delCritere($nomCritere)
    {
        $this->Session->delete('Filtre.Criteres.' . $nomCritere);
    }

    /**
     * Initialise la valeur sélectionnée du critère
     * @param string $nomCritere : nom du critère
     * @param string $valCritere valeur du critère
     */
    public function setCritere($nomCritere, $valCritere)
    {
        // initialisation
        if (!$this->critereExists($nomCritere)) {
            return;
        }
        // selon le type de critère
        if ($this->Session->check('Filtre.Criteres.' . $nomCritere . '.inputOptions.options')) {
            // select : recherche de l'option correspondante à $valCritere
            $options = $this->Session->read('Filtre.Criteres.' . $nomCritere . '.inputOptions.options');
            if (array_key_exists($valCritere, $options)) {
                $this->Session->write('Filtre.Criteres.' . $nomCritere . '.inputOptions.selected', $valCritere);
                $this->Session->write('Filtre.Fonctionnement.actif', true);
            }
        }
    }

    /**
     * Supprime tous les criteres du filtre (en session)
     */
    public function supprimer()
    {
        $this->Session->delete('Filtre');
    }

    /**
     * Retourne un tableau de conditions en fonction de la valeur des filtres de la vue
     */
    public function conditions()
    {
        $conditions = [];

        if (!$this->Session->check('Filtre.Criteres')) {
            return $conditions;
        }

        $criteres = $this->Session->read('Filtre.Criteres');
        foreach ($criteres as $critere) {
            if (!array_key_exists('selected', $critere['inputOptions'])) {
                continue;
            }
            if (is_array($critere['inputOptions']['selected'])) {
                if (empty($critere['inputOptions']['selected'])) {
                    continue;
                }
            } else {
                if (strlen($critere['inputOptions']['selected']) === 0) {
                    continue;
                }
            }
            //si aucun champ d'une table n est ajouté on ne tient pas conte du champ
            if (!empty($critere['field'])) {
                if (array_key_exists('type', $critere['inputOptions'])
                    && $critere['inputOptions']['type'] === 'date'
                ) {
                    // vérifie qu'il sagit d'une date passé au format yyyy-mm-dd hh:mm:ss avec pas à plusieur espaces
                    if (preg_match(
                        '#([0-9]{4})(-)([0-9]{2})(-)([0-9]{2})(\s*)([0-9]{2})(:)([0-9]{2})(:)([0-9]{2})#',
                        $critere['inputOptions']['selected']
                    )
                    ) {
                        $conditions[$critere['field']] = $critere['inputOptions']['selected'];
                    }
                } elseif (array_key_exists('type', $critere['inputOptions'])
                    && $critere['inputOptions']['type'] === 'text'
                ) {
                    //gestion d'un champ text le paramttre ILIKE est utilisé car
                    // insenssible à la casse le cast est la pour la gestions des id
                    $conditions['CAST(' . $critere['field'] . ' AS TEXT) ILIKE'] = '%'
                        . $critere['inputOptions']['selected'] . '%';
                } else {
                    // select : cas ou la valeur sélectionnée commence par '>|', '>=|', '<|', '<=|'
                    if (!is_array($critere['inputOptions']['selected'])
                        && strpos($critere['inputOptions']['selected'], '|') !== false
                    ) {
                        $tabCritere = explode('|', $critere['inputOptions']['selected']);
                        $conditions[$critere['field'] . ' ' . $tabCritere[0]] = $tabCritere[1];
                    } elseif (is_array($critere['field'])) {
                        if (!empty($critere['inputOptions']['selected'])) {
                            foreach ($critere['field'] as $key => $value) {
                                $conditions['OR'][] = [$value => $critere['inputOptions']['selected']];
                            }
                        }
                    } else {
                        $conditions['AND'][$critere['field']] = $critere['inputOptions']['selected'];
                    }
                }
            }
        }

        return $conditions;
    }

    /**
     * Test la valeur du critère $name a changé
     * @param string $nomCritere : nom du filtre à tester si vide test l'existence de la présence de critères
     * @return bool true si la valeur a changé, false dans le cas contraire ou si le critère n'existe pas
     */
    public function critereChanged($nomCritere)
    {
        if (!$this->critereExists($nomCritere)) {
            return false;
        }
        return $this->Session->read('Filtre.Criteres.' . $nomCritere . '.changed');
    }

    /**
     * Retourne la valeur sélectionnée du critère $name
     * @param string $nomCritere : nom du filtre à tester si vide test l'existence de la présence de critères
     * @return bool valeur sélectionnée
     */
    public function critereSelected($nomCritere)
    {
        if (!$this->critereExists($nomCritere)) {
            return false;
        }
        return $this->Session->read('Filtre.Criteres.' . $nomCritere . '.inputOptions.selected');
    }

    /* Purge du Request pour l'histoique des liens
     * @param Controller $controller A reference to the instantiating controller object
     * @return array tableau de paramêtre de l'action
     */

    protected function purgeRequest(CakeRequest $params)
    {
        if (!($params instanceof CakeRequest)) {
            throw Exception(__('this request is not instance of CakeRequest'), 500);
        }
        $params = $params->params;

        if (empty($params['prefix'])) {
            $prefixes = Configure::read('Routing.prefixes');
            foreach ($prefixes as $value) {
                $params[$value] = false;
            }
        }

        $pass = isset($params['pass']) ? $params['pass'] : [];

        $named = [];
        if (isset($params['named'])) {
            //On retire la pagination du titre du filtre
            unset($params['named']['page']);
            $named = $params['named'];
        }

        //Suppression des clés par default de cakePHP
        unset(
            $params['pass'],
            $params['named'],
            $params['paging'],
            $params['models'],
            $params['url'],
            $params['autoRender'],
            $params['bare'],
            $params['requested'],
            $params['return'],
            $params['_Token']
        );


        return array_merge($params, $pass, $named);
    }
}
