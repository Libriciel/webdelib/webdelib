<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('AppTools', 'Lib');

use Sabre\DAV;

/**
 * [SabreDavComponent description]
 * @package     app.Controller.Component
 * @version     v5.1
 * @since       v4.3.0
 */
class WopiComponent extends Component
{
    public $components = ['Session', 'TokensMethods'];
    private $wopiPath;
    private $wopiClient;

    /**
     * @param type $controller
     * @version 5.1
     */
    public function initialize(Controller $controller)
    {
//        Configure::write('Editor.wopi.client.url', 'http://webdelib.local/loleaflet/dist/loleaflet.html');
//        Configure::write('Editor.wopi.path', 'http://webdelib.local/api/rest/wopi/files/');

        $this->wopiClient = Configure::read('Editor.wopi.client.url');
        $this->wopiPath = Configure::read('Editor.wopi.path');

        $this->url = Configure::read('App.fullBaseUrl');
    }

    public function getUrl($path)
    {
        return $this->wopiPath . $this->constructURL($path);
    }

    public function getClientUrl()
    {
        return $this->wopiClient;
    }

    public function constructURL($path)
    {
        return str_replace('/', '$', $path);
    }

    public function unconstructURL($path)
    {
        return str_replace('$', '/', $path);
    }
}
