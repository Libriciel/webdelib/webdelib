<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('AppTools', 'Lib');

use Sabre\DAV;

/**
 * [SabreDavComponent description]
 * @package     app.Controller.Component
 * @version     v5.1
 * @since       v4.3.0
 */
class SabreDavComponent extends Component // phpcs:ignore
{
    public $components = ['Session','TokensMethods'];
    private $folder;
    private $file;
    private $sDir;
    private $sUrl;
    /**
     * @var Controller|type
     */
    private $controller;

    /**
     * @version 5.1
     * @param type $controller
     */
    public function initialize(Controller $controller)
    {
        parent::initialize($controller);
        $this->controller = $controller;

        $port = Configure::read('Webdav.port');
        $protocole = Configure::read('Webdav.protocole');

        if (!empty($_SERVER['SERVER_NAME'])) {
            $this->sUrl = $protocole . "://" . $_SERVER['SERVER_NAME'] . (!empty($port) ? ':' . $port : '');
        }

        $this->sDir = $this->controller->request->controller
            . (
                !empty($this->controller->request->action) ? DS . $this->controller->request->action
                : ''
            );
    }

    /**
     * Create the folder
     * @version 5.1
     */
    public function start($id = null)
    {
        $path = TMP . 'files' . DS . 'webdav' . DS . $this->controller->getTenantName() . DS . date('Ymd')
        . DS . AuthComponent::user('id') . DS . $this->sDir . (!empty($id) ? DS . $id : '');
        $this->folder = new Folder(
            AppTools::newTmpDir($path),
            true,
            0777
        );
    }

    /**
     *
     */
    public function purge()
    {
        $folderPurge = new Folder(
            TMP . 'files' . DS . 'webdav' . DS
            . $this->controller->getTenantName() . DS . date('Ymd')
            . DS . AuthComponent::user('id')
            . DS . $this->sDir,
            false
        );
        $folderPurge->dirsize();
        $folderPurge->delete();
    }

    /** Initialisation du serveur webdav
     * @version v5.0.1
     * @since v4.3
     */
    public function server()
    {
        $publicDir = new MyDirectory(
            TMP . 'files' . DS . 'webdav'
        );

        $server = new DAV\Server($publicDir);

        $server->setBaseUri('/webdav');

        //Mise en place des locks
        $lockBackend = new DAV\Locks\Backend\File(
            TMP . 'files' . DS . 'webdav'
            . DS . $this->controller->getTenantName() . DS . 'locks'
        );
        $lockPlugin = new DAV\Locks\Plugin($lockBackend);
        $server->addPlugin($lockPlugin);

        $server->exec();
        exit;
    }

    /**
     *
     * @param type $filename
     * @param type $data
     * @param type $path_supp
     * @return type
     */
    public function newFileDav($filename, $data, $path_supp = null)
    {
        $this->file = new File(
            $this->folder->pwd() . (!empty($path_supp) ? DS . $path_supp : '') . DS . $filename,
            true,
            0777
        );
        $this->file->append($data);

        return strstr($this->file->pwd(), 'webdav/');
    }

    /**
     *
     * @return type
     */
    public function newFileDavUrl()
    {
        return $this->sUrl . DS . strstr($this->file->pwd(), 'webdav/');
    }

    /**
     *
     * @param string $dir
     * @return string
     */
    public function fileDavUrl($dir)
    {
        return $this->sUrl . DS . $dir;
    }

    /**
     *
     * @param string $fileDir
     * @return File
     */
    public function getFileDav($fileDir)
    {
        $this->file = new File(TMP . 'files' . DS . $fileDir);
        return $this->file;
    }
}

/**
 *
 */
class MyDirectory extends DAV\Collection // phpcs:ignore
{
    private $myPath;

    /**
     *
     * @param type $myPath
     */
    public function __construct($myPath)
    {
        $this->myPath = $myPath;
    }

    /**
     *
     * @return type
     */
    public function getChildren()
    {
        $children = [];
        // Loop through the directory, and create objects for each node
        foreach (scandir($this->myPath) as $node) {
            // Ignoring files staring with .
            if ($node[0] === '.') {
                continue;
            }
            $children[] = $this->getChild($node);
        }

        return $children;
    }

    /**
     *
     * @param type $name
     * @return \MyFile|\MyDirectory
     * @throws DAV\Exception\NotFound
     */
    public function getChild($name)
    {
        $path = $this->myPath . '/' . $name;

        // We have to throw a NotFound exception if the file didn't exist
        if (!file_exists($path)) {
            throw new DAV\Exception\NotFound('The file with name: ' . $path . ' could not be found');
        }

        // Some added security
        if ($name[0] == '.') {
            throw new DAV\Exception\NotFound('Access denied');
        }
        if (is_dir($path)) {
            return new MyDirectory($path);
        } else {
            return new MyFile($path);
        }
    }

    /**
     *
     * @param type $name
     * @return type
     */
    public function childExists($name)
    {
        return file_exists($this->myPath . '/' . $name);
    }

    /**
     *
     * @return type
     */
    public function getName()
    {
        return basename($this->myPath);
    }

    /**
     *
     * @return string
     */
    public function getLastModified()
    {
        return filemtime($this->myPath);
    }

    /**
     *
     * @param type $name
     * @param type $data
     */
    public function createFile($name, $data = null)
    {
        $file = new File($this->myPath . DS . $name, true, 0777);
        $file->append(stream_get_contents($data));
        $file->close();

        //throw new DAV\Exception\Forbidden('à implémenter');
    }

    /**
     *
     * @param type $name
     */
    public function createDirectory($name)
    {
        $folder = new Folder($this->myPath . DS . $name, true);

        //throw new DAV\Exception\Forbidden($this->myPath.$name);
    }
}

/**
 *
 */
class MyFile extends DAV\File // phpcs:ignore
{
    private $myPath;

    /**
     *
     * @param type $myPath
     */
    public function __construct($myPath)
    {
        $this->myPath = $myPath;
    }

    /**
     *
     * @return type
     */
    public function getName()
    {
        return basename($this->myPath);
    }

    /**
     *
     * @return type
     */
    public function get()
    {
        return fopen($this->myPath, 'rb');
    }

    /**
     *
     * @param type $data
     */
    public function put($data)
    {
        $file = new File($this->myPath, true, 0777);
        $file->write(stream_get_contents($data));
        $file->close();
    }

    /**
     *
     * @return type
     */
    public function getSize()
    {
        return filesize($this->myPath);
    }

    /**
     *
     * @return type
     */
    public function getETag()
    {
        return '"' . md5_file($this->myPath) . '"';
    }

    /**
     * Returns the mime-type for a file
     *
     * If null is returned, we'll assume application/octet-stream
     *
     * @return mixed
     */
    public function getContentType()
    {
        return 'application/vnd.oasis.opendocument.text';
    }
}
