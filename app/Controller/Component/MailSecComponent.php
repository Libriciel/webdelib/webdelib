<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Component', 'Controller');
App::uses('ConnectorS2low', 'Lib');
App::uses('CakeEmail', 'Network/Email');

/**
 * [MailsecComponent description]
 *
 * @version 5.2.0
 */
class MailSecComponent extends Component
{
    /**
     * [private description]
     * @var [type]
     */
    protected $useMailSec;
    /**
     * [private description]
     * @var [type]
     */
    protected $connecteurType;


    public $components = ['S2low', 'Pastell'];


    /**
     * [__construct description]
     */
    public function __construct()
    {
        $this->useMailSec = Configure::read('USE_MAILSEC');
        $this->connecteurType = Configure::read('MAILSEC');
    }

    /**
     * [sendMail description]
     * @param  [type] $data [description]
     * @return [type]       [description]
     *
     * @version 5.2.0
     */
    public function sendMail(CakeEmail $Email)
    {
        if ($this->useMailSec) {
            if ($this->connecteurType =='PASTELL') {
                return $this->sendMailPastell($Email);
            } else {
                return $this->sendMailS2low($Email);
            }
        }

        return $this->sendMailSimple($Email);
    }

    /**
     * [sendMailSimple description]
     * @param  CakeEmail $Email [description]
     * @return [type]           [description]
     */
    public function sendMailSimple(CakeEmail $email)
    {
        // On met en copie cachée les emails supplémentaires
        if (count($email->to())>1) {
            $emailTo = $email->to();
            $email->to(array_shift($emailTo));
            $email->bcc($emailTo);
        }

        return $email->send();
    }

    public function sendMailS2low(CakeEmail $Email)
    {
        $Email->config([
            'host' => Configure::read('S2LOW_HOST'),
            'username' => Configure::read('S2LOW_LOGIN'),
            'password' => Configure::read('S2LOW_PWD'),
            'mail_password' => Configure::read('S2LOW_MAILSEC_PWD_USE') ? Configure::read('S2LOW_MAILSEC_PWD') : null,
            'proxy_host' => Configure::read('S2LOW_USEPROXY') ? Configure::read('S2LOW_PROXYHOST') : null,
            'transport' => 'S2low',
            'tls' => true,
            'ssl_cert' => Configure::read('S2LOW_PEM'),
            'ssl_certpassword' =>  Configure::read('S2LOW_CERTPWD'),
            'ssl_key' => Configure::read('S2LOW_SSLKEY')
          ]);

        return $Email->send();
    }


    /**
     * @param int $id_e identifiant de la collectivité
     * @param string $type type de flux (pastell)
     * @return integer id_d Identifiant unique du document crée.
     * @throws Exception Si erreur lors de la création
     */
    public function sendMailPastell(CakeEmail $Email)
    {
        $Email->config([
            'host' => Configure::read('PASTELL_MAILSEC_HOST'),
            'username' => Configure::read('PASTELL_MAILSEC_LOGIN'),
            'password' => Configure::read('PASTELL_MAILSEC_PWD'),
            'pastell_ide' => Configure::read('PASTELL_MAILSEC_IDE'),
            'pastell_flux' => Configure::read('PASTELL_MAILSEC_FLUX'),
            'ssl_allow_self_signed' => Configure::read('PASTELL_MAILSEC_SSL_ALLOW_SELF_SIGNED'),
            'transport' => 'Pastell'
          ]);

        return $Email->send();
    }
}
