<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('AppTools', 'Lib');
App::uses('Deliberation', 'Model');
App::uses('Typeseance', 'Model');
App::uses('Seance', 'Model');
App::uses('Nature', 'Model');

require_once APP . DS . 'Vendor' . DS . 'cmis_repository_wrapper.php';
/**
 * Component ExportGed
 *
 * @package app.Controller.Component
 * @version 5.1.0
 */
class ExportGedCMISComponent extends Component
{
    /** @var Object Contains a CMISService */
    public $client;

    /** @var Object Contains a folder CMISService */
    public $folder;

    /** @var Object Contains a folder temp for send */
    public $folderSend;

    /** @var Array Contains a components */
    public $components = ['Conversion', 'Progress', 'ProjetTools', 'PDFStamp'];

    /** @var Array Contains a pluralize terms */
    private $pluralizes = [
        'TexteActe' => 'TextesActes',
        'Annexe' => 'Annexes',
        'Rapport' => 'Rapports',
        'Signature' => 'Signatures',
        'BordereauProjet' => 'BordereauxProjet',
        'BordereauTdt' => 'BordereauxTdt',
        'BordereauParapheur' => 'BordereauxParapheur',
        'ArActe' => 'ArActes',
        'AnnexeTampon' => 'AnnexesTampons',
        'ActeTampon' => 'ActesTampons',
        'CourrierSimple' => 'CourriersSimple',
        'LettreObservation' => 'LettresObservation',
        'PieceComplementaire' => 'PiecesComplementaire',
        'DefereTribunalAdministratif' => 'DeferesTribunalAdministratif',
        'Annulation' => 'Annulations',
        'ActePublication' => 'ActesPublication',
        'AnnexePublication' => 'AnnexesPublication'
    ];

    public function initialize(Controller $controller)
    {
        $this->Deliberation = ClassRegistry::init('Deliberation');
        $this->Typeseance = ClassRegistry::init('Typeseance');
        $this->Seance = ClassRegistry::init('Seance');
        $this->Nature = ClassRegistry::init('Nature');
        $this->Typologiepiece = ClassRegistry::init('Typologiepiece');

        parent::initialize($controller);
    }

    /**
     * @param Folder $folderSend
     * @version 5.1.0
     */
    public function service(&$folderSend)
    {
        $this->folderSend = &$folderSend;
        if (Configure::read('USE_GED')) {
            $this->client = $this->getClient();
            $this->folder = $this->getClient()->getObjectByPath(Configure::read('CMIS_REPO'));
        }
    }

    /**
     * Create element for Ged
     * @param DOMDocument $dom
     * @param string $tag_name
     * @param string $value
     * @param array() $attributes
     * @return DOMElement $element
     * @throws Exception
     * @version 5.1.0
     */
    public function createElement($dom, $tag_name, $value = null, $attributes = null)
    {
        try {
            $element = ($value != null) ? $dom->createElement($tag_name, AppTools::xmlEntityEncode($value))
                : $dom->createElement($tag_name);

            if ($attributes != null) {
                foreach ($attributes as $attr => $val) {
                    $element->setAttribute($attr, $val);
                }
            }
        } catch (Exception $e) {
            CakeLog::error(
                __(
                    '[Ged][%s]Create error(%s) tag name: %s',
                    __METHOD__,
                    $e->getCode(),
                    $tag_name
                ) . "\n" . $e->getMessage(),
                'connector'
            );
            throw $e;
        }
        return $element;
    }

    /**
     *
     * @param type $zip
     * @param type $dom
     * @param type $domObj
     * @param type $id
     * @param type $model
     * @version 5.1.0
     */
    public function createElementInfosupsByZip(&$zip, &$dom, &$domObj, $id, $model)
    {
        App::uses('Infosup', 'Model');
        $this->Infosup = ClassRegistry::init('Infosup');

        $aInfosup = $this->Infosup->export($id, $model);
        if (isset($aInfosup) && !empty($aInfosup)) {
            $infosup = $this->createElement($dom, 'infosup' . $model, null, ['type' => 'infosup']);

            foreach ($aInfosup as $code => $value) {
                switch ($value['type']) {
                    case 'string':
                        $infosup->appendChild($this->createElement($dom, $code, $value['content'], null));
                        break;

                    case 'odtFile':
                    case 'geojson':
                    case 'file':
                        //Gestion des fichiers odt
                        if ($value['type'] == 'odtFile') {
                            $file_data = $this->Conversion->convertirFlux($value['content'], 'odt', 'pdf');
                            $file_rel_name = $value['id'] . '.pdf';
                            $file_name = basename($value['file_name'], '.odt') . '.pdf';
                            $mimetype = 'application/pdf';
                        } else {
                            $file_data = $value['content'];
                            $file_rel_name = $value['id'] . strrchr($value['file_name'], '.');
                            $mimetype = $value['file_type'];
                            $file_name = $value['file_name'];
                        }
                        $document = $this->createElement(
                            $dom,
                            'document',
                            null,
                            ['nom' => $file_name, 'relName' => 'infosup' . $model . DS . $file_rel_name]
                        );
                        $document->appendChild($this->createElement($dom, 'mimetype', $mimetype));
                        $document->appendChild($this->createElement($dom, 'encoding', 'utf-8'));
                        ${$code} = $this->createElement($dom, $code, null, null);
                        //Gestion des fichiers vide
                        if (!empty($value['content'])) {
                            ${$code}->appendChild($document);
                            $zip->addFromString('infosup' . $model . DS . $file_name, $file_data);
                        }
                        $infosup->appendChild(${$code});
                        break;

                    default:
                        break;
                }
            }
            $domObj->appendChild($infosup);
        }
    }

    /**
     * Création des éléments d'informations supplémentaires
     * @param type $dom
     * @param type $domObj
     * @param type $id
     * @param type $model
     * @version 5.1.0
     */
    public function createElementInfosups(&$dom, &$domObj, $id, $model)
    {
        App::uses('Infosup', 'Model');
        $this->Infosup = ClassRegistry::init('Infosup');

        $aInfosup = $this->Infosup->export($id, $model);
        if (!empty($aInfosup)) {
            $infosup = $this->createElement($dom, 'infosup' . $model, null, ['type' => 'infosup']);

            foreach ($aInfosup as $code => $value) {
                switch ($value['type']) {
                    case 'string':
                        $infosup->appendChild($this->createElement($dom, $code, $value['content'], null));
                        break;
                    case 'odtFile':
                    case 'geojson':
                    case 'file':
                        //Gestion des fichiers odt
                        if ($value['type'] == 'odtFile') {
                            $file_data = $this->Conversion->convertirFlux($value['content'], 'odt', 'pdf');
                            $file_rel_name = $value['id'] . '.pdf';
                            $file_name = basename($value['file_name'], '.odt') . '.pdf';
                            $mimetype = 'application/pdf';
                        } else {
                            $file_data = $value['content'];
                            $file_rel_name = $value['id'] . strrchr($value['file_name'], '.');
                            $mimetype = $value['file_type'];
                            $file_name = $value['file_name'];// FIX suite sonarqube (a verifier)
                        }
                        //Ajout du fichier
                        $relativePath = './' . $this->inflectorNameForFolder('Infosup') . $model;
                        $absolutePath = $this->folderSend->pwd() . DS . $relativePath;
                        $document = $this->createElement(
                            $dom,
                            'document',
                            null,
                            [
                                'fileName' => $file_name,
                                'relativePath' => $relativePath . DS . $file_rel_name
                            ]
                        );
                        $document->appendChild($this->createElement($dom, 'mimetype', $mimetype));
                        $document->appendChild($this->createElement($dom, 'encoding', 'utf-8'));
                        ${$code} = $this->createElement($dom, $code, null, null);
                        //Gestion des fichiers vide
                        if (!empty($value['content'])) {
                            ${$code}->appendChild($document);

                            $folder = new Folder();
                            if (!$folder->cd($absolutePath)) {
                                CakeLog::info(
                                    __('[Ged][%s]Acte: create folder %s', __METHOD__, $relativePath),
                                    'connector'
                                );
                                if (!$folder->create($absolutePath, 0777)) {
                                    $message = __(
                                        '[Ged][%s]Acte: error create folder %s',
                                        __METHOD__,
                                        $relativePath
                                    );
                                    CakeLog::error($message, 'connector');
                                    throw new Exception($message, '500');
                                }
                                $folder->cd($absolutePath);
                            }
                            $file = new File($folder->pwd() . DS . $file_name);
                            CakeLog::info(
                                __('[Ged][%s]Acte: add file %s', __METHOD__, $folder->pwd() . DS . $file_name),
                                'connector'
                            );
                            $file->write($file_data);
                            $file->close();
                        }
                        $infosup->appendChild(${$code});
                        break;

                    default:
                        break;
                }
            }
            $domObj->appendChild($infosup);
        }
    }

    /**
     * Suppression du répertoire de dépot en Ged
     * @param string $dir
     * @version 5.1.0
     */
    public function deletetoGed($dir)
    {
        // Règle de gestion on écrase les documents existants
        try {
            CakeLog::info(
                __('[Ged][%s]Delete : %s', __METHOD__, $dir),
                'connector'
            );
            //On recherche le dossier
            CakeLog::info(
                __('[Ged][%s]Execute getObjectByPath : %s', __METHOD__, $dir),
                'connector'
            );
            $objet_cmis = $this->client->getObjectByPath($dir);

            if (is_object($objet_cmis)) {
                //On recherche tous les enfants du dossier
                CakeLog::info(
                    __(
                        '[Ged][%s]Execute getChildren : %s',
                        __METHOD__,
                        $objet_cmis->properties['cmis:path']
                    ),
                    'connector'
                )
                ;
                $children = $this->client->getChildren($objet_cmis->id);
                CakeLog::info(
                    __('[Ged][%s]Children count: %s', __METHOD__, count($children->objectList)),
                    'connector'
                );

                //On boucle sur les enfants
                foreach ($children->objectList as $child) {
                    if (!empty($child->properties['cmis:path'])) {
                        $other_children = $this->client->getChildren($child->id);
                        CakeLog::info(
                            __(
                                '[Ged][%s]Execute getOtherChildren : %s',
                                __METHOD__,
                                $child->properties['cmis:path']
                            ),
                            'connector'
                        );
                        CakeLog::info(
                            __(
                                '[Ged][%s]OtherChildren count: %s',
                                __METHOD__,
                                count($other_children->objectList)
                            ),
                            'connector'
                        );

                        foreach ($other_children->objectList as $other_child) {
                            //On supprimer l'enfant selectionné
                            CakeLog::info(
                                __('[Ged][%s]Execute deleteObject: %s', __METHOD__, $other_child->id),
                                'connector'
                            );
                            $this->client->deleteObject($other_child->id);
                        }
                    }
                    //On supprimer l'enfant selectionné
                    CakeLog::info(
                        __('[Ged][%s]Execute deleteObject: %s', __METHOD__, $child->id),
                        'connector'
                    );
                    $this->client->deleteObject($child->id);
                }
                //On peut maitenant supprimer le dossier
                CakeLog::info(
                    __(
                        '[Ged][%s]Execute deleteObject: %s',
                        __METHOD__,
                        $objet_cmis->properties['cmis:path']
                    ),
                    'connector'
                );
                $this->client->deleteObject($objet_cmis->id);
            }
        } catch (CmisObjectNotFoundException $e) {
            // L'objet n'existe pas encore : ne rien faire
        } catch (CmisPermissionDeniedException $e) {
            // L'objet n'existe pas encore : ne rien faire
        } catch (CmisRuntimeException $e) {
            // echo $e->getMessage();exit
            // L'objet n'existe pas encore : ne rien faire
        }
    }

    /**
     * [getTdtMessageForGed description]
     *
     * les types de document (messages) :
     *    2 : Courrier simple;
     *    3 : Demande de pièces complémentaires;
     *    4 : Lettre d'observation;
     *    5 : Déféré au tribunal administratif;
     *    6 : Annulation.
     *
     * les etats (Gère les documents réponse de la collectivité) :
     *   -1 : Erreur;
     *    0 : Annulé;
     *    1 : Posté;
     *    2 : En attente de transmission;
     *    3 : Transmis;
     *    4 : Acquittement reçu;
     *    5 : Validé;
     *    6 : Refusé.
     *
     * les etats (Demande de la préfecture):
     *    7 : Document reçu;
     *    8 : Acquittement envoyé;
     *    21 : Document reçu (pas d'AR).
     *
     *
     * @param  [type] $acte_id [description]
     * @return [type]          [description]
     * @throws Exception
     * @version 5.1.0
     */
    public function getTdtMessageForGed($acte_id)
    {
        try {
            App::uses('TdtMessage', 'Model');
            $this->TdtMessage = ClassRegistry::init('TdtMessage');
            //FIX
            $messages = $this->TdtMessage->find(
                'all',
                [
                    'fields' => ['id', 'tdt_id', 'tdt_type', 'tdt_etat', 'tdt_data'],
                    'conditions' => [
                        'TdtMessage.delib_id' => $acte_id,
                        'TdtMessage.parent_id is null'
                    ],
                    'contain' => [
                        'Reponse' => ['fields' => ['id', 'tdt_id', 'tdt_type', 'tdt_etat', 'tdt_data']]],
                    'recursive' => -1,
                ]
            );
            $return = [];
            foreach ($messages as $message) {
                switch ($message['TdtMessage']['tdt_type']) {
                    case 2:
                        $type = 'courrierSimple';
                        $name = 'courriersimple.pdf';
                        break;
                    case 3:
                        $type = 'pieceComplementaire';
                        $name = 'piececomplementaire.pdf';
                        $name_reponse = 'reponsepiececomplementaire.pdf';
                        break;
                    case 4:
                        $type = 'lettreObservation';
                        $name = 'lettreobservation.pdf';
                        $name_reponse = 'reponselettreobservation.pdf';
                        break;
                    case 5:
                        $type = 'defereTribunalAdministratif';
                        $name = 'defereTribunalAdministratif.pdf';
                        break;
                    case 6:
                        $type = 'annulation';
                        $name = 'annulation.pdf';
                        break;
                    default:
                        continue 2;
                }
                $reponses = [];
                if (!empty($message['Reponse'])) {
                    foreach ($message['Reponse'] as $reponse) {
                        if (empty($reponse['tdt_data'])) {
                            throw new Exception(
                                'Le message est indiponible. (num message id=' . $reponse['id'] . ')'
                            );
                        }
                        $tdt_data = $this->TdtMessage->recupMessagePdfFromTar($reponse['tdt_data']);
                        $reponses[] = [
                            'name' => $name_reponse,
                            'type' => $type,
                            'relName' => $tdt_data['filename'],
                            'content_pdf' => $tdt_data['content']
                        ];
                    }
                }

                if (empty($message['TdtMessage']['tdt_data'])) {
                    throw new Exception(
                        'Le message est indiponible. (num message id=' . $message['TdtMessage']['id'] . ')'
                    );
                }
                $tdt_data = $this->TdtMessage->recupMessagePdfFromTar($message['TdtMessage']['tdt_data']);
                $return[] = [
                    'name' => $name,
                    'type' => $type,
                    'relName' => $tdt_data['filename'],
                    'content_pdf' => $tdt_data['content'],
                    'reponses' => $reponses
                ];
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $return;
    }

    /**
     *  Modification pour le nom de dossier
     *
     * @param type $name
     * @return type
     */
    public function inflectorNameForFolder($name)
    {
        return Inflector::slug($name);
    }

    /**
     *  Modification du nom de dossier avec la première Lettre en majuscule et mise en place du pluriel
     *
     * @param type $name
     * @return type
     */
    public function inflectorPluralizeForFolder($name)
    {
        $name = ucfirst($name);
        if (array_key_exists($name, $this->pluralizes)) {
            return $this->pluralizes[$name];
        }
        return $name;
    }

    /**
     * Noeud document[pv_complet]
     * @param type $seance_id
     * @return type
     */
    public function createElementPvComplet(&$dom, &$dom_seance, &$seance)
    {
        CakeLog::info(__('[Ged][%s]Create file pv complet', __METHOD__), 'connector');
        $document = $this->createElement(
            $dom,
            'document',
            null,
            [
                'type' => 'pv_complet',
                'relativePath' => './',
                'fileName' => 'Pv_complet.pdf'
            ]
        );
        $document->appendChild($this->createElement($dom, 'mimetype', 'application/pdf'));
        $document->appendChild($this->createElement($dom, 'encoding', 'utf-8'));
        $dom_seance->appendChild($document);

        $file_pv = new File($this->folderSend->pwd() . DS . 'Pv_complet.pdf');
        $file_pv->write($seance['Seance']['pv_complet']);
        $file_pv->close();
    }

    /**
     * Noeud document[pv_complet]
     * @param type $seance_id
     * @return type
     */
    public function createElementPvSommaire(&$dom, &$dom_seance, &$seance)
    {
        CakeLog::info(__('[Ged][%s]Create file pv sommaire', __METHOD__), 'connector');
        //Noeud document[pv_sommaire]
        $document = $this->createElement(
            $dom,
            'document',
            null,
            [
                'type' => 'pv_sommaire',
                'relativePath' => './',
                'fileName' => 'Pv.pdf'

            ]
        );
        $document->appendChild($this->createElement($dom, 'mimetype', 'application/pdf'));
        $document->appendChild($this->createElement($dom, 'encoding', 'utf-8'));
        $dom_seance->appendChild($document);
        //Ajout au zip
        $file_pv = new File($this->folderSend->pwd() . DS . 'Pv.pdf');
        $file_pv->write($seance['Seance']['pv_sommaire']);
        $file_pv->close();
    }

    /**
     * Noeud document[pv_complet]
     * @param type $seance_id
     * @return type
     */
    public function createElementOdj(&$dom, &$dom_seance, &$seance)
    {
        CakeLog::info(__('[Ged][%s]Create file odj', __METHOD__), 'connector');
        //Noeud document[odj]
        $document = $this->createElement(
            $dom,
            'document',
            null,
            [
                'type' => 'odj',
                'relativePath' => './',
                'fileName' => 'Odj.pdf'
            ]
        );
        $document->appendChild($this->createElement($dom, 'mimetype', 'application/pdf'));
        $document->appendChild($this->createElement($dom, 'encoding', 'utf-8'));
        $dom_seance->appendChild($document);

        // fusion du document de ordre du jour
        $file_ordredujour = new File($this->folderSend->pwd() . DS . 'Odj.pdf');
        $file_ordredujour->write($this->fileFusionSeance($seance['Seance']['id'], 'ordredujour'));
        $file_ordredujour->close();
    }

    /**
     * Noeud document[pv_complet]
     * @param type $seance_id
     * @return type
     */
    public function createElementConvocation(&$dom, &$dom_seance, &$seance)
    {
        CakeLog::info(__('[Ged][%s]Create file convocation', __METHOD__), 'connector');
        //Noeud document[convocation]
        $document = $this->createElement(
            $dom,
            'document',
            null,
            [
                'type' => 'convocation',
                'relativePath' => './',
                'fileName' => 'Convocation.pdf'
            ]
        );
        $document->appendChild($this->createElement($dom, 'mimetype', 'application/pdf'));
        $document->appendChild($this->createElement($dom, 'encoding', 'utf-8'));
        $dom_seance->appendChild($document);

        $file_convocation = new File($this->folderSend->pwd() . DS . 'Convocation.pdf');
        $file_convocation->write($this->fileFusionSeance($seance['Seance']['id'], 'convocation'));
        $file_convocation->close();
    }

    /**
     * Récupération des informations de l'acte
     * @param type $id
     * @return type
     */
    private function getActe($id)
    {
        $acte = $this->Deliberation->find(
            'first',
            [
                'fields' => [
                    'Deliberation.id', 'Deliberation.num_delib', 'Deliberation.objet_delib', 'Deliberation.titre',
                    'Deliberation.num_pref', 'typologiepiece_code', 'tdt_document_papier',
                    'Deliberation.deliberation', 'Deliberation.deliberation_size',
                    'Deliberation.delib_pdf', 'Deliberation.signature', 'signature_type',
                    'Deliberation.parapheur_etat', 'Deliberation.signee', 'Deliberation.parapheur_bordereau',
                    'Deliberation.tdt_data_pdf', 'Deliberation.tdt_data_bordereau_pdf',
                    'Deliberation.tdt_ar_date', 'Deliberation.tdt_id', 'Deliberation.tdt_ar',
                    'Deliberation.date_acte', 'Deliberation.publier_etat',
                    'Deliberation.publier_date','Deliberation.tdt_ar',
                ],
                'contain' => [
                    'Service' => ['fields' => ['name']],
                    'Theme' => ['fields' => ['libelle']],
                    'Typeacte' => [
                        'fields' => ['name', 'modele_projet_id', 'modele_bordereau_projet_id', 'nature_id'],
                        'Nature' => ['fields' => ['name']]
                    ],
                    'Redacteur' => ['fields' => ['nom', 'prenom']],
                    'Rapporteur' => ['fields' => ['nom', 'prenom']],
                ],
                'conditions' => ['Deliberation.id' => $id],
                'recursive' => -1
            ]
        );

        $nature = $this->Nature->find('first', [
            'fields' => ['name'],
            'conditions' => [
                'Nature.id' => $acte['Typeacte']['nature_id']]
        ]);

        return $acte + $nature;
    }

    public function getActeCmis(&$dom, &$dom_depot, $acte_id, &$seance = null)
    {
        try {
            CakeLog::info(
                __('[Ged][%s]Acte(%s): create elements for DOMDocument', __METHOD__, $acte_id),
                'connector'
            );

            $attributes = ['idActe' => $acte_id];
            if (!empty($seance['Seance']['id'])) {
                $attributes = array_merge($attributes, ['refSeance' => $seance['Seance']['id']]);
            }

            $doc = $this->createElement($dom, 'delib', null, $attributes);

            CakeLog::info(__('[Ged][%s]Acte(%s): search informations', __METHOD__, $acte_id), 'connector');

            $acte = $this->getActe($acte_id);

            if (!empty($seance['Seance']['id'])) {
                $doc->appendChild($this->createElement(
                    $dom,
                    'position',
                    $this->Deliberation->getPosition($acte_id, $seance['Seance']['id'])
                ));
            }
            $doc->appendChild($this->createElement(
                $dom,
                'libelle',
                $acte['Deliberation']['objet_delib']
            ));
            $doc->appendChild($this->createElement(
                $dom,
                'titre',
                $acte['Deliberation']['titre']
            ));
            $doc->appendChild($this->createElement(
                $dom,
                'nature',
                !empty($acte['Nature']['name']) ? $acte['Nature']['name'] : ''
            ));
            $doc->appendChild($this->createElement(
                $dom,
                'typeActeLibelle',
                $acte['Typeacte']['name']
            ));
            $doc->appendChild($this->createElement(
                $dom,
                'classification',
                $acte['Deliberation']['num_pref']
            ));
            $doc->appendChild($this->createElement(
                $dom,
                'classificationLibelle',
                $this->ProjetTools->getMatiereByKey($acte['Deliberation']['num_pref'])
            ));
            $doc->appendChild($this->createElement(
                $dom,
                'transmisPrefecturePapier',
                $acte['Deliberation']['tdt_document_papier']
            ));
            $doc->appendChild($this->createElement(
                $dom,
                'typologiePieceCode',
                $acte['Deliberation']['typologiepiece_code']
            ));
            $doc->appendChild($this->createElement(
                $dom,
                'typologiePieceCodeLibelle',
                $this->Typologiepiece->getTypologieNamebyCode($acte['Deliberation']['typologiepiece_code'])
            ));
            $doc->appendChild($this->createElement(
                $dom,
                'date',
                $acte['Deliberation']['date_acte']
            ));
            $doc->appendChild($this->createElement(
                $dom,
                'numero',
                $acte['Deliberation']['num_delib']
            ));
            $doc->appendChild($this->createElement(
                $dom,
                'theme',
                $acte['Theme']['libelle']
            ));
            $doc->appendChild($this->createElement(
                $dom,
                'emetteur',
                $acte['Service']['name']
            ));
            $doc->appendChild($this->createElement(
                $dom,
                'redacteur',
                !empty($acte['Redacteur']) ? __('%s %s', $acte['Redacteur']['prenom'], $acte['Redacteur']['nom']) : null
            ));
            $doc->appendChild($this->createElement(
                $dom,
                'rapporteur',
                !empty($acte['Rapporteur']) ? __(
                    '%s %s',
                    $acte['Rapporteur']['prenom'],
                    $acte['Rapporteur']['nom']
                ) : null
            ));
            $doc->appendChild($this->createElement(
                $dom,
                'dateAr',
                CakeTime::i18nFormat($acte['Deliberation']['tdt_ar_date'], '%A %d %B %Y')
            ));
            $doc->appendChild($this->createElement(
                $dom,
                'datePublication',
                CakeTime::i18nFormat($acte['Deliberation']['publier_date'], '%A %d %B %Y')
            ));
            //Création du noeud des commissions
            $this->createElementCommissionsForActe($dom, $doc, $acte);

            //Création du noeud des informations supplémentaires du projet
            CakeLog::info(
                __('[Ged][%s]Acte(%s): create element "Infosups"', __METHOD__, $acte_id),
                'connector'
            );
            $this->createElementInfosups($dom, $doc, $acte_id, 'Deliberation');

            $i = 1;
            $aDocuments = [];
            $children = [];

            //Noeud document[TexteActe]
            CakeLog::info(
                __('[Ged][%s]Acte(%s): create element "TexteActe"', __METHOD__, $acte_id),
                'connector'
            );
            $aDocuments['TexteActe'] = $i++;
            $filename = 'TexteActe' . '_' . $acte_id . '.pdf';

            if (!empty($acte['Deliberation']['delib_pdf'])) {
                $texteActe = $acte['Deliberation']['delib_pdf'];
                if (!empty($acte['Deliberation']['signature'])) {
                    $children = [
                        'tag_name' => 'signature',
                        'value' => 'true',
                        'attributes' => [
                            'formatSignature' => $acte['Deliberation']['signature'] === 'PAdES' ? 'PAdES' : 'p7s'
                        ]
                    ];
                }
            } else {
                $texteActe = $this->fileFusionProjet(
                    $acte['Deliberation']['id'],
                    'deliberation',
                    !empty($seance['Typeseance']['modele_projet_id']) ? $seance['Typeseance']['modele_projet_id'] : null
                );
            }
            $this->createElementFile(
                $dom,
                $doc,
                'texteActe',
                $aDocuments['TexteActe'],
                null,
                $filename,
                'application/pdf',
                $texteActe,
                null,
                $children
            );
            unset($texteActe);

            //Récupération de l'acte tamponné
            if (!empty($acte['Deliberation']['tdt_data_pdf'])) {
                CakeLog::info(__('[Ged][%s]Acte(%s): create element "ActeTampon"', __METHOD__, $acte_id), 'connector');
                $aDocuments['ActeTampon'] = $i++;
                $filename = 'ActeTampon' . '_' . $acte_id . '.pdf';
                $this->createElementFile(
                    $dom,
                    $doc,
                    'acteTampon',
                    $aDocuments['ActeTampon'],
                    $aDocuments['TexteActe'],
                    $filename,
                    'application/pdf',
                    $acte['Deliberation']['tdt_data_pdf']
                );
            }

            //Récupération de l'acte tamponné
            if ($acte['Deliberation']['publier_etat']  > 0) {
                CakeLog::info(
                    __('[Ged][%s]Acte(%s): create element "ActePublication"', __METHOD__, $acte_id),
                    'connector'
                );
                $aDocuments['ActePublication'] = $i++;
                $filename = 'ActePublication' . '_' . $acte_id . '.pdf';
                $actePublication = $this->PDFStamp->stamp($acte['Deliberation']['delib_pdf'], $acte['Deliberation']);
                $this->createElementFile(
                    $dom,
                    $doc,
                    'actePublication',
                    $aDocuments['ActePublication'],
                    $aDocuments['TexteActe'],
                    $filename,
                    'application/pdf',
                    $actePublication
                );
            }

            //Récupération du rapport de projet
            CakeLog::info(__('[Ged][%s]Acte(%s): create element "Rapport"', __METHOD__, $acte_id), 'connector');
            $aDocuments['Rapport'] = $i++;
            $filename = 'Rapport' . '_' . $acte_id . '.pdf';
            $rapport = $this->fileFusionProjet(
                $acte['Deliberation']['id'],
                'rapport',
                !empty($seance['Seance']['id']) ? $seance['Typeseance']['modele_projet_id']
                    : $acte['Typeacte']['modele_projet_id']
            );
            $this->createElementFile(
                $dom,
                $doc,
                'rapport',
                $aDocuments['Rapport'],
                null,
                $filename,
                'application/pdf',
                $rapport
            );
            unset($rapport);

            //Récupération du rapport de projet
            if (!empty($acte['Typeacte']['modele_bordereau_projet_id'])) {
                CakeLog::info(
                    __('[Ged][%s]Acte(%s): create element "BordereauProjet"', __METHOD__, $acte_id),
                    'connector'
                );
                $aDocuments['BordereauProjet'] = $i++;
                $filename = 'BordereauProjet' . '_' . $acte_id . '.pdf';
                $bordereau_projet = $this->fileFusionProjet(
                    $acte['Deliberation']['id'],
                    'bordereau_projet',
                    $acte['Typeacte']['modele_bordereau_projet_id']
                );
                $this->createElementFile(
                    $dom,
                    $doc,
                    'bordereauProjet',
                    $aDocuments['BordereauProjet'],
                    null,
                    $filename,
                    'application/pdf',
                    $bordereau_projet
                );
                unset($bordereau_projet);
            }

            //Récupération de la signature
            if (!empty($acte['Deliberation']['signature']) && $acte['Deliberation']['signature_type'] != 'PAdES') {
                CakeLog::info(
                    __('[Ged][%s]Acte(%s): create element "Signature"', __METHOD__, $acte_id),
                    'connector'
                );
                $aDocuments['Signature'] = $i++;
                $filename = 'Signature' . '_' . $acte_id . '.zip';
                $this->createElementFile(
                    $dom,
                    $doc,
                    'signature',
                    $aDocuments['Signature'],
                    $aDocuments['TexteActe'],
                    $filename,
                    'application/zip',
                    $acte['Deliberation']['signature']
                );

                CakeLog::info(
                    __('[Ged][%s]Acte(%s): create element "BordereauParapheur"', __METHOD__, $acte_id),
                    'connector'
                );
                $aDocuments['BordereauParapheur'] = $i++;
                $filename = 'BordereauParapheur' . '_' . $acte_id . '.pdf';
                $this->createElementFile(
                    $dom,
                    $doc,
                    'bordereauParapheur',
                    $aDocuments['BordereauParapheur'],
                    null,
                    $filename,
                    'application/pdf',
                    $acte['Deliberation']['parapheur_bordereau']
                );
            }

            //Récupération du bordereau de transmission
            if (!empty($aDocuments['ActeTampon'])) {
                //Noeud document[Bordereau]
                CakeLog::info(
                    __('[Ged][%s]Acte(%s): create element "BordereauTdt"', __METHOD__, $acte_id),
                    'connector'
                );
                $aDocuments['BordereauTdt'] = $i++;
                $filename = 'BordereauTdt' . '_' . $acte_id . '.pdf';
                $this->createElementFile(
                    $dom,
                    $doc,
                    'bordereauTdt',
                    $aDocuments['BordereauTdt'],
                    $aDocuments['ActeTampon'],
                    $filename,
                    'application/zip',
                    $acte['Deliberation']['tdt_data_bordereau_pdf']
                );
            }

            //Ajout de toutes les annexes du projets
            $annexes_id = $this->Deliberation->Annexe->getAnnexesFromDelibId($acte_id);
            if (!empty($annexes_id)) {
                CakeLog::info(
                    __('[Ged][%s]Acte(%s): create element "Annexe"', __METHOD__, $acte_id),
                    'connector'
                );
                foreach ($annexes_id as &$annex_id) {
                    $aDocuments['Annexe'] = $i++;
                    //Récupération des informations de l'annexe
                    $annexe = $this->Deliberation->Annexe->getContentToGed($annex_id['Annexe']['id']);
                    $children = [
                        ['tag_name' => 'nom', 'value' => $annexe['name']],
                        ['tag_name' => 'titre', 'value' => $annexe['titre']],
                        ['tag_name' => 'signature', 'value' => 'false'],
                    ];
                    if ($annexe['joindre_ctrl_legalite']) {
                        $tags = [
                            ['tag_name' => 'transmisPrefecture', 'value' => 'true'],
                            ['tag_name' => 'typologiePieceCode', 'value' => $annexe['typologiepiece_code']],
                            ['tag_name' => 'typologiePieceCodeLibelle', 'value' =>
                                $this->Typologiepiece->getTypologieNamebyCode($annexe['typologiepiece_code'])],
                        ];
                        $children = array_merge($children, $tags);
                    }
                    $filename = 'Annexe' . '_' . $annex_id['Annexe']['id'] . '.' . $annexe['type'];
                    $this->createElementFile(
                        $dom,
                        $doc,
                        'annexe',
                        $aDocuments['Annexe'],
                        null,
                        $filename,
                        $annexe['filetype'],
                        $annexe['data'],
                        null,
                        $children
                    );

                    if (!empty($annexe['data_tampon'])) {
                        $aDocuments['AnnexeTampon'] = $i++;
                        $this->createElementFile(
                            $dom,
                            $doc,
                            'annexeTampon',
                            $aDocuments['AnnexeTampon'],
                            $aDocuments['Annexe'],
                            $annexe['filename'],
                            $annexe['filetype'],
                            $annexe['data_tampon'],
                            null
                        );
                    }

                    if ($acte['Deliberation']['publier_etat'] > 0 && $annexe['joindre_ctrl_legalite'] === true) {
                        $aDocuments['AnnexePublication'] = $i++;
                        $annexe['data_publication'] = $this->PDFStamp->stamp($annexe['data'], $acte['Deliberation']);
                        $this->createElementFile(
                            $dom,
                            $doc,
                            'AnnexePublication',
                            $aDocuments['AnnexePublication'],
                            $aDocuments['Annexe'],
                            $filename,
                            $annexe['filetype'],
                            $annexe['data_publication'],
                            null
                        );
                    }
                }
            }

            //Récupération des ARacte de transmission
            if (!empty($acte['Deliberation']['tdt_ar'])) {
                CakeLog::info(
                    __('[Ged][%s]Acte(%s): create element "ARacte"', __METHOD__, $acte_id),
                    'connector'
                );
                $aDocuments['ARacte'] = $i++;
                $filename = 'ARacte' . '_' . $acte_id . '.xml';
                $this->createElementFile(
                    $dom,
                    $doc,
                    'arActe',
                    $aDocuments['ARacte'],
                    null,
                    $filename,
                    'application/xml',
                    $acte['Deliberation']['tdt_ar']
                );
            }

            //Récupération des messages TDT
            $messages = $this->getTdtMessageForGed($acte['Deliberation']['id']);
            if (!empty($messages)) {
                CakeLog::info(
                    __('[Ged][%s]Acte(%s): create element "ARacte"', __METHOD__, $acte_id),
                    'connector'
                );
                foreach ($messages as $message) {
                    $aDocuments['TdtMessage'] = $i++;
                    $this->createElementFile(
                        $dom,
                        $doc,
                        $this->inflectorPluralizeForFolder($message['type']),
                        $aDocuments['TdtMessage'],
                        null,
                        $message['relName'],
                        'application/pdf',
                        $message['content_pdf']
                    );

                    //Récupération des réponses
                    if (!empty($message['reponses'])) {
                        foreach ($message['reponses'] as $reponse) {
                            $aDocuments['TdtMessageReponse'] = $i++;
                            $this->createElementFile(
                                $dom,
                                $doc,
                                $this->inflectorPluralizeForFolder($reponse['type']),
                                $aDocuments['TdtMessageReponse'],
                                $aDocuments['TdtMessage'],
                                $reponse['relName'],
                                'application/pdf',
                                $reponse['content_pdf']
                            );
                        }
                    }
                }
            }

            CakeLog::info(
                __('[Ged][%s]Acte(%s): add to DOMDocument', __METHOD__, $acte_id),
                'connector'
            );
            $dom_depot->appendChild($doc);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     *
     */
    public function createElementCommissionsForActe(&$dom, &$doc, &$acte)
    {
        $seanceIds = $this->Deliberation->getSeancesid($acte['Deliberation']['id']);
        if (empty($seanceIds)) {
            return;
        }

        $document = $this->createElement($dom, 'listeCommissions', null);
        foreach ($seanceIds as $commission_id) {
            if (!$this->Deliberation->Seance->isDeliberante($commission_id)) {
                $commission = $this->createElement(
                    $dom,
                    'commission',
                    null,
                    ['idCommission' => $commission_id]
                );
                $commission->appendChild($this->createElement(
                    $dom,
                    'typeSeance',
                    $this->Deliberation->Seance->Typeseance->getLibelle(
                        $this->Deliberation->Seance->getType($commission_id)
                    )
                ));
                $commission->appendChild($this->createElement(
                    $dom,
                    'dateSeance',
                    $this->Deliberation->Seance->getDate($commission_id)
                ));
                $document->appendChild($commission);
            }
        }

        $doc->appendChild($document);
    }

    /**
     *
     */
    public function createElementFile(
        &$dom,
        &$doc,
        $type,
        $id,
        $ref,
        $filename,
        $mimetype,
        $contents,
        $attributes = [],
        $children = []
    ) {
        //Ajout du fichier
        $relativePath = $this->inflectorPluralizeForFolder($type);
        $absolutePath = $this->folderSend->pwd() . DS . $relativePath;

        $folder = new Folder();
        CakeLog::info(
            __('[Ged][%s]Acte: create folder %s', __METHOD__, $relativePath),
            'connector'
        );
        if (!$folder->create($absolutePath, 0777)) {
            $message = __('[Ged][%s]Acte: error create folder %s', __METHOD__, $relativePath);
            CakeLog::error($message, 'connector');
            throw new Exception($message, '500');
        }
        $folder->cd($absolutePath);
        $file = new File($folder->pwd() . DS . $filename);
        CakeLog::info(
            __('[Ged][%s]Acte: add file %s', __METHOD__, $folder->pwd() . DS . $filename),
            'connector'
        );
        $file->write($contents);
        $file->close();

        $attributes_default = [
            'idDocument' => $id,
            'type' => $type,
            'relativePath' => './' . $relativePath,
            'fileName' => $filename
        ];

        if (!empty($ref)) {
            $attributes_default = array_merge($attributes_default, ['refDocument' => $ref]);
        }

        //ajout des attibuts
        if (!empty($attributes)) {
            $attributes = array_merge($attributes_default, $attributes);
        } else {
            $attributes = $attributes_default;
        }

        //ajout des childs par défaut
        array_unshift(
            $children,
            ['tag_name' => 'mimetype', 'value' => $mimetype],
            ['tag_name' => 'encoding', 'value' => 'utf-8']
        );

        $document = $this->createElement($dom, 'document', null, $attributes);
        foreach ($children as $child) {
            $document->appendChild($this->createElement($dom, $child['tag_name'], $child['value']));
        }
        $doc->appendChild($document);
    }

    public function getClient()
    {
        if ($this->client instanceof CMISService) {
            return $this->client;
        }
        
        return $this->client = new CMISService(
            Configure::read('CMIS_HOST'),
            Configure::read('CMIS_LOGIN'),
            Configure::read('CMIS_PWD'),
            null,
            [
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false]
        );
    }

    public function getFolderId()
    {
        return $this->folder->id;
    }

    public function send(&$folderSend, &$cmisFolder)
    {
        CakeLog::info(__('[Ged][%s]Find folder and files', __METHOD__), 'connector');
        $folder_send = $folderSend->read();

        //Envoi des fichiers dans les sous répertoires n-1
        $folders = $folder_send[0];
        foreach ($folders as $folder) {
            $cmisFolderSend = $this->client->createFolder($cmisFolder->id, $folder);
            CakeLog::info(__('[Ged][%s]Folder send: %s', __METHOD__, $folder), 'connector');
            $folder = new Folder($folderSend->pwd() . DS . $folder);

            $files = $folder->find('.*');
            foreach ($files as $file) {
                $file = new File($folder->pwd() . DS . $file);
                $contents = $file->read();
                $this->client->createDocument(
                    $cmisFolderSend->id,
                    $file->name,
                    [],
                    $file->read(),
                    ($file->mime() == "application/xml" ? 'text/xml' : $file->mime())
                );
                CakeLog::info(
                    __('[Ged][%s]File send %s (%s)', __METHOD__, $file->name, $file->mime()),
                    'connector'
                );
                $file->close();
            }
        }

        //Envoi des fichiers depuis absolutePath
        $files = $folder_send[1];

        //on s'assure que le fichier de description tienne en dernier
        // dans le tableau pour qu'il soit envoyer en dernier.
        $key = array_keys(preg_grep("/^XML_DESC/", $files));
        $tmp = preg_grep("/^XML_DESC/", $files);
        unset($files[$key[0]]);
        $files[] = $tmp[$key[0]];

        foreach ($files as $file) {
            $file = new File($folderSend->pwd() . DS . $file);
            $contents = $file->read();
            $this->client->createDocument(
                $cmisFolder->id,
                $file->name,
                [],
                $file->read(),
                ($file->mime() == "application/xml" ? 'text/xml' : $file->mime())
            );
            CakeLog::info(
                __('[Ged][%s]File send %s (%s)', __METHOD__, $folderSend->pwd() . DS . $file->name, $file->mime()),
                'connector'
            );
            $file->close();
        }
    }

    public function fileFusionProjet($id, string $modeleName, $modele_projet_id): string
    {
        return $this->Deliberation->fusion(
            $id,
            $modeleName,
            $modele_projet_id
        );
    }

    public function fileFusionSeance($id, string $modeleName): string
    {
        return $this->Seance->fusion(
            $id,
            $modeleName
        );
    }
}
