<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [TeletransmissionComponent description]
 * @package     app.Controller.Component
 */
class TeletransmissionComponent extends Component
{
    public $components = [];

    /**
     * @param type $controller
     * @version 5.1
     */
    public function initialize(Controller $controller)
    {
        $this->Typologiepiece = ClassRegistry::init('Typologiepiece');
        $this->Nature = ClassRegistry::init('Nature');
    }

    /**
     *     //Fix $selectedTypeseanceIds
     * @param type $typeacte_id
     * @param type $projet_id
     * @param type $selectedTypeseanceIds
     * @return boolean
     */
    public function getTypologiePieces(
        $type,
        $typeacteId = null,
        $codeClassification = null,
        $projetId = null,
        $selectedTypologiePieceCode = null
    ) {
        $typologiePieces = $this->Typologiepiece->getTypologiePieceByTypeActe($type, $typeacteId, $codeClassification);
        $options = [];
        if (empty($selectedTypologiePieceCode) && $type==='Principale') {
            $selectedTypologiePieceCode = $this->Nature->getNatureDefaultCodeByTypeActe($typeacteId);
        }
        foreach ($typologiePieces as $typologiePiece) {
            $options[] = [
                'name' => __(
                    '%s (%s)',
                    $typologiePiece['Typologiepiece']['name'],
                    $typologiePiece['Typologiepiece']['code']
                ),
                'value' => $typologiePiece['Typologiepiece']['code'],
                'selected' => !empty($selectedTypologiePieceCode)
                && $typologiePiece['Typologiepiece']['code'] == $selectedTypologiePieceCode ? true : false
            ];
        }

        return $options;
    }
}
