<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('VerificationLib', 'Lib');

class VerificationsController extends AppController
{
    public $components = [
        'Auth' => [
            'mapActions' => [
                'read' => ['admin_index']
            ]
        ]];

    public $helpers = ['Check'];

    public $resultatsTests = [];

    /**
     *
     */
    public function admin_index() // phpcs:ignore
    {
        $results = [];

        $results['verifVersionWebdelib'] = [VerificationLib::versionWebdelib()];
        $results['verifVersionOS'] = [VerificationLib::versionOS()];
        $results['verifVersionCakePHP'] = [
            VerificationLib::versionCakePhp(Configure::read('app_check.versionCakePHPAttendue'))
        ];
        $results['verifVersionPHP'] = [
            VerificationLib::versionPHP(Configure::read('app_check.versionPHPAttendu'))
        ];
        $results['verifVersionApache'] = [
            VerificationLib::versionApache(Configure::read('app_check.versionAPACHEAttendue'))
        ];
        $results['verifPermissionsRepertoires'] = VerificationLib::permissionsRepertoires();
        $results['verifConsoleCakePhp'] = VerificationLib::consoleCakePhp();

        $results['apache_check_modules'] = VerificationLib::checkConditions(['mod_rewrite'], 'apacheCheckModule');

        $results['verifExtensionsPhp'] = VerificationLib::checkConditions([
            'soap', 'pgsql', 'xsl', 'curl', 'dom', 'zlib', 'imagick'
        ], 'phpCheckExtension');


        $results['verifLibrairiesPhp'] = VerificationLib::checkConditions(['XML_RPC2' => [
                        'class' => 'XML_RPC2_Client',
                        'require' => 'XML/RPC2/Client.php']], 'phpCheckLibrairie');

        $conditionsBinaires = [
            'pdfinfo' => [
                'executable' => 'pdfinfo',
                'option_version' => '-v',
                'attendu' => '0.86.1'
            ],
            'fido' => [
                'executable' => 'fido',
                'option_version' => '-v',
                'attendu' => 'FIDO v1.6.1 (formats-v109.xml, container-signature-20200121.xml, format_extensions.xml)'
            ],
            'pdftk' => ['executable' => 'pdftk', 'option_version' => '--version', 'attendu' => '3.0.9']
        ];

        $results['verifBinaires'] = VerificationLib::checkConditions($conditionsBinaires, 'checkBinaire');
        $results['verifPresenceFichiers'] = VerificationLib::checkConditions([
            'webdelib.inc',
            'formats.inc',
            'pastell.inc',
        ], 'checkFichierIni');


        $results['configDataBase'] = VerificationLib::configDataBase();

        if ($results['configDataBase'][0]['okko'] === 'ok') {
            $results['InfosDataBase'] = VerificationLib::infosDataBase();
            $results['ConnectionDataBase'] = VerificationLib::connectionDataBase();
            if ($results['ConnectionDataBase'][0]['okko'] === 'ok') {
                $results['ConnectionDataBaseConfig'] = VerificationLib::connectionDataBaseConfig();
            }
        }



        $results['verifcheckVersionSchema'] = VerificationLib::checkVersionSchema();
        $results['verifcheckSchema'] = VerificationLib::checkSchema();

        if ($results['verifPresenceFichiers']['webdelib.inc']['okko'] == 'ok') {
            $configmailnecessaire = ['MAIL_FROM' => 'Mail from'];
            $results['configmail'] = VerificationLib::checkConditions($configmailnecessaire, 'VariableNecessaire');

            $MailAdminOk = empty(Hash::extract($results['configmail'], '{n}[okko=/^ko$/]'));

            $variablesNecessairesConversions = [
                'FusionConv.method' => 'Type d\'outil de conversion',
                'FusionConv.cloudooo_host' => 'Url de CLOUDOOO',
            ];
            $results['verifVariableNecessaireConversion'] = VerificationLib::checkConditions(
                $variablesNecessairesConversions,
                'VariableNecessaire'
            );
            $variablesConversionsOK = empty(Hash::extract(
                $results['verifVariableNecessaireConversion'],
                '{n}[libelle=/^webdelib.inc$/][okko=/^ko$/]'
            ));

            if ($variablesConversionsOK) {
                $results['verifTypeConversion'] = [VerificationLib::typeConversion()];
                $results['verifConversion'] = [VerificationLib::conversion()];
                $results['afficheInfosCloudoo'] = [VerificationLib::afficheInfosCloudoo()];
            }

            $variablesNecessairesFusion = ['FusionConv.Gedooo.wsdl' => 'Outil de fusion des modèles odt Gedooo'];
            $results['verifVariableNecessaireFusion'] = VerificationLib::checkConditions(
                $variablesNecessairesFusion,
                'VariableNecessaire'
            );
            $variablesFusionOK = empty(Hash::extract(
                $results['verifVariableNecessaireFusion'],
                '{n}[libelle=/^webdelib.inc$/][okko=/^ko$/]'
            ));

            if ($variablesFusionOK) {
                $results['testerReponseGedooo'] = [VerificationLib::reponseGedooo()];
                $results['veriftesterOdfGedooo'] = [VerificationLib::versionGedooo()];
                $results['testerFusionGedoo'] = [VerificationLib::fusionGedoo()];
            }


            if (Configure::read('USE_S2LOW')) {
                $results['getClassification'] = VerificationLib::getClassification();
            }

            if (Configure::read('USE_PARAPHEUR') && Configure::read('PARAPHEUR') === 'IPARAPHEUR') {
                $results['getCircuitsParapheur'] = VerificationLib::getCircuitsParapheur();
            }

            if (Configure::read('USE_PASTELL')) {
                $results['getPastellVersion'] = VerificationLib::getPastellVersion();
            }

            if (Configure::read('USE_GED')) {
                $results['checkGED'] = VerificationLib::checkGED();
            }

            if (Configure::read('USE_IDELIBRE')) {
                $results['checkIdelibre'] = VerificationLib::checkIdelibre();
                $results['checkIdelibreVersion'] = VerificationLib::checkIdelibreVersion();
                $results['checkIdelibreConnexion'] = VerificationLib::checkIdelibreConnexion();
            }

            if (Configure::read("LdapManager.Ldap.use")) {
                $results['checkLDAPUser'] = [VerificationLib::checkLDAPUser()];
                $results['checkLdap'] = VerificationLib::checkLdap();
            }
        }

        $this->set($results);
    }
}
