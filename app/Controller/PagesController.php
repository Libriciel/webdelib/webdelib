<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class PagesController extends AppController
{
    public $uses = [];
    public $components = [
        // @see AuthManager.ignoreControllers dans app/Config/bootstrap.php
        // Ça évite d'avoir une entrée controllers/Pages dans les permissions
    ];

    /**
     *
     */
    public function beforeFilter()
    {
        if (!$this->Auth->user()) {
            $this->redirect($this->Auth->loginAction);
        }

        parent::beforeFilter();
    }

    /**
     *
     */
    public function help()
    {
    }
}
