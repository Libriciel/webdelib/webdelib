<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Class WebdavController
 *
 * @version 4.3
 * @package app.Controller
 */
class WebdavController extends AppController
{
    /**
     * @version 4.3
     * @access public
     * @var type
     */
    public $components = [
        'SabreDav',
        'Auth' => [
            'mapActions' => [
                'allow' => ['index']
            ],
          ]
    ];

    /**
     * @version 5.1
     * @since 4.3
     * @access public
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow();

        $this->SabreDav->server();
    }

    /**
     * @access public
     */
    public function index()
    {
    }
}
