<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('CakeTime', 'Utility');
App::uses('SaeVersement', 'Model');
/**
 * Contrôleur des Post-séances
 *
 * @package app.Controller
 * @version 5.1.2
 * @since 1.0.0
 */
class PostseancesController extends AppController
{
    /**
     * [public description]
     * @var [type]
     */
    public $name = 'Postseances';
    /**
     * [public description]
     * @var [type]
     */
    public $helpers = ['TinyMCE','DelibTdt'];
    /**
     * [public description]
     * @var [type]
     */
    public $components = [
        'RequestHandler',
        'ProjetTools',
        'Filtre',
        'Progress',
        'Conversion',
        'Paginator',
        'Signatures',
        'Auth' => [
            'mapActions' => [
                'read' => [
                  'index',
                  'afficherProjets',
                  'changeStatus',
                  'download'
                ],
                'ExportGedPostSeances',
                'sendToSae',
                'toSend' => ['sendToTdt', 'depotManuel', 'typologiePieceToTdtAjax', 'editInfoSendToTdt'],
                'transmit',
                'signature'
            ]
        ]];
    /**
     * [public description]
     * @var [type]
     */
    public $uses = [
        'Deliberation',
        'Annexe',
        'DeliberationSeance',
        'DeliberationTypeseance',
        'Infosup',
        'Seance',
        'User',
        'Listepresence',
        'Vote',
        'Nomenclature',
        'Service',
        'Circuit',
        'ModelOdtValidator.Modeltemplate',
        'Theme',
        'Typeseance',
        'Typeacte',
        'Nature',
        'TdtMessage',
        'Historique',
        'Typologiepiece',
        'SaeVersement'
    ];

    /**
     * @version 4.3
     * @access public
     */
    public function index()
    {
        //Présence de débat ou non
        $this->Seance->virtualFields['hasGlobalDebat'] =
            "CASE WHEN Seance.debat_global_size  > 0 THEN true ELSE false END";
        $this->Seance->Deliberation->virtualFields['hasDebat'] =
            "CASE WHEN Deliberation.debat_size  > 0 THEN 1 ELSE 0 END";


        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);
        if (!$this->Filtre->critereExists()) {
            $typesSeance = $this->Typeseance->find('list', ['fields' => ['id', 'libelle']]);

            //Filtre sur le type de séance.
            $this->Filtre->addCritere('Type de séance', [
                'field' => 'Seance.type_id',
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Type de séance'),
                    'data-placeholder' => __('Sélectionner un type de séance recherché'),
                    'data-allow-clear' => true,
                    'options' => $typesSeance],
                'column' => 2
            ]);

            $this->Filtre->addCritere('Date', [
                'field' => 'Seance.id',
                'inputOptions' => [
                    'type' => 'select',
                    'retourLigne' => true,
                    'label' => __('Date'),
                    'data-placeholder' => __('Sélectionner une date'),
                    'data-allow-clear' => true,
                    'options' =>$this->Seance->generateAllList(['Seance.traitee' => 1])
                ],
                'column' => 2
            ]);


            $this->Filtre->addCritere('Débats généraux', [
                'field' => 'Seance.hasGlobalDebat',
                'inputOptions' => [
                    'type' => 'select',
                    'retourLigne' => true,
                    'label' => __('Débats généraux'),
                    'data-placeholder' => __('Sélectionner les séances avec des débats généraux'),
                    'data-allow-clear' => true,
                    'options' => ["true" => "Oui", "false"=>"Non"]],
                'column' => 2
            ]);
        }

        $conditions = array_merge($this->Filtre->conditions(), ['Seance.traitee' => 1]);
        $this->paginate = [
            'fields' => [
              'id',
              'traitee',
              'date',
              'type_id',
              'pv_figes',
              'numero_depot',
              'sae_numero_versement',
              'sae_etat',
              'sae_atr',
              'sae_pastell_id',
              'sae_commentaire',
              'hasGlobalDebat',
              'parent_id'
            ],
            'contain' => [
                'Deliberation' => [
                  'fields'=> ['etat','hasDebat','sae_etat'],
                ],
                'Typeseance' => [
                  'fields'=> [
                      'libelle','action','color','modele_convocation_id',
                      'modele_ordredujour_id','modele_pvsommaire_id','modele_pvdetaille_id'
                  ],
                  'Modele_pvdetaille.name',
                  'Modele_pvsommaire.name',
                ],
                'SeanceChildren' => [
                  'fields'=> ['date'],
                  'Typeseance' => ['fields'=> ['libelle']]
                ],
                'SeanceParent' => [
                  'fields'=> ['date'],
                  'Typeseance' => ['fields'=> ['libelle']]
                ]
            ],
            'conditions' => $conditions,
            'order' => ['Seance.date'=>'DESC'],
            'limit' => 10,
            'allow' => ['typeseance_id']
        ];


        $actions = [];
        if ($this->Acl->check(
            ['User' => ['id' => $this->Auth->user('id')]],
            'ExportGedSeances',
            'read'
        )
            && Configure::read('USE_GED')
            && Configure::read('GED') ==='CMIS') {
            $actions[] = 'ged';
        }
        if ($this->Acl->check(['User' => ['id' => $this->Auth->user('id')]], 'sendToSae', 'read')
            && Configure::read('USE_SAE')
            && Configure::read('SAE') ==='PASTELL') {
            $actions[] = 'sendToSae';
        }
        if ($this->Acl->check(
            ['User' => ['id' => $this->Auth->user('id')]],
            'sendToSaeDeliberation',
            'read'
        )
            && Configure::read('USE_SAE')
            && Configure::read('SAE') ==='PASTELL') {
            $actions[] = 'sendToSaeDeliberation';
        }

        $seances = $this->paginate('Seance');

        foreach ($seances as &$seance) {
            $seance['Seance']['pv_figes'] = (bool)$seance['Seance']['pv_figes'];
            $seance['nombreDeProjet'] = count($seance['Deliberation']);
            $seance['nombreDeDebats'] = count(Hash::extract($seance, 'Deliberation.{n}[hasDebat=1].hasDebat'));
            $seance['nombreDeliberationTdtOk'] = count(Hash::extract($seance, 'Deliberation.{n}[etat=5].etat'))
            + count(Hash::extract($seance, 'Deliberation.{n}[etat=11].etat'));
            $seance['nombreDeliberationSaeOk'] = count(Hash::extract($seance, 'Deliberation.{n}[sae_etat='
                . SaeVersement::ACCEPTER .
                '].sae_etat'));
            $seance['Seance']['isSeanceOk'] = $this->SaeVersement->isSeanceOk($seance['Seance']['id'])
                && $seance['Seance']['pv_figes'];
        }

        $this->set('actions', $actions);
        $this->set('use_tdt', Configure::read('USE_TDT'));
        $this->set('seances', $seances);
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     * @param type $return
     * @return type
     */
    public function afficherProjets($id = null, $render = null, $return = null)
    {
        $actions = $delibs = [];

        $seance = $this->Seance->find('first', [
            'fields' => ['traitee', 'pv_figes', 'date'],
            'contain' => ['Typeseance' => ['fields' => ['Typeseance.id', 'Typeseance.libelle', 'Typeseance.action']]],
            'conditions' => ['Seance.id' => $id],
            'recursive' => -1
        ]);

        $this->set('pv_figes', $seance['Seance']['pv_figes']);

        if (!isset($return)) {
            $this->set('lastPosition', $this->Seance->getLastPosition($id));
            $deliberations = $this->Seance->getDeliberationsId($id);
            $i = 0;
            foreach ($deliberations as $delib_id) {
                $delib = $this->Deliberation->find('first', [
                    'fields' => [
                        'objet_delib', 'titre', 'etat', 'Deliberation.id',
                        'num_delib', 'signee', 'numero_depot'
                    ],
                    'conditions' => ['Deliberation.id' => $delib_id],
                    'contain' => ['Theme.libelle', 'Rapporteur.nom', 'Rapporteur.prenom'],
                    'recursive' => -1,
                ]);
                $delibs[$i] = $delib;
                $delibs[$i]['Modeltemplate']['id'] =
                    $this->Typeseance->modeleProjetDelibParTypeSeanceId(
                        $seance['Typeseance']['id'],
                        $delib['Deliberation']['etat']
                    );
                $i++;
            }
            if (Configure::read('USE_GED') && Configure::read('GED') =='PASTELL') {
                array_push($actions, 'ged');
            }
            $this->set('actions', $actions);
            $this->set('seance_id', $id);
            $this->set('projets', $delibs);
            $this->set('seance', $seance);
        } else {
            $condition = ["seance_id" => $id, "etat >=" => 2];
            return $this->Deliberation->find('all', [
                'conditions' => $condition,
                'order' => ['Deliberation.position' => 'ASC']
            ]);
        }
        ($render===null) ?: $this->render($render);
    }

    /**
     * @version 4.3
     * @access public
     * @param type $seance_id
     */
    public function changeStatus($seance_id)
    {
        if (!$this->stockPvs($seance_id)) {
            $this->Flash->set(
                "Au moins un PV n'a pas été généré correctement... Impossible de figer les débats",
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
        }
        $this->redirect($this->previous);
    }

    /**
     *
     * @access private
     *
     * @param type $seanceId
     * @return boolean
     * @throws Exception
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    private function stockPvs($seanceId)
    {
        // début de transaction
        $this->Seance->begin();

        try {
            // lecture de la séance
            $seance = $this->Seance->find('first', [
                'recursive' => 0,
                'fields' => ['Seance.id', 'Seance.pv_figes', 'Typeseance.modele_pvsommaire_id',
                    'Typeseance.modele_pvdetaille_id'],
                'conditions' => ['Seance.id' => $seanceId]]);
            if (empty($seance)) {
                throw new Exception("Séance introuvable");
            }

            // fusion du pv sommaire
            $content = $this->Seance->fusion(
                $seanceId,
                'pvsommaire',
                $seance['Typeseance']['modele_pvsommaire_id']
            );
            $seance['Seance']['pv_sommaire'] = &$content;
            if (!$this->Seance->save($seance['Seance'], false)) {
                throw new Exception("Erreur lors de la sauvegarde du pv sommaire de la séance");
            }
            unset($seance['Seance']['pv_sommaire']);

            // fusion du pv détaillé
            if ($seance['Typeseance']['modele_pvsommaire_id'] !== $seance['Typeseance']['modele_pvdetaille_id']) {
                $content = $this->Seance->fusion(
                    $seanceId,
                    'pvdetaille',
                    $seance['Typeseance']['modele_pvdetaille_id']
                );
            }
            $seance['Seance']['pv_complet'] = &$content;
            $seance['Seance']['pv_figes'] = true;
            if (!$this->Seance->save($seance['Seance'], false)) {
                throw new Exception("Erreur lors de la sauvegarde du pv détaillé de la séance");
            }
            unset($seance);
            unset($content);

            $this->Seance->commit();
            return true;
        } catch (Exception $e) {
            $this->Seance->rollback();
            return false;
        }
    }

    /**
     * @version 4.3
     * @access public
     * @param type $seance_id
     * @param type $type
     */
    public function download($seance_id, $type)
    {
        $seance = $this->Seance->find('first', [
            'conditions'=> [
                'Seance.id' => $seance_id
            ],
        ]);
        $this->response->disableCache();
        $this->response->type('application/pdf');
        if ($type == "sommaire") {
            $this->response->body($seance['Seance']['pv_sommaire']);
            $this->response->download('pv_sommaire.pdf');
        }
        if ($type == "complet") {
            $this->response->body($seance['Seance']['pv_complet']);
            $this->response->download('pv_complet.pdf');
        }
        if ($type == "sae_atr") {
            $this->response->type('application/xml');
            $this->response->body($seance['Seance']['sae_atr']);
            $this->response->download('sae_atr.xml');
        }

        return $this->response;
    }

    /**
     * modify informations for send to TDT (s2low ou pastell)
     *
     * @access public
     * @return type
     */
    public function editInfoSendToTdt()
    {
        parent::editInfoSendToTdt();
    }

    /**
     * [public description]
     * @version 5.1.2
     * @since 5.1.1
     */
    public function typologiePieceToTdtAjax()
    {
        parent::typologiePieceToTdtAjax();
    }

    public function depotManuel()
    {
        parent::sendToTdt();

        return $this->redirect($this->previous);
    }

    /**
     * @version 4.3
     * @access public
     * @param type $seance_id
     */
    public function toSend($seance_id = null)
    {
        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);

        if (Configure::read('USE_TDT')) {
            App::uses('Tdt', 'Lib');
            $Tdt = new Tdt();
            $this->set('dateClassification', $Tdt->getDateClassification());
        }

        $this->set('tabNature', $this->ProjetTools->getNatureListe());
        // FIX
        if (empty($seance_id)) {
            $conditions1 = $this->handleConditions($this->Filtre->conditions());
            $conditions['Deliberation.etat <'] = 5;
            $conditions1['Deliberation.etat IN'] = $this->Deliberation->isReadyForTDT();
        } else {
            //Ajout de la condition sur séance par le filtre
            $conditions1 = $this->Filtre->conditions();
            $conditions1['DeliberationSeance.seance_id'] = $seance_id;
            $conditions1 = $this->handleConditions($conditions1);
        }
        $conditions1['Deliberation.signee'] = true;

        $conditions=[
            $conditions1,
//            'OR'=>['Deliberation.etat' => 10,
//                'Deliberation.signee' =>false]
        ];

        $conditions['NOT']['Deliberation.delib_pdf'] = null;

        $db = $this->DeliberationSeance->getDataSource();
        $subQuery = [
                'fields' => [
                    'DeliberationSeance.deliberation_id'
                ],
                'contain' => false,
                'joins' => [
                    $this->DeliberationSeance->join('Seance', ['type' => 'INNER']),
                    $this->Deliberation->join('Typeacte', [
                        'type' => 'INNER',
                        'conditions' => ['Typeacte.teletransmettre' => true, 'Typeacte.deliberant' => true]
                    ]),
                ]
            ];

        $subQuery = $this->DeliberationSeance->sql($subQuery);
        $subQuery = ' "Deliberation"."id" IN (' . $subQuery . ') ';
        $subQueryExpression = $db->expression($subQuery);
        $conditions[] = $subQueryExpression;

        $this->paginate = [
            'countField' => 'DISTINCT Deliberation.id',
            'fields' => [
                'DISTINCT Deliberation.id',
                'Deliberation.objet_delib',
                'Deliberation.num_delib',
                'Deliberation.titre',
                'Deliberation.etat',
                'Deliberation.signee',
                'Deliberation.parapheur_bordereau',
                'Deliberation.parapheur_etat',
                'Deliberation.tdt_id',
                'Deliberation.num_pref',
                'Deliberation.typologiepiece_code',
                'Deliberation.tdt_document_papier',
                'Deliberation.circuit_id',
                'Deliberation.typeacte_id',
                'Deliberation.theme_id',
                'Deliberation.pastell_id',
                'Deliberation.tdt_status',
                'Deliberation.service_id'],
            'conditions' => $conditions,
            'joins' => $this->handleJoins(),
            'contain' => [
                'Service' => ['fields' => ['name']],
                'Theme' => ['fields' => ['libelle']],
                'Typeacte' => ['fields' => ['name']],
                'Circuit' => ['fields' => ['nom']],
                'Annexe' => [
                    'fields' => ['id', 'data_pdf', 'filename', 'typologiepiece_code'],
                    'conditions' => ['joindre_ctrl_legalite' => true]
                ],
                'DeliberationTypeseance' => [
                    'fields' => ['id'],
                    'Typeseance' => [
                        'fields' => ['id', 'libelle', 'action'],
                    ]
                ],
                'DeliberationSeance' => [
                    'fields' => ['id'],
                    'Seance' => [
                        'fields' => ['id', 'date', 'type_id'],
                        'Typeseance' => [
                            'fields' => ['id', 'libelle', 'action']
                        ]]]],
            'order' => ['Deliberation.id' => 'DESC'],
            'recursive' => -1,
            'limit' => 20,
            'allow' => ['typeacte_id'],
        ];

        $actes = $this->Paginator->paginate('Deliberation');
        $this->ajouterFiltre($actes);

        if (!empty($seance_id)) {
            $this->Filtre->delCritere('DeliberationSeanceId');
            $this->Filtre->delCritere('DeliberationTypeseanceId');
            $this->set('seance_id', $seance_id);
        }

        $droits = $this->ProjetTools->getDroits(['toSend' => 'read']);

        for ($i = 0; $i < count($actes); $i++) {
            $this->ProjetTools->actionDisponible($actes[$i], $droits);
            $actes[$i]['Deliberation']['num_pref_libelle'] =
                $this->ProjetTools->getMatiereByKey($actes[$i]['Deliberation']['num_pref']);
            $actes[$i]['Deliberation']['num_pref'] =
                str_replace('.', '_', $actes[$i]['Deliberation']['num_pref']);
            //On cherche à savoir si la délibération est vérrouillée par un utilisateur
            $this->ProjetTools->locked($actes[$i]['Deliberation']);
            $actes[$i]['Deliberation']['typologiepiece_code_libelle'] =
                $this->Typologiepiece->getTypologieNamebyCode($actes[$i]['Deliberation']['typologiepiece_code']);
            foreach ($actes[$i]['Annexe'] as &$annexe) {
                $annexe['typologiepiece_code_libelle'] =
                    $this->Typologiepiece->getTypologieNamebyCode($annexe['typologiepiece_code']);
            }
            $actes[$i]['isTypologiepieceEmpty'] = $this->Deliberation->isParamTdtEmptyByActe($actes[$i]);
        }

        $this->set('traitement_lot', true);

        $this->set('deliberations', $actes);
    }

    /**
     * @version 4.3
     * @access public
     * @param type $seance_id
     * @return type
     */
    public function transmit($seance_id = null)
    {
        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);

        $conditions = $this->handleConditions($this->Filtre->conditions());
        if (isset($conditions['Seance.id'])) {
            $seance_id = $conditions['Seance.id'];
            unset($conditions['Seance.id']);
        }
        if ($seance_id != null) {
            $conditions['Deliberation.id'] = $this->Seance->getDeliberationsId($seance_id);
        }
        $conditions['Deliberation.typeacte_id'] = $this->Deliberation->Typeacte->getTypeActeIdDeliberant();
        $conditions['Deliberation.etat'] = [5,11]  ; //transmits TDT via WD et transmits manuellement

        //Rechercher les projets transmissibles
        $db = $this->DeliberationSeance->getDataSource();
        $subQuery = [
                'fields' => [
                    'DeliberationSeance.deliberation_id'
                ],
                'contain' => false,
                'joins' => [
                    $this->DeliberationSeance->join('Seance', ['type' => 'INNER']),
                    $this->Deliberation->join('Typeacte', [
                            'type' => 'INNER',
                            'conditions' => ['Typeacte.teletransmettre' => true, 'Typeacte.deliberant' => true]
                        ]),
                ]
            ];

        $subQuery = $this->DeliberationSeance->sql($subQuery);
        $subQuery = ' "Deliberation"."id" IN (' . $subQuery . ') ';
        $subQueryExpression = $db->expression($subQuery);
        $conditions[] = $subQueryExpression;

        $this->paginate = ['Deliberation' => [
                'countField' => 'DISTINCT Deliberation.id',
                'fields' => [
                    'DISTINCT Deliberation.id',
                    'Deliberation.objet',
                    'Deliberation.objet_delib',
                    'Deliberation.num_delib',
                    'Deliberation.tdt_dateAR',
                    'Deliberation.tdt_ar_date',
                    'Deliberation.tdt_ar',
                    'Deliberation.parapheur_id',
                    'Deliberation.num_pref',
                    'Deliberation.typologiepiece_code',
                    'Deliberation.tdt_document_papier',
                    'Deliberation.etat',
                    'Deliberation.titre',
                    'Deliberation.tdt_id',
                    'Deliberation.pastell_id',
                    'Deliberation.typeacte_id',
                    'Deliberation.theme_id',
                    'Deliberation.service_id',
                    'Deliberation.circuit_id',
                    'Deliberation.tdt_status',
                    'Deliberation.tdt_message'],
                'joins' => array_merge(
                    [
                      $this->Deliberation->join('DeliberationSeance', ['type' => 'LEFT']),
                      $this->Deliberation->join('DeliberationTypeseance', ['type' => 'LEFT']),
                        ],
                    $this->handleJoins()
                ),
                'conditions' => $conditions,
                'contain' => [
                    'Service' => ['fields' => ['name']],
                    'Circuit' => ['fields' => ['nom']],
                    'Theme' => ['fields' => ['libelle']],
                    'Annexe' => [
                        'fields' => ['id', 'filename', 'filetype', 'typologiepiece_code'],
                        'conditions' => ['joindre_ctrl_legalite' => true],
                        'order' => ['position']
                    ],
                    'Typeacte' => ['fields' => ['name', 'teletransmettre']],
                    'TdtMessage' => ['fields' => ['tdt_id', 'tdt_type', 'tdt_etat', 'parent_id'],
                        'conditions' => ['parent_id is null'],
                        'Reponse' => ['fields' => ['tdt_id', 'tdt_type', 'tdt_etat']]],
                     'DeliberationTypeseance' => ['fields' => ['id'],
                         'Typeseance' => [
                           'fields' => ['id', 'libelle', 'color', 'action'
                           ],
                         ]],
                     'DeliberationSeance' => [
                         'fields' => ['id'],
                         'Seance' => [
                             'fields' => ['id', 'date', 'type_id'],
                             'Typeseance' => [
                                 'fields' => ['id', 'libelle', 'color', 'action'],
                             ],

                         ]]
                  ],
                'order' => ['Deliberation.id' => 'DESC'],
                'allow' => ['typeacte_id'],
                'limit' => 20,
                'recursive' => -1]];

        $this->set('tdt', Configure::read('TDT'));
        $this->set('tdt_host', Configure::read(Configure::read('TDT') . '_HOST'));

        if (Configure::read('USE_TDT')) {
            App::uses('Tdt', 'Lib');
            $Tdt = new Tdt();
            $this->set('dateClassification', $Tdt->getDateClassification());
        }

        // On affiche que les delibs vote pour.
        $deliberations = $this->Paginator->paginate('Deliberation');
        $this->ProjetTools->sortProjetSeanceDate($deliberations);

        $docs_type = Configure::read('DOC_TYPE');
        foreach ($deliberations as $i => $projet) {
            $this->Deliberation->id =  $deliberations[$i]['Deliberation']['id'];
            $deliberations[$i]['Deliberation']['cancelSendTDT'] = (
                $this->Deliberation->isCancelableForTdt()
                && $this->Acl->check(
                    ['User' => ['id' => $this->Auth->user('id')]],
                    'Teletransmettre/deliberationCancelSendToTDT',
                    'read'
                )
            ); //Annuler l'envoi au TdT
            $deliberations[$i]['Deliberation']['num_pref_libelle'] =
                $this->ProjetTools->getMatiereByKey($deliberations[$i]['Deliberation']['num_pref']);
            $deliberations[$i]['Deliberation']['num_pref'] =
                str_replace('.', '_', $deliberations[$i]['Deliberation']['num_pref']);
            //On cherche à savoir si la délibération est vérrouillée par un utilisateur
            $deliberations[$i]['Deliberation']['typologiepiece_code_libelle'] =
                $this->Typologiepiece->getTypologieNamebyCode(
                    $deliberations[$i]['Deliberation']['typologiepiece_code']
                );
            foreach ($deliberations[$i]['Annexe'] as &$annexe) {
                if ($docs_type[$annexe['filetype']]['convertir']) {
                    $annexe['filename'] = AppTools::getNameFile($annexe['filename']) . '.pdf';
                }
                $annexe['typologiepiece_code_libelle'] =
                    $this->Typologiepiece->getTypologieNamebyCode($annexe['typologiepiece_code']);
            }
        }

        $seances = $this->Seance->find(
            'all',
            [
            'fields' => ['Seance.id', 'Seance.date'],
            'conditions' => ['Seance.traitee' => 1],
            'recursive' => -1,
                ]
        );

        foreach ($seances as $seance) {
            $toutes_seances[$seance['Seance']['id']] = $seance['Seance']['date'];
        }
        $this->ajouterFiltre($deliberations);

        if (!empty($seance_id)) {
            $this->Filtre->delCritere('DeliberationSeanceId');
            $this->Filtre->delCritere('DeliberationTypeseanceId');
        }

        $this->set(
            'acl',
            [
            'deliberationCancelSendTDT' => $this->Acl->check(
                ['User' => ['id' => $this->Auth->user('id')]],
                'Teletransmettre/deliberationCancelSendToTDT',
                'read'
            ),
            'deliberationCancelSignature' => $this->Acl->check(
                ['User' => ['id' => $this->Auth->user('id')]],
                'Signatures/deliberationCancelSignature',
                'read'
            )
            ]
        ) ;
        $this->set('deliberations', $deliberations);
    }

    /**
     * @access private
     *
     * @param type $conditions
     * @return type
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    private function handleConditions($conditions)
    {
        $projet_type_ids = [];
        $projet_seance_ids = [];

        if (isset($conditions['DeliberationTypeseance.typeseance_id'])) {
            $type_id = $conditions['DeliberationTypeseance.typeseance_id'];
            $typeseances = $this->DeliberationTypeseance->find('all', [
                'conditions' => ['DeliberationTypeseance.typeseance_id' => $type_id
                ],
                'recursive' => -1]);
            foreach ($typeseances as $typeseance) {
                $projet_type_ids[] = $typeseance['DeliberationTypeseance']['deliberation_id'];
            }
            unset($conditions['DeliberationTypeseance.typeseance_id']);
        }
        if (isset($conditions['DeliberationSeance.seance_id'])) {
            $projet_seance_ids = $this->Seance->getDeliberationsId($conditions['DeliberationSeance.seance_id']);
            unset($conditions['DeliberationSeance.seance_id']);
        }
        $result = null;
        if (!empty($projet_type_ids) && !empty($projet_seance_ids)) {
            $result = array_intersect($projet_type_ids, $projet_seance_ids);
        } elseif (empty($projet_type_ids) && empty($projet_seance_ids)) {
            return $conditions;
        } elseif (empty($projet_type_ids)) {
            $result = $projet_seance_ids;
        } elseif (empty($projet_seance_ids)) {
            $result = $projet_type_ids;
        }

        if (isset($conditions['Deliberation.id'])) {
            $conditions['Deliberation.id'] = array_intersect($conditions['Deliberation.id'], $result);
        } elseif (!empty($result)) {
            $conditions['Deliberation.id'] = $result;
        }

        return ($conditions);
    }

    /**
     * @version 5.1
     * @since 4.3
     * @access private
     * @param type $projets
     */
    private function ajouterFiltre(&$projets)
    {
        if (!$this->Filtre->critereExists() && !empty($projets)) {
            //FIX
//            $subQuery = $this->Deliberation->sql($sousRequete);
//
//            $subQuery = ' "' . $this->Deliberation->alias . '"."id" IN (' . $subQuery . ') ';
//            $subQueryExpression = $db->expression($subQuery);
//            $conditions[] = $subQueryExpression;
            //FIX
            $conditions[] = 'Seance.id IN ('
                    . 'SELECT DISTINCT deliberations_seances.seance_id'
                    . ' FROM deliberations_seances '
                    . ' INNER JOIN deliberations  ON ( deliberations.id=deliberations_seances.deliberation_id )'
                    . ' INNER JOIN seances  ON ( seances.id=deliberations_seances.seance_id )'
                    . ' INNER JOIN typeseances ON ( typeseances.id=seances.type_id )'
                    . ' INNER JOIN typeactes  ON ( typeactes.id=deliberations.typeacte_id )'
                    . ' WHERE typeactes.teletransmettre = TRUE'
                    . ' )';

            $seances = $this->Seance->find('all', [
                'fields' => ['Seance.id', 'Seance.type_id', 'Seance.date'],
                'conditions' => $conditions,
                //'conditions' => array('Seance.traitee' => 0),
                'contain' => ['Typeseance.libelle', 'Typeseance.retard'],
                'order' => ['Seance.date' => 'DESC', 'Typeseance.libelle' => 'ASC'],
            ]);

            $options_seances = [];
            foreach ($seances as $seance) {
                //Voir tous les projets ou tous les futurs dates avec un delais respecté
                $options_seances[$seance['Seance']['id']] =
                    $seance['Typeseance']['libelle'] . ' : ' . CakeTime::i18nFormat(
                        $seance['Seance']['date'],
                        '%A %e %B %Y'
                    ) . ' à ' . CakeTime::i18nFormat($seance['Seance']['date'], '%k:%M');
            }

            $this->Filtre->addCritere('DeliberationSeanceId', [
                'field' => 'DeliberationSeance_Filter.seance_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des séances'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'label' => __('Séances'),
                    'options' => $options_seances]]);


            $typeseances = [];
            foreach ($projets as $projet) {
                if (!empty($projet['DeliberationTypeseance'])) {
                    foreach ($projet['DeliberationTypeseance'] as $typeseance) {
                        if (!array_key_exists($typeseance['id'], $typeseances)) {
                            $typeseances[$typeseance['Typeseance']['id']] = $typeseance['Typeseance']['libelle'];
                        }
                    }
                }
            }
            $this->Filtre->addCritere('DeliberationTypeseanceId', [
                'field' => 'DeliberationTypeseance_Filter.typeseance_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des séances'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'label' => __('Type de séance'),
                    'options' => $typeseances]]);

            /* $this->Filtre->addCritere('DeliberationSeanceId', array('field' => 'DeliberationSeance.seance_id',
              'classeDiv' => 'demi',
              'retourLigne' => true,
              'inputOptions' => array(
              'label' => __('Séances'),
              'empty' => __('toutes'),
              'options' => $this->Deliberation->getSeancesFromArray($projets)))); */

            $typeactes = $this->Deliberation->Typeacte->find('list', [
                'recursive' => -1,
                'order' => ['Typeacte.name' => 'ASC'],
                'allow' => ['Typeacte.id' => 'read']]);

            $this->Filtre->addCritere('Typeacte', [
                'field' => 'Deliberation.typeacte_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des séances'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'label' => __('Type d\'acte'),
                    'options' => $typeactes
                ]]);
            $themes = $this->Deliberation->Theme->generateTreeListByOrder(
                ['Theme.actif' => '1'],
                '&nbsp;&nbsp;&nbsp;&nbsp;'
            );
            $this->Filtre->addCritere('ThemeId', [
                'field' => 'Deliberation.theme_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'label' => __('Thème'),
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des thèmes'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'options' => $themes,
                    'escape' => false
                ]]);
            $services = $this->Service->find('list', [
                'fields' => ['id', 'name'],
                'order' => ['Service.name' => 'ASC'],
                'conditions' => ['actif' => true], 'recursive' => -1]);
            $this->Filtre->addCritere('ServiceId', [
                'field' => 'Deliberation.service_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'label' => __('Service émetteur'),
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des services éméteur'),
                    'data-allow-clear' => true,
                    'multiple' => 'multiple',
                    'options' => $services,
                ]]);
            $circuits = $this->Circuit->find('list', [
                'fields' => ['id', 'nom'],
                'order' => ['Circuit.nom' => 'ASC'],
                'conditions' => ['actif' => true], 'recursive' => -1]);
            $this->Filtre->addCritere('CircuitId', [
                'field' => 'Deliberation.circuit_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'label' => __('Circuit de validation'),
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des services services'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'options' => $circuits,
                ]]);
        }
    }

    /**
     * @version 4.3
     * @access public
     * @param type $seance_id
     */
    public function signature($seance_id = null)
    {
        $this->Filtre->initialisation(
            $this->name . ':' . $this->action . ':' . $seance_id,
            $this->data,
            ['url' => $this->here]
        );
        $conditions = $this->handleConditions($this->Filtre->conditions());
        $this->set('seance_id', $seance_id);

        //FIX
        $conditions['OR']['Deliberation.parapheur_etat >'] = 0;
        $conditions['OR']['Deliberation.signee'] = true;
        $conditions['OR']['OR']['Deliberation.etat'] = 10;
        $conditions['Deliberation.etat >'] = 2;

        $db = $this->Deliberation->getDataSource();

        $subQuery = [
            'fields' => [
                'DISTINCT Deliberation.id'
            ],
            'contain' => false,
            'joins' => [
                $this->Deliberation->join('DeliberationSeance', ['type' => 'INNER']),
                $this->DeliberationSeance->join('Seance', ['type' => 'INNER']),
                $this->DeliberationSeance->Seance->join('Typeseance', ['type' => 'INNER']),
                $this->Deliberation->join('Typeacte', ['type' => 'INNER']),
            ],
            'conditions' => [
                'Typeseance.action' => 0,
            //'Typeacte.teletransmettre' => true
            ]
        ];

        $subQuery = $this->Deliberation->sql($subQuery);

        $subQuery = ' "' . $this->Deliberation->alias . '"."id" IN (' . $subQuery . ') ';
        $subQueryExpression = $db->expression($subQuery);
        $conditions[] = $subQueryExpression;

        $this->paginate = ['Deliberation' => [
                'countField' => 'DISTINCT Deliberation.id',
                'fields' => ['DISTINCT Deliberation.id',
                    'Deliberation.objet_delib',
                    'Deliberation.num_delib',
                    'Deliberation.titre',
                    'Deliberation.etat',
                    'Deliberation.circuit_id',
                    'Deliberation.parapheur_etat',
                    'Deliberation.parapheur_bordereau',
                    'Deliberation.parapheur_commentaire',
                    'Deliberation.num_pref',
                    'Deliberation.signee',
                    'Deliberation.signature',
                    'Deliberation.tdt_status',
                    'Deliberation.typeacte_id',
                    'Deliberation.theme_id',
                    'Deliberation.service_id',
                    'Deliberation.date_acte',
                    'Deliberation.signature_type',
                    'Deliberation.vote_resultat',
                    'Deliberation.signature_date'

                ],
                'conditions' => $conditions,
                'joins' => $this->handleJoins(),
                'contain' => [
                    'Service.name',
                    'Theme.libelle',
                    'Typeacte.name',
                    'Circuit.nom',
                    'DeliberationTypeseance' => [
                        'fields' => ['id'],
                        'Typeseance' => [
                            'fields' => ['id', 'libelle', 'action'],
                        ]
                    ],
                    'DeliberationSeance' => [
                        'fields' => ['id'],
                        'Seance' => [
                            'fields' => ['id', 'date', 'type_id'],
                            'Typeseance' => [
                                'fields' => ['id', 'libelle', 'action']
                            ]]]],
                'recursive' => -1,
                'limit' => 20,
                'order' => ['Deliberation.date_acte' => 'DESC']]];

        $delibs = $this->Paginator->paginate('Deliberation');
        $this->ajouterFiltre($delibs);

        $droits = $this->ProjetTools->getDroits([
            'Projets' => ['read', 'update', 'delete'],
            'autresActesAEnvoyer' => 'read',
            'editerTous' => 'read',
            'editPostSign' => 'read',
            'toSend' => 'read',
        ]);
        for ($i = 0; $i < count($delibs); $i++) {
            $this->ProjetTools->actionDisponible($delibs[$i], $droits);
            $delibs[$i]['Deliberation'][$delibs[$i]['Deliberation']['id'] . '_num_pref'] =
                $delibs[$i]['Deliberation']['num_pref'];
            $delibs[$i]['Deliberation']['num_pref_libelle'] =
                $this->ProjetTools->getMatiereByKey($delibs[$i]['Deliberation']['num_pref']);
        }

        $this->set(
            'acl',
            [
                'deliberationCancelSignature'=> $this->Acl->check(
                    ['User' => ['id' => $this->Auth->user('id')]],
                    'Signatures/deliberationCancelSignature',
                    'read'
                )
            ]
        ) ;

        $this->set('circuits', $this->Signatures->getCircuits());
        $this->set('deliberations', $delibs);
        $this->set('signature', true);
    }

    /**
     * @access private
     * @param type $conditions
     * @return type
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    private function handleJoins()
    {
        $joins = [
            ['table' => 'deliberations_seances',
                'alias' => 'DeliberationSeance_Filter',
                'type' => 'LEFT',
                'conditions' => [
                    'Deliberation.id = DeliberationSeance_Filter.deliberation_id',
                ]
            ],
            ['table' => 'seances',
                'alias' => 'Seance_Filter',
                'type' => 'LEFT',
                'conditions' => [
                    'DeliberationSeance_Filter.seance_id = Seance_Filter.id'
                ]
            ],
            ['table' => 'deliberations_typeseances',
                'alias' => 'DeliberationTypeseance_Filter',
                'type' => 'LEFT',
                'conditions' => [
                    'Deliberation.id = DeliberationTypeseance_Filter.deliberation_id'
                ]
            ],
        ];

        return $joins;
    }
}
