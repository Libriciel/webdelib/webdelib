<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppController', 'Controller');
App::uses('VoteService', 'Lib/Service');
App::uses('AppTools', 'Lib');

/**
 * Class VotesController
 *
 * @package app.Controller
 */
class VotesController extends AppController
{
    /**
     * [public description]
     * @var [type]
     */
    public $uses = [
        'Deliberation',
        'Seance',
        'DeliberationSeance',
        'Typeseance',
        'Acteur',
        'Listepresence',
        'ListePresenceGlobale',
        'Vote',
        'Historique'
    ];
    /**
     * [public description]
     * @var [type]
     */
    public $components = [
        'RequestHandler',
        'SeanceTools',
        'Idelibre',
        'Progress',
        'Auth' => [
            'mapActions' => [
                'create' => ['add'],
                'read' => [ 'index'],
                'downloadVotesElectroniqueJson',
                'sendSeanceVotesToIdelibre',
                //'importVotesIdelibre',
                'update' => [
                    'edit',
                    'saisirDebat',
                    'deleteDebat',
                    'copyFromPrevious',
                    'listerPresents',
                    'listerPresencesGlobale',
                ],
                'delete' => ['delete'],
            ],
        ]];

    /**
     * @version 4.3
     * @access public
     */
    public function beforeFilter()
    {
        $this->VoteService = new VoteService();
        parent::beforeFilter();
    }

    /**
     * Liste des projets en commission
     *
     * @access public
     *
     * @param Integer $id
     *
     * @version 5.1.0
     * @since 1.0.0
     */
    public function index($id = null)
    {
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($id, 'update');

        $this->set('seance_id', $id);

        $seance = $this->Seance->find(
            'first',
            [
                'fields' => [
                    'Seance.type_id',
                    'Seance.idelibre_id',
                    'Seance.idelibre_votes_warning'
                ],
                'conditions' => ['Seance.id' => $id],
                'contain' => ['Typeseance.libelle', 'Typeseance.action'],
                'recursive' => -1
            ]
        );

        $this->VoteService->afficherListePresentsGlobale($id);

        $projets = $this->Deliberation->find('all', [
            'fields' => [
                'Deliberation.id',
                'Deliberation.objet',
                'Deliberation.objet_delib',
                'Deliberation.etat',
                'Deliberation.signee',
                'Deliberation.titre',
                'Deliberation.date_limite',
                'Deliberation.anterieure_id',
                'Deliberation.num_pref',
                'Deliberation.rapporteur_id',
                'Deliberation.redacteur_id',
                'Deliberation.circuit_id',
                'Deliberation.typeacte_id',
                'Deliberation.theme_id',
                'Deliberation.date_acte',
                'Deliberation.tdt_ar_date',
                'Deliberation.service_id',
                'Deliberation.num_delib',
                'Deliberation.parapheur_etat',
                'Theme.libelle',
                'Theme.order',
                'DeliberationSeance.position'
            ],
            'contain' => [
                'Theme' => ['fields' => ['order', 'libelle']],
                'Typeacte' => [
                    'fields' => ['name'],
                    'Nature' => ['fields' => ['code']]
                ],
            ],
            'joins' => [
                $this->Deliberation->join('DeliberationSeance', ['type' => 'INNER'])
            ],
            'conditions' => [
                'DeliberationSeance.seance_id' => $id,
                'Deliberation.etat >=' => 0
            ],
            'order' => 'DeliberationSeance.position ASC',
            'recursive' => -1
        ]);

        $permissionsCheck = [];
        if ($this->Idelibre->isUsed()
            && !empty($projets)) {
            $permissionsCheck[] = 'btnImportVotesIdelibreActif';
        }
        $aPosition=[];
        for ($i = 0, $iMax = count($projets); $i < $iMax; $i++) {
            $projets[$i]['Deliberation']['is_delib'] =
                $this->Deliberation->isDeliberant($projets[$i]['Deliberation']['id']);
            $projets[$i]['Modeltemplate']['id'] =
                $this->Typeseance->modeleProjetDelibParTypeSeanceId(
                    $seance['Seance']['type_id'],
                    $projets[$i]['Deliberation']['etat']
                );

            $btn_actions = ['saisirDebat'];
            if ($seance['Typeseance']['action'] != 2
                && (
                    empty($projets[$i]['Deliberation']['parapheur_etat'])
                    or $projets[$i]['Deliberation']['parapheur_etat']==-1
                )
                && empty($projets[$i]['Deliberation']['signee'])
            ) {
                $btn_actions[] = 'genereFusionToClient';
                if ($projets[$i]['Deliberation']['etat'] > 2) {
                    $btn_actions[] = 'resetVote';
                    unset($permissionsCheck['btnImportVotesIdelibreActif']);
                }
                $btn_actions[] = 'voter';
            } else {
                $btn_actions[] = 'genereFusionToClientByModelTypeNameAndSeanceId';
            }

            $aPosition[$projets[$i]['DeliberationSeance']['position']] = $projets[$i]['DeliberationSeance']['position'];

            $projets[$i]['btn_actions'] = $btn_actions;
        }

        if (!empty($seance['Seance']['idelibre_id'])) {
            if (empty($seance['Seance']['idelibre_votes_warning'])) {
                $this->set('seanceIdelibreVotesEtat', 1);
            } else {
                $idelibreVotesWarning = json_decode(
                    $seance['Seance']['idelibre_votes_warning'],
                    true,
                    512,
                    JSON_THROW_ON_ERROR
                );
                $this->set('idelibreVotesWarning', $idelibreVotesWarning);
                $this->set('seanceIdelibreVotesEtat', (count($idelibreVotesWarning)>0 ? -1 : 2));
            }
        }

        $this->set('permissionsCheck', $permissionsCheck);
        $this->set('aPosition', $aPosition);
        $this->set('seance', $seance);
        $this->set('date_seance', $this->Seance->getDate($id));
        $this->SeanceTools->afficheProjets('index', $projets);
    }

    /**
     * Liste des délibérations à voter en séance
     *
     * @access public
     *
     * @param int $deliberation_id
     * @param int $seance_id
     *
     * @version 5.1.0
     * @since 1.0.0
     */
    public function edit($deliberation_id, $seance_id)
    {
        $this->Deliberation->Behaviors->load(
            'Version',
            $this->Deliberation->actsAsVersionOptionsList['Version']
        );

        //FIX Sens des variables $deliberation_id<=>$seance_id et changement $seance_id => $id
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($seance_id, 'update');

        $deliberation = $this->Deliberation->find('first', [
            'conditions' => ['Deliberation.id' => $deliberation_id],
            'recursive' => -1
        ]);

        $seance = $this->Seance->find('first', [
            'conditions' => ['Seance.id' => $seance_id],
            'fields' => ['id', 'date', 'president_id', 'type_id'],
            'contain' => ['Typeseance.compteur_id','Typeseance.libelle'],
            'recursive' => -1
        ]);

        if ($this->request->is('Post') || $this->request->is('Put')) {
            $this->request->data['Deliberation']['id'] = $deliberation_id;

            $this->Deliberation->id = $deliberation_id;
            //Gestion du droit des types de séance
            $this->SeanceTools->checkTypeSeance($seance_id, 'delete');
            $this->Vote->deleteAll(['delib_id' => $deliberation_id], false);
            switch ($this->data['Vote']['typeVote']) {
                case 1://Détails des voix
                    $this->request->data['Deliberation']['vote_nb_oui'] = 0;
                    $this->request->data['Deliberation']['vote_nb_non'] = 0;
                    $this->request->data['Deliberation']['vote_nb_abstention'] = 0;
                    $this->request->data['Deliberation']['vote_nb_retrait'] = 0;
                    if (!empty($this->data['detailVote'])) {
                        foreach ($this->data['detailVote'] as $acteur_id => $vote) {
                            $this->Vote->create();
                            $this->request->data['Vote']['acteur_id'] = $acteur_id;
                            $this->request->data['Vote']['delib_id'] = $deliberation_id;
                            $this->request->data['Vote']['resultat'] = $vote;
                            $this->Vote->save($this->data['Vote']);

                            if ($vote == 3) {
                                $this->request->data['Deliberation']['vote_nb_oui'] ++;
                            } elseif ($vote == 2) {
                                $this->request->data['Deliberation']['vote_nb_non'] ++;
                            } elseif ($vote == 4) {
                                $this->request->data['Deliberation']['vote_nb_abstention'] ++;
                            } elseif ($vote == 5) {
                                $this->request->data['Deliberation']['vote_nb_retrait'] ++;
                            }
                        }
                    }

                    if ($this->data['Deliberation']['vote_nb_oui'] > $this->data['Deliberation']['vote_nb_non']) {
                        $this->request->data['Deliberation']['etat'] = 3;
                        $this->request->data['Deliberation']['vote_resultat'] = true;
                    } else {
                        $this->request->data['Deliberation']['etat'] = 4;
                        $this->request->data['Deliberation']['vote_resultat'] = false;
                    }
                    $this->request->data['Deliberation']['vote_prendre_acte'] = null;
                    break;

                case 2://Total des voix
                    // Saisie du total du vote
                    if ($this->data['Deliberation']['vote_nb_oui'] > $this->data['Deliberation']['vote_nb_non']) {
                        $this->request->data['Deliberation']['etat'] = 3;
                        $this->request->data['Deliberation']['vote_resultat'] = true;
                    } else {
                        $this->request->data['Deliberation']['etat'] = 4;
                        $this->request->data['Deliberation']['vote_resultat'] = false;
                    }
                    $this->request->data['Deliberation']['vote_prendre_acte'] = null;
                    break;

                case 3://Résultat
                    $this->request->data['Deliberation']['vote_nb_oui'] = 0;
                    $this->request->data['Deliberation']['vote_nb_non'] = 0;
                    $this->request->data['Deliberation']['vote_nb_abstention'] = 0;
                    $this->request->data['Deliberation']['vote_nb_retrait'] = 0;
                    $this->request->data['Deliberation']['vote_prendre_acte'] = null;
                    $this->request->data['Deliberation']['vote_resultat'] =
                        isset($this->request->data['Deliberation']['etat']) ?
                            ($this->request->data['Deliberation']['etat'] == 3 ? true : false)
                            : null;
                    break;

                case 4://Prendre Acte
                    // Saisie du total du vote: prend pour acte
                    if ($this->data['Deliberation']['vote_prendre_acte'] == true) {
                        $this->request->data['Deliberation']['etat'] = 3;
                        $this->request->data['Deliberation']['vote_resultat'] = true;
                    } else {
                        $this->request->data['Deliberation']['etat'] = 4;
                        $this->request->data['Deliberation']['vote_resultat'] = false;
                    }
                    break;
                default:
                    break;
            }

            $createDelibNumber= false;
            // Attribution du numéro de la délibération si pas déjà attribué
            if (empty($deliberation['Deliberation']['num_delib'])) {
                $createDelibNumber= true;
                $this->request->data['Deliberation']['num_delib'] =
                    $this->Seance->Typeseance->Compteur->genereCompteur(
                        $seance['Typeseance']['compteur_id'],
                        $seance['Seance']['date']
                    );
                $this->request->data['Deliberation']['num_delib'] = str_replace(
                    '#p#',
                    $this->Deliberation->getPosition($deliberation_id, $seance_id),
                    $this->request->data['Deliberation']['num_delib']
                );
            }

            //Retour vers la liste des projets
            if ($this->Deliberation->save($this->data['Deliberation'])) {
                if ($this->request->data['Deliberation']['vote_resultat']) {
                    $this->Historique->enregistre(
                        $deliberation_id,
                        $this->Auth->user('id'),
                        __('Le projet est adopté')
                    );
                } else {
                    $this->Historique->enregistre(
                        $deliberation_id,
                        $this->Auth->user('id'),
                        __('Le projet est rejeté')
                    );
                }
                if ($createDelibNumber) {
                    $this->Historique->enregistre(
                        $deliberation_id,
                        $this->Auth->user('id'),
                        __("Génération du numéro d'acte")
                    );
                }
                $this->redirect(['controller' => 'votes', 'action' => 'index', $seance_id]);
            }
        }
        //Initialisation président de séance
        $acteursConvoques = $this->Seance->Typeseance->acteursConvoquesParTypeSeanceId(
            $seance['Seance']['type_id'],
            true,
            ['id', 'nom', 'prenom']
        );

        $tab = [];
        if (!empty($acteursConvoques)) {
            foreach ($acteursConvoques as $acteurConvoque) {
                $tab[$acteurConvoque['Acteur']['id']] =
                    $acteurConvoque['Acteur']['prenom'] . ' ' . $acteurConvoque['Acteur']['nom'];
            }
        }

        $this->set('acteurs', $tab);

        if (empty($deliberation['Deliberation']['president_id'])) {
            $this->set('selectedPresident', $seance['Seance']['president_id']);
        } else {
            $this->set('selectedPresident', $deliberation['Deliberation']['president_id']);
        }

        $nbAbsent = 0;
        // Initialisation du détail du vote
        $donnees = $this->Vote->find('all', [
            'conditions' => ['Vote.delib_id' => $deliberation_id],
            'recursive' => -1]);

        foreach ($donnees as $donnee) {
            if (!empty($donnee['Vote']['resultat'])) {
                $this->request->data['detailVote'][$donnee['Vote']['acteur_id']] = $donnee['Vote']['resultat'];
            }
        }

        // Initialisation du total des voix
        $this->request->data['Deliberation']['vote_nb_oui'] = $deliberation['Deliberation']['vote_nb_oui'];
        $this->request->data['Deliberation']['vote_nb_non'] = $deliberation['Deliberation']['vote_nb_non'];
        $this->request->data['Deliberation']['vote_nb_abstention'] =
            $deliberation['Deliberation']['vote_nb_abstention'];
        $this->request->data['Deliberation']['vote_nb_retrait'] =
            $deliberation['Deliberation']['vote_nb_retrait'];
        $this->request->data['Deliberation']['vote_prendre_acte'] =
            $deliberation['Deliberation']['vote_prendre_acte'];

        // Initialisation du resultat
        $this->request->data['Deliberation']['etat'] = $deliberation['Deliberation']['etat'];

        // Initialisation du commentaire
        $this->request->data['Deliberation']['vote_commentaire'] =
            $deliberation['Deliberation']['vote_commentaire'];

        $this->set('seance', $seance);
        $this->set('deliberation', $deliberation);

        $listPresents = $this->VoteService->afficherListePresents($deliberation_id, $seance_id);
        $typeacteurs = [];

        foreach ($listPresents as $present) {
            $typeacteurs[$present['Acteur']['Typeacteur']['id']] = $present['Acteur']['Typeacteur']['nom'];
        }

        $this->set('typeacteurs', $typeacteurs);
        $this->set('presents', $listPresents);

        $nbPresent = count($listPresents);
        foreach ($listPresents as $present) {
            if (empty($present['Listepresence']['present']) && empty($present['Listepresence']['mandataire'])) {
                $nbAbsent++;
            } else {
                $nbPresent++;
            }
        }

        if ($nbPresent / 2 < $nbAbsent) {
            $this->set('message', __('Attention, le quorum n\'est plus atteint...'));
        }
        //Type de vote
        if (!empty($this->request->data['Deliberation']['vote_prendre_acte'])) {
            $this->request->data['Vote']['typeVote'] = 4;
        } elseif (!empty($this->request->data['detailVote'])) {
            $this->request->data['Vote']['typeVote'] = 1;
        } elseif (empty($this->request->data['detailVote'])
            && !empty($this->request->data['Deliberation']['vote_nb_oui'])
        ) {
            $this->request->data['Vote']['typeVote'] = 2;
        } elseif (empty($this->request->data['detailVote'])
            && empty($this->request->data['Deliberation']['vote_nb_oui'])
            && in_array($this->request->data['Deliberation']['etat'], [3, 4], true)
        ) {
            $this->request->data['Vote']['typeVote'] = 3;
        }
    }

    /**
     * Suppression du vote
     *
     * @access public
     *
     * @param int $seance_id
     * @param int $projet_id
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function delete($id, $projet_id)
    {
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($id, 'delete');

        try {
            $this->Deliberation->resetVote($projet_id, $id);
            $this->Flash->set(
                __('Le vote du projet a été supprimé'),
                ['element' => 'growl','params'=> ['type' => 'success']]
            );
        } catch (Exception $e) {
            $this->Flash->set($e->getMessage(), ['element' => 'growl','params'=>['type' => 'danger']]);
        }

        $this->redirect($this->previous);
    }

    /**
     * Lister les acteurs présent pour la séance
     *
     * @access public
     *
     * @param int $delib_id
     * @param int $seance_id
     * @return void
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function listerPresents($delib_id, $seance_id)
    {
        //FIX Sens des variables $deliberation_id<=>$seance_id et changement $seance_id => $id
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($seance_id, 'update');

        if ($this->request->is('Post') || $this->request->is('Put')) {
            $this->updateAttendanceList($delib_id);

            //if ($nbVoix < ($nbConvoques/2)) {
            //   $this->reporteDelibs($delib_id);
            // }
            return $this->redirect(['controller' => 'votes', 'action' => 'edit', $delib_id, $seance_id]);
        }

        $liste_presents = $this->VoteService->getAttendanceList($delib_id);

        //Pour sélectionner les acteurs
        foreach ($liste_presents as $present) {
            $this->request->data[$present['Listepresence']['acteur_id']]['present'] =
                $present['Listepresence']['present'];
            $this->request->data[$present['Listepresence']['acteur_id']]['mandataire'] =
                $present['Listepresence']['mandataire'];
        }

        $this->set('liste_presents', $liste_presents);
        $this->set('mandataires', $this->Acteur->generateListElus());
        $this->set('delib_id', $delib_id);
        $this->set('seance_id', $seance_id);
    }

    /**
     * Lister les acteurs présent pour la séance
     *
     * @access public
     *
     * @param int $delib_id
     * @param int $seance_id
     * @return void
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function listerPresencesGlobale($id)
    {
        //FIX Sens des variables $deliberation_id<=>$seance_id et changement $seance_id => $id
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($id, 'update');

        if ($this->request->is('Post') || $this->request->is('Put')) {
            $this->updateAttendanceList($id, true);

            return $this->redirect(['controller' => 'votes', 'action' => 'index', $id]);
        }

        $liste_presents = $this->VoteService->getAttendanceList($id, true);
//var_dump($liste_presents);
        // Pour sélectionner les acteurs
        foreach ($liste_presents as $present) {
            $this->request->data[$present['ListePresenceGlobale']['acteur_id']]['present'] =
                $present['ListePresenceGlobale']['present'];
            $this->request->data[$present['ListePresenceGlobale']['acteur_id']]['mandataire'] =
                $present['ListePresenceGlobale']['mandataire'];
        }
        $this->set('liste_presents', $liste_presents);
        $this->set('mandataires', $this->Acteur->generateListElus());
        $this->set('seance_id', $id);
    }

    /**
     * copyFromPrevious
     *
     * @access public
     *
     * @param int $delib_id
     * @param int $seance_id
     * @return void
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function copyFromPrevious($delib_id, $seance_id)
    {
        //FIX commentaire
        //FIX Sens des variables $deliberation_id<=>$seance_id et changement $seance_id => $id
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($seance_id, 'update');

        $this->Listepresence->deleteAll(['delib_id' => $delib_id], false);
        $this->VoteService->copyFromPreviousList($delib_id, $seance_id);
        $this->Flash->set(__('Récupération de la délibération précédente effectuée.'), ['element' => 'growl']);

        return $this->redirect(['controller' => 'votes', 'action' => 'edit', $delib_id, $seance_id]);
    }

    /**
     * [downloadVotesElectroniqueJson description]
     * @param  [type] $seanceId [description]
     * @return [type]           [description]
     */
    public function downloadVotesElectroniqueJson($seanceId)
    {
        $seance = $this->Seance->find('first', [
            'fields' => ['Seance.id', 'Seance.type_id', 'Seance.date'],
            'contain' => ['Typeseance.libelle'],
            'conditions' => ['Seance.id' => $seanceId],
            'recursive' => -1
        ]);

        $nameSeance = __(
            '%s du %s à %s',
            $seance['Typeseance']['libelle'],
            CakeTime::i18nFormat($seance['Seance']['date'], '%A %e %B %Y'),
            CakeTime::i18nFormat($seance['Seance']['date'], '%kh%M')
        );

        $json = [
            'application' => 'webdelib',
            'version' => '1.0.0',
            'seance' => [
                'type' => $seance['Typeseance']['libelle'],
                'date' => $seance['Seance']['date'],
                'nom' => $nameSeance
            ],
            'acteurs' => [],
            'projets' => []
        ];

        $acteurs = $this->Typeseance->acteursConvoquesParTypeSeanceId($seance['Seance']['type_id'], true);
        foreach ($acteurs as $acteur) {
            $suppleant = $this->Acteur->find('first', [
                'fields' => ['id', 'nom', 'prenom','salutation','position', 'titre'],
                'conditions' => ['Acteur.id' => $acteur['Acteur']['suppleant_id'],
                    'Acteur.actif' => true],
                'order' => 'nom ASC',
                'recursive' => -1
            ]);
            if (!empty($suppleant)) {
                $suppleant = [
                    'identifiant' => $suppleant['Acteur']['id'],
                    'salutation' => $suppleant['Acteur']['salutation'],
                    'nom' => $suppleant['Acteur']['nom'],
                    'prenom' => $suppleant['Acteur']['prenom'],
                    'titre' => $suppleant['Acteur']['titre'],
                ];
            }
            $json['acteurs'][] = [
                'identifiant' => $acteur['Acteur']['id'],
                'position' => $acteur['Acteur']['position'],
                'salutation' => $acteur['Acteur']['salutation'],
                'nom' => $acteur['Acteur']['nom'],
                'prenom' => $acteur['Acteur']['prenom'],
                'titre' => $acteur['Acteur']['titre'],
                'type' => [
                    'nom' => $acteur['Typeacteur']['nom']
                ],
                'suppleants' => $suppleant

            ];
        }

        $projets = $this->Deliberation->find('all', [
            'fields' => [
                'Deliberation.id',
                'Deliberation.objet',
                'Deliberation.etat',
                'Deliberation.rapporteur_id',
                'Deliberation.redacteur_id',
                'Deliberation.num_delib',
                'Deliberation.titre',
                'Theme.libelle',
                'DeliberationSeance.position'
            ],
            'contain' => [
                'Redacteur' => ['fields' => ['id', 'nom', 'prenom']],
                'Rapporteur' => ['fields' => ['id', 'nom', 'prenom']],
                'Theme' => ['fields' => ['order', 'libelle']],
                'Service' => ['fields' => ['id', 'order', 'name']],
                'Typeacte' => [
                    'fields' => ['name'],
                    'Nature' => ['fields' => ['code']]
                ],
            ],
            'joins' => [$this->Deliberation->join('DeliberationSeance', ['type' => 'INNER'])],
            'conditions' => ['DeliberationSeance.seance_id' => $seanceId, 'Deliberation.etat >=' => 0],
            'recursive' => -1,
            'order' => ['DeliberationSeance.position' => 'ASC']
        ]);

        foreach ($projets as $projet) {
            $json['projets'][] = [
                'identifiant' => $projet['Deliberation']['id'],
                'ordre' => $projet['DeliberationSeance']['position'],
                'theme' => $projet['Theme']['libelle'],
                'nom' => $projet['Deliberation']['objet'],
                'titre' => $projet['Deliberation']['titre'],
                'service' => $projet['Service']['name'],
                'redacteur' => __('%s %s;', $projet['Redacteur']['nom'], $projet['Redacteur']['prenom']),
                'rapporteur' => __('%s %s;', $projet['Rapporteur']['nom'], $projet['Rapporteur']['prenom'])

            ];
        }

        $this->response->disableCache();
        $this->response->body(
            json_encode(
                $json,
                JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_INVALID_UTF8_IGNORE
            )
        );
        $this->response->type('application/json');
        $this->response->download(__('Export de la séance: %s', $nameSeance) . '.json');

        return $this->response;
    }

    /**
     * Envoi d'une séance et ses projets à idelibre
     *
     * @version 5.1
     * @since 4.3
     * @access public
     * @param integer $seance_id
     * @return mixed
     * @throws Exception
     */
    public function sendSeanceVotesToIdelibre($seance_id, $cookieTokenKey)
    {
        try {
            if (!$this->Idelibre->isUsed()) {
                throw new Exception(__('Le connecteur idelibre n\'est pas activé.'));
            }

            // Initialisation des paramètres curl
            $url = Configure::read('IDELIBRE_HOST') . '/seances.json';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);

            if (Configure::read('IDELIBRE_USEPROXY')) {
                curl_setopt($ch, CURLOPT_PROXY, Configure::read('IDELIBRE_PROXYHOST'));
            }

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

            $this->Progress->start($cookieTokenKey);
            $this->Progress->at(0, __('Initialisation'));
            $this->Progress->at(1, __('Récupération des données de la séance...'));

            $seance = $this->Seance->find('first', [
                'conditions' => ['Seance.id' => $seance_id],
                'fields' => ['id', 'date', 'type_id'],
                'contain' => [
                    'Typeseance.libelle',
                    'Typeseance.action',
                    'Typeseance.id',
                    'Typeseance.modele_convocation_id',
                    'Typeseance.modele_projet_id',
                    'Typeseance.action'],
                'recursive' => -1
            ]);

            $acteurs_convoques =
                $this->Seance->Typeseance->acteursConvoquesParTypeSeanceId($seance['Typeseance']['id']);
            if (empty($acteurs_convoques)) {
                throw new Exception(__('Il n\'y a pas d\'acteur à convoquer.'));
            }
            foreach ($acteurs_convoques as $acteur) {
                if (empty($acteur['Acteur']['email'])) {
                    throw new Exception(
                        __('Les emails des acteurs doivent être renseignés pour l\'envoi à idelibre.')
                    );
                }
            }

            $data = [
                'username' => Configure::read('IDELIBRE_LOGIN'),
                'password' => Configure::read('IDELIBRE_PWD'),
                'conn' => Configure::read('IDELIBRE_CONN')
            ];

            $tmpDirSeance = new Folder(AppTools::newTmpDir(
                AppTools::newTmpDir(TMP . 'files' . DS . 'idelibre' . DS),
            ));

            $this->Progress->at(5, __("Génération de la convocation à la séance..."));

            // fusion de la convocation
            $file_convocation = new File(
                $this->Seance->fusionToFile(
                    $seance_id,
                    'convocation',
                    $seance['Typeseance']['modele_convocation_id'],
                    $tmpDirSeance->path
                )
            );
            $data['convocation'] = curl_file_create(
                $file_convocation->pwd(),
                'application/pdf',
                $file_convocation->name
            );
            CakeLog::write('idelibre', __('send convocation file %s', $file_convocation->name));
            $file_convocation->close();

            $jsonData = [
                'date_seance' => $seance['Seance']['date'],
                'type_seance' => $seance['Typeseance']['libelle'],
                'acteurs_convoques' => json_encode($acteurs_convoques),
            ];

            $this->Progress->at(10, __('Récupération des délibérations de la séance...'));
            $i = 0;
            $delibs = $this->Seance->getDeliberationsId($seance_id);
            $num_delib = count($delibs);
            $this->Seance->clear();
            $this->Deliberation->clear();

            foreach ($delibs as $delib_id) {
                $tmpDir = new Folder($tmpDirSeance->path . DS . $delib_id, true);
                $projet = [];
                $this->Progress->at(
                    10 + $i * (50 / $num_delib),
                    __('Génération du projet %s/%s...', $i, $num_delib)
                );
                $delib = $this->Deliberation->find('first', [
                    'fields' => [
                        'Deliberation.objet',
                        'Deliberation.objet_delib',
                        'Deliberation.typeacte_id',
                        'Deliberation.theme_id',
                        'Deliberation.etat'
                    ],
                    'contain' => [
                        'Theme.libelle',
                        'Theme.order',
                        'Rapporteur' => [
                            'fields' => ['nom', 'prenom']
                        ]
                    ],
                    'conditions' => ['Deliberation.id' => $delib_id],
                    'recursive' => -1
                ]);

                $projet['Rapporteur']['rapporteurlastname'] = $delib['Rapporteur']['nom'];
                $projet['Rapporteur']['rapporteurfirstname'] = $delib['Rapporteur']['prenom'];

                $projet['libelle'] = $delib['Deliberation']['objet_delib'];
                $projet['ordre'] = $i;
                $projet['theme'] = implode(
                    ',',
                    array_reverse($this->Deliberation->Theme->getLibelleParent($delib['Deliberation']['theme_id']))
                );

                // fusion du rapport
                $keyRapport = 'projet_' . $i . '_rapport';
                $projet_filename = new File(
                    $this->Deliberation->fusionToFile(
                        $delib_id,
                        'rapport',
                        $seance['Typeseance']['modele_projet_id'],
                        $tmpDir->path,
                        'pdf'
                    )
                );
                $data[$keyRapport] = curl_file_create(
                    $projet_filename->pwd(),
                    'application/pdf',
                    $projet_filename->name
                );
                CakeLog::write('idelibre', __('send projet file %s (%s)', $projet_filename->name, $keyRapport));
                $projet_filename->close();

                $j = 1;

                // annexes
                // IDELIBRE_APPENDIX_FUSION => Retirer les annexes
                $annexes = (Configure::read('IDELIBRE_APPENDIX_FUSION') == false) ?
                    $this->Deliberation->Annexe->getAnnexesFromDelibId($delib_id) :
                    null;

                $annexesToSend = [];

                foreach ($annexes as $annex) {
                    $keyAnnexe='projet_' . $i . '_' . $j . '_annexe';
                    $file = new File($tmpDir->path . DS . $annex['Annexe']['filename'], true);
                    $file->write($annex['Annexe']['data']);
                    $data[$keyAnnexe] = curl_file_create($file->pwd(), 'application/pdf', $file->name);
                    CakeLog::write('idelibre', __('send annexe file %s (%s)', $file->name, $keyAnnexe));
                    $annexesToSend[] = [
                        'libelle' => $annex['Annexe']['titre'],
                        'ordre' => $j
                    ];
                    $file->close();
                    $j++;
                }
                $projet['annexes'] = $annexesToSend;
                $jsonData['projets'][] = $projet;
                $this->Deliberation->clear();
                $i++;
            }
            // Encodage en json
            $data['jsonData'] = json_encode($jsonData);

            $this->Progress->at(85, __('Envoi des informations à idelibre...'));

            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            // Exécution de la requête
            $retour = curl_exec($ch);
            // Décodage en tableau du retour
            $retour = json_decode($retour, true);
            // Fermeture de la connexion
            curl_close($ch);
            // Log
            CakeLog::write(
                'idelibre',
                'Taille totale : ' . AppTools::humanFilesize(
                    $tmpDirSeance->dirsize(),
                    4
                )
            );

            // Suppression du dossier d'envoi temporaire
            $tmpDirSeance->delete();

            if (!empty($retour)) {
                CakeLog::write('idelibre', var_export($retour, true));
            } else {
                CakeLog::write('idelibre', 'Aucune réponse : ' . curl_error($ch));
            }

            if (empty($retour)) {
                throw new Exception(__('Erreur de communication avec idelibre.'));
            } elseif (!empty($retour['error'])) {
                if (!empty($retour['name'])) {
                    throw new Exception(preg_replace('/\s/', ' ', $retour['name']));
                } else {
                    throw new Exception(__('L\'export vers idelibre a échoué.'));
                }
            } elseif (isset($retour['success'])) {
                if ($retour['success']===false) {
                    throw new Exception(__('[%s] %s', $retour['code'], $retour['message']));
                }
                if (!empty($retour['uuid'])) {
                    $this->Seance->id = $seance_id;
                    $this->Seance->saveField('idelibre_id', $retour['uuid']);
                    $this->Progress->redirect(['controller' => 'seances', 'action' => 'index']);
                    $this->Flash->set(
                        __('Séance envoyée avec succès à idelibre'),
                        ['element' => 'growl','params'=> ['type' => 'success']]
                    );
                    $this->Progress->stop(__('Séance envoyée avec succès à idelibre'));
                } else {
                    throw new Exception(__('Impossible de récupérer l\'identifiant idelibre de la séance.'));
                }
            } else {
                throw new Exception(__('Réponse idelibre incorrecte'));
            }
        } catch (Exception $e) {
            CakeLog::error("idelibre: {$e->getMessage()}");
            $this->Progress->error(
                new Exception(
                    $e->getMessage() . " \n " . __('Veuillez contacter votre administrateur.'),
                    $e->getCode()
                )
            );
        }

        $this->autoRender = false;
        $this->response->type(['json' => 'text/x-json']);
        $this->RequestHandler->respondAs('json');
        $this->response->body(json_encode($this->Session->read('Progress.Tokens')));
        return $this->response;
    }

//    public function importVotesIdelibre($seanceId)
//    {
//        try {
//            if ($this->Idelibre->processingSaveImportResults($seanceId)) {
//                $this->Flash->set(
//                    __('Les résultats de la séance ont été récupérés'),
//                    ['element' => 'growl','params'=> ['type' => 'success']]
//                );
//            } else {
//                $this->Flash->set(
//                    __('Erreurs lors de la récupération des résultats de la séance'),
//                    ['element' => 'growl','params'=> ['type' => 'danger']]
//                );
//            }
//        } catch (CakeException $e) {
//            $this->Flash->set(
//                __('Erreur lors de la récupération des résultats : %s', $e->getMessage()),
//                ['element' => 'growl','params'=> ['type' => 'danger']]
//            );
//        }
//
//        $this->redirect(['controller' => 'votes', 'action' => 'index', $seanceId]);
//    }

    private function updateAttendanceList($id, $globale = false)
    {
        $modelAttendance = 'Listepresence';
        $columnIdentity = 'delib_id';
        if ($globale === true) {
            $modelAttendance = 'ListePresenceGlobale';
            $columnIdentity = 'seance_id';
        }
        
        $nbConvoques = 0;
        $nbVoix = 0;
        $nbPresents = 0;

        $this->{$modelAttendance}->deleteAll([$columnIdentity => $id]);

        foreach ($this->data['Acteur'] as $acteur_id => $tab) {
            if ($acteur_id === 0) {
                continue;
            }

            if (isset($this->data['Acteur'][$acteur_id]['suppleant_id'])
                && $acteur_id != $this->data['Acteur'][$acteur_id]['suppleant_id']
            ) {
                $this->request->data[$modelAttendance]['suppleant_id'] = $tab['suppleant_id'];
            } else {
                $this->request->data[$modelAttendance]['suppleant_id'] = null;
            }

            $this->{$modelAttendance}->create();

            $nbConvoques++;
            $this->request->data[$modelAttendance]['acteur_id'] = $acteur_id;

            //Pour savoir si l'acteur principal est present
            if (isset($tab['present'])) {
                $this->request->data[$modelAttendance]['present'] = $tab['present'];
                if ($tab['present'] === 1) {
                    $nbPresents++;
                    $nbVoix++;
                }
            }

            if (!empty($tab['mandataire'])) {
                $this->request->data[$modelAttendance]['mandataire'] = $tab['mandataire'];
                $nbVoix++;
            } else {
                $this->request->data[$modelAttendance]['mandataire'] = null;
            }

            $this->request->data[$modelAttendance][$columnIdentity] = $id;
            $this->{$modelAttendance}->save($this->data[$modelAttendance]);
            $this->{$modelAttendance}->clear();
        }
    }
}
