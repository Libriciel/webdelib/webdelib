<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('CakeTime', 'Utility');
App::uses('AppTools', 'Lib');

class VersionsController extends AppController
{
    public $helpers = ['TinyMCE', 'ProjetUtil','DelibTdt'];
    public $uses = [
        'Deliberation',
        'Typeacte',
        'User',
        'Annexe',
        'Typeseance',
        'Seance',
        'TypeSeance',
        'Infosupdef',
        'Infosup',
        'Historique',
        'Cakeflow.Circuit',
        'Cakeflow.Traitement',
        'DeliberationSeance',
        'DeliberationTypeseance',
        'Service',
        'Nature',
        'Typologiepiece',
        'ProjetEtat'
    ];
    public $components = [
        'Auth' => [
            'mapActions' => [
                'allow' => [
                    'index',
                ]
            ]
        ]
    ];

    /**
     * Page de vue version
     * @access public
     * @param type $id
     * @return type
     */
    public function index($id = null)
    {
        $this->Deliberation->Behaviors->load(
            'Version',
            $this->Deliberation->actsAsVersionOptionsList['Version']
        );

        //Pour l'affichage de l'onglet
        if (isset($this->request['named']['nameTab'])) {
            $this->set('nameTab', $this->request['named']['nameTab']);
        }
        $revision = $this->Deliberation->version($id);

        //Get annexes
        foreach ($revision['deliberations_versions']['data_version']['Annexe'] as $key => $annexe) {
            $this->Deliberation->Annexe->Behaviors->load(
                'Version',
                $this->Deliberation->actsAsVersionOptionsList['Version']
            );
            $annexeTmp = $this->Deliberation->Annexe->getRevisionByModified($annexe);
            if (!empty($annexeTmp)) { // si l'annexe n'est pas versionnée.
                $revision['deliberations_versions']['data_version']['Annexe'][$key]['annexeVersion_id'] = $annexeTmp[0];
            }
            $revision['deliberations_versions']['data_version']['Annexe'][$key]['typologiepiece_code_libelle'] =
              !empty($annexe['typologiepiece_code']) ?
              $this->Typologiepiece->getTypologieNamebyCode($annexe['typologiepiece_code']) : '';
        }

        $revision['deliberations_versions']['data_version']['DeliberationSeance'] = Hash::sort(
            $revision['deliberations_versions']['data_version']['DeliberationSeance'],
            '{n}.Seance.Typeseance.action',
            'asc'
        );

        $revision['deliberations_versions']['data_version']['Deliberation']['num_pref'] =
            $this->ProjetTools->getMatiereByKey(
                $revision['deliberations_versions']['data_version']['Deliberation']['num_pref']
            );
        $revision['deliberations_versions']['data_version']['Deliberation']['typologiepiece_code_libelle'] =
          !empty($revision['deliberations_versions']['data_version']['Deliberation']['typologiepiece_code']) ?
          $this->Typologiepiece->getTypologieNamebyCode(
              $revision['deliberations_versions']['data_version']['Deliberation']['typologiepiece_code']
          ) : '';
        //'num_pref', 'typologiepiece_code', 'tdt_document_papier', 'num_delib',

        $this->set(
            'inBannette',
            in_array($this->Auth->user('id'), $this->Traitement->whoIs($id, 'current', 'RI', false), true)
        );

        // Mise en forme des données du projet ou de la délibération
        $revision['deliberations_versions']['data_version']['libelleEtat'] = $this->ProjetEtat->libelleEtat(
            $revision['deliberations_versions']['data_version']['Deliberation']['etat'],
            $revision['deliberations_versions']['data_version']['Typeacte']['Nature']['code']
        );
        // initialisation des séances
        $listeTypeSeance = [];

        $revision['deliberations_versions']['data_version']['listeSeances'] = [];
        if (!empty($revision['deliberations_versions']['data_version']['DeliberationSeance'])) {
            foreach ($revision['deliberations_versions']['data_version']['DeliberationSeance'] as $seance) {
                $revision['deliberations_versions']['data_version']['listeSeances'][] = [
                    'seance_id' => $seance['Seance']['id'],
                    'type_id' => $seance['Seance']['type_id'],
                    'action' => $seance['Seance']['Typeseance']['action'],
                    'libelle' => $seance['Seance']['Typeseance']['libelle'],
                    'color' => $seance['Seance']['Typeseance']['color'],
                    'date' => $seance['Seance']['date']];
                $listeTypeSeance[] = $seance['Seance']['type_id'];
            }
        }

        if (!empty($revision['deliberations_versions']['data_version']['DeliberationTypeseance'])) {
            foreach ($revision['deliberations_versions']['data_version']['DeliberationTypeseance'] as $typeseance) {
                if (!in_array($typeseance['Typeseance']['id'], $listeTypeSeance, true)) {
                    $revision['deliberations_versions']['data_version']['listeSeances'][] = [
                        'seance_id' => null,
                        'type_id' => $typeseance['Typeseance']['id'],
                        'action' => $typeseance['Typeseance']['action'],
                        'libelle' => $typeseance['Typeseance']['libelle'],
                        'color' => $typeseance['Typeseance']['color'],
                        'date' => null];
                }
            }
        }
        $revision['deliberations_versions']['data_version']['listeSeances'] = Hash::sort(
            $revision['deliberations_versions']['data_version']['listeSeances'],
            '{n}.action',
            'asc'
        );

        $revision['deliberations_versions']['data_version']['Service']['name'] = $this->Deliberation->Service->doList(
            $revision['deliberations_versions']['data_version']['Deliberation']['service_id']
        );
        $revision['deliberations_versions']['data_version']['Circuit']['libelle'] = $this->Circuit->getLibelle(
            $revision['deliberations_versions']['data_version']['Deliberation']['circuit_id']
        );

        $revision['deliberations_versions']['userName'] =
          !empty($this->User->prenomNom($revision['deliberations_versions']['user_id'])) ?
          $this->User->prenomNom($revision['deliberations_versions']['user_id'])
          : __('Historique auto') ;

        // Définitions des infosup
        $this->set('infosupdefs', $this->Infosupdef->find('all', [
            'recursive' => -1,
            'conditions' => [
                'model' => 'Deliberation',
                'actif' => true
            ],
            'order' => 'ordre']));

        $target_id = empty($revision['deliberations_versions']['data_version']['Deliberation']['parent_id']) ?
          $revision['deliberations_versions']['data_version']['Deliberation']['id'] :
          $revision['deliberations_versions']['data_version']['Deliberation']['parent_id'];

        //Test si le projet a été inséré dans un circuit, si oui charger l'affichage
        $wkf_exist = $this->Traitement->find(
            'count',
            ['recursive' => -1, 'conditions' => ['target_id' => $target_id]]
        );
        if (!empty($wkf_exist)) {
            $this->set('visu', $this->requestAction(['plugin' => 'cakeflow',
                'controller' => 'traitements',
                'action' => 'visuTraitement', $target_id], ['return']));
        } else {
            $this->set('visu', null);
        }
        $this->set('revision', $revision);
    }
}
