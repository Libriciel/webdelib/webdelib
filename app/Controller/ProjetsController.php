<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('CakeTime', 'Utility');
App::uses('AppTools', 'Lib');

/**
 *
 */
class ProjetsController extends AppController
{
    public $helpers = ['TinyMCE', 'ProjetUtil','DelibTdt'];
    public $uses = ['Acteur',
        'Deliberation', 'Typeacte', 'User', 'Annexe', 'Typeseance', 'Seance',
        'TypeSeance', 'Commentaire', 'ModelOdtValidator.Modeltemplate', 'Theme',
        'Collectivite', 'Vote', 'Listepresence', 'Infosupdef', 'Infosup', 'Historique',
        'Cakeflow.Circuit', 'Cakeflow.Composition', 'Cakeflow.Etape', 'Cakeflow.Traitement',
        'Cakeflow.Visa', 'Nomenclature', 'DeliberationSeance', 'DeliberationTypeseance',
        'DeliberationUser', 'Service', 'Cron',
        'Nature','Typologiepiece',
        'ProjetEtat'
    ];

    public $components = [
        'RequestHandler',
        'ProjetTools',
        'ModelOdtValidator.Fido',
        'SabreDav',
        'Wopi',
        'Email',
        'Acl',
        'Iparapheur',
        'Filtre',
        'Progress',
        'Conversion',
        'PDFStamp',
        'S2low',
        'Paginator',
        'Teletransmission',
        'Auth' => [
            'mapActions' => [
                'read' => [
                    'mesProjetsRedaction', 'view', 'download',
                    'genereFusionToClient', 'genereFusionToClientByModelTypeNameAndSeanceId',
                    'textsynthesevue', 'deliberationvue', 'textprojetvue',
                    'version',
                  ],
                'create' => [
                    'add',
                  ],
                'update' => [
                    'edit',
                  ],
                'delete' => ['delete'],
                'editerTous',
                'duplicate',
                'supprimerTous',
                'allow' => [
                    'cancel'
                ]
            ]
        ]
    ];

    /**
     * Page de vue détaillée
     *
     * @version 5.1
     * @access public
     * @param type $id
     * @return type
     *
     * @SuppressWarnings(PHPMD)
     */
    public function view($id = null)
    {
        $this->Deliberation->Behaviors->load(
            'Version',
            $this->Deliberation->actsAsVersionOptionsList['Version']
        );

        //Pour l'affichage de l'onglet
        if (isset($this->request['named']['nameTab'])) {
            $this->set('nameTab', $this->request['named']['nameTab']);
        }
        $projet = $this->Deliberation->find('first', [
            'fields' => [
                'id', 'anterieure_id', 'service_id', 'circuit_id', 'typeacte_id', 'anterieure_id',
                'etat',
                'num_pref', 'typologiepiece_code', 'tdt_document_papier', 'num_delib',
                'titre', 'objet', 'objet_delib','parent_id', 'date_limite', 'date_acte',
                'signature_date', 'date_envoi_signature',
                'texte_projet_name', 'texte_synthese_name', 'deliberation_name', 'signee', 'redacteur_id',
                'Deliberation.parapheur_etat',
                'Deliberation.created', 'Deliberation.modified', 'deliberation', 'texte_projet',
                'texte_synthese', 'tdt_data_pdf', 'parapheur_bordereau'],
            'contain' => [
                'User' => ['id', 'nom', 'prenom'],
                'Multidelib' => [
                    'fields' => [
                      'id', 'objet', 'objet_delib', 'num_delib', 'etat', 'deliberation', 'deliberation_name'
                    ],
                    'Annexe' => [
                      'fields' => [
                        'id', 'titre', 'joindre_ctrl_legalite', 'joindre_fusion',
                          'filename','typologiepiece_code', 'size',
                      ]
                    ]
                ],
                'Redacteur' => ['fields' => ['id', 'nom', 'prenom']],
                'Rapporteur' => ['fields' => ['id', 'nom', 'prenom']],
                'Infosup',
                'Annexe' => [
                    'fields' => [
                      'id', 'titre', 'joindre_ctrl_legalite', 'joindre_fusion', 'filename',
                        'typologiepiece_code', 'size',
                    ],
                    'order' => ['Annexe.position'=>'ASC']
                ],
                'Service' => ['fields' => ['name']],
                'Theme' => ['fields' => ['libelle']],
                'Typeacte' => [
                    'fields' => ['name', 'modele_bordereau_projet_id'],
                    'Nature' => ['fields' => ['code']]
                ],
                'Circuit' => ['fields' => ['nom']],
                'DeliberationTypeseance' => ['fields' => ['id'],
                    'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]],
                'DeliberationSeance' => ['fields' => ['id'],
                    'Seance' => ['fields' => ['id', 'date', 'type_id'],
                        'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]
                    ]
                ]
            ],
            'conditions' => ['Deliberation.id' => $id],
            'recursive' => -1
        ]);

        if (empty($projet)) {
            $this->Flash->set(__("Le projet n° %s est introuvable !", $id), ['element' => 'growl']);
            return $this->redirect($this->previous);
        }

        //On cherche à savoir si la délibération est vérrouillée par un utilisateur
        $this->ProjetTools->locked($projet['Deliberation']);

        // récupération des droits pour bouton
        $droits = $this->ProjetTools->getDroits([
            'Projets' => ['read', 'update', 'delete'],
            'duplicate' => 'read',
            'editerTous' => 'read',
            'supprimerTous' => 'read',
            'goNext' => 'read',
            'validerEnUrgence' => 'read',
            'editPostSign' => 'read'
          ]);

        $liste_actions = [];
        //actions complémentaires sans droits
        if (!empty($projet['Typeacte']['modele_bordereau_projet_id'])) {
            array_push($liste_actions, 'generer_bordereau');
        }

        $this->ProjetTools->actionDisponible($projet, $droits, $liste_actions);

        // Multi-délibération
        $info_id = !empty($projet['Deliberation']['parent_id']) ? $projet['Deliberation']['parent_id'] : $id;


        if ($projet['Deliberation']['etat'] == 0 && $projet['Deliberation']['anterieure_id'] != 0) {
            $projet['Deliberation']['iconeEtat'] = $this->ProjetTools->iconeEtat(
                $projet['Deliberation']['id'],
                -2,
                $projet['Typeacte']['Nature']['code']
            );
        } elseif ($projet['Deliberation']['etat'] === 1) {
            $estDansCircuit = $this->Traitement->triggerDansTraitementCible(
                $this->Auth->user('id'),
                $projet['Deliberation']['id']
            );
            $tourDansCircuit = $estDansCircuit
                ? $this->Traitement->positionTrigger($this->Auth->user('id'), $projet['Deliberation']['id'])
                : 0;
            $estRedacteur = ($this->Auth->user('id') == $projet['Deliberation']['redacteur_id']);
            $projet['Deliberation']['iconeEtat'] = $this->ProjetTools->iconeEtat(
                $projet['Deliberation']['id'],
                $projet['Deliberation']['etat'],
                $projet['Typeacte']['Nature']['code'],
                false,
                $estDansCircuit,
                $estRedacteur,
                $tourDansCircuit
            );
        } else {
            $projet['Deliberation']['iconeEtat'] = $this->ProjetTools->iconeEtat(
                $projet['Deliberation']['id'],
                $projet['Deliberation']['etat'],
                $projet['Typeacte']['Nature']['code'],
                $droits['editerTous/read']
            );
        }

        $projet['DeliberationSeance'] = Hash::sort(
            $projet['DeliberationSeance'],
            '{n}.Seance.Typeseance.action',
            'asc'
        );

        $projet['Deliberation']['num_pref'] = $this->ProjetTools->getMatiereByKey($projet['Deliberation']['num_pref']);

        if (!$this->Acl->check(['User' => ['id' => $this->Auth->user('id')]], 'tousLesProjetsRecherche', 'read')) {
            $conditions['Deliberation.id'] = $id;
            $conditions['OR']['redacteur_id'] = $this->Auth->user('id');

            if ($this->Acl->check(['User' => ['id' => $this->Auth->user('id')]], 'Projets', 'read')) {
                $services = [];
                $conditions['Deliberation.id'] = $id;
                $conditions['OR']['redacteur_id'] = $this->Auth->user('id');

                $user = $this->Deliberation->find('first', [
                    'conditions' => ['Deliberation.id' => $id],
                    'contain' => ['User' => ['conditions' => ['User.id' => $this->Auth->user('id')]]],
                    'fields' => ['Deliberation.redacteur_id'],
                    'recursive' => -1]);

                if (!empty($user['User'])) {
                    $conditions['OR']['redacteur_id'] = $user['Deliberation']['redacteur_id'];
                }
                $user_services = $this->User->find(
                    'first',
                    [
                        'conditions' => ['User.id' => $this->Auth->user('id')],
                        'fields' => ['User.id'],
                        'contain' => ['Service.id']
                    ]
                );
                foreach ($user_services['Service'] as $service) {
                    $services[] = $service['id'];
                }
                $conditions['OR']['service_id'] = $services;
            }

            $acte = $this->Deliberation->find('first', [
                'conditions' => $conditions,
                'fields' => ['Deliberation.id'],
                'recursive' => -1]);

            if (!empty($user['User'])) {
                //pour le multi rédacteur
                $estDansCircuit = $this->Traitement->triggerDansTraitementCible(
                    $user['Deliberation']['redacteur_id'],
                    $info_id
                );
            } else {
                $estDansCircuit = $this->Traitement->triggerDansTraitementCible($this->Auth->user('id'), $info_id);
            }

            if (empty($acte) && ($estDansCircuit == false)) {
                $this->Flash->set(__("Vous n'avez pas les droits pour visualiser cet acte"), [
                    'element' => 'growl',
                    'params' => ['type' => 'danger']
                ]);

                return $this->redirect($this->previous);
            }
        }

        // Compactage des informations supplémentaires
        if (!empty($projet['Infosup'])) {
            $this->set('infosups', $this->Deliberation->Infosup->compacte($projet['Infosup'], false));
        }

        // Lecture des versions anterieures
        if (!empty($projet['Deliberation']['anterieure_id'])) {
            $this->set(
                'tab_anterieure',
                $this->chercherVersionAnterieure(
                    $projet['Deliberation']['anterieure_id'],
                    0,
                    [],
                    'view'
                )
            );
        }

        //Lecture de la version supérieure
        $this->set('versionsup', $this->Deliberation->chercherVersionSuivante($id));
        $this->set('inBannette', in_array(
            $this->Auth->user('id'),
            $this->Traitement->whoIs($id, 'current', 'RI', false),
            true
        ));

        $commentaires_conditions = ['Commentaire.delib_id' => $info_id,];
        if ($projet['Deliberation']['etat'] == 1) {
            $commentaires_conditions['Commentaire.pris_en_compte'] = 0;
        }
        $this->set('commentaires', $this->Commentaire->find('all', [
            'fields' => ['id', 'Commentaire.texte', 'Commentaire.created'],
            'contain' => ['User.nom', 'User.prenom'],
            'conditions' => $commentaires_conditions,
            'order' => ['Commentaire.created' => 'DESC'],
            'recursive' => -1
        ]));
        $this->set('historiques', $this->Historique->find('all', [
            'fields' => ['Historique.user_id', 'Historique.commentaire', 'Historique.created','Historique.revision_id'],
            'contain' => ['User.nom', 'User.prenom'],
            'conditions' => ['Historique.delib_id' => $id],
            //'joins' => array($this->Historique->join('User',array( 'type' => 'INNER' ))),
            'order' => ['Historique.created' => 'DESC','Historique.id' => 'DESC'],
            'recursive' => -1
        ]));

        //Récupération du model_id (pour lien bouton generer)
        $model_id = $this->Deliberation->getModelId($id);
        $projet['Modeltemplate']['id'] = $model_id;

        // Mise en forme des données du projet ou de la délibération
        $projet['Deliberation']['libelleEtat'] = $this->ProjetEtat->libelleEtat(
            $projet['Deliberation']['etat'],
            $projet['Typeacte']['Nature']['code']
        );
        $projet['Deliberation']['typologiepiece_code_libelle'] = $this->Typologiepiece->getTypologieNamebyCode(
            $projet['Deliberation']['typologiepiece_code']
        );
        foreach ($projet['Annexe'] as &$annexe) {
            $annexe['typologiepiece_code_libelle'] = $this->Typologiepiece->getTypologieNamebyCode(
                $annexe['typologiepiece_code']
            );
        }
        foreach ($projet['Multidelib'] as &$multidelib) {
            foreach ($multidelib['Annexe'] as &$annexe) {
                $annexe['typologiepiece_code_libelle'] = $this->Typologiepiece->getTypologieNamebyCode(
                    $annexe['typologiepiece_code']
                );
            }
        }
        // initialisation des séances
        $listeTypeSeance = [];
        $projet['listeSeances'] = [];
        if (isset($projet['DeliberationSeance']) && !empty($projet['DeliberationSeance'])) {
            foreach ($projet['DeliberationSeance'] as $seance) {
                $projet['listeSeances'][] = [
                    'seance_id' => $seance['Seance']['id'],
                    'type_id' => $seance['Seance']['type_id'],
                    'action' => $seance['Seance']['Typeseance']['action'],
                    'libelle' => $seance['Seance']['Typeseance']['libelle'],
                    'color' => $seance['Seance']['Typeseance']['color'],
                    'date' => $seance['Seance']['date']];
                $listeTypeSeance[] = $seance['Seance']['type_id'];
            }
        }

        if (isset($projet['DeliberationTypeseance']) && !empty($projet['DeliberationTypeseance'])) {
            foreach ($projet['DeliberationTypeseance'] as $typeseance) {
                if (!in_array($typeseance['Typeseance']['id'], $listeTypeSeance, true)) {
                    $projet['listeSeances'][] = [
                        'seance_id' => null,
                        'type_id' => $typeseance['Typeseance']['id'],
                        'action' => $typeseance['Typeseance']['action'],
                        'libelle' => $typeseance['Typeseance']['libelle'],
                        'color' => $typeseance['Typeseance']['color'],
                        'date' => null];
                }
            }
        }
        $projet['listeSeances'] = Hash::sort($projet['listeSeances'], '{n}.action', 'asc');
        $projet['Service']['name'] = $this->Deliberation->Service->doList($projet['Deliberation']['service_id']);
        $projet['Circuit']['libelle'] = $this->Circuit->getLibelle($projet['Deliberation']['circuit_id']);

        // Définitions des infosup
        $this->set('infosupdefs', $this->Infosupdef->find('all', [
            'recursive' => -1,
            'conditions' => [
                'model' => 'Deliberation',
                'actif' => true
            ],
            'order' => 'ordre']));

        //Test si le projet a été inséré dans un circuit, si oui charger l'affichage
        $wkf_exist = $this->Traitement->find('count', ['recursive' => -1, 'conditions' => ['target_id' => $info_id]]);
        if (!empty($wkf_exist)) {
            $this->set('visu', $this->requestAction(['plugin' => 'cakeflow',
                'controller' => 'traitements',
                'action' => 'visuTraitement', $info_id], ['return']));
        } else {
            $this->set('visu', null);
        }

        $this->set('projet', $projet);
        $this->set('revisions', $this->Deliberation->versions($id));
    }

    /**
     * Page de vue version
     * @version 5.1
     * @access public
     * @param type $id
     * @return type
     */
    public function version($id = null)
    {
        $this->Deliberation->Behaviors->load(
            'Version',
            $this->Deliberation->actsAsVersionOptionsList['Version']
        );

        //Pour l'affichage de l'onglet
        if (isset($this->request['named']['nameTab'])) {
            $this->set('nameTab', $this->request['named']['nameTab']);
        }
        $revision = $this->Deliberation->version($id);

        //Get annexes
        foreach ($revision['deliberations_versions']['data_version']['Annexe'] as $key => $annexe) {
            $this->Deliberation->Annexe->Behaviors->load(
                'Version',
                $this->Deliberation->actsAsVersionOptionsList['Version']
            );
            $annexeTmp = $this->Deliberation->Annexe->getRevisionByModified($annexe);
            if (!empty($annexeTmp)) { // si l'annexe n'est pas versionnée.
                $revision['deliberations_versions']['data_version']['Annexe'][$key]['annexeVersion_id'] = $annexeTmp[0];
            }
            $revision['deliberations_versions']['data_version']['Annexe'][$key]['typologiepiece_code_libelle'] =
              !empty($annexe['typologiepiece_code']) ?
              $this->Typologiepiece->getTypologieNamebyCode($annexe['typologiepiece_code']) : '';
        }

        $revision['deliberations_versions']['data_version']['DeliberationSeance'] = Hash::sort(
            $revision['deliberations_versions']['data_version']['DeliberationSeance'],
            '{n}.Seance.Typeseance.action',
            'asc'
        );

        $revision['deliberations_versions']['data_version']['Deliberation']['num_pref'] =
            $this->ProjetTools->getMatiereByKey(
                $revision['deliberations_versions']['data_version']['Deliberation']['num_pref']
            );
        $revision['deliberations_versions']['data_version']['Deliberation']['typologiepiece_code_libelle'] =
          !empty($revision['deliberations_versions']['data_version']['Deliberation']['typologiepiece_code']) ?
          $this->Typologiepiece->getTypologieNamebyCode(
              $revision['deliberations_versions']['data_version']['Deliberation']['typologiepiece_code']
          ) : '';
        //'num_pref', 'typologiepiece_code', 'tdt_document_papier', 'num_delib',

        $this->set(
            'inBannette',
            in_array($this->Auth->user('id'), $this->Traitement->whoIs($id, 'current', 'RI', false), true)
        );

        // Mise en forme des données du projet ou de la délibération
        $revision['deliberations_versions']['data_version']['libelleEtat'] = $this->ProjetEtat->libelleEtat(
            $revision['deliberations_versions']['data_version']['Deliberation']['etat'],
            $revision['deliberations_versions']['data_version']['Typeacte']['Nature']['code']
        );
        // initialisation des séances
        $listeTypeSeance = [];

        $revision['deliberations_versions']['data_version']['listeSeances'] = [];
        if (!empty($revision['deliberations_versions']['data_version']['DeliberationSeance'])) {
            foreach ($revision['deliberations_versions']['data_version']['DeliberationSeance'] as $seance) {
                $revision['deliberations_versions']['data_version']['listeSeances'][] = [
                    'seance_id' => $seance['Seance']['id'],
                    'type_id' => $seance['Seance']['type_id'],
                    'action' => $seance['Seance']['Typeseance']['action'],
                    'libelle' => $seance['Seance']['Typeseance']['libelle'],
                    'color' => $seance['Seance']['Typeseance']['color'],
                    'date' => $seance['Seance']['date']];
                $listeTypeSeance[] = $seance['Seance']['type_id'];
            }
        }

        if (!empty($revision['deliberations_versions']['data_version']['DeliberationTypeseance'])) {
            foreach ($revision['deliberations_versions']['data_version']['DeliberationTypeseance'] as $typeseance) {
                if (!in_array($typeseance['Typeseance']['id'], $listeTypeSeance, true)) {
                    $revision['deliberations_versions']['data_version']['listeSeances'][] = [
                        'seance_id' => null,
                        'type_id' => $typeseance['Typeseance']['id'],
                        'action' => $typeseance['Typeseance']['action'],
                        'libelle' => $typeseance['Typeseance']['libelle'],
                        'color' => $typeseance['Typeseance']['color'],
                        'date' => null];
                }
            }
        }
        $revision['deliberations_versions']['data_version']['listeSeances'] = Hash::sort(
            $revision['deliberations_versions']['data_version']['listeSeances'],
            '{n}.action',
            'asc'
        );

        $revision['deliberations_versions']['data_version']['Service']['name'] = $this->Deliberation->Service->doList(
            $revision['deliberations_versions']['data_version']['Deliberation']['service_id']
        );
        $revision['deliberations_versions']['data_version']['Circuit']['libelle'] = $this->Circuit->getLibelle(
            $revision['deliberations_versions']['data_version']['Deliberation']['circuit_id']
        );

        $revision['deliberations_versions']['userName'] =
          !empty($this->User->prenomNom($revision['deliberations_versions']['user_id'])) ?
          $this->User->prenomNom($revision['deliberations_versions']['user_id'])
          : __('Historique auto') ;

        // Définitions des infosup
        $this->set('infosupdefs', $this->Infosupdef->find('all', [
            'recursive' => -1,
            'conditions' => [
                'model' => 'Deliberation',
                'actif' => true
            ],
            'order' => 'ordre']));

        $target_id = empty($revision['deliberations_versions']['data_version']['Deliberation']['parent_id']) ?
          $revision['deliberations_versions']['data_version']['Deliberation']['id'] :
          $revision['deliberations_versions']['data_version']['Deliberation']['parent_id'];

        //Test si le projet a été inséré dans un circuit, si oui charger l'affichage
        $wkf_exist = $this->Traitement->find(
            'count',
            ['recursive' => -1, 'conditions' => ['target_id' => $target_id]]
        );
        if (!empty($wkf_exist)) {
            $this->set('visu', $this->requestAction(['plugin' => 'cakeflow',
                'controller' => 'traitements',
                'action' => 'visuTraitement', $target_id], ['return']));
        } else {
            $this->set('visu', null);
        }
        $this->set('revision', $revision);
    }

    /**
     * @version 5.1
     * @since 4.3
     * @access public
     * @return redirect
     *
     * @SuppressWarnings(PHPMD)
     */
    public function add()
    {
        $this->Deliberation->Behaviors->load(
            'Version',
            $this->Deliberation->actsAsVersionOptionsList['Version']
        );

        $canEditAll = $this->Acl->check(
            ['User' => ['id' => $this->Auth->user('id')]],
            'Deliberations',
            'admin'
        );
        if ($this->request->is('post')) {
            $this->request->data['Deliberation']['redacteur_id'] = $this->Auth->user('id');
            $this->request->data['Deliberation']['service_id'] = $this->Auth->user('ServiceEmetteur.id');
            if (!empty($this->data['Deliberation']['num_pref'])) {
                $this->request->data['Deliberation']['num_pref'] =
                    str_replace('_', '.', $this->data['Deliberation']['num_pref']);
            }


            if (!empty($this->request->data['Deliberation']['date_limite'])) {
                $this->request->data['Deliberation']['date_limite'] = CakeTime::format(
                    str_replace('/', '-', $this->data['Deliberation']['date_limite']),
                    '%Y-%m-%d 00:00:00'
                );
            }

            //gabarits pour ce type d'acte ?
            $typeacte = $this->Deliberation->Typeacte->find('first', [
                'conditions' => ['id' => $this->data['Deliberation']['typeacte_id']],
                'recursive' => -1
            ]);

            //texte_projet_active
            if (!empty($typeacte['Typeacte']['gabarit_projet'])) {
                if ($typeacte['Typeacte']['gabarit_projet_active'] !== false) {
                    $this->request->data['Deliberation']['texte_projet'] = $typeacte['Typeacte']['gabarit_projet'];
                    $this->request->data['Deliberation']['texte_projet_name'] =
                        $typeacte['Typeacte']['gabarit_projet_name'];
                    $this->request->data['Deliberation']['texte_projet_size'] =
                        strlen($typeacte['Typeacte']['gabarit_projet']);
                    $this->request->data['Deliberation']['texte_projet_type'] =
                        'application/vnd.oasis.opendocument.text';
                }
            }
            $this->request->data['Deliberation']['texte_projet_active'] =
              !empty($typeacte['Typeacte']['gabarit_projet_active']) ?
              $typeacte['Typeacte']['gabarit_projet_active'] : false;
            //texte_synthese_active
            if (!empty($typeacte['Typeacte']['gabarit_synthese'])) {
                if ($typeacte['Typeacte']['gabarit_synthese_active'] !== false) {
                    $this->request->data['Deliberation']['texte_synthese'] = $typeacte['Typeacte']['gabarit_synthese'];
                    $this->request->data['Deliberation']['texte_synthese_name'] =
                        $typeacte['Typeacte']['gabarit_synthese_name'];
                    $this->request->data['Deliberation']['texte_synthese_size'] =
                        strlen($typeacte['Typeacte']['gabarit_synthese']);
                    $this->request->data['Deliberation']['texte_synthese_type'] =
                        'application/vnd.oasis.opendocument.text';
                }
            }
            $this->request->data['Deliberation']['texte_synthese_active'] =
              !empty($typeacte['Typeacte']['gabarit_synthese_active']) ?
              $typeacte['Typeacte']['gabarit_synthese_active']
              : false;
            //texte_acte_active
            if (!empty($typeacte['Typeacte']['gabarit_acte'])) {
                if ($typeacte['Typeacte']['gabarit_acte_active'] !== false) {
                    $this->request->data['Deliberation']['deliberation'] = $typeacte['Typeacte']['gabarit_acte'];
                    $this->request->data['Deliberation']['deliberation_name'] =
                        $typeacte['Typeacte']['gabarit_acte_name'];
                    $this->request->data['Deliberation']['deliberation_size'] =
                        strlen($typeacte['Typeacte']['gabarit_acte']);
                    $this->request->data['Deliberation']['deliberation_type'] =
                        'application/vnd.oasis.opendocument.text';
                }
            }
            $this->request->data['Deliberation']['texte_acte_active'] =
                !empty($typeacte['Typeacte']['gabarit_acte_active']) ?
                $typeacte['Typeacte']['gabarit_acte_active']
                : false;
            $this->request->data['Deliberation']['is_multidelib_management'] = false;

            $db = $this->Deliberation->getDataSource();
            $transactionBegun = $db->begin();
            try {
                $success = true;

                $this->Deliberation->create();
                if ($this->Deliberation->save($this->request->data['Deliberation'])) {
                    //Récupération de l'identifiant du nouveau projet
                    $this->request->data['Deliberation']['id'] = $this->Deliberation->getLastInsertId();
                } else {
                    $success = false;
                }

                //Gestion des types de séances
                if (!$this->DeliberationTypeseance->saveDeliberationTypeseance($this->request->data)) {
                    $success = false;
                }
                //Gestion des séances
                if (!$this->DeliberationSeance->saveDeliberationSeance($this->request->data)) {
                    $success = false;
                }

                if ($success) {
                    $success = $this->DeliberationUser->saveDeliberationUser($this->request->data);
                }

                if ($success) {
                    $success = $this->Infosup->saveInfosup($this->request->data, 'Deliberation', true);
                }

                if ($transactionBegun) {
                    if ($success) {
                        $db->commit();
                    } else {
                        throw new Exception('Invalid Fields');
                    }
                }

                $this->Flash->set(
                    __('Projet créé sous le n° %s ', $this->request->data['Deliberation']['id']),
                    ['element' => 'growl']
                );

                if (isset($this->request->data['nameTab'])) {
                    $this->redirect([
                      'controller' => 'projets',
                      'action' => 'edit',
                      $this->request->data['Deliberation']['id'],
                      'nameTab' => $this->request->data['nameTab']
                    ]);
                }

                $this->redirect($this->request->data['Save'] === 'SaveAndQuit' ? $this->previous : [
                  'controller' => 'projets',
                  'action' => 'edit',
                  $this->request->data['Deliberation']['id']
                ]);
            } catch (Exception $e) {
                if ($transactionBegun) {
                    $db->rollback();
                }

                $this->Flash->set(__('Veuillez corriger les erreurs ci-dessous :'), [
                  'element' => 'growl','params' => [
                        'type' => 'danger',
                        'settings' => ['delay' => 10000]]]);
                //throw $e;
            }
        }
        //Informations
        $this->set(['projet' => [
          'service' => [
            'name' => $this->Deliberation->Service->doList($this->Auth->user('ServiceEmetteur.id'))
            ],
          'redacteur' => [
            'nom' => $this->Auth->user('nom'),
            'prenom' => $this->Auth->user('prenom')
          ]
        ]]);

        if (!empty($this->data['Deliberation']['num_pref'])) {
            //FIX
            $this->request->data['Deliberation']['num_pref_libelle'] =
                $this->ProjetTools->getMatiereByKey($this->data['Deliberation']['num_pref']);
            $this->request->data['Deliberation']['num_pref'] = $this->data['Deliberation']['num_pref'];
        }

        $this->set('themes', $this->Deliberation->Theme->generateTreeListByOrder(
            ['Theme.actif' => '1'],
            '&nbsp;&nbsp;&nbsp;&nbsp;'
        ));
        $this->set('nomenclatures', $this->Nomenclature->generateTreeListWithOptionGroup());
        if (!empty($this->data['Deliberation']['num_pref'])) {
            $this->request->data['Deliberation']['num_pref'] =
                str_replace('.', '_', $this->data['Deliberation']['num_pref']);
        }
        $this->set('rapporteurs', $this->Acteur->generateListElus('Acteur.nom'));
        $this->set('selectedRapporteur', $this->Acteur->selectActeurEluIdParDelegationId(
            $this->Auth->user('service_id')
        ));

        //$this->set('date_seances', $this->Seance->generateList(null, $canEditAll));

        if (!empty($this->request->data['Deliberation']['date_limite'])) {
            $this->request->data['Deliberation']['date_limite'] = CakeTime::format(
                $this->request->data['Deliberation']['date_limite'],
                '%d/%m/%Y',
                'invalid'
            );
        }

        $this->set('profil_id', $this->Auth->user('profil_id'));
        $typeseances = [];
        $seances = [];
        // initialisation de la liste des types des séances
        if (!empty($this->request->data['Deliberation']['typeacte_id'])) {
            $typeseances = $this->ProjetTools->getTypeseancesParTypeacte(
                $this->request->data['Deliberation']['typeacte_id'],
                null,
                $this->request->data['DeliberationTypeseance'],
                $this->request->data['DeliberationSeance']
            );
            // initialisation de la liste des séances
            if (!empty($this->request->data['DeliberationTypeseance'])) {
                $seances = $this->ProjetTools->getSeancesParTypeseance(
                    $this->request->data['DeliberationTypeseance'],
                    null,
                    $this->request->data['DeliberationSeance']
                );
            }
        }
        $this->set('deliberationTypeseances', $typeseances);
        $this->set('deliberationSeances', $seances);
        $this->set('typeactes', $this->Deliberation->Typeacte->find('list', [
                    'order' => ['Typeacte.name' => 'ASC'],
                    'recursive' => -1,
                    'allow' => ['Typeacte.id' => 'create']
        ]));
        $typologiePieces = [];
        if (!empty($this->request->data['Deliberation']['typologiepiece_code'])) {
            $typologiePieces = $this->Teletransmission->getTypologiePieces(
                'Principal',
                $this->request->data['Deliberation']['typeacte_id'],
                null,
                null,
                $this->request->data['Deliberation']['typologiepiece_code']
            );
        }
        $this->set('typologiepieces', $typologiePieces);
        $this->set('infosupdefExist', $this->Infosupdef->isInfosupdefExistForModel('Deliberation'));
        $this->set('deliberationUsers', $this->listRedacteurs($this->Auth->user('id'), 'editerTous'));
        $this->set('redirect', $this->previous);
    }

    /**
     * @version 5.1.0
     * @access public
     * @param type $id
     * @param type $file
     */
    public function download($id, $file, $isRevision = false)
    {
        $this->autoRender = false;

        $fileType = $file . '_type';
        $fileSize = $file . '_size';
        $fileName = $file . '_name';
        if ($isRevision !== false) {
            $this->Deliberation->Behaviors->load(
                'Version',
                $this->Deliberation->actsAsVersionOptionsList['Version']
            );

            $delib = $this->Deliberation->version($isRevision);

            $delib['Deliberation'][$fileType] =
                $delib['deliberations_versions']['data_version']['Deliberation'][$fileType];
            $delib['Deliberation'][$fileName] =
                $delib['deliberations_versions']['data_version']['Deliberation'][$fileName];
            $delib['Deliberation'][$file] = $delib['deliberations_versions'][$file];
        } else {
            $delib = $this->Deliberation->find('first', [
                'conditions' => ["Deliberation.id" => $id],
                'fields' => [$fileType, $fileSize, $fileName, $file],
                'recursive' => -1
            ]);
        }
        $this->response->type($delib['Deliberation'][$fileType]);
        $this->response->download($delib['Deliberation'][$fileName]);
        $this->response->body($delib['Deliberation'][$file]);
    }

    /**
     * Edite la délibération pour modification
     *
     * @version 5.1
     * @since 4.3
     * @access public
     * @param type $id
     * @return type
     * @throws NotFoundException
     *
     * @SuppressWarnings(PHPMD)
     */
    public function edit($id = null)
    {
        $this->Deliberation->Behaviors->load(
            'Version',
            $this->Deliberation->actsAsVersionOptionsList['Version']
        );

        if (!$id) {
            throw new NotFoundException(__('Id de déliberation invalide'));
        }

        $deliberation = $this->Deliberation->find('first', [
            'fields' => [
                'id', 'parent_id',
                'typeacte_id', 'theme_id', 'etat',
                'objet', 'objet_delib', 'titre',
                'texte_projet', 'texte_projet_name', 'texte_projet_type', 'texte_projet_size', 'texte_projet_active',
                'texte_synthese', 'texte_synthese_name', 'texte_synthese_type',
                'texte_synthese_size', 'texte_synthese_active',
                'deliberation', 'deliberation_name', 'deliberation_type', 'deliberation_size', 'texte_acte_active',
                'date_limite',
                'num_pref', 'typologiepiece_code', 'num_delib','tdt_document_papier',
                'service_id',
                'is_multidelib',
                'parapheur_etat',
                'rapporteur_id',
                'created',
                'modified',
              ],
            'contain' => [
                'Infosup',
                'DeliberationUser',
                'DeliberationTypeseance' => ['fields' => ['typeseance_id']],
                'DeliberationSeance'=> ['fields' => ['seance_id']],
                'Annexe' => [
                    'fields' => ['id', 'filename', 'filetype', 'titre',
                        'joindre_ctrl_legalite', 'joindre_fusion', 'position', 'typologiepiece_code', 'size'],
                    'order' => ['Annexe.position' => 'ASC']
                ],
                'Multidelib' => [
                    'fields' => ['Multidelib.id', 'Multidelib.objet', 'Multidelib.objet_delib',
                        'Multidelib.deliberation', 'Multidelib.deliberation_name', 'Multidelib.deliberation_type'
                    ]
                ],
                'Redacteur' => ['fields' => ['id', 'nom', 'prenom']]
            ],
            'conditions' => ['Deliberation.id' => $id],
            'recursive' => -1
        ]);

        $this->isProjetLocked($deliberation);

        if (!$deliberation) {
            $this->Flash->set(__("Le projet n° %s est introuvable !", $id), ['element' => 'growl']);
            //throw new NotFoundException(__('Invalid deliberation'));
            $this->redirect($this->previous);
        }

        $this->isProjetCanModified($id, $deliberation);

        //Gestion des multidélibération
        if (!empty($deliberation['Deliberation']['parent_id'])) {
            $this->redirect(['action' => 'edit', $deliberation['Deliberation']['parent_id']]);
        }

        $canEditAll = $this->Acl->check(
            ['User' => ['id' => $this->Auth->user('id')]],
            'editerTous',
            'read'
        );
        // on soumet le formulaire
        if ($this->request->is(['post', 'put'])) {
            $this->Deliberation->id = $id;
            $this->request->data['Deliberation']['is_multidelib_management'] = false;

            $db = $this->Deliberation->getDataSource();
            $transactionBegun = $db->begin();
            try {
                $success = true;
                if (!$this->Deliberation->save($this->request->data['Deliberation'])) {
                    $success = false;
                }

                //Gestion des types de séances
                if (!$this->DeliberationTypeseance->saveDeliberationTypeseance($this->request->data)) {
                    $success = false;
                }
                //Gestion des séances
                if (!$this->DeliberationSeance->saveDeliberationSeance($this->request->data)) {
                    $success = false;
                }

                if ($success
                    && ($deliberation['Redacteur']['id'] === $this->Auth->user('id') || $canEditAll)
                    && isset($this->request->data['DeliberationUser'])
                ) {
                    $success = $this->DeliberationUser->saveDeliberationUser($this->request->data);
                }

                if ($success) {
                    $success = $this->Infosup->saveInfosup($this->request->data, 'Deliberation');
                }

                // suppression des délibérations rattachées
                if ($success && !empty($this->data['MultidelibASupprimer'])) {
                    foreach ($this->data['MultidelibASupprimer'] as $multidelib_id) {
                        $success = $this->Deliberation->supprimer($multidelib_id, $this->Auth->user('id'));
                        if (!empty($this->data['Multidelib'])) {
                            foreach ($this->data['Multidelib'] as $multidelib_key => $multidelib) {
                                if (!empty($multidelib['id']) && $multidelib['id']==$multidelib_id) {
                                    unset($this->request->data['Multidelib'][$multidelib_key]);
                                }
                            }
                        }
                    }
                }

                if (!empty($this->data['MultidelibAdd']) && !empty($this->data['Multidelib'])) {
                    $this->request->data['Multidelib'] = array_merge(
                        $this->data['Multidelib'],
                        $this->data['MultidelibAdd']
                    );
                } elseif (!empty($this->data['MultidelibAdd'])) {
                    $this->request->data['Multidelib'] = $this->data['MultidelibAdd'];
                }

                // sauvegarde des délibérations rattachées
                if ($success && !empty($this->data['Multidelib'])) {
                    foreach ($this->data['Multidelib'] as $multidelib_key => $multidelib) {
                        $multidelib_id = $this->Deliberation->saveDelibRattachees($id, $multidelib);
                        //Récupération des annexes pour la délibération rattachées
                        if (array_key_exists('Annexe', $this->data) && $multidelib_id!==false) {
                            foreach ($this->request->data['Annexe'] as &$annexe) {
                                if (!empty($annexe['projet_id']) && $annexe['projet_id']===$multidelib_key) {
                                    $annexe['projet_id'] = $multidelib_id;
                                }
                            }
                        }

                        $success = $multidelib_id == false ? false : true;
                    }
                }

                if ($success) {
                    $success = $this->Deliberation->saveInfoDelibRattachees($id);
                }
                // Sauvegarde des nouvelles Annexes pour tous les projets
                if ($success && array_key_exists('Annexe', $this->data)) {
                    if (!$this->Annexe->saveAnnexe($this->request->data['Annexe'])) {
                        if (!empty($this->Annexe->validationErrors)) {
                            foreach ($this->Annexe->validationErrors as $validationError) {
                                foreach ($validationError as $filename => $annexe_errors) {
                                    $annexesErrors[$filename][] = implode(',', $annexe_errors);
                                }
                            }
                        }
                        $success = false;
                    }
                }

                if ($success) {
                    $this->Deliberation->id = $id;
                    //FIX OPTIMIZE
                    // suppression des annexes
                    if (array_key_exists('AnnexesASupprimer', $this->data)) {
                        foreach ($this->data['AnnexesASupprimer'] as $annexe_id) {
                            $this->Annexe->delete($annexe_id);
                        }
                    }
                    // Modification des annexes
                    if (!empty($this->data['AnnexesAModifier'])) {
                        foreach ($this->request->data['AnnexesAModifier'] as $annexe_id => $annexe) {
                            $annexe_filename = $this->Annexe->find('first', [
                              'fields' => ['filename', 'filetype', 'id', 'foreign_key', 'size'],
                              'conditions' => ['Annexe.id' => $annexe_id],
                              'recursive' => -1
                            ]);
                            if (!empty($annexe_filename['Annexe'])) {
                                $this->Annexe->id = $annexe_id;
                                $annexe['edition_data'] = null;
                                $annexe['data_pdf'] = null;
                                if (isset($annexe['file'])
                                    && (
                                        $annexe_filename['Annexe']['filetype'] ==
                                        'application/vnd.oasis.opendocument.text'
                                        || $annexe_filename['Annexe']['filetype'] ==
                                        'application/vnd.oasis.opendocument.spreadsheet'
                                    )
                                ) {
                                    $file = $this->SabreDav->getFileDav($annexe['file']);
                                    $annexe['data'] = $file->read();
                                    $annexe['size'] = $file->size();
                                }
                                if (isset($annexe['joindre_ctrl_legalite'])
                                    || isset($annexe['typologiepiece_code'])
                                    || isset($annexe['joindre_fusion'])
                                ) {
                                    $annexe['joindre_ctrl_legalite'] =
                                        !empty($annexe['joindre_ctrl_legalite'])
                                            ? $annexe['joindre_ctrl_legalite'] : false;
                                    $annexe['typologiepiece_code'] =
                                        !empty($annexe['typologiepiece_code'])
                                            ? $annexe['typologiepiece_code'] : null;
                                    $annexe['joindre_fusion'] =
                                        !empty($annexe['joindre_fusion']) ? $annexe['joindre_fusion'] : false;
                                }
                                $this->Annexe->save($annexe);

                                if (!empty($this->Annexe->validationErrors)) {
                                    $success = false;
                                    $titre = !empty($annexe['titre'])
                                        ? $annexe['titre'] : $annexe_filename['Annexe']['filename'];
                                    foreach ($this->Annexe->validationErrors as $validationError) {
                                        $annexesErrors[$titre][] = implode(',', $validationError);
                                    }
                                }
                                $this->Annexe->clear();
                            }
                        }
                    }
                    //Validation du dossier
                    if (!$this->Annexe->checkControlTypologiePieceByProjetId($id)) {
                        if (!empty($this->Annexe->validationErrors)) {
                            foreach ($this->Annexe->validationErrors as $validationError) {
                                foreach ($validationError as $filename => $annexe_errors) {
                                    $annexesErrors[$filename][] = implode(',', $annexe_errors);
                                }
                            }
                        }
                        $success = false;
                    }
                    $this->Deliberation->updateLastRevision();
                    //Validation du dossier
                    if (!$this->Annexe->checkControlTypologiePieceByProjetId($id)) {
                        if (!empty($this->Annexe->validationErrors)) {
                            foreach ($this->Annexe->validationErrors as $validationError) {
                                foreach ($validationError as $filename => $annexe_errors) {
                                    $annexesErrors[$filename][] = implode(',', $annexe_errors);
                                }
                            }
                        }
                        $success = false;
                    }
                }

                if ($transactionBegun) {
                    if ($success) {
                        $db->commit();
                    } else {
                        throw new Exception('Invalid Fields');
                    }
                }
                // Save new history
                $this->Historique->enregistre($id, $this->Auth->user('id'), __("Modification du projet"));


                //FIX OPTIMIZE
                //Notification de projet modifier aux utilisateurs qui ont créer le projet
                //Envoi d'une notification de modification au rédacteur
                $currentUser = $this->Auth->user('id');
                $redacteurId = $deliberation['Redacteur']['id'];
                if ($currentUser != $redacteurId) {
                    $this->User->notifier($id, $redacteurId, 'modif_projet_cree');
                }

                //Notification de projet modifier aux utilisateurs qui ont déjà validé le projet
                $destinataires = $this->Traitement->whoIs($id, 'before', ['OK', 'IN']);
                foreach ($destinataires as $destinataire_id) {
                    if ($destinataire_id > 0 && !in_array($destinataire_id, [$currentUser, $redacteurId], true)) {
                        $this->User->notifier($id, $destinataire_id, 'modif_projet_valide');
                    }
                }


                $cmd = 'nohup nice -n 10 -- ' . APP . 'Console' . DS
                    . 'cake Maintenance convertDataToFusion Deliberation'
                    . ' ' . $id
                    . ' -t ' . Configure::read('Config.tenantName')
                    . ' >/dev/null 2>&1  & echo $!';
                $PID = shell_exec($cmd);

                $this->Flash->set(__("Le projet %s a été enregistré", $id), ['element' => 'growl']);

                if ($this->request->data['Save'] === 'SaveAndQuit') {
                    $this->TokensMethods->clear($this->Auth->user('id'));
                    $this->redirect($this->previous);
                }
                $this->redirect($this->here);
            } catch (Exception $e) {
                if ($transactionBegun) {
                    $db->rollback();
                }

                $msg_error = __('Corrigez les erreurs ci-dessous. \n');
                if (!empty($annexesErrors)) {
                    foreach ($annexesErrors as $annexeName => $annexError) {
                        $msg_error .= "Annexe '" . $annexeName . '\' : \n ';
                        foreach ($annexError as $error) {
                            $msg_error .= "- " . $error . '\n ';
                        }
                    }
                }

                $infosupErrors = $this->Infosup->invalidFields();
                if (!empty($infosupErrors)) {
                    foreach ($infosupErrors as $infosupErrors) {
                        $msg_error .= 'Information supplémentaire : \n ';
                        foreach ($infosupErrors as $infosupCode => $infosupError) {
                            $msg_error .= '- ['.$this->Infosupdef->getLabel(
                                'Deliberation',
                                $infosupCode
                            ).']' . $infosupError . '\n ';
                        }
                    }
                }

                $multiDelibErrors = $this->Deliberation->invalidFields();
                if (!empty($multiDelibErrors)) {
                    foreach ($multiDelibErrors as $multiDelibError) {
                        $msg_error .= 'Multi-délibération : \n ';
                        foreach ($multiDelibError as $error) {
                            $msg_error .= '- ' . $error . '\n ';
                        }
                    }
                }

                $this->Flash->set(
                    $msg_error . ' \n '
                    . __('Attention, les modifications ou ajouts des fichiers n\'ont pas été enregistrées'),
                    [
                        'element' => 'growl',
                        'params' => [
                            'type' => 'danger',
                            'settings' => ['delay' => 10000]
                        ]
                    ]
                );
            }
        }

        //Pour la gestion du webdav
        //FIX avec le vérrou
        $this->SabreDav->start($id);

        //Préparation des données à éditer
        if (!$this->request->data) {
            $this->request->data = $deliberation;

            //Gestion du webdav
            if (!empty($this->data['Deliberation']['texte_projet'])
              || !empty($this->data['Deliberation']['texte_synthese'])
              || !empty($this->data['Deliberation']['deliberation'])) {
                //Webddav texte_projet
                if (!empty($this->data['Deliberation']['texte_projet'])) {
                    $pathFileDav = $this->SabreDav->newFileDav(
                        $this->data['Deliberation']['texte_projet_name'],
                        $this->data['Deliberation']['texte_projet'],
                        'texte_projet'
                    );
                    $this->request->data['Deliberation']['file_texte_projet'] = $pathFileDav;
                    $this->request->data['Deliberation']['file_url_texte_projet'] = $this->SabreDav->newFileDavUrl();
                    $this->request->data['Deliberation']['file_wopi_texte_projet'] = $this->Wopi->getUrl($pathFileDav);
                }
                //Webddav texte_synthese
                if (!empty($this->data['Deliberation']['texte_synthese'])) {
                    $pathFileDav = $this->SabreDav->newFileDav(
                        $this->data['Deliberation']['texte_synthese_name'],
                        $this->data['Deliberation']['texte_synthese'],
                        'texte_synthese'
                    );
                    $this->request->data['Deliberation']['file_texte_synthese'] = $pathFileDav;
                    $this->request->data['Deliberation']['file_url_texte_synthese'] = $this->SabreDav->newFileDavUrl();
                    $this->request->data['Deliberation']['file_wopi_texte_synthese'] =
                        $this->Wopi->getUrl($pathFileDav);
                }
                //Webddav texte_deliberation
                if (!empty($this->data['Deliberation']['deliberation'])) {
                    $pathFileDav = $this->SabreDav->newFileDav(
                        $this->data['Deliberation']['deliberation_name'],
                        $this->data['Deliberation']['deliberation'],
                        'deliberation'
                    );
                    $this->request->data['Deliberation']['file_deliberation'] = $pathFileDav;
                    $this->request->data['Deliberation']['file_url_deliberation'] = $this->SabreDav->newFileDavUrl();
                    $this->request->data['Deliberation']['file_wopi_deliberation'] = $this->Wopi->getUrl($pathFileDav);
                }
            }


            // création des fichiers des infosup de type odtFile
            foreach ($this->request->data['Infosup'] as $infosup_key => $infosup) {
                $infoSupDef = $this->Infosupdef->find('first', [
                    'fields' => ['type'],
                    'conditions' => [
                      'id' => $infosup['infosupdef_id'],
                      'model' => 'Deliberation',
                      'actif' => true],
                    'recursive' => -1,
                  ]);

                //FIX
                if (!empty($infoSupDef['Infosupdef']['type'])
                  && $infoSupDef['Infosupdef']['type'] === 'odtFile'
                  && !empty($infosup['file_name']) && !empty($infosup['content'])) {
                    $this->request->data['Infosup'][$infosup_key]['file_infosup'] =
                        $this->SabreDav->newFileDav($infosup['file_name'], $infosup['content']);
                    $this->request->data['Infosup'][$infosup_key]['file_infosup_url'] =
                        $this->SabreDav->newFileDavUrl();
                }
            }

            //Gestion des informations supplémentaires;
            $this->request->data['Infosup'] = $this->Deliberation->Infosup->compacte($this->request->data['Infosup']);

            $this->request->data['Deliberation']['date_limite'] = CakeTime::format(
                str_replace('/', '-', $this->data['Deliberation']['date_limite']),
                '%Y-%m-%d 00:00:00'
            );
            $this->request->data['Service']['name'] = $this->Deliberation->Service->doList(
                $this->request->data['Deliberation']['service_id']
            );

            //on récupère le gabarit rattaché au type d'acte en cours d'utilisation par le projet
            $this->set('gabarits_acte', $this->Deliberation->Typeacte->find('first', [
                        'fields' => ['id', 'gabarit_acte_name'],
                        'conditions' => [
                            'id' => $this->request->data['Deliberation']['typeacte_id']
                        ],
                        'recursive' => -1]));

            //Pour l'affichage de l'onglet
            if (isset($this->request['named']['nameTab'])) {
                $this->set('nameTab', $this->request['named']['nameTab']);
            }

            $this->set('selectedRapporteur', $this->request->data['Deliberation']['rapporteur_id']);

            $this->set('is_multi', $this->request->data['Deliberation']['is_multidelib']);
            if ($this->request->data['Deliberation']['parapheur_etat'] >= 1) {
                $this->Flash->set(
                    __("Attention, l'acte est en cours de signature !"),
                    ['element' => 'growl', 'params' => ['type' => 'danger']]
                );
            }
        } else {
            //On reset les modifcations des annexes
            $this->request->data['Annexe'] = $deliberation['Annexe'];

            //On reset les modifcations des multi-délibération
            $this->request->data['Multidelib'] = $deliberation['Multidelib'];

            $this->request->data['Deliberation']['texte_acte_active'] =
                $deliberation['Deliberation']['texte_acte_active'];
            $this->request->data['Deliberation']['texte_projet_active'] =
                $deliberation['Deliberation']['texte_projet_active'];
            $this->request->data['Deliberation']['texte_synthese_active'] =
                $deliberation['Deliberation']['texte_synthese_active'];
        }


        //Informations
        $this->set(['projet' => [
          'created' => $deliberation['Deliberation']['created'],
          'modified' => $deliberation['Deliberation']['modified'],
          'service' => [
            'name' => $this->Deliberation->Service->doList($deliberation['Deliberation']['service_id'])
          ],
            'redacteur' => [
              'nom' => $deliberation['Redacteur']['nom'],
              'prenom' => $deliberation['Redacteur']['prenom']
              ]
          ]
        ]);

        $this->set('rapporteurs', $this->Acteur->generateListElus('Acteur.nom'));
        $this->set('infosuplistedefs', $this->Infosupdef->generateListes('Deliberation'));
        $this->set('infosupdefs', $this->Infosupdef->find('all', [
                    'conditions' => ['model' => 'Deliberation', 'actif' => true],
                    'order' => 'ordre',
                    'contain' => ['Profil.id']]));

        if (!isset($this->data['Deliberation']['texte_projet_name'])) {
            $this->request->data['Deliberation']['texte_projet_name'] =
                $deliberation['Deliberation']['texte_projet_name'];
            if (!empty($this->request->data['Deliberation']['file_texte_projet'])) {
                $this->request->data['Deliberation']['file_url_texte_projet'] =
                    $this->SabreDav->fileDavUrl($this->request->data['Deliberation']['file_texte_projet']);
            }
        }

        if (!isset($this->data['Deliberation']['texte_synthese_name'])) {
            $this->request->data['Deliberation']['texte_synthese_name'] =
                $deliberation['Deliberation']['texte_synthese_name'];
            if (!empty($this->request->data['Deliberation']['file_texte_synthese'])) {
                $this->request->data['Deliberation']['file_url_texte_synthese'] = $this->SabreDav->fileDavUrl(
                    $this->request->data['Deliberation']['file_texte_synthese']
                );
            }
        }

        if (!isset($this->data['Deliberation']['deliberation_name'])) {
            $this->request->data['Deliberation']['deliberation_name'] =
                $deliberation['Deliberation']['deliberation_name'];
            if (!empty($this->request->data['Deliberation']['file_deliberation'])) {
                $this->request->data['Deliberation']['file_url_deliberation'] =
                    $this->SabreDav->fileDavUrl($this->request->data['Deliberation']['file_deliberation']);
            }
        }

        if (!empty($this->request->data['Deliberation']['date_limite'])) {
            $this->request->data['Deliberation']['date_limite'] =
                CakeTime::format($this->data['Deliberation']['date_limite'], '%d/%m/%Y');
        }

        if (isset($this->data['Annexe'])) {
            unset($this->request->data['Annexe']);
            // création des fichiers des annexes de type vnd.oasis.opendocument
            foreach ($deliberation['Annexe'] as &$annexe) {
                if ($annexe['filetype'] == 'application/vnd.oasis.opendocument.text'
                    || $annexe['filetype'] == 'application/vnd.oasis.opendocument.spreadsheet'
                ) {
                    $annexe['edit'] = true;
                    $this->Annexe->recursive = -1;
                    $annexe_data = $this->Annexe->findById($annexe['id'], 'data');
                    $annexe['file'] = $this->SabreDav->newFileDav(
                        $annexe['filename'],
                        $annexe_data['Annexe']['data'],
                        'Annexe_' . $annexe['id']
                    );
                    $annexe['file_url'] = $this->SabreDav->newFileDavUrl();
                    unset($annexe_data);
                }
                //Mise en place du libelle
                $annexe['typologiepiece_code_libelle'] =
                    $this->Typologiepiece->getTypologieNamebyCode($annexe['typologiepiece_code']);
            }
            $this->request->data['Annexe'] = $deliberation['Annexe'];
        }

        $this->set('typeActes', $this->Deliberation->Typeacte->find('list', [
                    'order' => ['Typeacte.name' => 'ASC'],
                    'recursive' => -1,
                    'allow' => ['Typeacte.id' => 'update']
        ]));

        $typeseances = [];
        $seances = [];
        // initialisation de la liste des types des séances
        if (!empty($this->request->data['Deliberation']['typeacte_id'])) {
            $typeseances = $this->ProjetTools->getTypeseancesParTypeacte(
                $this->request->data['Deliberation']['typeacte_id'],
                $id,
                $this->request->data['DeliberationTypeseance'],
                $this->request->data['DeliberationSeance']
            );
            // initialisation de la liste des séances
            if (!empty($this->request->data['DeliberationTypeseance'])) {
                $seances = $this->ProjetTools->getSeancesParTypeseance(
                    $this->request->data['DeliberationTypeseance'],
                    $id,
                    $this->request->data['DeliberationSeance']
                );
            }
        }

        $this->set('deliberationTypeseances', $typeseances);
        $this->set('deliberationSeances', $seances);

        //FIX BUG CAKEPHP FormHelper
        if (!empty($this->data['Deliberation']['num_pref'])) {
            $this->request->data['Deliberation']['num_pref'] =
                str_replace('.', '_', $this->data['Deliberation']['num_pref']);
        }
        //Nomenclatures
        $this->set('nomenclatures', $this->Nomenclature->generateTreeListWithOptionGroup());

        $typologiePieces = [];
        if (!empty($this->request->data['Deliberation']['typologiepiece_code'])) {
            $typologiePieces = $this->Teletransmission->getTypologiePieces(
                'Principale',
                $this->request->data['Deliberation']['typeacte_id'],
                $this->request->data['Deliberation']['num_pref'],
                null,
                $this->request->data['Deliberation']['typologiepiece_code']
            );
        }
        $this->set('typologiepieces', $typologiePieces);

        // liste des rédacteurs
        $this->set('redacteur_disable', true);
        if (($deliberation['Redacteur']['id'] === $this->Auth->user('id')
        && $deliberation['Deliberation']['etat'] === 0)
        || $this->Acl->check(['User' => ['id' => $this->Auth->user('id')]], 'editerTous', 'read')) {
            $this->set('redacteur_disable', false);
        }

        $this->set('deliberationUsers', $this->listRedacteurs(
            $deliberation['Redacteur']['id'],
            'editerTous',
            true
        ));
        //Selection des rédacteurs
        if (!empty($this->request->data['DeliberationUser'])
            && Set::check($this->request->data['DeliberationUser'], '0.user_id')
        ) {
            $this->request->data['DeliberationUser'] =
                Set::extract('/user_id', $this->request->data['DeliberationUser']);
        }

        if (Configure::read('DELIBERATIONS_MULTIPLES')) {
            // initialisation des délibérations rattachées
            if (!empty($this->request->data['Multidelib']) && $this->request->data['Deliberation']['is_multidelib']) {
                $MultidelibAnnex = [];
                foreach ($this->request->data['Multidelib'] as $key => &$delib) {
                    //Webddav texte_deliberation
                    if (!empty($delib['deliberation'])) {
                        $delib['file_deliberation'] = $this->SabreDav->newFileDav(
                            $delib['deliberation_name'],
                            $delib['deliberation'],
                            'Multidelib_' . $delib['id']
                        );
                        $delib['file_url_deliberation'] = $this->SabreDav->newFileDavUrl();
                    }
                    // création des fichiers des annexes de type vnd.oasis.opendocument
                    if (!empty($delib['id'])) {
                        $annexes = $this->Annexe->find('all', [
                            'recursive' => -1,
                            'fields' => [
                                  'id', 'position', 'foreign_key', 'filename', 'filetype', 'titre',
                                  'joindre_ctrl_legalite', 'joindre_fusion', 'typologiepiece_code', 'size',
                                ],
                            'conditions' => ['foreign_key' => $delib['id']],
                            'order' => 'position asc']);
                        foreach ($annexes as &$annexe) {
                            $annexe['Annexe']['typologiepiece_code_libelle'] =
                                $this->Typologiepiece->getTypologieNamebyCode($annexe['Annexe']['typologiepiece_code']);
                            if ($annexe['Annexe']['filetype'] == 'application/vnd.oasis.opendocument.text'
                                || $annexe['Annexe']['filetype'] == 'application/vnd.oasis.opendocument.spreadsheet'
                            ) {
                                $annexeData = $this->Annexe->find('first', [
                                    'fields' => 'data',
                                    'conditions' => ['id' => $annexe['Annexe']['id']],
                                    'recursive' => -1]);
                                $annexe['Annexe']['edit'] = true;
                                $annexe['Annexe']['file'] = $this->SabreDav->newFileDav(
                                    $annexe['Annexe']['filename'],
                                    $annexeData['Annexe']['data'],
                                    'Multidelib_' . $delib['id'] . DS . 'Annexe_' . $annexe['Annexe']['id']
                                );
                                $annexe['Annexe']['file_url'] = $this->SabreDav->newFileDavUrl();
                            }
                            $tempID = $annexe['Annexe']['foreign_key'];
                            $MultidelibAnnex[$tempID][] = $annexe['Annexe'];
                        }
                        $delibRattachee['Annexes'] = $annexes;
                    }
                }
                $this->request->data['MultidelibAnnex'] = $MultidelibAnnex;
            }
        }

        $this->set('typeactes', $this->Deliberation->Typeacte->find('list', [
                    'order' => ['Typeacte.name' => 'ASC'],
                    'recursive' => -1,
                    'allow' => ['Typeacte.id' => 'create']
        ]));

        // liste des services
        $this->set('services', $this->Deliberation->Service->find(
            'list',
            ['conditions' => ['Service.actif' => '1']]
        ));
        // liste des thèmes
        $this->set('themes', $this->Deliberation->Theme->generateTreeListByOrder(
            ['Theme.actif' => '1'],
            '&nbsp;&nbsp;&nbsp;&nbsp;'
        ));
        // liste des raporteurs
        $this->set('rapporteurs', $this->Acteur->generateListElus('Acteur.nom'));

        if ($deliberation['Deliberation']['texte_acte_active'] === true
        || $deliberation['Deliberation']['texte_projet_active'] === true
        || $deliberation['Deliberation']['texte_synthese_active'] === true) {
            $this->set('projet_textes_active', true);
        }

        //FILE
        $extensions = [];
        $extensionsFusion = [];
        $extensionsCtrl = [];
        foreach (Configure::read('DOC_TYPE') as $format) {
            if (!is_array($format['extension'])) {
                $extensions[] = $format['extension'];
                if (!empty($format['joindre_fusion'])) {
                    $extensionsFusion[] = $format['extension'];
                }
                if (!empty($format['joindre_ctrl_legalite'])) {
                    $extensionsCtrl[] = $format['extension'];
                }
            } else {
                foreach ($format['extension'] as $extension) {
                    $extensions[] = $extension;
                    if (!empty($format['joindre_fusion'])) {
                        $extensionsFusion[] = $extension;
                    }
                    if (!empty($format['joindre_ctrl_legalite'])) {
                        $extensionsCtrl[] = $extension;
                    }
                }
            }
        }

        $this->set('extensions', $extensions);
        $this->set('extensionsFusion', $extensionsFusion);
        $this->set('extensionsCtrl', $extensionsCtrl);
        $this->set('wopiClientUrl', $this->Wopi->getClientUrl());
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     * FIX :: suppression des projets votés, signé et envoyé au TdT
     */
    public function delete($id = null)
    {
        $delib = $this->Deliberation->find('first', [
            'recursive' => -1,
            'fields' => [
                'Deliberation.id', 'Deliberation.redacteur_id', 'Deliberation.etat',
                'Deliberation.parapheur_etat'
            ],
            'conditions' => ['id' => $id]]);

        if (empty($delib)) {
            $this->Flash->set(
                __('ID invalide pour le projet de déliberation : suppression impossible'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
        } elseif ($delib['Deliberation']['parapheur_etat'] == 1) {
            $this->Flash->set(
                __('Le projet est dans une étape parapheur, il ne peut être supprimé.'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
        } else {
            //$this->request->allowMethod('post');
            $canDelete = $this->Acl->check(
                ['User' => ['id' => $this->Auth->user('id')]],
                'Projets/supprimerTous',
                'read'
            );
            $canRedacDelete = $this->Acl->check(
                ['User' => ['id' => $this->Auth->user('id')]],
                'Projets',
                'delete'
            );
            if ((($delib['Deliberation']['redacteur_id'] == $this->Auth->user('id'))
                    && $delib['Deliberation']['etat'] === 0 && $canRedacDelete) || ($canDelete)
            ) {
                $this->Deliberation->supprimer($id, $this->Auth->user('id'));
                $this->Flash->set(
                    __('Le projet %s a été supprimé.', $id),
                    ['element' => 'growl','params'=> ['type' => 'success']]
                );
            } else {
                $this->Flash->set(
                    __('Vous ne pouvez pas supprimer ce projet'),
                    ['element' => 'growl','params'=> ['type' => 'danger']]
                );
            }
        }
        $this->redirect($this->previous);
    }

    /**
     * @access public
     * @param type $id
     */
    public function textprojetvue($id = null)
    {
        $this->set('deliberation', $this->Deliberation->find('first', [
            'conditions'=> [
                'Deliberation.id' => $id
            ],
        ]));
        $this->set('delib_id', $id);
    }

    /**
     * @access public
     * @param type $id
     */
    public function textsynthesevue($id = null)
    {
        $this->set('deliberation', $this->Deliberation->find('first', [
            'conditions'=> [
                'Deliberation.id' => $id
            ],
        ]));
        $this->set('delib_id', $id);
    }

    /**
     * @access public
     * @param type $id
     */
    public function deliberationvue($id = null)
    {
        $this->set('deliberation', $this->Deliberation->find('first', [
            'conditions'=> [
                'Deliberation.id' => $id
            ],
        ]));
        $this->set('delib_id', $id);
    }

    /**
     * Affiche la liste des projets en cours de redaction (etat = 0) dont l'utilisateur connecté
     * est le rédacteur.
     *
     * @version 5.1
     * @since 4.3
     * @access public
     */
    public function mesProjetsRedaction()
    {
        if (isset($this->params['render']) && ($this->params['render'] === 'bannette')) {
            $limit = Configure::read('LIMIT');
        } else {
            $limit = 10;
        }

        //Choix du rendu à appliquer
        if (isset($this->params['render'])) {
            $render = $this->params['render'];
        } else {
            $render = 'traitement_lot';
        }

        $listeLiens = $this->Acl->check(
            ['User' => ['id' => $this->Auth->user('id')]],
            'Deliberations',
            'create'
        ) ? ['add'] : [];

        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);
        $conditions = $this->ProjetTools->handleConditions($this->Filtre->conditions());

        $conditions['AND']['Deliberation.etat'] = 0;
        $conditions['AND']['Deliberation.parent_id'] = null;
        $conditions['AND']['OR']['Deliberation.redacteur_id'] = $this->Auth->user('id');
        $conditions['AND']['OR']['DeliberationUser.user_id'] = $this->Auth->user('id');

        $joins = [
            $this->Deliberation->join('DeliberationUser', ['type' => 'LEFT']),
        ];

        $this->paginate = [
            'countField' => 'DISTINCT Deliberation.id',
            'fields' => [
                'DISTINCT Deliberation.id', 'Deliberation.objet', 'Deliberation.etat', 'Deliberation.signee',
                'Deliberation.titre', 'Deliberation.date_limite', 'Deliberation.anterieure_id',
                'Deliberation.num_pref', 'Deliberation.redacteur_id',
                'Deliberation.rapporteur_id',  'Deliberation.circuit_id',
                'Deliberation.typeacte_id', 'Deliberation.theme_id', 'Deliberation.service_id'
            ],
            'joins' => array_merge($joins, $this->handleJoins()),
            'conditions' => $conditions,
            'contain' => [
                'User',
                'Rapporteur' => ['fields'=>['nom', 'prenom']],
                'Annexe' => ['fields' => ['id', 'filename'], 'order' => ['position']],
                'Service' => ['fields' => ['name']],
                'Theme' => ['fields' => ['libelle']],
                'Typeacte' => [
                    'fields' => ['name'],
                    'Nature' => ['fields' => ['code']]
                ],
                'Circuit' => ['fields' => ['nom']],
                'DeliberationTypeseance' => ['fields' => ['id'],
                    'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action'],
                    ]],
                'DeliberationSeance' => ['fields' => ['id'],
                    'Seance' => ['fields' => ['id', 'date', 'type_id'],
                        'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]]],
            ],
            'order' => ['Deliberation.id' => 'DESC'],
            'recursive' => -1,
            'limit' => $limit,
            'allow' => ['typeacte_id']
        ];

        $projets = $this->Paginator->paginate('Deliberation');

        $this->ProjetTools->sortProjetSeanceDate($projets);

        $this->ajouterFiltre($projets);

        $this->set('crumbs', [__('Mes projets'), __('En cours de rédaction')]);
        $this->afficheProjets(
            $render,
            $projets,
            __('Mes projets en cours de rédaction'),
            [],
            $listeLiens
        );
    }

    /**
     * Affiche la liste des projets en cours de validation (etat = 1) qui sont dans les circuits
     * de validation de l'utilisateur connecté et dont le tour de validation est venu.
     *
     * @access public
     *
     * @version 5.1.2
     * @since 4.3
     */
    public function mesProjetsATraiter()
    {
        if (isset($this->params['render']) && ($this->params['render'] == 'bannette')) {
            $limit = Configure::read('LIMIT');
        } else {
            $limit = 10;
        }
        //Choix du rendu à appliquer
        if (isset($this->params['render'])) {
            $render = $this->params['render'];
        } else {
            $render = 'traitement_lot';
        }
        $this->set('actions_possibles', ['valider' => 'Valider', 'refuser' => 'Refuser']);

        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);
        $conditions = $this->ProjetTools->handleConditions($this->Filtre->conditions());
        $conditions['Deliberation.etat'] = 1;
        $delibs_ids = $this->Traitement->listeTargetId(
            $this->Auth->user('id'),
            [
            'etat' => 'NONTRAITE',
            'traitement' => 'AFAIRE',
            'joins'=> [[
                    'table' => 'deliberations',
                    'alias' => 'Deliberation',
                    'type' => 'INNER',
                    'conditions' => [
                        'Traitement.target_id = Deliberation.id'
                        ]
                ]
            ]]
        );
        if (isset($conditions['Deliberation.id'])) {
            $conditions['Deliberation.id'] = array_intersect($conditions['Deliberation.id'], $delibs_ids);
        } else {
            $conditions['Deliberation.id'] = $delibs_ids;
        }
        $conditions['Deliberation.parent_id'] = null;
        $this->paginate = [
            'countField' => 'DISTINCT Deliberation.id',
            'fields' => [
                'DISTINCT Deliberation.id', 'Deliberation.objet', 'Deliberation.etat', 'Deliberation.signee',
                'Deliberation.titre', 'Deliberation.date_limite', 'Deliberation.anterieure_id',
                'Deliberation.num_pref', 'Deliberation.redacteur_id', 'Deliberation.rapporteur_id',
                'Deliberation.circuit_id',
                'Deliberation.typeacte_id', 'Deliberation.theme_id', 'Deliberation.service_id'
            ],
            'conditions' => $conditions,
            'joins' => $this->handleJoins(),
            'contain' => [
                'User',
                'Annexe' => ['fields' => ['id', 'filename'], 'order' => ['position']],
                'Service' => ['fields' => ['name']],
                'Theme' => ['fields' => ['libelle']],
                'Typeacte' => [
                    'fields' => ['name'],
                    'Nature' => ['fields' => ['code']]
                ],
                'Circuit' => ['fields' => ['nom']],
                'DeliberationTypeseance' => ['fields' => ['id'],
                    'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action'],
                    ]],
                'DeliberationSeance' => ['fields' => ['id'],
                    'Seance' => ['fields' => ['id', 'date', 'type_id'],
                        'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]]],
            ],
            'order' => ['Deliberation.id' => 'DESC'],
            'recursive' => -1,
            'limit' => $limit,
            'allow' => ['typeacte_id']
        ];

        $projets = $this->Paginator->paginate('Deliberation');

        $this->ProjetTools->sortProjetSeanceDate($projets);
        $this->ajouterFiltre($projets);
        $this->set('crumbs', [__('Mes Projets'), __('À traiter')]);
        $this->afficheProjets(
            $render,
            $projets,
            __('Mes projets à traiter'),
            ['view', 'traiter', 'generer']
        );
    }

    /**
     * Affiche la liste des projets en cours de validation (etat = 1) qui sont dans les circuits
     * de validation de l'utilisateur connecté et dont ce n'est pas le tour de valider et les projets
     * dont il est le rédacteur
     *
     * @version 4.3
     * @access public
     */
    public function mesProjetsValidation()
    {
        if (isset($this->params['render']) && ($this->params['render'] == 'bannette')) {
            $limit = Configure::read('LIMIT');
        } else {
            $limit = 10;
        }
        //Choix du rendu à appliquer
        if (isset($this->params['render'])) {
            $render = $this->params['render'];
        } else {
            $render = 'traitement_lot';
        }
        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);
        $conditions = $this->ProjetTools->handleConditions($this->Filtre->conditions());

        $delibs_ids = $this->Traitement->listeTargetId($this->Auth->user('id'), [
            'etat' => 'NONTRAITE',
            'traitement' => 'NONAFAIRE',
            'joins'=> [[
                    'table' => 'deliberations',
                    'alias' => 'Deliberation',
                    'type' => 'INNER',
                    'conditions' => [
                        'Traitement.target_id = Deliberation.id'
                        ]
                ]
            ]]);

        if (isset($conditions['Deliberation.id'])) {
            $conditions['AND']['OR']['Deliberation.id'] = array_intersect($conditions['Deliberation.id'], $delibs_ids);
        } elseif (!empty($delibs_ids)) {
            $conditions['AND']['OR']['Deliberation.id'] = $delibs_ids;
        }

        $conditions['AND']['Deliberation.etat'] = 1;
        $conditions['AND']['Deliberation.parent_id'] = null;
        $conditions['AND']['Deliberation.signee'] = false;

        $conditions['AND']['OR']['Deliberation.redacteur_id'] = $this->Auth->user('id');
        $conditions['AND']['OR']['DeliberationUser.user_id'] = $this->Auth->user('id');

        $joins = [
            $this->Deliberation->join('DeliberationUser', ['type' => 'LEFT']),
        ];

        $this->paginate = [
            'countField' => 'DISTINCT Deliberation.id',
            'fields' => [
                'DISTINCT Deliberation.id', 'Deliberation.objet', 'Deliberation.etat', 'Deliberation.signee',
                'Deliberation.titre', 'Deliberation.date_limite', 'Deliberation.anterieure_id',
                'Deliberation.num_pref', 'Deliberation.redacteur_id',
                'Deliberation.rapporteur_id', 'Deliberation.circuit_id',
                'Deliberation.typeacte_id', 'Deliberation.theme_id', 'Deliberation.service_id'
            ],
            'joins' => array_merge($joins, $this->handleJoins()),
            'conditions' => $conditions,
            'contain' => [
                'User',
                'Annexe' => ['fields' => ['id', 'filename'], 'order' => ['position']],
                'Service' => ['fields' => ['name']],
                'Theme' => ['fields' => ['libelle']],
                'Typeacte' => [
                    'fields' => ['name'],
                    'Nature' => ['fields' => ['code']]
                ],
                'Circuit' => ['fields' => ['nom']],
                'DeliberationTypeseance' => ['fields' => ['id'],
                    'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action'],
                    ]],
                'DeliberationSeance' => ['fields' => ['id'],
                    'Seance' => ['fields' => ['id', 'date', 'type_id'],
                        'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]]],
            ],
            'order' => ['Deliberation.id' => 'DESC'],
            'recursive' => -1,
            'limit' => $limit,
            'allow' => ['typeacte_id']
        ];

        $projets = $this->Paginator->paginate('Deliberation');

        $this->ProjetTools->sortProjetSeanceDate($projets);
        $this->ajouterFiltre($projets);
        $this->set('crumbs', [__('Mes projets'), __('En cours de validation')]);
        $this->afficheProjets(
            $render,
            $projets,
            __('Mes projets en cours d\'élaboration et de validation')
        );
    }

    /**
     * Affiche les projets validés (etat = 2) dont l'utilisateur connecté est le rédacteur
     * ou qu'il est dans les circuits de validation des projets
     *
     * @access public
     * @version 5.1.2
     * @since 4.3
     */
    public function mesProjetsValides()
    {
        if (isset($this->params['render']) && ($this->params['render'] == 'bannette')) {
            $limit = Configure::read('LIMIT');
        } else {
            $limit = 10;
        }
        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);
        $conditions = $this->ProjetTools->handleConditions($this->Filtre->conditions());

        $conditions['OR']['Deliberation.id'] = $this->Traitement->listeTargetId(
            $this->Auth->user('id'),
            [
                'etat' => 'TRAITE',
                'joins'=> [[
                        'table' => 'deliberations',
                        'alias' => 'Deliberation',
                        'type' => 'INNER',
                        'conditions' => ['Traitement.target_id = Deliberation.id']
                    ]]
              ]
        );
        $conditions['OR']['Deliberation.redacteur_id'] = $this->Auth->user('id');
        $conditions['OR']['DeliberationUser.user_id'] = $this->Auth->user('id');

        $conditions['AND']['Deliberation.etat'] = 2;
        $conditions['AND']['Deliberation.parent_id'] = null;
        $conditions['AND']['Deliberation.signee'] = false;

        $joins = [
            $this->Deliberation->join('DeliberationUser', ['type' => 'LEFT']),
        ];

        $this->paginate = [
            'countField' => 'DISTINCT Deliberation.id',
            'fields' => [
                'DISTINCT Deliberation.id', 'Deliberation.objet', 'Deliberation.etat', 'Deliberation.signee',
                'Deliberation.titre', 'Deliberation.date_limite', 'Deliberation.anterieure_id',
                'Deliberation.num_pref', 'Deliberation.redacteur_id', 'Deliberation.rapporteur_id',
                'Deliberation.circuit_id',
                'Deliberation.typeacte_id', 'Deliberation.theme_id', 'Deliberation.service_id'
            ],
            'conditions' => $conditions,
            'joins' => array_merge($joins, $this->handleJoins()),
            'contain' => [
                'User',
                'Annexe' => ['fields' => ['id', 'filename'], 'order' => ['position']],
                'Service' => ['fields' => ['name']],
                'Theme' => ['fields' => ['libelle']],
                'Typeacte' => [
                    'fields' => ['name'],
                    'Nature' => ['fields' => ['code']]
                ],
                'Circuit' => ['fields' => ['nom']],
                'DeliberationTypeseance' => ['fields' => ['id'],
                    'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action'],
                    ]],
                'DeliberationSeance' => ['fields' => ['id'],
                    'Seance' => ['fields' => ['id', 'date', 'type_id'],
                        'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]]],
            ],
            'order' => ['Deliberation.id' => 'DESC'],
            'recursive' => -1,
            'limit' => $limit,
            'allow' => ['typeacte_id']
        ];

        $projets = $this->Paginator->paginate('Deliberation');

        $this->ProjetTools->sortProjetSeanceDate($projets);
        $this->ajouterFiltre($projets);
        $this->set('crumbs', [__('Mes projets'), __('Validés')]);
        $this->afficheProjets('traitement_lot', $projets, __('Mes projets validés'));
    }

    /**
     * Affiche la liste de tous les projets dont le rédacteur fait parti de mon/mes services
     * Permet de valider en urgence un projet
     *
     * @access public
     *
     * @version 5.1.2
     * @since 4.3
     */
    public function projetsMonService()
    {
        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);
        $conditions = $this->ProjetTools->handleConditions($this->Filtre->conditions());

        if (!isset($conditions['Deliberation.service_id'])) {
            $aService_id = [];
            foreach ($this->Auth->user('Service') as $service_id => $service) {
                $aService_id = array_merge($aService_id, $this->User->Service->doListId($service_id));
            }
            $conditions['Deliberation.service_id'] = array_unique($aService_id);
        }

        $conditions['Deliberation.etat !='] = -1;
        $conditions['Deliberation.etat <'] = 3;
        $conditions['Deliberation.parent_id'] = null;
        $joins = [
            $this->Deliberation->join('DeliberationUser', ['type' => 'LEFT']),
            $this->Deliberation->join('Typeacte', ['type' => 'LEFT']),
            $this->Deliberation->Typeacte->join('Nature', ['type' => 'INNER']),
        ];

        $this->paginate = [
            'countField' => 'DISTINCT Deliberation.id',
            'fields' => ['DISTINCT Deliberation.id', 'Deliberation.objet', 'Deliberation.etat', 'Deliberation.signee',
                'Deliberation.titre', 'Deliberation.date_limite', 'Deliberation.anterieure_id',
                'Deliberation.num_pref', 'Deliberation.redacteur_id',
                'Deliberation.rapporteur_id', 'Deliberation.circuit_id',
                'Deliberation.typeacte_id',
                'Deliberation.theme_id', 'Deliberation.service_id',
                'Typeacte.name', 'Nature.code',
            ],
            'joins' => array_merge($joins, $this->handleJoins()),
            'contain' => [
                'User' => ['fields' => ['id', 'nom', 'prenom']],
                'Redacteur' => ['fields' => ['id', 'nom', 'prenom']],
                'Annexe' => ['fields' => ['id', 'filename'], 'order' => ['position']],
                'Service' => ['fields' => ['name']],
                'Theme' => ['fields' => ['libelle']],
                'Circuit' => ['fields' => ['nom']],
                'DeliberationTypeseance' => ['fields' => ['id'],
                    'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action'],
                    ]],
                'DeliberationSeance' => ['fields' => ['id'],
                    'Seance' => ['fields' => ['id', 'date', 'type_id'],
                        'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]]]],
            'conditions' => $conditions,
            'order' => ['Deliberation.id' => 'DESC'],
            'recursive' => -1,
            'limit' => 10,
            'allow' => ['typeacte_id'],
        ];

        $projets = $this->Paginator->paginate('Deliberation');

        $this->ProjetTools->sortProjetSeanceDate($projets);
        $newFiltre = $this->ajouterFiltre($projets);
        // Ajout du filtre rédacteur
        if ($newFiltre && !empty($projets)) {
            $users = $this->User->find('all', [
                'fields' => ['id', 'nom', 'prenom'],
                'conditions' => ['active' => true],
                'order' => ['User.nom' => 'ASC'],
                'recursive' => -1]);
            $users = Hash::combine($users, '{n}.User.id', ['%s %s', '{n}.User.nom', '{n}.User.prenom']);
            $this->Filtre->addCritere('RedacteurId', [
                'field' => ['Deliberation.redacteur_id', 'DeliberationUser.user_id'],
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Rédacteur du projet'),
                    'data-placeholder' => __('Sélectioner un rédacteur'),
                    'data-allow-clear' => true,
                    'options' => $users
                ]]);
        }
        $this->set('crumbs', [__('Mes projets'), __('Mon service')]);
        $this->afficheProjets(
            'traitement_lot',
            $projets,
            __('Projets dont le rédacteur fait partie de mon service')
        );
    }

    /**
     * @version 5.1
     * @since 4.3
     * @access private
     * @param type $projets
     */
    private function ajouterFiltre(&$projets)
    {
        if ($this->params['render'] === 'bannette') {
            return false;
        }

        if (!$this->Filtre->critereExists() && !empty($projets)) {
            $seances = $this->Seance->find('all', [
                'fields' => ['Seance.id', 'Seance.type_id', 'Seance.date'],
                'conditions' => ['Seance.traitee' => 0],
                'contain' => ['Typeseance.libelle', 'Typeseance.retard'],
                'order' => ['Typeseance.libelle' => 'ASC', 'Seance.date' => 'ASC'],
            ]);

            $options_seances = [];
            foreach ($seances as $seance) {
                //Voir tous les projets ou tous les futurs dates avec un delais respecté
                $options_seances[$seance['Seance']['id']] =
                  $seance['Typeseance']['libelle'] .
                  ' : ' .
                  CakeTime::i18nFormat($seance['Seance']['date'], '%A %e %B %Y') .
                  ' à ' .
                  CakeTime::i18nFormat($seance['Seance']['date'], '%k:%M');
            }

            $this->Filtre->addCritere('DeliberationSeanceId', [
                'field' => 'DeliberationSeance_Filter.seance_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner une séance'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'label' => __('Séances'),
                    'options' => $options_seances]]);

            $typeseances = $this->Typeseance->find(
                'list',
                ['fields' => ['id', 'libelle'], 'recursive' => -1, 'order' => ['Typeseance.libelle' => 'ASC']]
            );
            $this->Filtre->addCritere('DeliberationTypeseanceId', [
                'field' => 'DeliberationTypeseance_Filter.typeseance_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un type de séance'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'label' => __('Type de séance'),
                    'options' => $typeseances]]);

            $typeactes = $this->Deliberation->Typeacte->find('list', [
                'recursive' => -1,
                'order' => ['Typeacte.name' => 'ASC'],
                'allow' => ['Typeacte.id' => 'read']]);

            $this->Filtre->addCritere('Typeacte', [
                'field' => 'Deliberation.typeacte_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Type d\'acte'),
                    'multiple' => true,
                    'data-placeholder' => __('Sélectionner un type d\'acte'),
                    'data-allow-clear' => true,
                    'options' => $typeactes
                ]]);

            $themes = $this->Deliberation->Theme->generateTreeListByOrder(
                ['Theme.actif' => '1'],
                '&nbsp;&nbsp;&nbsp;&nbsp;'
            );
            $this->Filtre->addCritere('ThemeId', [
                'field' => 'Deliberation.theme_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un thème'),
                    'data-allow-clear' => true,
                    'label' => __('Thème'),
                    'multiple' => true,
                    'options' => $themes,
                    'escape' => false
                ]]);
            $services = $this->Service->find('list', [
                'fields' => ['id', 'name'],
                'order' => ['Service.name' => 'ASC'],
                'conditions' => ['actif' => true], 'recursive' => -1]);
            $this->Filtre->addCritere('ServiceId', [
                'field' => 'Deliberation.service_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un service émetteur'),
                    'data-allow-clear' => true,
                    'label' => __('Service émetteur'),
                    'multiple' => true,
                    'options' => $services,
                ]]);
            $circuits = $this->Circuit->find('list', [
                'fields' => ['id', 'nom'],
                'order' => ['Circuit.nom' => 'ASC'],
                'conditions' => ['actif' => true], 'recursive' => -1]);
            $this->Filtre->addCritere('CircuitId', [
                'field' => 'Deliberation.circuit_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un circuit'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'label' => __('Circuit de validation'),
                    'options' => $circuits,
                ]]);

            $etats = $this->ProjetTools->generateListEtat($this->request->param('action'));
            if (!empty($etats)) {
                $this->Filtre->addCritere('Etat', [
                'field' => 'Deliberation.etat',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un état'),
                    'data-allow-clear' => true,
//                    ici le multi déconne
                    'label' => __('État du projet'),
//                    'empty' => __('Tous'),
                    'multiple' => true,
                    'options' => $etats
                ]]);
            }

            return true;
        }

        return false;
    }

    /**
     * @version 5.1
     * @version 4.3
     * @access private
     * @param type $projets
     */
    private function ajouterFiltreActe(&$projets)
    {
        if (!$this->Filtre->critereExists() && !empty($projets)) {
            $typeactes = $this->Deliberation->Typeacte->find('list', [
                'order' => ['Typeacte.name' => 'ASC'],
                'recursive' => -1,
                'allow' => ['Typeacte.id' => 'read']]);
            $this->Filtre->addCritere('Typeacte', [
                'field' => 'Deliberation.typeacte_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Type d\'acte'),
                    'data-placeholder' => __('Sélectionner un type d\'acte'),
                    'data-allow-clear' => true,
                    'options' => $typeactes
                ]]);

            $themes = $this->Deliberation->Theme->generateTreeListByOrder(
                ['Theme.actif' => '1'],
                '&nbsp;&nbsp;&nbsp;&nbsp;'
            );
            $this->Filtre->addCritere('ThemeId', [
                'field' => 'Deliberation.theme_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Thème'),
                    'data-placeholder' => __('Sélectionner un thème'),
                    'data-allow-clear' => true,
                    'options' => $themes,
                    'escape' => false
                ]]);

            $services = $this->Service->find('list', [
                'fields' => ['id', 'name'],
                'order' => ['Service.name' => 'ASC'],
                'conditions' => ['actif' => true], 'recursive' => -1]);
            $this->Filtre->addCritere('ServiceId', [
                'field' => 'Deliberation.service_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un service émetteur'),
                    'data-allow-clear' => true,
                    'label' => __('Service émetteur'),
                    'multiple' => true,
                    'options' => $services
                ]]);

            $users = $this->User->find('all', [
                'fields' => ['id', 'nom', 'prenom'],
                'order' => ['User.nom' => 'ASC'],
                'conditions' => ['active' => true],
                'recursive' => -1]);
            $users = Hash::combine($users, '{n}.User.id', ['%s %s', '{n}.User.nom', '{n}.User.prenom']);

            $this->Filtre->addCritere('RedacteurId', [
                'field' => ['Deliberation.redacteur_id', 'DeliberationUser.user_id'],
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un rédacteur'),
                    'data-allow-clear' => true,
                    'label' => __('Rédacteur du projet'),
                    'empty' => __('Tous'),
                    'options' => $users
                ]]);

            $circuits = $this->Circuit->find('list', [
                'fields' => ['id', 'nom'],
                'order' => ['Circuit.nom' => 'ASC'],
                'conditions' => ['actif' => true], 'recursive' => -1]);
            $this->Filtre->addCritere('CircuitId', [
                'field' => 'Deliberation.circuit_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un circuit'),
                    'data-allow-clear' => true,
                    'label' => __('Circuit de validation'),
                    'empty' => __('Tous'),
                    'options' => $circuits
                ]]);

            $etats = $this->ProjetEtat->generateListEtat($this->request->param('action'));
            $this->Filtre->addCritere('Etat', [
                'field' => 'Deliberation.etat',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un état'),
                    'data-allow-clear' => true,
                    'label' => __('État du projet'),
                    'empty' => __('Tous'),
                    'options' => $etats
                ]]);
        }
    }

    /**
     * @access private
     * @param type $conditions
     * @return type
     */
    private function handleJoins()
    {
        $joins = [
            ['table' => 'deliberations_seances',
                'alias' => 'DeliberationSeance_Filter',
                'type' => 'LEFT',
                'conditions' => [
                    'Deliberation.id = DeliberationSeance_Filter.deliberation_id',
                ]
            ],
            ['table' => 'seances',
                'alias' => 'Seance_Filter',
                'type' => 'LEFT',
                'conditions' => [
                    'DeliberationSeance_Filter.seance_id = Seance_Filter.id'
                ]
            ],
            ['table' => 'deliberations_typeseances',
                'alias' => 'DeliberationTypeseance_Filter',
                'type' => 'LEFT',
                'conditions' => [
                    'Deliberation.id = DeliberationTypeseance_Filter.deliberation_id'
                ]
            ],
        ];

        return $joins;
    }

    /**
     * Fonction de fusion d'un projet ou d'une délibération avec envoi du résultat vers le client
     *
     * @access public
     * @param integer $id --> id du projet ou de la délibération
     * @param integer $cookieToken --> numéro de cookie du client pour masquer la fenêtre attendable
     * @return CakeResponse
     * @throws Exception
     */
    public function genereFusionToClient($id, $cookieTokenKey)
    {
        try {
            $this->Progress->start($cookieTokenKey);

            // vérification de l'existence du projet/délibération en base de données
            if (!$this->Deliberation->hasAny(['id' => $id])) {
                throw new Exception(__('Projet/délibération #%s non trouvé(e) en base de données', $id));
            }
            $this->Progress->at(10, __('Récupération des données à générer'));
            // fusion du document
            $this->Deliberation->Behaviors->load('OdtFusion', ['id' => $id]);

            $this->Progress->at(40, __('Fusion du document'));
            $this->Deliberation->odtFusion();
            $filename = $this->Deliberation->fusionName();

            // selon le format d'envoi du document (pdf ou odt)
            if ($this->Auth->user('formatSortie') === 'pdf') {
                $this->Progress->at(70, __('Conversion du document'));
                $typemime = "application/pdf";
                $filename = $filename . '.pdf';
                $content = $this->Deliberation->getOdtFusionResult();
            } else {
                $this->Progress->at(70, __('Actualisation du sommaire'));
                $typemime = "application/vnd.oasis.opendocument.text";
                $filename = $filename . '.odt';
                $content = $this->Deliberation->getOdtFusionResult('odt');
            }
            unset($this->Deliberation->odtFusionResult);
            $this->Progress->at(90, __('Préparation du fichier de sortie'));
            $this->Progress->setFile($filename, $typemime, $content);

            $this->Progress->stop();
        } catch (Exception $e) {
            $this->Progress->error($e);
            $this->log(
                'Fusion :' . $e->getMessage() . __(' Fichier ') . $e->getFile()
                . __(' Ligne ') . $e->getLine(),
                'error'
            );
        }

        $this->autoRender = false;
        $this->response->type(['json' => 'text/x-json']);
        $this->RequestHandler->respondAs('json');
        $this->response->body(json_encode($this->Session->read('Progress.Tokens')));
        return $this->response;
    }

    /**
     * Fonction de fusion d'un projet ou d'une délibération avec envoi du résultat vers le client
     *
     * @access public
     * @param integer $id --> id du projet ou de la délibération
     * @param integer $cookieToken --> numéro de cookie du client pour masquer la fenêtre attendable
     * @return CakeResponse
     * @throws Exception
     */
    public function genereFusionToClientByModelTypeNameAndSeanceId($modeltype, $seanceId, $id, $cookieTokenKey)
    {
        try {
            $this->Progress->start($cookieTokenKey);

            if (!$this->Seance->hasAny(['id' => $seanceId])) {
                throw new Exception(__('Seance #%s non trouvé(e) en base de données', $id));
            }
            // vérification de l'existence du projet/délibération en base de données
            if (!$this->Deliberation->hasAny(['id' => $id])) {
                throw new Exception(__('Projet/délibération #%s non trouvé(e) en base de données', $id));
            }
            $this->Progress->at(10, __('Récupération des données à générer'));

            $seance = $this->Seance->find('first', [
                'fields' => ['Seance.id'],
                'contain' => [
                    'Typeseance' => ['fields' => [__('modele_%s_id', $modeltype)]]
                ],
                'conditions' => ['Seance.id' => $seanceId],
                'recursive' => -1
            ]);

            if (empty($seance['Typeseance'][__('modele_%s_id', $modeltype)])) {
                throw new Exception(
                    __('modele_%s_id %s non trouvé(e) en base de données', $modeltype, $seanceId)
                );
            }

            // fusion du document
            $this->Deliberation->Behaviors->load('OdtFusion', [
                'id' => $id,
                'modelTemplateId' => $seance['Typeseance'][__('modele_%s_id', $modeltype)],  //Model de projet
                'modelOptions' => ['modelTypeName' => $modeltype, 'seance_id' => $seanceId]
            ]);
            $this->Progress->at(40, __('Fusion du document'));
            $this->Deliberation->odtFusion();
            $filename = $this->Deliberation->fusionName();

            // selon le format d'envoi du document (pdf ou odt)
            if ($this->Auth->user('formatSortie') === 'pdf') {
                $this->Progress->at(70, __('Conversion du document'));
                $typemime = "application/pdf";
                $filename = $filename . '.pdf';
                $content = $this->Deliberation->getOdtFusionResult();
            } else {
                $this->Progress->at(70, __('Actualisation du document'));
                $typemime = "application/vnd.oasis.opendocument.text";
                $filename = $filename . '.odt';
                $content = $this->Deliberation->getOdtFusionResult('odt');
            }
            unset($this->Deliberation->odtFusionResult);
            $this->Progress->at(90, __('Préparation du fichier de sortie'));
            $this->Progress->setFile($filename, $typemime, $content);

            $this->Progress->stop();
        } catch (Exception $e) {
            $this->Progress->error($e);
            $this->log(
                'Fusion :' . $e->getMessage()
                . __(' Fichier ') . $e->getFile() . __(' Ligne ') . $e->getLine(),
                'error'
            );
        }

        $this->autoRender = false;
        $this->response->type(['json' => 'text/x-json']);
        $this->RequestHandler->respondAs('json');
        $this->response->body(json_encode($this->Session->read('Progress.Tokens')));
        return $this->response;
    }

    /**
     * @access public
     * @param type $anterieure_id
     * @param type $nb_recursion
     * @param type $listeAnterieure
     * @param type $action
     * @return type
     */
    private function chercherVersionAnterieure($anterieure_id, $nb_recursion, $listeAnterieure, $action)
    {
        if ($anterieure_id != 0) {
            $projet = $this->Deliberation->find('first', [
                'fields' => ['id', 'created', 'anterieure_id'],
                'conditions' => ["Deliberation.id" => $anterieure_id],
                'recursive' => -1,
            ]);

            $date_version = $projet['Deliberation']['created'];
            $listeAnterieure[$nb_recursion]['id'] = $anterieure_id;

            $listeAnterieure[$nb_recursion]['lien'] = Router::url(
                ['controller' => 'projets', 'action' => $action, $anterieure_id]
            );
            $listeAnterieure[$nb_recursion]['date_version'] = $date_version;
            //on stocke les id des delibs anterieures
            $listeAnterieure = $this->chercherVersionAnterieure(
                $projet['Deliberation']['anterieure_id'],
                $nb_recursion + 1,
                $listeAnterieure,
                $action
            );
        }
        return $listeAnterieure;
    }

    /**
     * Générer la liste des rédacteurs
     *
     * @access private
     * @param integer $redactor Rédacteur principal
     * @param string $order_by Pour ordonner la liste
     * @return array
     *
     * @version 5.1
     * @since 4.3
     */
    private function generateListRedacteurs($redactor = null, $aco = null)
    {
        if ($this->Acl->check(['User' => ['id' => $this->Auth->user('id')]], $aco, 'read')) {
            // on recherche ensuite les rédacteurs attachés à ce service

            $this->User->virtualFields['name'] = 'User.nom || \' \' || User.prenom';
            $users = $this->User->find('list', [
                'fields' => ['id', 'name','modified','active'],
                'order' => ['nom' => 'asc'],
                'conditions'=> ['active'=>true],
                'recursive' => -1,
            ]);
            unset($this->User->virtualFields['name']);
        } else {
            $joins_conditions = [];
            if (empty($redactor)) {
                // Si pas de rédacteur on chercher les services auxquels appartient l'utilisateur
                $joins_conditions['ServiceUser.service_id'] = array_keys($this->Auth->user('Service'));
            } else {
                // on cherche les services auxquels appartient le rédacteur principal
                $services = $this->Deliberation->Redacteur->find('first', [
                    'fields' => ['Redacteur.id'],
                    'conditions' => ['Redacteur.id' => $redactor],
                    'contain' => ['Service' => ['fields' => ['Service.id']]],
                    'recursive' => -1,
                ]);

                if (!empty($services) && !empty($services['Service'])) {
                    $joins_conditions['ServiceUser.service_id'] =
                        array_values(Hash::extract($services, 'Service.{n}.id'));
                }
            }
            $joins = [
                ['table' => 'services_users',
                    'alias' => 'ServiceUser',
                    'type' => 'RIGHT',
                    'conditions' => [
                        'User.id = ServiceUser.user_id',
                        $joins_conditions
                    ]
                ],
            ];

            // on recherche ensuite les rédacteurs attachés à ce service
            $users = $this->User->find('all', [
                'fields' => ['id', 'username', 'nom', 'prenom', 'ServiceUser.service_id'],
                'conditions' => ['User.active' => true],
                'joins' => $joins,
                'order' => ['nom' => 'asc'],
                'recursive' => -1,
            ]);

            if (empty($users)) {
                return [];
            }

            $users = Hash::combine($users, '{n}.User.id', ['%s %s', '{n}.User.nom', '{n}.User.prenom']);
        }

        //Retrait du rédacteur si rédacteur renseigné (add/edit projet)
        if (isset($redactor) && isset($users[$redactor])) {
            unset($users[$redactor]);
        }


        return $users;
    }

    /**
     * @version 5.1
     * @return \Cake\Network\Response|null
     *
     */
    public function cancel()
    {
        $this->TokensMethods->clear();
        return $this->redirect($this->previous);
    }

    /**
     * Générer la liste des rédacteurs
     *
     * @access private
     * @param integer $redac_principal_id Rédacteur principal
     * @param string $aco Le droit nécessaire pour changer le rédacteur
     * @param boolean $allow_current_redactor Détermine si l'utilisateur
     * loggé doit figurer dans la liste des utilisateurs
     * @return array
     *
     * @version 5.1.2
     * @since 4.3
     */
    private function listRedacteurs($redac_principal_id = null, $aco = null, $allow_current_redactor = false)
    {
        if ($this->Acl->check(['User' => ['id' => $this->Auth->user('id')]], $aco, 'read')) {
            // on recherche ensuite les rédacteurs attachés à ce service

            $this->User->virtualFields['name'] = 'User.nom || \' \' || User.prenom';
            $users = $this->User->find('list', [
              'fields' => ['id', 'name'],
              'conditions' => ['User.active' => true],
              'order' => ['nom' => 'asc'],
              'recursive' => -1,
            ]);

            unset($this->User->virtualFields['name']);
        } else {
            $joins_conditions = [];
            if (empty($redac_principal_id)) {
                // Si pas de rédacteur on chercher les services auxquels appartient l'utilisateur
                $joins_conditions['ServiceUser.service_id'] = array_keys($this->Auth->user('Service'));
            } else {
                // on cherche les services auxquels appartient le rédacteur principal
                $services = $this->Deliberation->Redacteur->find('first', [
                  'fields' => ['Redacteur.id'],
                  'conditions' => ['Redacteur.id' => $redac_principal_id],
                  'contain' => ['Service' => ['fields' => ['Service.id']]],
                  'recursive' => -1,
                ]);

                if (!empty($services) && !empty($services['Service'])) {
                    $joins_conditions['ServiceUser.service_id'] =
                        array_values(Set::extract('/Service/id', $services));
                }
            }
            $joins = [
              ['table' => 'services_users',
                  'alias' => 'ServiceUser',
                  'type' => 'RIGHT',
                  'conditions' => [
                      'User.id = ServiceUser.user_id',
                      $joins_conditions
                  ]
              ],
            ];
            // on recherche ensuite les rédacteurs attachés à ce service
            $users = $this->User->find('all', [
              'fields' => ['id', 'username', 'nom', 'prenom', 'ServiceUser.service_id'],
              'conditions' => ['User.active' => true],
              'joins' => $joins,
              'order' => ['nom' => 'asc'],
              'recursive' => -1,
            ]);

            if (empty($users)) {
                return [];
            }

            $users = Set::combine($users, '{n}.User.id', ['%s %s', '{n}.User.nom', '{n}.User.prenom']);
        }
        //Ajout de l'utilisateur loggé
        if ($allow_current_redactor && !isset($users[$this->Auth->user('id')])) {
            $users = array_merge($users, [
              $this->Auth->user('id') => __(
                  '%s %s',
                  $this->Auth->user('nom'),
                  $this->Auth->user('prenom')
              )
            ]);
        }
        //Retrait du rédacteur principal
        if (isset($redac_principal_id) && isset($users[$redac_principal_id])) {
            unset($users[$redac_principal_id]);
        }
        return $users;
    }
    private function isProjetLocked($deliberation)
    {
        $locked = $this->TokensMethods->get(['entity_id'=>$deliberation['Deliberation']['id']]);

        if ($this->request->is(['post', 'put'])) {
            if (!$this->TokensMethods->haveToken(
                [
                    'controller' => 'projets',
                    'action' => 'edit',
                    'entity_id' => $deliberation['Deliberation']['id']
                ]
            )
            ) {
                $this->Flash->set(
                    __(
                        "Attention, les modifications n'ont pas été enregistrées car un "
                        . "administrateur a supprimé votre verrou. \n"
                        . "Pour plus d'informations, veuillez contacter votre administrateur"
                    ),
                    [
                        'element' => 'growl','params' => ['type' => 'danger', 'settings' => ['delay' => 10000]]]
                );

                $this->redirect($this->previous);
            }
        }
        if ($locked === false) {
            $this->Flash->set(
                __('Vous ne pouvez pas modifier ce projet, car il est actuellement édité par un autre utilisateur.' .
                    "\n" .
                    'Pour plus d\'informations, veuillez contacter votre administrateur'),
                [
                    'element' => 'growl',
                    'params' => [
                        'type' => 'warning'
                    ]
                ]
            );

            $this->redirect($this->previous);
        }
    }

    private function isProjetCanModified($id, $deliberation)
    {
        //FIX Droit
        if (!$this->Acl->check(
            ['User' => ['id' => $this->Auth->user('id')]],
            ['Typeacte' => ['id' => $deliberation['Deliberation']['typeacte_id']]],
            'update'
        )
        ) {
            $this->Flash->set(__("Vous ne pouvez pas éditer le projet '%s' en raison de son type d'acte.", $id), [
                'element' => 'growl',
                'params' => ['type' => 'danger']
            ]);

            $this->redirect($this->previous);
        }

        // teste si le projet est modifiable par l'utilisateur connecté
        if (!$this->Acl->check(['User' => ['id' => $this->Auth->user('id')]], 'Projets', 'update')
            || !$this->Deliberation->estModifiable(
                $id,
                $this->Auth->user('id'),
                $this->Acl->check(['User' => ['id' => $this->Auth->user('id')]], 'editerTous', 'read')
            )
        ) {
            $this->Flash->set(
                __("Vous n'avez pas les droits pour éditer le projet '%s'.", $id),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );

            $this->redirect($this->previous);
        }
    }
}
