<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Controller', 'Controller');
App::uses('ConnectionManager', 'Model');

/**
 * App Controller pour webdelib
 * @package   app.Controller
 * @version   5.1.2
 * @since     1.0.0
 */
class AppController extends Controller
{
    public $components = [
        'Flash',
        'Acl',
        'Auth' => [
            'loginAction' => ['admin' => false, 'plugin' => null, 'controller' => 'users', 'action' => 'login'],
            'logoutRedirect' => ['admin' => false, 'plugin' => null, 'controller' => 'users', 'action' => 'login'],
            //'unauthorizedRedirect' => false,
            'loginRedirect' => ['admin' => false, 'plugin' => null, 'controller' => 'dossiers', 'action' => 'index'],
            'authError' => 'Vous n\'êtes pas autorisé à effectuer cette action !',
            //'allowedActions'=>array('Pages'),//Actions sans authentification
            'authenticate' => [
                //Paramètre à passer pour tous les objets d'authentifications
                'all' => [
                    'userModel' => 'User',
                    'scope' => ['User.active' => true],
                ],
                'AuthManager.Cas' => [
                    'loginAction' => [
                        'admin' => false, 'plugin' => null, 'controller' => 'users', 'action' => 'caslogin'
                    ],
                    'logoutRedirect' => [
                        'admin' => false, 'plugin' => null, 'controller' => 'users', 'action' => 'caslogout'
                    ],
                    'except' => ['admin']],
                'AuthManager.Ldap' => [
                    'except' => ['admin']],
                'Form' => [
                    'passwordHasher' => [
                        'className' => 'AuthManager.SimpleNotSecuritySalt',
                        'hashType' => 'md5'
                    ]
                ]
            ],
            'authorize' => ['Controller'],
        ],
        'Session',
        'History' => [
            'unAuthorize' => [
                '/add',
                '/edit',
                '/delete',
                'duplicate',
                'components/',
                'downloadAttached',
                'downloadZip',
                'download/',
                'downloadTampon/',
                'downloadTdtTampon',
                'downloadTdtMessage',
                'deliberations/download',
                'debats/download',
                'downloadBordereau/',
                'downloadGabarit/',
                'genereToken/',
                'genereFusionToClient/',
                'genereFusionToFiles/',
                'getProgress',
                'getTampon/',
                'getBordereauTdt/',
                'img/',
                'components/',
                'files/',
                'webdav/',
                'TraitementParLot',
                'connecteurs/maintenance',
                'downloadVotesElectroniqueJson',
                'favicon',
                'getGeoJsonFromApi',
                'notification',
                'search/quickSearch',
                'collectivites/rgpd',
                'importVotesIdelibre',
                'Validations/validerEnUrgence',
                'Validations/rebond',
                'Validations/retour',
                'votes/copyFromPrevious',
                'sendDeliberationsToSignature',
                'sendDeliberationsToSignature',
                'visuCircuit',
                'projets/cancel',
                'fusion_download'
            ]
        ],
        'ProjetTools',
        'TokensMethods'
    ];
    public $helpers = [
        'Flash',
        'Html',
        'BHtml' => [
            'className' => 'Bootstrap3.BootstrapHtml'],
        'BForm' => [
            'className' => 'Bootstrap3.BootstrapForm'],
        'Session',
        'Html2',
        'Lock',
        'Bs', 'BsForm',
        'AuthManager.Permissions',
        'Navbar' => [
            'className' => 'Bootstrap3.BootstrapNavbar']
    ];

    /**
     * $uses
     * @var [array]
     */
    public $uses = [
        'Acteur',
        'Annexe',
        'Commentaire',
        'Deliberation',
        'ModelOdtValidator.Modeltemplate',
        'Nature',
        'Nomenclature',
        'Cakeflow.Traitement',
        'Typeseance',
        'Typologiepiece',
        'Service',
        'Collectivite',
        'User'
    ];
    /**
     * @var array|int[]|mixed|Shell
     */
    public $fqdnDataBases;

    public function __construct($request = null, $response = null)
    {
        $this->initConfigMultiBase();
        parent::__construct($request, $response);
    }

    /**
     * @version 4.3
     * @access public
     */
    public function beforeFilter()
    {
        parent::beforeFilter();

        //initialisation des mapActions pour les droits CRUD
        if (isset($this->components['Auth']['mapActions'])) {
            //Mise en place des actions publiques
            if (isset($this->components['Auth']['mapActions']['allow'])) {
                $this->Auth->allow($this->components['Auth']['mapActions']['allow']);
                unset($this->components['Auth']['mapActions']['allow']);
            }

            $this->Auth->mapActions($this->components['Auth']['mapActions']);
        }

        $isWebdav = ($this->request->params['controller'] === 'webdav');
        //Pas d'autentification pour les requesteds
        if (isset($this->request->params['requested']) || $isWebdav) {
            $this->Auth->allow($this->action);
            parent::beforeFilter();
            return;
        }

        $user = $this->Auth->user();
        $route = Router::normalize($this->request->params);
        $isLoginPage = in_array($route, ['/users/login','/users/caslogin'], true);
        $isLogoutPage = in_array($route, ['/users/logout','/users/caslogout'], true);
        $isAllo = ($this->request->params['controller'] === 'allos');
        $isWopi = ($this->request->params['controller'] === 'wopi');
        if (!$isLoginPage && !$isLogoutPage && !$isAllo && !$isWopi && empty($user)) {
            //le forcer a se connecter
            $this->Session->write('Auth.loginRedirect', $this->here);
            if (Configure::read('AuthManager.Authentification.use')
                && Configure::read('AuthManager.Authentification.type') === 'CAS'
            ) {
                $this->redirect($this->Auth->authenticate['AuthManager.Cas']['loginAction']);
            }
            $this->redirect($this->components['Auth']['loginAction']);
        }

        // Désactivation du cache du navigateur: (quand on revient en arrière dans l'historique de
        // navigation, la page n'est pas cachée du côté du navigateur, donc il ré-exécute la demande)
        //CakeResponse::disableCache();
        // passage de paramètre en utilisant 'all'
        CakeLog::info("user_id ({$this->Auth->user('id')}) => " . Router::normalize($this->request->here()));
        // We can remove this line after we're finished
        //$this->Auth->allow();
        if (!preg_match('/^\/deliberations\/edit|\/annexes\/download|\/webdav|favicon.ico/', $route)
            && !$this->request->is('ajax')
        ) {
            //CakeLog::debug(__('[Token][%s]clear: %s', __METHOD__, var_export($this->request->is('ajax'), true)));
            //CakeLog::debug(__('[Token][%s]clear: %s', __METHOD__, var_export($route, true)));
            $this->TokensMethods->clear();
        }

        $this->set('name', $this->name);
    }

    /**
     * @version 4.3
     * @access public
     */
    public function afterFilter()
    {
        parent::afterFilter();
    }

    /**
     * @version 4.3
     * @access public
     */
    public function beforeRender()
    {
        //Si utilisateur connecté
        if (!empty($this->Auth->user())) {
            $user = $this->User->find('first', [
                'conditions' => ['User.id' => $this->Auth->user('id')],
                'contain' => 'Service',
                'recursive' => -1,
            ]);

            $services = Hash::combine($user['Service'], '{n}.id', '{n}.name');

            foreach ($services as $idService => $nameService) {
                if ($idService === $user['User']['service_defaut_id']) {
                    $this->Session->write('Auth.User.ServiceEmetteur.id', $idService);
                    $this->Session->write('Auth.User.ServiceEmetteur.name', $nameService);
                }
            }

            if (!array_key_exists($user['User']['service_defaut_id'], $services)) {
                $this->Session->write('Auth.User.ServiceEmetteur.id', array_keys($services)[0]);
                $this->Session->write('Auth.User.ServiceEmetteur.name', array_values($services)[0]);
                $user['User']['service_defaut_id'] = array_keys($services)[0];
            }

            $this->set('infoUser', $this->Auth->user('prenom') . ' ' . $this->Auth->user('nom'));
            $this->set('infoFormatSortie', $this->Auth->user('formatSortie'));
            $this->set('infoServiceEmeteur', $this->Auth->user('ServiceEmetteur.name'));
            $this->set('infoCollectivite', ['nom' => $this->Session->read('Collectivite.nom')]);
            $theme = $this->Auth->user('theme');
            if (!empty($theme)) {
                $this->set('theme', $this->Auth->user('theme'));
            }
            if (isset($user['User']['preference_first_login_active'])
                && !$user['User']['preference_first_login_active']
            ) {
                $this->set('notificationsForm', $user);
                $this->set('notif', ['1' => __('oui'), '0' => __('non')]);
                $this->set('not_accept_notif', $user['User']['accept_notif']==true ? false : true);
            }
        } else {
            $this->set('theme', 'Normal');
        }

        $this->set('base_url', $this->base);
    }

    /**
     *
     * @access protected
     *
     * @param type $data
     * @param type $key
     * @return type
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    protected function selectedArray($data, $key_id = 'id')
    {
        if (!is_array($data)) {
            $model = $data;
            if (!empty($this->data[$model][$model])) {
                return $this->data[$model][$model];
            }
            if (!empty($this->data[$model])) {
                $data = $this->data[$model];
            }
        }
        $array = [];
        if (!empty($data)) {
            foreach ($data as $key => $var) {
                if (isset($var[$key_id])) {
                    $array[$var[$key_id]] = $var[$key_id];
                } else {
                    $array[] = $var;
                }
            }
        }
        return $array;
    }

    /**
     * @param type $user
     * @return boolean
     * @access public
     */
    public function isAuthorized($user = null)
    {
        App::uses('Component', 'Controller');
        App::uses('ComponentCollection', 'Controller');
        App::uses('CrudAuthorize', 'AuthManager.Controller/Component/Auth');

        $collection = new ComponentCollection();
        $aclAdapter = $this->Acl->adapter();
        $aclAdapter->Permission->Behaviors->load('DynamicDbConfig');
        $aclAdapter->Aco->Behaviors->load('DynamicDbConfig');
        $aclAdapter->Aro->Behaviors->load('DynamicDbConfig');
        $this->Acl->adapter($aclAdapter);
        $collection->set('Acl', $this->Acl);
        $CrudAuthorize = new CrudAuthorize($collection);

        //initialisation des mapActions pour les droits CRUD
        if (isset($this->components['Auth']['mapActions'])) {
            $CrudAuthorize->mapActions($this->components['Auth']['mapActions']);
        }
        // Seulement les administrateurs peuvent accéder aux fonctions d'administration
        if (isset($this->request->params['manager']) && $user['Profil']['role_id'] !== 3) {
            return false;
        }

        // Seulement les administrateurs peuvent accéder aux fonctions d'administration
        if (isset($this->request->params['admin']) && $user['Profil']['role_id'] !== 2) {
            return false;
        }

        // Par défaut n'autorise pas
        return $CrudAuthorize->authorize(['id' => $this->Auth->user('id')], $this->request);
    }

    /**
     * Fonction générique pour afficher les projets sour forme d'index
     *
     * @param string $render
     * @param array $projets
     * @param string $titreVue
     * @param array $listeActions
     * @param array $listeLiens
     * @param int $nbProjets
     * @version 5.1
     * @since 4.3
     * @access protected
     */
    protected function afficheProjets(
        $render = 'index',
        &$projets = [],
        $titreVue = '',
        $listeActions = [],
        $listeLiens = [],
        $nbProjets = null
    ) {
        App::uses('Typeseance', 'Model');
        $typeseance = ClassRegistry::init('Typeseance');

        App::uses('ProjetEtat', 'Model');
        $projetEtat = ClassRegistry::init('ProjetEtat');

        // initialisation de l'utilisateur connecté et des droits
        $this->set('typeseances', $typeseance->find('list', ['recursive' => -1]));

        $this->request->data = $projets;

        //récupération des droits pour bouton
        $droits = $this->ProjetTools->getDroits([
            'Projets' => ['read', 'update', 'delete'],
            'attribuerCircuit' => 'read',
            'reattribuerTous' => 'read',
            'editerTous' => 'read',
            'supprimerTous' => 'read',
            'duplicate' => 'read',
            'goNext' => 'read',
            'validerEnUrgence' => 'read',
            'editPostSign' => 'read']);
        // Gestion par lot
        if ($render === 'traitement_lot') {
            $this->set('traitement_lot', true);
            $list_modeles = $this->Modeltemplate->find('list', [
                'recursive' => -1,
                'fields' => ['Modeltemplate.name'],
                'conditions' => ['modeltype_id' => [MODELE_TYPE_RECHERCHE]]
            ]);
            $generation = ['generation' => __('Générer un document des projets sélectionnés')];
            if (isset($this->viewVars['actions_possibles'])) {
                $this->viewVars['actions_possibles']['generation'] = __('Générer un document des projets sélectionnés');
            } elseif (!empty($list_modeles)) {
                $this->set('actions_possibles', $generation);
            }
            $this->set('modeles', $list_modeles);
            $render = 'index';
        }

        /* initialisation pour chaque projet ou délibération */
        if (!empty($this->request->data)) {
            foreach ($this->request->data as $i => $projet) {
                // initialisation des icônes
                if (isset($projet[0]) && $nbProjets === 1) {
                    $projet['Deliberation'] = $projet[0];
                }
                //On cherche à savoir si la délibération est vérrouillée par un utilisateur
                $this->ProjetTools->locked($this->request->data[$i]['Deliberation']);
                // Récupération du rapporteur principal
                $this->Acteur->virtualFields['name'] = 'Acteur.nom || \' \' || Acteur.prenom';
                $acteur = $this->Acteur->find('first', [
                    'fields' => ['name'],
                    'conditions' => ['Acteur.id' => $projet['Deliberation']['rapporteur_id']],
                    'recursive' => -1
                ]);
                $this->request->data[$i]['Deliberation']['rapporteur_principal'] =
                    $acteur['Acteur']['name'] ?? _('');

                $lastVisa = $this->Traitement->getLastVisaTrigger($projet['Deliberation']['id']);
                //Récupère l'étape courante
                $this->request->data[$i]['Circuit']['current_step'] =
                    $projet['Deliberation']['etat'] !== 0 ? $this->Traitement->getEtapeCouranteName(
                        $projet['Deliberation']['id']
                    ) : _('En cours de rédaction');

                if (!empty($lastVisa)) {
                    $this->request->data[$i]['last_viseur'] = $lastVisa['last_viseur'];
                    $this->request->data[$i]['last_viseur_date'] = $lastVisa['date'];
                }

                $this->request->data[$i]['Deliberation']['num_pref'] =
                    !empty($this->request->data[$i]['Deliberation']['num_pref']) ? $this->ProjetTools->getMatiereByKey(
                        $this->request->data[$i]['Deliberation']['num_pref']
                    ) : null;
                //recuperation des derniers commentaires
                $this->request->data[$i]['Deliberation']['commentaires'] = $this->Commentaire->find('all', [
                    'fields' => ['Commentaire.texte', 'Commentaire.created'],
                    'contain' => ['User.nom', 'User.prenom'],
                    'conditions' => [
                        'Commentaire.delib_id' => $projet['Deliberation']['id'],
                        'Commentaire.pris_en_compte' => 0
                    ],
                    'order' => ['Commentaire.created' => 'DESC'],
                    'recursive' => -1,
                    'limit' => 3
                ]);
                //recupération de la liste des annexes
                if (!empty($projet['Annexe'])) {
                    foreach ($projet['Annexe'] as $key => $annexe) {
                        $this->request->data[$i]['Deliberation']['annexes'][$key]['filename'] = $annexe['filename'];
                        $this->request->data[$i]['Deliberation']['annexes'][$key]['id'] = $annexe['id'];
                    }
                }

                if ($projet['Deliberation']['etat'] == 0 && $projet['Deliberation']['anterieure_id'] != 0) {
                    $this->request->data[$i]['iconeEtat'] = $this->ProjetTools->iconeEtat(
                        $projet['Deliberation']['id'],
                        -2,
                        $projet['Nature']['code'] ?? $projet['Typeacte']['Nature']['code']
                    );
                } elseif ($projet['Deliberation']['etat'] == 1) {
                    //Recherche si l'auteur est dans le circuit de validation du projet
                    $estDansCircuit = $this->Traitement->triggerDansTraitementCible(
                        $this->Auth->user('id'),
                        $projet['Deliberation']['id']
                    );

                    $tourDansCircuit = $estDansCircuit ? $this->Traitement->positionTrigger(
                        $this->Auth->user('id'),
                        $projet['Deliberation']['id']
                    ) : 0;
                    $estRedacteur = ($this->Auth->user('id') == $projet['Deliberation']['redacteur_id']);
                    $this->request->data[$i]['iconeEtat'] = $this->ProjetTools->iconeEtat(
                        $projet['Deliberation']['id'],
                        $projet['Deliberation']['etat'],
                        $projet['Nature']['code'] ?? $projet['Typeacte']['Nature']['code'],
                        false,
                        $estDansCircuit,
                        $estRedacteur,
                        $tourDansCircuit
                    );
                } else {
                    $this->request->data[$i]['iconeEtat'] = $this->ProjetTools->iconeEtat(
                        $projet['Deliberation']['id'],
                        $projet['Deliberation']['etat'],
                        $projet['Nature']['code'] ?? $projet['Typeacte']['Nature']['code'],
                        $droits['editerTous/read']
                    );
                }
                $this->request->data[$i]['etat_libelle'] = $projetEtat->libelleEtat(
                    $projet['Deliberation']['etat'],
                    $projet['Nature']['code'] ?? $projet['Typeacte']['Nature']['code']
                );
                // initialisation des séances
                $listeTypeSeance = [];
                $this->request->data[$i]['listeSeances'] = [];
                if (isset($projet['DeliberationSeance']) && !empty($projet['DeliberationSeance'])) {
                    foreach ($projet['DeliberationSeance'] as $keySeance => $seance) {
                        if (!empty($seance['Seance']['id'])) {
                            $this->request->data[$i]['listeSeances'][] = ['seance_id' => $seance['Seance']['id'],
                                'type_id' => $seance['Seance']['type_id'],
                                'color' => !empty($seance['Seance']['Typeseance']['color']) ?
                                    $seance['Seance']['Typeseance']['color'] : '',
                                'action' => $seance['Seance']['Typeseance']['action'],
                                'libelle' => $seance['Seance']['Typeseance']['libelle'],
                                'date' => $seance['Seance']['date']];
                            $listeTypeSeance[] = $seance['Seance']['type_id'];
                        }
                    }
                }
                if (!empty($projet['DeliberationTypeseance'])) {
                    foreach ($projet['DeliberationTypeseance'] as $typeseance) {
                        if (!empty($typeseance['Typeseance']['id'])) {
                            if (!in_array($typeseance['Typeseance']['id'], $listeTypeSeance, true)) {
                                $this->request->data[$i]['listeSeances'][] = [
                                    'seance_id' => null,
                                    'type_id' => $typeseance['Typeseance']['id'],
                                    'color' => !empty($typeseance['Typeseance']['color']) ?
                                        $typeseance['Typeseance']['color'] : '',
                                    'action' => $typeseance['Typeseance']['action'],
                                    'libelle' => $typeseance['Typeseance']['libelle'],
                                    'date' => null
                                ];
                            }
                        }
                    }
                }

                $this->request->data[$i]['listeSeances'] = Hash::sort(
                    $this->request->data[$i]['listeSeances'],
                    '{n}.action',
                    'asc'
                );

                $this->ProjetTools->actionDisponible($this->request->data[$i], $droits, $listeActions);

                //remplissage des 9 cases depuis le json, pour chaque projet
                $this->request->data[$i]['fields'] = $this->ProjetTools->get9casesData($projet['Deliberation']['id']);

                // initialisation des dates, modèle et service
                //$seances_id = array();

                if (isset($this->request->data[$i]['listeSeances'])
                    && !empty($this->request->data[$i]['listeSeances'])
                ) {
                    foreach ($this->request->data[$i]['listeSeances'] as $seance) {
                        if ($seance['action'] === 0) {
                            $this->request->data[$i]['Modeltemplate']['id'] =
                                $this->Typeseance->modeleProjetDelibParTypeSeanceId(
                                    $seance['type_id'],
                                    $projet['Deliberation']['etat']
                                );
                            break;
                        }
                    }
                }
                if (!isset($this->request->data[$i]['Modeltemplate']['id'])) {
                    $this->request->data[$i]['Modeltemplate']['id'] =
                        $this->Deliberation->Typeacte->getModelId(
                            $projet['Deliberation']['typeacte_id'],
                            'modele_projet_id'
                        );
                }

                if (isset($this->data[$i]['Service']['id'])) {
                    $this->request->data[$i]['Service']['name'] =
                        $this->Deliberation->Service->doList($projet['Service']['id']);
                }
                if (isset($this->data[$i]['Deliberation']['date_limite'])) {
                    $this->request->data[$i]['Deliberation']['date_limite'] = $projet['Deliberation']['date_limite'];
                }
            }
        }
        $this->set('titreVue', $titreVue);
        $this->set('listeLiens', $listeLiens);
        if ($nbProjets == null) {
            $nbProjets = count($projets);
        }
        $this->set('nbProjets', $nbProjets);
        $this->render($render);
    }

    /**
     * Envois des actes vers le TDT
     *
     * @access protected
     *
     * @return type
     * @throws Exception
     *
     * @version 5.1.1
     * @since 4.3.0
     */
    protected function sendToTdt()
    {
        $this->Deliberation->Behaviors->load(
            'Version',
            $this->Deliberation->actsAsVersionOptionsList['Version']
        );

        if ($this->request->param('action') === 'depotManuel') {
            $this->depotManuel();
            return;
        }

        App::uses('Tdt', 'Lib');
        $tdt = new Tdt();

        if (!Configure::read('USE_TDT')) {
            $this->Flash->set(
                __(
                    'Erreur : TDT désactivé. Pour activer ce service, veuillez contacter votre administrateur.'
                ),
                [
                    'element' => 'growl',
                    'type' => 'error', 'settings' => ['delay' => 600000]
                ]
            );
            return $this->redirect($this->previous);
        }
        //Cases sélectionnées ?
        if (!empty($this->data['Deliberation'])) {
            $aDelibToSend = [];
            foreach ($this->data['Deliberation'] as $id => $acte) {
                $this->Deliberation->id = $id;
                if (!empty($acte['num_pref'])
                    || !empty($acte['typologiepiece_code'])
                    || !empty($acte['tdt_document_papier'])) {
                    $this->Deliberation->save(
                        [
                            'num_pref' => str_replace('_', '.', $acte['num_pref']),
                            'typologiepiece_code' => $acte['typologiepiece_code'],
                            'tdt_document_papier' => $acte['tdt_document_papier'],
                        ],
                        true,
                        ['num_pref', 'typologiepiece_code', 'tdt_document_papier']
                    );
                }
                if (!empty($acte['Annexe'])) {
                    foreach ($acte['Annexe'] as $annexe) {
                        $this->Annexe->id = $annexe['id'];
                        $this->Annexe->save(
                            [
                                'typologiepiece_code' => $annexe['typologiepiece_code'],
                            ],
                            true,
                            ['typologiepiece_code']
                        );
                        $this->Annexe->clear();
                    }
                }
                $aDelibToSend[] = $id;
                $this->Deliberation->clear();
            }
            $nbEnvoyee = 0;
            $infos = '';
            $error = false;
            //Pour chaque délib
            foreach ($aDelibToSend as $delib_id) {
                try {
                    $this->Deliberation->clear();
                    $this->Deliberation->id = $delib_id;
                    $this->Deliberation->recursive = -1;
                    $this->Deliberation->contain(['Typeacte']);
                    $acte = $this->Deliberation->read();
                    $acte['acte_nature_code'] = $this->Nature->getNatureCodeByTypeActe(
                        $acte['Deliberation']['typeacte_id']
                    );
                    $typologiepieceAnnexes = $this->Annexe->findAllByForeignKeyAndJoindreCtrlLegalite(
                        $acte['Deliberation']['id'],
                        true,
                        ['typologiepiece_code'],
                        ['position' => 'ASC'],
                        null,
                        null,
                        -1
                    );
                    $acte['annexe_typologiepiece_code'] = !empty($typologiepieceAnnexes) ?
                        Hash::extract($typologiepieceAnnexes, '{n}.Annexe.typologiepiece_code') : '';

                    if (empty($acte['acte_nature_code'])) {
                        throw new Exception(
                            __(
                                '%s (%s) : Aucune nature disponible.',
                                $this->Deliberation->field('objet_delib'),
                                $this->Deliberation->field('num_delib')
                            )
                        );
                    }
                    if (empty($acte['Deliberation']['num_pref'])) {
                        throw new Exception(
                            __(
                                '%s (%s) : Aucune classification sélectionnée.',
                                $this->Deliberation->field('objet_delib'),
                                $this->Deliberation->field('num_delib')
                            )
                        );
                    }
                    if (empty($acte['Deliberation']['typologiepiece_code'])) {
                        throw new Exception(
                            __(
                                '%s (%s) : Aucun type de pièce sélectionnée.',
                                $this->Deliberation->field('objet_delib'),
                                $this->Deliberation->field('num_delib')
                            )
                        );
                    }
                    if (!empty($typologiepieceAnnexes)) {
                        if (empty($acte['annexe_typologiepiece_code'])) {
                            throw new Exception(
                                __(
                                    '%s (%s) : Un ou plusieur type de pièce'
                                    .' n\'ont pas été sélectionné pour les annexes.',
                                    $this->Deliberation->field('objet_delib'),
                                    $this->Deliberation->field('num_delib')
                                )
                            );
                        }
                        if (count($typologiepieceAnnexes) !== count($acte['annexe_typologiepiece_code'])) {
                            throw new Exception(
                                __(
                                    '%s (%s) : Un ou plusieur type de pièce'
                                    .' n\'ont pas été sélectionné pour les annexes.',
                                    $this->Deliberation->field('objet_delib'),
                                    $this->Deliberation->field('num_delib')
                                )
                            );
                        }
                    }
                    $docPrincipal = $this->Deliberation->field('delib_pdf');
                    $annexes = $this->Deliberation->getAnnexesToSend($delib_id);
                    if ($tdt->send($acte, $docPrincipal, $annexes, $this->Auth->user('id')) == false) {
                        throw new Exception(
                            __(
                                '%s (%s) : Erreur d\'envoi.',
                                $this->Deliberation->field('objet_delib'),
                                $this->Deliberation->field('num_delib')
                            )
                        );
                    }
                } catch (Exception $e) {
                    $error = true;
                    $infos .= $e->getMessage() . "\n";
                    continue;
                }
                $nbEnvoyee++;
            }

            if (!empty($error)) {
                $this->Flash->set($infos, ['element' => 'growl', 'params' => ['type' => 'danger']]);
            } else {
                $this->Flash->set(
                    __('%s acte(s) envoyé(s) correctement au TdT', $nbEnvoyee)
                    . $infos
                    . "\n",
                    ['element' => 'growl', 'params' => ['type' => 'info']]
                );
            }
        } else {
            $this->Flash->set(__('Aucun Acte sélectionné'), ['element' => 'growl', 'params' => ['type' => 'danger']]);
        }
    }

    /**
     * [depotManuel description]
     * @return [type] [description]
     *
     * @version 5.1.1
     */
    private function depotManuel()
    {
        if (!empty($this->data['Deliberation'])) {
            $aDelibToSend = [];
            foreach ($this->data['Deliberation'] as $id => $dataDeliberation) {
                $this->Deliberation->id = $id;
                if (!empty($dataDeliberation['num_pref'])) {
                    $this->Deliberation->saveField('num_pref', str_replace('_', '.', $dataDeliberation['num_pref']));
                }

                if (!empty($dataDeliberation['send']) && $dataDeliberation == true) {
                    $aDelibToSend[] = $id;
                }
            }

            $nbEnvoyee = 0;
            $infos = '';
            $error = false;
            foreach ($aDelibToSend as $delib_id) {
                try {
                    $this->Deliberation->clear();
                    $this->Deliberation->id = $delib_id;
                    $this->Deliberation->recursive = -1;

                    $acte = $this->Deliberation->read();
                    $this->Deliberation->sendManually();
                } catch (Exception $e) {
                    $error = true;
                    $infos .= $e->getMessage() . "\n";
                    continue;
                }

                if (!empty($error)) {
                    $this->Flash->set($infos, ['element' => 'growl', 'params' => ['type' => 'danger']]);
                } else {
                    $this->Flash->set(
                        __(
                            '%s acte(s) signalé(s) comme déposé(s) au TdT',
                            count($aDelibToSend)
                        ) . "\n",
                        ['element' => 'growl', 'params' => ['type' => 'info']]
                    );
                }
            }
        } else {
            $this->Flash->set(__('Aucun Acte sélectionné'), ['element' => 'growl', 'params' => ['type' => 'danger']]);
        }
    }

    protected function editInfoSendToTdt()
    {
        $body = '';
        $statusCode = '200';
        try {
            if (!empty($this->data['Deliberation'])) {
                foreach ($this->data['Deliberation'] as $acte) {
                    $this->Deliberation->id = $acte['id'];
                    $this->Deliberation->save(
                        [
                            'num_pref' => str_replace('_', '.', $acte['num_pref']),
                            'typologiepiece_code' => $acte['typologiepiece_code'],
                            'tdt_document_papier' => $acte['tdt_document_papier'],
                        ],
                        true,
                        ['num_pref', 'typologiepiece_code', 'tdt_document_papier']
                    );
                    if (!empty($acte['Annexe'])) {
                        foreach ($acte['Annexe'] as $annexe) {
                            $this->Annexe->id = $annexe['id'];
                            $this->Annexe->save(
                                [
                                    'typologiepiece_code' => $annexe['typologiepiece_code'],
                                ],
                                true,
                                ['typologiepiece_code']
                            );
                            $this->Annexe->clear();
                        }
                    }
                    $this->Deliberation->clear();
                }
            }
        } catch (Exception $e) {
            $body = $e->getMessage();
            $statusCode = '500';
        }

        $this->autoRender = false;
        $this->response->type('html', 'text/html');
        $this->RequestHandler->respondAs('html');
        $this->response->statusCode($statusCode);
        $this->response->body($body);

        return $this->response;
    }

    /**
     * [public description]
     * @var [type]
     *
     * @version 5.1.2
     */
    protected function typologiePieceToTdtAjax()
    {
        $acte = $this->Deliberation->find(
            'first',
            [
                'fields' => [
                    'typologiepiece_code', 'typeacte_id', 'objet_delib',
                    'num_delib',
                    'num_pref', 'tdt_document_papier'],
                'contain' => [
                    'Annexe' => [
                        'fields' => ['id', 'filename', 'titre', 'typologiepiece_code'],
                        'conditions' => ['joindre_ctrl_legalite' => true],
                        'order' => ['Annexe.position' => 'ASC']
                    ]
                ],
                'conditions' => ['id' => $this->data['acte_id']],
                'recursive' => -1
            ]
        );

        $this->set('acte_id', $this->data['acte_id']);
        $this->set('nomenclatures', $this->Nomenclature->generateTreeListWithOptionGroup());
        $this->set(
            'typologiepieces',
            $this->getTypologiePiecesSendTdt('Principal', $acte['Deliberation']['typeacte_id'])
        );
        if (empty($acte['Deliberation']['typologiepiece_code'])) {
            $acte['Deliberation']['typologiepiece_code'] = $this->Nature->getNatureDefaultCodeByTypeActe(
                $acte['Deliberation']['typeacte_id']
            );
        }
        $acte['Deliberation']['num_pref'] = str_replace('.', '_', $acte['Deliberation']['num_pref']);
        $this->set(
            'typologiepieceAnnexes',
            $this->getTypologiePiecesSendTdt('Annexe', $acte['Deliberation']['typeacte_id'])
        );
        $this->set('acte', $acte);

        $this->view = '/Teletransmettre/typologie_piece_to_tdt_ajax';
    }

    /**
     *     //Fix $selectedTypeseanceIds
     * @param type $typeacte_id
     * @param type $projet_id
     * @param type $selectedTypeseanceIds
     * @return boolean
     *
     * @version 5.1.2
     */
    private function getTypologiePiecesSendTdt($type, $typeacteId, $codeClassification = null)
    {
        $typologiePieces = $this->Typologiepiece->getTypologiePieceByTypeActe($type, $typeacteId, $codeClassification);
        $options = [];
        foreach ($typologiePieces as $typologiePiece) {
            $options[] = [
                'name' => __(
                    '%s (%s)',
                    $typologiePiece['Typologiepiece']['name'],
                    $typologiePiece['Typologiepiece']['code']
                ),
                'value' => $typologiePiece['Typologiepiece']['code']
            ];
        }

        return $options;
    }

    public function versioningEnable($model)
    {
        $this->{$model}->Behaviors->load('Version', $this->{$model}->actsAsVersionOptionsList['Version']);
    }

    private function initConfigMultiBase()
    {
        // Charger dynamiquement la connexion à la base de données
        try {
            Configure::load('fqdn_databases');
            $this->fqdnDataBases = Configure::read('fqdn_config');
            $this->setDbMultiBase(env('HTTP_HOST'));
        } catch (CakeException|Exception $e) {
            $this->log('Erreur lors de l\'initialisation du tenant : ' . $e->getMessage());
            throw new BadRequestException($e->getMessage(), '404');
        }
    }

    protected function setDbMultiBase($fqdnName)
    {
        if (!isset($this->fqdnDataBases[$fqdnName])) {
            throw new CakeException('Le FQDN n\'existe pas');
        }
        $bddName = $this->fqdnDataBases[$fqdnName];

        Configure::write('Config.url', $fqdnName);
        Configure::write('Config.database', $bddName);
        Configure::write('Config.tenantName', $bddName);
        Configure::write('Acl.database', $bddName);
        Configure::write('App.fullBaseUrl', 'https://' . $fqdnName);

        // Créer une nouvelle connexion
        ConnectionManager::create($bddName);
        // Récupérer la connexion nouvellement créée
        $connection = ConnectionManager::getDataSource($bddName);
        require_once DATA . $bddName. DS . 'config/webdelib.inc';
        require_once DATA . $bddName . DS . 'config/formats.inc';
        require_once DATA . $bddName . DS . 'config/pastell.inc';
        if (!$connection->isConnected()) {
            throw new CakeException('Connexion non réussie à PostgreSQL !');
        }
    }

    public function getFqdnDataBases()
    {
        return array_unique($this->fqdnDataBases);
    }

    public function getFQDNName()
    {
        return Configure::read('Config.url');
    }

    public function getTenantName()
    {
        return Configure::read('Config.database');
    }
}
