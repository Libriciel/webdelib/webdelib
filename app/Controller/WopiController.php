<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Class WopiController
 *
 * @version 6.0.x
 * @package app.Controller
 */
class WopiController extends AppController
{
    /**
     * @version 6.0.X
     * @access public
     * @var type
     */
    public $components = [
        'RequestHandler',
        'Wopi',
        'Auth' => [
            'mapActions' => [
                'allow' => [
                    'getFile',
                    'putFile',
                    'getFileInfo',
                    'parseWopiRequest',
                    'getAccessToken',
                    'index',
                    'onlyoffice'
                ]
            ],
        ]
    ];

    /**
     * @version 6.0.X
     * @access public
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow();
    }

    /**
     *  Parses the URL in order to route the data in the request to the appropriate wopi endpoint handler.
     */
    public function parseWopiRequest($uri)
    {
        $path = parse_url($uri, PHP_URL_PATH);
        preg_match('/^\/example_php\/wopi\/files\/([[:alnum:]]+)(\/contents)?$/', $path, $matches);
        $numMatches = count($matches);

        // In a real case it is needed to encode an access token based on the file owner id
        // which should be used to set the value attribute of the 'access_token' <input> field
        // in the form used for requesting to load Collabora Online (see load.php).
        // In this example the access token attached to the request is retrieved and logged
        // to the Apache error log file. Anyway in a real implementation it has to be decoded
        // so that is possible to get the file owner id and check if he has really the rights
        // for the requested document.
        $token = getAccessToken($uri);
        error_log('INFO: access token: ' . $token);

        if ($numMatches == 3) {
            switch ($_SERVER['REQUEST_METHOD']) {
                case 'GET':
                    wopiGetFile($matches[1]);
                    break;
                case 'POST':
                    wopiPutFile($matches[1]);
                    break;
            }
        } elseif ($numMatches == 2) {
            wopiCheckFileInfo($matches[1]);
        }
    }

    /**
     *  Parses the URL in order to extract from the query section the access token value.
     */
    public function getAccessToken($uri)
    {
        $query = parse_url($uri, PHP_URL_QUERY);
        if ($query) {
            $params = explode('&', $query);
            foreach ($params as $param) {
                $pair = explode('=', $param);
                if ($pair && count($pair) == 2) {
                    if ($pair[0] == 'access_token') {
                        return $pair[1];
                    }
                }
            }
        }
        return '';
    }

    /**
     *  Returns info about the file with the given document id.
     *  The response has to be in JSON format and at a minimum it needs to include
     *  the file name and the file size.
     *  The CheckFileInfo wopi endpoint is triggered by a GET request at
     *  https://HOSTNAME/example_php/wopi/files/<document_id>
     */
    public function getFileInfo($id)
    {
        // test.txt is just a fake text file
        // the Size property is the length of the string
        // returned in wopiGetFile
        $file = new File(TMP . 'files' . DS . $this->Wopi->unconstructURL($id));

        if (!$file->exists()) {
            $this->response->statusCode(404);
            return $this->response;
        }
        $fileInfo = [
            'BaseFileName' => $file->name,
            'Size' => $file->size(),
            'UserId' => 'user-1',
            'OwnerId' => 'webdelib', // Create File
            'UserCanRename' => false,
            'UserFriendlyName' => 'webdelib',
            'UserCanWrite' => $file->writable(),
            'ReadOnly' => !$file->writable(),
            'FileExtension' => '.odt',
            //'CloseUrl' => 'https://example.com/url-to-close-page.com',
//            'LastModifiedTime' => !empty($file->modified) ? $file->modified->format(DATE_ISO8601) : '',
            'SupportsFileCreation' => false,
        ];

        $this->autoRender = false;
        $this->response->type(['json' => 'text/x-json']);
        $this->RequestHandler->respondAs('json');
        $this->response->body(json_encode($fileInfo));
        $this->response->header('X-WOPI-ItemVersion', '1');

        return $this->response;
    }

    /* *
     *  Given a request access token and a document id, sends back the contents of the file.
     *  The GetFile wopi endpoint is triggered by a request with a GET verb at
     *  https://HOSTNAME/example_php/wopi/files/<document_id>/contents
     */
    public function getFile($id)
    {
        // we just return the content of a fake text file
        // in a real case you should use the document id
        // for retrieving the file from the storage and
        // send back the file content as response
        $this->file = new File(TMP . 'files' . DS . $this->Wopi->unconstructURL($id));

        if (!$this->file->exists()) {
            throw new NotFoundException('Impossible de trouver le fichier');
        }

        $this->response->file($this->file->pwd());

        return $this->response;
    }

    /**
     *  Given a request access token and a document id, replaces the files with the POST request body.
     *  The PutFile wopi endpoint is triggered by a request with a POST verb at
     *  https://HOSTNAME/example_php/wopi/files/<document_id>/contents
     */
    public function putFile($id)
    {
        // we log to the apache error log file so that is possible
        // to check that saving has triggered this wopi endpoint
//        error_log('INFO: wopiPutFile invoked: document id: ' . $id);
//
        $fh = fopen('php://input', 'r');
        $fileContent = '';
        while ($line = fgets($fh)) {
            $fileContent = $fileContent . $line;
        }
        fclose($fh);

        $file = new File(TMP . 'files' . DS . $this->Wopi->unconstructURL($id));
        $file->write($fileContent);
        $file->close();

        $this->response->statusCode(200);

        return $this->response;
    }
}
