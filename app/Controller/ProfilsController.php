<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class ProfilsController extends AppController
{
    public $helpers = ['Tree', 'TinyMCE'];
    public $uses = ['Profil', 'Role', 'Aro'];
    public $components = ['Progress',
        'RequestHandler',
        'Auth' => [
            'mapActions' => [
                'create' => ['admin_add'],
                'read' => ['admin_index', 'admin_notifier'],
                'update' => ['admin_edit', 'admin_updateDroits'],
                'delete' => ['admin_delete']],
        ],
        'LdapManager.GroupManager',
        'AuthManager.AclManager',
        'Acl'
    ];

    /**
     * @access public
     */
    public function admin_index() // phpcs:ignore
    {
        $profils = $this->Profil->find('threaded', ['order' => 'Profil.id ASC', 'recursive' => -1]);
        $this->isDeletable($profils);
        $this->set('data', $profils);
    }

    /**
     * @version 4.3
     * @access public
     * @param type $profils
     */
    private function isDeletable(&$profils)
    {
        foreach ($profils as &$profil) {
            if ($this->Profil->User->find(
                'first',
                ['conditions' => ['User.profil_id' => $profil['Profil']['id']], 'recursive' => -1]
            )
            ) {
                $profil['Profil']['deletable'] = false;
            } else {
                $profil['Profil']['deletable'] = true;
            }
            if ($profil['children'] !== []) {
                $this->isDeletable($profil['children']);
            }
        }
    }

    /**
     * @version 4.3
     * @access public
     */
    public function admin_add() // phpcs:ignore
    {
        if ($this->request->is('post')) {
            $this->set('roles', $this->Profil->Role->find('list', ['recursive' => -1]));

            if ($this->Profil->save($this->data)) {
                $this->Flash->set(__('Le profil a été sauvegardé'), ['element' => 'growl']);
                $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Veuillez corriger les erreurs ci-dessous.'),
                    ['element' => 'growl','params'=>['type' => 'danger']]
                );
                $this->Profil->rollback();
            }
        }

        $this->set('profils', $this->Profil->find('list', ['order' => ['Profil.name ASC']]));
        $this->set('roles', $this->Profil->Role->find('list', ['recursive' => -1]));
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     */
    public function admin_edit($id = null) // phpcs:ignore
    {
        if ($this->request->is('post')) {
            if ($this->Profil->save($this->data)) {
                if (!empty($this->request->data['Aco']['Profil'])) {
                    $this->AclManager->setPermissionsProfil('Profil', $id, $this->request->data['Aco']['Profil']);
                }

                if (isset($this->request->data['Profil']['ModelGroup'])) {
                    $this->GroupManager->setGroupsProfil($id, $this->request->data['Profil']['ModelGroup']);
                }

                $this->Flash->set(__('Le profil a été modifié'), ['element' => 'growl']);
                $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Veuillez corriger les erreurs ci-dessous.'),
                    ['element' => 'growl','params'=>['type' => 'danger']]
                );
            }
        }

        $this->request->data = $this->Profil->find('first', [
            'fields' => ['id', 'name', 'role_id'],
            'conditions' => ["Profil.id" => $id],
            'recursive' => -1
        ]);

        if (empty($this->data)) {
            $this->Flash->set(__('ID invalide pour le profil'), ['element' => 'growl','params'=>['type' => 'danger']]);
            $this->redirect($this->previous);
        }

        $crud = [];
        //Ne pas prendre en compte le role manager pour les roles utilisateurs
        if ($this->data['Profil']['role_id'] === 3) {
            $crud[] = 'manager';
        }

        $filter = ['role' => array_search((int)$this->data['Profil']['role_id'], [
          'user' => 1,
          'admin' => 2,
          'manager' => 3], true)];

        $this->AclManager->permissionsProfil(
            $id,
            null,
            array_merge(['create', 'read', 'update', 'delete'], $crud),
            null,
            $filter
        );

        //Liste groups ldap
        $this->GroupManager->getGroupsProfil($id);

        $profils = $this->Profil->find('list', [
            'conditions' => ['Profil.id <>' => $id],
            'recursive' => -1,
            'order' => ['Profil.name'=>'ASC']]);

        $this->set('profils', $profils);
        $this->set('roles', $this->Profil->Role->find('list', ['recursive' => -1]));
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     */
    public function admin_delete($id = null) // phpcs:ignore
    {
        if (!$id) {
            $tab = $this->Profil->findAll("Profil.id=$id");
            $this->Flash->set(__('ID invalide pour le profil'), ['element' => 'growl']);
            $this->redirect($this->previous);
        }
        if (!$this->Profil->User->find('first', [
                'conditions' => ['User.profil_id' => $id],
                'recursive' => -1
            ])
        ) {
            if ($this->Profil->delete($id)) {
                $this->Flash->set(
                    __(
                        'Le profil a été supprimé'
                    ),
                    ['element' => 'growl','params'=> ['type' => 'success']]
                );
                $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __(
                        'Impossible de supprimer ce profil'
                    ),
                    ['element' => 'growl','params'=>['type' => 'danger']]
                );
                $this->redirect($this->previous);
            }
        } else {
            $this->Flash->set(
                __(
                    'Impossible de supprimer ce profil car il est attribué.'
                ),
                ['element' => 'growl','params'=>['type' => 'danger']]
            );
            $this->redirect($this->previous);
        }
    }

    /**
     * @version 4.3
     * @access public
     * @param int $profil_id
     */
    public function admin_notifier($profil_id, $cookieTokenKey = null) // phpcs:ignore
    {
        $profil = $this->Profil->find(
            'first',
            [
                'conditions' => ['Profil.id' => $profil_id],
                'recursive' => -1]
        );

        if (!$this->request->is('Post')) {
            $this->set('libelle_profil', $profil['Profil']['name']);
            $this->set('id', $profil['Profil']['id']);
        } else {
            try {
                $this->Progress->start($cookieTokenKey);

                $this->Progress->at(1, __('Récupération des données des utilisateurs...'));
                $conditions['AND']['User.profil_id'] = $profil['Profil']['id'];
                $conditions['AND']['User.active'] = '1';
                $conditions['AND']['User.email ILIKE'] = "%@%";
                $users = $this->Profil->User->find(
                    'all',
                    ['conditions' => $conditions,
                    'recursive' => -1]
                );
                $nbUsers = count($users);
                $cpt = 0;

                App::uses('CakeEmail', 'Network/Email');
                $Email = new CakeEmail();
                foreach ($users as $user) {
                    $this->Progress->at($cpt * (100 / $nbUsers), $user['User']['email'] . '...');
                    $cpt++;
                    $Email->template('default', 'default')
                            ->to($user['User']['email'])
                            ->subject(__('Notification aux utilisateurs du profil : %s', $profil['Profil']['name']))
                            ->send($this->data['Profil']['content']);
                    sleep(1);
                }
                $this->Progress->redirect(
                    ['admin'=> true, 'prefix'=> 'admin', 'controller'=>'profils', 'action'=>'index']
                );
                $this->Progress->stop(__('Opération terminée'));
                $this->Flash->set(__('Les mails ont été envoyés'), ['element' => 'growl']);
            } catch (Exception $e) {
                $this->log('Erreur ' . $e->getCode() . " ! \n" . $e->getMessage(), 'error');
            }

            $this->autoRender = false;
            $this->response->type(['json' => 'text/x-json']);
            $this->RequestHandler->respondAs('json');
            $this->response->body(json_encode($this->Session->read('Progress.Tokens')));
            return $this->response;
        }
    }

    /**
     * @access public
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     * @throws NotFoundException
     */
    public function admin_updateDroits($id, $cookieTokenKey) // phpcs:ignore
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid profil'));
        }
        try {
            $this->Progress->start($cookieTokenKey);

            $profil = $this->Profil->find('first', [
                'conditions'=> [
                    'Profil.id' => $id
                ],
                'recursive' => -1
            ]);

            if (!$profil) {
                throw new NotFoundException(__('Profil not exist'));
            }

            $this->Progress->at(1, __('Récupération des données des utilisateurs...'));
            $users = $this->Profil->User->find('all', [
                'conditions' => ['User.active' => true, 'User.profil_id' => $id],
                'order' => 'username ASC',
                'recursive' => -1]);

            $nbUsers = count($users);

            $this->Aro->recursive = -1;
            $aroProfil = $this->Aro->findByForeignKeyAndModel($id, 'Profil');
            $permissions = $this->Aro->Permission->find('all', [
                'conditions' => [
                    'aro_id' => $aroProfil['Aro']['id']],
                'recursive' => -1]);
            if ($nbUsers > 0) {
                $cpt = 1;
                foreach ($users as $user) {
                    $this->Progress->at(
                        $cpt * (100 / $nbUsers),
                        __('Mise à jour des données pour l\'utilisateur: %s...', $user['User']['username'])
                    );

                    $this->Aro->recursive = -1;
                    $aroUser = $this->Aro->findByForeignKeyAndModel($user['User']['id'], 'User');

                    foreach ($permissions as $permission) {
                        $this->Aro->recursive = -1;
                        $acoAro = $this->Aro->Permission->findByAcoIdAndAroId(
                            $permission['Permission']['aco_id'],
                            $aroUser['Aro']['id']
                        );
                        if (empty($acoAro)) {
                            $this->Aro->Permission->create();
                            $acoAro['Permission']['aro_id'] = $aroUser['Aro']['id'];
                            $acoAro['Permission']['aco_id'] = $permission['Permission']['aco_id'];
                        }

                        foreach ($this->Aro->Permission->getAcoKeys($this->Aro->Permission->schema()) as $permKeys) {
                            $acoAro['Permission'][$permKeys] = $permission['Permission'][$permKeys];
                        }

                        $this->Aro->Permission->save($acoAro);
                        unset($acoAro);
                    }
                    $cpt++;
                }
            }
        } catch (Exception $e) {
            $this->log('Erreur ' . $e->getCode() . " ! \n" . $e->getMessage(), 'error');
        }

        $this->Progress->stop(__('Opération terminée'));
        $this->Flash->set(__('Le profil a été rechargé'), ['element' => 'growl']);

        $this->autoRender = false;
        $this->response->type(['json' => 'text/x-json']);
        $this->RequestHandler->respondAs('json');
        $this->response->body(json_encode($this->Session->read('Progress.Tokens')));
        return $this->response;
    }
}
