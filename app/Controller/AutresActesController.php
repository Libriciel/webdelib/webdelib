<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('CakeTime', 'Utility');
App::uses('AppTools', 'Lib');

/**
 * [AutresActesController description]
 *
 * @package   app.Controller
 *
 *
 */
class AutresActesController extends AppController
{
    public $helpers = ['TinyMCE', 'ProjetUtil','DelibTdt'];
    public $uses = ['Acteur',
        'Deliberation', 'Typeacte', 'User', 'Annexe', 'Typeseance', 'Seance',
        'TypeSeance', 'Commentaire', 'ModelOdtValidator.Modeltemplate', 'Theme',
        'Infosup',
        'Cakeflow.Circuit', 'Cakeflow.Traitement',
        'Nomenclature', 'DeliberationSeance', 'DeliberationTypeseance',
        'DeliberationUser', 'Service',
        'Nature','Typologiepiece'];
    public $components = [
        'RequestHandler',
        'ProjetTools',
        'Acl',
        'Filtre',
        'Progress',
        'Paginator',
        'Signatures',
        'Auth' => [
            'mapActions' => [
                'read' => [],
                'autresActesAbandon' => ['abandonActe'],
                'allow' => ['download']
            ]
        ]
    ];

    /**
     * @version 5.1.0
     * @access public
     * @param type $id
     * @param type $file
     */
    public function download($id, $file, $isRevision = false)
    {
        $this->autoRender = false;

        $fileType = $file . '_type';
        $fileSize = $file . '_size';
        $fileName = $file . '_name';
        if ($isRevision !== false) {
            $this->Deliberation->Behaviors->load(
                'Version',
                $this->Deliberation->actsAsVersionOptionsList['Version']
            );

            $delib = $this->Deliberation->version($isRevision);

            $delib['Deliberation'][$fileType] =
                $delib['deliberations_versions']['data_version']['Deliberation'][$fileType];
            $delib['Deliberation'][$fileName] =
                $delib['deliberations_versions']['data_version']['Deliberation'][$fileName];
            $delib['Deliberation'][$file] = $delib['deliberations_versions'][$file];
        } else {
            $delib = $this->Deliberation->find('first', [
                'conditions' => ["Deliberation.id" => $id],
                'fields' => [$fileType, $fileSize, $fileName, $file],
                'recursive' => -1
            ]);
        }
        $this->response->type($delib['Deliberation'][$fileType]);
        $this->response->download($delib['Deliberation'][$fileName]);
        $this->response->body($delib['Deliberation'][$file]);
    }

    /**
     * @version 5.1
     * @version 4.3
     * @access private
     * @param type $projets
     */
    private function ajouterFiltre(&$projets)
    {
        if (!$this->Filtre->critereExists() && !empty($projets)) {
            $typeactes = $this->Deliberation->Typeacte->find('list', [
                'order' => ['Typeacte.name' => 'ASC'],
                'recursive' => -1,
                'allow' => ['Typeacte.id' => 'read']]);
            $this->Filtre->addCritere('Typeacte', [
                'field' => 'Deliberation.typeacte_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Type d\'acte'),
                    'data-placeholder' => __('Sélectionner un type d\'acte'),
                    'data-allow-clear' => true,
                    'options' => $typeactes
                ]]);

            $themes = $this->Deliberation->Theme->generateTreeListByOrder(
                ['Theme.actif' => '1'],
                '&nbsp;&nbsp;&nbsp;&nbsp;'
            );
            $this->Filtre->addCritere('ThemeId', [
                'field' => 'Deliberation.theme_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Thème'),
                    'data-placeholder' => __('Sélectionner un thème'),
                    'data-allow-clear' => true,
                    'options' => $themes,
                    'escape' => false
                ]]);

            $services = $this->Service->find('list', [
                'fields' => ['id', 'name'],
                'order' => ['Service.name' => 'ASC'],
                'conditions' => ['actif' => true], 'recursive' => -1]);
            $this->Filtre->addCritere('ServiceId', [
                'field' => 'Deliberation.service_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un service émetteur'),
                    'data-allow-clear' => true,
                    'label' => __('Service émetteur'),
                    'multiple' => true,
                    'options' => $services
                ]]);

            $users = $this->User->find('all', [
                'fields' => ['id', 'nom', 'prenom'],
                'order' => ['User.nom' => 'ASC'],
                'conditions' => ['active' => true],
                'recursive' => -1]);
            $users = Hash::combine($users, '{n}.User.id', ['%s %s', '{n}.User.nom', '{n}.User.prenom']);

            $this->Filtre->addCritere('RedacteurId', [
                'field' => ['Deliberation.redacteur_id', 'DeliberationUser.user_id'],
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un rédacteur'),
                    'data-allow-clear' => true,
                    'label' => __('Rédacteur du projet'),
                    'empty' => __('Tous'),
                    'options' => $users
                ]]);

            $circuits = $this->Circuit->find('list', [
                'fields' => ['id', 'nom'],
                'order' => ['Circuit.nom' => 'ASC'],
                'conditions' => ['actif' => true], 'recursive' => -1]);
            $this->Filtre->addCritere('CircuitId', [
                'field' => 'Deliberation.circuit_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un circuit'),
                    'data-allow-clear' => true,
                    'label' => __('Circuit de validation'),
                    'empty' => __('Tous'),
                    'options' => $circuits
                ]]);

            $etats = $this->ProjetTools->generateListEtat($this->request->param('action'));
            $this->Filtre->addCritere('Etat', [
                'field' => 'Deliberation.etat',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner un état'),
                    'data-allow-clear' => true,
                    'label' => __('État du projet'),
                    'empty' => __('Tous'),
                    'options' => $etats
                ]]);
        }
    }

    /**
     *
     */
    public function autresActesAValider()
    {
        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);

        $conditions = $this->ProjetTools->handleConditions($this->Filtre->conditions());

        if (!isset($conditions['Deliberation.etat'])) {
            $conditions['Deliberation.etat <'] = 2;
            $conditions['Deliberation.etat >'] = -1;
        }

        $conditions['Deliberation.signee'] = false;

        $this->paginate = ['Deliberation' => [
                'countField' => 'DISTINCT Deliberation.id',
                'fields' => [
                    'DISTINCT Deliberation.id',
                    'Deliberation.redacteur_id',
                    'Deliberation.objet',
                    'Deliberation.objet_delib',
                    'Deliberation.titre',
                    'Deliberation.etat',
                    'Deliberation.signee',
                    'Deliberation.typeacte_id',
                    'Deliberation.parapheur_etat',
                    'Deliberation.signature_type',
                    'Deliberation.theme_id',
                    'Deliberation.circuit_id',
                    'Deliberation.num_pref',
                    'Deliberation.num_delib',
                    'Deliberation.service_id',
                    'Typeacte.name',
                ],
                'joins' => [
                    $this->Deliberation->join('DeliberationUser', ['type' => 'LEFT']),
                    $this->Deliberation->join('Typeacte', [
                            'type' => 'INNER', 'conditions' => ['Typeacte.deliberant' => false]
                        ]),
                ],
                'contain' => [
                    'User' => ['fields' => ['id', 'nom', 'prenom']],
                    'Redacteur' => ['fields' => ['id', 'nom', 'prenom']],
                    'Service.name',
                    'Circuit.nom',
                    'Theme.libelle',
                    'Seance.id',
                    'Seance.type_id',
                    'Seance.date',
                    'Typeseance.id',
                    'Typeseance.libelle',
                ],
                'conditions' => $conditions,
                'order' => ['Deliberation.id' => 'DESC'],
                'allow' => ['typeacte_id'],
                'limit' => 20,
                'recusive' => -1
        ]];

        $actes = $this->Paginator->paginate('Deliberation');

        $this->ajouterFiltre($actes);
        $liste_droits = [
            'Projets' => ['read', 'update', 'delete'],
            'editerTous' => 'read',
            'supprimerTous' => 'read',
            'duplicate' => 'read',
            'attribuerCircuit' => 'read',
            'goNext' => 'read',
            'validerEnUrgence' => 'read'];
        $droits = $this->ProjetTools->getDroits($liste_droits);

        for ($i = 0; $i < count($actes); $i++) {
            $actes[$i]['Deliberation'][$actes[$i]['Deliberation']['id'] . '_num_pref'] =
                str_replace('.', '_', $actes[$i]['Deliberation']['num_pref']);
            $actes[$i]['Deliberation']['num_pref_libelle'] =
                $this->ProjetTools->getMatiereByKey($actes[$i]['Deliberation']['num_pref']);
            $this->ProjetTools->actionDisponible($actes[$i], $droits);
            //On cherche à savoir si la délibération est vérrouillée par un utilisateur
            $this->ProjetTools->locked($actes[$i]['Deliberation']);
        }

        $this->set('crumbs', [__('Tous les projets'), __('Autres actes'), __('À valider')]);
        $this->set('titreVue', __('Autres actes à valider'));
        $this->set('actes', $actes);

        $this->render('autres_actes');
    }

    /**
     * @access public
     * @return type
     *
     * @version 5.1.2
     * @since 4.3
     */
    public function autresActesValides()
    {
        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);
        $conditions = $this->ProjetTools->handleConditions($this->Filtre->conditions());

        $conditions['Deliberation.etat'] = [2,3,4,10];
        $conditions['Deliberation.signee'] = false;

        $this->paginate = ['Deliberation' => [
                'countField' => 'DISTINCT Deliberation.id',
                'fields' => [
                    'DISTINCT Deliberation.id',
                    'Deliberation.redacteur_id',
                    'Deliberation.num_delib',
                    'Deliberation.objet',
                    'Deliberation.objet_delib',
                    'Deliberation.titre',
                    'Deliberation.etat',
                    'Deliberation.signee',
                    'Deliberation.parapheur_etat',
                    'Deliberation.parapheur_commentaire',
                    'Deliberation.typeacte_id',
                    'Deliberation.theme_id',
                    'Deliberation.service_id',
                    'Deliberation.circuit_id',
                    'Deliberation.date_acte',
                    'Typeacte.name',
                    'Typeacte.modele_projet_id',
                    'Typeacte.modele_final_id',
                    'Typeacte.nature_id',
                ],
                'joins' => [
                    $this->Deliberation->join('DeliberationUser', ['type' => 'LEFT']),
                    $this->Deliberation->join('Typeacte', [
                        'type' => 'INNER', 'conditions' => ['Typeacte.deliberant' => false]
                        ]),
                ],
                'contain' => [
                    'User' => ['fields' => ['id', 'nom', 'prenom']],
                    'Redacteur' => ['fields' => ['id', 'nom', 'prenom']],
                    'Service.name',
                    'Circuit.nom',
                    'Theme.libelle',
                    'Seance.id',
                    'Seance.type_id',
                    'Seance.date',
                    'Typeseance.id',
                    'Typeseance.libelle',
                ],
                'conditions' => $conditions,
                'order' => ['Deliberation.id' => 'DESC'],
                'allow' => ['typeacte_id'],
                'limit' => 20,
                'recusive' => -1
        ]];

        $actes = $this->Paginator->paginate('Deliberation');

        $this->ajouterFiltre($actes);


        $liste_droits = [
            'Projets' => ['read', 'update', 'delete'],
            'reattribuerTous' => 'read',
            'editerTous' => 'read',
            'supprimerTous' => 'read',
            'duplicate' => 'read',
            'goNext' => 'read',
            'validerEnUrgence' => 'read',
        ];
        $droits = $this->ProjetTools->getDroits($liste_droits);

        for ($i = 0; $i < count($actes); $i++) {
            $actes[$i]['Deliberation']['date_acte'] =
                CakeTime::format($actes[$i]['Deliberation']['date_acte'], '%d/%m/%Y');
            $this->ProjetTools->actionDisponible($actes[$i], $droits);
            $this->ProjetTools->locked($actes[$i]['Deliberation']);
        }

        $this->set('actes', $actes);

        $this->set(
            'acl',
            [
                'autresActesAbandon'=> $this->Acl->check(
                    [
                      'User' => ['id' => $this->Auth->user('id')]
                    ],
                    'AutresActes/autresActesAbandon',
                    'read'
                ),
                'sendAutresActesToSignature'=> $this->Acl->check(
                    [
                        'User' => ['id' => $this->Auth->user('id')]
                    ],
                    'Signatures/sendAutresActesToSignature',
                    'read'
                ),
            ]
        ) ;
        $circuits = $this->Signatures->getCircuits();
        unset($circuits['Standard']);

        $this->set('crumbs', [__('Tous les projets'), __('Autres actes'), __('Validés')]);
        $this->set('titreVue', __('Autres actes validés'));
        $this->set('circuits', $circuits);
        // à factoriser
        $this->set('traitement_lot', true);
        $this->set('modeles', $this->Modeltemplate->find('list', [
                    'recursive' => -1,
                    'fields' => ['Modeltemplate.name'],
                    'conditions' => ['modeltype_id' => [MODELE_TYPE_RECHERCHE]]
        ]));
        $this->render('autres_actes');
    }

    /**
     * @access public
     * @return type
     * @version 5.1.2
     * @since 4.3
     */
    public function autresActesAEnvoyer()
    {
        if (Configure::read('USE_TDT')) {
            App::uses('Tdt', 'Lib');
            $Tdt = new Tdt();
            $date_classification = $Tdt->getDateClassification();
            $this->set('dateClassification', $date_classification);

            $this->set('nomenclatures', $this->Nomenclature->generateTreeListWithOptionGroup());
        }
        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);
        $conditions['AND'] = $this->ProjetTools->handleConditions($this->Filtre->conditions());

        if (!isset($conditions['AND']['Deliberation.etat'])) {
            $conditions['AND']['Deliberation.etat'] = [3, 4];
            $conditions['AND']['Deliberation.signee'] = true;
        } elseif ($conditions['AND']['Deliberation.etat'] === 7) {
            $conditions['AND']['Deliberation.signee'] = true;
        }

        $this->paginate = ['Deliberation' => [
                'countField' => 'DISTINCT Deliberation.id',
                'fields' => [
                    'DISTINCT Deliberation.id',
                    'Deliberation.num_delib',
                    'Deliberation.objet',
                    'Deliberation.objet_delib',
                    'Deliberation.titre',
                    'Deliberation.num_pref',
                    'Deliberation.typologiepiece_code',
                    'Deliberation.tdt_document_papier',
                    'Deliberation.signee',
                    'Deliberation.etat',
                    'Deliberation.typeacte_id',
                    'Deliberation.theme_id',
                    'Deliberation.service_id',
                    'Deliberation.tdt_status',
                    'Deliberation.circuit_id',
                    'Deliberation.parapheur_bordereau',
                    'Typeacte.name',
                ],
                'joins' => [
                    $this->Deliberation->join('DeliberationUser', ['type' => 'LEFT']),
                    $this->Deliberation->join('Typeacte', [
                      'type' => 'INNER',
                      'conditions' => ['Typeacte.teletransmettre' => true, 'Typeacte.deliberant' => false]
                    ]),
                ],
                'contain' => [
                    'Annexe' => [
                      'fields' => ['id', 'filename', 'filetype', 'typologiepiece_code'],
                      'conditions' => ['joindre_ctrl_legalite' => true],
                      'order' => ['position']
                    ],
                    'User' => ['fields' => ['id', 'nom', 'prenom']],
                    'Redacteur' => ['fields' => ['id', 'nom', 'prenom']],
                    'Service.name',
                    'Circuit.nom',
                    'Theme.libelle',
                    'Seance.id',
                    'Seance.type_id',
                    'Seance.date',
                    'Typeseance.id',
                    'Typeseance.libelle',
                ],
                'conditions' => $conditions,
                'order' => ['Deliberation.id' => 'DESC'],
                'allow' => ['typeacte_id'],
                'limit' => 20,
                'recusive' => -1]]
        ;

        $actes = $this->Paginator->paginate('Deliberation');
        $this->set('traitement_lot', true);
        $this->set('signature', true);
        $this->ajouterFiltre($actes);

        $droits = $this->ProjetTools->getDroits([
          'autresActesAEnvoyer' => 'read',
          'editPostSign' => 'read'
        ]);

        for ($i = 0; $i < count($actes); $i++) {
            $this->ProjetTools->actionDisponible($actes[$i], $droits);
            $actes[$i]['Modeltemplate']['modele_projet_id'] =
                $this->Typeacte->getModelId($actes[$i]['Deliberation']['typeacte_id'], 'modele_projet_id');
            $actes[$i]['Modeltemplate']['modele_final_id'] =
                $this->Typeacte->getModelId($actes[$i]['Deliberation']['typeacte_id'], 'modele_final_id');

            $actes[$i]['Deliberation']['num_pref_libelle'] =
                $this->ProjetTools->getMatiereByKey($actes[$i]['Deliberation']['num_pref']);
            $actes[$i]['Deliberation']['num_pref'] = str_replace('.', '_', $actes[$i]['Deliberation']['num_pref']);
            //On cherche à savoir si la délibération est vérrouillée par un utilisateur
            $this->ProjetTools->locked($actes[$i]['Deliberation']);

            $actes[$i]['Deliberation']['typologiepiece_code_libelle'] =
                $this->Typologiepiece->getTypologieNamebyCode($actes[$i]['Deliberation']['typologiepiece_code']);
            foreach ($actes[$i]['Annexe'] as &$annexe) {
                $annexe['typologiepiece_code_libelle'] =
                    $this->Typologiepiece->getTypologieNamebyCode($annexe['typologiepiece_code']);
            }
            $actes[$i]['isTypologiepieceEmpty'] = $this->Deliberation->isParamTdtEmptyByActe($actes[$i]);
        }

        $this->set(
            'acl',
            [
                'autresActesAbandon'=> $this->Acl->check(
                    ['User' => ['id' => $this->Auth->user('id')]],
                    'AutresActes/autresActesAbandon',
                    'read'
                ),
                'autresActesCancelSignature'=> $this->Acl->check(
                    ['User' => ['id' => $this->Auth->user('id')]],
                    'Signatures/autresActesCancelSignature',
                    'read'
                ),
            ]
        ) ;

        $this->set('actes', $actes);
    }

    /**
     * @access public
     *
     * @version 5.1.2
     * @since 4.4.0
     */
    public function nonTransmis()
    {
        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);
        $conditions = $this->ProjetTools->handleConditions($this->Filtre->conditions());

        if (!isset($conditions['AND']['Deliberation.etat'])) {
            $conditions['AND']['Deliberation.etat'] = [3, 4];
            $conditions['AND']['Deliberation.signee'] = true;
        } elseif ($conditions['AND']['Deliberation.etat'] == 7) {
            $conditions['AND']['Deliberation.signee'] = true;
        }

        $this->paginate = ['Deliberation' => [
                'countField' => 'DISTINCT Deliberation.id',
                'fields' => [
                    'DISTINCT Deliberation.id',
                    'Deliberation.num_delib',
                    'Deliberation.objet',
                    'Deliberation.objet_delib',
                    'Deliberation.titre',
                    'Deliberation.date_acte',
                    'Deliberation.signee',
                    'Deliberation.etat',
                    'Deliberation.typeacte_id',
                    'Deliberation.numero_depot'
                //  'Typeacte.name'
                ],
                'joins' => [
                    $this->Deliberation->join('DeliberationUser', ['type' => 'LEFT']),
                    $this->Deliberation->join('Typeacte', [
                      'type' => 'INNER',
                      'conditions' => ['Typeacte.teletransmettre' => false, 'Typeacte.deliberant' => false]
                    ]),
                ],
                'contain' => [
                    'Annexe' => ['fields' => ['id', 'filename'], 'order' => ['position']],
                    'Service' => ['fields' => ['id', 'name']]
                ],
                'conditions' => $conditions,
                'allow' => ['typeacte_id'],
                'order' => ['date_acte' => 'DESC'],
                'limit' => 20,
                'recusive' => -1
        ]];


        $this->set('traitement_lot', true);
        $this->set('signature', true);

        $actes  = $this->Paginator->paginate('Deliberation');

        $this->set(
            'acl',
            [
                'autresActesCancelSendTDT'=> $this->Acl->check(
                    [
                      'User' => ['id' => $this->Auth->user('id')]
                    ],
                    'Teletransmettre/autresActesCancelSendToTDT',
                    'read'
                ),
                'autresActesCancelSignature'=> $this->Acl->check(
                    [
                      'User' => ['id' => $this->Auth->user('id')]
                    ],
                    'Signatures/autresActesCancelSignature',
                    'read'
                ),
            ]
        ) ;



        $this->request->data = $actes ;
        $this->ajouterFiltre($this->request->data);
    }

    /**
     * @access public
     *
     * @version 5.1.2
     * @since 4.3
     */
    public function autresActesEnvoyes()
    {
        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);
        $conditions = $this->ProjetTools->handleConditions($this->Filtre->conditions());

        $conditions['Deliberation.etat'] = [5,11];

        $this->paginate = ['Deliberation' => [
                'countField' => 'DISTINCT Deliberation.id',
                'fields' => [
                    'DISTINCT Deliberation.id',
                    'Deliberation.num_delib',
                    'Deliberation.objet',
                    'Deliberation.objet_delib',
                    'Deliberation.titre',
                    'Deliberation.num_pref',
                    'Deliberation.typologiepiece_code',
                    'Deliberation.tdt_document_papier',
                    'Deliberation.etat',
                    'Deliberation.tdt_id',
                    'Deliberation.tdt_ar_date',
                    'Deliberation.tdt_status',
                    'Deliberation.tdt_message',
                    'Deliberation.typeacte_id',
                    'Deliberation.date_acte',
                    'Deliberation.theme_id',
                    'Deliberation.circuit_id',
                    'Deliberation.service_id',
                    'Deliberation.numero_depot',
                    'Typeacte.name',
                ],
                'joins' => [
                    $this->Deliberation->join('DeliberationUser', ['type' => 'LEFT']),
                    $this->Deliberation->join('Typeacte', [
                      'type' => 'INNER',
                      'conditions' => ['Typeacte.teletransmettre' => true,'Typeacte.deliberant' => false]
                    ]),
                ],
                'contain' => [
                    'Annexe' => [
                        'fields' => ['id', 'filename', 'filetype', 'typologiepiece_code'],
                        'conditions' => ['joindre_ctrl_legalite' => true],
                        'order' => ['position']
                    ],
                    'User' => ['fields' => ['id', 'nom', 'prenom']],
                    'Redacteur' => ['fields' => ['id', 'nom', 'prenom']],
                    'TdtMessage' => ['fields' => ['tdt_id', 'tdt_type', 'tdt_etat', 'parent_id'],
                        'conditions' => ['parent_id is null'],
                        'Reponse' => ['fields' => ['tdt_id', 'tdt_type', 'tdt_etat']]],
                    'Service.name',
                    'Circuit.nom',
                    'Theme.libelle',
                    'Seance.id',
                    'Seance.type_id',
                    'Seance.date',
                    'Typeseance.id',
                    'Typeseance.libelle',
                ],
                'conditions' => $conditions,
                'allow' => ['typeacte_id'],
                'limit' => 20,
                'order' => ['Deliberation.date_acte' => 'DESC'],
                'recusive' => -1
        ]];

        $actes = $this->Paginator->paginate('Deliberation');

        $this->ajouterFiltre($actes);

        for ($i = 0; $i < count($actes); $i++) {
            $this->Deliberation->id = $actes[$i]['Deliberation']['id'];
            $actes[$i]['Deliberation']['num_pref_libelle'] =
                $this->ProjetTools->getMatiereByKey($actes[$i]['Deliberation']['num_pref']);
            //On cherche à savoir si la délibération est vérrouillée par un utilisateur
            $this->ProjetTools->locked($actes[$i]['Deliberation']);
            $actes[$i]['Deliberation']['cancelSendTDT'] = (
                $this->Deliberation->isCancelableForTdt()
              && $this->Acl->check(
                  ['User' => ['id' => $this->Auth->user('id')]],
                  'Teletransmettre/autresActesCancelSendToTDT',
                  'read'
              )
            ); //Annuler l'envoi au TdT

            $actes[$i]['Deliberation']['typologiepiece_code_libelle'] =
                $this->Typologiepiece->getTypologieNamebyCode($actes[$i]['Deliberation']['typologiepiece_code']);
            foreach ($actes[$i]['Annexe'] as &$annexe) {
                $annexe['typologiepiece_code_libelle'] =
                    $this->Typologiepiece->getTypologieNamebyCode($annexe['typologiepiece_code']);
            }
        }

        $this->set('tdt', Configure::read('TDT'));
        $this->set('tdt_host', Configure::read(Configure::read('TDT') . '_HOST'));
        if (Configure::read('USE_TDT')) {
            App::uses('Tdt', 'Lib');
            $Tdt = new Tdt();
            $this->set('dateClassification', $Tdt->getDateClassification());
        }
        $this->set('deliberations', $actes);
        // à factoriser
        $this->set('traitement_lot', true);
        $this->set('modeles', $this->Modeltemplate->find('list', [
                    'recursive' => -1,
                    'fields' => ['Modeltemplate.name'],
                    'conditions' => ['modeltype_id' => [MODELE_TYPE_RECHERCHE]]
        ]));
        $this->set(
            'acl',
            [
                'autresActesCancelSendTDT'=> $this->Acl->check(
                    ['User' => ['id' => $this->Auth->user('id')]],
                    'Teletransmettre/autresActesCancelSendToTDT',
                    'read'
                ),
                'autresActesCancelSignature'=> $this->Acl->check(
                    ['User' => ['id' => $this->Auth->user('id')]],
                    'Signatures/autresActesCancelSignature',
                    'read'
                ),
                'autresActesAbandon'=> false,
            ]
        ) ;

        $this->render('autres_actes_envoyes');
    }

    public function autresActesAbandon()
    {
        foreach ($this->request->data['Deliberation'] as $deliberation) {
            $success = $this->Deliberation->abandon($deliberation);
            if ($success) {
                $this->Flash->set(__('Acte n° %s abandonné.', $success), [
                    'element' => 'growl',
                    'params' => ['type' => 'success']
                ]);
            } else {
                $this->Flash->set(__('Echec de l\'abandon de l\'acte %s.', $success), [
                    'element' => 'growl',
                    'params' => ['type' => 'danger']
                ]);
            }
        }
        return $this->redirect($this->previous);
    }

    /**
     * @return void
     */
    public function abandonActe()
    {
        foreach ($this->request->data['parameters'] as $parameter) {
            $this->set($parameter['name'], $parameter['value']);
        }
    }
}
