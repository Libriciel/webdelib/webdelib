<?php

/**
 * Contrôleur des tâches planifiées (Crons)
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       web-delib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 * @version     v4.3
 * @package     app.Controller
 */
App::uses('AppTools', 'Lib');

class CronsController extends AppController
{
    public $helpers = ['DurationPicker'];
    public $components = [
        'VueDetaillee',
        'Applist',
        'Paginator',
        'Auth' => [
            'mapActions' => [
                'read' => ['admin_executer', 'admin_index'
                    , 'admin_planifier', 'admin_runCrons', 'admin_unlock', 'admin_view']
            ]
        ]
    ];

    public const FORMAT_DATE = 'Y-m-d H:i:s';

    /**
     * Vue détaillée des crons (tâches planifiées)
     *
     * @version 4.3
     * @param type $id
     * @return type
     */
    public function admin_view($id = null) // phpcs:ignore
    {
        // initialisations
        $this->request->data = $this->{$this->modelClass}->find('first', [
                'recursive' => 1,
                'conditions' => ['Cron.id' => $id]
            ]);
        if (empty($this->request->data)) {
            $this->Flash->set(
                __('ID invalide pour la tâche planifiée : affichage de la vue impossible.'),
                ['element' => 'growl', 'params'=>['type' => 'important']]
            );
            return $this->redirect(['action' => 'index']);
        } else {
            $this->pageTitle = Configure::read('appName') . ' : ' . __('Tâche planifiée : vue détaillée');

            /* préparation des informations à afficher dans la vue détaillée */
            $maVue = new $this->VueDetaillee(
                $this->request->data[$this->modelClass]['nom'],
                __('Retour à la liste des tâches planifiées')
            );
            $maVue->ajouteSection(__('Informations principales'));
            $maVue->ajouteLigne(__('Identifiant interne (id)'), $this->request->data[$this->modelClass]['id']);
            $maVue->ajouteLigne(__('Nom'), $this->request->data[$this->modelClass]['nom']);
            $maVue->ajouteLigne(__('Description'), $this->request->data[$this->modelClass]['description']);
            $maVue->ajouteLigne(
                __('Fonction appelée'),
                $this->request->data[$this->modelClass]['plugin'] . '/'
                . $this->request->data[$this->modelClass]['model'] . '/'
                . $this->request->data[$this->modelClass]['action']
            );
            if ($this->request->data[$this->modelClass]['params'] != ""
                && $this->request->data[$this->modelClass]['params'] != "NULL"
            ) {
                $maVue->ajouteLigne(__('Paramètre de la fonction'), $this->request->data[$this->modelClass]['params']);
            }
            $maVue->ajouteLigne(
                __('Active'),
                $this->{$this->modelClass}->libelleActive($this->request->data[$this->modelClass]['active'])
            );
            $maVue->ajouteLigne(
                __('Date de création'),
                AppTools::timeFormat($this->request->data[$this->modelClass]['created'], 'd-m-Y à H:i:s')
            );
            $maVue->ajouteElement(
                __('Par'),
                $this->request->data["CreatedUser"]["prenom"] . ' ' . $this->request->data["CreatedUser"]["nom"]
            );
            $maVue->ajouteLigne(
                __('Date de dernière modification'),
                AppTools::timeFormat($this->request->data[$this->modelClass]['modified'], 'd-m-Y à H:i:s')
            );
            $maVue->ajouteElement(
                __('Par'),
                $this->request->data["ModifiedUser"]["prenom"] . " " . $this->request->data["ModifiedUser"]["nom"]
            );

            $maVue->ajouteSection(__('Prochaine exécution'));
            $maVue->ajouteLigne(
                __('Date prévue'),
                AppTools::timeFormat(
                    $this->request->data[$this->modelClass]['next_execution_time'],
                    'd-m-Y à H:i:s'
                )
            );
            $maVue->ajouteLigne(
                __('Délai entre 2 exécutions'),
                AppTools::durationToString($this->request->data[$this->modelClass]['execution_duration'])
            );

            $maVue->ajouteSection(__('Dernière exécution'));
            $maVue->ajouteLigne(
                __('Statut'),
                $this->{$this->modelClass}->libelleStatus(
                    $this->request->data[$this->modelClass]['last_execution_status']
                )
            );
            $maVue->ajouteLigne(
                __('Début d\'exécution'),
                AppTools::timeFormat(
                    $this->request->data[$this->modelClass]['last_execution_start_time'],
                    'd-m-Y à H:i:s'
                )
            );
            $maVue->ajouteElement(
                __('Fin d\'exécution'),
                AppTools::timeFormat(
                    $this->request->data[$this->modelClass]['last_execution_end_time'],
                    'd-m-Y à H:i:s'
                )
            );
            $maVue->ajouteLigne(
                __('Rapport'),
                nl2br($this->request->data[$this->modelClass]['last_execution_report'])
            );

            $this->set('contenuVue', $maVue->getContenuVue());
        }
    }

    /**
     * Planification d'une tâche
     *
     * @version 4.3
     * @param type $id
     * @return type
     */
    public function admin_planifier($id = null) // phpcs:ignore
    {
        $sortie = false;
        if (empty($this->request->data)) {
            // Initialisations
            $this->request->data = $this->{$this->modelClass}->find('first', [
                    'recursive' => -1,
                    'conditions' => ['id' => $id]
                ]);
            if (empty($this->request->data)) {
                $this->Flash->set(
                    __('Id invalide : planification impossible.'),
                    ['element' => 'growl', 'params'=>['type' => 'important']]
                );
                $sortie = true;
            } else {
                if (!empty($this->request->data[$this->modelClass]['next_execution_time'])) {
                    $nextExecutionTime = explode(' ', $this->request->data[$this->modelClass]['next_execution_time']);
                    $this->request->data[$this->modelClass]['next_execution_date'] = $nextExecutionTime[0];
                    $this->request->data[$this->modelClass]['next_execution_heure'] = $nextExecutionTime[1];
                }
            }
        } else {
            // Initialisations avant sauvegarde
            $this->request->data[$this->modelClass]['next_execution_time'] = array_merge(
                $this->request->data[$this->modelClass]['next_execution_date'],
                $this->request->data[$this->modelClass]['next_execution_heure']
            );

            unset($this->request->data[$this->modelClass]['next_execution_date']);
            unset($this->request->data[$this->modelClass]['next_execution_heure']);
            $this->request->data[$this->modelClass]['modified_user_id'] = $this->Auth->user('id');
            if ($this->{$this->modelClass}->save($this->request->data)) {
                $nomCron = $this->{$this->modelClass}->field('nom');
                $this->Flash->set(
                    __('La tâche « %s » a été correctement planifiée.', $nomCron),
                    ['element' => 'growl']
                );
                $sortie = true;
            } else {
                $this->Flash->set(
                    __('Veuillez corriger les erreurs du formulaire.'),
                    ['element' => 'growl', 'params'=>['type' => 'danger']]
                );
            }
        }
        if ($sortie) {
            return $this->redirect(['action' => 'index']);
        } else {
            $this->pageTitle = Configure::read('appName') . ' : ' . __('Tâche planifiée') . ' : ' . __('Planification');
        }
    }

    /**
     * Liste des crons
     *
     * @version 4.3
     */
    public function admin_index() // phpcs:ignore
    {
        $this->request->data = $this->Cron->find('all', ['order' => ['Cron.nom ASC']]);
        // mise en forme pour la vue
        foreach ($this->request->data as &$cron) {
            $cron['Cron']['statusLibelle'] =
                $this->{$this->modelClass}->libelleStatus($cron['Cron']['last_execution_status']);
            $cron['Cron']['activeLibelle'] =
                $this->{$this->modelClass}->libelleActive($cron['Cron']['active']);
            $cron['Cron']['durationLibelle'] =
                $this->{$this->modelClass}->libelleDuration($cron['Cron']['execution_duration']);
        }
    }

    /**
     * @version 4.3
     * @param type $id
     */
    public function admin_unlock($id) // phpcs:ignore
    {
        if ($id != null) {
            $this->Cron->id = $id;
            $this->Cron->saveField('lock', false);
            $this->Flash->set(
                __('Tâche planifiée numéro %s déverrouillée !', $id),
                ['element' => 'growl', 'params'=>['type' => 'important']]
            );
        } else {
            $this->Flash->set(
                __('Tâche planifiée numéro %s introuvable !', $id),
                ['element' => 'growl', 'params'=>['type' => 'danger']]
            );
        }
        $this->redirect($this->previous);
    }

    /**
     * Fonction d'exécution du cron $id
     *
     * @version 4.3
     * @param integer $id id de la tâche a exécuter
     * @return redirect
     */
    public function admin_executer($id) // phpcs:ignore
    {
        // lecture du crons à exécuter
        $cron = $this->Cron->find('first', [
                'recursive' => -1,
                'fields' => ['id', 'nom', 'lock'],
                'conditions' => ['id' => $id]
            ]);

        // excécutions
        if (empty($cron)) {
            $this->Flash->set(
                __('ID invalide pour la tâche planifiée : exécution impossible.'),
                ['element' => 'growl', 'params'=>['type' => 'important']]
            );
        } elseif ($cron['Cron']['lock']) {
            $this->Flash->set(
                __('Tâche "%s" verrouillée !', $cron['Cron']['nom']),
                ['element' => 'growl']
            );
        } else {
            $cmd = 'nohup nice -n 10 -- '
                . APP . 'Console'
                . DS . 'cake Cron'
                 . ' runId ' . $id
                . ' -t ' . Configure::read('Config.tenantName')
                . ' >/dev/null 2>&1  & echo $!';
            $pid = shell_exec($cmd);
            sleep(3);
            $this->Flash->set(
                __('Tâche "%s" en cours d\'exécution (%s).', $cron['Cron']['nom'], trim($pid)),
                ['element' => 'growl']
            );
        }
        return $this->redirect($this->previous);
    }

    /**
     * Fonction d'exécution de tous les crons actifs (appelée par le shell 'cron')
     *
     * @version 4.3
     * @return type
     */
    public function admin_runCrons() // phpcs:ignore
    {
        // lecture du crons à exécuter
        $crons = $this->Cron->find('all', [
                'conditions' => ['active' => true, 'lock' => false, 'run_all' => true],
                'recursive' => -1,
                'fields' => ['id', 'nom', 'lock']
            ]);

        // excécutions
        if (empty($crons)) {
            $this->Flash->set(
                __(
                    'Aucune tâche planifiée active : exécution impossible.'
                ),
                ['element' => 'growl', 'params'=>['type' => 'important']]
            );
        }

        $flash='';
        foreach ($crons as $cron) {
            if ($cron['Cron']['lock']) {
                $flash .= __('Tâche "%s" verrouillée !', $cron['Cron']['nom']).'\n';
            } else {
                $cmd = 'nohup nice -n 10 -- '
                    . APP . 'Console' . DS . 'cake Cron runId '
                    . $cron['Cron']['id']
                    . ' -t ' . Configure::read('Config.tenantName')
                    . ' >/dev/null 2>&1  & echo $!';
                $pid = shell_exec($cmd);
                $flash .=__('Tâche "%s" en cours d\'exécution (%s).', $cron['Cron']['nom'], trim($pid)).'\n';
            }
        }
        $this->Flash->set($flash, ['element' => 'growl', 'params'=>['type' => 'important']]);
        sleep(3);

        return $this->redirect($this->previous);
    }
}
