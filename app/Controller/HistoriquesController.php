<?php

/**
 * HistoriquesController file
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       web-delib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * Class HistoriquesController
 *
 * @version 4.3
 * @package app.Controller
 */
class HistoriquesController extends AppController
{
    public $components = ['Paginator',
        'Filtre',
        'Auth' => ['mapActions' => ['read' => ['admin_index']]]
    ];

    /**
     * Affichage de l'historique des commentaires
     *
     * @version 4.3
     * @access public
     */
    public function admin_index() // phpcs:ignore
    {
        //on initialise le filtre avec le retour de donné pour paramétrer le filtre
        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);
        //initialisation des conditions pour la recherche
        $conditions = $this->Filtre->conditions();
        //condition permanente
        //requette avec paginate pour avoir des pages de 20 resultats
        //$conditions['Historique.created IS NOT NULL'] = '';
        //initialisation des champs du filtres
        $this->ajouterFiltre();

        $joins = [
                //$this->Historique->join('User', array('type' => 'INNER')),
                //$this->Historique->join('Deliberation', array('type' => 'INNER')),
        ];


        $this->paginate = ['Historique' => [
                'countField' => 'DISTINCT Historique.id',
                'fields' => ['DISTINCT Historique.id', 'Historique.user_id',
                    'Deliberation.id', 'Deliberation.objet',
                    'User.username', 'User.nom', 'User.prenom',
                    'commentaire', 'created', 'delib_id'],
                'conditions' => $conditions,
                'joins' => $joins,
                'maxLimit' => 20,
                'order' => ['Historique.created' => 'DESC',
                    'recursive' => -1
                ]
        ]];

        $historiques = $this->Paginator->paginate('Historique');

        $this->set('historiques', $historiques);
    }

    /**
     * Ajoute les filtres voulues
     *
     * @since 4.3
     * @version 5.0
     * @param type $projets --> paramètre pouvant contenir les informations vouluent ds une combobox
     */
    private function ajouterFiltre($projets = null)
    {
        if (!$this->Filtre->critereExists()) {
            //champ texte
            $this->Filtre->addCritere('Deliberationobjet', ['field' => 'Deliberation.objet',
                'inputOptions' => ['label' => __('Libellé du projet'),
                    'type' => 'text',
                    'title' => __('Filtre sur les libellés de projets')],
                'column' => 3]);

            $this->Filtre->addCritere('DeliberationId', ['field' => 'Historique.delib_id',
                'retourLigne' => true,
                'inputOptions' => ['label' => __('Id'),
                    'type' => 'text',
                    'title' => __('Filtre sur les ids des délibérations')],
                'column' => 3]);

            $this->Filtre->addCritere('UserLogin', ['field' => 'User.username',
                'inputOptions' => ['label' => __('Login'),
                    'type' => 'text',
                    'title' => __('Filtre sur les identifiants de connexion des utilisateurs')],
                'column' => 3]);

            $this->Filtre->addCritere('UserNom', ['field' => 'User.nom',
                'inputOptions' => ['label' => __('Nom'),
                    'type' => 'text',
                    'title' => __('Filtre sur les noms des utilisateurs')],
                'column' => 3]);

            $this->Filtre->addCritere('UserPrenom', ['field' => 'User.prenom',
                'retourLigne' => true,
                'inputOptions' => ['label' => __('Prénom'),
                    'type' => 'text',
                    'title' => __('Filtre sur les prénoms des utilisateurs')],
                'column' => 3]);

            //champ datetimepicker
            $this->Filtre->addCritere('dateDebut', ['field' => 'Historique.created >=',
                'inputOptions' => ['label' => __('date et heure de début'),
                    'type' => 'date',
                    'style' => 'cursor:pointer;background-color: white;',
                    'help' => __('Cliquez sur le champ ci-dessus pour choisir la date et heure de début'),
                    //'disabled' => false,//readonly
                    'title' => __('Filtre sur la date de début.')],
                'Datepicker' => ['language' => 'fr',
                    'autoclose' => 'true',
                    'format' => 'yyyy-mm-dd hh:00:00',
                    'startView' => 'decade', //decade
                    'minView' => 'day',
                ],
                'column' => 3]);

            $this->Filtre->addCritere('dateFin', ['field' => 'Historique.created <=',
                'inputOptions' => ['label' => __('date et heure de fin'),
                    'type' => 'date',
                    'style' => 'cursor:pointer;background-color: white;',
                    'help' => __('Cliquez sur le champ ci-dessus pour choisir la date et heure de fin'),
                    'readonly' => '',
                    'title' => __('Filtre sur la date de fin.')],
                'Datepicker' => ['language' => 'fr',
                    'autoclose' => 'true',
                    'format' => 'yyyy-mm-dd hh:00:00',
                    'startView' => 'decade', //decade
                    'minView' => 'day',
                ],
                'column' => 3]);

            //champ select
            $this->Filtre->addCritere('difDate', ['retourLigne' => true,
                'inputOptions' => ['type' => 'select',
                    'data-placeholder' => __('Sélectioner une plage de dates'),
                    'data-allow-clear' => true,
                    'label' => __('Plage de dates'),
                    'options' => ['-1 hour' => __('1 heure'),
                        '-1 day' => __('1 jour'),
                        '-1 week' => __('1 semaine'),
                        '-1 month' => __('1 mois'),
                        '-1 year' => __('1 an')],
                ],
                'column' => 3]);
        }
    }
}
