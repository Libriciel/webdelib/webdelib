<?php

App::uses('HttpSocket', 'Network/Http');

/**
 * Contrôleur des Informations supplémentaires
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       webdelib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 * @version     v4.3
 * @package     app.Controller
 */
class InfosupsController extends AppController
{
    public $resultType = [
        'Voie', 'POI'
    ];
    public $uses = [
        'Infosup',
        'Infosupdef'
    ];

    public $components = [
        'RequestHandler',
    ];

    /**
     * Retourn l'information supplémentaire demandé en paramètre
     * @access public
     * @param int $foreign_key
     * @param int $infosupdef_id
     * @return response
     * @version 4.4
     */
    public function download($id, $type = null)
    {
        $infosup = $this->Infosup->find('first', [
            'fields' => ['content', 'edition_data', 'file_name', 'file_type'],
            'conditions' => ['id' => $id],
            'recursive' => -1,
        ]);

        switch ($type) {
            case 'edition_data':
                $content = $infosup['Infosup']['edition_data'];
                $filename = 'edition_data.odt';
                $typemime = 'application/vnd.oasis.opendocument.text';
                break;

            default:
                $content = $infosup['Infosup']['content'];
                $filename = $infosup['Infosup']['file_name'];
                $typemime = $infosup['Infosup']['file_type'];
                break;
        }

        $this->response->disableCache();
        $this->response->body($content);
        $this->response->type($typemime);
        $this->response->download($filename);

        return $this->response;
    }


    /**
     * Gère l'appel ajax fait pour récupérer le geojson
     */
    public function getGeoJsonFromApi()
    {
        $HttpSocket = new HttpSocket([
                'ssl_verify_peer' => false,
                'ssl_allow_self_signed' => true
            ]);
        $infosup = $this->Infosupdef->find('first', [
            'fields' => ['api','api_type','api_limit','api_query','correlation'],
            'conditions' => [
                'model' => $this->request->query['model'],
                'id'=> $this->request->query['id'],
                'actif' => true,
            ],
            'recursive' => -1
            ]);


        //variabiliser la requete.
        $limit = explode("=", $infosup['Infosupdef']['api_limit']);
        $correlations = json_decode($infosup['Infosupdef']['correlation']);
        try {
            $response = $HttpSocket->get($infosup['Infosupdef']['api'], [
                'autocomplete' => '0',
                $infosup['Infosupdef']['api_query'] => $this->request->query['query'],
                'type' => $infosup['Infosupdef']['api_type'] ?? null,
                $limit[0] => $limit[1],
            ]);
            // ajout du mot clef pour afficher le résultat.
            $json = json_decode($response->body);
            $json->{$correlations->label} = 'label';
            $reponseJson = json_encode($json);
        } catch (Exception $e) {
            $reponseJson = '{}';
        }

        $this->autoRender = false;
        $this->response->type(['json' => 'text/x-json']);
        $this->RequestHandler->respondAs('json');
        $this->response->body($reponseJson);

        return $this->response;
    }
}
