<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class CompteursController extends AppController
{
    public $components = [
        'Auth' => [
            'mapActions' => [
                'create' => ['admin_add'],
                'read' => ['admin_index', 'admin_view'],
                'update' => ['admin_edit'],
                'delete' => ['admin_delete'],
            ]
        ]
    ];

    /**
     * @version 4.3
     * @access public
     */
    public function beforeFilter()
    {
        if (property_exists($this, 'demandePost')) {
            call_user_func_array([$this->Security, 'requirePost'], $this->demandePost);
        }
        parent::beforeFilter();
    }

    /**
     * @version 4.3
     * @access public
     */
    public function admin_index() // phpcs:ignore
    {
        $this->set('compteurs', $this->Compteur->find('all', [
            'contain' => ['Typeseance', 'Sequence'],
            'recursive' => -1,
            'order' => 'Compteur.nom ASC']));
        $this->render('index');
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     */
    public function admin_view($id) // phpcs:ignore
    {
        if (!$this->Compteur->exists($id)) {
            $this->Flash->set(
                __('ID invalide pour le compteur'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            $this->redirect($this->previous);
        } else {
            $this->set('compteur', $this->Compteur->find('first', ['conditions' => ['Compteur.id' => $id]]));
        }

        $this->render('view');
    }

    /**
     * @version 4.3
     * @access public
     */
    public function admin_add() // phpcs:ignore
    {
        $this->_add_edit();
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     */
    public function admin_edit($id) // phpcs:ignore
    {
        $this->_add_edit($id);
    }

    /**
     * @version 4.3
     * @access private
     * @param type $id
     */
    private function _add_edit($id = null) // phpcs:ignore
    {
        $readOnlyDefReInit = false;
        if ($id) {
            $compteur = $this->Compteur->find('first', [
                'conditions'=> [
                    'Compteur.id' => $id
                ],
                'recursive' => -1
            ]);
            $readOnlyDefReInit = !empty($compteur['Compteur']['def_reinit']);
            if (!$compteur) {
                $this->Flash->set(__('Invalide id pour le compteur'), [
                    'element' => 'growl',
                    'params' => ['type' => 'danger']]);

                $this->redirect($this->previous);
            }
        }

        if ($this->request->is(['post', 'put'])) {
            if (strlen(str_replace('#', '', $this->data['Compteur']['def_compteur'])) <= 15) {
                if ($this->Compteur->save($this->data)) {
                    $this->Flash->set(
                        __(
                            'Le compteur "%s" a été %s',
                            [h($this->data['Compteur']['nom']), (!empty($id) ? 'modifié' : 'ajouté')]
                        ),
                        ['element' => 'growl','params' => ['type' => 'success']]
                    );
                    $this->redirect($this->previous);
                } else {
                    $this->Flash->set(__('Veuillez corriger les erreurs ci-dessous.'), [
                        'element' => 'growl',
                        'params' => ['type' => 'danger']]);
                }
            } else {
                $this->Flash->set(
                    __(
                        'La valeur générée par l\'attribut "Définition du compteur" ne doit pas comporter'
                        .' plus de 15 caractères. (sans les dièses)'
                    ),
                    [
                        'element' => 'growl',
                        'params' => ['type' => 'danger']
                    ]
                );
            }
        }
        if (!$this->request->data && !empty($compteur)) {
            $this->request->data = $compteur;
        }

        $this->Compteur->Sequence->recursive = -1;
        $this->set('sequences', $this->Compteur->Sequence->find('list'));


        $aideformatOptions = [
            '#s#' => __('Numéro de la séquence'),
            '#S#' => __('Numéro de la séquence sur 1 chiffre'),
            '#SS#' => __('Numéro de la séquence sur 2 chiffres (complété par un souligné)'),
            '#SSS#' => __('Numéro de la séquence sur 3 chiffres (complété par des soulignés)'),
            '#SSSS#' => __('Numéro de la séquence sur 4 chiffres (complété par des soulignés)'),
            '#00#' => __('Numéro de la séquence sur 2 chiffres (complété par un zéro)'),
            '#000#' => __('Numéro de la séquence sur 3 chiffres (complété par des zéros)'),
            '#0000#' => __('Numéro de la séquence sur 4 chiffres (complété par des zéros)'),
            '#AAAA#' => __('Année sur 4 chiffres'),
            '#AA#' => __('Année sur 2 chiffres'),
            '#M#' => __('Numéro du mois sans zéro significatif'),
            '#MM#' => __('Numéro du mois avec zéro significatif'),
            '#J#' => __('Numéro du jour sans zéro significatif'),
            '#JJ#' => __('Numéro du jour avec zéro significatif'),
            '#p#' => __('Numéro de la position')
        ];
        $this->set('aideformatOptions', $aideformatOptions);
        $aideformatDateOptions = [
            '#AAAA#' => __('Année'),
            '#MM#' => __('Mois'),
            '#JJ#' => __('Jour')
        ];
        $this->set('aideformatDateOptions', $aideformatDateOptions);
        $this->set('readOnlyDefReInit', $readOnlyDefReInit);

        $this->render('edit');
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     */
    public function admin_delete($id) // phpcs:ignore
    {
        $this->delete($id);
    }

    /**
     * @version 4.3
     * @access private
     * @param type $id
     */
    private function delete($id)
    {
        $compteur = $this->Compteur->find('first', [
            'fields' => ['id', 'nom'],
            'conditions'=> [
                'Compteur.id' => $id
            ],
        ]);
        if (empty($compteur)) {
            $this->Flash->set(
                __('ID invalide pour le compteur'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
        } elseif (!empty($compteur['Typeseance'])) {
            $this->Flash->set(
                __(
                    'Le compteur \'%s\' est utilisé par un type de séance. Suppression impossible.',
                    $compteur['Compteur']['nom']
                ),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
        } elseif ($this->Compteur->delete($id)) {
            $this->Flash->set(
                __('Le compteur \'%s\' a été supprimé', $compteur['Compteur']['nom']),
                ['element' => 'growl','params'=> ['type' => 'success']]
            );
        }
        $this->redirect($this->previous);
    }
}
