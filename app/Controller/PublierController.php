<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Class WopiController
 *
 * @version 6.0.x
 * @package app.Controller
 */
class PublierController extends AppController
{
    /**
     * @version 6.0.X
     * @access public
     * @var type
     */
    public $components = [
        'RequestHandler',
        'Progress',
        'Filtre',
        'Paginator',
        'Publication',
        'PDFStamp',
        'Auth' => [
            'mapActions' => [
                'sendToPublish',
                'sendToPublishDeliberation',
                'allow' => ['download', 'downloadJSON', 'publish'],
            ],
        ]];

    /**
     * [public description]
     * @var [type]
     */
    public $uses = [
        'Deliberation',
        'Annexe',
        'DeliberationSeance',
        'DeliberationTypeseance',
        'Infosup',
        'Seance',
        'User',
        'Listepresence',
        'Vote',
        'Nomenclature',
        'Service',
        'Circuit',
        'Theme',
        'Typeseance',
        'Typeacte',
        'Nature',
        'TdtMessage',
        'Historique',
        'Typologiepiece',
        'Collectivite'
    ];

    /**
     * @return void
     */
    public function sendToPublish()
    {
        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);

        $conditions = $this->handleConditions($this->Filtre->conditions());

        //Rechercher les projets transmissibles
        $db = $this->DeliberationSeance->getDataSource();
        $subQuery = [
            'fields' => [
                'Deliberation.id'
            ],
            'contain' => false,
            'joins' => [
                $this->Deliberation->join('Typeacte', ['type' => 'INNER', 'conditions' => [
                    'Typeacte.publier' => true,
                    'Typeacte.teletransmettre' => true,
                    'Typeacte.deliberant' => false
                ]]),
            ],
            'conditions' => [
                'Deliberation.etat' => [5,11],
                'Deliberation.tdt_ar_date <>' => null,
                'Deliberation.tdt_ar <>' => null,
            ]
        ];
        $subQueryTransmissibles = $this->DeliberationSeance->sql($subQuery);

        //Rechercher les projets non transmissibles
        $db = $this->DeliberationSeance->getDataSource();
        $subQuery = [
            'fields' => [
                'Deliberation.id'
            ],
            'contain' => false,
            'joins' => [
                $this->Deliberation->join('Typeacte', ['type' => 'INNER', 'conditions' => [
                    'Typeacte.publier' => true,
                    'Typeacte.teletransmettre' => false,
                    'Typeacte.deliberant' => false
                ]]),
            ],
            'conditions' => [
                'Deliberation.etat' => [3,4,7],
                'Deliberation.signee' => true
            ]
        ];

        $subQueryNonTransmissibles = $this->DeliberationSeance->sql($subQuery);
        $subQuery = ' "Deliberation"."id" IN (('
            . $subQueryTransmissibles
            . ') UNION ('
            . $subQueryNonTransmissibles
            . '))';
        $subQueryExpression = $db->expression($subQuery);
        $conditions[] = $subQueryExpression;

        $conditions['Deliberation.date_acte >='] = '2022-07-01 00:00:00';

        $this->paginate = ['Deliberation' => [
            'countField' => 'DISTINCT Deliberation.id',
            'fields' => [
                'DISTINCT Deliberation.id',
                'Deliberation.objet',
                'Deliberation.objet_delib',
                'Deliberation.num_delib',
                'Deliberation.tdt_dateAR',
                'Deliberation.tdt_ar_date',
                'Deliberation.tdt_ar',
                'Deliberation.parapheur_id',
                'Deliberation.num_pref',
                'Deliberation.typologiepiece_code',
                'Deliberation.tdt_document_papier',
                'Deliberation.etat',
                'Deliberation.titre',
                'Deliberation.tdt_id',
                'Deliberation.pastell_id',
                'Deliberation.typeacte_id',
                'Deliberation.theme_id',
                'Deliberation.service_id',
                'Deliberation.tdt_status',
                'Deliberation.tdt_message',
                'Deliberation.publier_etat',
                'Deliberation.publier_date',
            ],
            'conditions' => $conditions,
            'order' => ['Deliberation.id' => 'DESC'],
            'limit' => 20,
            'allow' => ['typeacte_id'],
            'recursive' => -1]];

        // On affiche que les delibs vote pour.
        $deliberations = $this->Paginator->paginate('Deliberation');

        $this->ProjetTools->sortProjetSeanceDate($deliberations);

        $this->ajouterFiltre($deliberations);

        $this->set('deliberations', $deliberations);
    }

    public function sendToPublishDeliberation()
    {
        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);

        $conditions = $this->handleConditions($this->Filtre->conditions());
        if (isset($conditions['Seance.id'])) {
            $seance_id = $conditions['Seance.id'];
            unset($conditions['Seance.id']);
        }
        $conditions['Deliberation.date_acte >='] = '2022-07-01 00:00:00';

        //Rechercher les projets transmissibles
        $db = $this->DeliberationSeance->getDataSource();
        $subQuery = [
            'fields' => [
                'DeliberationSeance.deliberation_id'
            ],
            'contain' => false,
            'joins' => [
                $this->DeliberationSeance->join('Deliberation', ['type' => 'INNER']),
                $this->DeliberationSeance->join('Seance', ['type' => 'INNER']),
                $this->Deliberation->join('Typeacte', ['type' => 'INNER', 'conditions' => [
                    'Typeacte.teletransmettre' => true,
                    'Typeacte.publier' => true,
                    'Typeacte.deliberant' => true]]),
            ],
            'conditions' => [
                'Deliberation.etat' => [5,11],
                'Deliberation.tdt_ar_date <>' => null,
                'Deliberation.tdt_ar <>' => null,
            ]
        ];

        $subQueryTransmissibles = $this->DeliberationSeance->sql($subQuery);

        //Rechercher les projets non transmissibles
        $db = $this->DeliberationSeance->getDataSource();
        $subQuery = [
            'fields' => [
                'DeliberationSeance.deliberation_id'
            ],
            'contain' => false,
            'joins' => [
                $this->DeliberationSeance->join('Deliberation', ['type' => 'INNER']),
                $this->DeliberationSeance->join('Seance', ['type' => 'INNER']),
                $this->Deliberation->join('Typeacte', ['type' => 'INNER', 'conditions' => [
                    'Typeacte.teletransmettre' => false,
                    'Typeacte.publier' => true,
                    'Typeacte.deliberant' => true]]),
            ],
            'conditions' => [
                'Deliberation.etat' => [3,4,7],
                'Deliberation.signee' => true
            ]
        ];

        $subQueryNonTransmissibles = $this->DeliberationSeance->sql($subQuery);
        $subQuery = ' "Deliberation"."id" IN (('
            . $subQueryTransmissibles
            . ') UNION (' . $subQueryNonTransmissibles
            . '))';
        $subQueryExpression = $db->expression($subQuery);
        $conditions[] = $subQueryExpression;

        $this->paginate = ['Deliberation' => [
            'countField' => 'DISTINCT Deliberation.id',
            'fields' => [
                'DISTINCT Deliberation.id',
                'Deliberation.objet',
                'Deliberation.objet_delib',
                'Deliberation.num_delib',
                'Deliberation.tdt_dateAR',
                'Deliberation.tdt_ar_date',
                'Deliberation.tdt_ar',
                'Deliberation.parapheur_id',
                'Deliberation.num_pref',
                'Deliberation.typologiepiece_code',
                'Deliberation.tdt_document_papier',
                'Deliberation.etat',
                'Deliberation.titre',
                'Deliberation.tdt_id',
                'Deliberation.pastell_id',
                'Deliberation.typeacte_id',
                'Deliberation.theme_id',
                'Deliberation.service_id',
                'Deliberation.circuit_id',
                'Deliberation.tdt_status',
                'Deliberation.tdt_message',
                'Deliberation.publier_etat',
                 'Deliberation.publier_date',
            ],
            'joins' => array_merge(
                [
                    $this->Deliberation->join('DeliberationSeance', ['type' => 'LEFT']),
                    $this->Deliberation->join('DeliberationTypeseance', ['type' => 'LEFT']),
                ],
                $this->handleJoins()
            ),
            'conditions' => $conditions,
            'contain' => [
                'Service' => ['fields' => ['name']],
                'Circuit' => ['fields' => ['nom']],
                'Theme' => ['fields' => ['libelle']],
                'Annexe' => [
                    'fields' => ['id', 'filename', 'filetype', 'typologiepiece_code'],
                    'conditions' => ['joindre_ctrl_legalite' => true],
                    'order' => ['position']
                ],
                'Typeacte' => ['fields' => ['name', 'teletransmettre']],
                'TdtMessage' => ['fields' => ['tdt_id', 'tdt_type', 'tdt_etat', 'parent_id'],
                    'conditions' => ['parent_id is null'],
                    'Reponse' => ['fields' => ['tdt_id', 'tdt_type', 'tdt_etat']]],
                'DeliberationTypeseance' => ['fields' => ['id'],
                    'Typeseance' => [
                        'fields' => ['id', 'libelle', 'color', 'action'
                        ],
                    ]],
                'DeliberationSeance' => [
                    'fields' => ['id'],
                    'Seance' => [
                        'fields' => ['id', 'date', 'type_id'],
                        'Typeseance' => [
                            'fields' => ['id', 'libelle', 'color', 'action'],
                        ],

                    ]]
            ],
            'order' => ['Deliberation.id' => 'DESC'],
            'limit' => 20,
            'allow' => ['typeacte_id'],
            'recursive' => -1]];

        // On affiche que les delibs vote pour.
        $deliberations = $this->Paginator->paginate('Deliberation');
        $this->ProjetTools->sortProjetSeanceDate($deliberations);

        $this->ajouterFiltreDeliberation($deliberations);

        $this->set('deliberations', $deliberations);
        $this->render('send_to_publish');
    }

    /**
     * @access private
     *
     * @param type $conditions
     * @return type
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    private function handleConditions($conditions)
    {
        $projet_type_ids = [];
        $projet_seance_ids = [];

        if (isset($conditions['DeliberationTypeseance.typeseance_id'])) {
            $type_id = $conditions['DeliberationTypeseance.typeseance_id'];
            $typeseances = $this->DeliberationTypeseance->find(
                'all',
                ['conditions' => ['DeliberationTypeseance.typeseance_id' => $type_id],
                'recursive' => -1]
            );
            foreach ($typeseances as $typeseance) {
                $projet_type_ids[] = $typeseance['DeliberationTypeseance']['deliberation_id'];
            }
            unset($conditions['DeliberationTypeseance.typeseance_id']);
        }
        if (isset($conditions['DeliberationSeance.seance_id'])) {
            $projet_seance_ids = $this->Seance->getDeliberationsId($conditions['DeliberationSeance.seance_id']);
            unset($conditions['DeliberationSeance.seance_id']);
        }
        $result = null;
        if (!empty($projet_type_ids) && !empty($projet_seance_ids)) {
            $result = array_intersect($projet_type_ids, $projet_seance_ids);
        } elseif (empty($projet_type_ids) && empty($projet_seance_ids)) {
            return $conditions;
        } elseif (empty($projet_type_ids)) {
            $result = $projet_seance_ids;
        } elseif (empty($projet_seance_ids)) {
            $result = $projet_type_ids;
        }

        if (isset($conditions['Deliberation.id'])) {
            $conditions['Deliberation.id'] = array_intersect($conditions['Deliberation.id'], $result);
        } elseif (!empty($result)) {
            $conditions['Deliberation.id'] = $result;
        }

        return $conditions;
    }

    /**
     * @access private
     * @param type $conditions
     * @return type
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    private function handleJoins()
    {
        $joins = [
            ['table' => 'deliberations_seances',
                'alias' => 'DeliberationSeance_Filter',
                'type' => 'LEFT',
                'conditions' => [
                    'Deliberation.id = DeliberationSeance_Filter.deliberation_id',
                ]
            ],
            ['table' => 'seances',
                'alias' => 'Seance_Filter',
                'type' => 'LEFT',
                'conditions' => [
                    'DeliberationSeance_Filter.seance_id = Seance_Filter.id'
                ]
            ],
            ['table' => 'deliberations_typeseances',
                'alias' => 'DeliberationTypeseance_Filter',
                'type' => 'LEFT',
                'conditions' => [
                    'Deliberation.id = DeliberationTypeseance_Filter.deliberation_id'
                ]
            ],
        ];

        return $joins;
    }

    /**
     * @version 5.1
     * @since 4.3
     * @access private
     * @param type $projets
     */
    private function ajouterFiltreDeliberation(&$projets)
    {
        if (!$this->Filtre->critereExists() && !empty($projets)) {
            $conditions[] = 'Seance.id IN ('
                . 'SELECT DISTINCT deliberations_seances.seance_id'
                . ' FROM deliberations_seances '
                . ' INNER JOIN deliberations  ON ( deliberations.id=deliberations_seances.deliberation_id )'
                . ' INNER JOIN seances  ON ( seances.id=deliberations_seances.seance_id )'
                . ' INNER JOIN typeseances ON ( typeseances.id=seances.type_id )'
                . ' INNER JOIN typeactes  ON ( typeactes.id=deliberations.typeacte_id )'
                . ' WHERE typeactes.teletransmettre = TRUE'
                . ' )';

            $seances = $this->Seance->find('all', [
                'fields' => ['Seance.id', 'Seance.type_id', 'Seance.date'],
                'conditions' => $conditions,
                //'conditions' => array('Seance.traitee' => 0),
                'contain' => ['Typeseance.libelle', 'Typeseance.retard'],
                'order' => ['Seance.date' => 'DESC', 'Typeseance.libelle' => 'ASC'],
            ]);

            $options_seances = [];
            foreach ($seances as $seance) {
                //Voir tous les projets ou tous les futurs dates avec un delais respecté
                $options_seances[$seance['Seance']['id']] =
                    $seance['Typeseance']['libelle'] . ' : '
                    . CakeTime::i18nFormat($seance['Seance']['date'], '%A %e %B %Y')
                    . ' à '
                    . CakeTime::i18nFormat($seance['Seance']['date'], '%k:%M');
            }

            $this->Filtre->addCritere('DeliberationSeanceId', ['field' => 'DeliberationSeance_Filter.seance_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des séances'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'label' => __('Séances'),
                    'options' => $options_seances]]);


            $typeseances = [];
            foreach ($projets as $projet) {
                if (!empty($projet['DeliberationTypeseance'])) {
                    foreach ($projet['DeliberationTypeseance'] as $typeseance) {
                        if (!array_key_exists($typeseance['id'], $typeseances)) {
                            $typeseances[$typeseance['Typeseance']['id']] = $typeseance['Typeseance']['libelle'];
                        }
                    }
                }
            }
            $this->Filtre->addCritere('DeliberationTypeseanceId', [
                'field' => 'DeliberationTypeseance_Filter.typeseance_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des séances'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'label' => __('Type de séance'),
                    'options' => $typeseances]]);

            $typeactes = $this->Deliberation->Typeacte->find('list', [
                'recursive' => -1,
                'order' => ['Typeacte.name' => 'ASC'],
                'allow' => ['Typeacte.id' => 'read']]);

            $this->Filtre->addCritere('Typeacte', [
                'field' => 'Deliberation.typeacte_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des séances'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'label' => __('Type d\'acte'),
                    'options' => $typeactes
                ]]);
            $themes = $this->Deliberation->Theme->generateTreeListByOrder(
                ['Theme.actif' => '1'],
                '&nbsp;&nbsp;&nbsp;&nbsp;'
            );
            $this->Filtre->addCritere('ThemeId', [
                'field' => 'Deliberation.theme_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'label' => __('Thème'),
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des thèmes'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'options' => $themes,
                    'escape' => false
                ]]);
            $services = $this->Service->find('list', [
                'fields' => ['id', 'name'],
                'order' => ['Service.name' => 'ASC'],
                'conditions' => ['actif' => true], 'recursive' => -1]);
            $this->Filtre->addCritere('ServiceId', [
                'field' => 'Deliberation.service_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'label' => __('Service émetteur'),
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des services éméteur'),
                    'data-allow-clear' => true,
                    'multiple' => 'multiple',
                    'options' => $services,
                ]]);
            $this->Filtre->addCritere('Publier', [
                'field' => 'Deliberation.publier_etat',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Publié'),
                    'options' => [
                        0 => __('En attente de publication'),
                        1 => __('Publié'),
                        2 => __('Publié manuellement'),
                    ],
                    'data-placeholder' => __('Sélectionner un état'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'title' => __('Filtre sur la publication')],
                'column' => 2
            ]);
            //Affichage des utilisateurs actifs par défaut
            //$this->Filtre->setCritere('Publier', 0);
        }
    }


    /**
     * @version 5.1
     * @since 4.3
     * @access private
     * @param array $projets
     */
    private function ajouterFiltre(&$projets)
    {
        if (!$this->Filtre->critereExists() && !empty($projets)) {
            $typeactes = $this->Deliberation->Typeacte->find('list', [
                'recursive' => -1,
                'order' => ['Typeacte.name' => 'ASC'],
                'allow' => ['Typeacte.id' => 'read']]);

            $this->Filtre->addCritere('Typeacte', [
                'field' => 'Deliberation.typeacte_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des séances'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'label' => __('Type d\'acte'),
                    'options' => $typeactes
                ]]);
            $themes = $this->Deliberation->Theme->generateTreeListByOrder(
                ['Theme.actif' => '1'],
                '&nbsp;&nbsp;&nbsp;&nbsp;'
            );
            $this->Filtre->addCritere('ThemeId', [
                'field' => 'Deliberation.theme_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'label' => __('Thème'),
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des thèmes'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'options' => $themes,
                    'escape' => false
                ]]);
            $services = $this->Service->find('list', [
                'fields' => ['id', 'name'],
                'order' => ['Service.name' => 'ASC'],
                'conditions' => ['actif' => true], 'recursive' => -1]);
            $this->Filtre->addCritere('ServiceId', [
                'field' => 'Deliberation.service_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'label' => __('Service émetteur'),
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des services éméteur'),
                    'data-allow-clear' => true,
                    'multiple' => 'multiple',
                    'options' => $services,
                ]]);
            $this->Filtre->addCritere('Publier', [
                'field' => 'Deliberation.publier_etat',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Publié'),
                    'options' => [
                        0 => __('En attente de publication'),
                        1 => __('Publié'),
                        2 => __('Publié manuellement')],
                    'data-placeholder' => __('Sélectionner un état'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'title' => __('Filtre sur la publication')],
                'column' => 2
            ]);
            //Affichage des utilisateurs actifs par défaut
            //$this->Filtre->setCritere('Publier', 0);
        }
    }

    /**
     * @param $id
     * @return mixed
     * @throws Exception
     */
    public function download($id)
    {
        $this->Deliberation->recursive=-1;
        $projet = $this->Deliberation->findById($id, ['num_delib']);
        $zipName = __('%s.zip', $projet['Deliberation']['num_delib']);

        // envoi au client
        $this->response->disableCache();
        $this->response->body($this->createEnveloppeOpenDataZip($projet['Deliberation']['num_delib'], $id));
        $this->response->type('application/zip');
        $this->response->download($zipName);

        return $this->response;
    }

    /**
     * @return false|string
     */
    private function createEnveloppeOpenDataJSON($projetId)
    {
        return $this->createEnveloppeOpenDataSCDLDeliberation($projetId);
    }

    private function createDocumentActePermalien($projetId)
    {
        $projet = $this->Deliberation->find('first', [
            'fields' => ['id','delib_pdf','tdt_ar', 'publier_date'],
            'conditions' => ['Deliberation.id' => $projetId],
            'recursive' => -1
        ]);

        return $this->PDFStamp->stamp($projet['Deliberation']['delib_pdf'], $projet['Deliberation']);
    }

    /**
     * @link https://guides.data.gouv.fr/publier-des-donnees/guide-qualite/preparer-un-jeu-de-donnees-de-qualite
     * @return false|string
     * @throws JsonException
     */
    private function createEnveloppeOpenDataSCDLDeliberation($projetId)
    {
        $projet = $this->Deliberation->find('first', [
            'fields' => [
                'id',
                'objet',
                'delib_pdf',
                'tdt_ar',
                'num_delib',
                'date_acte',
                'vote_nb_oui',
                'vote_nb_non',
                'vote_nb_abstention',
                'tdt_ar_date',
                'num_pref'
            ],
            'conditions' => ['Deliberation.id' => $projetId],
            'recursive' => -1
        ]);

        $collectivite = $this->Collectivite->find('first', [
            'recursive' => -1,
        ]);

        // nombre d'acteurs présents
        $nbActeurs = $this->Listepresence->find('count', [
            'recursive' => -1,
            'conditions' => ['Listepresence.delib_id' => $projetId]
        ]);
        $nbVotant = '';
        $voteNbOui = '';
        $voteNbNon = '';
        $voteNbAbstention = '';
        if ($projet['Deliberation']['vote_nb_oui'] >= 0 ||
            $projet['Deliberation']['vote_nb_abstention'] >= 0 ||
            $projet['Deliberation']['vote_nb_non'] >= 0
        ) {
            $voteNbOui = $this->formatVote($projet['Deliberation']['vote_nb_oui']);
            $voteNbNon = $this->formatVote($projet['Deliberation']['vote_nb_non']);
            $voteNbAbstention = $this->formatVote($projet['Deliberation']['vote_nb_abstention']);
            $nbVotant = $voteNbOui + $voteNbNon + $voteNbAbstention ;
        }

        return json_encode([
            'COLL_NOM' => $collectivite['Collectivite']['nom'],
            'COLL_SIRET' => $collectivite['Collectivite']['siret'],
            'DELIB_ID' => $projet['Deliberation']['num_delib'],
            'DELIB_DATE' => CakeTime::i18nFormat($projet['Deliberation']['date_acte'], '%Y-%m-%d'),
            'DELIB_MATIERE_CODE' => $this->formatCodeMaterial($projet['Deliberation']['num_pref']),
            'DELIB_MATIERE_NOM' => $this->formatNomenclaturesByNumPref($projet['Deliberation']['num_pref']),
            'DELIB_OBJET' => chr(34) . $projet['Deliberation']['objet'] . chr(34),
            'BUDGET_ANNEE' => '',
            'BUDGET_NOM' => '',
            'PREF_ID' => Configure::read('PUBLICATION_PREF_ID'),
            'PREF_DATE' => CakeTime::i18nFormat($projet['Deliberation']['tdt_ar_date'], '%Y-%m-%d'),
            'VOTE_EFFECTIF' => !empty($nbActeurs) ? $nbActeurs : 0,
            'VOTE_REEL' => !empty($nbVotant) ? $nbVotant : 0,
            'VOTE_POUR' => $voteNbOui,
            'VOTE_CONTRE' => $voteNbNon,
            'VOTE_ABSTENTION' => $voteNbAbstention,
            'DELIB_URL' => __(
                '%s%s.pdf',
                substr(Configure::read('PUBLICATION_PERMALIEN'), -1) !== '/' ?
                Configure::read('PUBLICATION_PERMALIEN').'/' : Configure::read('PUBLICATION_PERMALIEN'),
                $projet['Deliberation']['num_delib']
            )
        ], JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

    /**
     * Fonction récursive de doList
     *
     * @version 4.3
     * @access private
     * @param string $code
     * @return string
     */
    private function formatNomenclaturesByNumPref($numPref)
    {
        if (empty($numPref)) {
            return '';
        }

        $nomenclature = $this->Nomenclature->findByName($numPref, ['id']);

        if (empty($nomenclature['Nomenclature']['id'])) {
            return '';
        }

        $nomenclatures = [];
        $this->doListNomenclature($nomenclatures, $nomenclature['Nomenclature']['id']);
        $nomenclatures = array_reverse($nomenclatures);
        for ($i = 0; $i <= 1; $i++) {
            $nomenclatures[$i] = mb_strtolower(trim(substr(
                $nomenclatures[$i],
                strpos($nomenclatures[$i], " ")
            )));
        }

        return $nomenclatures[0] . (count($nomenclatures)>1 ? '/' . $nomenclatures[1] : '') ;
    }

    private function doListNomenclature(&$nomenclatures, $nomenclatureId)
    {
        $nomenclature = $this->Nomenclature->find('first', [
            'conditions' => ['Nomenclature.id' => $nomenclatureId],
            'fields' => ['libelle', 'parent_id'],
            'recursive' => -1]);

        $nomenclatures[] = $nomenclature['Nomenclature']['libelle'];

        if (empty($nomenclature['Nomenclature']['parent_id'])) {
            return $nomenclatures;
        }

        return $this->doListNomenclature($nomenclatures, $nomenclature['Nomenclature']['parent_id']);
    }

    private function formatCodeMaterial($code): string
    {
        if (empty($code)) {
            return '';
        }
        $aCode = explode('.', $code);
        return trim($aCode[0]) .'.'. trim($aCode[1]);
    }

    private function formatVote($vote): int
    {
        return !empty($vote) ? $vote : 0;
    }


    /**
     * @param $json
     * @return string
     * @throws JsonException
     */
    private function convertSCDLdataJSONToCSV($json): string
    {
        $jsonArray  = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
        $csv = '';
        $firstLineKey = array_keys($jsonArray);
        foreach ($firstLineKey as $key) {
            $csv .= $key . ',';
        }
        $csv = rtrim($csv, ",");

        $csv .= chr(13). chr(10);
        foreach ($jsonArray as $value) {
            $csv .= $value . ',';
        }

        return rtrim($csv, ",");
    }


    /**
     * @return false|string
     */
    private function createEnveloppeOpenDataZip($fileName, $projetId)
    {
        $folderTmp = new Folder(AppTools::newTmpDir(TMP . 'files' . DS . 'publier'), true, 0777);

        $json = $this->createEnveloppeOpenDataJSON($projetId);
        $csv = $this->convertSCDLdataJSONToCSV($json);

        $zip = new ZipArchive();
        $zip->open($folderTmp->pwd() . DS . $fileName . '.zip', ZipArchive::CREATE);
        $zip->addFromString($fileName . '.csv', $csv);
        $zip->addFromString($fileName . '.json', $json);
        $zip->addFromString($fileName . '.pdf', $this->createDocumentActePermalien($projetId));
        //Ajout de toutes les annexes du projets
        $annexes = $this->Deliberation->Annexe->getAnnexesFromDelibId($projetId);
        if (!empty($annexes)) {
            $acte = $this->Deliberation->find('first', [
                'fields' => ['id','delib_pdf','tdt_ar', 'publier_date'],
                'conditions' => ['Deliberation.id' => $projetId],
                'recursive' => -1
            ]);

            foreach ($annexes as $annexe) {
                if ($annexe['Annexe']['joindre_ctrl_legalite']) {
                    $zip->addFromString(
                        __(
                            'Annexe_%s-%s.pdf',
                            $annexe['Annexe']['position'],
                            AppTools::getNameFile($annexe['Annexe']['filename'])
                        ),
                        $this->PDFStamp->stamp(
                            $annexe['Annexe']['filetype']==='application/pdf' ? $annexe['Annexe']['data']
                                : $annexe['Annexe']['data_pdf'],
                            $acte['Deliberation']
                        )
                    );
                }
            }
        }
        $zip->close();
        $fileOutput = new File($folderTmp->pwd() . DS . $fileName . '.zip');
        $content = $fileOutput->read();
        $fileOutput->close();

        return $content;
    }

    /**
     * @access public
     * @return CakeResponse
     * @version 5.1.0
     * @since 4.0.0
     */
    public function publish($cookieTokenKey = null)
    {
        $this->Deliberation->Behaviors->load('Version', $this->Deliberation->actsAsVersionOptionsList['Version']);

        if (!isset($this->data['traitement_lot']['action']) or empty($this->data['traitement_lot']['action'])) {
            $this->Flash->set(
                __('Veuillez sélectionner une action.'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );

            return $this->redirect($this->previous);
        } else {
            $action = $this->data['traitement_lot']['action'];
        }

        if (isset($this->data['Deliberation']['check'])) {
            foreach ($this->data['Deliberation']['check'] as $tmp_id => $bool) {
                if ($bool) {
                    $delib_id = substr($tmp_id, 3, strlen($tmp_id));
                    $deliberationIds[] = $delib_id;
                }
            }
        } else {
            foreach ($this->data['Deliberation'] as $projet_id => $projet) {
                if (!empty($projet['is_checked']) && $projet['is_checked']) {
                    $deliberationIds[] = $projet_id;
                }
            }
        }
        if (!isset($deliberationIds)
            || (isset($deliberationIds) && count($deliberationIds) == 0)
        ) {
            $this->Flash->set(
                __('Veuillez sélectionner un acte.'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            return $this->redirect($this->previous);
        }
        $success = true;
        foreach ($deliberationIds as $deliberation_id) {
            $this->Deliberation->id = $deliberation_id;
            if ($action == 'publier') {
                $this->Deliberation->saveField(
                    'publier_date',
                    !empty($this->request->data['publier']['date_acte_all']) ?
                        CakeTime::format(
                            str_replace('/', '-', $this->request->data['publier']['date_acte_all']),
                            '%Y-%m-%d 00:00:00'
                        ) : date('Y-m-d')
                );

                $this->Deliberation->recursive=-1;
                $projet = $this->Deliberation->findById($deliberation_id, ['num_delib']);

                $data = [];
                $data['numero_de_lacte'] = $projet['Deliberation']['num_delib'];
                $data['document_json'] = $this->createEnveloppeOpenDataJSON($deliberation_id);
                $data['document_csv'] = $this->convertSCDLdataJSONToCSV($data['document_json']);
                $data['document_acte'] = $this->createDocumentActePermalien($deliberation_id);
                $data['document_annexes'] = $this->addAnnexes($deliberation_id);

                try {
                    $this->Publication->send($data);
                } catch (Exception $e) {
                    $success = false;
                    $this->Flash->set(
                        __(
                            'l\'acte "%s" n\'a pas été plublié. (Erreur: %s)',
                            $projet['Deliberation']['num_delib'],
                            $e->getMessage()
                        ),
                        ['element' => 'growl', 'params' => ['type' => 'danger']]
                    );
                    continue;
                }

                if ($this->Deliberation->saveField('publier_etat', 1)) {
                    $this->Deliberation->setHistorique(
                        __('Acte envoyé pour publication.'),
                        $deliberation_id,
                        null,
                        $this->Session->read('Auth.User.id')
                    );
                }
                $this->Deliberation->clear();
            }

            if ($action == 'publier_manuellement') {
                $this->Deliberation->saveField(
                    'publier_date',
                    !empty($this->request->data['publier_manuellement']['date_acte_all']) ?
                        CakeTime::format(
                            str_replace('/', '-', $this->request->data['publier_manuellement']['date_acte_all']),
                            '%Y-%m-%d 00:00:00'
                        ) : date('Y-m-d')
                );

                $this->Deliberation->recursive=-1;
                if ($this->Deliberation->saveField('publier_etat', 2)) {
                    $this->Deliberation->setHistorique(
                        __('Acte déclaré publié manuellement.'),
                        $deliberation_id,
                        null,
                        $this->Session->read('Auth.User.id')
                    );
                }
                $this->Deliberation->clear();
            }
        }

        if ($success!==true) {
            $this->Flash->set(__('Action effectuée avec des erreurs'), [
                'element' => 'growl', 'params' => [
                    'type' => 'warning'
                ]]);
            return $this->redirect($this->previous);
        }

        $this->Flash->set(__('Action effectuée avec succès'), ['element' => 'growl']);

        return $this->redirect($this->previous);
    }

    private function addAnnexes($acteId)
    {
        $publierAnnexes = [];
        $acte = $this->Deliberation->find('first', [
            'fields' => ['id','delib_pdf','tdt_ar', 'publier_date'],
            'conditions' => ['Deliberation.id' => $acteId],
            'recursive' => -1
        ]);

        $annexes = $this->Deliberation->Annexe->getAnnexesFromDelibId($acteId);
        if (!empty($annexes)) {
            foreach ($annexes as $annexe) {
                if ($annexe['Annexe']['joindre_ctrl_legalite']) {
                    $publierAnnexes[] = [
                        'filename' => __(
                            'Annexe_%s-%s',
                            $annexe['Annexe']['position'],
                            AppTools::getNameFile($annexe['Annexe']['filename']).'.pdf'
                        ),
                        'content' => $this->PDFStamp->stamp(
                            $annexe['Annexe']['filetype'] === 'application/pdf' ? $annexe['Annexe']['data']
                            : $annexe['Annexe']['data_pdf'],
                            $acte['Deliberation']
                        )
                    ];
                }
            }
        }

        return $publierAnnexes;
    }
}
