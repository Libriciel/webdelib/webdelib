<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppController', 'Controller');

/**
 * Contrôleur des Tableaux de Bord
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       web-delib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 * @version     v4.3
 * @package     app.Controller
 *
 * @author splaza
 */
App::uses('AppTools', 'Lib');

class TableauDeBordController extends AppController
{
    public $uses = [
        'Deliberation',
        'ModelOdtValidator.Modeltemplate',
        'Commentaire', 'Seance',
        'DeliberationSeance',
        'DeliberationTypeseance',
        'Typeseance',
        'User',
        'Circuit',
        'Acteur',
        'Service',
        'Typeacte',
        'Cakeflow.Traitement'];

    public $components = [
        'Paginator',
        'ProjetTools',
        'Filtre',
        'Auth' => [
            'mapActions' => [
                'read' => ['index', 'downloadCsv', 'update', 'delete', 'create', 'projets', 'traitements']
            ],
        ]];

    /**
     * @version 4.3
     * @access private
     * @param type $projets
     */
    private function ajouterFiltre($projets = [])
    {
        if (!$this->Filtre->critereExists() && !empty($projets)) {
            $seances = $this->Seance->find('all', [
                'fields' => ['Seance.id', 'Seance.type_id', 'Seance.date'],
                'conditions' => ['Seance.traitee' => 0],
                'contain' => ['Typeseance.libelle', 'Typeseance.retard'],
                'order' => ['Typeseance.libelle' => 'ASC', 'Seance.date' => 'ASC'],
            ]);

            $options_seances = [];
            foreach ($seances as $seance) {
                //Voir tous les projets ou tous les futurs dates avec un delais respecté
                $options_seances[$seance['Seance']['id']] =
                    $seance['Typeseance']['libelle'] . ' : ' . CakeTime::i18nFormat(
                        $seance['Seance']['date'],
                        '%A %e %B %Y'
                    ) . ' à ' . CakeTime::i18nFormat($seance['Seance']['date'], '%k:%M');
            }

            $this->Filtre->addCritere('DeliberationSeanceId', ['field' => 'DeliberationSeance.seance_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type'=> 'select',
                    'label' => __('Séances'),
                    'data-placeholder' => __('Sélectionner une séance'),
                    'data-allow-clear' => true,
                    'options' => $options_seances]]);

            $typeseances = $this->Typeseance->find(
                'list',
                ['fields' => ['id', 'libelle'], 'recursive' => -1, 'order' => ['Typeseance.libelle' => 'ASC']]
            );
            $this->Filtre->addCritere('DeliberationTypeseanceId', [
                'field' => 'DeliberationTypeseance.typeseance_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type'=> 'select',
                    'label' => __('Type de séance'),
                    'data-placeholder' => __('Sélectionner un type de séance'),
                    'data-allow-clear' => true,
                    'options' => $typeseances]]);

            $typeactes = $this->Deliberation->Typeacte->find('list', [
                'recursive' => -1,
                'order' => ['Typeacte.name' => 'ASC'],
                'allow' => ['Typeacte.id' => 'read']]);

            $this->Filtre->addCritere('Typeacte', [
                'field' => 'Deliberation.typeacte_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type'=> 'select',
                    'label' => __('Type d\'acte'),
                    'data-placeholder' => __('Sélectionner des types d\'actes'),
                    'data-allow-clear' => true,
                    'options' => $typeactes
                ]]);

            $themes = $this->Deliberation->Theme->generateTreeListByOrder(
                ['Theme.actif' => '1'],
                '&nbsp;&nbsp;&nbsp;&nbsp;'
            );
            $this->Filtre->addCritere('ThemeId', [
                'field' => 'Deliberation.theme_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type'=> 'select',
                    'label' => __('Thème'),
                    'data-placeholder' => __('Sélectionner un thème'),
                    'data-allow-clear' => true,
                    'options' => $themes,
                    'escape' => false
                ]]);
            $services = $this->Service->find('list', [
                'fields' => ['id', 'name'],
                'order' => ['Service.name' => 'ASC'],
                'conditions' => ['actif' => true], 'recursive' => -1]);
            $this->Filtre->addCritere('ServiceId', [
                'field' => 'Deliberation.service_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type'=> 'select',
                    'label' => __('Service émetteur'),
                    'data-placeholder' => __('Sélectionner des services émetteurs'),
                    'multiple' => 'multiple',
                    'options' => $services,
                ]]);
            $circuits = $this->Circuit->find('list', [
                'fields' => ['id', 'nom'],
                'order' => ['Circuit.nom' => 'ASC'],
                'conditions' => ['actif' => true], 'recursive' => -1]);
            $this->Filtre->addCritere('CircuitId', [
                'field' => 'Deliberation.circuit_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type'=> 'select',
                    'label' => __('Circuit de validation'),
                    'data-placeholder' => __('Sélectionner un circuit de validation'),
                    'data-allow-clear' => true,
                    'options' => $circuits,
                ]]);

            return true;
        }

        return false;
    }

    /**
     * Permet de télécharger au format csv les listes affichées dans le tableau de bord
     *
     * @access public
     * @param type $delib_ids --> liste des id des projets à retourner au format csv
     * @return type
     *
     * @version 5.1.2
     * @since 4.3
     */
    public function downloadCsv($etat)
    {
        $query = $this->filtreQueryProjets($etat);
        if (empty($query)) {
            $query = $this->filtreQueryTraitements($etat);
        }
        if (!empty($query)) {
            $joins = [];
            $this->Traitement->Visa->bindModel([
                'belongsTo' => [
                    'User' => [
                        'className' => 'User',
                        'foreignKey' => 'trigger_id',
                        'dependent' => false,
                        'conditions' => '',
                        'order' => '',
                        'limit' => '',
                        'unique' => true,
                        'finderQuery' => '',
                        'deleteQuery' => ''
                    ]
                ]
            ]);
            $conditions = $this->handleConditions($this->Filtre->conditions());
            $projets = $this->Deliberation->find('all', [
                'fields' => ['DISTINCT Deliberation.id', 'Deliberation.objet'],
                'joins' => !empty($query['joins']) ? array_merge($query['joins'], $joins) : $joins,
                'conditions' => array_merge($query['conditions'], $conditions, [
                    'Deliberation.parent_id' => null
                ]),
                'contain' => [
                    'Traitement' => [
                        'fields'=> ['id','numero_traitement','treated'],
                        'Visa' => [
                            'fields'=> ['etape_nom','date','action','numero_traitement'],
                            'order' => ['numero_traitement' => 'ASC'],
                            'User'=> ['fields'=> ['nom','prenom']]
                        ]
                        ]
                ],
                'order' => ['Deliberation.id' => 'DESC'],
                'recursive' => -1
            ]);

            // \r \n </br> "\n"
            //$csv = 'Affaire;Etapes';
            $csv = '';
            foreach ($projets as $projet) {
                $csv .= (!empty($csv) ? CHR(13) : '')
                    . '(' . $projet['Deliberation']['id'] . ');'
                    . str_replace(CHR(13)
                    . CHR(10), "", $projet['Deliberation']['objet']) . ';';
                foreach ($projet['Traitement'] as $traitement) {
                    foreach ($traitement['Visa'] as $visa) {
                        //Prise en compte des visas passés
                        if ($visa['numero_traitement'] < $traitement['numero_traitement'] ||
                                (
                                    $visa['numero_traitement'] === $traitement['numero_traitement']
                                    && $traitement['treated']
                                )
                        ) {
                            if (!in_array($visa['action'], ['KO','OK','ST','IN'], true)) {
                                continue;
                            }

                            $csv .= 'Etape "'.$visa['etape_nom'] . '" '.($visa['action'] === 'IN' ?
                                    __('injecté') :
                                    __('traité')).' le ' . CakeTime::format($visa['date'], '%d-%m-%Y')
                                . _(' par ') . $visa['User']['prenom'] . ' ' . $visa['User']['nom'] . ';';
                        } else {
                            $csv .= 'Etape "'.$visa['etape_nom'] . '" non traité';
                            continue 2;
                        }
                    }
                }
            }
            $dateExport = time();
            $nameFile = __(
                '%s à %s',
                CakeTime::i18nFormat($dateExport, '%A %e %B %Y'),
                CakeTime::i18nFormat($dateExport, '%kh%M')
            );
            $this->response->disableCache();
            $this->response->body($csv);
            $this->response->type('application/csv');
            $this->response->download($nameFile . '.csv');

            return $this->response;
        }
    }


    /**
     * [filtreQueryProjets description]
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    private function filtreQueryProjets($query = null)
    {
        $queries = [
            'new' => [
                'conditions' => [
                    'etat_id' => 0] //FIX
            ],
            'valid' => [
                'conditions' => [
                    'etat_id' => 1]
            ],
            'delay' => [
                'conditions' => [
                    'etat_id' => 9]
            ]
        ];

        if (!empty($query)) {
            if (!empty($queries[$query])) {
                return $queries[$query];
            } else {
                return false;
            }
        }

        return $queries;
    }

    private function filtreQueryTraitements($query = null)
    {
        $queries = [
            'process_future' => [
                'joins' => [
                    $this->Deliberation->join('Traitement', ['type' => 'LEFT', 'conditions' => [
                            'Traitement.treated' => false
                    ]]),
                    $this->Traitement->join('Visa', ['type' => 'LEFT', 'conditions' => [
                            'Visa.traitement_id = Traitement.id',
                    ]])
                ],
                'conditions' => [
                    'OR' => [
                        ['Deliberation.etat' => 0],
                        ['Deliberation.etat' => 1],
                    ]
                ]
            ],
            'process' => [
                'joins' => [
                    $this->Deliberation->join('Traitement', ['type' => 'INNER', 'conditions' => [
                            'Traitement.treated' => false
                    ]]),
                    $this->Traitement->join('Visa', ['type' => 'INNER', 'conditions' => [
                            'Visa.numero_traitement = Traitement.numero_traitement',
                    ]])
                ],
                'conditions' => [
                    'etat_id' => 1,
                    'Traitement.treated' => false]
            ],
            'processed' => [
                'joins' => [
                    $this->Deliberation->join('Traitement', ['type' => 'INNER'])
                ],
                'conditions' => [
                    'etat_id' => 2,
                    'Traitement.treated' => true]
            ],
        ];

        if (!empty($query)) {
            if (!empty($queries[$query])) {
                return $queries[$query];
            } else {
                return false;
            }
        }

        return $queries;
    }

    /**
     * Affichage du tableau de bord en fonction des services validateurs et rédacteurs
     *
     * @version 4.4
     * @access public
     */
    public function index()
    {
        $colors = AppTools::colors();
        foreach ($colors as $key => $value) {
            $colors[] = [
                'color' => $value['hex'],
                'highlight' => AppTools::colourBrightness($value['hex'], 0.8)
            ];
        }

        $this->set('colors', $colors);

        $dashboards = [];
        $dashboards_queries = $this->filtreQueryProjets();
        foreach ($dashboards_queries as $key => $query) {
            $dashboards[$key]['data']['_dashletCount'] = $this->dashletCount($key, $query);
            $dashboards[$key]['data']['_dashletChartByTypeActe'] = $this->dashletChartByTypeActe($key, $query);
            $dashboards[$key]['data']['_dashletChartByService'] = $this->dashletChartByService($key, $query);
        }
        $dashboards_queries = $this->filtreQueryTraitements();
        foreach ($dashboards_queries as $key => $query) {
            $dashboards[$key]['data']['_dashletCount'] = $this->dashletCount($key, $query);
            $dashboards[$key]['data']['_dashletPolarAreaChartByService'] =
                $this->dashletPolarAreaChartByService($key, $query);
        }

        $this->set('dashboards', $dashboards);
    }

    private function dashletCount($variable, $query)
    {
        $joins = [];
        $projets = $this->Deliberation->find('count', [
            'fields' => ['COUNT(DISTINCT Deliberation.id) as count'],
            'joins' => !empty($query['joins']) ? array_merge($query['joins'], $joins) : $joins,
            'conditions' => array_merge($query['conditions'], [
                'Deliberation.parent_id' => null
            ]),
            'recursive' => -1,
            'allow' => ['typeacte_id'],
        ]);

        return !empty($projets) ? $projets : '0';
    }

    private function dashletChartByTypeActe($variable, $query)
    {
        $joins = [$this->Deliberation->join('Typeacte', ['type' => 'INNER'])];
        $typeActes = $this->Deliberation->find('all', [
                    'fields' => [
                        'Typeacte.id', '"Typeacte"."name" AS "Dashboard__name"',
                        'COUNT(DISTINCT Deliberation.id) AS "Dashboard__count"'
                    ],
                    'joins' => !empty($query['joins']) ? array_merge($query['joins'], $joins) : $joins,
                    'conditions' => array_merge($query['conditions'], [
                        'Deliberation.parent_id' => null
                    ]),
                    'recursive' => -1,
                    'group' => ['Typeacte.id', 'Typeacte.name'],
                    'order' => ['Typeacte.id' => 'ASC'],
                    'allow' => ['typeacte_id'],
        ]);

        return !empty($typeActes) ? $typeActes : null;
    }

    private function dashletChartByService($variable, $query)
    {
        $joins = [$this->Deliberation->join('Service', ['type' => 'INNER'])];
        $services = $this->Deliberation->find('all', [
                    'fields' => [
                        'Service.id', '"Service"."name" AS "Dashboard__name"',
                        'COUNT(DISTINCT Deliberation.id) AS "Dashboard__count"'
                    ],
                    'joins' => !empty($query['joins']) ? array_merge($query['joins'], $joins) : $joins,
                    'conditions' => array_merge($query['conditions'], [
                        'Deliberation.parent_id' => null
                    ]),
                    'recursive' => -1,
                    'group' => ['Service.id', 'Service.name'],
                    'order' => ['Service.id' => 'ASC'],
                    'allow' => ['typeacte_id'],
        ]);

        return !empty($services) ? $services : null;
    }

    private function dashletPolarAreaChartByService($variable, $query)
    {
        $joins = [$this->Deliberation->join('Service', ['type' => 'INNER'])];

        return $this->Deliberation->find('all', [
                    'fields' => [
                        'Service.id', '"Service"."name" AS "Dashboard__name"',
                        'COUNT(DISTINCT Deliberation.id) AS "Dashboard__count"'
                    ],
                    'joins' => !empty($query['joins']) ? array_merge($query['joins'], $joins) : $joins,
                    'conditions' => array_merge($query['conditions'], [
                        'Deliberation.parent_id' => null
                    ]),
                    'recursive' => -1,
                    'group' => ['Service.id', 'Service.name'],
                    'order' => ['Service.id' => 'ASC'],
                    'allow' => ['typeacte_id'],
        ]);
    }

    /**
     * Affichage du tableau de bord en fonction des services validateurs et rédacteurs
     *
     * @version 5.1.2
     * @since 4.4
     * @access public
     */
    public function projets($etat, $dashlet = null)
    {
        switch ($etat) {
            case 'new':
                $titre = $crumb_titre = __('Nouveaux projets');
                break;
            case 'valid':
                $titre = $crumb_titre = __('Projets en cours de validation');
                break;
            case 'delay':
                $titre = $crumb_titre = __('Projets en retard');
                break;
            default:
                //Gestion d'erreur à faire
                break;
        }
        $this->Filtre->initialisation(null, $this->data);
        $conditions = $this->handleConditions($this->Filtre->conditions());
        // lecture en base
        $query = $this->filtreQueryProjets($etat);
        $conditions['Deliberation.parent_id'] = null;

        $this->paginate = [
            'countField' => 'DISTINCT Deliberation.id',
            'fields' => ['DISTINCT Deliberation.id', 'Deliberation.objet', 'Deliberation.etat',
                'Deliberation.signee',
                'Deliberation.titre', 'Deliberation.date_limite', 'Deliberation.anterieure_id',
                'Deliberation.num_pref', 'Deliberation.redacteur_id','Deliberation.rapporteur_id',
                'Deliberation.circuit_id',
                'Deliberation.typeacte_id', 'Deliberation.theme_id', 'Deliberation.service_id'],
            'joins' => !empty($query['joins']) ? $query['joins'] : null,
            'conditions' => array_merge($query['conditions'], $conditions),
            'contain' => [
                'User',
                'Annexe' => ['fields' => ['id', 'filename'], 'order' => ['position']],
                'Service' => ['fields' => ['name']],
                'Theme' => ['fields' => ['libelle']],
                'Typeacte' => [
                    'fields' => ['name'],
                    'Nature' => ['fields' => ['code']]
                ],
                'Circuit' => ['fields' => ['nom']],
                'DeliberationTypeseance' => ['fields' => ['id'],
                    'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action'],
                    ]],
                'DeliberationSeance' => ['fields' => ['id'],
                    'Seance' => ['fields' => ['id', 'date', 'type_id'],
                        'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]]]],
            'order' => ['Deliberation.id' => 'DESC'],
            'allow' => ['typeacte_id'],
            'recursive' => -1,
            'limit' => 10,
        ];

        $projets = $this->Paginator->paginate('Deliberation');
        $this->ProjetTools->sortProjetSeanceDate($projets);
        $newFiltre = $this->ajouterFiltre($projets);
        // Ajout du filtre rédacteur
        if ($newFiltre && !empty($projets)) {
            $users = $this->User->find('all', [
                'fields' => ['id', 'nom', 'prenom'],
                'conditions' => ['active' => true],
                'order' => ['User.nom' => 'ASC'],
                'recursive' => -1]);
            $users = Hash::combine($users, '{n}.User.id', ['%s %s', '{n}.User.nom', '{n}.User.prenom']);
            $this->Filtre->addCritere('RedacteurId', [
                'field' => ['Deliberation.redacteur_id', 'DeliberationUser.user_id'],
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type'=> 'select',
                    'label' => __('Rédacteur du projet'),
                    'data-placeholder' => __('Sélectionner un rédacteur de projet'),
                    'data-allow-clear' => true,
                    'options' => $users
                ]]);
        }
        $this->set('dashlet', $dashlet);
        $this->set('etat', $etat);
        $this->set('crumbs', [
            [
                __('Tableau de bord'),
                ['controller' => 'tableau_de_bord', 'action' => 'index']
            ],
            __('Projets'),
            $crumb_titre
            ]);
        $this->afficheProjets('projets', $projets, $titre);
    }

    /**
     * Affichage du tableau de bord en fonction des services validateurs et rédacteurs
     *
     * @version 4.4
     * @access public
     */
    public function traitements($etat, $dashlet = null)
    {
        switch ($etat) {
            case 'process_future':
                $titre = $crumb_titre = __('Projets à venir');
                break;
            case 'process':
                $titre = $crumb_titre = __('Projets à traiter');
                break;
            case 'processed':
                $titre = $crumb_titre = __('Projets validés');
                break;
            default:
                //Gestion d'erreur à faire
                break;
        }

        $this->Filtre->initialisation(null, $this->data);
        $conditions = $this->handleConditions($this->Filtre->conditions());
        $joins = [
            $this->Deliberation->join('DeliberationUser', ['type' => 'LEFT']),
        ];
        // lecture en base
        $query = $this->filtreQueryTraitements($etat);
        $joins = array_merge($joins, $query['joins']);
        $conditions['Deliberation.parent_id'] = null;

        $this->paginate = [
            'countField' => 'DISTINCT Deliberation.id',
            'fields' => [
                'DISTINCT Deliberation.id', 'Deliberation.objet', 'Deliberation.etat', 'Deliberation.signee',
                'Deliberation.titre', 'Deliberation.date_limite', 'Deliberation.anterieure_id',
                'Deliberation.num_pref', 'Deliberation.redacteur_id','Deliberation.rapporteur_id',
                'Deliberation.circuit_id',
                'Deliberation.typeacte_id', 'Deliberation.theme_id', 'Deliberation.service_id'
              ],
            'joins' => array_merge($joins, $this->handleJoins()),
            'conditions' => array_merge($query['conditions'], $conditions),
            'contain' => [
                'User',
                'Annexe' => ['fields' => ['id', 'filename'], 'order' => ['position']],
                'Service' => ['fields' => ['name']],
                'Theme' => ['fields' => ['libelle']],
                'Typeacte' => [
                    'fields' => ['name'],
                    'Nature' => ['fields' => ['code']]
                ],
                'Circuit' => [
                    'fields' => ['nom'],
                    'Etape' => ['fields' => ['id', 'nom'],
                    ]],
                'Traitement' => [
                    'fields' => ['id'],
                    'Visa' => ['fields' => ['id', 'etape_nom'],
                    ]],
                ],
            'order' => ['Deliberation.id' => 'DESC'],
            'allow' => ['typeacte_id'],
            'recursive' => -1,
            'limit' => 10,
        ];

        $projets = $this->Paginator->paginate('Deliberation');

        foreach ($projets as $key => &$projet) {
            $projet['Deliberation']['traitement_encours'] =
                $this->Traitement->listeEtapes($projet['Deliberation']['id'], ['selection' => 'ENCOURS']);
            $projet['Deliberation']['traitement_suivant'] =
                $this->Traitement->listeEtapes($projet['Deliberation']['id'], ['selection' => 'APRES']);
        }
        $this->ProjetTools->sortProjetSeanceDate($projets);
        $newFiltre = $this->ajouterFiltre($projets);
        // Ajout du filtre rédacteur
        if ($newFiltre && !empty($projets)) {
            $users = $this->User->find('all', [
                'fields' => ['id', 'nom', 'prenom'],
                'conditions' => ['active' => true],
                'order' => ['User.nom' => 'ASC'],
                'recursive' => -1]);
            $users = Hash::combine($users, '{n}.User.id', ['%s %s', '{n}.User.nom', '{n}.User.prenom']);
            $this->Filtre->addCritere('RedacteurId', [
                'field' => ['Deliberation.redacteur_id', 'DeliberationUser.user_id'],
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Rédacteur du projet'),
                    'data-placeholder' => __('Sélectioner un rédacteur'),
                    'data-allow-clear' => true,
                    'options' => $users
                ]]);
        }
        $this->set('dashlet', $dashlet);
        $this->set('etat', $etat);
        $this->set('crumbs', [
            [
                __('Tableau de bord'),
                ['controller' => 'tableau_de_bord', 'action' => 'index']
            ],
            __('Projets'),
            $crumb_titre
            ]);
        $this->afficheProjets('traitements', $projets, $titre);
    }

    /**
     * @access private
     * @param type $conditions
     * @return type
     *
     * @version 5.1.2
     * @since 4.3
     */
    private function handleConditions($conditions)
    {
        $projet_type_ids = [];
        $projet_seance_ids = [];

        if (isset($conditions['AND']['DeliberationTypeseance.typeseance_id'])) {
            $type_id = $conditions['AND']['DeliberationTypeseance.typeseance_id'];
            $typeseances = $this->DeliberationTypeseance->find(
                'all',
                [
                    'conditions' => ['DeliberationTypeseance.typeseance_id' => $type_id],
                    'recursive' => -1
                ]
            );
            foreach ($typeseances as $typeseance) {
                $projet_type_ids[] = $typeseance['DeliberationTypeseance']['deliberation_id'];
            }
            unset($conditions['AND']['DeliberationTypeseance.typeseance_id']);
        }
        if (isset($conditions['AND']['DeliberationSeance.seance_id'])) {
            $projet_seance_ids = $this->Seance->getDeliberationsId($conditions['AND']['DeliberationSeance.seance_id']);
            unset($conditions['AND']['DeliberationSeance.seance_id']);
        }
        $result = null;
        if (!empty($projet_type_ids) && !empty($projet_seance_ids)) {
            $result = array_intersect($projet_type_ids, $projet_seance_ids);
        } elseif (empty($projet_type_ids) && empty($projet_seance_ids)) {
            return $conditions;
        } elseif (empty($projet_type_ids)) {
            $result = $projet_seance_ids;
        } elseif (empty($projet_seance_ids)) {
            $result = $projet_type_ids;
        }

        if (isset($conditions['AND']['Deliberation.id'])) {
            $conditions['AND']['Deliberation.id'] = array_intersect($conditions['AND']['Deliberation.id'], $result);
        } elseif (!empty($result)) {
            $conditions['AND']['Deliberation.id'] = $result;
        }

        return $conditions;
    }

    /**
     * @access private
     * @param type $conditions
     * @return type
     *
     * @version 5.1.2
     */
    private function handleJoins()
    {
        $joins = [
            ['table' => 'deliberations_seances',
                'alias' => 'DeliberationSeance_Filter',
                'type' => 'LEFT',
                'conditions' => [
                    'Deliberation.id = DeliberationSeance_Filter.deliberation_id',
                ]
            ],
            ['table' => 'seances',
                'alias' => 'Seance_Filter',
                'type' => 'LEFT',
                'conditions' => [
                    'DeliberationSeance_Filter.seance_id = Seance_Filter.id'
                ]
            ],
            ['table' => 'deliberations_typeseances',
                'alias' => 'DeliberationTypeseance_Filter',
                'type' => 'LEFT',
                'conditions' => [
                    'Deliberation.id = DeliberationTypeseance_Filter.deliberation_id'
                ]
            ],
        ];

        return $joins;
    }
}
