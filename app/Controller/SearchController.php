<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('CakeTime', 'Utility');
App::uses('AppTools', 'Lib');

class SearchController extends AppController
{
    public $helpers = ['TinyMCE', 'ProjetUtil','DelibTdt'];
    public $uses = ['Acteur',
        'Deliberation', 'Typeacte', 'User', 'Annexe', 'Typeseance', 'Seance',
        'TypeSeance', 'Commentaire', 'ModelOdtValidator.Modeltemplate', 'Theme',
        'Collectivite', 'Vote', 'Listepresence', 'Infosupdef', 'Infosup', 'Historique',
        'Cakeflow.Circuit', 'Cakeflow.Composition', 'Cakeflow.Etape', 'Cakeflow.Traitement',
        'Cakeflow.Visa', 'Nomenclature', 'DeliberationSeance', 'DeliberationTypeseance',
        'DeliberationUser', 'Service',
        'Nature','Typologiepiece'];
    public $components = [
        'RequestHandler',
        'ProjetTools',
        'ModelOdtValidator.Fido',
        'SabreDav',
        'Email',
        'Acl',
        'Iparapheur',
        'Filtre',
        'Progress',
        'Conversion',
        'S2low',
        'Paginator',
        'Auth' => [
            'mapActions' => [
                'tousLesProjetsRecherche',
                'allow' => ['index','quickSearch']
            ]
        ]
    ];

    /**
     * @throws Exception
     */
    public function quickSearch()
    {
        if ($this->request->is('post')) {
            $conditionsRecherche = $this->searchTextual();
            $this->search(null, $conditionsRecherche);
        }
        $this->view = 'liste';
    }

    public function index($cookieTokenKey = null)
    {
        if ($this->request->is('post')) {
            $this->search($cookieTokenKey);
        }

        // liste des rapporteurs
        $this->set('rapporteurs', $this->Acteur->generateListElus(null, true));

        // liste des rédacteurs
        $redacteurs = $this->generateListRedacteurs(null, 'tousLesProjetsRecherche');
        $redacteurs[$this->Auth->user('id')] = $this->Auth->user('nom') . ' ' . $this->Auth->user('prenom');
        $this->set('redacteurs', $redacteurs);
        $this->set('typeActes', $this->Deliberation->Typeacte->find('list', [
            'recursive' => -1,
            'allow' => ['Typeacte.id']
        ]));
        $this->set('date_seances', $this->Seance->generateAllList());


        //Pour les utilisateurs n'ayant pas le droit tousLesProjetsRecherche la liste des services est restreinte
        if (!$this->Acl->check(
            ['User' => ['id' => $this->Auth->user('id')]],
            'tousLesProjetsRecherche',
            'read'
        )
        ) {
            $aService_id = [];
            foreach ($this->Auth->user('Service') as $service_id => $service) {
                $aService_id = array_merge($aService_id, $this->User->Service->doListId($service_id));
            }
            $conditions_service['id'] = array_unique($aService_id);
        }
        $services= $this->Deliberation->Service->generateTreeList(
            ['actif'=>true],
            null,
            null,
            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
        );

        $this->set('services', $services);


        $themes= $this->Deliberation->Theme->generateTreeList(
            ['actif'=>true],
            null,
            null,
            '&nbsp;&nbsp;&nbsp;&nbsp;'
        );

        $this->set('themes', $themes);

        $circuits = $this->Circuit->find('list', ['order' => ['Circuit.nom asc'],
            'fields' => ['Circuit.id', 'Circuit.nom',],
            'conditions' => ['Circuit.actif'=>true],
            'recursive' => -1]);
        $this->set('circuits', $circuits);

        $this->set('etats', $this->ProjetTools->generateListEtat());
        $this->set('infosupdefs', $this->Infosupdef->find(
            'all',
            [
                'conditions' => ['recherche' => 1, 'actif' => true],
                'fields' => ['id', 'code', 'nom', 'commentaire', 'type'],
                'order' => ['ordre'],
                'recursive' => -1
            ]
        ));
        $this->set('infosuplistedefs', $this->Infosupdef->generateListes('Deliberation'));
        $this->set('models', $this->Modeltemplate->find('list', [
            'conditions' => ['modeltype_id' => [MODELE_TYPE_RECHERCHE]],
            'fields' => ['Modeltemplate.id', 'Modeltemplate.name']]));
        $this->set('listeBoolean', $this->Infosupdef->listSelectBoolean);
    }

    /**
     * Recherche textuelle
     * @return array|CakeResponse|null
     */
    private function searchTextual()
    {
        $conditionsRecherche = [];
        if (empty($this->request->data['User']['search'])
            || (!ctype_digit(trim($this->request->data['User']['search']))
                && strlen(trim($this->request->data['User']['search'])) < 4)
        ) {
            $this->Flash->set(__('Vous devez saisir plus de 3 caractères.'), [
                'element' => 'growl',
                'params' => ['type' => 'danger']]);
            return $this->redirect($this->previous);
        }

        $field = trim($this->request->data['User']['search']);


        if (ctype_digit($field)) {
            $conditionsRecherche['OR']['Deliberation.id'][] = $field;
        }
        $conditionsRecherche['OR']['Deliberation.objet ILIKE'] = "%$field%";
        $conditionsRecherche['OR']['Deliberation.titre ILIKE'] = "%$field%";
        $conditionsRecherche['OR']['Deliberation.num_delib ILIKE'] = "%$field%";

        return $conditionsRecherche;
    }

    /**
     * Recherche multi-critères
     *
     * @return CakeResponse
     *
     * @SuppressWarnings(PHPMD)
     *@since 4.0
     * @version 5.1.4
     */
    private function search($cookieTokenKey = null, $conditionsRecherche = null)
    {
        $this->response->disableCache();

        //Pour la gestion de la progress Bar
        if (!empty($this->data['Deliberation']['model'])) {
            $this->Progress->start($cookieTokenKey);
        }

        $joins = [];
        $joins_users = false;
        $projets = [];

        // Vérification des droits sur les fichiers
        $conditionsDroits = [];
        if (!$this->Acl->check(
            [
            'User' => ['id' => $this->Auth->user('id')]],
            'tousLesProjetsRecherche',
            'read'
        )
        ) {
            $listeCircuits = $this->Circuit->listeCircuitsParUtilisateur($this->Auth->user('id'));
            if (!empty($listeCircuits)) {
                $conditionsDroits['OR']['Deliberation.circuit_id'] = explode(',', $listeCircuits);
            }
            $conditionsDroits['OR']['Deliberation.redacteur_id'] = $this->Auth->user('id');
            $joins_users = true;
            $conditionsDroits['OR']['DeliberationUser.user_id'] = $this->Auth->user('id');

            //Récupère la liste des délib que l'utilisateur a visé (résolution bug changement circuit non visible)

            $listeProjetsParticipe = $this->Traitement->getListTargetByTrigger($this->Auth->user('id'));
            if (!empty($listeProjetsParticipe)) {
                $conditionsDroits['OR']['Deliberation.id'] = explode(',', $listeProjetsParticipe);
            }

            $aService_id = [];
            foreach ($this->Auth->user('Service') as $service_id => $service) {
                $aService_id = array_merge($aService_id, $this->User->Service->doListId($service_id));
            }
            $conditionsDroits['OR']['Deliberation.service_id'] = array_unique($aService_id);
        }

        // Recherche par identifiant
        if (isset($this->data['Deliberation']['id']) && !empty($this->data['Deliberation']['id'])) {
            $conditionsRecherche["Deliberation.id"] = $this->searchDeliberationId($this->data);
        }

        // Texte
        if (!empty($this->data['Deliberation']['texte'])) {
            $conditionsRecherche['AND'][] = ['OR' => [
                    'Deliberation.objet ILIKE' => '%' . $this->data['Deliberation']['texte'] . '%',
                    'Deliberation.titre ILIKE' => '%' . $this->data['Deliberation']['texte'] . '%'
            ]];
        }
        // Rapporteur
        if (!empty($this->data['Deliberation']['rapporteur_id'])) {
            $conditionsRecherche["Deliberation.rapporteur_id"] = $this->data['Deliberation']['rapporteur_id'];
        }
        // Rédacteur
        if (!empty($this->data['Deliberation']['redacteur_id'])) {
            $conditionsRecherche['AND'][] = ['OR' => [
                    'Deliberation.redacteur_id' => $this->data['Deliberation']['redacteur_id'],
                    'DeliberationUser.user_id' => $this->data['Deliberation']['redacteur_id']
            ]];
            $joins_users = true;
        }
        // Service
        if (!empty($this->data['Deliberation']['service_id'])) {
            $service_ids = [];
            foreach ($this->data['Deliberation']['service_id'] as $key => $value) {
                $service_ids = array_merge($service_ids, $this->User->Service->doListId($value));
            }
            $conditionsRecherche['Deliberation.service_id'] = array_unique($service_ids);
        }
        // Type d'acte
        if (!empty($this->data['Deliberation']['typeacte_id'])) {
            $conditionsRecherche['Deliberation.typeacte_id'] = $this->data['Deliberation']['typeacte_id'];
        }
        // N° de délibération
        if (!empty($this->data['Deliberation']['num_delib'])) {
            $num_delibs = '';
            foreach ($this->data['Deliberation']['num_delib'] as $key => $value) {
                $num_delibs .= (!empty($num_delibs) ? '|' : '') . $value;
            }//SIMILAR TO '%(Ed|In)%'
            $conditionsRecherche['Deliberation.num_delib SIMILAR TO'] = '%(' . $num_delibs . ')%';
        }
        // Thème
        if (!empty($this->data['Deliberation']['theme_id'])) {
            $conditionsRecherche["Deliberation.theme_id"] = $this->data['Deliberation']['theme_id'];
        }
        // Circuit
        if (!empty($this->data['Deliberation']['circuit_id'])) {
            $conditionsRecherche["Deliberation.circuit_id"] = $this->data['Deliberation']['circuit_id'];
        }
        // État
        //
        if (isset($this->data['Deliberation']['etat']) && $this->data['Deliberation']['etat'] != '') {
            $this->searchEtat($this->data, $conditionsRecherche);
        }
        // Date de début
        if (!empty($this->data['Deliberation']['dateDebut'])) {
            $conditionsRecherche['Deliberation.date_acte >= DATE'] = $this->data['Deliberation']['dateDebut'];
        }
        // Date de fin
        if (!empty($this->data['Deliberation']['dateFin'])) {
            $conditionsRecherche['Deliberation.date_acte <= DATE'] = $this->data['Deliberation']['dateFin'];
        }
        // Date de début AR TdT
        if (!empty($this->data['Deliberation']['dateDebutAr'])) {
            $conditionsRecherche['Deliberation.tdt_ar_date >= DATE'] = $this->data['Deliberation']['dateDebutAr'];
        }
        // Date de fin AR TdT
        if (!empty($this->data['Deliberation']['dateFinAr'])) {
            $conditionsRecherche['Deliberation.tdt_ar_date <= DATE'] = $this->data['Deliberation']['dateFinAr'];
        }

        // Multi-séances
        if (empty($conditionsRecherche["Deliberation.id"]) && !empty($this->data['Deliberation']['seance_id'])) {
            $multiseances = [];
            foreach ($this->data['Deliberation']['seance_id'] as $seance_id) {
                $projet_ids = $this->Seance->getDeliberationsId($seance_id);
                $multiseances = array_merge($projet_ids, $multiseances);
            }
            $conditionsRecherche['Deliberation.id'] = $multiseances;
        }


        // Infos supplémentaires
        if (array_key_exists('Infosup', $this->data)) {
            $rechercheInfoSup = $this->Deliberation->Infosup->selectInfosup($this->data['Infosup']);
            if (!empty($rechercheInfoSup)) {
                $conditionsRecherche[] = $rechercheInfoSup;
            }
        }

        // Vérification conditions
        if (empty($conditionsRecherche) && empty($this->data['Deliberation']['model'])) {
            $this->Flash->set(__('Vous devez saisir au moins un critère.'), [
              'element' => 'growl',
              'params' => ['type' => 'danger']
            ]);
            $this->redirect([
                'admin' => false,
                'prefix' => null,
                'controller' => 'search',
                'action' => 'index']);
        }
        if (empty($conditionsRecherche)) {
            throw new Exception(__("Impossible d'effectuer la recherche sans critères."));
        }

        /// Go Recherche
        if ($joins_users) {
            $joins[] = $this->Deliberation->join('DeliberationUser', ['type' => 'LEFT']);
        }
        $conditions = [
          'AND' => [$conditionsDroits, $conditionsRecherche]
        ];
        //SubQuery des droits
        if (!empty($conditionsDroits)) {
            $db = $this->Deliberation->getDataSource();
            $subQuery = [
                'fields' => [
                    'DISTINCT Deliberation.id'
                ],
                'contain' => false,
                'joins' => $joins,
                'conditions' => $conditionsDroits,
            ];

            $subQuery = $this->Deliberation->sql($subQuery);
            $subQuery = ' "' . $this->Deliberation->alias . '"."id" IN (' . $subQuery . ') ';
            $subQueryExpression = $db->expression($subQuery);
            $conditionsDroits = [$subQueryExpression];
        }

        try {
            $fields = $this->searchFields();
            //Préparation des jointures
            $joins = array_merge(
                $joins,
                [
                $this->Deliberation->join('Theme', ['type' => 'LEFT'])],
                [['table' => 'deliberations_seances',
                    'alias' => 'DeliberationSeance_Filter',
                    'type' => 'LEFT',
                    'conditions' => [
                        'Deliberation.id = DeliberationSeance_Filter.deliberation_id',
                    ]
                ], $this->searchJoinSeance($this->data)
                ]
            );

            $ordre = $this->searchOrdre($this->data);
        } catch (Exception $e) {
            $this->Progress->error($e);
            $this->Flash->set(__($e->getMessage()), ['element' => 'growl', 'params' => ['type' => 'danger']]);
            $this->redirect($this->previous);
        }

        // Si il n'y pas de date de séance sélectionnée
        // et si l'on a un tri par ordre du jour
        // Alors on fait un tri sur les séances délibérantes
        //Si pas de séance délibérante ? sur certains projets ? on bloque la recherche.
        // Si pas de séance ? sur certains projets ? on bloque la recherche.
        $projetsId = [];

        if (empty($this->data['Deliberation']['seance_id'])
            && (in_array('DeliberationSeance_Filter.position asc', $ordre, true)
            || in_array('DeliberationSeance_Filter.position desc', $ordre, true))) {
            try {
                $projetsId = $this->Deliberation->find('all', [
                    'fields' => ['Deliberation.id'],
                    'contain' => ['DeliberationTypeseance'=> ['Typeseance.action']],
                    'joins' => $joins,
                    'conditions' => $conditions,

                    'recursive' => -1,
                    'order' => $ordre,
                    'allow' => ['typeacte_id']
                ]);
                $typeSeances = Hash::extract($projetsId, '{n}.DeliberationTypeseance.{n}');
                $ProjectsWithoutSessions = count($projetsId) - count($typeSeances);

                if (count($projetsId) > count(Hash::extract($projetsId, '{n}.DeliberationTypeseance.{n}'))
                ) {
                    throw new Exception(
                        __('Impossible d\'effectuer le tri par ordre du jour car %s acte(s)'.
                        ' ne possède(ent) pas une séance délibérante.', $ProjectsWithoutSessions)
                    );
                }

                $conditions['DeliberationSeance_Filter.seance_id'] = $this->data['Deliberation']['seance_id'];
            } catch (Exception $e) {
                $this->Progress->error($e);
                $this->Flash->set(__($e->getMessage()), [
                  'element' => 'growl',
                  'params' => ['type' => 'danger']
                ]);
                $this->redirect($this->previous);
            }
        }

        // Si la requête n'a pas encore été effectué.
        if (empty($projetsId)) {
            // on récupère les ID des délibérations
            $projetsId = $this->Deliberation->find('all', [
                'fields' => ['Deliberation.id'],
                'joins' => $joins,
                'conditions' => $conditions,
                'recursive' => -1,
                'order' => $ordre,
                'allow' => ['typeacte_id']
            ]);
        }
        //Fixe le problème des ids double
        $projet_ids = array_unique(Hash::extract($projetsId, '{n}.Deliberation.id'), SORT_NUMERIC);
        $NumberOfProjectsWithMoreThanOneSession = 0;

        foreach ($projet_ids as $projet_id) {
            $conditions = ['Deliberation.id' => $projet_id];
            // requête complète
            $deliberation = $this->Deliberation->find('first', [
                'fields' => $fields,
                'joins' => $joins,
                'contain' => [
                    'User' => ['fields' => ['id']],
                    'Annexe' => ['fields' => ['id', 'filename'], 'order' => ['position']],
                    'Service' => ['fields' => ['id', 'name']],
                    'Typeacte' => [
                        'fields' => ['name'],
                        'Nature' => ['fields' => ['code']]
                    ],
                    'Circuit' => ['fields' => ['nom']],
                    'DeliberationTypeseance' => [
                        'fields' => ['id'],
                        'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]
                    ],
                    'DeliberationSeance' => [
                        'fields' => ['id'],
                        'Seance' => [
                            'fields' => ['id', 'date', 'type_id'],
                            'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]
                        ]
                    ],
                ],
                'conditions' => $conditions,
                'recursive' => -1,
                'group' => [
                      'Deliberation.id', 'Deliberation.num_delib', 'Theme.libelle', 'Theme.order',
                      'Service.id', 'Circuit.id', 'Typeacte.id', 'Typeacte.name', 'Typeacte.nature_id',
                      'Seance_Filter.date','DeliberationSeance_Filter.position', 'Deliberation.debat_name'
                    ],
                'allow' => ['typeacte_id']
            ]);
            if (count($deliberation['DeliberationSeance'])>1) {
                $NumberOfProjectsWithMoreThanOneSession++;
            }
            $projets[] = $deliberation;
        }

        if (in_array('Seance_Filter.date asc', $ordre, true)
            || in_array('Seance_Filter.date desc', $ordre, true)
            || in_array('DeliberationSeance_Filter.position asc', $ordre, true)
            || in_array('DeliberationSeance_Filter.position desc', $ordre, true)) {
            try {
                $nombreDeTypeSeance = $this->Seance->find('all', [
                    'fields' => ['Seance.type_id'],
                    'conditions' => [
                        'Seance.id' => $this->data['Deliberation']['seance_id'],
                    ],
                    'contain'=>'DeliberationSeance','Typeseance',
                    'recursive' => -1,
                ]);

                $nombreDeTypeSeance = Hash::extract($nombreDeTypeSeance, '{n}.Seance.type_id') ;

                if (count(array_unique($nombreDeTypeSeance, SORT_REGULAR))>1) {
                    throw new Exception(__(
                        'Impossible de faire un tri sur une recherche combinant deux types de séances.'
                    ));
                }
            } catch (Exception $e) {
                $this->Progress->error($e);
                $this->Flash->set(__($e->getMessage()), [
                  'element' => 'growl',
                  'params' => ['type' => 'danger']
                ]);
                $this->redirect($this->previous);
            }

            try {
                /* Le try catch est ici pour gèrer le nombre de seance pour un projet.
                $NumberOfProjectsWithMoreThanOneSession doit être = 0 */
                if ($NumberOfProjectsWithMoreThanOneSession > 0
                    && empty($this->data['Deliberation']['seance_id'])) {
                    throw new Exception(
                        __(
                            'Impossible de combiner le tri par "ordre du jour" et "date de séance"'.
                            ' car le ou les actes (%s) dans plus d\'une séance.',
                            $NumberOfProjectsWithMoreThanOneSession
                        )
                    );
                }
            } catch (Exception $e) {
                $this->Progress->error($e);
                $this->Flash->set(__($e->getMessage()), [
                  'element' => 'growl',
                  'params' => ['type' => 'danger']
                ]);
                $this->redirect($this->previous);
            }
        }

        $this->ProjetTools->sortProjetSeanceDate($projets);

        $this->searchProjetLocked($projets);

        $this->searchFusion($projets, $conditionsRecherche, $cookieTokenKey);

        // Recherche Vide
        if (empty($projets)) {
            $this->Flash->set(
                __('Il n\'y a aucun projet correspondant aux critères de recherche.'),
                ['element' => 'growl','params' => ['type' => 'danger']]
            );

            return $this->redirect(
                [
                    'admin' => false,
                    'prefix' => null,
                    'controller' => 'search',
                    'action' => 'index'
                ]
            );
        }

        $this->afficheProjets('liste', $projets, __('Résultat de la recherche'), [], ['search']);
    }

    /**
     * [searchJoinSeance description]
     * @return [type] [description]
     *
     * @version 5.2.0
     * @since 5.1.4
     */
    protected function searchJoinSeance($data)
    {
        $conditions = null;
        // Si il y a un recherche par seance
        if (!empty($data['Deliberation']['seance_id'])) {
            $conditions = 'DeliberationSeance_Filter.seance_id IN ('
                . implode($data['Deliberation']['seance_id'], ',')
                .')';
        }

        return $joinSeance = ['table' => 'seances',
            'alias' => 'Seance_Filter',
            'type' => 'LEFT',
            'conditions' => [
                'DeliberationSeance_Filter.seance_id = Seance_Filter.id',
                $conditions
            ]
        ];
    }

    /**
     * [searchOrdre description]
     * @param  [type] $data [description]
     * @return [type]       [description]
     *
     * @version 5.1.4
     */
    protected function searchOrdre($data)
    {
        // ORDRE DE TRI
        $ordre = !empty($data['Deliberation']['tri'])
            ? array_unique(explode(",", $data['Deliberation']['tri']))
            : ['Deliberation.created' => 'desc'];
        if (count($ordre)>3) {
            throw new Exception(
                __("Impossible d'effectuer la recherche sur plus de trois critères de tri.")
            );
        }
        if (!$this->Deliberation->logicalValuesForOrder($ordre)) {
            throw new Exception(
                __("Impossible d'effectuer la recherche car les critère de tri sont incompatibles.")
            );
        }

        return $ordre;
    }


    /**
     * [searchFields description]
     * @return [type] [description]
     *
     * @version 5.1.4
     */
    protected function searchFields()
    {
        return [
            'DISTINCT ON ("Deliberation"."id") "Deliberation"."id"',
            'Deliberation.parent_id',
            'Deliberation.objet',
            'Deliberation.objet_delib',
            'Deliberation.etat',
            'Deliberation.signee',
            'Deliberation.titre',
            'Deliberation.date_limite',
            'Deliberation.anterieure_id',
            'Deliberation.num_pref',
            'Deliberation.rapporteur_id',
            'Deliberation.redacteur_id',
            'Deliberation.circuit_id',
            'Deliberation.typeacte_id',
            'Deliberation.theme_id',
            'MAX(Seance_Filter.date)',
            'Deliberation.date_acte',
            'Deliberation.tdt_ar_date',
            'Deliberation.service_id',
            'Deliberation.num_delib',
            'DeliberationSeance_Filter.position',
            'Deliberation.parapheur_etat',
            'Theme.libelle',
            'Theme.order'
        ];
    }

    /**
     * [searchDeliberationId description]
     * @param  [type] $data [description]
     *
     * @version 5.1.4
     */
    protected function searchDeliberationId($data)
    {
        $id_values = [];
        foreach ($this->data['Deliberation']['id'] as $key => $value) {
            //$id_values = preg_split("/[\s,;]+/", $data['Deliberation']['id']);
            if (strpos($value, '-') !== false) {
                $ecart = explode('-', $value);
                if (count($ecart) == 2 && $ecart[1] > $ecart[0]) {
                    for ($index = (int) $ecart[0]; $index <= $ecart[1]; $index++) {
                        if (!in_array($index, $id_values, true)) {
                            // debug($index);
                            array_push($id_values, $index);
                        }
                    }
                } elseif (count($ecart) == 2) {
                    $this->Flash->set(__('Vous devez saisir une plage valide. exemple: "150-200"'), [
                      'element' => 'growl',
                      'params' => ['type' => 'danger']
                    ]);
                    $this->redirect(['action' => 'search']);
                }
            } else {
                if (!is_numeric($value)) {
                    $this->Flash->set(__('Vous devez saisir un identifiant valide'), [
                       'element' => 'growl',
                       'params'=>['type' => 'danger']
                    ]);
                    $this->redirect(['action' => 'search']);
                }
                if (!in_array((int) $value, $id_values, true)) {
                    array_push($id_values, (int) $value);
                }
            }
        }

        return $id_values;
    }

    /**
     * [searchEtat description]
     * @param  [type] $data                [description]
     * @param  [type] $conditionsRecherche [description]
     *
     * @version 5.1.4
     */
    protected function searchEtat($data, &$conditionsRecherche)
    {
        // État
        switch ($data['Deliberation']['etat']) {
            case -2:// actes refusé
                $conditionsRecherche["Deliberation.etat"] = 0;
                $conditionsRecherche['NOT']["Deliberation.anterieure_id"] = null;
                break;
            case 2:// actes validés
                $conditionsRecherche["Deliberation.etat"] = 2;
                $conditionsRecherche['NOT']["Deliberation.signee"] = true;
                break;
            case 6:// actes numérotés
                // 2 pour les actes passés dans un circuit après génération du numéro
                $conditionsRecherche["Deliberation.etat"] = [2, 3, 4];
                $conditionsRecherche['NOT']["Deliberation.num_delib"] = '';
                $conditionsRecherche['NOT']["Deliberation.signee"] = true;
                break;
            case 7:// actes signés
                $conditionsRecherche["Deliberation.etat"] = [3, 4];
                $conditionsRecherche["Deliberation.signee"] = true;
                break;
            case 8:// actes non télétransmis
                $conditionsRecherche["Deliberation.etat"] = [3, 4];
                $conditionsRecherche["Deliberation.signee"] = true;
                $joins[] = ['table' => 'typeactes',
                    'alias' => 'Typeacte_Filter',
                    'type' => 'INNER',
                    'conditions' => [
                        'Deliberation.typeacte_id = Typeacte_Filter.id',
                        'Typeacte_Filter.teletransmettre' => 'false'
                    ]
                ];
                break;
            default:
                $conditionsRecherche["Deliberation.etat"] = $this->data['Deliberation']['etat'];
                break;
        }
    }

    /**
     * [searchProjetLocked description]
     * @param  [type] $projet [description]
     */
    protected function searchProjetLocked(&$projets)
    {
        // récupération des droits pour bouton
        $droits = $this->ProjetTools->getDroits(['editerTous' => 'read']);

        foreach ($projets as $key => $projet) {
            //On cherche à savoir si la délibération est vérrouillée par un utilisateur
            $this->ProjetTools->locked($projet['Deliberation']);

            if ($projets[$key]['Deliberation']['etat'] === 0 && $projet['Deliberation']['anterieure_id'] !== 0) {
                $projet['Deliberation']['iconeEtat'] = $this->ProjetTools->iconeEtat(
                    $projet['Deliberation']['id'],
                    -2,
                    $projet['Typeacte']['Nature']['code']
                );
            } elseif ($projet['Deliberation']['etat'] === 1) {
                $estDansCircuit = $this->Traitement->triggerDansTraitementCible(
                    $this->Auth->user('id'),
                    $projet['Deliberation']['id']
                );
                $tourDansCircuit = 0;
                if ($estDansCircuit) {
                    $tourDansCircuit = $this->Traitement->positionTrigger(
                        $this->Auth->user('id'),
                        $projet['Deliberation']['id']
                    );
                }
                $estRedacteur = ($this->Auth->user('id') == $projet['Deliberation']['redacteur_id']);
                $projet['Deliberation']['iconeEtat'] = $this->ProjetTools->iconeEtat(
                    $projet['Deliberation']['id'],
                    $projet['Deliberation']['etat'],
                    $projet['Typeacte']['Nature']['code'],
                    false,
                    $estDansCircuit,
                    $estRedacteur,
                    $tourDansCircuit
                );
            } else {
                $projet['Deliberation']['iconeEtat'] = $this->ProjetTools->iconeEtat(
                    $projet['Deliberation']['id'],
                    $projet['Deliberation']['etat'],
                    $projet['Typeacte']['Nature']['code'],
                    $droits['editerTous/read']
                );
            }
        }
    }
    /**
     * Générer la liste des rédacteurs
     *
     * @access private
     * @param integer $redactor Rédacteur principal
     * @param string $order_by Pour ordonner la liste
     * @return array
     *
     * @version 5.1
     * @since 4.3
     */
    private function generateListRedacteurs($redactor = null, $aco = null)
    {
        if ($this->Acl->check(['User' => ['id' => $this->Auth->user('id')]], $aco, 'read')) {
            // on recherche ensuite les rédacteurs attachés à ce service

            $this->User->virtualFields['name'] = 'User.nom || \' \' || User.prenom';
            $users = $this->User->find('list', [
                'fields' => ['id', 'name','modified','active'],
                'order' => ['nom' => 'asc'],
                'conditions'=> ['active'=>true],
                'recursive' => -1,
            ]);
            unset($this->User->virtualFields['name']);
        } else {
            $joins_conditions = [];
            if (empty($redactor)) {
                // Si pas de rédacteur on chercher les services auxquels appartient l'utilisateur
                $joins_conditions['ServiceUser.service_id'] = array_keys($this->Auth->user('Service'));
            } else {
                // on cherche les services auxquels appartient le rédacteur principal
                $services = $this->Deliberation->Redacteur->find('first', [
                    'fields' => ['Redacteur.id'],
                    'conditions' => ['Redacteur.id' => $redactor],
                    'contain' => ['Service' => ['fields' => ['Service.id']]],
                    'recursive' => -1,
                ]);

                if (!empty($services) && !empty($services['Service'])) {
                    $joins_conditions['ServiceUser.service_id'] =
                        array_values(Hash::extract($services, 'Service.{n}.id'));
                }
            }
            $joins = [
                ['table' => 'services_users',
                    'alias' => 'ServiceUser',
                    'type' => 'RIGHT',
                    'conditions' => [
                        'User.id = ServiceUser.user_id',
                        $joins_conditions
                    ]
                ],
            ];

            // on recherche ensuite les rédacteurs attachés à ce service
            $users = $this->User->find('all', [
                'fields' => ['id', 'username', 'nom', 'prenom', 'ServiceUser.service_id'],
                'conditions' => ['User.active' => true],
                'joins' => $joins,
                'order' => ['nom' => 'asc'],
                'recursive' => -1,
            ]);

            if (empty($users)) {
                return [];
            }

            $users = Hash::combine($users, '{n}.User.id', ['%s %s', '{n}.User.nom', '{n}.User.prenom']);
        }

        //Retrait du rédacteur si rédacteur renseigné (add/edit projet)
        if (isset($redactor) && isset($users[$redactor])) {
            unset($users[$redactor]);
        }


        return $users;
    }

    /**
     * Fonction de fusion de plusieurs projets ou délibérations avec envoi du résultat vers le client
     *
     * @access public
     * @param $ids
     * @param integer $modelTemplateId
     * @param integer $cookieToken numéro de cookie du client pour masquer la fenêtre attendable
     * @return CakeResponse
     */
    private function genereFusionMultiRechercheToClient($ids, $modelTemplateId, $formatOutput, $cookieTokenKey)
    {
        try {
            $this->Progress->start($cookieTokenKey);
            $folderTmp = new Folder(AppTools::newTmpDir(
                TMP . 'files' . DS . 'search'
            ), true, 0777);

            $this->Progress->at(10, __('Récupération des données à générer'));
            // fusion du document
            $this->Deliberation->Behaviors->load('OdtFusion', ['modelTemplateId' => $modelTemplateId]);
            $cpt = 11;
            $nb_ids = (count($ids) + 20) + 10;
            //FIX progression
            $this->Progress->at(40, __('Fusion des documents'));
            foreach ($ids as $key => $value) {
                $this->Progress->at(
                    100 - round((($nb_ids - $cpt) / $nb_ids * 100)),
                    __('Génération du document id : %s ...', $value)
                );
                $this->Deliberation->odtFusion(['id' => $value]);
                $filename = $this->Deliberation->fusionName([
                    'fileNameSuffixe'=> $value,
                    'modelTemplateFileOutputFormatValues'=> [
                        'sequence' => $key + 1
                    ]
                ]);
                $file_generation = new File($folderTmp->pwd() . DS . $filename . '.pdf', true);
                $file_generation->write($this->Deliberation->getOdtFusionResult());
                $cpt++;
                $this->Deliberation->deleteOdtFusionResult();
                $this->Deliberation->clear();
                $file_generation->close();
            }

            $this->Progress->at(80, __('Préparation du fichier de sortie'));
            $files = $folderTmp->find('.*\.pdf');
            // selon le format d'envoi du document (pdf ou odt)
            if ($formatOutput == 'pdf') {
                $typemime = 'application/pdf';
                $filename = $filename . '.pdf';
                $file_output = new File($folderTmp->pwd() . DS . 'output.pdf');
                $concat_files = [];
                foreach ($files as $file) {
                    $file_index = new File($folderTmp->pwd() . DS . $file);
                    $concat_files[] = $file_index->name;
                    $file_index->close();
                }
                $file_output_flux = $this->Conversion->concatFilesPDF(
                    $folderTmp,
                    $concat_files,
                    $file_output->name
                );
                $content = $file_output->read();
                $file_output->close();
            } else {
                $typemime = 'application/zip';
                $filename = 'documents.zip';
                $zip = new ZipArchive();
                $zip->open($folderTmp->pwd() . DS . 'documents.zip', ZipArchive::CREATE);
                foreach ($files as $file) {
                    $file_index = new File($folderTmp->pwd() . DS . $file);
                    $zip->addFromString($file_index->name, $file_index->read());
                    $file_index->close();
                }
                $zip->close();
                $file_output = new File($folderTmp->pwd() . DS . 'documents.zip');
                $content = $file_output->read();
                $file_output->close();
            }
            $folderTmp->delete();
            $this->Progress->setFile($filename, $typemime, $content);

            $this->Progress->stop();
        } catch (Exception $e) {
            if (isset($folderTmp)) {
                $folderTmp->delete();
            }
            $this->Progress->error($e);
            CakeLog::error(
                __('[Fusion] %s ligne %s' . "\n" . '%s', $e->getFile(), $e->getLine(), $e->getMessage())
            );
        }

        $this->autoRender = false;
        $this->response->type(['json' => 'text/x-json']);
        $this->RequestHandler->respondAs('json');
        $this->response->body(json_encode($this->Session->read('Progress.Tokens')));
        return $this->response;
    }

    private function searchFusion(&$projets, $conditionsRecherche, $cookieTokenKey)
    {
        if (!empty($this->data['Deliberation']['model'])) {
            try {
                if (empty($conditionsRecherche)) {
                    throw new Exception(__('Vous devez saisir au moins un critère.'));
                }
                if (!empty($projets)) {
                    $deliberationIds = [];
                    foreach ($projets as &$projet) {
                        $deliberationIds[] = $projet['Deliberation']['id'];
                    }
                    $fusion_limit = Configure::read('search.FusionConv.limit');
                    if (count($deliberationIds) > $fusion_limit) {
                        throw new Exception(__(
                            'Le nombre de résultat (%s) de votre recherche dépasse'.
                            ' la limite de %s projets pour le génération.',
                            count($deliberationIds),
                            Configure::read('search.FusionConv.limit')
                        ));
                    }

                    if (!empty($this->data['Recherche']['is_genere_multi'])
                        && $this->data['Recherche']['is_genere_multi'] == true) {
                        return $this->genereFusionMultiRechercheToClient(
                            $deliberationIds,
                            $this->data['Deliberation']['model'],
                            $this->data['Recherche']['format_output'],
                            $cookieTokenKey
                        );
                    } else {
                        return $this->genereFusionRechercheToClient(
                            $deliberationIds,
                            $this->data['Deliberation']['model'],
                            $cookieTokenKey
                        );
                    }
                } else {
                    throw new Exception(__('Il n\'y a aucun projet correspondant aux critères de recherche.'));
                }
            } catch (Exception $e) {
                $this->Progress->error($e);
                CakeLog::error(
                    __(
                        '[Fusion] %s ligne %s' . "\n" . '%s',
                        $e->getFile(),
                        $e->getLine(),
                        $e->getMessage()
                    )
                );
            }

            $this->autoRender = false;
            $this->response->type(['json' => 'text/x-json']);
            $this->RequestHandler->respondAs('json');
            $this->response->body(json_encode($this->Session->read('Progress.Tokens')));
            return $this->response;
        }
    }

    /**
     * Fonction de fusion de plusieurs projets ou délibérations avec envoi du résultat vers le client
     *
     * @access public
     * @param $ids
     * @param integer $modelTemplateId
     * @param integer $cookieToken numéro de cookie du client pour masquer la fenêtre attendable
     * @return CakeResponse
     */
    private function genereFusionRechercheToClient($ids, $modelTemplateId, $cookieTokenKey)
    {
        try {
            $this->Progress->start($cookieTokenKey);

            $this->Progress->at(10, __('Récupération des données à générer'));
            // fusion du document
            $this->Deliberation->Behaviors->load('OdtFusion', ['modelTemplateId' => $modelTemplateId]);

            $this->Progress->at(40, __('Fusion du document'));
            $this->Deliberation->odtFusion(['modelOptions' => ['deliberationIds' => $ids]]);
            $filename = $this->Deliberation->fusionName();

            // selon le format d'envoi du document (pdf ou odt)
            if ($this->Auth->user('formatSortie') === 'pdf') {
                $this->Progress->at(70, __('Conversion du document'));
                $typemime = "application/pdf";
                $filename = $filename . '.pdf';
                $content = $this->Deliberation->getOdtFusionResult();
            } else {
                $this->Progress->at(70, __('Actualisation du sommaire'));
                $typemime = "application/vnd.oasis.opendocument.text";
                $filename = $filename . '.odt';
                $content = $this->Deliberation->getOdtFusionResult('odt');
            }
            $this->Deliberation->deleteOdtFusionResult();
            $this->Progress->at(90, __('Préparation du fichier de sortie'));
            $this->Progress->setFile($filename, $typemime, $content);
            $this->Progress->stop();
        } catch (Exception $e) {
            $this->Progress->error($e);
            CakeLog::error(
                __('[Fusion] %s ligne %s' . "\n" . '%s', $e->getFile(), $e->getLine(), $e->getMessage())
            );
        }

        $this->autoRender = false;
        $this->response->type(['json' => 'text/x-json']);
        $this->RequestHandler->respondAs('json');
        $this->response->body(json_encode($this->Session->read('Progress.Tokens')));

        return $this->response;
    }
}
