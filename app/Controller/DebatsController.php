<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppController', 'Controller');

/**
 * Class VotesController
 *
 * @package app.Controller
 */
class DebatsController extends AppController
{
    /**
     * [public description]
     * @var [type]
     */
    public $uses = [
        'Deliberation',
        'Seance',
        'Typeseance',
        'Acteur',
        'Listepresence',
        'Vote',
        'Historique',
    ];
    /**
     * [public description]
     * @var [type]
     */
    public $components = [
        'RequestHandler',
        'SeanceTools',
        'SabreDav',
        'Auth' => [
            'mapActions' => [
                'create' => ['add'],
                'read' => [ 'index', 'download'],
                'update' => ['edit'],
                'delete' => ['delete'],
            ],
        ]];

    /**
     * Saisir le débat global
     *
     * @access public
     *
     * @param type $delib_id
     * @param type $seance_id
     * @param type $addFile
     * @return type
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function edit($delib_id = null, $seance_id = null, $addFile = null)
    {
        //FIX Sens des variables et changement $seance_id => $id
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($seance_id, 'update');
        $this->set('seance_id', $seance_id);
        $this->set('delib_id', $delib_id);


        $seance = $this->Seance->find('first', [
            'fields' => ['traitee', 'pv_figes', 'date'],
            'contain' => ['Typeseance' => ['fields' => ['Typeseance.libelle', 'Typeseance.action']]],
            'conditions' => ['Seance.id' => $seance_id],
            'recursive' => -1
        ]);

        $this->set('seance', $seance);

        if ($seance['Seance']['pv_figes'] == 1) {
            $this->Flash->set(
                __('Les PV ont été figés, vous ne pouvez plus saisir de débat pour cette délibération...'),
                ['element' => 'growl','params'=>['type' => 'danger']]
            );
            return $this->redirect(['controller' => 'postseances', 'action' => 'index']);
        }

        if ($this->request->isGet() && !empty($addFile) && $addFile == true) {
            $file = new File(APP . 'Config' . DS . 'OdtVide.odt', false);
            $this->request->data['Deliberation']['id'] = $delib_id;
            $this->request->data['Deliberation']['texte_doc']['tmp_name'] = $file->pwd();
            $this->request->data['Deliberation']['texte_doc']['name'] = 'debat.odt';
            $this->request->data['Deliberation']['texte_doc']['size'] = $file->size();
            $this->request->data['Deliberation']['texte_doc']['type'] = $file->mime();
            $this->request->data['Deliberation']['texte_doc']['error'] = false;

            if ($this->Deliberation->saveDebat($this->data['Deliberation'])) {
                $this->Flash->set(__('Débat ajouté'), ['element' => 'growl']);
                return $this->redirect(['controller' => 'debats', 'action' => 'edit', $delib_id, $seance_id]);
            }
        }

        if ($this->request->isPost()) {
            $this->request->data['Deliberation']['id'] = $delib_id;

            if (!empty($this->data['Deliberation']['file_debat'])) {
                $file = $this->SabreDav->getFileDav($this->data['Deliberation']['file_debat']);
                $this->request->data['Deliberation']['debat'] = $file->read();
                $this->request->data['Deliberation']['debat_size'] = $file->size();
            } else {
                $this->request->data['Deliberation']['debat'] = '';
                $this->request->data['Deliberation']['debat_size'] = 0;
            }

            if ($this->Deliberation->saveDebat($this->data['Deliberation'])) {
                $this->Flash->set(__('Débat enregistré'), ['element' => 'growl']);
                return $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Veuillez corriger les erreurs ci-dessous'),
                    ['element' => 'growl','params'=>['type' => 'danger']]
                );
            }
        }

        $deliberation = $this->Deliberation->find('first', [
            'conditions' => ['Deliberation.id' => $delib_id],
            'recursive' => -1]);

        $this->request->data = $deliberation;
        if (!empty($deliberation['Deliberation']['debat'])) {
            $this->SabreDav->start();
            $this->set('file_debat', $this->SabreDav->newFileDav(
                'Debat_' . $delib_id . '.odt',
                $deliberation['Deliberation']['debat'],
            ));
            $this->set('file_debat_url', $this->SabreDav->newFileDavUrl());
        }
    }

    /**
     * Suppression du débat du projet
     *
     * @access public
     *
     * @param type $delib_id
     * @param type $seance_id
     * @return type
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function delete($delib_id, $seance_id)
    {
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($seance_id, 'update');

        $this->Deliberation->id = $delib_id;
        $data = [
            'debat' => '',
            'debat_name' => '',
            'debat_size' => 0,
            'debat_type' => ''
        ];

        if ($this->Deliberation->save($data, false)) {
            $this->Flash->set(__('Débat supprimé !'), ['element' => 'growl']);
            $this->redirect($this->previous);
        } else {
            $this->Flash->set(
                __("Problème survenu lors de la suppression du débat"),
                ['element' => 'growl','params'=>['type' => 'danger']]
            );
            $this->redirect($this->here);
        }
    }

    /**
     * @param type $delib_id
     * @return type
     */
    public function download($delib_id, $isRevision = false)
    {
        if (!$isRevision) {
            $this->Deliberation->id = $delib_id;
            $debat = $this->Deliberation->field('debat');
            $num_delib = $this->Deliberation->field('num_delib');
        } else {
            $this->Deliberation->Behaviors->load(
                'Version',
                $this->Deliberation->actsAsVersionOptionsList['Version']
            );

            $delib = $this->Deliberation->version($isRevision);
            $debat = $delib['deliberations_versions']['debat'];
            $num_delib = $delib['deliberations_versions']['data_version']['Deliberation']['num_delib'];
        }

        if (!empty($num_delib)) {
            $filename = $num_delib ;
        } else {
            $filename = 'debat_projet_' . $delib_id;
        }

        if ($debat==null) {
            $this->Flash->set(
                __("Le débat n'existe pas pour le projet '%s'.", $delib_id),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            return $this->redirect($this->previous);
        }
        // envoi au client
        $this->response->disableCache();
        $this->response->body($debat);
        $this->response->type('application/vnd.oasis.opendocument.text');
        $this->response->download($filename . '.odt');
        return $this->response;
    }
}
