<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('CakeTime', 'Utility');
App::uses('AppTools', 'Lib');
App::uses('ValidationTrait', 'Lib/Trait');

class ValidationsController extends AppController
{
    use ValidationTrait;

    public $helpers = ['TinyMCE', 'ProjetUtil','DelibTdt'];
    public $uses = ['Acteur',
        'Deliberation', 'Typeacte', 'User', 'Annexe', 'Typeseance', 'Seance',
        'TypeSeance', 'Commentaire', 'ModelOdtValidator.Modeltemplate', 'Theme',
        'Collectivite', 'Vote', 'Listepresence', 'Infosupdef', 'Infosup', 'Historique',
        'Cakeflow.Circuit', 'Cakeflow.Composition', 'Cakeflow.Etape', 'Cakeflow.Traitement',
        'Cakeflow.Visa', 'Nomenclature', 'DeliberationSeance', 'DeliberationTypeseance',
        'DeliberationUser', 'Service', 'Cron',
        'Nature','Typologiepiece',
        'ProjetEtat'];
    public $components = [
        'RequestHandler',
        'ProjetTools',
        'ModelOdtValidator.Fido',
        'SabreDav',
        'Wopi',
        'Email',
        'Acl',
        'Iparapheur',
        'Filtre',
        'Progress',
        'Conversion',
        'PDFStamp',
        'S2low',
        'Paginator',
        'Auth' => [
            'mapActions' => [
                'reattribuerTous',
                'attribuerCircuit',
                'validerEnUrgence'=> ['validerUrgence'],
                'allow' => ['view','traiter','valider','refuser']
            ]
        ]
    ];

    /**
     * [addIntoCircuit Ajoute un projet dans un circuit]
     * @param [type] $id [id du projet]
     * @version 5.2
     * @since 4.3
     */
    private function addIntoCircuit($id = null)
    {
        $this->Deliberation->Behaviors->load(
            'Version',
            $this->Deliberation->actsAsVersionOptionsList['Version']
        );

        try {
            $this->Deliberation->id = $id;
            $rapporteur_id = $this->Deliberation->field('rapporteur_id');
            $redacteur_id = $this->Deliberation->field('redacteur_id');
            $utilisateur_id = $this->Auth->user('id');

            if ($redacteur_id === $utilisateur_id) {
                $message = __(
                    'Projet injecté au circuit : %s',
                    $this->Circuit->getLibelle($this->data['Deliberation']['circuit_id'])
                );
            } else {
                $user = $this->User->find('first', [
                    'conditions' => ['User.id' => $redacteur_id],
                    'recursive' => -1,
                    'fields' => ['User.nom', 'User.prenom']]);
                $message = __(
                    'Projet injecté au circuit : %s pour %s %s',
                    $this->Circuit->getLibelle($this->data['Deliberation']['circuit_id']),
                    $user['User']['prenom'],
                    $user['User']['nom']
                );
            }

            $data = [
                'date_envoi' => date('Y-m-d H:i:s'),
                'circuit_id'=> $this->request->data['Deliberation']['circuit_id'],
                'etat' => '1',
            ];

            if (!$this->Deliberation->save($data)) {
                throw new Exception(__('Problème de sauvegarde.'), 'error');
            }

            $this->Historique->enregistre($id, $this->Auth->user('id'), $message);
            // on récupère les utilisateurs associés multi-déliberation (table de liaison)
            $redactors = [$redacteur_id];
            $otherRedactors = $this->Deliberation->find('first', [
                'fields' => ['id'],
                'contain' => ['User' => ['fields' => ['id']]],
                'conditions' => ['id' => $id],
                'recursive' => -1,
              ]);
            //on récupère tout les utilisateurs secondaires
            foreach ($otherRedactors['User'] as $otherRedactor) {
                $redactors[] = $otherRedactor['id'];
            }

            // insertion dans le circuit de traitement
            //New add circuit
            if ($this->Traitement->targetExists($id)) {
                $this->Circuit->ajouteCircuit($this->data['Deliberation']['circuit_id'], $id, $this->Auth->user('id'));
                $this->Traitement->Visa->replaceDynamicTrigger($id, $redactors);
                $members = $this->Traitement->whoIs($id, 'current', 'RI');
                if (empty($members)) {
                    $this->Historique->enregistre($id, $this->Auth->user('id'), 'Projet validé');
                    $this->Deliberation->saveField('etat', 2);
                    $this->notifyValidate($id);
                } else {
                    while (in_array($this->Auth->user('id'), $members, true)) {
                        $traitementTermine = $this->Traitement->execute('OK', $this->Auth->user('id'), $id);
                        $this->Historique->enregistre($id, $this->Auth->user('id'), __('Projet visé (auto)'));
                        if ($traitementTermine) {
                            $this->Historique->enregistre($id, $this->Auth->user('id'), __('Projet validé'));
                            $this->Deliberation->saveField('etat', 2);
                            $this->Flash->set(__('Projet inséré dans le circuit et validé'), ['element' => 'growl']);
                            $this->redirect(['controller' => 'projets', 'action' => 'mesProjetsValides']);
                        }
                        $members = $this->Traitement->whoIs($id, 'current', 'RI');
                    }

                    if (!empty($rapporteur_id)) {
                        $this->Acteur->notifier($id, $rapporteur_id, 'insertion');
                    }

                    foreach ($members as $destinataire_id) {
                        if ($destinataire_id > 0) {
                            $this->User->notifier($id, $destinataire_id, 'traitement');
                        }
                    }
                    $members = $this->Traitement->whoIs($id, 'after', 'RI');
                    foreach ($members as $user_id) {
                        if ($user_id > 0) {
                            $this->User->notifier($id, $user_id, 'insertion');
                        }
                    }

                    $this->Flash->set(__('Projet inséré dans le circuit'), ['element' => 'growl']);
                    $this->redirect(['controller' => 'projets','action' => 'mesProjetsRedaction']);
                }
            } else {
                // création des étapes du circuit dans le visas
                $this->Circuit->insertDansCircuit(
                    $this->data['Deliberation']['circuit_id'],
                    $id,
                    $this->Auth->user('id')
                );
                $this->Traitement->Visa->replaceDynamicTrigger($id, $redactors);
                //tableau des rédacteurs
                $options = [
                    'insertion' => [
                        '0' => [
                            'Etape' => [
                                'etape_id' => null,
                                'etape_nom' => 'Rédacteur',
                                'etape_type' => Etape::getTypeValue('simple'),
                                'cpt_retard' => null
                            ],
                            'Visa' => [
                                '0' => [
                                    'trigger_id' => $redacteur_id,
                                    'type_validation' => 'V'
                                ]
                            ],
                        ],
                    ],
                    'optimisation' => configure::read('Cakeflow.optimisation')
                ];
                $traitementTermine = $this->Traitement->execute('IN', $this->Auth->user('id'), $id, $options);
                if ($traitementTermine) {
                    $this->Historique->enregistre($id, $this->Auth->user('id'), __('Projet visé (auto)'));
                    $this->Historique->enregistre($id, $this->Auth->user('id'), 'Projet validé');
                    $this->Deliberation->id = $id;
                    $this->Deliberation->saveField('etat', 2);
                    $this->notifyValidate($id);
                    $this->Flash->set(__('Projet inséré dans le circuit et validé'), ['element' => 'growl']);
                    $this->redirect(['controller' => 'projets', 'action' => 'mesProjetsValides']);
                }
                $this->Traitement->Visa->replaceDynamicTrigger($id, $redactors);

                // Notification des rapporteurs
                if (!empty($rapporteur_id)) {
                    $this->Acteur->notifier($id, $rapporteur_id, 'insertion');
                }
                // Notification des users en attente de traitement
                $members = $this->Traitement->whoIs($id, 'current', 'RI');
                foreach ($members as $current_id) {
                    if ($current_id > 0) {
                        $this->User->notifier($id, $current_id, 'traitement');
                    }
                }
                // Notification des users après le traitement courant
                $members = $this->Traitement->whoIs($id, 'after', 'RI');
                foreach ($members as $user_id) {
                    if ($user_id > 0) {
                        $this->User->notifier($id, $user_id, 'insertion');
                    }
                }
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     * @return type
     */
    public function attribuerCircuit($id = null)
    {
        if (!$id) {
            $this->Flash->set(
                __('ID invalide pour la déliberation'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            return $this->redirect($this->previous);
        }

        $parent_id = $this->Deliberation->getMultidelibParent($id);
        //Si traitement d'une delib "enfant" rediriger vers traitement delib "parent"
        if (!empty($parent_id)) {
            return $this->redirect(['controller' => 'validations', 'action' => 'attribuerCircuit', $parent_id]);
        }

        if ($this->request->is('post')) {
            $this->Deliberation->id = $id;
            if (!empty($this->request->data['Deliberation']['circuit_id'])) {
                $this->addIntoCircuit($id);
                $message = __(
                    "Projet inséré dans le circuit : %s",
                    $this->Circuit->getLibelle($this->data['Deliberation']['circuit_id'])
                );
                $this->Flash->set($message, ['element' => 'growl']);

                return $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Veuillez sélectionner un circuit'),
                    ['element' => 'growl', 'params' => ['type' => 'danger']]
                );
            }
        }


        $projet = $this->Deliberation->find(
            'first',
            [
            'fields' => ['modified', 'created', 'texte_projet', 'objet', 'num_pref'],
            'contain' => [
                'Typeacte.name',
                'Theme.libelle',
                'Service' => [
                    'fields' => ['name', 'circuit_defaut_id'],
                    'conditions' => ['actif' => true]],
                'Seance.date',
                'Seance.Typeseance.libelle',
                'Redacteur.nom',
                'Redacteur.prenom',
                'Rapporteur.nom',
                'Rapporteur.prenom',
            ],
            'conditions' => ['Deliberation.id' => $id],
            'recursive' => -1
                ]
        );

        $messageAbsence = $this->Deliberation->checkPresenceTextes($id);

        if (!empty($messageAbsence)) {
            $this->Flash->set(__($messageAbsence), ['element' => 'growl', 'params' => ['type' => 'important']]);
        }

        // circuits
        $circuits = $this->User->getCircuits($this->Auth->user('id'));
        foreach ($this->Auth->user('Service') as $key => $service) {
            $service_default = $this->Service->findById($key, ['circuit_defaut_id']);
            if (!empty($service_default['Service']['circuit_defaut_id'])) {
                $circuit = $this->Circuit->find('first', [
                    'field' => ['id', 'nom'],
                    'conditions' => [
                        'id' => $service_default['Service']['circuit_defaut_id'],
                        'actif' => true
                    ],
                    'recursive' => -1,
                ]);
                if (!empty($circuit)) {
                    $circuits[$service_default['Service']['circuit_defaut_id']] = $circuit['Circuit']['nom'];
                }
            }
        }
        natcasesort($circuits);
        $this->set('circuits', $circuits);

        // circuit par défaut de l'utilisateur connecté
        if ($this->request->is('get')) {
            $userCircuitDefaultId = $this->User->circuitDefaut($this->Auth->user('id'), 'id', true);
            if (empty($userCircuitDefaultId) && !empty($projet['Service']['circuit_defaut_id'])) {
                $userCircuitDefaultId = $projet['Service']['circuit_defaut_id'];
            }
        }


        // affichage du circuit existant
        if ($this->request->is('post') && !empty($this->data['Deliberation']['circuit_id'])) {
            $userCircuitDefaultId = $this->data['Deliberation']['circuit_id'];
        }

        if (!empty($userCircuitDefaultId)) {
            $this->set('userCircuitDefaultId', $userCircuitDefaultId);
            $this->set(
                'attribuer_circuit_visu',
                $this->requestAction(
                    [
                        'plugin' => 'cakeflow',
                        'controller' => 'circuits',
                        'action' => 'visuCircuit', $userCircuitDefaultId
                    ],
                    ['return']
                )
            );
        }

        $this->view($id);
    }

    /**
     * @version 4.3
     * @access public
     * @param type $delib_id
     * @return type
     */
    public function retour($delib_id)
    {
        if ($this->TokensMethods->locked(
            ['controller' => 'projets', 'action' => 'edit' , 'entity_id' => $delib_id]
        )
        ) {
            $this->Flash->set(
                _('Vous ne pouvez pas traiter ce projet, car il est actuellement'
                . 'edité par un autre utilisateur.'
                . "\n".'Pour plus d\'informations, veuillez contacter votre administrateur'),
                ['element' => 'growl', 'params' => ['type' => 'warning']]
            );

            return $this->redirect($this->previous);
        }

        $this->Deliberation->Behaviors->load(
            'Version',
            $this->Deliberation->actsAsVersionOptionsList['Version']
        );

        $delib = $this->Deliberation->find('first', [
            'recursive' => -1,
            'conditions' => ['Deliberation.id' => $delib_id]
        ]);

        if (empty($delib)) {
            $this->redirect($this->previous);
        }
        if (empty($this->data)) {
            $etapes = $this->Traitement->listeEtapesPrecedentes($delib['Deliberation']['id']);
            if (empty($etapes)) {
                $this->Flash->set(
                    __('Opération impossible, l\'étape courante est la première du circuit.'),
                    ['element' => 'growl', 'params' => ['type' => 'danger']]
                );
                return $this->redirect($this->previous);
            }
            $this->set('delib_id', $delib_id);
            $this->set('etapes', $etapes);
        } else {
            if (!empty($this->request->data['Traitement']['Commentaire'])) {
                $this->Commentaire->enregistre(
                    $delib_id,
                    $this->Auth->user('id'),
                    $this->request->data['Traitement']['Commentaire']
                );
            }
            $this->Traitement->execute(
                'JP',
                $this->Auth->user('id'),
                $delib_id,
                ['etape_id' => $this->data['Traitement']['etape']]
            );
            $destinataires = $this->Traitement->whoIs($delib_id, 'current', 'RI');
            foreach ($destinataires as $destinataire_id) {
                if ($destinataire_id > 0) {
                    $this->User->notifier($delib_id, $destinataire_id, 'traitement');
                }
            }
            $this->Historique->enregistre($delib_id, $this->Auth->user('id'), __("Projet retourné"));
            $this->Flash->set(__('Opération effectuée : projet retourné'), ['element' => 'growl']);

            $this->redirect($this->History->goBackBefore('/validations/traiter'));
        }
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     * @param type $valid
     * @return type
     */
    public function traiter($id = null, $valid = null)
    {
        $this->Deliberation->id = $id;
        if (!$this->Deliberation->exists()) {
            $this->Flash->set(
                __("Le projet n° %s est introuvable !", $id),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            return $this->redirect($this->previous);
        }

        $parent_id = $this->Deliberation->getMultidelibParent($id);
        //Si traitement d'une delib "enfant" rediriger vers traitement delib "parent"
        if (!empty($parent_id)) {
            return $this->redirect(['action' => 'traiter', $parent_id]);
        }

        if (!$this->request->is(['post', 'put']) &&  $this->TokensMethods->get(['entity_id' => $id]) === false) {
            $this->Flash->set(
                __('Vous ne pouvez pas valider ce projet, car il est actuellement'
                    . 'édité par un autre utilisateur.'
                    . "\n"
                    . 'Pour plus d\'informations, veuillez contacter votre administrateur'),
                ['element' => 'growl','params' => ['type' => 'warning']
                ]
            );

            $this->redirect($this->previous);
        }

        //FIX factoriser la vérification
        $signee = $this->Deliberation->field('signee');
        if (!empty($signee) && $signee) {
            $this->Flash->set(
                __("Le projet n° %s est signé ! Validation impossible", $id),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            return $this->redirect($this->previous);
        }

        $etat = $this->Deliberation->field('etat');

        if (isset($etat) && $etat !== 1) {
            $this->Flash->set(
                __(
                    'Le projet n° %s n\'est pas dans un circuit de validation ! Validation impossible',
                    $id
                ),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            return $this->redirect($this->previous);
        }
        $users = $this->Deliberation->Traitement->whoIs($id, 'current', 'RI');
        if (!in_array($this->Auth->user('id'), $users, true)) {
            $this->Flash->set(
                __(
                    "Vous ne faites pas partie des valideurs sur l'étape en cours ! Validation impossible",
                    $id
                ),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            return $this->redirect($this->previous);
        }

        //si bloqué à une étape de délégation
        $visa = false;
        $traitement = $this->Traitement->findByTargetId($id);
        if ($traitement != null) {
            //Si il n'y a pas eu de jump
            $jump = [
                'Visa.traitement_id' => $traitement['Traitement']['id'],
                'Visa.action' => "JS"
            ];
            //si reste des étapes de délégation en attente (passées)
            $delegation_restante = [
                'Visa.traitement_id' => $traitement['Traitement']['id'],
                'Visa.trigger_id' => -1,
                'Visa.action' => "RI"];
            if (!$traitement['Traitement']['treated']) {
                $conditions = ['traitement_id' => $traitement['Traitement']['id'],
                    'numero_traitement <=' => $traitement['Traitement']['numero_traitement'],
                    'trigger_id' => -1,
                    'action' => 'RI'];
                $visa = $this->Visa->hasAny($conditions);

                $delegation_restante['Visa.numero_traitement <'] = $traitement['Traitement']['numero_traitement'];
            } else { // pour voir bouton actualiser sur derniere etape de délégation
                $delegation_restante['Visa.numero_traitement <='] = $traitement['Traitement']['numero_traitement'];
            }
            $visas_retard = [];
            if (!$this->Visa->hasAny($jump)) {
                $visas_retard = $this->Visa->find('all', ["conditions" => $delegation_restante, "recursive" => -1]);
            }
            //boutons MàJ visas en retard
            $this->set('visas_retard', $visas_retard);
        }


        if ($this->request->is(['post', 'put'])) {
            if ($this->TokensMethods->locked(
                ['controller' => 'projets', 'action' => 'edit' , 'entity_id' => $id]
            )
            ) {
                $this->Flash->set(
                    __(
                        'Vous ne pouvez pas traiter ce projet, car il est actuellement '
                        . 'édité par un autre utilisateur.'
                        . "\n" .
                        'Pour plus d\'informations, veuillez contacter votre administrateur'
                    ),
                    [
                      'element' => 'growl',
                      'params' => [
                          'type' => 'warning'
                      ]
                    ]
                );

                return $this->redirect($this->previous);
            }

            if ($valid == '1') {
                $this->accepteDossier($id);
                $this->Flash->set(__('Vous venez d\'accepter le projet %s ', $id), ['element' => 'growl']);
            } else {
                $this->refuseDossier($id);
                $this->Flash->set(__('Vous venez de refuser le projet %s ', $id), ['element' => 'growl']);
            }
            return $this->redirect(['controller' => 'projets', 'action' => 'mesProjetsATraiter']);
        }

        //Afficher bouton MàJ
        $this->set('majDeleg', $visa);

        $this->view($id);
    }

    /**
     * @param type $delib_id
     * @return CakeResponse
     *@version 4.3
     * @access public
     */
    public function goNext($delib_id)
    {
        $this->Deliberation->Behaviors->load('Version', $this->Deliberation->actsAsVersionOptionsList['Version']);

        $delib = $this->Deliberation->find('first', [
            'conditions'=> [
                'Deliberation.id' => $delib_id
            ],
        ]);

        if (empty($delib)) {
            $this->Flash->set(
                __("Le projet n'existe pas"),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            return $this->redirect($this->previous);
        }

        if ($delib['Deliberation']['parapheur_etat'] === 1) {
            $this->Flash->set(
                __('Le projet est dans une étape parapheur, on ne peut pas sauter d\'étapes.'),
                ['element' => 'growl','params' => ['type' => 'danger']]
            );
            return $this->redirect($this->previous);
        }

        if (empty($this->data)) {
            $etapes = $this->Traitement->listeEtapes($delib['Deliberation']['id'], ['selection' => 'APRES']);
            if (empty($etapes)) {
                $this->Flash->set(
                    __("Le projet n'a pas d'étape suivante"),
                    ['element' => 'growl','params' => ['type' => 'danger']]
                );
                return $this->redirect($this->previous);
            }
            $this->set('delib_id', $delib_id);
            $this->set('etapes', $etapes);
        } else {
            if (Configure::read('Cakeflow.commentaire.obligatoire')
                && empty($this->data['Traitement']['Commentaire'])
            ) {
                $this->Flash->set(__('Le commentaire est obligatoire lors d\'un saut d\'étape'), [
                  'element' => 'growl',
                  'params' => ['type' => 'danger']
                ]);
                return $this->redirect($this->previous);
            }
            $this->Commentaire->enregistre(
                $delib_id,
                $this->Auth->user('id'),
                $this->data['Traitement']['Commentaire']
            );

            $insertion = [
                '0' => [
                    'Etape' => [
                        'etape_id' => null,
                        'etape_nom' => __('Aller à une étape suivante'),
                        'etape_type' => 1
                    ],
                    'Visa' => [
                        '0' => [
                            'trigger_id' => $this->Auth->user('id'),
                            'type_validation' => 'V'
                            ]
                    ]
                ]
            ];

            $this->Traitement->execute('JS', $this->Auth->user('id'), $delib_id, [
                'insertion' => $insertion,
                'numero_traitement' => $this->data['Traitement']['etape']
            ]);

            $destinataires = $this->Traitement->whoIs($delib_id, 'current', 'RI');
            foreach ($destinataires as $destinataire_id) {
                if ($destinataire_id > 0) {
                    $this->User->notifier($delib_id, $destinataire_id, 'traitement');
                }
            }

            $this->Historique->enregistre(
                $delib_id,
                $this->Auth->user('id'),
                __("Saut d'étape du projet")
            );
            $this->Flash->set(
                __("Le projet est maintenant à l'étape suivante "),
                ['element' => 'growl']
            );
            return $this->redirect($this->previous);
        }
    }

    /**
     * La fonction gère l'affichage de la page "envoyer à" puis gère
     * l'envoi des données nécessaires pour Cakeflow
     *
     * @version 4.3
     * @access public
     * @param type $delib_id --> id de la délibération
     * @return type Page courante
     */
    public function rebond($delib_id)
    {
        $isLocked = $this->TokensMethods->locked(
            [
              'controller' => 'projets',
              'action' => 'edit' ,
              'entity_id' => $delib_id
            ]
        );
        if ($isLocked) {
            $this->Flash->set(
                __(
                    'Vous ne pouvez pas traiter ce projet, car il est actuellement édité '
                    . 'par un autre utilisateur.'
                    . "\n" .
                    'Pour plus d\'informations, veuillez contacter votre administrateur'
                ),
                ['element' => 'growl', 'params' => ['type' => 'warning']]
            );

            return $this->redirect($this->previous);
        }

        $this->Deliberation->Behaviors->load(
            'Version',
            $this->Deliberation->actsAsVersionOptionsList['Version']
        );

        $this->set('delib_id', $delib_id);
        $acte = $this->Deliberation->find('first', [
            'conditions' => ['Deliberation.id' => $delib_id],
            'fields' => ['Deliberation.redacteur_id'],
            'recursive' => -1]);
        $redacteur_id = $acte['Deliberation']['redacteur_id'];

        if (empty($this->request->data)) {
            $this->request->data['Insert']['retour'] = true;
            $users = $this->User->getUsersUsernameByAcl('mesProjetsATraiter', 'read');
            $this->User->setUsersRedacteurWithUsername($users, $redacteur_id);
            $this->set('users', $users);
            $this->set(
                'collaboratif',
                $this->Traitement->typeEtape($delib_id) === $this->Etape->getTypeValue('collaboratif') ? true : false
            );
        } elseif (empty($this->data['Insert']['users_id'])) {
            $this->Flash->set(
                __('Aucun destinataire(s) sélectionné(s)'),
                [
                  'element' => 'growl',
                  'params' => ['type' => 'danger']
                ]
            );

            return $this->redirect($this->here);
        } else {
            $destinataires = '';
            $destinataires_historiques = '';
            foreach ($this->data['Insert']['users_id'] as $id) {
                if ($destinataires != '') {
                    $destinataires .= ', ';
                    $destinataires_historiques .= ',';
                }
                $user = $this->User->find('first', [
                    'fields' => ['nom', 'prenom', 'username'],
                    'conditions' => ['User.id' => $id],
                    'recursive' => -1]);
                $destinataires .= $user['User']['nom'] . ' ' . $user['User']['prenom'];
                $destinataires_historiques .=
                    $user['User']['prenom'] . ' ' . $user['User']['nom'] . ' (' . $user['User']['username'] . ')';
            }

            if ($this->data['Insert']['etape_choisie'] != 1) {
                if (count($this->data['Insert']['users_id']) <= 1) {
                    $this->Flash->set(__('Veuillez sélectioner au moins 2 utilisateurs.'), [
                        'element' => 'growl',
                        'params' => ['type' => 'danger']
                    ]);
                    return $this->redirect(['action' => 'rebond', $delib_id]);
                }
            }
            //on récupère les données de l'utilisateur courant
            // initialisation des visas à ajouter au traitement
            //on crée le tableau de mise à jour
            $options = [
                'insertion' => [0 => [
                        'Etape' => [
                            'etape_id' => null,
                            'etape_nom' => $destinataires,
                            //simple 2ou 3et
                            'etape_type' => $this->data['Insert']['etape_choisie'],
                        ],
                        'Visa' => [0 => [
                                'type_validation' => 'V',
                                'trigger_id' => $this->data['Insert']['users_id']
                            ]
                        ]
                    ]
                ]
            ];
            if ($this->data['Insert']['option'] === 'retour') {
                $action_com = __('envoyé en aller-retour');
                $action = 'IL';
            } elseif ($this->data['Insert']['option'] === 'detour') {
                $action_com = __('envoyé (sans retour)');
                $action = 'IP';
            } elseif ($this->data['Insert']['option'] === 'validation') {
                $action_com = __('envoyé en validation finale');
                $action = 'VF';
            }
            if (!empty($this->request->data['Insert']['Commentaire'])) {
                $this->Commentaire->enregistre(
                    $delib_id,
                    $this->Auth->user('id'),
                    $this->request->data['Insert']['Commentaire']
                );
            }

            //Cakeflow insertion de/des utilisateurs dans le circuits
            //la gestion et/ou est faite par l'envoi d'un tableau id voulue
            //dans le triger_id au lieu d'un seul
            $this->Traitement->execute($action, $this->Auth->user('id'), $delib_id, $options);
            $this->Historique->enregistre(
                $delib_id,
                $this->Auth->user('id'),
                __('Le projet a été %s à %s', $action_com, $destinataires_historiques)
            );

            foreach ($this->data['Insert']['users_id'] as $id) {
                if ($id > 0) {
                    $this->User->notifier($delib_id, $id, 'traitement');
                }
            }
            $this->Flash->set(
                __('Le projet a été %s à %s', $action_com, $destinataires),
                ['element' => 'growl', 'params' => ['type' => 'info']]
            );

            $this->redirect($this->History->goBackBefore('/validations/traiter'));
        }
    }

    /**
     * @return CakeResponse|null
     */
    public function validerUrgence()
    {
        $deliberationIds = $this->ProjetTools->traitementParLotVerification();

        if (!empty($deliberationIds)) {
            foreach ($deliberationIds as $deliberation_id) {
                $this->validerEnUrgence($deliberation_id, false);
            }
            $this->Flash->set(__('Action effectuée avec succès'), ['element' => 'growl']);
        }

        return $this->redirect($this->previous);
    }

    /**
     * Valide un ou plusieurs dossier présents dans une liste
     * @version 5.1
     * @return CakeResponse|null
     */
    public function valider()
    {
        $deliberationIds = $this->ProjetTools->traitementParLotVerification();

        if (!empty($deliberationIds)) {
            foreach ($deliberationIds as $deliberation_id) {
                $this->accepteDossier($deliberation_id);
            }
            $this->Flash->set(__('Action effectuée avec succès'), ['element' => 'growl']);
        }

        $this->redirect($this->History->goBackBefore('/validations/traiter'));
    }


    /**
     * @version 5.1
     * @return CakeResponse|null
     */
    public function refuser()
    {
        $deliberationIds = $this->ProjetTools->traitementParLotVerification();

        if (!empty($deliberationIds)) {
            foreach ($deliberationIds as $deliberation_id) {
                $this->refuseDossier($deliberation_id, false);
            }
            $this->Flash->set(__('Action effectuée avec succès'), ['element' => 'growl']);
        }

        $this->redirect($this->History->goBackBefore('/validations/traiter'));
    }

    /**
     * Page de vue détaillée
     *
     * @version 5.1
     * @access public
     * @param type $id
     * @return type
     *
     * @SuppressWarnings(PHPMD)
     */
    public function view($id = null)
    {
        $this->Deliberation->Behaviors->load(
            'Version',
            $this->Deliberation->actsAsVersionOptionsList['Version']
        );

        //Pour l'affichage de l'onglet
        if (isset($this->request['named']['nameTab'])) {
            $this->set('nameTab', $this->request['named']['nameTab']);
        }
        $projet = $this->Deliberation->find('first', [
            'fields' => [
                'id', 'anterieure_id', 'service_id', 'circuit_id', 'typeacte_id', 'anterieure_id',
                'etat',
                'num_pref', 'typologiepiece_code', 'tdt_document_papier', 'num_delib',
                'titre', 'objet', 'objet_delib','parent_id', 'date_limite', 'date_acte',
                'texte_projet_name', 'texte_synthese_name', 'deliberation_name', 'signee', 'redacteur_id',
                'Deliberation.parapheur_etat',
                'Deliberation.created', 'Deliberation.modified', 'deliberation', 'texte_projet',
                'texte_synthese', 'tdt_data_pdf', 'parapheur_bordereau'],
            'contain' => [
                'User' => ['id', 'nom', 'prenom'],
                'Multidelib' => [
                    'fields' => [
                        'id', 'objet', 'objet_delib', 'num_delib', 'etat', 'deliberation', 'deliberation_name'
                    ],
                    'Annexe' => [
                        'fields' => [
                            'id', 'titre', 'joindre_ctrl_legalite', 'joindre_fusion',
                            'filename','typologiepiece_code', 'size',
                        ]
                    ]
                ],
                'Redacteur' => ['fields' => ['id', 'nom', 'prenom']],
                'Rapporteur' => ['fields' => ['id', 'nom', 'prenom']],
                'Infosup',
                'Annexe' => [
                    'fields' => [
                        'id', 'titre', 'joindre_ctrl_legalite', 'joindre_fusion', 'filename',
                        'typologiepiece_code', 'size',
                    ],
                    'order' => ['Annexe.position'=>'ASC']
                ],
                'Service' => ['fields' => ['name']],
                'Theme' => ['fields' => ['libelle']],
                'Typeacte' => [
                    'fields' => ['name', 'modele_bordereau_projet_id'],
                    'Nature' => ['fields' => ['code']]
                ],
                'Circuit' => ['fields' => ['nom']],
                'DeliberationTypeseance' => ['fields' => ['id'],
                    'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]],
                'DeliberationSeance' => ['fields' => ['id'],
                    'Seance' => ['fields' => ['id', 'date', 'type_id'],
                        'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]
                    ]
                ]
            ],
            'conditions' => ['Deliberation.id' => $id],
            'recursive' => -1
        ]);

        if (empty($projet)) {
            $this->Flash->set(__("Le projet n° %s est introuvable !", $id), ['element' => 'growl']);
            return $this->redirect($this->previous);
        }

        //On cherche à savoir si la délibération est vérrouillée par un utilisateur
        $this->ProjetTools->locked($projet['Deliberation']);

        // Multi-délibération
        $info_id = !empty($projet['Deliberation']['parent_id']) ? $projet['Deliberation']['parent_id'] : $id;


        $projet['Deliberation']['iconeEtat'] = $this->ProjetTools->iconeEtat(
            $projet['Deliberation']['id'],
            $projet['Deliberation']['etat'],
            $projet['Typeacte']['Nature']['code'],
        );

        $projet['DeliberationSeance'] = Hash::sort(
            $projet['DeliberationSeance'],
            '{n}.Seance.Typeseance.action',
            'asc'
        );

        $projet['Deliberation']['num_pref'] = $this->ProjetTools->getMatiereByKey($projet['Deliberation']['num_pref']);


        //Récupération du model_id (pour lien bouton generer)
        $model_id = $this->Deliberation->getModelId($id);
        $projet['Modeltemplate']['id'] = $model_id;

        // Mise en forme des données du projet ou de la délibération
        $projet['Deliberation']['libelleEtat'] = $this->ProjetEtat->libelleEtat(
            $projet['Deliberation']['etat'],
            $projet['Typeacte']['Nature']['code']
        );
        $projet['Deliberation']['typologiepiece_code_libelle'] = $this->Typologiepiece->getTypologieNamebyCode(
            $projet['Deliberation']['typologiepiece_code']
        );
        foreach ($projet['Annexe'] as &$annexe) {
            $annexe['typologiepiece_code_libelle'] = $this->Typologiepiece->getTypologieNamebyCode(
                $annexe['typologiepiece_code']
            );
        }
        foreach ($projet['Multidelib'] as &$multidelib) {
            foreach ($multidelib['Annexe'] as &$annexe) {
                $annexe['typologiepiece_code_libelle'] = $this->Typologiepiece->getTypologieNamebyCode(
                    $annexe['typologiepiece_code']
                );
            }
        }
        // initialisation des séances
        $listeTypeSeance = [];
        $projet['listeSeances'] = [];
        if (isset($projet['DeliberationSeance']) && !empty($projet['DeliberationSeance'])) {
            foreach ($projet['DeliberationSeance'] as $seance) {
                $projet['listeSeances'][] = [
                    'seance_id' => $seance['Seance']['id'],
                    'type_id' => $seance['Seance']['type_id'],
                    'action' => $seance['Seance']['Typeseance']['action'],
                    'libelle' => $seance['Seance']['Typeseance']['libelle'],
                    'color' => $seance['Seance']['Typeseance']['color'],
                    'date' => $seance['Seance']['date']];
                $listeTypeSeance[] = $seance['Seance']['type_id'];
            }
        }

        if (isset($projet['DeliberationTypeseance']) && !empty($projet['DeliberationTypeseance'])) {
            foreach ($projet['DeliberationTypeseance'] as $typeseance) {
                if (!in_array($typeseance['Typeseance']['id'], $listeTypeSeance, true)) {
                    $projet['listeSeances'][] = [
                        'seance_id' => null,
                        'type_id' => $typeseance['Typeseance']['id'],
                        'action' => $typeseance['Typeseance']['action'],
                        'libelle' => $typeseance['Typeseance']['libelle'],
                        'color' => $typeseance['Typeseance']['color'],
                        'date' => null];
                }
            }
        }
        $projet['listeSeances'] = Hash::sort($projet['listeSeances'], '{n}.action', 'asc');
        $projet['Service']['name'] = $this->Deliberation->Service->doList($projet['Deliberation']['service_id']);
        $projet['Circuit']['libelle'] = $this->Circuit->getLibelle($projet['Deliberation']['circuit_id']);

        $commentaires_conditions = ['Commentaire.delib_id' => $info_id,];
        if ($projet['Deliberation']['etat'] == 1) {
            $commentaires_conditions['Commentaire.pris_en_compte'] = 0;
        }
        $this->set('commentaires', $this->Commentaire->find('all', [
            'fields' => ['id', 'Commentaire.texte', 'Commentaire.created'],
            'contain' => ['User.nom', 'User.prenom'],
            'conditions' => $commentaires_conditions,
            'order' => ['Commentaire.created' => 'DESC'],
            'recursive' => -1
        ]));

        //Test si le projet a été inséré dans un circuit, si oui charger l'affichage
        $wkf_exist = $this->Traitement->find('count', ['recursive' => -1, 'conditions' => ['target_id' => $info_id]]);
        if (!empty($wkf_exist)) {
            $this->set('visu', $this->requestAction(['plugin' => 'cakeflow',
                'controller' => 'traitements',
                'action' => 'visuTraitement', $info_id], ['return']));
        } else {
            $this->set('visu', null);
        }

        // Compactage des informations supplémentaires
        if (!empty($projet['Infosup'])) {
            $this->set('infosups', $this->Deliberation->Infosup->compacte($projet['Infosup'], false));
        }

        // Définitions des infosup
        $this->set('infosupdefs', $this->Infosupdef->find('all', [
            'recursive' => -1,
            'conditions' => [
                'model' => 'Deliberation',
                'actif' => true
            ],
            'order' => 'ordre']));


        $this->set('historiques', $this->Historique->find('all', [
            'fields' => ['Historique.user_id', 'Historique.commentaire', 'Historique.created','Historique.revision_id'],
            'contain' => ['User.nom', 'User.prenom'],
            'conditions' => ['Historique.delib_id' => $id],
            //'joins' => array($this->Historique->join('User',array( 'type' => 'INNER' ))),
            'order' => ['Historique.created' => 'DESC','Historique.id' => 'DESC'],
            'recursive' => -1
        ]));

        $this->set('projet', $projet);
        $this->set('revisions', $this->Deliberation->versions($id));
    }

    /**
     * Permet de valider un projet en cours de validation en court-circuitant le circuit de validation
     * Appelée depuis la vue deliberations/tous_les_projets
     *
     * @param int $id
     * @param boolean $redirect
     * @return CakeResponse
     */
    public function validerEnUrgence($id, $redirect = true)
    {
        $this->validerEnUrgenceLeTraitement($id, $redirect);
    }
}
