<?php

/**
 *
 * AllosController File
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       web-delib v4.4
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * AllosController AppController
 *
 * @version 4.3
 * @package app.Controller.AllosController
 */
class AllosController extends AppController
{
    public $uses = ['Collectivite'];
    public $components = [
        'RequestHandler',
        'Auth' => [
            'mapActions' => [
                'allow' => ['version', 'create', 'update', 'read', 'delete']
            ]
        ]];

    /**
     * @version 4.3
     * @access public
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    /**
     * Retourne la base des informations concernant le produit, la version et le client utilisant l'application.
     *
     * @version 4.3
     * @access public
     */
    public function version()
    {
        if ($this->request->is('get')) {
            $this->Collectivite->recursive = 1;
            $collectivte = $this->Collectivite->findById(1);
            $json = [
                'produit' => 'web-delib',
                'version' => VERSION,
                'refClient' => Configure::read('REF_CLIENT')
            ];

            $this->autoRender = false;
            $this->response->type(['json' => 'text/x-json']);
            $this->RequestHandler->respondAs('json');
            $this->response->body(json_encode($json));
            return $this->response;
        }
    }
}
