<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppController', 'Controller');

/**
 * Class VotesController
 *
 * @package app.Controller
 */
class AvisController extends AppController
{
    /**
     * [public description]
     * @var [type]
     */
    public $uses = [
        'Deliberation',
        'DeliberationTypeseance',
        'Seance',
        'Typeseance',
    ];
    /**
     * [public description]
     * @var [type]
     */
    public $components = [
        'RequestHandler',
        'SeanceTools',
        'Auth' => [
            'mapActions' => [
                'create' => ['add'],
                'read' => ['index'],
                'update' => ['edit'],
                'delete' => ['delete'],
                'sendSeanceAvisToIdelibre',
            ],
        ]];

    /**
     * Donner un avis et affecté une ou des nouvelles séances au projet de délibération
     *
     * @param type $deliberation_id
     * @param type $seance_id
     *
     */
    public function edit($deliberation_id, $seance_id)
    {
        //FIX Sens des variables et changement $seance_id => $id
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($seance_id, 'update');

        // Initialisations
        $deliberation = $this->Deliberation->find('first', [
            'conditions' => ['Deliberation.id' => $deliberation_id],
            'fields' => [
                'Deliberation.id', 'Deliberation.typeacte_id',
                'Deliberation.objet', 'Deliberation.objet_delib', 'Deliberation.etat'
            ]
        ]);
        $delib_seance = $this->Deliberation->DeliberationSeance->find('first', [
            'conditions' => [
                'DeliberationSeance.seance_id' => $seance_id,
                'DeliberationSeance.deliberation_id' => $deliberation_id],
            'recursive' => -1,
        ]);

        $seance = $this->Seance->find('first', [
            'conditions' => ['Seance.id' => $seance_id],
            'fields' => ['id', 'date', 'president_id', 'type_id'],
            'contain' => ['Typeseance.compteur_id','Typeseance.libelle'],
            'recursive' => -1
        ]);

        if ($this->request->is('Post')) {
            if (!array_key_exists('avis', $this->data['Deliberation'])) {
                $this->Seance->invalidate('avis');
            } else {
                $this->Deliberation->DeliberationSeance->id = $delib_seance['DeliberationSeance']['id'];
                $this->Deliberation->DeliberationSeance->set('deliberation_id', $deliberation_id);
                $this->Deliberation->DeliberationSeance->set('seance_id', $seance_id);
                $this->Deliberation->DeliberationSeance->set(
                    'avis',
                    $this->data['Deliberation']['avis'] == 1 ? true : false
                );
                $this->Deliberation->DeliberationSeance->set('reset', false);
                $this->Deliberation->DeliberationSeance->set(
                    'commentaire',
                    $this->data['Deliberation']['commentaire']
                );
                $this->Deliberation->DeliberationSeance->save();

                if (!empty($this->data['Deliberation']['seance_id'])) {
                    foreach ($this->data['Deliberation']['seance_id'] as $seanceId) {
                        //Ajout du type de séance pour la délibération
                        $this->DeliberationTypeseance->create();
                        $this->DeliberationTypeseance->save(['deliberation_id' => $deliberation_id,
                            'typeseance_id' => $this->Seance->getType($seanceId)]);
                        //Ajout de la séance pour la délibération
                        $this->Deliberation->DeliberationSeance->addDeliberationSeance($deliberation_id, $seanceId);
                    }
                }

                // ajout du commentaire
                $date_seance = $this->Seance->getDate($seance_id);
                $type_seance = $this->Seance->getType($seance_id);
                $this->Seance->Typeseance->recusive = -1;
                $this->Seance->Typeseance->id = $type_seance;
                $this->request->data['Commentaire']['delib_id'] = $this->data['Deliberation']['id'];
                $this->request->data['Commentaire']['commentaire_auto'] = 1;
                $this->request->data['Commentaire']['texte'] = __(
                    'A reçu un avis %s en %s du %s à %s',
                    $this->data['Deliberation']['avis'] == '1' ? 'favorable' : 'défavorable',
                    $this->Seance->Typeseance->field('libelle'),
                    CakeTime::i18nFormat($date_seance, '%A %e %B %Y'),
                    CakeTime::i18nFormat($date_seance, '%k:%M')
                );
                $this->Deliberation->Commentaire->save($this->data);

                $this->redirect($this->previous);
            }
        } else {
            $this->request->data = $deliberation;
        }

        $afficherTtesLesSeances = $this->Acl->check(
            ['User' => ['id' => $this->Auth->user('id')]],
            'Deliberations',
            'editerTous',
            'read'
        );

        //On retire les séances ou le projet est déja inclus
        $DeliberationSeance = $this->Deliberation->DeliberationSeance->find('all', [
            'fields' => ['DeliberationSeance.seance_id'],
            'conditions' => ['DeliberationSeance.deliberation_id' => $deliberation_id],
            'recursive' => -1]);
        $seance_notinclude = [];
        $exclude = false;
        foreach ($DeliberationSeance as $deliberation_seance) {
            $delibSeanceId = $deliberation_seance['DeliberationSeance']['seance_id'];
            if ($this->Seance->isDeliberante($delibSeanceId)) {
                $exclude = true;
            }
            $notincludes[$delibSeanceId] = $delibSeanceId;
        }

        // On liste les séances délibérantes pour les exclure aussi
        // si la séance actuelle est elle-même délibérante
        if ($exclude) {
            $deliberantes = $this->Seance->getSeancesDeliberantes();
            foreach ($deliberantes as $deliberante) {
                $notincludes[$deliberante] = $deliberante;
            }
        }

        // on définit le tableau des séances à exclure
        foreach ($notincludes as $notinclude) {
            $seance_notinclude[] = ['Seance.id <>' => $notinclude];
        }

        $this->set('seance', $seance);
        $this->set('seances', $this->Seance->generateList(
            $seance_notinclude,
            $afficherTtesLesSeances,
            $deliberation['Deliberation']['typeacte_id']
        ));
        $this->set('avis', [true => 'Favorable', false => 'Défavorable']);
        $this->set('avis_selected', $delib_seance['DeliberationSeance']['avis']);
        $this->set('commentaire', $delib_seance['DeliberationSeance']['commentaire']);
        $this->set('seances_selected', $this->Deliberation->getCurrentSeances(
            $deliberation_id,
            false
        ));
        $this->set('seance_id', $seance_id);
    }

    /**
     *  Liste des avis de commissions
     *
     * @access public
     *
     * @param type $seance_id
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function index($id = null)
    {
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($id, 'update');
        $this->set('seance_id', $id);

        $seance = $this->Seance->find(
            'first',
            [
                'conditions' => ['Seance.id' => $id],
                'fields' => ['Seance.type_id'],
                'contain' => ['Typeseance.libelle', 'Typeseance.action']]
        );

        $projets = $this->Deliberation->find('all', [
            'fields' => [
                'Deliberation.id',
                'Deliberation.objet',
                'Deliberation.objet_delib',
                'Deliberation.etat',
                'Deliberation.signee',
                'Deliberation.titre',
                'Deliberation.date_limite',
                'Deliberation.anterieure_id',
                'Deliberation.num_pref',
                'Deliberation.rapporteur_id',
                'Deliberation.redacteur_id',
                'Deliberation.circuit_id',
                'Deliberation.typeacte_id',
                'Deliberation.theme_id',
                'Deliberation.date_acte',
                'Deliberation.tdt_ar_date',
                'Deliberation.service_id',
                'Deliberation.num_delib',
                'Deliberation.parapheur_etat',
                'Theme.libelle',
                'Theme.order',
                'DeliberationSeance.position'
            ],
            'contain' => [
                'Theme' => ['fields' => ['order', 'libelle']],
                'User' => ['fields' => ['id']],
                'Annexe' => ['fields' => ['id', 'filename'], 'order' => ['position']],
                'Service' => ['fields' => ['id', 'order', 'name']],
                'Typeacte' => [
                    'fields' => ['name'],
                    'Nature' => ['fields' => ['code']]
                ],
                'Circuit' => ['fields' => ['nom']],
                'DeliberationTypeseance' => [
                    'fields' => ['id'],
                    'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]
                ],
                'DeliberationSeance' => [
                    'fields' => ['id'],
                    'Seance' => [
                        'fields' => ['id', 'date', 'type_id'],
                        'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]
                    ]
                ],
                'Rapporteur' => ['fields' => ['nom', 'prenom']],
                'President' => ['fields' => ['nom', 'prenom']],
            ],
            'joins' => [$this->Deliberation->join('DeliberationSeance', ['type' => 'INNER'])],
            'conditions' => ['DeliberationSeance.seance_id' => $id, 'Deliberation.etat >=' => 0],
            'order' => 'DeliberationSeance.position ASC'
        ]);

        $aPosition=[];
        for ($i = 0, $iMax = count($projets); $i < $iMax; $i++) {
            $deliberation_id = $projets[$i]['Deliberation']['id'];
            $delib_seance = $this->Deliberation->DeliberationSeance->find(
                'first',
                [
                    'conditions' => [
                        'DeliberationSeance.seance_id' => $id,
                        'DeliberationSeance.deliberation_id' => $deliberation_id],
                    'recursive' => -1]
            );
            $projets[$i]['Deliberation']['avis'] = $delib_seance['DeliberationSeance']['avis'];
            $projets[$i]['Service']['libelle'] = $this->Deliberation->Service->doList($projets[$i]['Service']['id']);
            $projets[$i]['Modeltemplate']['id'] = $this->Typeseance->modeleProjetDelibParTypeSeanceId(
                $this->Seance->getType($id),
                $projets[$i]['Deliberation']['etat']
            );

            $btn_actions = [
                'genereFusionToClientByModelTypeNameAndSeanceId', 'saisirDebat', 'donnerAvis','annulerAvis'
            ];

            $aPosition[$projets[$i]['DeliberationSeance']['position']] =
                $projets[$i]['DeliberationSeance']['position'];
            $projets[$i]['btn_actions'] = $btn_actions;
        }

        $this->set('aPosition', $aPosition);
        $this->set('seance', $seance);
        $this->set('date_seance', $this->Seance->getDate($id));
        $this->SeanceTools->afficheProjets('index', $projets);
    }

    /**
     * [annulerAvis description]
     * @param  [type]  $deliberation_id [description]
     * @param  [type]  $id              [description]
     * @return boolean                  [description]
     */
    public function delete($deliberation_id, $id)
    {
        //Gestion du droit des types de séance
        $this->SeanceTools->checkTypeSeance($id, 'delete');

        $delib_seance = $this->Deliberation->DeliberationSeance->find('first', [
            'conditions' => [
                'DeliberationSeance.seance_id' => $id,
                'DeliberationSeance.deliberation_id' => $deliberation_id],
            'recursive' => -1,
        ]);

        try {
            $this->Deliberation->id = $deliberation_id;
            $delib_seance['DeliberationSeance']['avis'] = null;
            $delib_seance['DeliberationSeance']['reset'] = true;
            $this->Deliberation->DeliberationSeance->save($delib_seance['DeliberationSeance']);
        } catch (Exception $e) {
            $this->Flash->set($e->getMessage(), ['element' => 'growl','params'=>['type' => 'danger']]);
        }

        // ajout du commentaire
        $this->request->data['Commentaire']['delib_id'] = $deliberation_id;
        $this->request->data['Commentaire']['commentaire_auto'] = 1;
        $this->request->data['Commentaire']['texte'] = __('L\'avis a été supprimé');
        try {
            $this->Deliberation->Commentaire->save($this->data);
            $this->Flash->set(__('L\'avis a été supprimé'), ['element' => 'growl','params'=>['type' => 'success']]);
        } catch (Exception $e) {
            $this->Flash->set($e->getMessage(), ['element' => 'growl','params'=>['type' => 'danger']]);
        }

        $this->redirect($this->previous);
    }
}
