<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppController', 'Controller');
App::uses('AppTools', 'Lib');
App::uses('SaeVersement', 'Model');

/**
 * Controller versement SAE des actes
 *
 * @package app.Controller
 * @version 7.1.0
 */
class VerserSaeController extends AppController
{
    /** @var Array Contains a uses */
    public $uses = [
        'Seance',
        'Deliberation',
        'DeliberationSeance',
        'DeliberationTypeseance',
        'Nature',
        'Nomenclature',
        'SaeVersement'
    ];

    /** @var Array Contains a components */
    public $components = [
        'RequestHandler',
        'Progress',
        'Filtre',
        'Paginator',
        'Conversion',
        'VersementSae',
        'Auth' => [
            'mapActions' => [
                'sendToSaeAutresActes',
                'sendToSaeDeliberation',
                'allow' => ['verser']
            ],
        ]];


    public $helpers = ['Sae'];

    /**
     * @version 7.1
     * @access private
     * @param type $projets
     */
    private function ajouterFiltreAutresActes(&$projets)
    {
        if (!$this->Filtre->critereExists() && !empty($projets)) {
            $typeactes = $this->Deliberation->Typeacte->find('list', [
                'recursive' => -1,
                'conditions' => [
                    'Typeacte.deliberant' => false
                ],
                'order' => ['Typeacte.name' => 'ASC'],
                'allow' => ['Typeacte.id' => 'read']]);

            $this->Filtre->addCritere('Typeacte', [
                'field' => 'Deliberation.typeacte_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des séances'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'label' => __('Type d\'acte'),
                    'options' => $typeactes
                ]]);
            $themes = $this->Deliberation->Theme->generateTreeListByOrder(
                ['Theme.actif' => '1'],
                '&nbsp;&nbsp;&nbsp;&nbsp;'
            );
            $this->Filtre->addCritere('ThemeId', [
                'field' => 'Deliberation.theme_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'label' => __('Thème'),
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des thèmes'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'options' => $themes,
                    'escape' => false
                ]]);
            $services = $this->Service->find('list', [
                'fields' => ['id', 'name'],
                'order' => ['Service.name' => 'ASC'],
                'conditions' => ['actif' => true], 'recursive' => -1]);
            $this->Filtre->addCritere('ServiceId', [
                'field' => 'Deliberation.service_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'label' => __('Service émetteur'),
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des services éméteur'),
                    'data-allow-clear' => true,
                    'multiple' => 'multiple',
                    'options' => $services,
                ]]);
            $this->Filtre->addCritere('Verser', [
                'field' => 'Deliberation.sae_etat',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Versé'),
                    'options' => [
                        SaeVersement::ERREUR => __('Versement en erreur'),
                        SaeVersement::REFUSER => __('Transfert du dossier refusé'),
                        SaeVersement::ATTENTE => __('En attente de versement'),
                        SaeVersement::VERSER => __('Versement en cours'),
                        SaeVersement::ACCEPTER => __('Transfert du dossier accepté'),
                        SaeVersement::VERSER_MANUELLEMENT => __('Versé manuellement'),
                    ],
                    'data-placeholder' => __('Sélectionner un état'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'title' => __('Filtre sur le versement')],
                'column' => 2
            ]);
            //Affichage des utilisateurs actifs par défaut
            //$this->Filtre->setCritere('Publier', 0);
        }
    }

    /**
     * @version 7.1
     * @param type $projets
     */
    private function ajouterFiltreDeliberation(&$projets)
    {
        if (!$this->Filtre->critereExists() && !empty($projets)) {
            $conditions[] = 'Seance.id IN ('
                . 'SELECT DISTINCT deliberations_seances.seance_id'
                . ' FROM deliberations_seances '
                . ' INNER JOIN deliberations  ON ( deliberations.id=deliberations_seances.deliberation_id )'
                . ' INNER JOIN seances  ON ( seances.id=deliberations_seances.seance_id )'
                . ' INNER JOIN typeseances ON ( typeseances.id=seances.type_id )'
                . ' INNER JOIN typeactes  ON ( typeactes.id=deliberations.typeacte_id )'
                . ' WHERE typeactes.teletransmettre = TRUE'
                . ' )';

            $seances = $this->Seance->find('all', [
                'fields' => ['Seance.id', 'Seance.type_id', 'Seance.date'],
                'conditions' => $conditions,
                //'conditions' => array('Seance.traitee' => 0),
                'contain' => ['Typeseance.libelle', 'Typeseance.retard'],
                'order' => ['Seance.date' => 'DESC', 'Typeseance.libelle' => 'ASC'],
            ]);

            $options_seances = [];
            foreach ($seances as $seance) {
                //Voir tous les projets ou tous les futures dates avec un delais respecté
                $options_seances[$seance['Seance']['id']] =
                    $seance['Typeseance']['libelle'] . ' : '
                    . CakeTime::i18nFormat($seance['Seance']['date'], '%A %e %B %Y')
                    . ' à '
                    . CakeTime::i18nFormat($seance['Seance']['date'], '%k:%M');
            }

            $this->Filtre->addCritere('DeliberationSeanceId', ['field' => 'DeliberationSeance_Filter.seance_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des séances'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'label' => __('Séances'),
                    'options' => $options_seances]]);


            $typeseances = [];
            foreach ($projets as $projet) {
                if (!empty($projet['DeliberationTypeseance'])) {
                    foreach ($projet['DeliberationTypeseance'] as $typeseance) {
                        if (!array_key_exists($typeseance['id'], $typeseances)) {
                            $typeseances[$typeseance['Typeseance']['id']] = $typeseance['Typeseance']['libelle'];
                        }
                    }
                }
            }
            $this->Filtre->addCritere('DeliberationTypeseanceId', [
                'field' => 'DeliberationTypeseance_Filter.typeseance_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des séances'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'label' => __('Type de séance'),
                    'options' => $typeseances]]);

            $typeactes = $this->Deliberation->Typeacte->find('list', [
                'recursive' => -1,
                'conditions' => [
                    'Typeacte.deliberant' => true
                ],
                'order' => ['Typeacte.name' => 'ASC'],
                'allow' => ['Typeacte.id' => 'read']]);

            $this->Filtre->addCritere('Typeacte', [
                'field' => 'Deliberation.typeacte_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des séances'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'label' => __('Type d\'acte'),
                    'options' => $typeactes
                ]]);
            $themes = $this->Deliberation->Theme->generateTreeListByOrder(
                ['Theme.actif' => '1'],
                '&nbsp;&nbsp;&nbsp;&nbsp;'
            );
            $this->Filtre->addCritere('ThemeId', [
                'field' => 'Deliberation.theme_id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'label' => __('Thème'),
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des thèmes'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'options' => $themes,
                    'escape' => false
                ]]);
            $services = $this->Service->find('list', [
                'fields' => ['id', 'name'],
                'order' => ['Service.name' => 'ASC'],
                'conditions' => ['actif' => true], 'recursive' => -1]);
            $this->Filtre->addCritere('ServiceId', [
                'field' => 'Deliberation.service_id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'label' => __('Service émetteur'),
                    'type' => 'select',
                    'data-placeholder' => __('Sélectionner des services éméteur'),
                    'data-allow-clear' => true,
                    'multiple' => 'multiple',
                    'options' => $services,
                ]]);
            $this->Filtre->addCritere('Verser', [
                'field' => 'Deliberation.sae_etat',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Versé'),
                    'options' => [
                        SaeVersement::ERREUR => __('Versement en erreur'),
                        SaeVersement::REFUSER => __('Transfert du dossier refusé'),
                        SaeVersement::ATTENTE => __('En attente de versement'),
                        SaeVersement::VERSER => __('Versement en cours'),
                        SaeVersement::ACCEPTER => __('Transfert du dossier accepté'),
                        SaeVersement::VERSER_MANUELLEMENT => __('Versé manuellement'),
                    ],
                    'data-placeholder' => __('Sélectionner un état'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'title' => __('Filtre sur le versement')],
                'column' => 2
            ]);
            //Affichage des utilisateurs actifs par défaut
            //$this->Filtre->setCritere('Publier', 0);
        }
    }

    /**
     * @param type $conditions
     * @return type
     *
     * @version 7.1.0
     */
    private function handleConditions($conditions)
    {
        $projet_type_ids = [];
        $projet_seance_ids = [];

        if (isset($conditions['DeliberationTypeseance.typeseance_id'])) {
            $type_id = $conditions['DeliberationTypeseance.typeseance_id'];
            $typeseances = $this->DeliberationTypeseance->find(
                'all',
                [
                    'conditions' => ['DeliberationTypeseance.typeseance_id' => $type_id
                    ],
                    'recursive' => -1]
            );
            foreach ($typeseances as $typeseance) {
                $projet_type_ids[] = $typeseance['DeliberationTypeseance']['deliberation_id'];
            }
            unset($conditions['DeliberationTypeseance.typeseance_id']);
        }
        if (isset($conditions['DeliberationSeance.seance_id'])) {
            $projet_seance_ids = $this->Seance->getDeliberationsId($conditions['DeliberationSeance.seance_id']);
            unset($conditions['DeliberationSeance.seance_id']);
        }
        $result = null;
        if (!empty($projet_type_ids) && !empty($projet_seance_ids)) {
            $result = array_intersect($projet_type_ids, $projet_seance_ids);
        } elseif (empty($projet_type_ids) && empty($projet_seance_ids)) {
            return $conditions;
        } elseif (empty($projet_type_ids)) {
            $result = $projet_seance_ids;
        } elseif (empty($projet_seance_ids)) {
            $result = $projet_type_ids;
        }

        if (isset($conditions['Deliberation.id'])) {
            $conditions['Deliberation.id'] = array_intersect($conditions['Deliberation.id'], $result);
        } elseif (!empty($result)) {
            $conditions['Deliberation.id'] = $result;
        }

        return ($conditions);
    }

    public function sendToSaeAutresActes()
    {
        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);

        $conditions = $this->handleConditions($this->Filtre->conditions());

        //Rechercher les projets transmissibles
        $db = $this->DeliberationSeance->getDataSource();
        $subQuery = [
            'fields' => [
                'Deliberation.id'
            ],
            'contain' => false,
            'joins' => [
                $this->Deliberation->join('Typeacte', ['type' => 'INNER', 'conditions' => [
                    'Typeacte.verser_sae' => true,
                    'Typeacte.teletransmettre' => true,
                    'Typeacte.deliberant' => false
                ]]),
            ],
            'conditions' => [
                'Deliberation.etat' => [
                    Deliberation::ETAT_ACTE_TDT_TRANSMIS,
                    Deliberation::ETAT_ACTE_TDT_TRANSMIS_MANUELLEMENT
                ],
                'Deliberation.tdt_ar_date <>' => null,
                'Deliberation.tdt_ar <>' => null,
            ]
        ];
        $subQueryTransmissibles = $this->DeliberationSeance->sql($subQuery);

        //Rechercher les projets non transmissibles
        $db = $this->DeliberationSeance->getDataSource();
        $subQuery = [
            'fields' => [
                'Deliberation.id'
            ],
            'contain' => false,
            'joins' => [
                $this->Deliberation->join('Typeacte', ['type' => 'INNER', 'conditions' => [
                    'Typeacte.publier' => true,
                    'Typeacte.teletransmettre' => false,
                    'Typeacte.deliberant' => false
                ]]),
            ],
            'conditions' => [
                'Deliberation.etat' => [
                    Deliberation::ETAT_ACTE_VOTE_ADOPTE,
                    Deliberation::ETAT_ACTE_VOTE_REJET,
                    Deliberation::ETAT_ACTE_SIGNATURE_SIGNEE
                ],
                'Deliberation.signee' => true
            ]
        ];

        $subQueryNonTransmissibles = $this->DeliberationSeance->sql($subQuery);
        $subQuery = ' "Deliberation"."id" IN (('
            . $subQueryTransmissibles
            . ') UNION ('
            . $subQueryNonTransmissibles . '))';
        $subQueryExpression = $db->expression($subQuery);
        $conditions[] = $subQueryExpression;

        $this->paginate = ['Deliberation' => [
            'countField' => 'DISTINCT Deliberation.id',
            'fields' => [
                'DISTINCT Deliberation.id',
                'Deliberation.objet',
                'Deliberation.objet_delib',
                'Deliberation.num_delib',
                'Deliberation.tdt_dateAR',
                'Deliberation.tdt_ar_date',
                'Deliberation.tdt_ar',
                'Deliberation.parapheur_id',
                'Deliberation.num_pref',
                'Deliberation.typologiepiece_code',
                'Deliberation.tdt_document_papier',
                'Deliberation.etat',
                'Deliberation.titre',
                'Deliberation.tdt_id',
                'Deliberation.pastell_id',
                'Deliberation.typeacte_id',
                'Deliberation.theme_id',
                'Deliberation.service_id',
                'Deliberation.tdt_status',
                'Deliberation.tdt_message',
                'Deliberation.sae_etat',
                'Deliberation.sae_date',
                'Deliberation.sae_commentaire',
            ],
            'conditions' => $conditions,
            'order' => ['Deliberation.id' => 'DESC'],
            'limit' => 20,
            'allow' => ['typeacte_id'],
            'recursive' => -1]];

        // On affiche que les delibs vote pour.
        $deliberations = $this->Paginator->paginate('Deliberation');

        $this->ProjetTools->sortProjetSeanceDate($deliberations);

        $this->ajouterFiltreAutresActes($deliberations);

        $this->set('deliberations', $deliberations);

        $this->set('nomenclatures', $this->Nomenclature->generateTreeListWithOptionGroup());

        $this->render('send_to_sae');
    }

    public function sendToSaeDeliberation($seanceId = null)
    {
        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);

        if (!empty($seanceId)) {
            $conditions['Seance.id'] = $seanceId;
            //Ajout de la condition sur séance par le filtre
            $conditions = $this->Filtre->conditions();
            $conditions['DeliberationSeance.seance_id'] = $seanceId;
            $conditions = $this->handleConditions($conditions);
        } else {
            $conditions = $this->handleConditions($this->Filtre->conditions());
            if (isset($conditions['Seance.id'])) {
                $seance_id = $conditions['Seance.id'];
                unset($conditions['Seance.id']);
            }
        }

        //Rechercher les projets transmissibles
        $db = $this->DeliberationSeance->getDataSource();
        $subQuery = [
            'fields' => [
                'DeliberationSeance.deliberation_id'
            ],
            'contain' => false,
            'joins' => [
                $this->DeliberationSeance->join('Deliberation', ['type' => 'INNER']),
                $this->DeliberationSeance->join('Seance', ['type' => 'INNER']),
                $this->Deliberation->join('Typeacte', ['type' => 'INNER', 'conditions' => [
                    'Typeacte.teletransmettre' => true,
                    'Typeacte.verser_sae' => true,
                    'Typeacte.deliberant' => true]]),
            ],
            'conditions' => [
                'Deliberation.etat' => [
                    Deliberation::ETAT_ACTE_TDT_TRANSMIS,
                    Deliberation::ETAT_ACTE_TDT_TRANSMIS_MANUELLEMENT
                ],
                'Deliberation.tdt_ar_date <>' => null,
                'Deliberation.tdt_ar <>' => null,
            ]
        ];

        $subQueryTransmissibles = $this->DeliberationSeance->sql($subQuery);

        //Rechercher les projets non transmissibles
        $db = $this->DeliberationSeance->getDataSource();
        $subQuery = [
            'fields' => [
                'DeliberationSeance.deliberation_id'
            ],
            'contain' => false,
            'joins' => [
                $this->DeliberationSeance->join('Deliberation', ['type' => 'INNER']),
                $this->DeliberationSeance->join('Seance', ['type' => 'INNER']),
                $this->Deliberation->join('Typeacte', ['type' => 'INNER', 'conditions' => [
                    'Typeacte.teletransmettre' => false,
                    'Typeacte.publier' => true,
                    'Typeacte.deliberant' => true]]),
            ],
            'conditions' => [
                'Deliberation.etat' => [
                    Deliberation::ETAT_ACTE_VOTE_ADOPTE,
                    Deliberation::ETAT_ACTE_VOTE_REJET,
                    Deliberation::ETAT_ACTE_SIGNATURE_SIGNEE
                ],
                'Deliberation.signee' => true
            ]
        ];

        $subQueryNonTransmissibles = $this->DeliberationSeance->sql($subQuery);
        $subQuery = ' "Deliberation"."id" IN (('
            . $subQueryTransmissibles
            . ') UNION ('
            . $subQueryNonTransmissibles . '))';
        $subQueryExpression = $db->expression($subQuery);
        $conditions[] = $subQueryExpression;

        $this->paginate = ['Deliberation' => [
            'countField' => 'DISTINCT Deliberation.id',
            'fields' => [
                'DISTINCT Deliberation.id',
                'Deliberation.objet',
                'Deliberation.objet_delib',
                'Deliberation.num_delib',
                'Deliberation.tdt_dateAR',
                'Deliberation.tdt_ar_date',
                'Deliberation.tdt_ar',
                'Deliberation.parapheur_id',
                'Deliberation.num_pref',
                'Deliberation.typologiepiece_code',
                'Deliberation.tdt_document_papier',
                'Deliberation.etat',
                'Deliberation.titre',
                'Deliberation.tdt_id',
                'Deliberation.pastell_id',
                'Deliberation.typeacte_id',
                'Deliberation.theme_id',
                'Deliberation.service_id',
                'Deliberation.circuit_id',
                'Deliberation.tdt_status',
                'Deliberation.tdt_message',
                'Deliberation.sae_etat',
                'Deliberation.sae_date',
                'Deliberation.sae_commentaire',
            ],
            'joins' => array_merge(
                [
                    $this->Deliberation->join('DeliberationSeance', ['type' => 'LEFT']),
                    $this->Deliberation->join('DeliberationTypeseance', ['type' => 'LEFT']),
                ],
                $this->handleJoins()
            ),
            'conditions' => $conditions,
            'contain' => [
                'Service' => ['fields' => ['name']],
                'Circuit' => ['fields' => ['nom']],
                'Theme' => ['fields' => ['libelle']],
                'Annexe' => [
                    'fields' => ['id', 'filename', 'filetype', 'typologiepiece_code'],
                    'conditions' => ['joindre_ctrl_legalite' => true],
                    'order' => ['position']
                ],
                'Typeacte' => ['fields' => ['name', 'teletransmettre']],
                'TdtMessage' => ['fields' => ['tdt_id', 'tdt_type', 'tdt_etat', 'parent_id'],
                    'conditions' => ['parent_id is null'],
                    'Reponse' => ['fields' => ['tdt_id', 'tdt_type', 'tdt_etat']]],
                'DeliberationTypeseance' => ['fields' => ['id'],
                    'Typeseance' => [
                        'fields' => ['id', 'libelle', 'color', 'action'
                        ],
                    ]],
                'DeliberationSeance' => [
                    'fields' => ['id'],
                    'Seance' => [
                        'fields' => ['id', 'date', 'type_id'],
                        'Typeseance' => [
                            'fields' => ['id', 'libelle', 'color', 'action'],
                        ],

                    ]]
            ],
            'order' => ['Deliberation.id' => 'DESC'],
            'limit' => 20,
            'allow' => ['typeacte_id'],
            'recursive' => -1]];

        // On affiche que les delibs vote pour.
        $deliberations = $this->Paginator->paginate('Deliberation');
        $this->ProjetTools->sortProjetSeanceDate($deliberations);

        $this->ajouterFiltreDeliberation($deliberations);
        if (!empty($seanceId)) {
            $this->Filtre->delCritere('DeliberationSeanceId');
            $this->Filtre->delCritere('DeliberationTypeseanceId');
            $this->set('seance_id', $seanceId);
        }

        $this->set('deliberations', $deliberations);
        $this->set('nomenclatures', $this->Nomenclature->generateTreeListWithOptionGroup());

        $this->render('send_to_sae');
    }

    /**
     * @access public
     * @return CakeResponse
     * @version 5.1.0
     * @since 4.0.0
     */
    public function verser($cookieTokenKey = null)
    {
        $this->Deliberation->Behaviors->load(
            'Version',
            $this->Deliberation->actsAsVersionOptionsList['Version']
        );

        if (!isset($this->data['traitement_lot']['action']) or empty($this->data['traitement_lot']['action'])) {
            $this->Flash->set(__('Veuillez sélectionner une action.'), [
                'element' => 'growl', 'params' => [
                    'type' => 'danger'
                ]]);

            return $this->redirect($this->previous);
        } else {
            $action = $this->data['traitement_lot']['action'];
        }

        if (isset($this->data['Deliberation']['check'])) {
            foreach ($this->data['Deliberation']['check'] as $tmp_id => $bool) {
                if ($bool) {
                    $delib_id = substr($tmp_id, 3, strlen($tmp_id));
                    $deliberationIds[] = $delib_id;
                }
            }
        } else {
            foreach ($this->data['Deliberation'] as $projet_id => $projet) {
                if (!empty($projet['is_checked']) && $projet['is_checked']) {
                    $deliberationIds[] = $projet_id;
                }
            }
        }
        if (!isset($deliberationIds) || (isset($deliberationIds) && count($deliberationIds) == 0)) {
            $this->Flash->set(__('Veuillez sélectionner un acte.'), [
                'element' => 'growl', 'params' => [
                    'type' => 'danger'
                ]]);
            return $this->redirect($this->previous);
        }
        $success = true;
        foreach ($deliberationIds as $deliberation_id) {
            $this->Deliberation->id = $deliberation_id;
            $this->Deliberation->recursive=-1;

            if ($action === 'verser') {
                $this->Deliberation->saveField(
                    'sae_date',
                    !empty($this->request->data['verser']['date_acte_all']) ? CakeTime::format(
                        str_replace('/', '-', $this->request->data['verser']['date_acte_all']),
                        '%Y-%m-%d 00:00:00'
                    ) : date('Y-m-d')
                );

                try {
                    $this->VersementSae->sendActe($deliberation_id);
                } catch (CakeException|Exception $e) {
                    $success = false;
                    $this->Flash->set($e->getMessage(), ['element' => 'growl', 'params' => ['type' => 'danger']]);
                    continue;
                }
                $this->Deliberation->clear();
            }

            if ($action === 'verser_manuellement') {
                $this->Deliberation->saveField(
                    'sae_date',
                    !empty($this->request->data['verser_manuellement']['date_acte_all']) ? CakeTime::format(
                        str_replace('/', '-', $this->request->data['verser_manuellement']['date_acte_all']),
                        '%Y-%m-%d 00:00:00'
                    ) : date('Y-m-d')
                );

                if ($this->Deliberation->saveField('sae_etat', SaeVersement::VERSER_MANUELLEMENT)) {
                    $this->Deliberation->setHistorique(
                        __('Acte déclaré versé manuellement dans le SAE'),
                        $deliberation_id,
                        null,
                        $this->Session->read('Auth.User.id')
                    );
                }
                $this->Deliberation->clear();
            }
        }

        if ($success!==true) {
            $this->Flash->set(__('Action effectuée avec des erreurs'), [
                'element' => 'growl', 'params' => [
                    'type' => 'warning'
                ]]);
            return $this->redirect($this->previous);
        }
        $this->Flash->set(__('Action effectuée avec succès'), ['element' => 'growl']);

        return $this->redirect($this->previous);
    }

    /**
     * @return array[]
     */
    private function handleJoins(): array
    {
        return [
            ['table' => 'deliberations_seances',
                'alias' => 'DeliberationSeance_Filter',
                'type' => 'LEFT',
                'conditions' => [
                    'Deliberation.id = DeliberationSeance_Filter.deliberation_id',
                ]
            ],
            ['table' => 'seances',
                'alias' => 'Seance_Filter',
                'type' => 'LEFT',
                'conditions' => [
                    'DeliberationSeance_Filter.seance_id = Seance_Filter.id'
                ]
            ],
            ['table' => 'deliberations_typeseances',
                'alias' => 'DeliberationTypeseance_Filter',
                'type' => 'LEFT',
                'conditions' => [
                    'Deliberation.id = DeliberationTypeseance_Filter.deliberation_id'
                ]
            ],
        ];
    }
}
