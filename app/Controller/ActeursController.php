<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('CakeTime', 'Utility');

/**
 * Class ActeursController
 *
 * @version 4.3
 * @package app.Controller
 */
class ActeursController extends AppController
{
    /**
     * @version 4.2
     * @access public
     * @var type
     */
    public $helpers = [];

    /**
     * @version 4.3
     * @access public
     * @var array
     */
    public $components = ['Paginator',
        'Auth' => [
            'mapActions' => [
                'create' => ['admin_add'],
                'read' => ['admin_index', 'admin_view'],
                'update' => ['admin_edit'],
                'delete' => ['admin_delete'],
                'allow' => ['view']
            ],
        ]
    ];

    /**
     * @version 4.2
     * @access public
     * @var array
     */
    public $uses = ['Acteur', 'Deliberation', 'Vote'];

    /**
     * @version 4.2
     * @access public
     * @var array
     */
    public $paginate = [
        'Acteur' => [
            'conditions' => ['Acteur.actif' => 1],
            'fields' => [
                'DISTINCT Acteur.id', 'Acteur.nom', 'Acteur.prenom',
                'Acteur.salutation', 'Acteur.telfixe', 'Acteur.telmobile',
                'Acteur.suppleant_id', 'Acteur.titre', 'Acteur.position',
                'Typeacteur.nom', 'Typeacteur.elu', 'Suppleant.nom', 'Suppleant.prenom'
            ],
            'limit' => 20,
            'joins' => [
                [
                    'table' => 'acteurs_services',
                    'alias' => 'ActeursServices',
                    'type' => 'LEFT',
                    'conditions' => ['ActeursServices.acteur_id = Acteur.id']
                ],
                [
                    'table' => 'services',
                    'alias' => 'Service',
                    'type' => 'LEFT',
                    'conditions' => ['Service.id = ActeursServices.service_id']
                ]
            ],
            'order' => ['Acteur.position' => 'asc']
        ]
    ];

    /**
     * @version 4.3
     * @access public
     */
    public function admin_index() // phpcs:ignore
    {
        $this->paginate = [
            'Acteur' => [
                'contain' => [
                    'Service',
                    'Suppleant',
                    'Typeacteur'],
                'conditions' => ['Acteur.actif' => true],
                'limit' => 20,
                'recursive' => -1,
                'order' => ['Acteur.position' => 'asc']
            ]];

        $this->Paginator->settings = $this->paginate;
        $acteurs = $this->Paginator->paginate('Acteur');
        foreach ($acteurs as &$acteur) {
            $acteur['Typeacteur']['elu'] = $this->Acteur->Typeacteur->libelleElu($acteur['Typeacteur']['elu']);
            $acteur['Acteur']['libelleOrdre'] = $this->Acteur->libelleOrdre($acteur['Acteur']['position']);
        }
        $this->set('acteurs', $acteurs);
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     */
    public function admin_view($id = null) // phpcs:ignore
    {
        $this->view_acteur($id);
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     */
    private function view_acteur($id = null) // phpcs:ignore
    {
        $acteur = $this->Acteur->find('first', [
            'conditions'=> [
                'Acteur.id' => $id
            ],
        ]);
        if (empty($acteur)) {
            $this->Flash->set(__('ID invalide pour l\'acteur'), ['element' => 'growl']);
            $this->redirect(['action' => 'index']);
        } else {
            $this->set('acteur', $acteur);
            $this->set('canEdit', $this->Acl->check(['User' => ['id' => $this->Auth->user('id')]], 'Acteurs', 'read'));
        }

        $this->render('admin_view');
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     */
    public function view($id = null)
    {
        $this->view_acteur($id);
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     */
    public function admin_add() // phpcs:ignore
    {
        if (!empty($this->data)) {
            if ($this->controleEtSauve()) {
                $this->Flash->set(
                    __(
                        'L\'acteur "%s %s" a été ajouté',
                        $this->data['Acteur']['prenom'],
                        $this->data['Acteur']['nom']
                    ),
                    ['element' => 'growl']
                );
                $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Veuillez corriger les erreurs ci-dessous :'),
                    ['element' => 'growl','params'=>['type' => 'danger']]
                );
            }
        }

        $this->Acteur->virtualFields['full_name'] = 'CONCAT(Acteur.prenom, \' \', Acteur.nom)';
        $acteurs = $this->Acteur->find('list', [
            'fields' => ['Acteur.id', 'Acteur.full_name'],
            'conditions' => ['Acteur.actif' => true],
            'recursive' => -1
        ]);
        $this->set('acteurs', $acteurs);
        $this->Acteur->Typeacteur->recursive = 0;
        $typeacteurs = $this->Acteur->Typeacteur->find('all', ['fields' => ['id', 'nom', 'elu']]);
        if (empty($typeacteurs)) {
            $this->Flash->set(
                __(
                    'Veuillez créer un type d\'acteur.'
                ),
                ['element' => 'growl','params' => ['type' => 'danger']]
            );
            $this->redirect(['controller' => 'typeacteurs', 'action' => 'add']);
        }
        $this->set('typeacteurs', Hash::combine(
            $typeacteurs,
            '{n}.Typeacteur.id',
            '{n}.Typeacteur.nom'
        ));
        $this->set('services', $this->Acteur->Service->generateTreeList(
            ['Service.actif' => '1'],
            null,
            null,
            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
        ));
        $this->set('selectedServices', null);

        $type_elus = [];
        foreach ($typeacteurs as $typeacteur) {
            if ($typeacteur['Typeacteur']['elu']) {
                $type_elus[] = $typeacteur['Typeacteur']['id'];
            }
        }
        $this->set('type_elus', implode(",", $type_elus));
        $this->set('notif', ['1' => __('oui'), '0' => __('non')]);
        $this->render('admin_edit');
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     */
    public function admin_edit($id = null) // phpcs:ignore
    {
        $acteur = $this->Acteur->find('first', ['conditions' => ['Acteur.id' => $id]]);

        if (empty($acteur)) {
            $this->Flash->set(__('ID d\'acteur invalide'), ['element' => 'growl']);
            $this->redirect($this->previous);
        }

        if ($this->request->is(['post', 'put'])) {
            if (!empty($this->request->data['Acteur']['typeacteur_id'])) {
                if ($this->Acteur->Typeacteur->field(
                    'elu',
                    ['id' => $this->request->data['Acteur']['typeacteur_id']]
                )
                ) {
                    // pour un élu : initialisation de 'position' si non définie
                    if (!$this->request->data['Acteur']['position']) {
                        $this->request->data['Acteur']['position'] = $this->Acteur->getPostionMaxParActeursElus() + 1;
                    }
                } else {
                    // pour un non élu : suppression des informations éventuellement saisies
                    if (array_key_exists('Service', $this->request->data)) {
                        $this->request->data['Service']['Service'] = [];
                    }
                    $this->request->data['Acteur']['position'] = 999;
                }
            }

            if ($this->Acteur->save($this->request->data)) {
                $this->Flash->set(
                    __(
                        'L\'acteur "%s %s" a été modifié',
                        $this->request->data['Acteur']['prenom'],
                        $this->request->data['Acteur']['nom']
                    ),
                    ['element' => 'growl']
                );
                $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Veuillez corriger les erreurs ci-dessous :'),
                    ['element' => 'growl','params'=>['type' => 'danger']]
                );
                if (array_key_exists('Service', $this->request->data)) {
                    $this->set('selectedServices', $this->request->data['Service']['Service']);
                } else {
                    $this->set('selectedServices', null);
                }
            }
        }

        if (!$this->request->data) {
            $this->request->data = $acteur;
        }

        $this->Acteur->virtualFields['full_name'] = 'CONCAT(Acteur.prenom, \' \', Acteur.nom)';
        $acteurs = $this->Acteur->find('list', [
            'fields' => ['Acteur.id', 'Acteur.full_name'],
            'conditions' => ['Acteur.actif' => true],
            'recursive' => -1
        ]);
        $this->set('acteurs', $acteurs);

        $this->set('selectedServices', $this->selectedArray($acteur['Service']));

        $typeacteurs = $this->Acteur->Typeacteur->find('all', [
            'fields' => ['id', 'nom', 'elu'],
            'recursive' => -1
        ]);
        $this->set('typeacteurs', Hash::combine($typeacteurs, '{n}.Typeacteur.id', '{n}.Typeacteur.nom'));
        $this->set('services', $this->Acteur->Service->generateTreeList(
            ['Service.actif' => '1'],
            null,
            null,
            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
        ));
        $type_elus = [];
        foreach ($typeacteurs as $typeacteur) {
            if (!empty($typeacteur['Typeacteur']['elu']) && $typeacteur['Typeacteur']['elu'] === true) {
                $type_elus[] = $typeacteur['Typeacteur']['id'];
            }
        }
        $this->set('notif', ['1' => __('oui'), '0' => __('non')]);
        $this->set('type_elus', implode(",", $type_elus));
    }

    /**
     * @version 4.3
     * @access public
     * @return type
     */
    private function controleEtSauve() // phpcs:ignore
    {
        if (!empty($this->request->data['Acteur']['typeacteur_id'])) {
            if ($this->Acteur->Typeacteur->field('elu', 'id = ' . $this->request->data['Acteur']['typeacteur_id'])) {
                // pour un élu : initialisation de 'position' si non définie
                if (!$this->request->data['Acteur']['position']) {
                    $this->request->data['Acteur']['position'] = $this->Acteur->getPostionMaxParActeursElus() + 1;
                }
            } else {
                // Non élu : suppression des informations saisies (service, position, date naissance)
                if (array_key_exists('Service', $this->request->data)) {
                    $this->request->data['Service']['Service'] = [];
                }
                $this->request->data['Acteur']['position'] = 999;
            }
        }
        return $this->Acteur->save($this->request->data);
    }

    /**
     * Dans le controleur car utilisé dans la vue index pour l'affichage
     *
     * @version 4.3
     * @access public
     * @param type $acteur
     * @param string $message
     * @return boolean
     */
    private function _isDeletable($acteur, &$message) // phpcs:ignore
    {
        if ($this->Deliberation->find('count', ['conditions' => ['rapporteur_id' => $acteur['Acteur']['id']]])) {
            $message = __(
                'L\'acteur "%s %s" ne peut pas être supprimé car il est le rapporteur de délibérations',
                $acteur['Acteur']['prenom'],
                $acteur['Acteur']['nom']
            );
            return false;
        }
        if ($this->Vote->find('count', ['conditions' => ['acteur_id' => $acteur['Acteur']['id']]])) {
            $message = __(
                'L\'acteur "%s %s" ne peut pas être supprimé car il est le rapporteur de délibérations',
                $acteur['Acteur']['prenom'],
                $acteur['Acteur']['nom']
            );
            return false;
        }
        return true;
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     *
     */
    public function admin_delete($id = null) // phpcs:ignore
    {
        $acteur = $this->Acteur->find('first', [
            'conditions' => ['Acteur.id' => $id],
            'fields' => ['id', 'nom', 'prenom'],
            'recursive' => -1]);

        if (empty($acteur)) {
            $this->Flash->set(__('ID d\'acteur invalide'), ['element' => 'growl']);
        } else {
            $this->Flash->set(
                __(
                    'L\'acteur "%s %s" a été supprimé',
                    $acteur['Acteur']['prenom'],
                    $acteur['Acteur']['nom']
                ),
                ['element' => 'growl','params'=> ['type' => 'success']]
            );
            $this->Acteur->id = $id;
            $this->Acteur->saveField('actif', 0);
        }
        $this->redirect($this->previous);
    }

    /**
     * @version 4.3
     * @access public
     */
    public function beforeFilter()
    {
        parent::beforeFilter();

        /* $this->Auth->mapActions(array(
          'create' => array('add'),
          'view' => array('index', 'view')
          )); */
    }
}
