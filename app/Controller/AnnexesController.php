<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [AnnexesController description]
 *
 * @version 5.1.0
 * @since 4.3.0
 */
class AnnexesController extends AppController
{
    public $uses = ['Annexe'];
    public $components = [
        'Conversion',
        'Auth' => [
            'mapActions' => [
                'allow' => ['download', 'delete', 'downloadTampon']
            ]
        ]];

    /**
     * @version 4.3
     * @access public
     * @param type $id
     * @param type $type
     * @return type
     */
    public function download($id = null, $type = '', $isRevision = false)
    {
        App::uses('AppTools', 'Lib');

        if ($isRevision !== false) {
            $annexe = $this->Annexe->version($isRevision);
            $content =$annexe['annexes_versions']['data'] ;
            $typemime = $annexe['annexes_versions']['data_version']['Annexe']['filetype'];
            $filename = $annexe['annexes_versions']['data_version']['Annexe']['filename'];
        } else {
            $annexe = $this->Annexe->find('first', [
                'fields' => ['data', 'edition_data', 'data_pdf', 'filename', 'filetype', 'tdt_data_edition'],
                'conditions' => ['id' => $id],
                'recursive' => -1
            ]);

            switch ($type) {
                case 'tdt_data_edition':
                    $content = $annexe['Annexe']['tdt_data_edition'];
                    $filename = 'tdt_data_edition.odt';
                    $typemime = 'application/vnd.oasis.opendocument.text';
                    break;

                case 'edition_data':
                    $content = $annexe['Annexe']['edition_data'];
                    $filename = 'edition_data.odt';
                    $typemime = 'application/vnd.oasis.opendocument.text';
                    break;

                case 'pdf':
                    $content = !empty($annexe['Annexe']['data_pdf']) ? $annexe['Annexe']['data_pdf']
                        : $annexe['Annexe']['data'];
                    $filename = AppTools::getNameFile($annexe['Annexe']['filename']) . '.pdf';
                    $typemime = 'application/pdf';
                    break;

                default:
                    $content = $annexe['Annexe']['data'];
                    $filename = $annexe['Annexe']['filename'];
                    $typemime = $annexe['Annexe']['filetype'];
                    break;
            }
        }

        $this->response->disableCache();
        $this->response->body($content);
        $this->response->type($typemime);
        $this->response->download($filename);

        return $this->response;
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     * @return type
     */
    public function downloadTampon($id = null)
    {
        App::uses('AppTools', 'Lib');
        $annexe = $this->Annexe->find('first', [
            'fields' => ['data_pdf', 'filename', 'filetype', 'tdt_data_pdf', 'data'],
            'conditions' => ['id' => $id],
            'recursive' => -1
        ]);

        $docs_type = Configure::read('DOC_TYPE');
        if ($docs_type[$annexe['Annexe']['filetype']]['convertir']
            || $annexe['Annexe']['filetype'] === 'application/pdf'
        ) {
            if (empty($annexe['Annexe']['tdt_data_pdf'])) {
                $this->Flash->set(
                    __('L\'annexe tamponnée n\'est pas encore disponible'),
                    ['element' => 'growl','params'=>['type' => 'info']]
                );
                $this->redirect($this->previous);
            }
            $content = $annexe['Annexe']['tdt_data_pdf'];
            $filename = AppTools::getNameFile($annexe['Annexe']['filename']) . '.pdf';
            $typemime = 'application/pdf';
        } else {
            $content = $annexe['Annexe']['data'];
            $filename = $annexe['Annexe']['filename'];
            $typemime = $annexe['Annexe']['filetype'];
        }

        $this->response->disableCache();
        $this->response->body($content);
        $this->response->type($typemime);
        $this->response->download($filename);

        return $this->response;
    }
}
