<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class InfosupdefsController extends AppController
{
    public $uses = ['Infosupdef', 'Profil'];
    public $helpers = [];
    public $components = ['Filtre',
        'Auth' => [
            'mapActions' => [
                'read' => [
                    'admin_index',
                    'admin_add',
                    'admin_edit',
                    'admin_delete',
                    'admin_view',
                    'changeOrder',
                    'admin_disable',
                    'admin_enable'
                ]
            ]
        ]
    ];

    public $geojsonApi =
        [
            'api' => 'https://api-adresse.data.gouv.fr/search',
            'api_type' => 'housenumber',
            'api_query' => 'q',
            'api_limit' => 'limit=5'
        ];

    public $geojsonLabels =
        [
            'label',
            'housenumber',
            'type',
            'name',
            'postcode',
            'citycode',
            'x',
            'y',
            'city',
            'street'
        ];

    /**
     * @version 4.3
     */
    public function beforeFilter()
    {
        $this->set('Infosupdef', $this->Infosupdef);
        parent::beforeFilter();
    }

    /**
     * @version 5.1
     * @since 4.3
     * @param type $model
     */
    public function admin_index($model) // phpcs:ignore
    {
        $this->Filtre->initialisation(
            $this->name . ':' . $this->request->action.'_'.$model,
            $this->request->data,
            [
                'url' => [
                    'admin' => true,
                    'prefix' => 'admin',
                    'controller' => $this->request->params['controller'],
                    'action' => $this->request->params['action'],
                    $model
                ]]
        );
        if (!$this->Filtre->critereExists()) {
            $this->Filtre->addCritere('Actif', [
                'field' => 'Infosupdef.actif',
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Actif'),
                    'data-placeholder' => __('Sélectionner un état'),
                    'data-allow-clear' => true,
                    'options' => [1 => 'Oui', 0 => 'Non']]]);
            $this->Filtre->setCritere('Actif', 1);
        }
        $conditions = $this->Filtre->conditions();

        $conditions['AND']['model'] = $model;
        $infosupdefs = $this->request->data = $this->Infosupdef->find('all', [
            'recursive' => -1,
            'conditions' => $conditions,
            'order' => 'ordre']);

        $this->set(
            'titre',
            __(
                'Informations supplémentaires de %s',
                $model == 'Seance' ? __('séance') : __('projet')
            )
        );

        $this->set('lienAdd', ['admin' => true,
            'prefix' => 'admin',
            'controller' => 'infosupdefs',
            'action' => 'add', $model]);

        //Tableau de correspondance.
        $aPosition=[];
        foreach ($infosupdefs as $infosupdef) {
            $aPosition[$infosupdef['Infosupdef']['ordre']] = $infosupdef['Infosupdef']['ordre'];
        }
        $this->set('aPosition', $aPosition);
        $this->set('infosupdefs', $infosupdefs);
        $this->set(
            'modifiableApresSignature',
            ($model == 'Deliberation' ? __('Modifiable après signature') : null)
        );
    }

    /**
     * @version 4.3
     * @param type $id
     */
    public function admin_view($id = null) // phpcs:ignore
    {
        $this->request->data = $this->{$this->modelClass}->findById($id, null, null, -1);
        if (empty($this->data)) {
            $this->Flash->set(
                __('ID invalide pour l\'information supplémentaire : édition impossible'),
                ['element' => 'growl']
            );
            $this->redirect($this->previous);
        } else {
            $this->request->data['Infosupdef']['libelleType'] =
                $this->Infosupdef->libelleType($this->data['Infosupdef']['type']);
            $this->request->data['Infosupdef']['libelleRecherche'] =
                $this->Infosupdef->libelleRecherche($this->data['Infosupdef']['recherche']);
            $this->request->data['Infosupdef']['libelleActif'] =
                $this->Infosupdef->libelleActif($this->data['Infosupdef']['actif']);
            $this->set(
                'titre',
                __(
                    'Détails de l\'information supplémentaire de %s',
                    $this->data['Infosupdef']['model'] == 'Deliberation' ? 'projet' : 'séance'
                )
            );
        }
    }

    /**
     * @version 4.3
     * @param type $model
     * @return type
     */
    public function admin_add($model) // phpcs:ignore
    {
        // initialisations
        $codePropose = '';

        $this->request->data['Infosupdef']['model'] = $model;

        if ($this->request->is('Post')) {
            // traitement de la valeur par defaut
            if ($this->request->data['Infosupdef']['type'] == 'date') {
                $this->request->data['Infosupdef']['val_initiale'] =
                    $this->request->data['Infosupdef']['val_initiale_date'];
            } elseif ($this->request->data['Infosupdef']['type'] == 'boolean') {
                $this->request->data['Infosupdef']['val_initiale'] =
                    $this->request->data['Infosupdef']['val_initiale_boolean'];
            } elseif ($this->request->data['Infosupdef']['type'] == 'file') {
                $this->request->data['Infosupdef']['val_initiale'] = '';
            }

            if ($this->request->data['Infosupdef']['type'] == 'geojson') {
                $jsonData = [];
                // Récupérer dans correlation !
                // Mettre dans corrélation !
                foreach ($this->geojsonLabels as $name) {
                    $jsonData[$name] = $this->request->data['Infosupdef']['geojson'][$name];
                }
                $this->request->data['Infosupdef']['correlation'] = json_encode($jsonData);

                $this->request->data['Infosupdef']['geojson'] = '';
            } else {
                $this->request->data['Infosupdef']['geojson'] = '';
            }

            if ($this->{$this->modelClass}->save($this->request->data)) {
                $this->Flash->set(
                    __(
                        'L\'information supplémentaire \'%s\' a été ajoutée',
                        $this->request->data['Infosupdef']['nom']
                    ),
                    ['element' => 'growl']
                );
                return $this->redirect($this->previous);
            } else {
                $this->Flash->set(__('Veuillez corriger les erreurs ci-dessous.'), ['element' => 'growl']);
                $codePropose = Inflector::variable($this->request->data['Infosupdef']['code']);
                if (!empty($this->request->data['Infosupdef']['geojson'])) {
                    $this->set('geojson', json_decode($this->request->data['Infosupdef']['geojson'], true));
                }
            }
        }

        $this->set(
            'titre',
            __(
                'Ajout d\'une information supplémentaire de %s',
                $model == 'Deliberation' ? 'projet' : 'séance'
            )
        );
        $this->set('types', $this->{$this->modelClass}->generateListType());
        App::uses('AppTools', 'Lib');

        $this->set('geojsonLabels', $this->geojsonLabels);
        $this->set(
            'defaults',
            [
                'geojson' => (array)Configure::read('Infosupdef.geojson.defaults')
                    + $this->geojsonApi
                    + array_combine($this->geojsonLabels, $this->geojsonLabels)
            ]
        );

        $this->set('typemimes', $this->getTypemimesFilesList());
        $this->set('typemimes_fusion', $this->getTypemimesFilesForFusionList());
        $this->set('listEditBoolean', $this->{$this->modelClass}->listEditBoolean);
        $this->set('codePropose', $codePropose);
        $this->set('profils', $this->Profil->find('list', ['conditions' => ['Profil.actif' => 1]]));
        $this->render('admin_edit');
    }

    /**
     * [getTypemimesFilesList description]
     * @return [type] [description]
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    private function getTypemimesFilesList()
    {
        $typemimes = AppTools::getTypemimesFiles();
        return Hash::combine(
            $typemimes,
            '{n}.typemime',
            ['%s, %s (%s)', '{n}.extension', '{n}.formatname', '{n}.typemime']
        );
    }

    /**
     * [getTypemimesFilesForFusionList description]
     * @return [type] [description]
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    private function getTypemimesFilesForFusionList()
    {
        $typemimes = AppTools::getTypemimesFilesForFusion();
        return Hash::combine(
            $typemimes,
            '{n}.typemime',
            ['%s, %s (%s)', '{n}.extension', '{n}.formatname', '{n}.typemime']
        );
    }

    /**
     * @version 4.3
     * @param int $id
     */
    public function admin_edit($id) // phpcs:ignore
    {
        // initialisations
        $codePropose = '';

        if (empty($this->request->data)) {
            $this->request->data = $this->{$this->modelClass}->find('first', [
                'contain' => ['Profil'],
                'conditions' => ["Infosupdef.id" => $id]]);
            if (empty($this->request->data)) {
                $this->Flash->set(
                    __('ID invalide pour l\'information supplémentaire : édition impossible'),
                    ['element' => 'growl']
                );
            }

            // traitement de la valeur par défaut pour les dates et les booléens
            if ($this->request->data['Infosupdef']['type'] == 'date') {
                $this->request->data['Infosupdef']['val_initiale_date'] =
                    $this->request->data['Infosupdef']['val_initiale'];
            } elseif ($this->request->data['Infosupdef']['type'] == 'boolean') {
                $this->request->data['Infosupdef']['val_initiale_boolean'] =
                    $this->request->data['Infosupdef']['val_initiale'];
            } elseif ($this->request->data['Infosupdef']['type'] == 'geojson') {
                if (!empty($this->request->data['Infosupdef']['geojson'])) {
                    $this->set('geojson', json_decode($this->request->data['Infosupdef']['geojson'], true));
                }
            }
        } else {
            // traitement de la valeur par défaut
            if ($this->request->data['Infosupdef']['type'] == 'date') {
                $this->request->data['Infosupdef']['val_initiale'] =
                    $this->request->data['Infosupdef']['val_initiale_date'];
            } elseif ($this->request->data['Infosupdef']['type'] == 'boolean') {
                $this->request->data['Infosupdef']['val_initiale'] =
                    $this->request->data['Infosupdef']['val_initiale_boolean'];
            } elseif ($this->request->data['Infosupdef']['type'] == 'file') {
                $this->request->data['Infosupdef']['val_initiale'] = '';
            }
            if ($this->request->data['Infosupdef']['type'] == 'geojson') {
                $jsonData = [];
                foreach ($this->geojsonLabels as $name) {
                    $jsonData[$name] = $this->request->data['Infosupdef']['geojson'][$name];
                }
                $this->request->data['Infosupdef']['correlation'] = json_encode($jsonData);
            } else {
                $this->request->data['Infosupdef']['geojson'] = '';
            }

            if ($this->{$this->modelClass}->save($this->request->data)) {
                $this->Flash->set(
                    __(
                        'L\'information supplémentaire "%s" a été modifiée',
                        $this->request->data['Infosupdef']['nom']
                    ),
                    ['element' => 'growl']
                );
                $this->redirect($this->previous);
            } else {
                $this->Flash->set(__('Veuillez corriger les erreurs ci-dessous.'), ['element' => 'growl']);
                $codePropose = Inflector::variable($this->request->data['Infosupdef']['code']);
                if (!empty($this->request->data['Infosupdef']['geojson'])) {
                    $this->set('geojson', json_decode($this->request->data['Infosupdef']['geojson'], true));
                }
            }
        }

        $this->set(
            'titre',
            __(
                'Modifier une information supplémentaire de %s',
                $this->request->data['Infosupdef']['model'] === 'Deliberation' ? 'projet' : 'séance'
            )
        );
        $this->set('types', $this->{$this->modelClass}->generateListType());
        App::uses('AppTools', 'Lib');
        $this->set('geojsonLabels', $this->geojsonLabels);
        $this->set('typemimes', $this->getTypemimesFilesList());
        $this->set('typemimes_fusion', $this->getTypemimesFilesForFusionList());
        $this->set('listEditBoolean', $this->{$this->modelClass}->listEditBoolean);
        $this->set('codePropose', $codePropose);
        $this->set('selectedTypes', $this->{$this->modelClass}->generateResultType());
        $this->set('profils', $this->Profil->find('list', ['conditions' => ['Profil.actif' => 1]]));
    }

    /**
     * @version 4.3
     * @param int $id
     */
    public function admin_delete($id = null) // phpcs:ignore
    {
        $data = $this->{$this->modelClass}->find('first', [
            'recursive' => -1,
            'conditions' => ['id' => $id]]);
        if (empty($data)) {
            $this->Flash->set(
                __('ID invalide pour l\'information supplémentaire : suppression impossible'),
                ['element' => 'growl']
            );
        } elseif (!$this->{$this->modelClass}->isDeletable($id)) {
            $this->Flash->set(
                __('Cette information supplémentaire ne peut pas être supprimée'),
                ['element' => 'growl']
            );
        } elseif ($this->{$this->modelClass}->delete($id)) {
            $this->{$this->modelClass}->Infosuplistedef->delList($id);
            $this->Flash->set(
                __('L\'information supplémentaire "%s" a bien été supprimée', $data['Infosupdef']['nom']),
                ['element' => 'growl']
            );
        }

        $this->redirect($this->previous);
    }

    /**
     * Allow to change the order of the selected infosup
     * @version 5.1
     * @param type $id
     * @param type $suivant
     */
    public function changeOrder($id, $new_position = true, $model) // phpcs:ignore
    {
        $infoSup = $this->{$this->modelClass}->find('first', [
            'recursive' => -1,
            'fields' => ['id', 'model','ordre'],
            'conditions' => ['id' => $id,'model'=>$model]]);

        $old_position = $infoSup['Infosupdef']['ordre'];
        if ($new_position < $old_position) {
            $delta = 1;
            $start = $new_position;
            $end = $old_position - 1;
        } else {
            $delta = -1;
            $start = $old_position + 1;
            $end = $new_position;
        }

        if (empty($infoSup)) {
            $this->Flash->set(__('ID invalide : déplacement impossible.'), ['element' => 'growl']);
        } else {
            $this->{$this->modelClass}->updateAll(
                [
                  'Infosupdef.ordre' => "Infosupdef.ordre+$delta",
                ],
                [
                  "Infosupdef.ordre >= " => $start,
                  'model' => $model,
                  "Infosupdef.ordre <= " => $end
                ]
            );
            $this->{$this->modelClass}->clear();
            $this->{$this->modelClass}->id = $infoSup['Infosupdef']['id'];
            $this->{$this->modelClass}->saveField('ordre', $new_position);
        }

        $this->redirect($this->previous);
    }

    /**
     * Activer une information suopplémentaire
     * @version v5.1
     * @access public
     * @param type $id
     */
    public function admin_enable($id, $model) // phpcs:ignore
    {
        $this->toggleActiveInfoSuppDef($id, $model, true);
    }

    /**
     * Désactiver une information suopplémentaire
     * @version v5.1
     * @access public
     * @param type $id
     */
    public function admin_disable($id, $model) // phpcs:ignore
    {
        $this->toggleActiveInfoSuppDef($id, $model, false);
    }

    /**
     * Active ou desactive un compte utilisateur
     * @version v5.1
     * @access private
     * @param int $id
     * @param boolean $enabled
     */
    private function toggleActiveInfoSuppDef($id, $model, $enabled = false)
    {
        $infoSupp = $this->Infosupdef->find('first', [
            'conditions' => ['Infosupdef.id' => $id],
            'fields' => ['id', 'Infosupdef.actif','nom'],
            'recursive' => -1]);
        if (empty($infoSupp)) {
            $this->Flash->set(__('ID invalide pour l\'information supplémentaire'), ['element' => 'growl']);
        } else {
            if ($this->Infosupdef->save([
              'id'=>$id,
              'model' => $model,
              'actif'=> $enabled
              ])) {
                $this->Flash->set(
                    __(
                        'L\'information supplémentaire "%s" a été %s',
                        $infoSupp['Infosupdef']['nom'],
                        ($enabled) ? 'activée' : 'désactivée'
                    ),
                    ['element' => 'growl','params'=> ['type' => 'warning']]
                );
            }
        }

        $this->redirect($this->previous);
    }
}
