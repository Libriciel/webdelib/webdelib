<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('File', 'Utility');

/**
 * Collectivites Controller
 *
 * @package app.Controller.Collectivites
 * @version 5.2.0
 * @since   4.0.0
 */
class CollectivitesController extends AppController
{
    /**
     * @version 4.3
     * @access public
     * @var array
     */
    public $uses = ['Collectivite', 'User', 'Infosupdef'];

    /**
     * @version 4.3
     * @access public
     * @var array
     */
    public $components = [
        'Auth' => [
            'mapActions' => [
                'allow' => ['rgpd'],
                'read' => ['admin_index'],
                'create' => ['admin_add'],
                'update' => ['admin_edit', 'setMails', 'admin_edit9Cases', 'admin_login', 'admin_rgpd']
            ],
        ]
    ];

    /**
     * [admin_index description]
     * @version 5.1.0
     * @version 1.0.0
     * @return [type] [description]
     */
    public function admin_index() // phpcs:ignore
    {
        $collectivite = $this->Collectivite->find('first', [
          'conditions' => ['Collectivite.id' => 1],
          'recursive' => -1]);
        $this->set('collectivite', $collectivite);

        $file = new File(APP . WEBROOT_DIR . DS . 'files' . DS . 'image' . DS . 'logo');
        if (!$file->exists()) {
            $this->set('logo_path', $this->base . "/files/image/Libriciel_SCOP.png");
        } else {
            $this->set('logo_path', $this->base . "/files/image/logo");
        }
        $file->close();
    }

    /**
     * [admin_edit description]
     * @version 5.1.0
     * @version 1.0.0
     * @return [type] [description]
     */
    public function admin_edit() // phpcs:ignore
    {
        if ($this->request->is('Post') || $this->request->is('Put')) {
            $this->Collectivite->id = 1;
            if ($this->Collectivite->save($this->request->data)) {
                $this->Flash->set(__('La collectivité a été modifiée'), ['element' => 'growl']);

                $this->redirect($this->previous);
            } else {
                $this->Flash->set(__('Veuillez corriger les erreurs ci-dessous.'), [
                  'element' => 'growl',
                  'params'=>['type' => 'danger']]);
            }
        }

        if (!$this->request->data) {
            $this->Collectivite->recursive = -1;
            $this->request->data = $this->Collectivite->read(null, 1);
        }

        $this->set('use_pastell', Configure::read('USE_PASTELL'));
    }

    /**
     * @version 4.3
     * @access public
     * @return type
     */
    public function setMails()
    {
        $path = APP . DS . 'Config' . DS . 'emails/';
        $this->set('email_path', $path);

        if (!empty($this->data)) {
            $cpt = 1;
            foreach ($this->data['Mail'] as $mail) {
                if ($cpt == 1) {
                    if ($mail['name'] == '') {
                        continue;
                    }
                    $name_file = 'refus';
                    $tmp_file = $mail['tmp_name'];
                    if (!move_uploaded_file($tmp_file, $path . $name_file)) {
                        exit(__("Impossible de copier le fichier"));
                    }
                }
                if ($cpt == 2) {
                    if ($mail['name'] == '') {
                        continue;
                    }
                    $name_file = 'traiter';
                    $tmp_file = $mail['tmp_name'];
                    if (!move_uploaded_file($tmp_file, $path . $name_file)) {
                        exit(__("Impossible de copier le fichier"));
                    }
                }
                if ($cpt == 3) {
                    if ($mail['name'] == '') {
                        continue;
                    }
                    $name_file = 'insertion';
                    $tmp_file = $mail['tmp_name'];
                    if (!move_uploaded_file($tmp_file, $path . $name_file)) {
                        exit(__("Impossible de copier le fichier"));
                    }
                }
                if ($cpt == 4) {
                    if ($mail['name'] == '') {
                        continue;
                    }
                    $name_file = 'convocation';
                    $tmp_file = $mail['tmp_name'];
                    if (!move_uploaded_file($tmp_file, $path . $name_file)) {
                        exit(__("Impossible de copier le fichier"));
                    }
                }
                $cpt++;
            }
            return $this->redirect(['action' => 'admin_index']);
        }
    }

    /**
     * Recupere le tableau des differentes options d'affichage pour les selecteurs
     *
     * @version 5.1
     * @since 4.3
     * @access private
     * @return type
     */
    private function selecteur9Cases()
    {
        $selecteur = [
            'Deliberation.objet' => __('Libellé du projet'),
            'Deliberation.titre' => __('Titre du projet'),
            'Typeacte.name' => __('Type d\'acte'),
            'Theme.libelle' => __('Thème'),
            'Deliberation.num_pref' => __('Classification'),
            'Seance.libelle' => __('Séance(s)'),
            'Deliberation.annexes' => __('Annexe(s)'),
            'Deliberation.commentaires' => __('Commentaire(s)'),
            'Service.libelle' => __('Service émetteur'),
            'Deliberation.rapporteur_principal' => __('Rapporteur'),
            'Deliberation.date_limite' => __('A traiter avant le'),
        ];

        $selecteur['Circuit'] = [
            'Circuit.nom' => __('Nom du circuit'),
            'Circuit.last_viseur' => __('Dernière action de'),
            'Circuit.current_step' => __('Étape courante')];

        //infos supplémentaires
        $this->Infosupdef->virtualFields['nomType'] = 'Infosupdef.nom || \' (\' || Infosupdef.type ||\')\'';
        $this->Infosupdef->virtualFields['idInfosup'] = 'Infosupdef.id';
        $infosupList = $this->Infosupdef->find('list', [
            'fields' => ['idInfosup', 'nomType'],
            'conditions' => ['actif' => true, 'model' => 'Deliberation'],
            'recursive' => -1,
            'order' => 'nom'
        ]);
        $children = [];
        foreach ($infosupList as $key => $value) {
            $children["Infosupdef.id.$key"] = $value;
        }
        $selecteur['Informations supplementaires'] = $children;

        return $selecteur;
    }

    /**
     * @version 5.1
     * @since 4.3
     * @access public
     * @param type $opt
     */
    public function admin_edit9Cases($opt = null) // phpcs:ignore
    {
        $this->edit9Cases($opt);
    }

    /**
     * Affichage de l'editeur des 9 cases pour l'ensemble des projets
     *
     * @version 5.1
     * @since 4.3
     * @access private
     * @param type $opt --> Appel en méthode $_GET
     */
    private function edit9Cases($opt = null)
    {
        //Sauvegarde des 9 cases
        if ($this->request->is('post')) {
            $this->Collectivite->id = 1;
            $templateProjectPost = array_values($this->request->data);

            if ($this->Collectivite->saveField('templateProject', json_encode($templateProjectPost))) {
                $this->Session->write('Collectivite.templateProject', $templateProjectPost);
                $this->Flash->set(
                    __('La modification a été correctement sauvegardée'),
                    ['element' => 'growl','params'=>['type' => 'info']]
                );
                $templateProject = $templateProjectPost;
                unset($templateProjectPost);
            } else {
                $this->Flash->set(
                    __('La modification n\'a pas été sauvegardée'),
                    ['element' => 'growl','params'=>['type' => 'danger']]
                );
            }
        } elseif ($opt == 'revertModification') {
            //On remet les valeurs par defaut des 9 cases
            $templateProject = $this->Collectivite->getJson9Cases(); //valeur pas défaut

            $this->Collectivite->id = 1;
            if ($this->Collectivite->saveField('templateProject', $templateProject)) {
                $templateProject = json_decode($templateProject, true);
                $this->Session->write('Collectivite.templateProject', $templateProject);
                $this->Flash->set(
                    __('La restauration a été correctement effectuée'),
                    ['element' => 'growl','params'=>['type' => 'success']]
                );
            } else {
                $this->Flash->set(
                    __('La restauration a échoué'),
                    ['element' => 'growl','params'=>['type' => 'danger']]
                );
            }
        } elseif ($this->Session->check('Collectivite.templateProject')) {
            //On affiche l'enregistrement existant en Session
            $templateProject = $this->Session->read('Collectivite.templateProject');
        }

        //on construit les options et on affiche la valeur par defaut pour chaque select
        $this->set('selecteurProject', $this->selecteur9Cases());
        $this->set('templateProject', $templateProject);

        $this->render('edit9Cases');
    }

    /**
     * [admin_login description]
     * @return [type] [description]
     *
     * @version 5.2
     */
    public function admin_login() // phpcs:ignore
    {
        if ($this->request->is('post')) {
            try {
                if (!empty($this->request->data)) {
                    $this->request->data['showCustom'] = filter_var(
                        $this->request->data['showCustom'],
                        FILTER_VALIDATE_BOOLEAN
                    );
                    $this->request->data['showInformation'] = filter_var(
                        $this->request->data['showInformation'],
                        FILTER_VALIDATE_BOOLEAN
                    );

                    $this->Collectivite->id = 1;
                    $this->Collectivite->saveField(
                        'config_login',
                        json_encode(
                            $this->request->data,
                            JSON_HEX_QUOT | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_TAG
                        )
                    );

                    return new CakeResponse([
                      'body'=> json_encode([
                          'val' => __('La configuration de la page de connexion a été enregistrée')
                      ]),
                      'status' => 200
                    ]);
                } else {
                    throw new Exception(
                        __(
                            'Une erreur est survenue lors de l\'enregistrement. Merci de contacter votre administrateur'
                        ),
                        1
                    );
                }
            } catch (Exception $e) {
                return new CakeResponse([
                  'body'=> json_encode([
                      'val' => $e->getMessage()
                  ]),
                  'status' => 500
                ]);
            }
        }

        $this->Collectivite->recursive = -1;
        $collectivite = $this->Collectivite->read(['config_login'], 1);


        $this->set('loginConfig', $collectivite['Collectivite']['config_login']);
    }

    /**
     * @return void
     *
     * @version 7.0.0
     */
    public function admin_rgpd() // phpcs:ignore
    {
        if (!empty($this->data)) {
            $this->Collectivite->id = 1;
            if ($this->Collectivite->save($this->request->data)) {
                $this->Flash->set(
                    __('Les informations liées à la confidentialité ont bien été ajoutées.'),
                    ['element' => 'growl']
                );
                return $this->redirect($this->previous);
            } else {
                $this->Flash->set(
                    __('Veuillez corriger les erreurs ci-dessous.'),
                    ['element' => 'growl','params'=>['type' => 'danger']]
                );
            }
        }

        if (!$this->request->data) {
            $this->Collectivite->recursive = -1;
            $this->request->data = $this->Collectivite->read(null, 1);
        }

        $this->render('admin_rgpd');
    }

    /**
     * @return void
     *
     * @version 7.0.0
     */
    public function rgpd()
    {
        $collectivite = $this->Collectivite->find('first', [
            'conditions' => ['Collectivite.id' => 1],
        ]);

        $this->set('collectivite', $collectivite['Collectivite']);

        $this->render('rgpd_view');
    }
}
