<?php

/**
 * Contrôleur des Séquances
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       web-delib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 * @version     v4.3
 * @package     app.Controller
 */
class SequencesController extends AppController
{
    public $name = 'Sequences';
    public $components = [
        'Auth' => [
            'mapActions' => [
                'create' => ['admin_add'],
                'read' => ['admin_index', 'admin_view'],
                'update' => ['admin_edit'],
                'delete' => ['admin_delete'],
            ]
        ]
    ];

    /**
     * @version 4.3
     * @access public
     */
    public function admin_index() // phpcs:ignore
    {
        $this->set('sequences', $this->Sequence->find('all', ['recursive' => 1]));

        $this->render('index');
    }

    /**
     * @version 4.3
     * @access public
     * @param type $id
     */
    public function admin_view($id) // phpcs:ignore
    {
        if (!$this->Sequence->exists($id)) {
            $this->Flash->set(
                __('ID invalide pour la séquence'),
                ['element' => 'growl', 'params'=>['type' => 'danger']]
            );
            $this->redirect($this->previous);
        } else {
            $this->set('sequence', $this->Sequence->find('first', [
                'conditions'=> [
                    'Sequence.id' => $id
                ],
            ]));
        }

        $this->render('view');
    }

    /**
     * @access public
     */
    public function admin_add() // phpcs:ignore
    {
        $sortie = false;
        if (!empty($this->data)) {
            $this->Sequence->create($this->request->data);
            if ($this->Sequence->save()) {
                $this->Flash->set(
                    __('La séquence "%s" a été ajoutée', $this->data['Sequence']['nom']),
                    ['element' => 'growl']
                );
                $sortie = true;
            } else {
                $this->Flash->set(
                    __('Veuillez corriger les erreurs ci-dessous.'),
                    ['element' => 'growl', 'params'=>['type' => 'danger']]
                );
            }
        }
        if ($sortie) {
            $this->redirect($this->previous);
        } else {
            $this->render('edit');
        }
    }

    /**
     * @access public
     * @param type $id
     */
    public function admin_edit($id) // phpcs:ignore
    {
        $sortie = false;
        if (empty($this->data)) {
            $this->data = $this->Sequence->find('first', [
                'conditions'=> [
                    'Sequence.id' => $id
                ],
            ]);
            if (empty($this->data)) {
                $this->Flash->set(
                    __('ID invalide pour la séquence'),
                    ['element' => 'growl', 'params'=>['type' => 'danger']]
                );
                $sortie = true;
            }
        } else {
            if ($this->Sequence->save($this->data)) {
                $this->Flash->set(
                    __('La séquence "%s" a été modifiée', $this->data['Sequence']['nom']),
                    ['element' => 'growl']
                );
                $sortie = true;
            } else {
                $this->Flash->set(
                    __('Veuillez corriger les erreurs ci-dessous.'),
                    ['element' => 'growl', 'params'=>['type' => 'danger']]
                );
            }
        }
        if ($sortie) {
            $this->redirect($this->previous);
        }

        $this->render('edit');
    }

    /**
     * @access public
     * @param type $id
     */
    public function admin_delete($id) // phpcs:ignore
    {
        $sequence = $this->Sequence->read('id, nom', $id);
        if (empty($sequence)) {
            $this->Flash->set(
                __('ID invalide pour la séquence'),
                ['element' => 'growl', 'params'=>['type' => 'danger']]
            );
        } elseif (!empty($sequence['Compteur'])) {
            $this->Flash->set(
                __(
                    'La séquence "%s" est utilisée par un compteur. Suppression impossible.',
                    $sequence['Sequence']['nom']
                ),
                ['element' => 'growl', 'params'=>['type' => 'danger']]
            );
        } elseif ($this->Sequence->delete($id)) {
            $this->Flash->set(
                __('La séquence "%s" a été supprimée', $sequence['Sequence']['nom']),
                ['element' => 'growl', 'params'=>['type' => 'success']]
            );
        }
        $this->redirect($this->previous);
    }
}
