<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class SignaturesController extends AppController
{
    public $uses = [
        'Deliberation',
        'DeliberationSeance',
        'Seance',
        'Cakeflow.Traitement',
        'ProjetEtat'
    ];
    public $components = [
        'Filtre',
        'Signatures',
        'Auth' => [
            'mapActions' => [
                'sendDeliberationsToSignature' => ['refreshSignature', 'index'],
                'sendAutresActesToSignature' => ['refreshSignature'],
                'allow' => []
            ]
        ]];

    /**
     * Envoi un/des projet(s) dans le parapheur
     * @param null $seance_id
     * @return mixed
     */
    public function index($seance_id = null)
    {
        // Formulaire envoyé
        if (!empty($this->data['Deliberation'])) {
            $this->sendDeliberationsToSignature();
        }
        $this->Filtre->initialisation(
            $this->name . ':' . $this->action . ':' . $seance_id,
            $this->data,
            ['url' => $this->here]
        );
        $conditions = $this->ProjetTools->handleConditions($this->Filtre->conditions());
        $this->set('seance_id', $seance_id);

        //FIX
        if (empty($seance_id)) {
            $conditions['OR']['Deliberation.parapheur_etat >'] = 0;
            $conditions['OR']['Deliberation.signee'] = true;
            $conditions['Deliberation.etat >'] = 2;

            $db = $this->Deliberation->getDataSource();

            $sousRequete = [
                'fields' => [
                    'Deliberation.id'
                ],
                'contain' => false,
                'joins' => [
                    $this->Deliberation->join('DeliberationSeance', ['type' => 'INNER']),
                    $this->DeliberationSeance->join('Seance', ['type' => 'INNER']),
                    $this->DeliberationSeance->Seance->join('Typeseance', ['type' => 'INNER']),
                    $this->Deliberation->join('Typeacte', ['type' => 'INNER']),
                ],
                'conditions' => [
                    'Typeseance.action' => 0,
                    //'Typeacte.teletransmettre' => true
                ]
            ];

            $subQuery = $this->Deliberation->sql($sousRequete);

            $subQuery = ' "' . $this->Deliberation->alias . '"."id" IN (' . $subQuery . ') ';
            $subQueryExpression = $db->expression($subQuery);
            $conditions[] = $subQueryExpression;

            $order = ['Deliberation.date_acte DESC'];
            $delibs = $this->Deliberation->find('all', [
                'fields' => ['Deliberation.id',
                    'Deliberation.objet_delib',
                    'Deliberation.num_delib',
                    'Deliberation.titre',
                    'Deliberation.etat',
                    'Deliberation.circuit_id',
                    'Deliberation.parapheur_etat',
                    'Deliberation.parapheur_bordereau',
                    'Deliberation.signee',
                    'Deliberation.signature',
                    'Deliberation.typeacte_id',
                    'Deliberation.theme_id',
                    'Deliberation.service_id',
                    'Deliberation.date_acte',
                    'Deliberation.signature_type',
                    'Deliberation.vote_resultat'
                ],
                'conditions' => $conditions,
                'contain' => [
                    'Service.name',
                    'Theme.libelle',
                    'Typeacte' => [
                        'fields' => ['name'],
                        'Nature' => ['fields' => ['code']]
                    ],
                    'Circuit.nom',
                    'DeliberationTypeseance' => [
                        'fields' => ['id'],
                        'Typeseance' => [
                            'fields' => ['id', 'libelle', 'action'],
                        ]
                    ],
                    'DeliberationSeance' => [
                        'fields' => ['id'],
                        'Seance' => [
                            'fields' => ['id', 'date', 'type_id'],
                            'Typeseance' => [
                                'fields' => ['id', 'libelle', 'action']
                            ]]]],
                'recursive' => -1,
                'order' => [$order]]);

            $this->ProjetTools->ajouterFiltre($delibs);
        } else {
            $conditions['DeliberationSeance.seance_id'] = $seance_id;
            $conditions['Deliberation.etat >='] = 0;
            // Formulaire non envoyé
            if (!isset($this->data['Parapheur']['circuit_id'])) {
                $delibs = $this->DeliberationSeance->find(
                    'all',
                    [
                        'recursive' => -1,
                        'fields' => [
                            'DeliberationSeance.seance_id',
                            'DeliberationSeance.deliberation_id',
                            'DeliberationSeance.position',
                            'Seance.id',
                            'Seance.type_id',
                            'Deliberation.id',
                            'Deliberation.service_id',
                            'Deliberation.theme_id',
                            'Deliberation.circuit_id',
                            'Deliberation.typeacte_id',
                            'Deliberation.anterieure_id',
                            'Deliberation.redacteur_id',
                            'Deliberation.etat',
                            'Deliberation.parapheur_id',
                            'Deliberation.parapheur_etat',
                            'Deliberation.objet_delib',
                            'Deliberation.titre',
                            'Deliberation.num_delib',
                            'Deliberation.parapheur_bordereau',
                            'Deliberation.signee',
                            'Deliberation.signature',
                            'Deliberation.parapheur_commentaire',
                            'Deliberation.date_acte',
                            'Deliberation.signature_type',
                            'Deliberation.vote_resultat',

                        ],
                        'conditions' => $conditions,
                        'contain' => [
                            'Seance',
                            'Deliberation' => [
                                'Typeacte.nature_id',
                                'Typeacte' => [
                                    'fields' => ['name'],
                                    'Nature' => ['fields' => ['code']]
                                ],
                                'Service.name',
                                'Theme.libelle',
                                'Circuit.nom'
                            ]
                        ],
                        'recursive' => -1,
                        'order' => 'DeliberationSeance.position ASC',
                    ]
                );
                $this->ProjetTools->ajouterFiltreSeance($delibs);
            }
        }
        $droits = $this->ProjetTools->getDroits([
            'Projets' => ['read', 'update', 'delete'],
            'reattribuerTous' => 'read',
            'editerTous' => 'read',
            'supprimerTous' => 'read',
            'duplicate' => 'read',
            'goNext' => 'read',
            'validerEnUrgence' => 'read',
            'editPostSign' => 'read']);

        for ($i = 0; $i < count($delibs); $i++) {
            $this->ProjetTools->actionDisponible($delibs[$i], $droits);

            if ($delibs[$i]['Deliberation']['etat'] == 0 && $delibs[$i]['Deliberation']['anterieure_id'] != 0) {
                $delibs[$i]['Deliberation']['iconeEtat'] = $this->ProjetTools->iconeEtat(
                    $delibs[$i]['Deliberation']['id'],
                    -2,
                    isset($delibs[$i]['Nature']['code'])
                        ? $delibs[$i]['Nature']['code'] : $delibs[$i]['Deliberation']['Typeacte']['Nature']['code']
                );
            } elseif ($delibs[$i]['Deliberation']['etat'] == 1) {
                //Recherche si l'auteur est dans le circuit de validation du delibs[$i]
                $estDansCircuit = $this->Traitement->triggerDansTraitementCible(
                    $this->Auth->user('id'),
                    $delibs[$i]['Deliberation']['id']
                );

                $tourDansCircuit = $estDansCircuit ? $this->Traitement->positionTrigger(
                    $this->Auth->user('id'),
                    $delibs[$i]['Deliberation']['id']
                )
                    : 0;
                $estRedacteur = ($this->Auth->user('id') == $delibs[$i]['Deliberation']['redacteur_id']);
                $delibs[$i]['Deliberation']['iconeEtat'] = $this->ProjetTools->iconeEtat(
                    $delibs[$i]['Deliberation']['id'],
                    $delibs[$i]['Deliberation']['etat'],
                    isset($delibs[$i]['Nature']['code'])
                        ? $delibs[$i]['Nature']['code'] : $delibs[$i]['Deliberation']['Typeacte']['Nature']['code'],
                    false,
                    $estDansCircuit,
                    $estRedacteur,
                    $tourDansCircuit
                );
            } else {
                $delibs[$i]['Deliberation']['iconeEtat'] = $this->ProjetTools->iconeEtat(
                    $delibs[$i]['Deliberation']['id'],
                    $delibs[$i]['Deliberation']['etat'],
                    isset($delibs[$i]['Nature']['code'])
                        ? $delibs[$i]['Nature']['code'] : $delibs[$i]['Deliberation']['Typeacte']['Nature']['code'],
                    $droits['editerTous/read']
                );
            }
            $delibs[$i]['etat_libelle'] = $this->ProjetEtat->libelleEtat(
                $delibs[$i]['Deliberation']['etat'],
                isset($delibs[$i]['Nature']['code'])
                    ? $delibs[$i]['Nature']['code'] : $delibs[$i]['Deliberation']['Typeacte']['Nature']['code']
            );
        }

        $circuits = $this->Signatures->getCircuits();
        $this->set('deliberations', $delibs);
        $this->set('signature', true);
        $this->set('circuits', $circuits);
    }


    /**
     * Cancel a signature
     * @version 5.1
     */
    private function cancelSignature($id = null)
    {
        if ($id) {
            $this->Deliberation->id = $id;
            $this->Deliberation->cancelSignature();
        }
        return $this->redirect($this->previous);
    }

    /**
     * Envoyer à la signature
     *
     * @access public
     * @return type
     */
    public function sendAutresActesToSignature()
    {
        if (!empty($this->request->data['signatureManuscrite'])
            || !empty($this->request->data['Parapheur']['circuit_id'])
        ) {
            $this->sendAutresActes();
            $this->Flash->set(_('Vous devez sélectionner au moins un acte à générer.'), [
                'element' => 'growl',
                'params' => ['type' => 'danger']
            ]);
        }

        return $this->redirect($this->previous);
    }

    /**
     * Envoyer à la signature
     *
     * @access public
     * @return type
     */
    public function sendDeliberationsToSignature()
    {
        $this->Deliberation->Behaviors->load(
            'Version',
            $this->Deliberation->actsAsVersionOptionsList['Version']
        );

        // Formulaire envoyé
        if (empty($this->data['Deliberation'])) {
            $this->Flash->set(__('Formulaire Vide'), ['element' => 'growl']);
            return $this->redirect($this->previous);
        }
        $message = '';
        try {
            foreach ($this->data['Deliberation'] as $id => $acte) {
                if ((isset($acte['is_checked']) && $acte['is_checked'])) {
                    $type_acte = $this->Deliberation->find('first', [
                        'fields' => ['Deliberation.id','Deliberation.num_pref'],
                        'contain' => ['Typeacte.teletransmettre'],
                        'conditions' => ['Deliberation.id' => $id],
                        'recursive' => -1
                    ]);
                    $this->Deliberation->id = $id;
                    $dateSignature = null;
                    if (!empty($this->request->data['signatureManuscrite']['is_checked'])
                        && !empty($this->request->data['signatureManuscrite']['date_acte_all'])
                    ) {
                        $dateSignature = CakeTime::format(
                            str_replace('/', '-', $this->request->data['signatureManuscrite']['date_acte_all']),
                            '%Y-%m-%d 00:00:00'
                        );
                    }
                    if (isset($this->data['traitement_lot']['action'])
                        && $this->data['traitement_lot']['action'] === 'signatureManuscrite'
                    ) {
                        $signee = $this->Deliberation->signatureManuscrite(
                            $id,
                            $this->Auth->user('id'),
                            $dateSignature
                        );
                        $message .= __("Acte n° %s : ", $id)
                            . ($signee ? __("Signé correctement")
                                : __("Erreur de signature")) . "\n";
                    } else {
                        $envoye = $this->Deliberation->envoyerAuParapheur(
                            $id,
                            $this->data['Parapheur']['circuit_id'],
                            $this->Auth->user('id')
                        );
                        $message .= __("Acte n° %s : ", $id)
                            . ($envoye ? __("Envoyé")
                                : __("Echec de l'envoi")) . "\n";
                    }
                }
            }
            if ($message != '') {
                $this->Flash->set($message, ['element' => 'growl']);
            }
        } catch (Exception $e) {
            $this->Flash->set($e->getMessage(), ['element' => 'growl', 'params' => ['type' => 'danger']]);
        }

        return $this->action === 'sendDeliberationsToSignature' ?
            $this->redirect($this->previous) : $this->redirect($this->here);
    }

    /**
     * Mise à jour de l'état des dossiers envoyés au parapheur pour signature
     *
     * @access public
     * @return type
     */
    public function refreshSignature()
    {
        $this->Signatures->updateAll();

        return $this->redirect($this->previous);
    }

    public function autresActesCancelSignature($id = null)
    {
        $this->cancelSignature($id);
    }

    public function deliberationCancelSignature($id = null)
    {
        $this->cancelSignature($id);
    }

    /**
     * Envoi d'actes en signature pour les délibérations
     *
     * @version 4.3
     * @access public
     * @return mixed
     */
    private function sendAutresActes()
    {
        try {
            if (!empty($this->request->data['sendActesToParapheur'])) {
                App::uses('Signature', 'Lib');
                $this->Signature = new Signature();
            }
            $message = '';
            $typeMessage = 'info';
            foreach ($this->data['Deliberation'] as $id => $acte) {
                if (!empty($acte['is_checked'])) {
                    $type_acte = $this->Deliberation->find('first', [
                        'fields' => ['Deliberation.id','num_pref'],
                        'contain' => ['Typeacte.teletransmettre'],
                        'conditions' => ['Deliberation.id' => $id],
                        'recursive' => -1
                    ]);
                    $this->Deliberation->id = $id;
                    $continue = false;
                    $acte['num_pref'] = empty($acte['num_pref'])
                        ? $type_acte['Deliberation']['num_pref'] : $acte['num_pref'];
                    if (!empty($acte['num_pref'])) {
                        $this->Deliberation->saveField('num_pref', str_replace('_', '.', $acte['num_pref']));
                    }
                    $dateSignature = null;
                    if (!empty($this->request->data['signatureManuscrite']['is_checked'])
                        && !empty($this->request->data['signatureManuscrite']['date_acte_all'])) {
                        $dateSignature = CakeTime::format(
                            str_replace('/', '-', $this->request->data['signatureManuscrite']['date_acte_all']),
                            '%Y-%m-%d 00:00:00'
                        );
                    }
                    //On écrase la valeur de l'état au cas ou l'acte serait repassé dans un circuit
                    $this->Deliberation->saveField('etat', 3);

                    if (!empty($this->request->data['signatureManuscrite']['is_checked'])) {
                        //En cas de signature manuscrite on efface la valeur de l'état parapheur
                        $this->Deliberation->saveField('parapheur_etat', null);
                        $signee = $this->Deliberation->signatureManuscrite(
                            $id,
                            $this->Auth->user('id'),
                            $dateSignature
                        );
                        $message .= __('Acte n° %s : ', $id)
                            . ($signee ? __('signé correctement') : __('Erreur de signature')) . '\n';
                        $typeMessage = 'info';
                    } else {
                        $envoye = $this->Deliberation->envoyerAuParapheur(
                            $id,
                            $this->data['Parapheur']['circuit_id'],
                            $this->Auth->user('id')
                        );
                        $message .= __('Acte n° %s : ', $id)
                            . ($envoye ? __('envoyé') : __('échec de l\'envoi')) . '\n';
                        $typeMessage = $envoye ? 'info' : 'danger';
                    }
                }
            }
            if (empty($message)) {
                $this->Flash->set(__('Veuillez sélectionner une action'), [
                    'element' => 'growl',
                    'params' => ['type' => 'danger']
                ]);
                return $this->redirect($this->here);
            }

            $this->Flash->set($message, [
                'element' => 'growl',
                'params' => ['type' => $typeMessage, 'settings' => ['delay' => 60000]]
            ]);
            return $this->redirect($this->here);
        } catch (Exception $e) {
            $this->Flash->set($e->getMessage(), ['element' => 'growl', 'params' => ['type' => 'danger']]);
            return $this->redirect($this->here);
        }
    }
}
