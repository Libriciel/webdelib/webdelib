<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppController', 'Controller');
App::uses('ValidationTrait', 'Lib/Trait');
/**
 * Class VotesController
 *
 * @package app.Controller
 */
class TraitementParLotController extends AppController
{
    use ValidationTrait;
    /**
     * [public description]
     * @var [type]
     */
    public $uses = ['Acteur',
        'Deliberation', 'Typeacte', 'User', 'Annexe', 'Typeseance', 'Seance',
        'TypeSeance', 'Commentaire', 'ModelOdtValidator.Modeltemplate', 'Theme',
        'Collectivite', 'Vote', 'Listepresence', 'Infosupdef', 'Infosup', 'Historique',
        'Cakeflow.Circuit', 'Cakeflow.Composition', 'Cakeflow.Etape', 'Cakeflow.Traitement',
        'Cakeflow.Visa', 'Nomenclature', 'DeliberationSeance', 'DeliberationTypeseance',
        'DeliberationUser', 'Service', 'Cron',
        'Nature','Typologiepiece',
        'ProjetEtat'
    ];
    /**
     * [public description]
     * @var [type]
     */
    public $components = [
        'RequestHandler',
        'ProjetTools',
        'SabreDav',
        'Progress',
        'Auth' => [
            'mapActions' => [
                'allow' => [ 'traiter'],
            ],
        ]];

    /**
     * @access public
     * @return CakeResponse|null
     * @version 5.1.0
     * @since 4.0.0
     */
    public function traiter($cookieTokenKey = null)
    {
        if (!isset($this->data['traitement_lot']['action']) || empty($this->data['traitement_lot']['action'])) {
            $this->Flash->set(
                __('Veuillez sélectionner une action.'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );

            return $this->redirect($this->previous);
        } else {
            $action = $this->data['traitement_lot']['action'];
        }

        if (isset($this->data['Deliberation']['check'])) {
            foreach ($this->data['Deliberation']['check'] as $tmp_id => $bool) {
                if ($bool) {
                    $delib_id = substr($tmp_id, 3, strlen($tmp_id));
                    $deliberationIds[] = $delib_id;
                }
            }
        } else {
            foreach ($this->data['Deliberation'] as $projet_id => $projet) {
                if (!empty($projet['is_checked']) && $projet['is_checked']) {
                    $deliberationIds[] = $projet_id;
                }
            }
        }
        if (empty($deliberationIds)) {
            $this->Flash->set(
                __('Veuillez sélectionner un projet.'),
                ['element' => 'growl', 'params' => ['type' => 'danger']]
            );
            return $this->redirect($this->previous);
        }

        foreach ($deliberationIds as $deliberation_id) {
            if ($action === 'suppression') {
                $this->Deliberation->supprimer($deliberation_id, $this->Auth->user('id'));
            }
            if ($action === 'valider') {
                $this->accepteDossier($deliberation_id);
            }
            if ($action === 'refuser') {
                $this->refuseDossier($deliberation_id);
            }
            if ($action === 'validerUrgence') {
                $this->validerEnUrgenceLeTraitement($deliberation_id, false);
            }
        }
        if ($action === 'generation') {
            return $this->genereFusionRechercheToClient(
                $deliberationIds,
                $this->data['traitement_lot']['modele'],
                $cookieTokenKey
            );
        }
        $this->Flash->set(__('Action effectuée avec succès'), ['element' => 'growl']);

        return $this->redirect($this->previous);
    }

    /**
     * Fonction de fusion de plusieurs projets ou délibérations avec envoi du résultat vers le client
     *
     * @access public
     * @param $ids
     * @param integer $modelTemplateId
     * @param integer $cookieToken numéro de cookie du client pour masquer la fenêtre attendable
     * @return CakeResponse
     */
    private function genereFusionRechercheToClient($ids, $modelTemplateId, $cookieTokenKey)
    {
        try {
            $this->Progress->start($cookieTokenKey);

            $this->Progress->at(10, __('Récupération des données à générer'));
            // fusion du document
            $this->Deliberation->Behaviors->load('OdtFusion', ['modelTemplateId' => $modelTemplateId]);

            $this->Progress->at(40, __('Fusion du document'));
            $this->Deliberation->odtFusion(['modelOptions' => ['deliberationIds' => $ids]]);
            $filename = $this->Deliberation->fusionName();

            // selon le format d'envoi du document (pdf ou odt)
            if ($this->Auth->user('formatSortie') === 'pdf') {
                $this->Progress->at(70, __('Conversion du document'));
                $typemime = "application/pdf";
                $filename = $filename . '.pdf';
                $content = $this->Deliberation->getOdtFusionResult();
            } else {
                $this->Progress->at(70, __('Actualisation du sommaire'));
                $typemime = "application/vnd.oasis.opendocument.text";
                $filename = $filename . '.odt';
                $content = $this->Deliberation->getOdtFusionResult('odt');
            }
            $this->Deliberation->deleteOdtFusionResult();
            $this->Progress->at(90, __('Préparation du fichier de sortie'));
            $this->Progress->setFile($filename, $typemime, $content);
            $this->Progress->stop();
        } catch (Exception $e) {
            $this->Progress->error($e);
            CakeLog::error(
                __('[Fusion] %s ligne %s' . "\n" . '%s', $e->getFile(), $e->getLine(), $e->getMessage())
            );
        }

        $this->autoRender = false;
        $this->response->type(['json' => 'text/x-json']);
        $this->RequestHandler->respondAs('json');
        $this->response->body(json_encode($this->Session->read('Progress.Tokens')));

        return $this->response;
    }
}
