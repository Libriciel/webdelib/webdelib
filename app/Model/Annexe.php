<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppTools', 'Lib');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * [Annexe description]
 *
 * @SuppressWarnings(PHPMD)
 */
class Annexe extends AppModel
{
    public $name = 'Annexe';
    public $displayField = 'titre';
    public $actsAsVersionOptionsList = [
        'Version' => [
            'implementedFinders' => ['versions' => 'findVersions'],
            'versionTable' => 'annexes_versions',
            'versionField' => 'version_id',
            'additionalVersionFields' => ['created'],
            'fields' => [
                'id',
                'created',
                'modified',
                'data',
                'filename',
                'filetype',
                'size',
                'position',
                'joindre_fusion',
                'joindre_ctrl_legalite',
                'typologiepiece_code',
                'foreign_key',

            ],
            'byteaFields' => [
                'data',
                'edition_data',
                'data_pdf',
                'tdt_data_edition',
                'tdt_data_pdf',
            ],
            'contain' => [
            ],
            'foreignKey' => 'foreign_key',
            'referenceName' => 'AnnexesVersion',
            'belongsTo'=> 'Deliberation', // on mettra à jour la révision de la délibération.
        ]];

    public $belongsTo = [
        'Deliberation' => [
            'foreignKey' => 'foreign_key'
        ]
    ];
    public $validate = [
        'joindre_ctrl_legalite' => [
            'rule' => 'checkFormatControlLegalite',
            'message' => 'Le format de fichier est invalide pour joindre au contrôle de légalité'],
        'joindre_fusion' => [
            'rule' => 'checkFormatFusion',
            'message' => 'Le format de fichier est invalide pour le joindre à la fusion'],
        'filename' => [
            'regleFilename-1' => [
                'rule' => ['maxLength', 45],
                'message' => 'Le nom du fichier est trop long (45 caractères maximum)', 'growl'
            ],
            'regleFilename-2' => [
                'rule' => '/^[a-zA-Z0-9-_.& ]{5,}$/i',
                'message' =>
                    'Seulement les lettres, les entiers, les espaces et les caractères spéciaux -_.& '
                    .'sont autorisés dans le nom du fichier. Minimum de 5 caractères',
                    'growl'
            ]
        ],
        'titre' => [
            'rule' => ['maxLength', 200],
            'message' => 'Le titre du fichier est trop long (200 caractères maximum)', 'growl']
    ];

    /**
     * [reorderAnnexe reOrdonne chaques positions des annexes]
     * @param  [type] $foreign_key [description]
     *
     * @version 5.1.2
     * @since 4.3
     */
    public function reorderAnnexe($foreign_key)
    {
        if (empty($foreign_key)) {
            throw new Exception("Error Processing Request", 1);
        }

        //La position par default
        $position = 1;
        $oAnnexe = ClassRegistry::init('Annexe');
        $annexes = $oAnnexe->find('all', [
            'fields' => ['id', 'position', 'modified'],
            'conditions' => ['foreign_key' => $foreign_key],
            'order' => ['Annexe.position' => 'ASC'],
            'recursive' => -1,
        ]);

        foreach ($annexes as $annexe) {
            $oAnnexe->save(
                [
                  'Annexe' => [
                  'id'=> $annexe['Annexe']['id'],
                  'modified' => false,
                  'position' => $position
                  ]
                ],
                [
                  'fieldList' => ['position'],
                  'validate' => false,
                  'callbacks' => false
                ]
            );

            $position++;
        }
    }

    /**
     * [beforeDelete description]
     * @return [type] [description]
     *
     * @version 5.1.0
     */
    public function beforeDelete($cascade = false)
    {
        $this->recursive  = -1;
        $annexe = $this->findById($this->id, ['foreign_key', 'position']);
        $this->data['Annexe']['foreign_key'] = $annexe['Annexe']['foreign_key'];
        $this->data['Annexe']['position'] = $annexe['Annexe']['position'];

        return true;
    }

    /**
     * [afterDelete description]
     * @return [type] [description]
     *
     * @version 5.1.0
     */
    public function afterDelete()
    {
        if (!empty($this->data['Annexe']['foreign_key'])) {
            $this->reorderAnnexe($this->data['Annexe']['foreign_key']);
        }
    }


    /**
     * Enregistrement des annexes
     *
     * @access private
     *
     * @param int|string $delibId
     * @param array $annexe
     * @param array $annexesErrors
     * @return boolean
     *
     * @version 5.1.0
     */
    public function saveAnnexe($annexes)
    {
        $this->Behaviors->load('Version', $this->actsAsVersionOptionsList['Version']);

        $annexesErrors = [];
        foreach ($annexes as $key => $annexe) {
            $success = true;
            //Pour la gestion des erreurs des annexes
            $titre = !empty($annexe['titre']) ? $annexe['titre'] : $annexe['file']['name'];
            if (!empty($annexe['file']['error'])) {
                if ($annexe['file']['error'] === UPLOAD_ERR_INI_SIZE) {
                    $annexesErrors[$titre][] = __(
                        'Limite de taille par fichier atteinte (Maximum autorisé: %s)',
                        AppTools::humanPHPSize(Configure::read('App.upload_max_filesize'))
                    );
                } else {
                    $annexesErrors[$titre][] = 'Erreur lors de l\'envoi';
                }
            } elseif (AppTools::convertToBytes(Configure::read('App.upload_max_filesize')) < $annexe['file']['size']
            ) {
                $annexesErrors[$titre][] = __(
                    'Limite de taille par fichier atteinte (Maximum autorisé: %s)',
                    AppTools::humanPHPSize(Configure::read('App.upload_max_filesize'))
                );
            } elseif (is_array($annexe) && $this->isUploadedFile(['file' => $annexe['file']])) {
                $newAnnexe['Annexe']['model'] = 'Deliberation';
                $newAnnexe['Annexe']['position'] = $annexe['numAnnexe'];
                $newAnnexe['Annexe']['foreign_key'] = $annexe['projet_id'];
                $newAnnexe['Annexe']['titre'] = $annexe['titre'];
                $newAnnexe['Annexe']['joindre_ctrl_legalite'] = !empty($annexe['ctrl']) ? $annexe['ctrl'] : false;
                $newAnnexe['Annexe']['typologiepiece_code'] = !empty($annexe['typologiepiece_code'])
                    ? $annexe['typologiepiece_code'] : null;
                $newAnnexe['Annexe']['joindre_fusion'] = !empty($annexe['fusion']) ? $annexe['fusion'] : false;

                $file = new File($annexe['file']['tmp_name'], false);
                $folder_temp = new Folder(AppTools::newTmpDir(TMP . DS . 'files' . DS . 'upload'));
                $file_test = new File(
                    $folder_temp->pwd() . 'file_test.' . AppTools::getExtensionFile($annexe['file']['name'])
                );
                $file_test->write($file->read());

                $infoContent = AppTools::fileMime($file_test->pwd());
                if (empty($infoContent['mimetype'])) {
                    $annexesErrors[$titre][] =
                        __('Format de fichier non reconnu. Veuillez contacter votre administrateur');
                    $file->close();
                    $success = false;
                }
                if (!empty($infoContent['mimetype'])
                    && !in_array($infoContent['mimetype'], AppTools::getTypemimesFilesValid(), true)
                ) {
                    $annexesErrors[$titre][] = __(
                        'Le document précisé en annexe %s (%s) n\'est pas au un format autorisé. '
                        .'Veuillez contacter votre administrateur',
                        $infoContent['formatname'],
                        $infoContent['puid']
                    );
                    $file->close();
                    $success = false;
                }
                if ($success) {
                    $newAnnexe['Annexe']['filetype'] = $infoContent['mimetype'];
                    $newAnnexe['Annexe']['size'] = $file->size();
                    $newAnnexe['Annexe']['data'] = $file->read();
                    $newAnnexe['Annexe']['filename'] = $annexe['file']['name'];
                    $file->close();

                    $this->create();
                    //CakeLog::error(var_export($newAnnexe['Annexe'], true));
                    if (!$this->save($newAnnexe)) {
                        foreach ($this->validationErrors as $error_annexe) {
                            $annexesErrors[$titre][] = $error_annexe;
                        }
                        $this->validationErrors = [];
                    }
                }
                $this->clear();
            } else {
                $annexesErrors[$titre][] = 'Erreur inconnue';
            }
        }

        if (!empty($annexesErrors)) {
            $this->validationErrors[$this->alias] = $annexesErrors;
            return false;
        }

        return true;
    }

    /**
     * Regarde dans le fichier formats.inc si le document peut être envoyé au controle de légalité
     *
     * @version 4.3
     * @access public
     * @return boolean
     */
    public function checkFormatControlLegalite()
    {
        if ($this->data['Annexe']['joindre_ctrl_legalite'] == 1) {
            $DOC_TYPE = Configure::read('DOC_TYPE');
            if (!empty($this->data['Annexe']['filename'])) {
                $mime = $this->data['Annexe']['filetype'];
            } else {
                $annex = $this->find('first', [
                    'fields' => ['Annexe.filetype'],
                    'conditions' => ['Annexe.id' => $this->id],
                    'recursive' => -1,
                    ]);
                $mime = $annex['Annexe']['filetype'];
            }
            return !empty($DOC_TYPE[$mime]['joindre_ctrl_legalite']);
        }
        return true;
    }

    /**
     * [checkControlTypologiePiece description]
     * @return [type] [description]
     *
     * @version 5.1.0
     */
    public function checkControlTypologiePiece()
    {
        if (!empty($this->data['Annexe']['typologiepiece_code'])) {
            $this->Typologiepiece = ClassRegistry::init('Typologiepiece');
            $this->recursive = -1;
            $projet = $this->Deliberation->findById($this->data['Annexe']['foreign_key'], ['typeacte_id']);
            $typologiepieces = $this->Typologiepiece->getTypologiePieceByTypeActe(
                'Annexe',
                $projet['Deliberation']['typeacte_id']
            );
            if (!in_array(
                $this->data['Annexe']['typologiepiece_code'],
                Hash::extract($typologiepieces, '{n}.Typologiepiece.code'),
                true
            )
            ) {
                return false;
            }
        }
        return true;
    }

    /**
     * Regarde dans le fichier formats.inc si le document peut être joint à la fusion
     *
     * @version 4.3
     * @access public
     * @return boolean
     */
    public function checkFormatFusion()
    {
        if ($this->data['Annexe']['joindre_fusion'] == 1) {
            $DOC_TYPE = Configure::read('DOC_TYPE');
            if (!empty($this->data['Annexe']['filename'])) {
                $mime = $this->data['Annexe']['filetype'];
            } else {
                $annex = $this->find('first', ['conditions' => ['Annexe.id' => $this->id],
                    'recursive' => -1,
                    'fields' => ['Annexe.filetype']]);
                $mime = $annex['Annexe']['filetype'];
            }
            return !empty($DOC_TYPE[$mime]['joindre_fusion']);
        }
        return true;
    }

    /**
     * Cherche les annexes d'une Deliberation
     *
     * @version 4.3
     * @access public
     * @param string $delib_id identifiant de la Deliberation
     * @param bool $to_send ne chercher que les annexes avec la propriété joindre_ctrl_legalite
     * @param bool $to_merge ne chercher que les annexes avec la propriété joindre_fusion
     * @param bool $joindreParent inclure les Annexe de la Deliberation parente
     * @return array
     */
    public function getAnnexesFromDelibId($delib_id, $to_send = false, $to_merge = false, $joindreParent = false)
    {
        $conditions['Annexe.foreign_key'] = $delib_id;
        //$conditions['Annexe.model'] = 'Deliberation';
        if ($to_send) {
            $conditions['Annexe.joindre_ctrl_legalite'] = true;
        }
        if ($to_merge) {
            $conditions['Annexe.joindre_fusion'] = true;
        }
        $annexes = $this->find('all', [
            'conditions' => $conditions,
            'order' => ['Annexe.position' => 'ASC'],
            'recursive' => -1,
        ]);

        if ($joindreParent) {
            $this->Deliberation->id = $delib_id;
            $parent_id = $this->Deliberation->field('parent_id');
            if (!empty($parent_id)) {
                $parent_annexes = $this->getAnnexesFromDelibId($parent_id, $to_send, $to_merge, $joindreParent);
                if (!empty($parent_annexes)) {
                    foreach ($parent_annexes as $i => $parent_annexe) {
                        if ($parent_annexe['Annexe']['model'] == 'Deliberation') {
                            unset($parent_annexes[$i]);
                        }
                    }
                    $annexes = array_merge($annexes, $parent_annexes);
                }
            }
        }
        return $annexes;
    }

    /**
     * Récupère toutes les annexes (associées à 0, 1 ou plusieurs délibs) ayant la propriété joindre_fusion à false
     *
     * @version 4.3
     * @access public
     * @param null|int|array $delib_id
     * @return mixed tableau d'annexes
     */
    public function getAnnexesWithoutFusion($delib_id = null)
    {
        $conditions = ['joindre_fusion' => false];

        if (!empty($delib_id)) {
            $conditions['foreign_key'] = $delib_id;
        }
        $annexes = $this->find('all', [
            'conditions' => $conditions,
            'fields' => ['id', 'filetype', 'filename', 'data', 'titre'],
            'order' => ['position' => 'ASC'],
            'recursive' => -1
        ]);

        return $annexes;
    }

    /**
     * @version 4.3
     * @access public
     * @param type $annex_id
     * @return type
     */
    public function getContentToTdT($annex_id)
    {
        $annex = $this->find('first', [
            'conditions' => ['Annexe.id' => $annex_id],
            'recursive' => -1,
            'fields' => ['data_pdf']
        ]);

        return [
            'type' => 'pdf',
            'data' => $annex['Annexe']['data_pdf']
        ];
    }

    /**
     * Fonction d'initialisation des variables de fusion pour les avis en séance
     * les bibliothèques Gedooo doivent être inclues par avance
     * génère une exception en cas d'erreur
     *
     * @version 5.1.0
     * @since 4.3.0
     * @access public
     * @param type $aData
     * @param object_by_ref $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param string $modelName --> nom du model lié
     * @param integer $foreignKey --> id du model lié
     * @return type
     * @throws Exception
     */
    public function setVariablesFusion(&$aData, &$modelOdtInfos, $modelName, $foreignKey)
    {
        //Récupère les sections de type Annexes
        $sectionAnnexes = array_filter($modelOdtInfos->getSections(), function ($value, $key) {
            return preg_match('/^Annexes|^Annexes_.*([0-9])/', $value);
        }, ARRAY_FILTER_USE_BOTH);

        $count_annexes = $this->find('count', [
            'conditions' => [
                'foreign_key' => $foreignKey,
                'joindre_fusion' => true],
            'recursive' => -1,
              ]);

        if ($modelOdtInfos->hasUserFieldDeclared('nombre_annexe')) {
            $aData['nombre_annexe'] = ['value' => $count_annexes, 'type' => 'text'];
        }

        foreach ($sectionAnnexes as $sectionName) {
            $suffixe = '';
            if ($sectionName != "Annexes" && strpos($sectionName, '_')) {
                $suffixe = substr($sectionName, strpos($sectionName, '_'), strlen($sectionName));
            }

            if (empty($count_annexes)) {
                //Gedooo a besoin de "fields" non null pour correctement effectuer la fusion.
                $aData[$sectionName][] = $this->getVariablesFusionEmpty($modelOdtInfos, $suffixe);
                continue;
            }


            //Récuparation des champs pour le select
            $fields = $this->getListVariablesForFusion($modelOdtInfos, $suffixe);
            if (!empty($fields)) {
                $annexes = $this->getAppendixForFusion($foreignKey, $fields);
                // fusion des variables pour chaque annexe
                foreach ($annexes as $annexe) {
                    $aAnnexe = [];
                    if (in_array('titre', $fields, true)) {
                        $aAnnexe['titre_annexe' . $suffixe] = [
                            'value' => (!empty($annexe['Annexe']['titre']) ? $annexe['Annexe']['titre'] : ''),
                            'type' => 'text'
                        ];
                    }
                    if (in_array('filename', $fields, true)) {
                        $aAnnexe['nom_fichier' . $suffixe] = [
                            'value' => $annexe['Annexe']['filename'],
                            'type' => 'text'
                        ];
                    }
                    if (in_array('edition_data', $fields, true)
                        && !empty($annexe['Annexe']['edition_data'])
                    ) {
                        $aAnnexe['fichier' . $suffixe] = [
                            'value' => $annexe['Annexe']['edition_data'], 'type' => 'file'
                        ];
                    } elseif (in_array('edition_data', $fields, true)) {
                        throw new Exception(
                            'Toutes les annexes du projet :'
                            . $foreignKey
                            . ' ne sont pas encore converties pour générer le document.'
                            ."\n"
                            .'Veuillez réessayer dans quelques instants ...'
                        );
                    }
                    if (in_array('tdt_data_edition', $fields, true)
                        && !empty($annexe['Annexe']['tdt_data_edition'])
                    ) {
                        $aAnnexe['Annexe_tdt_tampon' . $suffixe] = [
                            'value' => $annexe['Annexe']['tdt_data_edition'],
                            'type' => 'file'
                        ];
                    } elseif (in_array('tdt_data_edition', $fields, true)
                        && !empty($annexe['Annexe']['tdt_data_pdf'])
                    ) {
                        throw new Exception(
                            'Toutes les annexes du projet :'
                            . $foreignKey
                            . ' ne sont pas encore converties pour générer le document.'
                            . "\n"
                            .'Veuillez réessayer dans quelques instants ...'
                        );
                    } else { //Si génération avant envoi au TdT
                        $aAnnexe['Annexe_tdt_tampon' . $suffixe] = [
                            'value' => file_get_contents(APP . DS . 'Config' . DS . 'OdtVide.odt'),
                            'type' => 'file'
                        ];
                    }
                    $aData[$sectionName][] = $aAnnexe;
                }
            } else {
                $aData[$sectionName][] = $this->getVariablesFusionEmpty($modelOdtInfos, $suffixe);
            }
        }
    }

    /**
     * @version 4.3
     * @access public
     * @param type $annex_id
     * @return type
     */
    public function getContentToGed($annexeId)
    {
        $DOC_TYPE = Configure::read('DOC_TYPE');
        $file_extension = null;

        $annexe = $this->find(
            'first',
            [
              'conditions' => ['Annexe.id' => $annexeId],
              'fields' => [
                'filetype',
                'data',
                'data_pdf',
                'filename',
                'titre',
                'joindre_ctrl_legalite',
                'typologiepiece_code',
                'tdt_data_pdf'
              ],
              'recursive' => -1
            ]
        );

        // For multiple extension select the first extension to array
        if (is_array($DOC_TYPE[$annexe['Annexe']['filetype']]['extension'])) {
            $file_extension = $DOC_TYPE[$annexe['Annexe']['filetype']]['extension'][0];
        } else {
            $file_extension = $DOC_TYPE[$annexe['Annexe']['filetype']]['extension'];
        }

        if ($annexe['Annexe']['filetype'] === 'application/pdf') {
            return [
                'type' => $DOC_TYPE[$annexe['Annexe']['filetype']]['extension'],
                'filetype' => 'application/pdf',
                'name' => AppTools::getNameFile($annexe['Annexe']['filename']) . '.pdf',
                'filename' => $annexeId . '.pdf',
                'titre' => $annexe['Annexe']['titre'],
                'joindre_ctrl_legalite' => $annexe['Annexe']['joindre_ctrl_legalite'],
                'data_tampon'=> $annexe['Annexe']['tdt_data_pdf'],
                'data' => $annexe['Annexe']['data']];
        }
        if ($annexe['Annexe']['joindre_ctrl_legalite'] && !empty($annexe['Annexe']['data_pdf'])) {
            return [
                'type' => 'pdf',
                'filetype' => 'application/pdf',
                'name' => AppTools::getNameFile($annexe['Annexe']['filename']) . '.pdf',
                'filename' => $annexeId . '.pdf',
                'titre' => $annexe['Annexe']['titre'],
                'joindre_ctrl_legalite' => $annexe['Annexe']['joindre_ctrl_legalite'],
                'data_tampon'=> $annexe['Annexe']['tdt_data_pdf'],
                'data' => $annexe['Annexe']['data_pdf']
            ];
        }

        return ['type' => $DOC_TYPE[$annexe['Annexe']['filetype']]['extension'],
            'filetype' => $annexe['Annexe']['filetype'],
            'name' => $annexe['Annexe']['filename'],
            'filename' => $annexeId . '.' . $file_extension,
            'titre' => $annexe['Annexe']['titre'],
            'joindre_ctrl_legalite' => $annexe['Annexe']['joindre_ctrl_legalite'],
            'data' => $annexe['Annexe']['data']];
    }

    /**
     * Liste des variables de fusion utilisées dans le template
     * @param $modelOdtInfos
     * @version 5.1.0
     * @since 4.2.0
     */
    private function getListVariablesForFusion($modelOdtInfos, $suffixe = '')
    {
        // liste des variables de fusion utilisées dans le template
        $fields = [];
        if ($modelOdtInfos->hasUserFieldDeclared('titre_annexe'.$suffixe)) {
            $fields[] = 'titre';
        }
        if ($modelOdtInfos->hasUserFieldDeclared('nom_fichier'.$suffixe)) {
            $fields[] = 'filename';
        }
        if ($modelOdtInfos->hasUserField('fichier'.$suffixe)) {
            $fields[] = 'edition_data';
        }
        if ($modelOdtInfos->hasUserField('Annexe_tdt_tampon'.$suffixe)) {
            $fields[] = 'tdt_data_pdf';
            $fields[] = 'tdt_data_edition';
        }
        if ($modelOdtInfos->hasUserFieldDeclared('nombre_annexe')) {
            $fields[] = 'id';
        }
        return $fields;
    }

    /**
     * @param $aData
     * @param $modelOdtInfos
     * @param $modelName
     * @param $foreignKey
     * @version 4.2.0
     */
    private function getAppendixForFusion($foreignKey, $fields)
    {

        // lecture en base de données
        return $this->find('all', [
            'fields' => $fields,
            'conditions' => [
                //'model' => $modelName, FIX Problème multidélibération
                'foreign_key' => $foreignKey,
                'joindre_fusion' => true],
            'order' => ['position ASC'],
            'recursive' => -1
          ]);
    }

    /**
     * Return empty fields when no appendix
     * @version 5.1
     * @return array $aAnnexe Contain
     */
    private function getVariablesFusionEmpty($modelOdtInfos, $suffixe = '')
    {
        $aAnnexe = [];
        if ($modelOdtInfos->hasUserFieldDeclared('titre_annexe'.$suffixe)) {
            $aAnnexe['titre_annexe'.$suffixe] = ['value' => '', 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('nom_fichier'.$suffixe)) {
            $aAnnexe['nom_fichier'.$suffixe] = ['value' => '', 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('fichier'.$suffixe)) {
            $aAnnexe['fichier'.$suffixe] = [
                'value' => file_get_contents(APP . DS . 'Config' . DS . 'OdtVide.odt'),
                'type' => 'file'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('tdt_data_edition'.$suffixe)) {
            $aAnnexe['tdt_data_edition'.$suffixe] = [
                'value' => file_get_contents(APP . DS . 'Config' . DS . 'OdtVide.odt'),
                'type' => 'file'
            ];
        }
        return $aAnnexe;
    }

    /**
     * [checkControlTypologiePiece description]
     * @return [type] [description]
     *
     * @version 5.1.0
     */
    public function checkControlTypologiePieceByProjetId($projetId)
    {
        $this->recusive = -1;
        $projet = $this->Deliberation->findById($projetId, ['typeacte_id', 'typologiepiece_code', 'num_pref']);

        if (!empty($projet['Deliberation']['typologiepiece_code'])) {
            $this->recusive = -1;
            $annexes = $this->findAllByForeignKey(
                $projetId,
                ['id','typologiepiece_code', 'titre', 'filename', 'joindre_ctrl_legalite']
            );
            $this->Typologiepiece = ClassRegistry::init('Typologiepiece');
            $typologiepieces = $this->Typologiepiece->getTypologiePieceByTypeActe(
                'Annexe',
                $projet['Deliberation']['typeacte_id'],
                str_replace('.', '_', $projet['Deliberation']['num_pref'])
            );
            $annexesErrors = [];
            foreach ($annexes as $annexe) {
                if (!empty($annexe['Annexe']['typologiepiece_code'])
                    && $annexe['Annexe']['joindre_ctrl_legalite']===true
                    && !in_array(
                        $annexe['Annexe']['typologiepiece_code'],
                        Hash::extract($typologiepieces, '{n}.Typologiepiece.code'),
                        true
                    )
                ) {
                    $titre = !empty($annexe['Annexe']['titre'])
                        ? $annexe['Annexe']['titre'] : $annexe['Annexe']['filename'];
                    $annexesErrors[$titre][] = __('Le type de pièce n\'est pas autorisé pour le type d\'acte');
                }
            }
            if (!empty($annexesErrors) && !empty($this->validationErrors[$this->alias])) {
                $this->validationErrors[$this->alias] = array_merge(
                    $this->validationErrors[$this->alias],
                    $annexesErrors
                );
                return false;
            } elseif (!empty($annexesErrors)) {
                $this->validationErrors[$this->alias] = $annexesErrors;
                return false;
            }
        }

        return true;
    }
}
