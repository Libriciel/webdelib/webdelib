<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppTools', 'Lib');
App::uses('CakeTime', 'Utility');

/**
 * Tâches planifiées (Cron)
 */
class Cron extends AppModel
{
    public $belongsTo = [
        'CreatedUser' => [
            'className' => 'User',
            'foreignKey' => 'created_user_id'],
        'ModifiedUser' => [
            'className' => 'User',
            'foreignKey' => 'modified_user_id']
    ];

    // constantes de la classe
    // status d'exécution
    public const EXECUTION_STATUS_SUCCES = 'SUCCES';
    public const EXECUTION_STATUS_WARNING = 'WARNING';
    public const EXECUTION_STATUS_FAILED = 'FAILED';
    public const EXECUTION_STATUS_LOCKED = 'LOCKED';
    public const MESSAGE_FIN_EXEC_SUCCES = 'TRAITEMENT_TERMINE_OK';
    public const MESSAGE_FIN_EXEC_WARNING = 'TRAITEMENT_TERMINE_ALERTE';
    public const MESSAGE_FIN_EXEC_ERROR = 'TRAITEMENT_TERMINE_ERREUR';
    // format de date
    public const FORMAT_DATE = 'Y-m-d H:i:s';

    /**
     * @access public
     * @return boolean
     */
    public function beforeSave($options = [])
    {
        if (isset($this->data[$this->alias]['execution_duration'])
            && is_array($this->data[$this->alias]['execution_duration'])
        ) {
            $this->data[$this->alias]['execution_duration'] =
                AppTools::arrayToDuration($this->data[$this->alias]['execution_duration']);
        }
        return true;
    }

    /**
     * Retourne le libéllé du statut d'exécution
     *
     * @version 4.3
     * @access public
     * @param type $status
     * @return string
     */
    public function libelleStatus($status)
    {
        switch ($status) {
            case self::EXECUTION_STATUS_LOCKED:
                $libelle = '<span class="label label-important" title="La tâche est verrouillée, '
                    .'ce qui signifie qu\'elle est en cours d\'exécution ou dans un état bloqué suite à une erreur">'
                    .'<i class="fa fa-lock"></i>'
                    . __('Verrouillée')
                    . '</span>';
                break;
            case self::EXECUTION_STATUS_SUCCES:
                $libelle = '<span class="label label-success" title="Opération exécutée avec succès">'
                    .'<i class="fa fa-check"></i>'
                    . __('Exécutée avec succès')
                    . '</span>';
                break;
            case self::EXECUTION_STATUS_WARNING:
                $libelle = '<span class="label label-warning" title="Avertissement(s) détecté(s) lors de l\'exécution, '
                    .'voir les détails de la tâche">'
                    .'<i class="fa fa-info"></i>'
                    . __('Exécutée, en alerte') . '</span>';
                break;
            case self::EXECUTION_STATUS_FAILED:
                $libelle = '<span class="label label-important" title="Erreur(s) détectée(s) lors de l\'exécution, '
                    .'voir les détails de la tâche">'
                    .'<i class="fa fa-warning"></i>'
                    . __('Non exécutée : erreur') . '</span>';
                break;
            default:
                $libelle = '<span class="label" title="La tâche n\'a jamais été exécutée">'
                    . __('En attente')
                    . '</span>';
        }
        return $libelle;
    }

    /**
     * Retourne le libellé correspondant à l'état 'active'
     *
     * @version 4.3
     * @access public
     * @param type $active
     * @return type
     */
    public function libelleActive($active)
    {
        return $active ? __('Oui') : __('Non');
    }

    /**
     * Retourne la forme litérale de 'execution_duration'
     *
     * @access public
     * @param type $duration
     * @return type
     */
    public function libelleDuration($duration)
    {
        return AppTools::durationToString($duration);
    }

    /**
     * [isExecutionTooLast description]
     * @param  [type]  $cron [description]
     * @return boolean       [description]
     */
    public function isExecutionTooLast($lastExecutionStartTime, $nextExecutionTime)
    {
        if (!empty($lastExecutionStartTime)) {
            // initialisations
            $nowTimestamp = CakeTime::fromString(date('Y-m-d H:i:s'));
            $timeDelayIsExecutionTooLast = AppTools::addSubDurationToDate(
                $nextExecutionTime,
                'PT3600S',
                self::FORMAT_DATE
            );
            $timeLastExecutionStartTimeTooFirt = AppTools::addSubDurationToDate(
                $lastExecutionStartTime,
                'PT3600S',
                self::FORMAT_DATE
            );
            if ($nowTimestamp > CakeTime::fromString($timeDelayIsExecutionTooLast)
              && $nowTimestamp > CakeTime::fromString($timeLastExecutionStartTimeTooFirt)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Retourne la prochaine date d'exécution pour le cron $cron
     *
     * @version 4.3
     * @access public
     * @param integer|array $id --> identifiant ou tableau de données de l'enregistrement
     * @return string --> date-heure de la prochaine exécution sous forme 'Y-m-d H:i:s'
     */
    public function calcNextExecutionTime($executionDuration, $nextExecutionTime)
    {
        // initialisations
        $now = date('Y-m-d H:i:s');
        $nowTimestamp = CakeTime::fromString($now);

        if (empty($nextExecutionTime)) {
            $nextExecuteTime = date('Y-m-d 00:00:00');
        } else {
            $nextExecuteTime = $nextExecutionTime;
        }

        //Si la date de prochaine exection est supérieur alors on n'éxécute la valeur de la tache programmé supérieur
        if ($nowTimestamp <= CakeTime::fromString($nextExecuteTime)) {
            return $nextExecuteTime;
        }

        // Récupération de l'échéance prochaine de l'exécution
        $nextExecuteTime = $nextDateDuration = AppTools::addSubDurationToDate(
            $nextExecuteTime,
            $executionDuration,
            self::FORMAT_DATE
        );
        //$nextExecuteTimestamp =  CakeTime::fromString($nextExecuteTime);
        $nextExecuteTimestamp = CakeTime::fromString($nextDateDuration);

        //Si l'intervale n'est pas supérieur à maitenant alors la taches à une date d'éxécution passé.
        if ($nowTimestamp > $nextExecuteTimestamp) {
            $dateNext = new DateTime($nextExecuteTime);
            $dateNow = new DateTime($now);
            $nowDateString = $dateNow->format('Y-m-d') . ' ' . $dateNext->format('H:i:s');
            do {
                $nowDateString = $nextExecuteTime = $nextDateDuration =
                    AppTools::addSubDurationToDate($nowDateString, $executionDuration, self::FORMAT_DATE);
                // La prochaine échéance devient la prochaine exécution
                $nextExecuteTimestamp = CakeTime::fromString($nextDateDuration);
            } while ($nowTimestamp >= $nextExecuteTimestamp);
        }

        if ($nowTimestamp < $nextExecuteTimestamp) {
            $nextExecuteTime = $nextDateDuration;
        }

        return $nextExecuteTime;
    }
}
