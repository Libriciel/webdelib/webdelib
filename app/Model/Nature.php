<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class Nature extends AppModel
{
    public $hasMany = ['Typeacte'];
    public $hasAndBelongsToMany = [
      'Typologiepiece' => [
          'classname' => 'Typologiepiece',
          'joinTable' => 'natures_typologiepieces',
          'foreignKey' => 'nature_id',
          'associationForeignKey' => 'typologiepiece_id',
          'conditions' => '',
          'order' => '',
          'limit' => '',
          'unique' => true,
          'finderQuery' => '',
          'deleteQuery' => '']
    ];

    /**
     * Initialisation des variables de fusion pour la nature d'un projet
     *
     * @access public
     * @version 5.0
     * @since 4.3
     * @param type $aData
     * @param object_by_ref $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param integer $id --> id du modèle lié
     */
    public function setVariablesFusion(&$aData, &$modelOdtInfos, $id)
    {
        if ($modelOdtInfos->hasUserFieldDeclared('nature_projet')
            || $modelOdtInfos->hasUserFieldDeclared('nature_acte')
        ) {
            $nature = $this->find('first', ['fields' => ['name'],
                'conditions' => ['Nature.id' => $id],
                'recursive' => -1
            ]);

            if ($modelOdtInfos->hasUserFieldDeclared('nature_projet')) {
                $aData['nature_projet'] = ['value' => $nature['Nature']['name'], 'type' => 'text'];
            }
            if ($modelOdtInfos->hasUserFieldDeclared('nature_acte')) {
                $aData['nature_acte'] = ['value' => $nature['Nature']['name'], 'type' => 'text'];
            }
        }
    }

    /**
     * Nature code par rapport au type d'acte pour l'envoi vers le Tdt
     *
     * @version 5.0
     * @access public
     * @param array $data
     * @return type
     */
    public function getNatureCodeByTypeActe($typeacte_id)
    {
        $typeacteClass = ClassRegistry::init('Typeacte');
        $typeacte = $typeacteClass->find('first', [
            'fields' => ['Typeacte.id'],
            'contain' => [
                'Nature' => ['fields' => 'Nature.code']],
            'conditions' => ['Typeacte.id' => $typeacte_id],
            'recursive' => -1,
        ]);
        switch ($typeacte['Nature']['code']) {
            case 'DE':
                $nature_code = 1;
                break;
            case 'AR':
                $nature_code = 2;
                break;
            case 'AI':
                $nature_code = 3;
                break;
            case 'CC':
                $nature_code = 4;
                break;
            case 'AU':
                $nature_code = 6;
                break;
            default:
                $nature_code = false;
        }

        return $nature_code;
    }

    public function getNatureDefaultCodeByTypeActe($typeacteId)
    {
        $code = $this->find('first', [
          'fields' => ['Nature.default'],
          'joins' => [
              $this->join('Typeacte', ['type' => 'INNER'])
          ],
          'conditions' => ['Typeacte.id' => $typeacteId],
          'recursive' => -1
        ]);

        return $code['Nature']['default'];
    }


    public function isAutresActesByCode($natureCode)
    {
        return  in_array($natureCode, ['AR', 'AI', 'CC', 'AU'], true);
    }

    public function saveNatureActes($natures)
    {
        // Delete all typologies pieces
        $this->Typologiepiece->deleteAll([['1 = 1']], true);

        foreach ($natures as $nature) {
            $this->recursive = -1;
            $natureEnCour = $this->findByCode($nature['code'], ['id']);
            if (!empty($natureEnCour)) {
                $this->Typologiepiece->saveTypesPJNatureActe(
                    $natureEnCour['Nature']['id'],
                    $nature['typesPJNatureActe']
                );
            }
        }
    }
}
