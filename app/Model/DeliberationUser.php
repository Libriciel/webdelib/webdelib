<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [DeliberationUser description]
 *
 * @version 5.1.0
 *
 * @SuppressWarnings(PHPMD)
 */
class DeliberationUser extends AppModel
{
    /**
     * [public description]
     * @var [type]
     */
    public $name = 'DeliberationUser';
    /**
     * [public description]
     * @var [type]
     */
    public $useTable = 'deliberations_users';
    /**
     * [public description]
     * @var [type]
     */
    public $belongsTo = ['Deliberation', 'User'];

    /**
     *
     * @access public
     * @param type $id
     * @param type $users_selectionnees
     *
     * @version 5.1
     */
    public function saveDeliberationUser($data)
    {
        if (!empty($data['Deliberation']['id']) && isset($data['DeliberationUser'])) {
            if (!empty($data['DeliberationUser'])) {
                foreach ($data['DeliberationUser'] as $key => $DeliberationUser) {
                    $data['DeliberationUser'][$key] = [
                        'deliberation_id' => $data['Deliberation']['id'],
                        'user_id' => $DeliberationUser,
                    ];
                }

                $selectedUserIds = Set::extract('/user_id', $data['DeliberationUser']);
            }
            //Supression des séances
            $nbr_users = $this->find(
                'count',
                [
                    'conditions' => ['deliberation_id' => $data['Deliberation']['id']],
                    'recursive' => -1
                ]
            );
            if (empty($data['DeliberationSeance']) && $nbr_users > 0) {
                $this->deleteAll(['deliberation_id' => $data['Deliberation']['id']]);
            }

            if (!empty($data['DeliberationUser'])) {
                $listUserIds = [];
                $users = $this->findAllByDeliberationId(
                    $data['Deliberation']['id'],
                    ['user_id'],
                    ['user_id'=>'ASC'],
                    null,
                    null,
                    -1
                );
                if (!empty($users)) {
                    foreach ($users as $user) {
                        $listUserIds[] = $user['DeliberationUser']['user_id'];
                    }

                    foreach (array_diff($listUserIds, $selectedUserIds) as $user_id) {
                        $this->deleteAll([
                        'DeliberationUser.user_id' => $user_id,
                        'DeliberationUser.deliberation_id' => $data['Deliberation']['id']], false);
                    }
                }

                if (is_array($listUserIds) && !empty($listUserIds)) {
                    $addListUserIds = array_diff($selectedUserIds, $listUserIds);
                } else {
                    $addListUserIds = $selectedUserIds;
                }

                $saveData = [];
                foreach ($addListUserIds as $user_id) {
                    $saveData[] = ['user_id' => $user_id, 'deliberation_id' => $data['Deliberation']['id']];
                }

                if (!empty($saveData)) {
                    $this->saveMany($saveData);
                }
            }
        }

        return true;
    }
}
