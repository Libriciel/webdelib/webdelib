<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [TypeseancesTypeacte description]
 * Liaison entre Type de séance et type d'acte
 *
 * @version 5.1.0
 * @since 4.0.0
 */
class TypeseancesTypeacte extends AppModel
{
    public $name = 'TypeseancesTypeacte';
    public $belongsTo = [
        'Typeacte' => [
            'className' => 'Typeacte',
            'foreignKey' => 'typeacte_id',
            'fields' => '',
            'conditions' => '',
            'order' => ''
            ],
        'Typeseance' => [
            'className' => 'Typeseance',
            'foreignKey' => 'typeseance_id',
            'fields' => '',
            'conditions' => '',
            'order' => '',
            ],
        ];

    /**
     * Liste tous les types de séance pour le type d'acte passé en paramètre
     * @version 5.0
     * @since 4.3
     * @access public
     * @param int $typeacte_id
     * @return array()
     */
    public function getTypeseanceByTypeActe($typeacte_id)
    {
        $typeseances = $this->find('all', [
            'fields' => 'typeseance_id',
            'conditions' => ['typeacte_id' => $typeacte_id],
            'recursive' => -1]);

        return (Set::extract('/TypeseancesTypeacte/typeseance_id', $typeseances));
    }

    /**
     * @version 4.3
     * @access public
     * @param type $typeseance_id
     * @return type
     */
    public function getNaturesParTypeseance($typeseance_id)
    {
        $liste = [];
        $natures = $this->find('all', ['conditions' => ['typeseance_id' => $typeseance_id],
            'fields' => ['typeacte_id'],
            'recursive' => -1]);
        foreach ($natures as $nature) {
            $liste[] = $nature['TypeseancesTypeacte']['typeacte_id'];
        }
        return($liste);
    }
}
