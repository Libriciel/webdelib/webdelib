<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('CakeTime', 'Utility');

class Compteur extends AppModel
{
    public $name = 'Compteur';
    public $displayField = 'nom';
    public $validate = [
        'nom' => [
            [
                'rule' => 'notBlank',
                'message' => 'Entrer un nom pour le compteur'
            ],
            [
                'rule' => 'isUnique',
                'message' => 'Entrer un autre nom, celui-ci est déjà utilisé.'
            ]
        ],
        'def_compteur' => [
            [
                'rule' => 'notBlank',
                'message' => 'Entrer la définition du compteur'
            ],
            [
                'rule' => ['whiteSpace'],
                'message' => 'La définition du compteur ne doit pas comporter d\'espaces.'
            ]
        ],
        'sequence_id' => [
            [
                'rule' => 'notBlank',
                'message' => 'Selectionner une séquence'
            ]
        ]
    ];
    public $belongsTo = 'Sequence';
    public $hasMany = 'Typeseance';
    public $cacheQueries = false;

    /**
     * Retourne la valeur suivante du compteur,
     * enregistre la nouvelle valeur de la séquence et du critère de réinitialisation en base
     * @param int $id Numéro de l'id du compteur
     * @param date $date Date personnalisée, date du jour si omis
     *
     * @return string Valeur suivante du compteur
     * @throws RuntimeException
     */

    public function genereCompteur($id, $date = null)
    {
        $this->recursive = -1;
        $compteur = $this->read(['def_reinit', 'sequence_id', 'def_compteur'], $id);

        if (empty($compteur)) {
            throw new NotFoundException(__('ID:"%s" de compteur invalide', $id));
        }

        // Initialisation de la date pour le remplacement
        $date = $date === null ? time() : strtotime($date);
        $numSequence = $this->Sequence->getNextValue(
            $date,
            $compteur['Compteur']['def_reinit'],
            $compteur['Compteur']['sequence_id']
        );

        // Retourne la valeur du compteur générée
        return $this->ecrireCompteur($compteur['Compteur']['def_compteur'], $date, $numSequence);
    }

    /**
     * Retourne un numéro d'acte à partir d'un template, de la date et  du numéro de séquence
     * @param string $defCompteur
     * @param int $date
     * @param int $num_sequence
     * @return string
     */

    public function ecrireCompteur($defCompteur, $date, $num_sequence)
    {
        // Initialisation du tableau de recherche et de remplacement pour la séquence
        $strnseqS = sprintf("%'_10d", $num_sequence);
        $strnseqZ = sprintf("%010d", $num_sequence);

        // Replacement de la définition du compteur avec la date
        $remplace = ["#AAAA#" => date("Y", $date),
          "#AA#" => date("y", $date),
          "#M#" => date("n", $date),
          "#MM#" => date("m", $date),
          "#J#" => date("j", $date),
          "#JJ#" => date("d", $date),
          "#s#" => $num_sequence,
          "#S#" => substr($strnseqS, -1, 1),
          "#SS#" => substr($strnseqS, -2, 2),
          "#SSS#" => substr($strnseqS, -3, 3),
          "#SSSS#" => substr($strnseqS, -4, 4),
          "#SSSSS#" => substr($strnseqS, -5, 5),
          "#SSSSSS#" => substr($strnseqS, -6, 6),
          "#SSSSSSS#" => substr($strnseqS, -7, 7),
          "#SSSSSSSS#" => substr($strnseqS, -8, 8),
          "#SSSSSSSSS#" => substr($strnseqS, -9, 9),
          "#SSSSSSSSSS#" => $strnseqS,
          "#0#" => substr($strnseqZ, -1, 1),
          "#00#" => substr($strnseqZ, -2, 2),
          "#000#" => substr($strnseqZ, -3, 3),
          "#0000#" => substr($strnseqZ, -4, 4),
          "#00000#" => substr($strnseqZ, -5, 5),
          "#000000#" => substr($strnseqZ, -6, 6),
          "#0000000#" => substr($strnseqZ, -7, 7),
          "#00000000#" => substr($strnseqZ, -8, 8),
          "#000000000#" => substr($strnseqZ, -9, 9),
          "#0000000000#" => $strnseqZ
        ];

        //Génération de la valeur du compteur
        return  str_replace(array_keys($remplace), array_values($remplace), $defCompteur);
    }

    public function whiteSpace(): bool
    {
        return !strpos($this->data['Compteur']['def_compteur'], ' ');
    }
}
