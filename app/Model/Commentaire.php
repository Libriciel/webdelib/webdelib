<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [Commentaire description]
 * @version 5.1.0
 * @since 4.3.0
 */
class Commentaire extends AppModel
{
    public $name = 'Commentaire';
    public $validate = ['texte' => [
            [
              'rule' => 'notBlank',
              'message' => 'Entrer un commentaire.']
            ]];
    public $belongsTo = [
        'User' => [
            'className' => 'User',
            'conditions' => '',
            'order' => '',
            'dependent' => false,
            'foreignKey' => 'agent_id'],
    ];

    /**
     * Fonction d'initialisation des variables de fusion pour les commentaires d'un projet/délibération
     * les bibliothèques Gedooo doivent être inclues par avance
     * génère une exception en cas d'erreur
     *
     * @param array $aData
     * @param object_by_ref $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param integer $deliberationId --> id du projet/délibération
     * @version 5.1.0
     * @since 4.3.0
     * @access public
     */
    public function setVariablesFusion(&$aData, &$oModelOdtInfos, $iDeliberationId)
    {
        if ($oModelOdtInfos->hasUserFieldDeclared('texte_commentaire')) {
            $aCommentaires = [];
            //lecture des commentaires
            //FIX
            $commentaires = $this->find('all', [
                'fields' => ['texte'],
                'contain' => ['User' => ['fields' => 'nom', 'prenom']],
                'conditions' => [
                    'delib_id' => $iDeliberationId,
                    'AND' => ['OR' => [
                        'commentaire_auto' => false,
                        'commentaire_auto IS NULL'
                    ]]
                ],
                'order' => ['Commentaire.created' => 'DESC'],
                'recursive' => -1,
            ]);

            if (empty($commentaires)) {
                return [self::getVariablesFusionEmpty()];
            }

            foreach ($commentaires as $commentaire) {
                $aCommentaires[] = [
                    'commentaire_nom' => [
                        'value' => $commentaire['User']['nom'],
                        'type' => 'text'],
                    'commentaire_prenom' => [
                        'value' => $commentaire['User']['prenom'],
                        'type' => 'text'],
                    'commentaire_texte' => [
                        'value' => $commentaire['Commentaire']['texte'],
                        'type' => 'text'],
                    //@deprecated 6.0
                    'texte_commentaire' => [
                        'value' => $commentaire['Commentaire']['texte'],
                        'type' => 'text']
                ];
            }

            $aData['Commentaires'] = $aCommentaires;
        }
    }

    /**
     * @version 5.1.0
     * @return type
     */
    private function getVariablesFusionEmpty()
    {
        return [
          'commentaire_nom' => [
              'value' => '',
              'type' => 'text'],
          'commentaire_prenom' => [
              'value' => '',
              'type' => 'text'],
          'commentaire_texte' => [
                'value' => '',
                'type' => 'text'],
          //@deprecated 6.0
          'texte_commentaire' => [
                'value' => '',
                'type' => 'text']
        ];
    }

    /**
     * @version 4.3
     * @access public
     * @param type $query
     * @return string
     */
    public function beforeFind($query)
    {
        // Préparation de la requete pour le afterFind
        if (isset($query['fields']) && !in_array('Commentaire.agent_id', $query['fields'], true)) {
            $query['fields'][] = 'Commentaire.agent_id';
        }

        if (isset($query['fields']) && !in_array('Commentaire.commentaire_auto', $query['fields'], true)) {
            $query['fields'][] = 'Commentaire.commentaire_auto';
        }
        return $query;
    }

    /**
     * @version 4.3
     * @access public
     * @param type $results
     * @param type $primary
     * @return string
     */
    public function afterFind($results, $primary = false)
    {
        foreach ($results as $key => $val) {
            if (isset($val['Commentaire']['agent_id']) && $val['Commentaire']['agent_id'] === -1) {
                $results[$key]['User']['prenom'] = "commentaire auto (Parapheur)";
                $results[$key]['User']['nom'] = '';
            } elseif (isset($val['Commentaire']['commentaire_auto'])
                && $val['Commentaire']['commentaire_auto'] === true
            ) {
                $results[$key]['User']['nom'] = 'commentaire auto';
                $results[$key]['User']['prenom'] = '';
            }
        }

        return $results;
    }

    /**
     * @version 4.3
     * @access public
     * @param type $projet_id
     * @param type $user_id
     * @param type $commentaire
     * @return type
     */
    public function enregistre($projet_id, $user_id, $commentaire)
    {
        if (!empty($commentaire)) {
            $this->create();
            $comment['Commentaire']['agent_id'] = $user_id;
            $comment['Commentaire']['delib_id'] = $projet_id;
            $comment['Commentaire']['texte'] = $commentaire;

            return $this->save($comment);
        }
    }
}
