<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [TdtMessage description]
 *
 * @version 5.1.4
 * @since 4.3.0
 */
class TdtMessage extends AppModel
{
    public $useTable = "tdt_messages";
    public $belongsTo = [
        'Deliberation' => [
            'foreignKey' => 'delib_id',
        ]
    ];
    public $hasMany = [
        'Reponse' => [
            'className' => 'TdtMessage',
            'foreignKey' => 'parent_id',
            'order' => 'tdt_id ASC',
            'dependent' => true],
    ];

    /**
     * @access public
     * @param type $data
     * @return type
     *
     * @version 5.1.4
     * @since 4.3.0
     */
    public function recupMessagePdfFromTar($data)
    {
        App::uses('AppTools', 'Lib');
        //Vérification si les données ne sont pas déjà du pdf (Message reponse Pastell)
        $infos = AppTools::fileMime($data, true, 'application/pdf');
        if ($infos['mimetype'] === 'application/pdf') {
            return ['filename' => 'Reponse.pdf', 'content' => $data];
        }

        $folder = new Folder(AppTools::newTmpDir(TMP . 'files' . DS . 'tdt'), true, 0777);
        $fileTgz = new File($folder->path . DS . 'WD_TDT_DOC.tgz', true, 0777);
        $fileTgz->write($data);
        $phar = new PharData($fileTgz->pwd());
        $phar->extractTo($folder->path);

        $files = $folder->find('.*\.pdf', true);
        foreach ($files as $file) {
            $file = new File($folder->pwd() . DS . $file);
            $content = $file->read();
            $name = $file->name;
        }
        $folder->delete();

        return ['filename' => $name, 'content' => $content];
    }
}
