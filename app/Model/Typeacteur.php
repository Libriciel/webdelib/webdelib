<?php

/**
 * Gestion des séquences utilisées par les compteurs paramétrables
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       1.0
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 * @version     5.0
 * @package     app.Model
 */
class Typeacteur extends AppModel
{
    public $name = 'Typeacteur';

    public $displayField = "nom";

    public $validate = [
        'nom' => [
            [
                'rule' => 'notBlank',
                'message' => 'Entrer un nom pour le type d\'acteur'
            ],
            [
                'rule' => 'isUnique',
                'message' => 'Entrer un autre nom, celui-ci est déjà utilisé.'
            ]
        ],
        'elu' => [
            'rule' => 'notBlank',
            'message' => 'Choisir un statut (élu ou non élu)'
        ]
    ];

    public $hasMany = ['Acteur'];

    /**
     * Retourne le libellé correspondant au champ elu 1 : élu, 0 : non élu
     *
     * @version 4.3
     * @access public
     * @param int $elu
     * @param string $majuscule
     * @return string
     */
    public function libelleElu($elu = null, $majuscule = false)
    {
        return $elu ? ($majuscule ? 'Elu' : 'élu') : ($majuscule ? 'Non élu' : 'non élu');
    }

    /**
     * Test la possibilité de supprimer un type d'acteur (le type est il affécté à un acteur ?)
     * @param integer $id --> identifiant du type d'acte à éliminer
     * @return boolean --> true si aucun acteur n'est associée à ce type d'acteur, false sinon
     */
    public function isDeletable($id)
    {
        $nbTypeActeurEnCours = $this->Acteur->find('count', [
            'conditions' => [
                'typeacteur_id' => $id,
                'actif' => true
            ],
            'recursive' => -1
        ]);

        return empty($nbTypeActeurEnCours);
    }
}
