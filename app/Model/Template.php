<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [Template description]
 */
class Template extends AppModel
{
    public $useTable = false;

    /**
     * [fileOutputFormat description]
     * @param  [type] $FormatOutput [description]
     * @param  [type] $options      [description]
     * @return [type]               [description]
     */
    public function fileOutputFormat($FormatOutput, $options)
    {
        // Initialisation du tableau de recherche et de remplacement pour la séquence
        $strnseqS = sprintf("%'_10d", $options['id']);
        $strnseqZ = sprintf("%010d", $options['id']);
        // Replacement de la définition du compteur avec la date
        $remplace = [
            '#T_N#' => $options['templateName'] ?? '',
            '#P_N#' => !empty($options['projetName']) ? mb_substr($options['projetName'], 0, 100, 'utf-8') : '',
            '#S_N#' => $options['seanceName'] ?? '',
            '#S_D#' => !empty($options['seanceDate'])
                ? CakeTime::i18nFormat($options['seanceDate'], '%d-%m-%Y') : '',
            '#S_DT#' => !empty($options['seanceDate'])
                ? CakeTime::i18nFormat($options['seanceDate'], '%d-%m-%Y_%Hh%M') : '',
            '#A_N#' => $options['acteurLastName'] ?? '',
            '#A_P#' => $options['acteurFirstName'] ?? '',
            "#D_AAAA#" => date("Y"),
            "#D_AA#" => date("y"),
            "#D_M#" => date("n"),
            "#D_MM#" => date("m"),
            "#D_J#" => date("j"),
            "#D_JJ#" => date("d"),
            "#i#" => $options['id'] ?? '',
            "#I#" => substr($strnseqS, -1, 1),
            "#II#" => substr($strnseqS, -2, 2),
            "#III#" => substr($strnseqS, -3, 3),
            "#IIII#" => substr($strnseqS, -4, 4),
            "#IIIII#" => substr($strnseqS, -5, 5),
            "#IIIIII#" => substr($strnseqS, -6, 6),
            "#IIIIIII#" => substr($strnseqS, -7, 7),
            "#IIIIIIII#" => substr($strnseqS, -8, 8),
            "#IIIIIIIII#" => substr($strnseqS, -9, 9),
            "#IIIIIIIIII#" => $strnseqS,
            "#0#" => substr($strnseqZ, -1, 1),
            "#00#" => substr($strnseqZ, -2, 2),
            "#000#" => substr($strnseqZ, -3, 3),
            "#0000#" => substr($strnseqZ, -4, 4),
            "#00000#" => substr($strnseqZ, -5, 5),
            "#000000#" => substr($strnseqZ, -6, 6),
            "#0000000#" => substr($strnseqZ, -7, 7),
            "#00000000#" => substr($strnseqZ, -8, 8),
            "#000000000#" => substr($strnseqZ, -9, 9),
            "#0000000000#" => $strnseqZ,
            "#s#" => $options['sequence'] ?? '',
            "#S#" => substr($strnseqS, -1, 1),
            "#SS#" => substr($strnseqS, -2, 2),
            "#SSS#" => substr($strnseqS, -3, 3),
            "#SSSS#" => substr($strnseqS, -4, 4),
            "#SSSSS#" => substr($strnseqS, -5, 5),
            "#SSSSSS#" => substr($strnseqS, -6, 6),
            "#SSSSSSS#" => substr($strnseqS, -7, 7),
            "#SSSSSSSS#" => substr($strnseqS, -8, 8),
            "#SSSSSSSSS#" => substr($strnseqS, -9, 9),
            "#SSSSSSSSSS#" => $strnseqS,
          ];

        return str_replace(array_keys($remplace), array_values($remplace), $FormatOutput);
    }

    /**
     * Retourne une liste de formats de sortie pour les fichiers
     *
     * @param string|null $modelName Nom du modèle pour filtrer la liste.
     * @return array Liste des formats de sortie disponibles.
     */
    public function aideFileOutputFormat($modelName = null)
    {
        $list = [
            '#i#' => [
                'label' => __('Identifiant'),
                'modeles_exclude' => ['MultiSeance'],
            ],
            '#I#' => [
                'label' => __('Identifiant sur 1 chiffre'),
                'modeles_exclude' => ['MultiSeance'],
            ],
            '#II#' => [
                'label' => __('Identifiant sur 2 chiffres (complété par un souligné)'),
                'modeles_exclude' => ['MultiSeance'],
            ],
            '#III#' => [
                'label' => __('Identifiant sur 3 chiffres (complété par des soulignés)'),
                'modeles_exclude' => ['MultiSeance'],
            ],
            '#IIII#' => [
                'label' => __('Identifiant sur 4 chiffres (complété par des soulignés)'),
                'modeles_exclude' => ['MultiSeance'],
            ],
            '#00#' => [
               'label' => __('Identifiant sur 2 chiffres (complété par un zéro)'),
                'modeles_exclude' => ['MultiSeance'],
            ],
            '#000#' => [
                'label' => __('Identifiant sur 3 chiffres (complété par des zéros)'),
                'modeles_exclude' => ['MultiSeance'],
            ],
            '#0000#' => [
                'label' => __('Identifiant sur 4 chiffres (complété par des zéros)'),
                'modeles_exclude' => ['MultiSeance'],
            ],
            '#T_N#' => __('Libellé du modèle'),
            '#P_N#' => [
              'label' => __('Libellé du projet'),
              'modeles' => ['Projet', 'Deliberation', 'Recherche', 'BordereauProjet'],
            ],
            '#S_N#' => [
              'label' => __('Type de séance'),
              'modeles' => ['Convocation', 'OrdreDuJour', 'PvSommaire', 'PVDetaille', 'JournalSeance'],
            ],
            '#S_D#' => [
              'label' => __('Date de séance (jj-mm-aaaa)'),
              'modeles' => ['Convocation', 'OrdreDuJour', 'PVSommaire', 'PvDetaille', 'JournalSeance'],
            ],
            '#S_DT#' => [
              'label' => __('Date de séance (jj-mm-aaaa_00h00)'),
              'modeles' => ['Convocation', 'OrdreDuJour', 'PVSommaire', 'PvDetaille', 'JournalSeance'],
            ],
            '#A_N#' => [
              'label' => __('Nom de l\'acteur'),
              'modeles' => ['OrdreDuJour', 'Convocation'],
            ],
            '#A_P#' => [
              'label' => __('Prénom de l\'acteur'),
              'modeles' => ['OrdreDuJour', 'Convocation'],
            ],
            '#D_AAAA#' => __('Année sur 4 chiffres'),
            '#D_AA#' => __('Année sur 2 chiffres'),
            '#D_M#' => __('Numéro du mois sans zéro significatif'),
            '#D_MM#' => __('Numéro du mois avec zéro significatif'),
            '#D_J#' => __('Numéro du jour sans zéro significatif'),
            '#D_JJ#' => __('Numéro du jour avec zéro significatif'),
            '#s#' => [
                'label' => __('Séquence'),
                'modeles_exclude' => ['MultiSeance'],
            ],
            '#S#' => [
                'label' => __('Séquence sur 1 chiffre'),
                'modeles_exclude' => ['MultiSeance'],
            ],
            '#SS#' => [
                'label' => __('Séquence sur 2 chiffres (complété par un souligné)'),
                'modeles_exclude' => ['MultiSeance'],
            ],
            '#SSS#' => [
                'label' => __('Séquence sur 3 chiffres (complété par des soulignés)'),
                'modeles_exclude' => ['MultiSeance'],
            ],
            '#SSSS#' => [
                'label' => __('Séquence sur 4 chiffres (complété par des soulignés)'),
                'modeles_exclude' => ['MultiSeance'],
            ],
        ];

        // Filtrage de la liste selon le modèle passé en paramètre
        foreach ($list as $key => &$value) {
            if (is_array($value)) {
                // Vérifie si le modèle doit être exclu ou inclus
                if (!empty($modelName)) {
                    // Exclure si le modèle est dans 'modeles_exclude' ou s'il n'est pas dans 'modeles'
                    // (s'il existe)
                    if ((isset($value['modeles_exclude']) && in_array($modelName, $value['modeles_exclude'], true))
                        ||
                        (isset($value['modeles']) && !in_array($modelName, $value['modeles'], true))) {
                        unset($list[$key]);
                        continue;
                    }
                }

                $value = $value['label'];
            }
        }


        return $list;
    }
}
