<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class Theme extends AppModel
{
    public $name = 'Theme';
    public $displayField = "libelle";
    public $actsAs = ['Tree'];
    public $validate = [
        'libelle' => [
            [
                'rule' => 'notBlank',
                'message' => 'Entrer le libellé.'
            ]
        ]
    ];

    /**
     * @version 4.3
     * @access public
     * @param type $theme_id
     * @param type $level
     * @return type
     */
    public function getLevel($theme_id, $level = 1)
    {
        $theme = $this->find('first', ['conditions' => ['Theme.id' => $theme_id],
            'recursive' => -1,
            'fields' => ['id', 'parent_id']]);

        if ($theme['Theme']['parent_id'] == 0) {
            return ($level);
        } else {
            $level = $level + 1;
            return ($this->getLevel($theme['Theme']['parent_id'], $level));
        }
    }

    /**
     * @version 4.3
     * @access public
     * @param type $theme_id
     * @param type $level
     * @param array $tableau
     * @return type
     */
    public function getLibelleParent($theme_id, $level = 1, $tableau = [])
    {
        $theme = $this->find('first', ['conditions' => ['Theme.id' => $theme_id],
            'recursive' => -1,
            'fields' => ['id', 'parent_id', 'libelle']]);
        $tableau[$level] = $theme['Theme']['libelle'];
        if ($theme['Theme']['parent_id'] == 0) {
            return ($tableau);
        } else {
            $level = $level + 1;
            return ($this->getLibelleParent($theme['Theme']['parent_id'], $level, $tableau));
        }
    }

    /**
     * Fonction d'initialisation des variables de fusion pour le thème d'un projet
     * les bibliothèques Gedooo doivent être inclues par avance
     * génère une exception en cas d'erreur
     *
     * @version 4.3
     * @access public
     * @param type $aData
     * @param object_by_ref $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param integer $id --> id du modèle lié
     */
    public function setVariablesFusion(&$aData, &$modelOdtInfos, $id)
    {
        $theme = $this->find('first', [
            'recursive' => -1,
            'fields' => ['libelle', 'order', 'lft', 'rght'],
            'conditions' => ['Theme.id' => $id]]);

        if ($modelOdtInfos->hasUserFieldDeclared('theme_projet')) {
            $aData['theme_projet'] = ['value' => $theme['Theme']['libelle'], 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('critere_trie_theme')) {
            $aData['critere_trie_theme'] = ['value' => $theme['Theme']['order'], 'type' => 'text'];
        }

        // arborescence des thèmes jusqu'au 10eme niveau
        $libelleThemesLevel = array_fill(1, 10, '');
        $themes = $this->find('all', [
            'recursive' => -1,
            'fields' => ['libelle'],
            'conditions' => ['lft <=' => $theme['Theme']['lft'], 'rght >=' => $theme['Theme']['rght']],
            'order' => ['lft']]);
        foreach ($themes as $i => $theme) {
            $libelleThemesLevel[$i + 1] = $theme['Theme']['libelle'];
        }

        foreach ($libelleThemesLevel as $level => $libelleThemeLevel) {
            if (!empty($libelleThemeLevel) && $modelOdtInfos->hasUserFieldDeclared("T" . $level . "_theme")) {
                $aData["T" . $level . "_theme"] = ['value' => $libelleThemeLevel, 'type' => 'text'];
            }
        }
        foreach ($libelleThemesLevel as $level => $libelleThemeLevel) {
            if ($modelOdtInfos->hasUserFieldDeclared("theme" . $level . "_projet")) {
                $aData["theme" . $level . "_projet"] = [
                    'value' => (!empty($libelleThemeLevel) ? $libelleThemeLevel : ''),
                    'type' => 'text'
                ];
            }
        }
    }

    /**
     * @param $threaded
     * @param $modelname
     * @param array $fields ex : array('id' => 'identifiant', 'display' => 'libelle', 'order' => 'ordre')
     * @param int $level
     * @return string
     */

    public function generateTreeListByOrder($conditions, $spacer)
    {
        $themes = $this->find('threaded', [
            'conditions' => $conditions,
            'order' => 'Theme.order ASC',
            'recursive' => -1]);

        return $this->generateTreeListThemesByOrder($themes, $spacer);
    }
    /**
     * @param $threaded
     * @param $modelname
     * @param array $fields ex : array('id' => 'identifiant', 'display' => 'libelle', 'order' => 'ordre')
     * @param int $level
     * @return string
     */

    public function generateTreeListThemesByOrder($threaded, $spacer = '_', $level = 0)
    {
        $treelist = [];
        foreach ($threaded as $thread) {
            $treelist[$thread[$this->name]['id']] = str_repeat($spacer, $level) . $thread[$this->name]['libelle'];

            if (!empty($thread['children'])) {
                $treelist += $this->generateTreeListThemesByOrder($thread['children'], $spacer, $level + 1);
            }
        }

        return ($treelist);
    }
}
