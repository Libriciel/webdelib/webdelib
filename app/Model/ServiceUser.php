<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class ServiceUser extends AppModel
{
    public $useTable = 'services_users';
    public $belongsTo = [
        'User' =>
        ['className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => ''
        ],
        'Service' =>
        ['className' => 'Service',
            'foreignKey' => 'service_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => ''
        ]
    ];

    /**
     * Fusionne 2 service entre eux
     *
     * @version 4.3
     * @access public
     * @param int $fusionId --> Service à fusionner
     * @param int $id --> Service cible
     * @throws Exception
     */
    public function fusion($fusionId, $id)
    {
        if (!$this->Service->exists($fusionId) or ! $this->Service->exists($id)) {
            throw new Exception(__('Invalide ids pour fusionner le service'));
        }

        if ($fusionId == $id) {
            throw new Exception(__('Impossible de fusionner le même service'));
        }

        $service = $this->Service->find('first', [
            'conditions' => ['parent_id' => $fusionId, 'actif' => true],
            'recursive' => -1]);

        if (!empty($service)) {
            throw new Exception(__('Impossible de fusionner ce service : il possède au moins un service'));
        }

        try {
            //Liste des utilisateurs à fusionner dans le service cible
            $users = $this->find('list', [
                'fields' => ['ServiceUser.user_id'],
                'conditions' => ['service_id' => $fusionId, 'service_id <>' => $id],
                'recursive' => -1,
                'order' => 'id ASC']);

            $this->begin();
            foreach ($users as $key => $user) {
                $this->delete($key);
                $userservice = $this->findByUserIdAndServiceId($user, $id);
                if (empty($userservice)) {
                    $this->create();
                    $this->save(['user_id' => $user, 'service_id' => $id]);
                }
            }
            $this->commit();

            //Désactivation du service
            $this->Service->id = $fusionId;
            $this->Service->saveField('actif', false);
        } catch (Exception $e) {
            $this->rollback();
            throw new Exception($e);
        }
    }

    /**
     * [setServiceUserPermissions Ajouter les services non disponibles pour le manager]
     * @param [type] $serviceSaveIds [description]
     * @param [type] $manager  [description]
     * @param [type] $userId  [description]
     */
    public function setServiceUserPermissions($serviceSaveIds, $manager, $userId)
    {
        if ($manager && !empty($userId)) {
            $serviceActifIds = $this->find('list', [
              'fields' => ['service_id'],
              'joins' => [$this->join(
                  'Service',
                  [
                    'type' => 'INNER',
                    'conditions' => ['Service.actif' => 1]
                  ]
              )
              ],
              'conditions' => ['user_id' => $userId],
              'recursive' => -1
            ]);
            $serviceManagerAllowIds = $this->Service->find('list', [
              'fields' => ['id'],
              'conditions' => ['Service.actif' => 1],
              'recursive' => -1,
              'allow' => ['Service.id'],
            ]);
            $servicesAAjouter = array_diff($serviceActifIds, $serviceManagerAllowIds);
            $serviceSaveIds = array_merge($serviceSaveIds, $servicesAAjouter);
        }

        return $serviceSaveIds;
    }
}
