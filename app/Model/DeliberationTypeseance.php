<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * Liaison entre Déliberations et Types de séance.
 *
 * @version 5.1.0
 * @since 4.3.0
 *
 * @SuppressWarnings(PHPMD)
 */
class DeliberationTypeseance extends AppModel
{
    /**
     * [public description]
     * @var [type]
     */
    public $name = 'DeliberationTypeseance';
    /**
     * [public description]
     * @var [type]
     */
    public $useTable = 'deliberations_typeseances';
    /**
     * [public description]
     * @var [type]
     */
    public $belongsTo = [
        'Deliberation' => [
            'className' => 'Deliberation',
            'foreignKey' => 'deliberation_id'
            ],
        'Typeseance' => [
            'className' => 'Typeseance',
            'foreignKey' => 'typeseance_id'
            ],
        ];

    /**
     * [public description]
     * @var [type]
     */
    public $validate = [
        'typeseance_id' => [
              [
                'rule' => ['canSaveTypeseanceDeliberante'],
                'message' => 'Vous ne pouvez affecter un projet que sur une seule séance de type délibérante'
              ],
        ],
    ];

    /**
     * @version 4.3
     * @access public
     * @param type $query
     * @return type
     */
    public function beforeFind($query = [])
    {
        //Gestion des droits sur les types de seances
        if (!empty($query['allow'])) {
            $query['conditions'] = (is_array($query['conditions'])) ? $query['conditions'] : [];
            $query['joins'] = $query['joins'] + [
              $this->join('Typeseance', ['type' => 'INNER'])
            ];

            //Gestion des droits sur les types d'actes
            $conditions = parent::allowAcl($query['allow'], 'Typeseance', 'typeseance_id');
            if ($conditions !== false) {
                $query['conditions'] = array_merge($query['conditions'], $conditions);
            }
        }
        return $query;
    }

    /**
     * [beforeValidate description]
     * @param  array  $options [description]
     * @return [type]          [description]
     * @version 5.1.0
     */
    public function beforeValidate($options = [])
    {
        return true;
    }

    /**
     * [beforeSave description]
     * @param  array  $options [description]
     * @return [type]          [description]
     * @version 5.1.0
     */
    public function beforeSave($options = [])
    {
        return true;
    }

    /**
     * [saveDeliberationTypeseance description]
     * @param  [type] $data [description]
     * @return [type]       [description]
     *
     * @version 5.1.0
     */
    public function saveDeliberationTypeseance($data)
    {
        $sucess = true;
        $validationErrors = [];

        //Vérification des types de séances
        if (!empty($data['DeliberationTypeseance'])) {
            $nbrTypeSeanceDeliberante = $this->nbrTypeSeanceDeliberante($data);
            if ($nbrTypeSeanceDeliberante > 1) {
                $this->validationErrors[$this->alias] = Set::extract('/typeseance_id/message', $this->validate);
                return false;
            }
        }

        if (!empty($data['Deliberation']['id']) && isset($data['DeliberationTypeseance'])) {
            if (!empty($data['DeliberationTypeseance'])) {
                foreach ($data['DeliberationTypeseance'] as $key => $deliberationTypeseance) {
                    $data['DeliberationTypeseance'][$key] =
                        [
                            'deliberation_id' => $data['Deliberation']['id'],
                            'typeseance_id' => $deliberationTypeseance,
                        ];
                }
                //debug($data['DeliberationTypeseance']);
                ///$debug = $this->saveMany($data['DeliberationTypeseance'], ['validate'=>'only','atomic'=>false]);
                // var_dump($this->validationErrors);
                $selectedTypeseanceIds = Set::extract('/typeseance_id', $data['DeliberationTypeseance']);
            }

            //Supression des séances
            $nbr_types_seances = $this->find(
                'count',
                [
                    'conditions' => ['deliberation_id' => $data['Deliberation']['id']],
                    'recursive' => -1
                ]
            );
            $nbr_types_seances_not_read = $this->find(
                'count',
                [
                    'conditions' => ['deliberation_id' => $data['Deliberation']['id']],
                    'recursive' => -1,
                    'allow' => ['typeseance_id'=> 'read <>'],
                ]
            );
            if ($nbr_types_seances_not_read === 0 && empty($data['DeliberationTypeseance']) && $nbr_types_seances > 0) {
                $this->deleteAll(['deliberation_id' => $data['Deliberation']['id']]);
            }

            if (!empty($data['DeliberationTypeseance'])) {
                //Suppression des séances pour lesquelles l'tilisateur a accès
                foreach ($data['DeliberationTypeseance'] as $key => $deliberationTypeseance) {
                    $deleteTypeseanceIds = $this->find(
                        'all',
                        [
                            'fields' => ['id'],
                            'conditions' => [
                              'deliberation_id' => $data['Deliberation']['id'],
                              'typeseance_id NOT' => $selectedTypeseanceIds,
                            ],
                            'recursive' => -1,
                            'allow' => ['Typeseance.id'=> 'read'],
                        ]
                    );

                    if (!empty($deleteTypeseanceIds)) {
                        $deleteTypeseanceIds = Set::extract('/DeliberationTypeseance/id', $deleteTypeseanceIds);
                        foreach ($deleteTypeseanceIds as $key => $deleteTypeseanceId) {
                            $deleteTypeseanceIds[$key]=['DeliberationTypeseance.id' => $deleteTypeseanceId];
                        }
                        $this->deleteAll($deleteTypeseanceIds, false, false);
                    }
                }

                //Ajout des séances pour lesquelles l'tilisateur a accès
                foreach ($data['DeliberationTypeseance'] as $key => $deliberationTypeseance) {
                    if (empty($this->findByTypeseanceIdAndDeliberationId(
                        $deliberationTypeseance['typeseance_id'],
                        $deliberationTypeseance['deliberation_id']
                    ))) {
                        $this->create();
                        if ($this->save($deliberationTypeseance) === false) {
                            $sucess = false;
                            $validationErrors[] = $this->validationErrors;
                        }
                        $this->clear();
                    }
                }
            }
        }

        if ($sucess === false) {
            $this->validationErrors[$this->alias] = $validationErrors[0]['typeseance_id'];
        }

        return $sucess;
    }

    /**
     * [saveDeliberationTypeseanceDelibRattachees description]
     * @param  [type] $projetParentId [description]
     * @param  [type] $projetId       [description]
     * @return [type]                 [description]
     *
     * @version 5.1.0
     */
    public function saveDeliberationTypeseanceDelibRattachees($projetParentId, $projetId)
    {

        //$DeliberationTypeseance = ClassRegistry::init('DeliberationTypeseance');

        $this->recursive = -1;
        $parent_typeseances = $this->findAllByDeliberationId($projetParentId);

        $this->deleteAll(['deliberation_id' => $projetId]);
        foreach ($parent_typeseances as $key => $parent_typeseance) {
            $this->create();
            $this->save(
                [
                    'deliberation_id' => $projetId,
                    'typeseance_id' => $parent_typeseance['DeliberationTypeseance']['typeseance_id']
                ],
                false
            );
            $this->clear();
        }

        return true;
    }


    private function nbrTypeSeanceDeliberante($data)
    {
        $count = 0;
        foreach ($data['DeliberationTypeseance'] as $typeseance_id) {
            if ($this->Typeseance->isDeliberante($typeseance_id)) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * Vérification type de séance délibérante
     *
     * @access public
     *
     * @param array $seances
     * @return boolean
     *
     * @version 5.1.0
     */
    public function canSaveTypeseanceDeliberante()
    {
        if (!empty($this->data['DeliberationTypeseance']['typeseance_id'])
            && !empty($this->data['DeliberationTypeseance']['deliberation_id'])
        ) {
            $isDeliberante = $this->Typeseance->isDeliberante($this->data['DeliberationTypeseance']['typeseance_id']);
            if (!$isDeliberante) {
                return true;
            }
            $nbr_deliberante = $this->find('count', [
                'fields' => ['id','Typeseance.action','deliberation_id','typeseance_id'],
                'joins' => [$this->join('Typeseance', ['type' => 'INNER', 'conditions' => ['action' => 0]])],
                'conditions' => [
                  'typeseance_id <>' => $this->data['DeliberationTypeseance']['typeseance_id'],
                  'deliberation_id' => $this->data['DeliberationTypeseance']['deliberation_id']
                ],
                'recursive' => -1
            ]);

            return $nbr_deliberante > 0 ? false : true;
        }

        return true;
    }

    // /**
    //  * [afterValidate description]
    //  * @param  array  $options [description]
    //  * @return [type]          [description]
    //  * @version 5.1.0
    //  */
    // public function afterValidate($options = [])
    // {
    //     //debug($this->validationErrors['typeseance_id']);
    //     $this->validationErrors['Typeseance'] =   $this->validationErrors['typeseance_id'];
    //     unset($this->validationErrors['typeseance_id']);
    //
    //     return true;
    // }
}
