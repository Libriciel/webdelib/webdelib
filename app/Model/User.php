<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::import('Vendor', 'wdPasswordStrengthMeterAnssi', ['file' => 'wd_password_strength_meter_anssi.php']);

/**
 * [User description]
 *
 * @SuppressWarnings(PHPMD)
 */
class User extends AppModel
{
    public $name = 'User';
    public $validate = [
        'username' => [
            [
                'required' => true,
                'rule' => 'notBlank',
                'message' => 'Entrez le login.'
            ],
            [
                'rule' => 'isUnique',
                'message' => 'Entrez un autre login, celui-ci est déjà utilisé.'
            ]
        ],
        'password' => [
            [
                'required' => true,
                'rule' => 'notBlank',
                'message' => 'Entrez un mot de passe.'
            ],
            [
                'rule' => ['samePassword'],
                'message' => 'Le nouveau mot de passe ne doit pas être différent de la confirmation du mot de passe.'

            ],
            [
                'rule' => 'checkPasswordStrength',
                'message' => 'Veuillez choisir un mot de passe plus complexe.'
            ]
        ],
        'nom' => [
            [
                'required' => true,
                'rule' => 'notBlank',
                'message' => 'Entrez le nom.'
            ]
        ],
        'prenom' => [
            [
                'required' => true,
                'rule' => 'notBlank',
                'message' => 'Entrez le prénom.'
            ]
        ],
        'email' => [
            [
                'rule' => 'emailDemande',
                'message' => 'Entrez l\'email.'
            ],
            [
                'rule' => 'email',
                'allowEmpty' => true,
                'message' => 'Adresse email non valide.'
            ]
        ],
        'profil_id' => [
            [
                'rule' => 'notBlank',
                'message' => 'Selectionner le profil utilisateur'
            ]
        ]
    ];
    public $displayField = "nom";
    public $belongsTo = [
        'Profil' => [
            'className' => 'Profil',
            'conditions' => '',
            'order' => '',
            'dependent' => false,
            'foreignKey' => 'profil_id']
    ];
    public $hasOne = [
        'Aro' => [
            'className' => 'Aro',
            'foreignKey' => false,
            'conditions' => [
                'User.id = Aro.foreign_key',
                'Aro.model' => 'User'
            ],
            'dependent' => false
        ]
    ];

    public $hasMany = [
        'Historique' => [
            'className' => 'Historique',
            'foreignKey' => 'user_id'
        ],
        'Composition' => [
            'className' => 'Cakeflow.Composition',
            'foreignKey' => 'trigger_id'
        ],
        'Deliberation' => [
            'className' => 'DeliberationUser',
            'foreignKey' => 'user_id'
        ]
    ];

    public $hasAndBelongsToMany = [
        'Service' => [
            'classname' => 'Service',
            'joinTable' => 'services_users',
            'foreignKey' => 'user_id',
            'associationForeignKey' => 'service_id',
            'conditions' => '',
            'order' => '',
            'limit' => '',
            'unique' => true,
            'finderQuery' => '',
            'deleteQuery' => ''],
        'Circuit' => [
            'className' => 'Cakeflow.Circuit',
            'joinTable' => 'circuits_users',
            'foreignKey' => 'user_id',
            'associationForeignKey' => 'circuit_id',
            'conditions' => '',
            'order' => '',
            'limit' => '',
            'unique' => true,
            'finderQuery' => '',
            'deleteQuery' => ''],
        'Deliberation' => [
            'className' => 'Deliberation',
            'foreignKey' => 'user_id',
            'joinTable' => 'deliberations_users',
            'associationForeignKey' => 'deliberation_id',
            'unique' => true,
            'conditions' => '',
            'fields' => 'Deliberation.id',
            //'order' => 'User.nom ASC',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => ''
        ]
    ];


    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->Behaviors->load('AuthManager.AclManager', ['type' => 'requester', 'loadBehavior' => 'DynamicDbConfig']);
    }

    /**
     * @access public
     * @return bool
     */
    public function notSameOldPassword()
    {
        return (
            !empty($this->data['User']['password'])
            && $this->data['User']['oldPassword'] != $this->data['User']['password']
        );
    }

    /**
     * @access public
     * @return bool
     */
    public function samePassword()
    {
        return (
            !empty($this->data['User']['password'])
            && $this->data['User']['passwordConfirm'] == $this->data['User']['password']
        );
    }

    /**
     * @access public
     * @param type $data
     * @return bool
     */
    public function sameOldPassword()
    {
        $oldPass = $this->find(
            'first',
            ['conditions' => ['id' => AuthComponent::user('id')], 'fields' => ['password'], 'recursive' => -1]
        );

        return md5($this->data['User']['oldPassword']) == $oldPass['User']['password'];
    }

    /**
     * @param $id
     * @return array|int[]|mixed
     */
    public function getMinUserPasswordEntropy()
    {
        $collectivite = ClassRegistry::init('Collectivite');
        $collectivite->recursive = -1;
        $collective = $collectivite->read(['force'], 1);

        return $collective['Collectivite']['force'];
    }

    public function checkPasswordStrength($data)
    {
        return (wdPasswordStrengthMeterAnssi::strength($data['password']) >= $this->getMinUserPasswordEntropy());
    }

    /**
     * @param array $data
     * @return boolean
     * @version 7
     * @access public
     */
    public function savePassword($data)
    {
        $this->validate = [];
        $defaultRules = [
            [
                'rule' => 'notBlank',
                'message' => 'Entrez un mot de passe.'
            ],
        ];
        $this->validator()->add('oldPassword', array_merge($defaultRules, [
            [
                'rule' => ['sameOldPassword'],
                'message' => 'L\'ancien mot de passe n\'est pas le correct.'
            ]
        ]));
        $this->validator()->add('password', array_merge($defaultRules, [
            [
                'rule' => ['notSameOldPassword'],
                'message' => 'Le nouveau mot de passe n\'est pas différent de l\'ancien mot de passe.'
            ],
            [
                'rule' => ['samePassword'],
                'message' => 'Le nouveau mot de passe ne doit pas être différent de la confirmation du mot de passe.'

            ],
            [
                'rule' => 'checkPasswordStrength',
                'message' => 'Veuillez choisir un mot de passe plus complexe.'
            ]
        ]));

        $this->validator()->add('passwordConfirm', $defaultRules);

        return parent::save($data, true, ['password', 'oldPassword', 'passwordConfirm']);
    }

    /**
     * @access public
     * @return bool
     */
    public function emailDemande()
    {
        return !(!empty($this->data['User']['accept_notif']) && empty($this->data['User']['email']));
    }

    /**
     * @version 4.3
     * @access public
     * @param type $options
     * @return boolean
     */
    public function beforeSave($options = [])
    {
        if (array_key_exists('password', $this->data['User'])) {
            $this->data['User']['password'] = md5($this->data['User']['password']);
        }

        return true;
    }

    /**
     * @version 4.3
     * @access public
     * @param type $options
     * @return boolean
     */
    public function beforeValidate($options = [])
    {
        if (!empty($options['request']) && empty($this->data['Service']['Service'])) {
            $this->invalidate('Service', true);
        }

        return true;
    }

    /**
     * @version 4.3
     * @access public
     * @param type $query
     * @return type
     */
    public function beforeFind($query = [])
    {
        $query['conditions'] = (is_array($query['conditions'])) ? $query['conditions'] : [];
        $db = $this->getDataSource();

        //Gestion des droits sur les types de services
        //  'allow' => array()
        if (!empty($query['allow'])
            && (
                in_array('Service.id', $query['allow'], true)
                || array_key_exists('Service.id', $query['allow'])
            )
        ) {
            if (in_array('Service.id', $query['allow'], true)) {
                $permission = ['Permission._' . $query['allow']['Service.id'] => 1];
            }
            if (array_key_exists('Service.id', $query['allow'])) {
                $permission = ['Permission._' . $query['allow']['Service.id'] => 1];
            }
            $Aro = ClassRegistry::init(['class' => 'Aro', 'alias' => 'AllowAro']);
            $Aro->Behaviors->attach('Database.DatabaseTable');
            $Aro->Permission->Behaviors->attach('Database.DatabaseTable');
            $Aro->Permission->Aco->Behaviors->attach('Database.DatabaseTable');
            $Aro->Permission->Aco->bindModel(
                ['belongsTo' => [
                            'ServiceUser' => [
                                'className' => 'ServiceUser',
                                'foreignKey' => false,
                                'conditions' => [
                                    'Aco.model' => 'Service',
                                    'AND' => ['OR' => [
                                            'ServiceUser.service_id = Aco.foreign_key',
                                            // Pour la gestion des sous services
                                            'ServiceUser.service_id = Children.foreign_key']]
                                ],
                            ]
                        ],
                        // Pour la gestion des sous services
                        'hasMany' => [
                            'Children' => [
                                'className' => 'Aco',
                                'foreignKey' => 'parent_id'
                            ]
                        ]
                    ]
            );


            $subQuery = [
                'fields' => [
                    'DISTINCT User.id'
                ],
                'contain' => false,
                //'alias'=>'AllowAro',
                'joins' => [
                    $Aro->join('Permission', ['type' => 'INNER']),
                    $Aro->Permission->join('Aco', ['type' => 'INNER']),
                    $Aro->Permission->Aco->join('Children', ['type' => 'LEFT']),
                    $Aro->Permission->Aco->join('ServiceUser', ['type' => 'INNER']),
                    $Aro->Permission->Aco->ServiceUser->join('User', ['type' => 'INNER']),
                ],
                'conditions' => [
                    'AllowAro.foreign_key' => AuthComponent::user('id'),
                    'Aco.model' => 'Service',
                    $permission
                ]
            ];
            // $subQuery['conditions'] = array_merge($subQuery['conditions'], $permission);


            $subQuery = $Aro->sql($subQuery);
            $subQuery = ' "' . $this->alias . '"."id" IN (' . $subQuery . ') ';
            $subQueryExpression = $db->expression($subQuery);
            $conditions[] = $subQueryExpression;

            $query['conditions'] = array_merge($query['conditions'], $conditions);
        }

        if (!empty($query['allow'])
            && (
                in_array('Role.id', $query['allow'], true)
                || array_key_exists('Role.id', $query['allow'])
            )
        ) {
            $subQuery = [
                'fields' => [
                    'DISTINCT User.id'
                ],
                'joins' => [
                    $this->join('Profil', ['type' => 'INNER']),
                ],
                'conditions' => [
                    'Profil.role_id' => $query['allow']['Role.id']
                ]
            ];

            $subQuery = $this->sql($subQuery);
            $subQuery = ' "' . $this->alias . '"."id" IN (' . $subQuery . ') ';
            $subQueryExpression = $db->expression($subQuery);
            $conditions[] = $subQueryExpression;

            $query['conditions'] = array_merge($query['conditions'], $conditions);
        }
        //FIX
        if (!empty($query['allow'])
            && (in_array('Deliberations/mesProjetsATraiter', $query['allow'], true)
            || array_key_exists('Deliberations/mesProjetsATraiter', $query['allow']))
        ) {
            //Gestion des droits sur les types d'actes
            $conditions = parent::allowAcl($query['allow'], $this->alias, 'Typeacte.id');
            if ($conditions !== false) {
                $query['conditions'] = array_merge($query['conditions'], $conditions);
            }
        }

        return $query;
    }

    /**
     * Retourne le circuit par défaut défini pour l'utilisateur $id
     * Si l'utilisateur n'a pas de circuit par défaut, retourne le circuit défini
     * au niveau du premier service de l'utilisateur.
     * Si $field est vide alors retourne la structure de la classe circuit.
     * Si $field est spécifiée, retourne la valeur du champ $field.
     *
     * @version 4.3
     * @access public
     * @param type $id
     * @param type $field
     * @return type
     */
    public function circuitDefaut($id = null, $field = '', $active = null)
    {
        $circuitDefautId = 0;
        $user = $this->find('first', [
            'conditions' => [
                'User.id' => $id],
            'contain' => ['Service'],
            'recursive' => -1
        ]);
        // Circuit par défaut défini au niveau de l'utilisateur
        if (!empty($user['User']['circuit_defaut_id'])) {
            $circuitDefautId = $user['User']['circuit_defaut_id'];
        } else {
            // Premier circuit par défaut défini pour les services de l'utilisateur
            foreach ($user['Service'] as $service) {
                if (!empty($service['circuit_defaut_id'])) {
                    $circuitDefautId = $service['circuit_defaut_id'];
                    break;
                }
            }
        }
        if ($circuitDefautId > 0) {
            $conditions = ['Circuit.id' => $circuitDefautId];
            if (isset($active) && $active !== null) {
                $conditions['Circuit.actif'] = $active;
            }
            $circuit = $this->Composition->Etape->Circuit->find('first', [
                'conditions' => $conditions,
                'recursive' => -1
            ]);
            if (empty($field) && !empty($circuit)) {
                return $circuit;
            } elseif (!empty($circuit['Circuit'][$field])) {
                return $circuit['Circuit'][$field];
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Retourne le prenom, nom et (username) de l'utilisateur $id
     *
     * @version 4.3
     * @access public
     * @param type $id
     * @return string
     */
    public function prenomNomLogin($id)
    {
        $this->recursive = -1;
        $this->data = $this->read('prenom, nom, username', $id);
        if (empty($this->data)) {
            return '';
        } else {
            return $this->data['User']['prenom']
                . ' ' . $this->data['User']['nom']
                . ' (' . $this->data['User']['username'] . ')';
        }
    }
    /**
     * Retourne le prenom, nom de l'utilisateur $id
     *
     * @version 4.3
     * @access public
     * @param type $id
     * @return string
     */
    public function prenomNom($id)
    {
        $this->recursive = -1;
        $this->data = $this->read('prenom, nom', $id);
        if (empty($this->data)) {
            return '';
        } else {
            return $this->data['User']['prenom'] . ' ' . $this->data['User']['nom'];
        }
    }

    /**
     * @version 4.3
     * @access public
     * @param type $user_id
     * @return type
     */
    public function getCircuits($user_id)
    {
        $circuits = [];
        $user = $this->find('first', [
            'contain' => [
                'Circuit' => ['fields' => [
                        'Circuit.id',
                        'Circuit.nom',
                        'Circuit.actif'
                    ],
                    'order' => ['Circuit.nom' => 'ASC']
                ]
            ],
            'conditions' => ['User.id' => $user_id],
            'recursive' => -1]);
        foreach ($user['Circuit'] as $circuit) {
            if ($circuit['actif']) {
                $circuits[$circuit['id']] = $circuit['nom'];
            }
        }
        return $circuits;
    }

    /**
     * Envoi une notification par mail à un utilisateur sur l'état d'un dossier
     *
     * @version 5.1
     * @version 4.3
     * @access public
     * @param integer $delib_id --> identifiant du dossier
     * @param integer $user_id -->  identifiant de l'utilisateur à notifier
     * @param string $type --> notification à envoyer
     * @return boolean --> succès de l'envoi
     */
    public function notifier($delib_id, $user_id, $type)
    {
        App::uses('Deliberation', 'Model');
        App::uses('Seance', 'Model');
        App::uses('CakeEmail', 'Network/Email');

        $user = $this->find('first', [
            'fields' => ['nom', 'prenom', 'email', 'accept_notif', 'mail_' . $type],
            'conditions' => ['id' => $user_id],
            'recursive' => -1,
        ]);
        // utilisateur existe et accepte les mails ?
        if (empty($user)
            || empty($type)
            || empty($user['User']['accept_notif'])
            || empty($user['User']['email'])
            || empty($user['User']["mail_$type"])
        ) {
            $message = "Utilisateur id ".$user_id." inexistant ou n'acceptant pas les mails: "
                . $type." (".$user['User']['email'].")\n";
            $this->log($message, 'error');
            return false;
        }

        $config_mail = 'default';
        $Email = new CakeEmail($config_mail);

        $this->Deliberation = ClassRegistry::init('Deliberation');
        $delib = $this->Deliberation->find('first', [
            'fields' => ['Deliberation.id', 'objet', 'titre', 'circuit_id', 'anterieure_id'],
            'conditions' => ['Deliberation.id' => $delib_id],
            'contain' => [
                'Commentaire' => [
                    'fields' => ['texte'],
                    'conditions' => ['OR' => [
                                ['commentaire_auto' => null], ['commentaire_auto' => false]]],
                    'order' => 'Commentaire.created DESC',
                    'limit' => 1
                ],
                'Theme' => [
                    'fields' => ['libelle'],
                ],
            ],
            'recursive' => -1,
        ]);

        $seance_id = $this->Deliberation->getSeanceDeliberanteId($delib_id);
        $type_seance_libelle = '';
        if (!empty($seance_id)) {
            $seance = $this->Deliberation->Seance->find('first', [
                'fields' => ['Seance.id', 'date'],
                'conditions' => ['Seance.id' => $seance_id],
                'contain' => [
                    'Typeseance' => [
                        'fields' => ['libelle']
                    ],
                ],
                'recursive' => -1,
            ]);
            $type_seance_libelle = __('[%s]', $seance['Typeseance']['libelle']);
            $delib['Deliberation']['SeanceDeliberante']['texte'] = __(
                '%s du %s à %s',
                $seance['Typeseance']['libelle'],
                CakeTime::i18nFormat(strtotime($seance['Seance']['date']), '%A %e %B %Y'),
                CakeTime::i18nFormat(strtotime($seance['Seance']['date']), '%k h %M')
            );
        }

        switch ($type) {
            case 'insertion':
                $template = 'projet_insertion';
                $subject = __(
                    '[Projet#%s]%s Vous allez recevoir un projet: %s',
                    $delib_id,
                    $type_seance_libelle,
                    $delib['Deliberation']['objet']
                );
                $typeMessageHistorique = __('d\'insertion');
                break;
            case 'traitement':
                $template = 'projet_traitement';
                $subject = __(
                    '[Projet#%s]%s Vous avez un projet à traiter: %s',
                    $delib_id,
                    $type_seance_libelle,
                    $delib['Deliberation']['objet']
                );
                $typeMessageHistorique = __('traitement');
                break;
            case 'refus':
                $template = 'projet_refus';
                $subject = __(
                    '[Projet#%s]%s Un projet que j\'ai créé ou validé a été refusé : %s',
                    $delib_id,
                    $type_seance_libelle,
                    $delib['Deliberation']['objet']
                );
                $delib_anterieur = $this->Deliberation->find('first', [
                    'fields' => ['Deliberation.id'],
                    'conditions' => ['Deliberation.id' => $delib['Deliberation']['anterieure_id']],
                    'contain' => [
                        'Commentaire' => [
                            'fields' => ['texte'],
                            'conditions' => ['OR' => [
                                ['commentaire_auto' => null], ['commentaire_auto' => false]]],
                            'order' => 'Commentaire.created DESC',
                            'limit' => 1
                        ]
                    ],
                    'recursive' => -1,
                ]);

                $delib['Commentaire'][0]['texte'] = !empty($delib_anterieur['Commentaire'][0]['texte'])
                    ? $delib_anterieur['Commentaire'][0]['texte'] : null;
                $typeMessageHistorique = __('de refus');
                break;
            case 'modif_projet_cree':
                $template = 'projet_modif_cree';
                $subject = __(
                    '[Projet#%s]%s Un projet que j\'ai créé a été modifié: %s',
                    $delib_id,
                    $type_seance_libelle,
                    $delib['Deliberation']['objet']
                );
                $typeMessageHistorique = __('de création');
                break;
            case 'modif_projet_valide':
                $template = 'projet_modif_valide';
                $subject = __(
                    '[Projet#%s]%s Un projet que j\'ai visé a été modifié: %s',
                    $delib_id,
                    $type_seance_libelle,
                    $delib['Deliberation']['objet']
                );
                $typeMessageHistorique = __('de modification');
                break;
            case 'retard_validation':
                $template = 'retard_validation';
                $subject = __(
                    '[Projet#%s]%s Un projet que je dois traiter est en retard : %s',
                    $delib_id,
                    $type_seance_libelle,
                    $delib['Deliberation']['objet']
                );
                $typeMessageHistorique = __('retard');
                break;
            case 'projet_valide':
                $template = 'projet_valide';
                $subject = __(
                    '[Projet#%s]%s Un projet que j\'ai rédigé a été validé : %s',
                    $delib_id,
                    $type_seance_libelle,
                    $delib['Deliberation']['objet']
                );
                $typeMessageHistorique = __('de validation (rédacteur)');
                break;
            case 'projet_valide_valideur':
                $template = 'projet_valide_valideur';
                $subject = __(
                    '[Projet#%s]%s Un projet que j\'ai traité a été validé : %s',
                    $delib_id,
                    $type_seance_libelle,
                    $delib['Deliberation']['objet']
                );
                $typeMessageHistorique = __('de validation (valideur)');
                break;
        }
        $aVariables = [
            'nom' => $user['User']['nom'],
            'prenom' => $user['User']['prenom'],
            'projet_identifiant' => $delib_id,
            'projet_objet' => $delib['Deliberation']['objet'],
            'projet_theme' => $delib['Theme']['libelle'],
            'seance_deliberante' => !empty($delib['Deliberation']['SeanceDeliberante']['texte']) ?
                $delib['Deliberation']['SeanceDeliberante']['texte']
                : __('Pas de séance délibérante sélectionnée'),
            'projet_dernier_commentaire' => !empty($delib['Commentaire'][0]['texte'])
                ? $delib['Commentaire'][0]['texte'] : 'Aucun commentaire',
            'projet_titre' => $delib['Deliberation']['titre'],
            // 'LIBELLE_CIRCUIT' => $this->Circuit->getLibelle($delib['Deliberation']['circuit_id']),
            'projet_url_traiter' =>
                Configure::read('App.fullBaseUrl') . '/validations/traiter/' . $delib['Deliberation']['id'],
            'projet_url_visualiser' =>
                Configure::read('App.fullBaseUrl') . '/projets/view/' . $delib['Deliberation']['id'],
            'projet_url_modifier' =>
                Configure::read('App.fullBaseUrl') . '/projets/edit/' . $delib['Deliberation']['id'],
        ];

        try {
            $message = "Utilisateur id ".$user_id." mail envoyé: ".$type." (".$user['User']['email'].")\n";
            $this->log($message, 'error');
            $Email->viewVars($aVariables);
            $Email->template($template, 'default')
                            ->to($user['User']['email'])
                            ->subject($subject);

            $fileTemplateHtml = new File(APP . __('Config/mails/html/%s.ctp', $template));
            if ($fileTemplateHtml->exists()) {
                $Email->viewRender('Email')->template($fileTemplateHtml->pwd());
            }
            $fileTemplateTxt = new File(APP . __('Config/mails/text/%s.ctp', $template));
            if ($fileTemplateTxt->exists()) {
                $Email->viewRender('Text')->template($fileTemplateTxt->pwd());
            }
            $retour = $Email->send();
        } catch (Exception $e) {
            $retour = false;
            CakeLog::error(
                __(
                    'Notification %s du projet (id:%s) n\'a pas été envoyée à %s %s',
                    $typeMessageHistorique,
                    $delib_id,
                    $user['User']['nom'],
                    $user['User']['prenom']
                )
            );
        }
        return $retour;
    }

    /**
     * Fonction d'initialisation des variables de fusion pour l'allias utilisé pour la liaison (Redacteur)
     * les bibliothèques Gedooo doivent être inclues par avance
     * génère une exception en cas d'erreur
     *
     * @version 4.3
     * @access public
     * @param type $aData
     * @param object_by_ref $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param integer $id --> id du modèle lié
     * @param string $suffixe --> suffixe des variables de fusion
     * @throws Exception
     */
    public function setVariablesFusion(&$aData, &$modelOdtInfos, $id, $suffixe = '')
    {
        // initialisations
        if (empty($suffixe)) {
            $suffixe = trim(strtolower($this->alias));
        }
        $fields = [];
        $variables = [
            'prenom',
            'nom',
            'email',
            'telmobile',
            'telfixe',
            'note'
        ];

        // liste des variables présentes dans le modèle d'édition
        foreach ($variables as $variable) {
            if ($modelOdtInfos->hasUserFieldDeclared($variable . '_' . $suffixe)) {
                $fields[] = $variable;
            }
        }
        if (empty($fields)) {
            return false;
        }
        // lecture en base de données
        $user = $this->find('first', [
            'recursive' => -1,
            'fields' => $fields,
            'conditions' => ['id' => $id]]);

        if (empty($user)) {
            throw new Exception('user ' . $suffixe . ' id:' . $id . ' non trouvé en base de données');
        }
        foreach ($user[$this->alias] as $field => $val) {
            $aData[$field . '_' . $suffixe] = ['value' => $val, 'type' => 'text'];
        }
    }

    /**
     * @version 4.3
     * @access public
     * @return boolean | type
     */
    public function parentNode()
    {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        if (isset($this->data['User']['profil_id'])) {
            $groupId = $this->data['User']['profil_id'];
        } else {
            $groupId = $this->field('profil_id');
        }
        if (!$groupId) {
            return null;
        }
        return ['Profil' => ['id' => $groupId]];
    }

    /**
     * @version 4.3
     * @access public
     * @return type
     */
    public function parentNodeAlias()
    {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        $data = $this->data;
        if (empty($this->data)) {
            $data = $this->read();
        }
        if (!isset($data['User']['id'])) {
            return null;
        }
        return ['User' => ['alias' => $data['User']['id']]];
    }

    /**
     * @version 4.3
     * @access public
     * @param type $userId
     * @return type
     */
    public function getTypeActes($userId)
    {
        $this->Aro->Behaviors->attach('DatabaseTable');
        $this->Aro->Permission->Behaviors->attach('DatabaseTable');
        $this->Aro->Permission->Aco->Behaviors->attach('DatabaseTable');
        $this->Aro->Permission->Aco->bindModel(
            ['belongsTo' => [
                        'Typeacte' => [
                            'className' => 'Typeacte',
                            'foreignKey' => false,
                            'conditions' => [
                                'Aco.model' => 'Typeacte',
                                'Typeacte.id = Aco.foreign_key'
                            ],
                        ]
                    ]
                ]
        );

        $typeactes = $this->find('list', [
            'fields' => ['Typeacte.id', 'Typeacte.name'],
            'joins' => [
                $this->join('Aro', ['type' => 'INNER']),
                $this->Aro->join('Permission', ['type' => 'INNER']),
                $this->Aro->Permission->join('Aco', ['type' => 'INNER']),
                $this->Aro->Permission->Aco->join('Typeacte', ['type' => 'INNER']),
            ],
            'conditions' => [
                'User.id' => $userId,
                'Permission._read' => '1',
                'Aco.model' => 'Typeacte',
                'Aro.model' => 'User'
            ]
        ]);

        return $typeactes;
    }

    /**
     * @version 4.3
     * @access public
     * @param array $data
     * @return type
     */
    public function saveLdapManagerLdap($data)
    {
        $this->Profil->recursive = -1;
        if (!$this->Profil->exists($data['User']['profil_id'])) {
            return false;
        }
        $this->create();
        $data['User']['accept_notif'] = false;

        return parent::save($data, [
                    'validate' => true,
                    'fieldList' => ['username', 'nom', 'prenom', 'email', 'profil_id', 'telfixe', 'telmobile'],
        ]);
    }

    /**
     * [getUsersByAcl description]
     * @param  [type] $acl [description]
     * @return [type]      [description]
     */
    public function getUsersUsernameByAcl($acoAlias, $crud)
    {
        $db = $this->getDataSource();
        $Aro = ClassRegistry::init('Aro');
        $Aro->Behaviors->load('DynamicDbConfig');
        $Aro->Behaviors->attach('Database.DatabaseTable');
        $Aro->Permission->Behaviors->load('DynamicDbConfig');
        $Aro->Permission->Behaviors->attach('Database.DatabaseTable');
        $Aro->Permission->Aco->Behaviors->attach('Database.DatabaseTable');

        $permissions = [];
        if (is_array($crud)) {
            foreach ($crud as $key => $permission) {
                array_push($permissions, ['Permission._' . $permission => 1]);
            }
        } else {
            $permissions = ['Permission._' . $crud => 1];
        }

        $subQuery = [
            'fields' => [
                'Aro.foreign_key'
            ],
            'contain' => false,
            'joins' => [
                $Aro->join('Permission', ['type' => 'INNER']),
                $Aro->Permission->join('Aco', ['type' => 'INNER'])
            ],
            'conditions' => [
                'Aro.model'=> 'User',
                'Aco.alias' => $acoAlias
            ] + $permissions
        ];

        $subQuery = $Aro->sql($subQuery);
        $subQuery = ' "' . $this->alias . '"."id" IN (' . $subQuery . ') ';
        $conditions[] = $db->expression($subQuery);

        $this->virtualFields['name'] = 'User.nom || \' \' || User.prenom';
        $users = $this->find('list', [
          'fields' => ['id','name'],
          'conditions' => $conditions + ['active' => true],
          'order' => ['User.nom' => 'ASC'],
          'recursive' => -1
        ]);

        unset($this->virtualFields['name']);

        return $users;
    }

    /**
     * [setUsersRedacteurWithUsername description]
     * @param [type] $users     [description]
     * @param [type] $redacteurId [description]
     */
    public function setUsersRedacteurWithUsername(&$users, $redacteurId)
    {
        if (array_key_exists($redacteurId, $users)) {
            $users = [$redacteurId => __('%s <Rédacteur du projet>', $users[$redacteurId])] + $users;
        }
    }
}
