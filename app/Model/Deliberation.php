<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppTools', 'Lib');
App::import('Component', 'Auth');
App::import('Component', 'TokensMethod');
App::uses('CakeTime', 'Utility');
App::uses('Tdt', 'Lib');
App::uses('SabreDavService', 'Lib/Service');

/**
 * AppModel Deliberation
 * @package     app.Model.Deliberation
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Deliberation extends AppModel
{
    public $actsAsVersionOptionsList = [
        'Version' => [
            'implementedFinders' => ['versions' => 'findVersions'],
            'versionTable' => 'deliberations_versions',
            'versionField' => 'version_id',
            'additionalVersionFields' => ['created'],
            'fields' => [],
            'byteaFields' => [
                'texte_projet',
                'texte_synthese',
                'tdt_data_edition',
                'tdt_data_pdf',
                'tdt_ar',
                'tdt_data_bordereau_pdf',
                'parapheur_bordereau',
                'debat',
                'deliberation',
                'delib_pdf',
                'commission',
                'signature',
                'Typeacte' => ['gabarit_projet', 'gabarit_synthese', 'gabarit_acte'],
//            'Annexe' => [],
                'Multidelib' => [],
                'Seance' => ['debat_global', 'pv_complet', 'pv_sommaire'],
                'Historique' => [],
                'Infosup' => [],
            ],
            'contain' => [
                'User' => ['id', 'nom', 'prenom'],
                'Multidelib' => [
                    'fields' => [
                        'id', 'objet', 'objet_delib', 'num_delib', 'etat',
                        'deliberation', 'deliberation_name'
                    ],
                    'Annexe' => ['fields' => ['id', 'titre', 'joindre_ctrl_legalite', 'filename','size']]
                ],
                'Redacteur' => ['fields' => ['id', 'nom', 'prenom']],
                'Rapporteur' => ['fields' => ['id', 'nom', 'prenom']],
                'Infosup',
                'Annexe' => [
                    'fields' => [
                        'id',
                        'titre',
                        'joindre_ctrl_legalite',
                        'joindre_fusion',
                        'size',
                        'filename',
                        'modified',
                        'typologiepiece_code'
                    ],
                    'order' => 'Annexe.position ASC'
                ],
                'Service' => ['fields' => ['name']],
                'Theme' => ['fields' => ['libelle']],
                'Typeacte' => [
                    'fields' => ['name', 'modele_bordereau_projet_id'],
                    'Nature' => ['fields' => ['code']]
                ],
                'Circuit' => ['fields' => ['nom']],
                'DeliberationTypeseance' => ['fields' => ['id'],
                    'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]],
                'DeliberationSeance' => ['fields' => ['id'],
                    'Seance' => ['fields' => ['id', 'date', 'type_id'],
                        'Typeseance' => ['fields' => ['id', 'libelle', 'color', 'action']]
                    ]
                ]
            ],
            'foreignKey' => 'foreign_key',
            'referenceName' => 'DeliberationsVersion',
            'onlyDirty' => false
        ]];

    //dependent : pour les suppression en cascades. ici à false pour ne pas modifier le referentiel
    public $belongsTo = [
        'Service' => [
            'className' => 'Service',
            'dependent' => false,
            'foreignKey' => 'service_id'
        ],
        'Theme' => [
            'className' => 'Theme',
            'dependent' => false,
            'foreignKey' => 'theme_id'
        ],
        'Circuit' => [
            'className' => 'Cakeflow.Circuit',
            'dependent' => false,
            'foreignKey' => 'circuit_id'
        ],
        'Redacteur' => [
            'className' => 'User',
            'dependent' => true,
            'foreignKey' => 'redacteur_id'
        ],
        'Rapporteur' => [
            'className' => 'Acteur',
            'dependent' => true,
            'foreignKey' => 'rapporteur_id'
        ],
        'President' => [
            'className' => 'Acteur',
            'dependent' => false,
            'foreignKey' => 'president_id'],
        'Typeacte' => [
            'className' => 'Typeacte',
            'foreignKey' => 'typeacte_id',
            'dependent' => false
        ]
    ];

    public $hasMany = [
        'TdtMessage' => [
            'className' => 'TdtMessage',
            'foreignKey' => 'delib_id'
        ],
        'Historique' => [
            'className' => 'Historique',
            'foreignKey' => 'delib_id'
        ],
        'Traitement' => [
            'className' => 'Cakeflow.Traitement',
            'foreignKey' => 'target_id',
            'dependent' => true
        ],
        'Annexe' => [
            'className' => 'Annexe',
            'foreignKey' => 'foreign_key',
            'order' => ['Annexe.position' => 'ASC'],
            'dependent' => true
        ],
        'Commentaire' => [
            'className' => 'Commentaire',
            'foreignKey' => 'delib_id'
        ],
        'Listepresence' => [
            'className' => 'Listepresence',
            'foreignKey' => 'delib_id'
        ],
        'Vote' => [
            'className' => 'Vote',
            'foreignKey' => 'delib_id'
        ],
        'Infosup' => [
            'foreignKey' => 'foreign_key',
            'conditions' => ['Infosup.model' => 'Deliberation'],
            'dependent' => true
        ],
        'Multidelib' => [
            'className' => 'Deliberation',
            'foreignKey' => 'parent_id',
            'order' => 'id ASC',
            'dependent' => false
        ],
        'DeliberationTypeseance' => [
            'className' => 'DeliberationTypeseance',
            'foreignKey' => 'deliberation_id',
            'dependent' => true
        ],
        'DeliberationSeance' => [
            'className' => 'DeliberationSeance',
            'foreignKey' => 'deliberation_id',
            'dependent' => true
        ],
        'DeliberationUser' => [
            'className' => 'DeliberationUser',
            'foreignKey' => 'deliberation_id',
            'dependent' => true
        ],
    ];
    public $hasAndBelongsToMany = [
        'Seance' => [
            'className' => 'Seance',
            'joinTable' => 'deliberations_seances',
            'foreignKey' => 'deliberation_id',
            'associationForeignKey' => 'seance_id',
            'unique' => true,
            'order' => 'Seance.date ASC'
        ],
        'Typeseance',
        'User' => [
            'className' => 'User',
            'joinTable' => 'deliberations_users',
            'foreignKey' => 'deliberation_id',
            'associationForeignKey' => 'user_id',
            'unique' => 'keepExisting',
            'fields' => ['id', 'username', 'nom', 'prenom'],
            'order' => 'User.nom ASC',
        ]
    ];

    /**
     * Règles de validation
     *
     * @version 4.3
     * @access public
     * @var array
     */
    public $validate = [
        'objet' => [
            [
                'rule' => 'notBlank',
                'message' => 'Le libellé est obligatoire'
            ],
            [
                'rule' => ['maxLength', 499],
                'message' => 'L\'objet du projet ne doit pas dépasser 499 caractères.'
            ]
        ],
        'titre' => [
            [
                'rule' => ['maxLength', 1000],
                'message' => 'Le titre du projet ne doit pas dépasser 1000 caractères.'
            ]
        ],
        'typeacte_id' => [
            'rule_typeacte_id-notBlank' => [
                'rule' => 'notBlank',
                'message' => 'Le type d\'acte est obligatoire'
            ],
            'rule_typeacte_id-canSaveTypeacte' => [
                'rule' => ['canSaveTypeacte'],
                'message' => 'Le type d\'acte est invalide'
            ]
        ],
        'theme_id' => [
            [
                'rule' => ['notBlank'],
                'message' => "Le thème est obligatoire"
            ]
        ],
        'texte_projet_upload' => [
            'texteProjetUploadRule-1' => [
                'rule' => 'uploadFileName',
                'message' => 'Seulement les lettres, les entiers et les caractères spéciaux -_.& '
                    .'sont autorisés dans le nom du fichier. Minimum de 5 caractères',
            ],
            'texteProjetUploadRule-2' => [
                'rule' => ['checkFormat', 'application/vnd.oasis.opendocument.text', false],
                'message' => "Format du document invalide. Autorisé : fichier ODT"
            ]
        ],
        'texte_synthese_upload' => [
            'texteSyntheseUploadRule-1' => [
                'rule' => 'uploadFileName',
                'message' => 'Seulement les lettres, les entiers et les caractères spéciaux -_.& '
                    .'sont autorisés dans le nom du fichier. Minimum de 5 caractères',
            ],
            'texteSyntheseUploadRule-2' => [
                'rule' => ['checkFormat', 'application/vnd.oasis.opendocument.text', false],
                'message' => "Format du document invalide. Autorisé : fichier ODT"
            ]
        ],
        'deliberation_upload' => [
            'deliberationUploadRule-1' => [
                'rule' => 'uploadFileName',
                'message' => 'Seulement les lettres, les entiers et les caractères spéciaux -_.& '
                    .'sont autorisés dans le nom du fichier. Minimum de 5 caractères',
            ],
            'deliberationUploadRule-2' => [
                'rule' => ['checkFormat', 'application/vnd.oasis.opendocument.text', false],
                'message' => "Format du document invalide. Autorisé : fichier ODT"
            ]
        ],
        'texte_doc' => [
            ['rule' => ['checkFormat', 'application/vnd.oasis.opendocument.text', false],
                'message' => "Format du document invalide. Autorisé : fichier ODT"
            ]
        ],
        'vote_commentaire' => [
            'rule' => ['maxLength', 1000],
            'message' => 'Le commentaire de vote ne doit pas dépasser 1000 caractères.'
        ]
    ];

    /**
     * État du projet : en cours de rédaction.
     */
    public const ETAT_PROJET_ABANDON = -3;
    public const ETAT_ACTE_ABANDON = self::ETAT_PROJET_ABANDON;
    public const ETAT_PROJET_EN_COURS_DE_REDACTION_REFUS = -2;
    public const ETAT_PROJET_VALIDATION_REFUS = -1;
    public const ETAT_PROJET_EN_COURS_DE_REDACTION = 0;
    public const ETAT_PROJET_VALIDATION_EN_COURS = 1;
    public const ETAT_PROJET_VALIDATION_VALID = 2;
    public const ETAT_PROJET_VALIDATION_RETARD = 9;
    public const ETAT_ACTE_VOTE_ADOPTE = 3;
    public const ETAT_ACTE_VOTE_REJET = 4;
    public const ETAT_ACTE_NUMEROTE = 6;
    public const ETAT_ACTE_SIGNATURE_SIGNEE = 7;
    /** Acte à Renvoyer en Signature
     */
    public const ETAT_ACTE_SIGNATURE_RENVOYER = 10;
    public const ETAT_ACTE_TDT_TRANSMIS = 5;
    /** Acte Transmis manuellement
     */
    public const ETAT_ACTE_TDT_TRANSMIS_MANUELLEMENT = 11;
    public const ETAT_ACTE_TDT_RENVOYER = 8;

    /**
     * Si vous avez besoin d’exécuter de la logique juste après chaque opération de sauvegarde,
     * placez-la dans cette méthode de rappel. Les données sauvegardées seront disponibles dans $this->data
     *
     * @param bool $created -->  La valeur de $created sera true si un nouvel
     * objet a été créé (plutôt qu’un objet mis à jour)
     * @param array $options -->  Le tableau $options est le même que celui passé dans Model::save()
     * @version 4.3
     * @access public
     */
    public function afterSave($created, $options = [])
    {

        // History create projet
        if (isset($this->data['Deliberation']['id'])) {
            $Historique = $this->Historique->find('first', [
                'conditions' => [
                    'Historique.delib_id' => $this->data['Deliberation']['id'],
                ],
                'recursive' => -1,
                'fields' => ['Historique.delib_id']
            ]);
            if (empty($Historique)) {
                $user = $this->find('first', [
                    'conditions' => [
                        'Deliberation.id' => $this->data['Deliberation']['id'],
                    ],
                    'contain' => 'Redacteur',
                    'recursive' => -1,
                    'fields' => ['Redacteur.nom', 'Redacteur.prenom']
                ]);
                $create = [
                    'delib_id' => $this->data['Deliberation']['id'],
                    'user_id' => $this->data['Deliberation']['redacteur_id'],
                    'circuit_id' => -1,
                    'commentaire' => '[' . $user['Redacteur']['prenom'] . ' ' . $user['Redacteur']['nom'] . '] '
                        . __('Le projet a été créé'),
                    'modified' => date("Y-m-d H:i:s"),
                    'revision_id' => isset($this->data['DeliberationVersion']['id'])
                        ? $this->data['DeliberationVersion']['id'] : null,
                    'created' => date("Y-m-d H:i:s")
                ];
                $this->Historique->create($create);
                $this->Historique->save();
            }
        }

        if (!$created && !empty($this->data['Deliberation']['etat'])) {
            $hasChildren = $this->hasAny(['Deliberation.parent_id' => $this->id]);
            //Si la delib a des enfants
            if ($hasChildren) {
                //Recopie des attributs etat (si < ou = à 2) et circuit_id
                if (array_key_exists('etat', $this->data['Deliberation'])
                    && $this->data['Deliberation']['etat'] <= 2
                ) {
                    $this->updateAll(
                        ['Deliberation.etat' => $this->data['Deliberation']['etat']],
                        ['Deliberation.parent_id' => $this->id]
                    );
                }

                if (array_key_exists('circuit_id', $this->data['Deliberation'])) {
                    $this->updateAll(
                        ['Deliberation.circuit_id' => $this->data['Deliberation']['circuit_id']],
                        ['Deliberation.parent_id' => $this->id]
                    );
                }
            }
        }
    }

    /*
     * Indique si le projet de délibération $delibId est modifiable pour $userId.
     * Attention : ne tient pas compte des droits qui sont fait dans le controller
     * En fonction de l'état du projet on a :
     * - le projet est refusé (etat = -1) : non modifiable
     * - le projet est en cours de rédaction (etat = 0) :
     *   + l'utilisateur connecté est le rédacteur du projet : modifiable
     *   + l'utilisateur connecté n'est pas le rédacteur du projet : non modifiable
     *  - le projet est en cours de validation (etat = 1) :
     *    + l'utilisateur connecté n'est pas dans le circuit de validation : non modifiable
     *    + l'utilisateur connecté est dans le circuit de validation :
     *      * il a déja validé le projet : non modifiable
     *      * c'est à son tour de traiter le projet : modifiable
     *      * son tour n'est pas encore passé : modifiable
     *  - le projet est validé (etat = 2) : non modifiable
     *  - le projet a été voté (etat = 3 ou 4) : non modifiable
     *  - le projet a été envoyé (etat = 5) : non modifiable
     *  - le projet a recu un avis (avis = 1 ou 2) : non modifiable
     *  - le projet est à renvoyer en signature : (etat = 10)
     *
     * @version 5.1
     * @since 4.3
     * @access public
     * @param type $delibId
     * @param type $userId
     * @param type $canEdit
     * @return boolean
     */

    public function estModifiable($delibId, $userId, $canEdit = false)
    {
        /* lecture en base */
        $delib = $this->find('first', [
            'fields' => ['etat', 'redacteur_id', 'signee'],
            'conditions' => [
                'Deliberation.id' => $delibId,
                'Deliberation.signee' => false
            ],
            'contain' => ['User.id'],
            'recursive' => -1,
        ]);

        if (!empty($delib)) {
            switch ($delib['Deliberation']['etat']) { /* traitement en fonction de l'état */
                case 0:
                    return ($canEdit
                        || in_array(
                            $userId,
                            array_merge(
                                [$delib['Deliberation']['redacteur_id']],
                                Hash::extract($delib['User'], '{n}.id')
                            ),
                            true
                        )
                    );
                case 1:
                    return ($canEdit || $this->Traitement->positionTrigger($userId, $delibId) === 0);
                case 2:
                    return $canEdit;
                case 3:
                    return $canEdit;
                case 4:
                    return $canEdit;
                case 10:
                    return $canEdit;
                default:
                    return false;
            }
        }
        return false;
    }

    /**
     * @param type $id
     * @return type
     * @version 4.3
     * @access public
     */
    public function getCurrentPosition($id)
    {
        $delib = $this->find('first', ['conditions' => ["Deliberation.id" => $id],
            'fields' => ['Deliberation.position'],
            'recursive' => -1]);
        return $delib['Deliberation']['position'];
    }

    /**
     * @param type $id
     * @param type $retourDetaillee
     * @return type
     * @version 4.3
     * @access public
     */
    public function getCurrentSeances($id, $retourDetaillee)
    {
        $delib = $this->find('first', [
            'fields' => 'Deliberation.id',
            'contain' => ($retourDetaillee ? 'Seance' : 'Seance.id'),
            'conditions' => ["Deliberation.id" => $id],
            'recursive' => -1,
        ]);
        if ($retourDetaillee) {
            return $delib['Seance'];
        } else {
            $seances = [];
            foreach ($delib['Seance'] as $seance) {
                $seances[] = $seance['id'];
            }
            return $seances;
        }
    }

    /**
     * @param type $delib_id
     * @param type $dateAR
     * @version 4.3
     * @access public
     */
    public function changeDateAR($delib_id, $dateAR)
    {
        $this->id = $delib_id;
        $this->saveField('tdt_ar_date', $dateAR);
    }

    /**
     * Retourne l'identifiant du modèle à utiliser selon le projet
     *
     * @param string $delib_id --> identifiant du projet
     * @return integer identifiant du modèle
     * @version 4.3
     * @access public
     */
    public function getModelId($delib_id)
    {
        // initialisations
        $etat = $this->field('etat', ['id' => $delib_id]);
        $seanceDeliberanteId = $this->getSeanceDeliberanteId($delib_id);
        if (!empty($seanceDeliberanteId)) {
            // lecture des modèles de la séance délibérante
            $seance = $this->Seance->find('first', [
                'fields' => ['type_id'],
                'contain' => ['Typeseance.modele_projet_id', 'Typeseance.modele_deliberation_id'],
                'conditions' => ['Seance.id' => $seanceDeliberanteId],
                'recursive' => -1
            ]);
            if ($etat < 3) {
                return $seance['Typeseance']['modele_projet_id'];
            } else {
                return $seance['Typeseance']['modele_deliberation_id'];
            }
        }

        //Avec le type de séance
        $typeseance = $this->find('first', [
            'fields' => ['Typeseance.modele_projet_id', 'Typeseance.modele_deliberation_id'],
            'joins' => [
                $this->join('DeliberationTypeseance', ['type' => 'INNER']),
                $this->DeliberationTypeseance->join('Typeseance', ['type' => 'INNER']),
            ],
            'conditions' => ['Deliberation.id' => $delib_id],
            'recursive' => -1,
            'order' => 'Typeseance.action ASC'
        ]);

        if (!empty($typeseance)) {
            if ($etat < 3) {
                return $typeseance['Typeseance']['modele_projet_id'];
            } else {
                return $typeseance['Typeseance']['modele_deliberation_id'];
            }
        }

        if (empty($typeseance)) {
            $typeacte_id = $this->field('typeacte_id', ['id' => $delib_id]);
            $typeacte = $this->Typeacte->find('first', [
                'recursive' => -1,
                'conditions' => ['id' => $typeacte_id],
                'fields' => ['modele_projet_id', 'modele_final_id']]);
            if ($etat < 3) {
                return $typeacte['Typeacte']['modele_projet_id'];
            } else {
                return $typeacte['Typeacte']['modele_final_id'];
            }
        }
    }

    /**
     * @param type $delib_id
     * @param type $seance_id
     * @return type
     * @version 4.3
     * @access public
     */
    public function getModelForSeance($delib_id, $seance_id)
    {
        $delib = $this->find('first', [
            'conditions' => ['Deliberation.id' => $delib_id],
            'recursive' => -1,
            'fields' => ['Deliberation.id', 'Deliberation.etat', 'Deliberation.typeacte_id']
        ]);
        $seance = $this->Seance->find('first', [
            'conditions' => ['Seance.id' => $seance_id],
            'fields' => ['Seance.id'],
            'contain' => ['Typeseance.modele_projet_id', 'Typeseance.modele_deliberation_id']]);

        if (!empty($seance)) {
            if ($delib['Deliberation']['etat'] < 3) {
                return $seance['Typeseance']['modele_projet_id'];
            } else {
                return $seance['Typeseance']['modele_deliberation_id'];
            }
        } else { // Pas de séance associée, chercher le model du type d'acte
            if ($delib['Deliberation']['etat'] < 3) {
                return $this->Typeacte->getModelId($delib['Deliberation']['typeacte_id'], 'modele_projet_id');
            } else {
                return $this->Typeacte->getModelId($delib['Deliberation']['typeacte_id'], 'modele_final_id');
            }
        }
    }

    /**
     * Refuse un dossier et cré un nouveau dossier avec l'ancien rataché au nouveau
     *
     * @access public
     * @param type $id --> l'id du dossier à supprimer
     * @return type --> nouvelle id
     * @version 5.0
     * @since 4.3
     */
    public function refusDossier($id)
    {
        // lecture en base de données
        $delib = $this->find('first', [
            'contain' => [
                'Annexe',
                'Seance',
                'User',
                'Typeseance'],
            'conditions' => ['Deliberation.id' => $id],
            'recursive' => -1,
        ]);

        // maj de l'etat de la delib dans la table deliberations
        $this->id = $id;
        $this->saveField('etat', '-1');

        // création de la nouvelle version
        unset($delib['Deliberation']['id']);
        unset($delib['Deliberation']['circuit_id']);
        unset($delib['Deliberation']['created']);
        unset($delib['Deliberation']['modified']);
        $delib['Deliberation']['etat'] = 0;
        $delib['Deliberation']['anterieure_id'] = $id;

        //on récupère les champ des users liers et on suprime les id afin de créer
        // une nouvelle delib et non de mettre a jour l'ancienne
        foreach ($delib['User'] as $user_id => $user) {
            unset($delib['User'][$user_id]['UsersDeliberation']['deliberation_id']);
            unset($delib['User'][$user_id]['UsersDeliberation']['id']);
        }

        $this->create();
        $this->saveAll($delib);
        $delib_id = $this->id;
        //$delib_id = $this->getLastInsertID();
        $this->copyPositionsDelibs($id, $delib_id);

        // copie des annexes du projet refusé vers le nouveau projet
        $this->Annexe->recursive = -1;
        $annexes = $this->Annexe->findAllByForeignKey($id);
        foreach ($annexes as $annexe) {
            $this->Annexe->create();
            unset($annexe['Annexe']['id']);
            $annexe['Annexe']['created'] = date('Y-m-d H:i:s', time());
            $annexe['Annexe']['modified'] = date('Y-m-d H:i:s', time());
            $annexe['Annexe']['foreign_key'] = $delib_id;
            $this->Annexe->save($annexe, false);
            $this->Annexe->clear();
        }

        // copie des infos supplémentaires du projet refusé vers le nouveau projet
        $this->Infosup->recursive = -1;
        $infoSups = $this->Infosup->findAllByForeignKey($id);
        foreach ($infoSups as $infoSup) {
            $this->Infosup->create();
            unset($infoSup['Infosup']['id']);
            $infoSup['Infosup']['created'] = date('Y-m-d H:i:s', time());
            $infoSup['Infosup']['modified'] = date('Y-m-d H:i:s', time());
            $infoSup['Infosup']['foreign_key'] = $delib_id;
            $this->Infosup->save($infoSup, false);
            $this->Infosup->clear();
        }

        // copie des délibérations rattachées vers le nouveau projet
        $delibRattachees = $this->find('all', [
            'contain' => ['Annexe'],
            'conditions' => ['Deliberation.parent_id' => $id]]);

        foreach ($delibRattachees as $delibRattachee) {
            // création de la nouvelle version
            $this->create();
            $anterieure_id = $delibRattachee['Deliberation']['id'];
            unset($delibRattachee['Deliberation']['id']);
            $delibRattachee['Deliberation']['parent_id'] = $delib_id;
            $delibRattachee['Deliberation']['etat'] = 0;
            $delibRattachee['Deliberation']['anterieure_id'] = $anterieure_id;
            unset($delibRattachee['Deliberation']['date_envoi']);
            $delibRattachee['Deliberation']['created'] = date('Y-m-d H:i:s', time());
            $delibRattachee['Deliberation']['modified'] = date('Y-m-d H:i:s', time());
            $this->save($delibRattachee);
            $delibRattachee_id = $this->getLastInsertID();

            // copie des annexes du projet refusé vers le nouveau projet
            $this->Annexe->recursive = -1;
            $annexes = $this->Annexe->findAllByForeignKey($id);
            foreach ($annexes as $annexe) {
                $this->Annexe->create();
                unset($annexe['Annexe']['id']);
                $annexe['Annexe']['created'] = date('Y-m-d H:i:s', time());
                $annexe['Annexe']['modified'] = date('Y-m-d H:i:s', time());
                $annexe['Annexe']['foreign_key'] = $delibRattachee_id;
                $this->Annexe->save($annexe, false);
                $this->Annexe->clear();
            }
        }
        return $delib_id;
    }

    /**
     * @return boolean
     * @version 4.3
     * @access public
     */
    public function canSaveTypeacte()
    {
        $canSaveTypeacte = true;
        if (empty($this->data['Deliberation']['typeacte_id'])) {
            $canSaveTypeacte = false;
        }
        if ($canSaveTypeacte
            && !empty($this->data['Deliberation']['id'])
            && !empty($this->data['Deliberation']['typeacte_id'])
        ) {
            if (!$this->Typeacte->getIsDeliberantById($this->data['Deliberation']['typeacte_id'])) {
                $nbr_seance = $this->find('count', [
                    'joins' => [$this->join('DeliberationSeance', ['type' => 'INNER'])],
                    'conditions' => [
                        'deliberation_id' => $this->data['Deliberation']['id']
                    ],
                    'recursive' => -1
                ]);

                $canSaveTypeacte = $nbr_seance === 0;
            }
        }

        if ($canSaveTypeacte
            && isset($this->data['Deliberation']['seance_id'])
            && (!empty($this->data['Deliberation']['seance_id']))
        ) {
            foreach ($this->data['Deliberation']['seance_id'] as $key => $seance_id) {
                $result = $this->Seance->naturecanSave($seance_id, $this->data['Deliberation']['typeacte_id']);
                if ($result === false) {
                    $canSaveTypeacte = false;
                }
            }
        }

        return $canSaveTypeacte;
    }

    /**
     * Opérations post sauvegarde des délibérations :
     * - gestion des séances et de l'ordre des projets
     * - mise à jour des délibérations rattachées
     *
     * @param integer $delibId --> id du projet à traiter
     * @return type
     * @version 4.3
     * @access public
     */
    public function majDelibRatt($delibId)
    {
        // initialisation
        $position = 0;
        $majPosition = false;
        $majFields = [
            'typeacte_id', 'theme_id', 'service_id', 'redacteur_id', 'rapporteur_id',
            'titre', 'num_pref', 'etat',
            'texte_projet', 'texte_projet_size', 'texte_projet_type', 'texte_projet_name',
            'texte_synthese', 'texte_synthese_size', 'texte_synthese_type', 'texte_synthese_name',
            'date_limite'];

        // lecture en base
        $delib = $this->find('first', [
            'fields' => $majFields,
            'contain' => ['Multidelib.id'],
            'conditions' => ['Deliberation.id' => $delibId]]);


        if (empty($delib['Multidelib'])) {
            return;
        }

        // faut-il mettre a jour la position dans la séance
        //  if (isset($this->data['Deliberation']['seance_id']) && !empty($delib['Deliberation']['seance_id']))
        //      $this->Seance->reOrdonne($id, $this->data['Deliberation']['seance_id']);
        // mise à jour des délibérations rattachées
        $majDelibRatt = [];
        foreach ($majFields as $fieldName) {
            $majDelibRatt['Deliberation'][$fieldName] = $delib['Deliberation'][$fieldName];
        }
        foreach ($delib['Multidelib'] as $delibRattachee) {
            $majDelibRatt['Deliberation']['id'] = $delibRattachee['id'];
            if ($majPosition || (!empty($delib['Deliberation']['seance_id']) && empty($delibRattachee['position']))) {
                if ($position > 0) {
                    $position++;
                }
                $majDelibRatt['Deliberation']['position'] = $position;
            } else {
                unset($majDelibRatt['Deliberation']['position']);
            }
            $this->save($majDelibRatt['Deliberation'], ['validate' => false, 'callbacks' => false]);
        }
    }

    /**
     * Reordonne les positions de la séance $seanceId
     *
     * @param type $seanceId
     * @version 4.3
     * @access public
     */
    public function reOrdonnePositionSeance($seanceId)
    {
        // initialisations
        $position = 0;
        // lecture des delibs de la séance
        $delibs = $this->find('all', [
            'recursive' => -1,
            'fields' => ['id', 'position'],
            'conditions' => [
                'etat <>' => -1,
                'seance_id' => $seanceId],
            'order' => 'position ASC']);
        // pour toutes les délibs
        foreach ($delibs as $delib) {
            $position++;
            if ($position !== $delib['Deliberation']['position']) {
                $this->save(
                    [
                        'id' => $delib['Deliberation']['id'],
                        'position' => $position],
                    [
                        'validate' => false, 'callbacks' => false
                    ]
                );
            }
        }
    }

    /**
     * Sauvegarde des délibérations attachées
     *
     * @param integer $parentId --> id de la délibération principale
     * @param array $delib --> délibération rattachée retourné par le formulaire 'edit'
     * @return boolean
     * @since 4.0.0
     * @access public
     * @version 5.1.0
     */
    public function saveDelibRattachees($parentId, $delib)
    {
        $this->recursive = -1;
        $projet_parent = $this->findById($parentId);

        //Vérification des types de séances
        if (isset($delib['objet_delib']) && empty($delib['objet_delib'])) {
            $this->validationErrors[$this->alias] = ['objet_delib' => __('Libellé manquants')];
            return false;
        }

        // initialisations
        $newDelib = [];

        if (!empty($delib['id'])) {
            // modification
            $this->id = $delib['id'];
            $newDelib['Deliberation']['id'] = $delib['id'];
        } else {
            $newDelib = $this->create();
            $newDelib['Deliberation']['parent_id'] = $parentId;
        }
        $newDelib['Deliberation']['is_multidelib_management'] = true;

        //Récupération des informations du rédacteur pour la multidélibération
        $newDelib['Deliberation']['redacteur_id'] = $projet_parent['Deliberation']['redacteur_id'];
        $newDelib['Deliberation']['service_id'] = $projet_parent['Deliberation']['service_id'];
        $newDelib['Deliberation']['typeacte_id'] = $projet_parent['Deliberation']['typeacte_id'];
        $newDelib['Deliberation']['theme_id'] = $projet_parent['Deliberation']['theme_id'];
        $newDelib['Deliberation']['num_pref'] = $projet_parent['Deliberation']['num_pref'];
        $newDelib['Deliberation']['typologiepiece_code'] = $projet_parent['Deliberation']['typologiepiece_code'];
        $newDelib['Deliberation']['tdt_document_papier'] = $projet_parent['Deliberation']['tdt_document_papier'];
        $newDelib['Deliberation']['etat'] = $projet_parent['Deliberation']['etat'];
        $newDelib['Deliberation']['is_multidelib'] = false;

        if (array_key_exists('objet_delib', $delib)) {
            $newDelib['Deliberation']['objet'] = $delib['objet_delib'];
            $newDelib['Deliberation']['objet_delib'] = $delib['objet_delib'];
        }
        $newDelib['Deliberation']['titre'] = !empty($delib['titre']) ? $delib['titre'] : null;
        if (isset($delib['deliberation']) && !empty($delib['deliberation']['name'])) {
            $newDelib['Deliberation']['deliberation_name'] = $delib['deliberation']['name'];
            $newDelib['Deliberation']['deliberation_type'] = $delib['deliberation']['type'];
            $newDelib['Deliberation']['deliberation_size'] = $delib['deliberation']['size'];
            if (empty($delib['deliberation']['tmp_name'])) {
                $newDelib['Deliberation']['deliberation'] = '';
            } else {
                $newDelib['Deliberation']['deliberation'] = file_get_contents($delib['deliberation']['tmp_name']);
            }
        } elseif (isset($delib['id'])) {
            if (!empty($delib['file_deliberation'])) {
                $sabreDavService = new SabreDavService();
                $file = $sabreDavService->getFileDav($delib['file_deliberation']);
                $newDelib['Deliberation']['deliberation'] = $file->read();
                $newDelib['Deliberation']['deliberation_size'] = $file->size();
            }
        } elseif (!empty($delib['gabarit'])) {
            $typeacte = $this->Typeacte->find('first', [
                'fields' => ['gabarit_acte', 'gabarit_acte_name'],
                'conditions' => ['id' => $projet_parent['Deliberation']['typeacte_id']],
                'recursive' => -1
            ]);
            $newDelib['Deliberation']['deliberation_name'] = $typeacte['Typeacte']['gabarit_acte_name'];
            $newDelib['Deliberation']['deliberation_type'] = 'application/vnd.oasis.opendocument.text';
            $newDelib['Deliberation']['deliberation_size'] = strlen($typeacte['Typeacte']['gabarit_acte']);
            $newDelib['Deliberation']['deliberation'] = $typeacte['Typeacte']['gabarit_acte'];
        }

        if (!$this->save($newDelib['Deliberation'], false)) {
            return false;
        }

        return $this->id;
    }

    /**
     * Sauvegarde des délibérations attachées
     *
     * @param integer $parentId --> id de la délibération principale
     * @param array $delib --> délibération rattachée retourné par le formulaire 'edit'
     * @return boolean
     * @since 4.0.0
     * @access public
     * @version 5.1.0
     */
    public function saveInfoDelibRattachees($parentId)
    {
        $this->recursive = -1;
        $projetParent = $this->findById($parentId, [
            'objet_delib',
            'redacteur_id',
            'service_id',
            'typeacte_id',
            'theme_id',
            'num_pref',
            'typologiepiece_code',
            'etat',
        ]);

        $this->recursive = -1;
        $projets = $this->findAllByParentId($parentId, ['id']);
        $updateProjet = [];

        foreach ($projets as $projet) {
            $updateProjet['is_multidelib_management'] = true;

            //Récupération des informations du rédacteur pour la multidélibération
            $updateProjet['id'] = $projet['Deliberation']['id'];
            $updateProjet['objet'] = $projetParent['Deliberation']['objet_delib'];
            $updateProjet['redacteur_id'] = $projetParent['Deliberation']['redacteur_id'];
            $updateProjet['service_id'] = $projetParent['Deliberation']['service_id'];
            $updateProjet['typeacte_id'] = $projetParent['Deliberation']['typeacte_id'];
            $updateProjet['theme_id'] = $projetParent['Deliberation']['theme_id'];
            $updateProjet['num_pref'] = $projetParent['Deliberation']['num_pref'];
            $updateProjet['typologiepiece_code'] = $projetParent['Deliberation']['typologiepiece_code'];
            $updateProjet['etat'] = $projetParent['Deliberation']['etat'];

            if (!$this->save($updateProjet, false)) {
                return false;
            }

            $this->DeliberationTypeseance->saveDeliberationTypeseanceDelibRattachees(
                $parentId,
                $projet['Deliberation']['id']
            );
            $this->DeliberationSeance->saveDeliberationSeanceDelibRattachees(
                $parentId,
                $projet['Deliberation']['id']
            );
            $this->Infosup->saveInfosupDelibRattachees($parentId, $projet['Deliberation']['id']);

            // Save new history
            $this->Historique->enregistre(
                $projet['Deliberation']['id'],
                AuthComponent::user('id'),
                __('Modification du projet')
            );
        }

        // order schedule
        $seances = $this->DeliberationSeance->find('all', [
            'fields' => ['seance_id'],
            'conditions' => ['DeliberationSeance.deliberation_id' => $parentId],
            'recursive' => -1]);
        foreach ($seances as $seance) {
            $this->DeliberationSeance->reOrdonne($seance['DeliberationSeance']['seance_id']);
        }

        return true;
    }

    /**
     * @param type $data
     * @return type
     * @version 4.3
     * @access public
     */
    public function saveDebat($data)
    {
        $this->validate = [];
        $this->validator()->add('texte_doc', [
            'texteDocRule-1' => [
                'rule' => ['uploadError', true],
                'message' => __('Une erreur est survenue lors de l\'upload du document')
            ],
            'texteDocRule-2' => [
                'rule' => 'uploadFileName',
                'message' => 'Seulement les lettres, les entiers et les caractères spéciaux -_.& sont '
                    . 'autorisés dans le nom du fichier. Minimum de 5 caractères',
                'growl'
            ],
            'checkFormat' => [
                'rule' => ['checkFormat', ['application/vnd.oasis.opendocument.text'], false],
                'message' => __('Format du document invalide. Autorisé : fichier ODT')
            ]
        ]);

        return parent::save($data);
    }

    /**
     * Fonction récursive de suppression de la délibération $delib8d, de ses versions
     * antérieures et de ses délibérations rattachées
     *
     * @param integer $id --> id de la délib à supprimer
     * @param integer $userId --> identifiant de l'utilisateur qui supprime le projet
     * @return type
     * @version 4.3
     * @access public
     */
    public function supprimer($id, $userId)
    {
        $this->Behaviors->load('Version', $this->actsAsVersionOptionsList['Version']);

        // lecture de la délib en base
        $delib = $this->find('first', [
            'recursive' => -1,
            'fields' => ['anterieure_id', 'parent_id'],
            'conditions' => ['id' => $id]]);

        if (empty($delib)) {
            return;
        }

        // gestion de la séance
        $aSeanceId = $this->getSeancesid($id);
        foreach ($aSeanceId as $seance_id) {
            $this->DeliberationSeance->deleteDeliberationSeance($id, $seance_id);
        }

        // delete multidelib
        if (empty($delib['Deliberation']['parent_id'])) {
            $delibRattachees = $this->find('all', [
                'recursive' => -1,
                'fields' => ['id'],
                'conditions' => ['parent_id' => $id]]);

            foreach ($delibRattachees as $delibRattachee) {
                // delete schedule
                $aSeanceId = $this->getSeancesid($delibRattachee['Deliberation']['id']);
                foreach ($aSeanceId as $seance_id) {
                    $this->DeliberationSeance->deleteDeliberationSeance(
                        $delibRattachee['Deliberation']['id'],
                        $seance_id
                    );
                }
                // delete previous project
                $this->deletePrevious($id);
                // delete multidelib
                $this->Historique->enregistre(
                    $delibRattachee['Deliberation']['id'],
                    $userId,
                    __('Le projet a été supprimé')
                );
                $this->supprimer($delibRattachee['Deliberation']['id'], $userId);
            }
        }

        // delete previous project
        $this->deletePrevious($id);
        // delete multidelib
        $this->Historique->enregistre($id, $userId, __('Le projet a été supprimé'));
        return $this->delete($id);
    }

    /**
     * Delete previous project
     *
     * @param  [type] $id [description]
     * @return [type]     [description]
     *
     * @version 5.1.0
     */
    private function deletePrevious($id)
    {
        $this->Behaviors->load('Version', $this->actsAsVersionOptionsList['Version']);

        $projet = $this->find('first', [
            'fields' => ['anterieure_id'],
            'conditions' => ['id' => $id],
            'recursive' => -1,
        ]);

        if (!empty($projet['Deliberation']['anterieure_id'])) {
            $this->Historique->enregistre(
                $projet['Deliberation']['anterieure_id'],
                AuthComponent::user('id'),
                __('Le projet a été supprimé')
            );
            $this->deletePrevious($projet['Deliberation']['anterieure_id']);
            $this->supprimer($projet['Deliberation']['anterieure_id'], AuthComponent::user('id'));
            // gestion de la séance
            $seance_ids = $this->getSeancesid($projet['Deliberation']['anterieure_id']);
            foreach ($seance_ids as $seance_id) {
                $this->DeliberationSeance->deleteDeliberationSeance(
                    $projet['Deliberation']['anterieure_id'],
                    $seance_id
                );
            }
        }
    }

    /**
     * Function erronée
     *
     * @param int $deliberation_id
     * @return array
     * @access public
     */
    public function getSeancesid($deliberation_id)
    {
        $seances = $this->DeliberationSeance->find(
            'all',
            [
                'fields' => ['DeliberationSeance.seance_id'],
                'conditions' => [
                    'DeliberationSeance.deliberation_id' => $deliberation_id
                ],
                'order' => ['DeliberationSeance.position' => 'ASC'],
                'recursive' => -1,
            ]
        );

        return (array) Hash::extract($seances, '{n}.DeliberationSeance.seance_id');
    }

    /**
     * @param int $delibId
     * @return int
     */
    public function getSeanceDeliberanteId(int $delibId)
    {
        return $this->Seance->getSeanceDeliberante($this->getSeancesid($delibId));
    }

    /**
     * @access public
     * @param type $deliberation_id
     * @return type
     */
    public function getNbSeances($deliberation_id)
    {
        return count($this->getSeancesid($deliberation_id));
    }

    /**
     * @access public
     * @param type $projets
     * @return string
     */
    public function getSeancesFromArray($projets)
    {
        $typeseances = $this->Seance->Typeseance->find('list', ['recursive' => -1, 'fields' => ['libelle']]);
        $seances = [];
        if (!empty($projets)) {
            foreach ($projets as $projet) {
                if (!empty($projet['Seance'])) {
                    foreach ($projet['Seance'] as $seance) {
                        $typeseance = $typeseances[$seance['type_id']];
                        $seances[$seance['id']] = $typeseance . ' : ' . $seance['date'];
                    }
                }
            }
        }
        return $seances;
    }

    /**
     * @access public
     * @param type $projets
     * @return type
     */
    public function getTypeseancesFromArray($projets)
    {
        $list = $this->Seance->Typeseance->find('list', ['recursive' => -1, 'fields' => ['libelle']]);

        $typeseances = [];
        if (isset($projets) && !empty($projets)) {
            foreach ($projets as $projet) {
                if (isset($projet['Seance']) && !empty($projet['Seance'])) {
                    foreach ($projet['Seance'] as $seance) {
                        if (isset($seance['type_id'])) {
                            $typeseance = $list[$seance['type_id']];
                            $typeseances[$seance['type_id']] = $typeseance;
                        }
                    }
                }
            }
        }
        return $typeseances;
    }

    /**
     * @access public
     * @param int $deliberation_id
     * @param int $seance_id
     * @return int
     */
    public function getPosition($deliberation_id, $seance_id)
    {
        $DeliberationSeance = $this->DeliberationSeance->find(
            'first',
            [
                'fields' => ['DeliberationSeance.position'],
                'recursive' => -1,
                'conditions' => [
                    'DeliberationSeance.seance_id' => $seance_id,
                    'DeliberationSeance.deliberation_id' => $deliberation_id
                ],
                'order' => ['DeliberationSeance.position ASC'],
            ]
        );

        return $DeliberationSeance['DeliberationSeance']['position'];
    }

    /**
     *
     * @param type $delib_id
     */
//    function getMultidelibs($delib_id)
//    {
//        $multidelib = $this->Multidelib->find('all', array(
//            'fields' => array('Deliberation.id'),
//            'contain' => array('DeliberationSeance.position'),
//            'conditions' => array('Deliberation.parent_id' => $delib_id),
//            'order' => array('DeliberationSeance.position'),
//            'recursive' => -1));
//    }

    /**
     * @param type $delib_id
     * @return type
     * @version 4.3
     * @access public
     */
    public function getMultidelibs($delib_id)
    {
        $deliberations = $this->find(
            'all',
            [
                'fields' => ['Deliberation.id'],
                'recursive' => -1,
                'conditions' => [
                    'Deliberation.parent_id' => $delib_id
                ],
                'order' => 'Deliberation.id ASC',
            ]
        );

        $deliberations = (array)Hash::extract($deliberations, '{n}.Deliberation.id');
        return $deliberations;
    }

    /**
     *
     * @access public
     * @param type $delib_id
     * @return type
     *
     * @version 5.1.0
     */
    public function getMultidelibParent($id)
    {
        $this->recursive = -1;
        $parent_id = $this->findById($id, ['parent_id']);

        return !empty($parent_id) ? $parent_id['Deliberation']['parent_id'] : false;
    }

    /**
     *
     * @param type $delib_id
     * @return type
     */
//    function getMultidelibs($delib_id)
//    {
//        $deliberations = $this->DeliberationSeance->find(
//                'all', array(
//            'fields' => array('Deliberation.parent_id'),
//            'contain' => array('DeliberationSeance', 'Deliberation'),
//            'recursive' => -1,
//            'conditions' => array(
//                'Deliberation.parent_id' => $delib_id
//            ),
//            'order' => 'DeliberationSeance.position ASC',
//                )
//        );
//
//
//        $deliberations = (array) Hash::extract($deliberations, '{n}.DeliberationSeance.deliberation_id');
//        return $deliberations;
//    }

    /**
     * @access public
     * @param type $delib_id
     * @param type $new_id
     */
    public function copyPositionsDelibs($delib_id, $new_id)
    {
        $DeliberationSeance = [];
        App::import('Model', 'DeliberationSeance');
        $this->DeliberationSeance = ClassRegistry::init('DeliberationSeance');
        $positions = $this->DeliberationSeance->find('all', [
            'fields' => ['DeliberationSeance.position', 'DeliberationSeance.id', 'DeliberationSeance.seance_id'],
            'conditions' => ['DeliberationSeance.deliberation_id' => $delib_id],
            'recursive' => -1]);
        foreach ($positions as $position) {
            $this->DeliberationSeance->id = $position['DeliberationSeance']['id'];
            $DeliberationSeance['DeliberationSeance']['position'] = $position['DeliberationSeance']['position'];
            $DeliberationSeance['DeliberationSeance']['seance_id'] = $position['DeliberationSeance']['seance_id'];
            $DeliberationSeance['DeliberationSeance']['deliberation_id'] = $new_id;
            $this->DeliberationSeance->save($DeliberationSeance);
        }
    }

    /**
     * @access public
     * @param type $id
     * @return type
     */
    public function isDeliberant($id)
    {
        $projet = $this->find('first', [
            'fields' => ['Deliberation.id'],
            'contain' => [
                'Typeacte' => [
                    'fields' => ['Typeacte.id','Typeacte.deliberant'],
                ]],
            'conditions' => ['Deliberation.id' => $id],
            'recursive' => -1
        ]);

        return !empty($projet['Typeacte']['deliberant']) && $projet['Typeacte']['deliberant'];
    }

    /**
     * @access public
     * @param type $delib_id
     * @param type $seance_deliberante_id
     * @return type
     */
    public function reportePositionToCommissions($delib_id, $seance_deliberante_id)
    {
        $position = $this->getPosition($delib_id, $seance_deliberante_id);
        $seances = $this->getSeancesid($delib_id);

        App::import('Model', 'DeliberationSeance');
        $this->DeliberationSeance = ClassRegistry::init('DeliberationSeance');

        foreach ($seances as $seance_id) {
            $DeliberationSeance = $this->DeliberationSeance->find('first', [
                'conditions' => [
                    'DeliberationSeance.deliberation_id' => $delib_id,
                    'DeliberationSeance.seance_id' => $seance_id
                ],
                'fields' => ['DeliberationSeance.id'],
                'recursive' => -1]);
            $this->DeliberationSeance->id = $DeliberationSeance['DeliberationSeance']['id'];
            $this->DeliberationSeance->saveField('position', $position);
        }
        return $seances;
    }

    /**
     * [setHistorique description]
     * @param [type]  $message    [description]
     * @param [type]  $delib_id   [description]
     * @param [type]  $circuit_id [description]
     * @param integer $user_id [description]
     *
     * @version 5.1.0
     * @since 4.0.0
     */
    public function setHistorique($message, $id, $circuit_id = null, $user_id = -1)
    {
        $this->versioningEnable();

        $deliberation_version = $this->lastVersion($id);

        $this->Historique->create();
        $history = [];
        $history['Historique']['delib_id'] = $id;
        $history['Historique']['user_id'] = $user_id;
        $history['Historique']['commentaire'] = $message;
        $history['Historique']['circuit_id'] = $circuit_id;
        $history['Historique']['revision_id'] =
            isset($deliberation_version['deliberations_versions']['id'])
                ? $deliberation_version['deliberations_versions']['id'] : 0;

        return $this->Historique->save($history, false);
    }

    /**
     * [setCommentaire description]
     * @param [type] $delib_id    [description]
     * @param [type] $commentaire [description]
     *
     * @version 5.1.0
     * @since 4.0.0
     */
    public function setCommentaire($id, $text, $date = null)
    {
        $this->Commentaire->create();
        $comment = [];
        $comment['Commentaire']['delib_id'] = $id;
        $comment['Commentaire']['agent_id'] = -1;
        $comment['Commentaire']['pris_en_compte'] = 0;
        $comment['Commentaire']['commentaire_auto'] = false;
        $comment['Commentaire']['texte'] = $text;
        if (!empty($date)) {
            $comment['Commentaire']['created'] = $date;
        }

        return $this->Commentaire->save($comment['Commentaire']);
    }

    /**
     * @return string status d'exécution et rapport
     * @version 4.3
     * @access public
     */
    public function majActesParapheur()
    {
        // status sous forme de chaines de caractères a insérer dans le rapport
        // d'exécution des procédures appelées par les crons
        $ret_success = 'TRAITEMENT_TERMINE_OK';
        $ret_error = 'TRAITEMENT_TERMINE_ERREUR';
        try {
            //Si service désactivé ==> quitter
            if (!Configure::read('USE_PARAPHEUR')) {
                return $ret_error . 'Parapheur désactivé';
            }
            App::import('Component', 'Iparapheur');
            $this->Iparapheur = new IparapheurComponent();

            // Controle de l'avancement des délibérations dans le parapheur
            $delibs = $this->find('all', [
                'conditions' => [
                    'Deliberation.etat >' => 2,
                    'Deliberation.parapheur_etat' => 1
                ],
                'recursive' => -1,
                'fields' => ['id', 'objet']]);

            $rapport = "\n";
            foreach ($delibs as $delib) {
                $objetDossier = $delib['Deliberation']['objet'];
                $rapport .= 'Projet "[' . $delib['Deliberation']['id'] . '] - '
                    . $delib['Deliberation']['objet'] . '" : ';
                $success_dos = $this->majActeParapheur($delib['Deliberation']['id']);
                if (!$success_dos) {
                    $rapport .= __("En attente de fin de circuit") . "\n";
                } else {
                    $rapport .= __("Circuit terminé, dossier effacé du iparapheur") . "\n";
                }
            }
            return $ret_success . "\n" . $rapport . "\n";
        } catch (Exception $e) {
            return $ret_error . $e->getTraceAsString();
        }
    }

    /**
     * @param integer $acte_id -->identifiant du projet à mettre à jour
     * @param string $objet
     * @param boolean $tdt
     * @return boolean --> true si le dossier à terminé son circuit
     * @version 4.3
     * @access public
     */
    public function majActeParapheur($acte_id)
    {
        $this->Behaviors->load('Version', $this->actsAsVersionOptionsList['Version']);

        $this->id = $acte_id;
        $acte = $this->find('first', [
            'fields' => ['parapheur_id'],
            'conditions' => [
                'id' => $acte_id,
                'etat >' => 2,
                'parapheur_etat' => 1
            ],
            'recursive' => -1
        ]);

        App::import('Component', 'Iparapheur');
        $this->Iparapheur = new IparapheurComponent();
        $infolog = 'Iparapheur (id:' . $acte_id . ', parapheur_id: ' . $acte['Deliberation']['parapheur_id'] . ') => ';
        // Récupération log historique coté parapheur
        $histo = $this->Iparapheur->getHistoDossierWebservice($acte['Deliberation']['parapheur_id']);

        if (isset($histo['logdossier'])) {
            CakeLog::info($infolog . 'Lecture du dossier');
            $recuperation = false;
            $signee = false;
            $refus = false;
            // Parcours des étapes du log
            $parafhisto = "Circuit de signature terminée :";
            for ($i = 0; $i < count($histo['logdossier']); $i++) {
                //Gestion des commentaires
                $date_histo = CakeTime::i18nFormat($histo["logdossier"][$i]['timestamp'], '%d/%m/%Y %k:%M');
                if (preg_match("/Dossier déposé*/", $histo["logdossier"][$i]['annotation']) == 1) {
                    //Changement de bureau
                    $parafhisto .= "\n" . $date_histo . " [Parapheur] "
                        . $histo["logdossier"][$i]['annotation'];
                } elseif ($histo["logdossier"][$i]['status'] == "Vise") { //Visa
                    $parafhisto .= "\n" . $date_histo . " [Parapheur] Dossier visé par "
                        . $histo["logdossier"][$i]['nom'];
                } elseif ($histo["logdossier"][$i]['status'] == "Signe") { //Signature
                    $parafhisto .= "\n" . $date_histo . " [Parapheur] Dossier signé par "
                        . $histo["logdossier"][$i]['nom'];
                } elseif (preg_match("/^Rejet*/", $histo["logdossier"][$i]['status']) == 1) { //Rejet
                    $parafhisto .= "\n" . $date_histo
                        . " [Parapheur] Dossier rejeté par "
                        . $histo["logdossier"][$i]['nom']
                        . " pour le motif suivant : "
                        . $histo["logdossier"][$i]['annotation'];
                } elseif (preg_match("/^Archive*/", $histo["logdossier"][$i]['status']) == 1) { //Archive
                    $parafhisto .= "\n" . $date_histo . ' [Parapheur] Dossier récupéré';
                }

                //Gestion du refus
                if (in_array($histo['logdossier'][$i]['status'], ['RejetSignataire', 'RejetVisa'], true)) {
                    // Etat refusé
                    $this->saveField('parapheur_etat', -1);
                    // Motif de rejet
                    $this->saveField(
                        'parapheur_commentaire',
                        AppTools::truncateComment($parafhisto, 1400)
                    );
                    // Supprimer le dossier du parapheur
                    $this->Iparapheur->effacerDossierRejeteWebservice($acte['Deliberation']['parapheur_id']);
                    // Annotation (motif de rejet)
                    $this->setCommentaire(
                        $acte_id,
                        AppTools::truncateComment($parafhisto, 1400),
                        CakeTime::format($histo["logdossier"][$i]['timestamp'], '%Y-%m-%d %H:%M:%S')
                    );
                    // Ajout de l'info de rejet parapheur dans l'historique
                    $this->setHistorique(__('Dossier rejeté par le parapheur'), $acte_id);
                    CakeLog::info($infolog . 'Refus de signature');

                    return true;
                }
                $signee = (!$signee && $histo['logdossier'][$i]['status'] == 'Signe' ? true : $signee);
                $signature_date = ($signee
                    ? CakeTime::format($histo['logdossier'][$i]['timestamp'], '%Y-%m-%d %H:%M:%S') : null);
                $recuperation = (!$recuperation && $histo['logdossier'][$i]['status'] == 'Archive'
                    ? true : $recuperation);
            }

            //Récuparation du dossier et informations sur le circuit
            if ($recuperation && $signee) {
                CakeLog::info($infolog . 'Récupération signature');
                $acte_signee = [
                    'signature_date' => $signature_date,
                    'signee' => true,
                    'parapheur_etat' => 2,
                    'parapheur_commentaire' => AppTools::truncateComment($parafhisto, 1400)
                ];
                if (!$this->isDeliberant($acte_id)) {
                    $acte_signee['date_acte'] = $signature_date;
                }
                //Récupération du dossier parapheur
                $dossier = $this->Iparapheur->GetDossierWebservice($acte['Deliberation']['parapheur_id']);

                if (!empty($dossier['getdossier']['docprinc']) && !empty($dossier['getdossier']['bordereau'])) {
                    // Sauvegarde du doc principal
                    $acte_signee['delib_pdf'] = base64_decode($dossier['getdossier']['docprinc'], true);
                    // Sauvegarde du bordereau (pdf)
                    $acte_signee['parapheur_bordereau'] =
                        base64_decode($dossier['getdossier']['bordereau'], true);
                }

                if (empty($dossier['getdossier']['signatureformat'])
                    || strpos($dossier['getdossier']['signatureformat'], 'PAdES') === false
                ) {
                    // Sauvegarde des signatures (zip)
                    if (!empty($dossier['getdossier']['signature'])) {
                        $this->saveField(
                            'signature',
                            base64_decode($dossier['getdossier']['signature'], true)
                        );
                    }
                    $this->saveField('signature_type', 'PKCS#7');
                } elseif (!empty($dossier['getdossier']['signatureformat'])
                    && strpos($dossier['getdossier']['signatureformat'], 'PAdES') !== false) {
                    $acte_signee['signature_type'] = 'PAdES';
                }

                $this->setCommentaire($acte_id, AppTools::truncateComment($parafhisto, 1400));
                $this->save($acte_signee, true, array_keys($acte_signee));
                // Ajout de l'info d:q!
                // :'approbation parapheur dans l'historique
                $this->setHistorique('Dossier approuvé par le parapheur', $acte_id);

                $this->Iparapheur->archiverDossierWebservice(
                    $acte['Deliberation']['parapheur_id'],
                    'EFFACER'
                );

                return true;
            }
            //Récupération de la dernière étape pour les logs
            end($histo['logdossier']);
            CakeLog::info($infolog
                . 'Rien à faire (' . $histo['logdossier'][key($histo['logdossier'])]['status']
                . ')');
        }
        //FIX Le dossierID est inconnu dans le Parapheur.

        return false;
    }

    /**
     * @access public
     * @return string --> status d'exécution et rapport
     * @link DeliberationsController::refreshPastell()
     * @link Signature::updateAll()
     */
    public function majSignaturesPastell()
    {
        // status sous forme de chaines de caractères a insérer dans le rapport
        // d'exécution des procédures appelées par les crons
        try {
            //Si service désactivé ==> quitter
            if (!Configure::read('USE_PASTELL')) {
                return "TRAITEMENT_TERMINE_ERREUR" . "\n" . __("Pastell désactivé");
            }

            // Controle de l'avancement des délibérations dans le parapheur
            $delibs = $this->find('all', [
                'conditions' => [
                    'Deliberation.etat >' => 2,
                    'Deliberation.parapheur_etat' => 1,
                    'Deliberation.parapheur_id !=' => null
                ],
                'recursive' => -1,
                'fields' => ['id', 'objet']]);
            $rapport = '';
            foreach ($delibs as $delib) {
                $rapport .= "\n" . __('Projet id n°%s : ', $delib['Deliberation']['id']);
                try {
                    $success_dos = $this->majSignaturePastell($delib['Deliberation']['id']);
                } catch (Exception $e) {
                    $rapport .= $e->getMessage();
                    continue;
                }
                if (!$success_dos) {
                    $rapport .= __("En attente de fin de circuit");
                } else {
                    $rapport .= __("Circuit terminé, dossier rapatrié");
                }
            }
            return 'TRAITEMENT_TERMINE_OK' . "\n" . $rapport;
        } catch (Exception $e) {
            return 'TRAITEMENT_TERMINE_ERREUR'
                . "\n Line: " . $e->getFile() . ' ' . $e->getLine() . "\n message:" . $e->getMessage();
        }
    }

    /**
     * @param integer $delib_id --> identifiant du projet à mettre à jour
     * @return boolean --> true si le dossier à terminé son circuit
     * @throws Exception
     * @version 4.3
     * @access public
     */
    public function majSignaturePastell($delib_id)
    {
        $this->Behaviors->load('Version', $this->actsAsVersionOptionsList['Version']);

        $delib = $this->find('first', [
            'recursive' => -1,
            'fields' => ['id', 'objet', 'pastell_id'],
            'conditions' => [
                'Deliberation.id' => $delib_id,
                'Deliberation.etat >' => 2,
                'Deliberation.parapheur_etat' => 1
            ]]);
        if (empty($delib)) {
            return false;
        }
        App::uses('Signature', 'Lib');
        $this->Signature = new Signature();
        $infos = $this->Signature->getDetailsPastell($delib['Deliberation']['pastell_id']);
        $this->id = $delib_id;

        if ($infos['last_action']['action'] == 'rejet-iparapheur') {
            $this->saveField('parapheur_etat', '-1');
            $this->saveField('parapheur_commentaire', $infos['last_action']['message']);
            //Ajout de l'action à l'historique
            $this->setHistorique($infos['last_action']['message'], $delib_id, 0);
            //Commentaire refus
            $this->setCommentaire($delib_id, $infos['last_action']['message']);
            return true;
        } elseif ($infos['last_action']['action'] == 'recu-iparapheur' || !empty($infos['data']['has_signature'])) {
            //Récupération du bordereau
            $bordereau = $this->Signature->getBordereau($delib['Deliberation']['pastell_id']);
            if (!empty($bordereau)) {
                $this->saveField('parapheur_bordereau', $bordereau);
            }

            //Signature
            if (!empty($infos['data']['has_signature'])) {
                if (!empty($infos['data']['is_pades']) && $infos['data']['is_pades'] === '1') {
                    //Récupération du document principal
                    $doc_principal = $this->Signature->getDocumentSigne($delib['Deliberation']['pastell_id']);
                    if (!empty($doc_principal)) {
                        $this->saveField('delib_pdf', $doc_principal);
                    }
                    //PKCS#7/single
                    //PAdES/basic
                    //PAdES/basicCertifie
                    $this->saveField('signature_type', 'PAdES');
                } else {
                    $doc_principal = $this->Signature->getDocument($delib['Deliberation']['pastell_id']);
                    if (!empty($doc_principal)) {
                        $this->saveField('delib_pdf', $doc_principal);
                    }
                    $signature = $this->Signature->getSignature($delib['Deliberation']['pastell_id']);
                    if (!empty($signature)) {
                        $this->saveField('signature', $signature);
                    }
                    $this->saveField('signature_type', 'PKCS#7');
                }
                $date_signature = $this->Signature->getSignatureDateFromParapheurHistoric(
                    $delib['Deliberation']['pastell_id']
                );
                if (!$this->isDeliberant($delib_id)) {
                    $this->saveField(
                        'date_acte',
                        CakeTime::format(
                            $date_signature ?? $infos['data']['date_de_lacte'],
                            '%Y-%m-%d %H:%M:%S'
                        )
                    );
                }
                $this->saveField(
                    'signature_date',
                    CakeTime::format($date_signature, '%Y-%m-%d %H:%M:%S')
                );
                $this->saveField('signee', true);
            }
            $this->saveField('parapheur_etat', '2');
            $this->saveField('parapheur_commentaire', $infos['last_action']['message']);

            $this->setHistorique($infos['last_action']['message'], $delib_id, 0);
            return true;
        }
        return false;
    }

    /**
     * Renvoie les délibérations envoyées il y a moins de 2 mois
     * @param type $delib_id
     * @param type $days
     */
    private function findDeliberationsArecuperer($delib_id = null, $tdt_status = null, $days = null)
    {
        if (!empty($delib_id)) {
            $conditions = ['id' => $delib_id];
        } else {
            $conditions = ['etat' => 5];

            if (!is_null($days)) {
                $strtotime_days = strtotime("-" . $days . " days");
                $date_recuperation = date('Y-m-d 00:00:00', $strtotime_days);
                array_push($conditions, ['OR' => [
                    'date_envoi_signature >=' => $date_recuperation, // Pour version antérieure à v4.3
                    'date_acte >=' => $date_recuperation, // Pour version antérieure à v4.3
                    'tdt_ar_date >=' => $date_recuperation,
                ]]);
            }
            if (empty($tdt_status)) {
                array_push($conditions, ['OR' => [
                    'tdt_status IS NULL',
                    'tdt_status' => ['0', 0]
                ]
                ]);
            }
            if (!is_array($tdt_status)) {
                array_push($conditions, $tdt_status);
            }
        }
        $actes = $this->find('all', [
            'fields' => [
                'id',
                'tdt_id',
                'pastell_id',
                'tdt_ar_date',
                'num_delib',
                'tdt_ar',
                'tdt_status'
            ],
            'contain' => [
                'Annexe' => ['fields' => ['id', 'position', 'filename', 'size', 'filetype'],
                    'conditions' => [
                        'joindre_ctrl_legalite' => true
                    ],
                    'order' => 'Annexe.position ASC'
                ]
            ],
            'conditions' => $conditions,
            'order' => ['id' => 'DESC'],
            'recursive' => -1,
        ]);
        return $actes;
    }

    /**
     * Met à jour un champ pdf tamponné si celui-ci est vide
     *
     * @param type $acteTdt
     * @param type $dataWD
     * @param type $field
     * @param type $method
     * @param type $num_delib
     * @param type $nomActe
     * @return string
     */
    public function majPdfTamponne($acteTdt, $dataWD, $field, $method, $num_delib, $nomActe)
    {
        $result = null;
        if (empty($dataWD->field($field))) {
            if ($acteTdt->{$method.'Exist'}()) {
                try {
                    $tampon = $acteTdt->{$method}();
                    $dataWD->save([$field => $tampon]);
                    $result = "Délibération (" . $nomActe . ") " . $num_delib
                        . " : " . __('Échange mis à jour') . "\n";
                } catch (Exception $e) {
                    $result = "Délibération (" . $nomActe . ") " . $num_delib
                        . " : " . __('En attente de réception') . "\n";
                }
            }
        }
        return $result;
    }

    /**
     * Mise à jour des echange TDT / Préfecture pour les envois de moins de 2 mois
     *
     * @access public
     *
     * @param type $delib_id
     * @return string
     *
     * @version 5.1.4
     * @since 4.3.0
     */
    public function updateTdtFiles($delib_id = null, $days = null)
    {
        $tdt = new Tdt();
        $rapport = '';

        $actes = $this->findDeliberationsArecuperer($delib_id, null, (is_null($days) ? 30 : $days));
        $this->Behaviors->load('Version', $this->actsAsVersionOptionsList['Version']);

        foreach ($actes as $acte) {
            try {
                $this->id = $acte['Deliberation']['id'];

                $acteTdt = $tdt->createActeTdt($acte);
                $rapport .= $this->majAR($acte, $acteTdt);                      //Mise à jour de l'AR
                if (!empty($this->field('tdt_ar'))) {
                    if ($this->saveField('tdt_status', 1)) {
                        $this->setHistorique(
                            __('Acquittement reçu du tiers de télétransmission.'),
                            $this->field('id'),
                            0
                        );
                    }
                }
                //Mise à jour des PDFs (Acte tamponné, Bordereau, Annexes éventuelles)
                $rapport .= $this->majPdf($acte, $acteTdt);
                // Sauvegarde du tdt_id si celui-ci n'existe pas (Pastell Mégalis)
                if (!empty($this->field('tdt_id'))) {
                    $this->saveField('tdt_id', $acteTdt->getTdtId());
                }
            } catch (Exception $e) {
                $rapport .= "Délibération " . $acte['Deliberation']['num_delib'] . " : " .
                    __(
                        '%s (%s)',
                        $e->getCode() ==='423' ? 'Échange en attente de reception' : 'Échange en erreur',
                        $e->getMessage()
                    ) . "\n";
            }
            $this->clear();
        }

        return $rapport;
    }

    /**
     * Mise à jour des echange TDT / Préfecture pour les envois de moins de 2 mois
     *
     * @param type $delib_id
     * @return string
     * @version 5.0.1
     * @since 4.3
     * @access public
     */
    public function updateTdtStatus($delib_id = null, $days = null, $notification = true)
    {
        App::uses('Profil', 'Model');
        $profil = ClassRegistry::init('Profil');
        $tdt = new Tdt();

        $tmpRapport = '';
        $rapport = '';
        $rapportMail = '';

        $tdt_status = ['OR' => [
            'tdt_status IS NULL',
            'tdt_status' => ['0', 0, 1, -2, -3]
        ]];

        $actes = $this->findDeliberationsArecuperer($delib_id, $tdt_status, (is_null($days) ? 80 : $days));

        foreach ($actes as $acte) {
            try {
                $this->id = $acte['Deliberation']['id'];
                $acteTdt = $tdt->createActeTdt($acte);
                if ($acteTdt->checkStatut('Annulé') && ($acte['Deliberation']['tdt_status'] !== -2)) {
                    $this->saveField('tdt_status', -2);
                    $tmpRapport = __(
                        'Délibération %s: Acte annulé sur le TDT',
                        $acte['Deliberation']['num_delib']
                    ) . "\n";
                    $rapport .= $tmpRapport;
                    $rapportMail .= $tmpRapport;
                    continue;
                }
                if ($acteTdt->checkStatut('Erreur') && ($acte['Deliberation']['tdt_status'] !== -1)) {
                    $this->saveField('tdt_status', -1);
                    $messageErreurTdt = $acteTdt->getMessageErreur();
                    $this->saveField('tdt_message', $messageErreurTdt);
                    $tmpRapport = __(
                        'Délibération %s: Échange en erreur TDT(%s)',
                        $acte['Deliberation']['num_delib'],
                        $messageErreurTdt
                    ) . "\n";
                    $rapport .= $tmpRapport;
                    $rapportMail .= $tmpRapport;
                    continue;
                }

                if ($acte['Deliberation']['tdt_status'] === -3) {
                    $this->saveField('tdt_status', 0);
                    continue;
                }

                if ($acte['Deliberation']['tdt_status'] === -1) {
                    $okTdtStatuts = ['Posté',
                        'En attente de transmission',
                        'Transmis',
                        'Acquittement reçu',
                        'Validé',
                        'Refusé',
                        'Document reçu',
                        'Acquittement envoyé',
                        'Document envoyé',
                        'Acquittement de document reçu',
                        'Envoyé au SAE',
                        'Archivé par le SAE',
                        'Erreur lors de l\'archivage',
                        'Reçu par le SAE',
                        'En attente d\'être postée',
                        'En attente d\'être signée',
                        'En attente de transmission au SAE',
                        'Erreur lors de l\'envoi au SAE',
                        'Document reçu (pas d\'AR)'
                    ];

                    foreach ($okTdtStatuts as $okTdtStatut) {
                        if ($acteTdt->checkStatut($okTdtStatut)) {
                            $this->saveField('tdt_status', 0);
                        }
                    }
                }
            } catch (Exception $e) {
                if (is_null($acte['Deliberation']['tdt_status'])) {
                    $this->saveField('tdt_status', -3);
                    $tmpRapport = "Délibération " . $acte['Deliberation']['num_delib']
                        . " : " . __('Échange en erreur (%s)', $e->getMessage()) . "\n";
                    $rapport .= $tmpRapport;
                    $rapportMail .= $tmpRapport;
                }
            }
        }
        if (!empty($rapportMail) && $notification) {
            $profil->notifierAdmin('majtdtcourrier', $rapportMail);
        }
        return $rapport;
    }

    /**
     * Met  à jour l'acte de réception d'un acte
     *
     * @param type $acte
     * @param type $acteTdt
     * @return string        Rapport d'execution
     */
    public function majAR(&$acte, $acteTdt)
    {
        $rapport = '';
        $num_delib = $acte['Deliberation']['num_delib'];
        if (empty($acte['Deliberation']['tdt_ar'])) {
            $tdtArActe = $acteTdt->getARActe();
            if (!empty($tdtArActe)) {
                $this->saveField('tdt_ar', $tdtArActe);     //Récupération de l'ARacte
                $rapport .= "Délibération " . $num_delib . " : " . __('Échange mis à jour') . "\n";
                $acte['Deliberation']['tdt_ar'] = $tdtArActe;
            } else {
                $rapport .= "Délibération " . $num_delib . " : " . __('Échange non mis à jour') . "\n";
            }
        }
        return $rapport;
    }

    /**
     * Met à jour les pdf tamponnés d'un acte (tampon, bordereau et annexes tamponnées
     *
     * @param type $acte
     * @param type $acteTdt
     * @return type
     */
    public function majPdf($acte, $acteTdt)
    {
        App::uses("AnnexesMatcher", "Lib/Tdt");
        $rapport = '';
        $num_delib = $acte['Deliberation']['num_delib'];
        if (!empty($acte['Deliberation']['tdt_ar'])) {
            $this->recursive = -1;
            $rapport .= $this->majPdfTamponne(
                $acteTdt,
                $this,
                'tdt_data_pdf',
                'getTampon',
                $num_delib,
                'Acte tampon'
            );                     //Récupération du tampon
            $rapport .= $this->majPdfTamponne(
                $acteTdt,
                $this,
                'tdt_data_bordereau_pdf',
                'getBordereau',
                $num_delib,
                'Acte bordereau'
            );   //Récupération du bordereau
            if (!empty($acte['Annexe'])) {                             //Récupération des annexes tamponnées
                $annexesDisponibles = $acteTdt->getAnnexesDetails();   //Récupération de la liste des annexes :
                $annexesMatcher = new AnnexesMatcher($acte['Annexe'], $annexesDisponibles);
                $closestArrangement = $annexesMatcher->findClosestArrangement();
                foreach ($closestArrangement as $arrangement) {
                    $id_S2low = $arrangement['id_S2low'];
                    $id_WD = $arrangement['id_WD'];
                    $annexesTdt = $acteTdt->findAnnexes(["id" => $id_S2low]);
                    if (!empty($annexesTdt)) {
                        $this->Annexe->id = $id_WD;
                        $annexeTdt = $annexesTdt[0];
                        $rapport .= $this->majPdfTamponne(
                            $annexeTdt,
                            $this->Annexe,
                            'tdt_data_pdf',
                            'getfichiertamponne',
                            $num_delib,
                            'Annexe tampon'
                        );
                    }
                }
            }
        }
        return $rapport;
    }

    /**
     * Récupère les messages du tdt
     *
     * @param type $delib_id
     * @param type $days
     */
    public function updateTdtMinisterialMails($delib_id = null, $days = null)
    {
        $rapport = "";
        $actes = $this->findDeliberationsArecuperer($delib_id, ['tdt_status' => 1], (is_null($days) ? 80 : $days));
        foreach ($actes as $acte) {
            try {
                if ($this->majEchangesTdt($acte)) {                    //Récupération des courriers
                    $rapport .= "Délibération " . $acte['Deliberation']['num_delib']
                        . " : " . __('Échange mis à jour') . "\n";
                }
            } catch (Exception $e) {
                $rapport .= "Délibération " . $acte['Deliberation']['num_delib']
                    . " : " . __('Échange en erreur (%s)', $e->getMessage()) . "\n";
                continue;
            }
        }
        return $rapport;
    }

    /**
     * Met à jour la date d'AR d'une délib
     *
     * @param array $acte --> objet Deliberation
     * @return boolean
     * @version 4.3
     * @access public
     */

    public function majEchangesTdt($acte)
    {
        App::uses('Tdt', 'Lib');
        try {
            $Tdt = new Tdt();
            if (!$infos = $Tdt->getReponses($acte)) {
                return false;
            }
            $this->TdtMessage->begin();
            foreach ($infos as $info) {
                //Si Le message existe déjà ne rien faire
                $message = $this->TdtMessage->find('first', [
                    'fields' => ['id', 'tdt_id'],
                    'conditions' => ['tdt_id' => $info['id']],
                    'recursive' => -1,
                ]);

                if (!empty($message)) {
                    continue;
                }
                //Recherche un parent a ce message
                $message_parent = $this->TdtMessage->find('first', [
                    'fields' => ['id', 'tdt_id'],
                    'conditions' => [
                        'tdt_type' => $info['type'],
                        'delib_id' => $acte['Deliberation']['id'],
                        'parent_id IS NULL'
                    ],
                    'recursive' => -1,
                ]);

                if (!empty($message_parent) && !empty($info['status']) && in_array($info['status'], [7, 8], true)) {
                    throw new Exception(__(
                        "Attention, il y a plusieurs messages de même type, '
                        . 'cette situation n'est pas traitée par webdelib (num_delib:%s => %s)",
                        $acte['Deliberation']['id'],
                        $info['type']
                    ));
                }

                $this->TdtMessage->create();
                $TdtMessage = [
                    'delib_id' => $acte['Deliberation']['id'],
                    'tdt_id' => $info['id'],
                    'tdt_type' => $info['type'],
                    'tdt_etat' => empty($info['status']) ? null : $info['status'],
                    'tdt_data' => $info['data'],
                    'date_message' => empty($info['date_message']) ? null : $info['date_message'],
                    'parent_id' => !empty($message_parent) ? $message_parent['TdtMessage']['id'] : null,
                ];
                $this->TdtMessage->save($TdtMessage);
            }
            $this->TdtMessage->commit();
            return true;
        } catch (Exception $e) {
            $this->TdtMessage->rollback();
            $this->log($e->getMessage() . PHP_EOL . $e->getTraceAsString(), 'tdt');
            return false;
        }
    }

    /**
     * @access public
     * @param type $projet_id
     * @return mixed int|false
     * @since 4.0
     * @version 5.0
     */
    public function chercherVersionSuivante($projet_id)
    {
        $this->recursive = -1;
        $projet = $this->findByAnterieureId($projet_id, ['id']);
        return empty($projet) ? false : $projet['Deliberation']['id'];
    }

    /**
     * Fonction de callback du behavior OdtFusion
     * retourne l'id du model odt à utiliser pour la fusion
     *
     * @access public
     * @param integer $id --> id de l'occurence en base de données
     * @param array $modelOptions --> options gérées par la classe appelante
     * @return integer --> id du modele odt à utiliser
     */
    public function getModelTemplateId($id, $modelOptions)
    {
        return $this->getModelId($id);
    }

    /**
     * [beforeValidate description]
     * @param array $options [description]
     * @return [type]          [description]
     * @version 5.1.0
     */
    public function beforeValidate($options = [])
    {
        $textes = ['texte_projet', 'texte_synthese', 'deliberation'];
        foreach ($textes as $texte) {
            $this->prepareTextesFiles($texte);
        }

        return true;
    }

    /**
     * Préapration des textes de projets pour la validation
     * @param array $options [description]
     * @return [type]          [description]
     *
     * @version 5.1.0
     */
    public function prepareTextesFiles($texte)
    {
        // && $this->validates(['fieldList' => [$texte . '_upload']]
        if (!empty($this->data['Deliberation'])) {
            if (array_key_exists($texte . '_upload', $this->data['Deliberation'])) {
                $this->data['Deliberation'][$texte . '_name'] =
                    $this->data['Deliberation'][$texte . '_upload']['name'];
                $this->data['Deliberation'][$texte . '_size'] =
                    $this->data['Deliberation'][$texte . '_upload']['size'];
                $this->data['Deliberation'][$texte . '_type'] = 'application/vnd.oasis.opendocument.text';
                $this->data['Deliberation'][$texte] =
                    !empty($this->data['Deliberation'][$texte . '_upload']['tmp_name']) ?
                        file_get_contents($this->data['Deliberation'][$texte . '_upload']['tmp_name'])
                        : '';
                unset($this->data['Deliberation'][$texte . '_upload']);
            } elseif (array_key_exists('file_' . $texte, $this->data['Deliberation'])) {
                $file = new File(TMP . 'files' . DS . $this->data['Deliberation']['file_' . $texte]);
                $this->data['Deliberation'][$texte] = $file->read();
                $this->data['Deliberation'][$texte . '_size'] = $file->size();
            }
        }
    }

    /**
     * [beforeValidate description]
     * @param array $options [description]
     * @return [type]          [description]
     * @version 5.1.0
     */
    public function afterValidate($options = [])
    {
        return true;
    }

    /**
     * @access public
     * @param type $options
     * @return boolean
     */
    public function beforeSave($options = [])
    {
        if (!empty($this->data['Deliberation']['num_pref'])) {
            $this->data['Deliberation']['num_pref'] =
                str_replace('_', '.', $this->data['Deliberation']['num_pref']);
        }
        if (!empty($this->data['Deliberation']['date_limite'])) {
            $this->data['Deliberation']['date_limite'] =
                CakeTime::format(
                    str_replace('/', '-', $this->data['Deliberation']['date_limite']),
                    '%Y-%m-%d 00:00:00'
                );
        }
        //For management multi-délibération
        if (isset($this->data['Deliberation']['is_multidelib_management'])
            && $this->data['Deliberation']['is_multidelib_management'] === false
            && !empty($this->data['Deliberation']['objet'])
        ) {
            $this->data['Deliberation']['objet_delib'] = $this->data['Deliberation']['objet'];
        }

        if (!empty($this->data['Deliberation']['texte_doc'])
            && !empty($this->data['Deliberation']['texte_doc']['tmp_name'])
        ) {
            $analyse = $this->analyzeFile($this->data['Deliberation']['texte_doc']['tmp_name']);
            $this->data['Deliberation']['debat_type'] = $analyse['mimetype'];
            $this->data['Deliberation']['debat_name'] = $this->data['Deliberation']['texte_doc']['name'];
            $this->data['Deliberation']['debat_size'] = $this->data['Deliberation']['texte_doc']['size'];
            $this->data['Deliberation']['debat'] =
                file_get_contents($this->data['Deliberation']['texte_doc']['tmp_name']);
        }

        return true;
    }

    /**
     * @param type $query
     * @return type
     * @version 4.3
     * @access public
     */
    public function beforeFind($query = [])
    {
        $query['conditions'] = (is_array($query['conditions'])) ? $query['conditions'] : [];
        $db = $this->getDataSource();

        //Gestion des droits sur les types d'actes
        //  'allow' => array()
        // if (&& !isset($query['Deliberation.typeacte_id']))
        //if (!empty($query['allow']) && in_array( $query['allow'], 'Deliberation.typeacte_id'))
        if (!empty($query['allow']) && in_array('typeacte_id', $query['allow'], true)) {
            $conditions = [];
            $Aro = ClassRegistry::init('Aro');

            $Aro->Behaviors->attach('Database.DatabaseTable');
            $Aro->Permission->Behaviors->attach('Database.DatabaseTable');
            $Aro->Permission->Aco->Behaviors->attach('Database.DatabaseTable');

            $subQuery = [
                'fields' => [
                    'Aco.foreign_key'
                ],
                'contain' => false,
                'joins' => [
                    $Aro->join('Permission', ['type' => 'INNER']),
                    $Aro->Permission->join('Aco', ['type' => 'INNER'])
                ],
                'conditions' => [
                    'Aro.foreign_key' => AuthComponent::user('id'),
                    'Permission._read' => 1,
                    'Aco.model' => 'Typeacte'
                ]
            ];

            $subQuery = $Aro->sql($subQuery);

            $subQuery = ' "' . $this->alias . '"."typeacte_id" IN (' . $subQuery . ') ';
            $subQueryExpression = $db->expression($subQuery);
            $conditions[] = $subQueryExpression;

            $query['conditions'] = array_merge($query['conditions'], $conditions);
        }
        if (!empty($query['conditions']) && array_key_exists('etat_id', $query['conditions'])) {
            $conditions = [];
            switch ($query['conditions']['etat_id']) {
                case -2:// actes refusé
                    $conditions["Deliberation.etat"] = 0;
                    $conditions['NOT']["Deliberation.anterieure_id"] = null;
                    break;
                case 2:// actes validés
                    $conditions["Deliberation.etat"] = 2;
                    $conditions['NOT']["Deliberation.signee"] = true;
                    break;
                case 6:// actes numérotés
                    // 2 pour les actes passés dans un circuit après génération du numéro
                    $conditions["Deliberation.etat"] = [2, 3, 4];
                    $conditions['NOT']["Deliberation.num_delib"] = '';
                    $conditions['NOT']["Deliberation.signee"] = true;
                    break;
                case 7:// actes signés
                    $conditions["Deliberation.etat"] = [3, 4];
                    $conditions["Deliberation.signee"] = true;
                    break;
                case 8:// actes non télétransmis
                    $conditions["Deliberation.etat"] = [3, 4];
                    $conditions["Deliberation.signee"] = true;
                    $joins[] = ['table' => 'typeactes',
                        'alias' => 'Typeacte_' . $this->alias . '_beforeFind',
                        'type' => 'INNER',
                        'conditions' => [
                            'Deliberation.typeacte_id = Typeacte_Filter.id',
                            'Typeacte_Filter.teletransmettre' => 'false'
                        ]
                    ];
                    break;
                case 9:// Projets en retard de validation
                    $conditions["Deliberation.etat"] = 1;
                    $conditions["Deliberation.signee"] = false;
                    $joins[] = ['table' => 'wkf_traitements',
                        'alias' => 'Traitement_' . $this->alias . '_beforeFind',
                        'type' => 'INNER',
                        'conditions' => [
                            'Deliberation.id = Traitement_' . $this->alias . '_beforeFind.target_id',
                        ]
                    ];
                    $joins[] = ['table' => 'wkf_visas',
                        'alias' => 'Visa_' . $this->alias . '_beforeFind',
                        'type' => 'INNER',
                        'conditions' => [
                            'Traitement_' . $this->alias . '_beforeFind.id = Visa_'
                            . $this->alias . '_beforeFind.traitement_id',
                            'not' => ['Visa_' . $this->alias . '_beforeFind.date_retard' => null],
                            'Visa_' . $this->alias . '_beforeFind.date_retard <=' => date('Y-m-d H:i:s'),
                            'Visa_' . $this->alias . '_beforeFind.action' => 'RI',
                        ]
                    ];
                    break;
                default:
                    $conditions["Deliberation.etat"] = $query['conditions']['etat_id'];
                    break;
            }

            $query['conditions'] = array_merge($query['conditions'], $conditions);
            unset($query['conditions']['etat_id']);
            if (!empty($joins)) {
                $query['joins'] = isset($query['joins']) ? array_merge($query['joins'], $joins) : $joins;
            }
        }

        return $query;
    }

    /**
     * @access public
     * @return type
     */
    public function parentNode()
    {
        return $this->name;
    }

    /**
     * Fonction de callback du behavior OdtFusion
     * initialise les variables de fusion Gedooo
     *
     * @access public
     * @param type $aData
     * @param object_by_ref $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param integer $id --> id de l'occurence en base de données
     * @param array modelOptions --> options gérées par la classe appelante
     */
    public function beforeFusion(&$aData, &$modelOdtInfos, $id, $modelOptions)
    {
        if (!empty($modelOptions['deliberationIds'])) {
            $this->setVariablesFusionDeliberations($aData, $modelOdtInfos, $modelOptions['deliberationIds']);
        } elseif (!empty($modelOptions['seance_id'])) {
            $this->setVariablesFusion($aData, $modelOdtInfos, $id, $modelOptions['seance_id']);
        } else {
            $this->setVariablesFusion($aData, $modelOdtInfos, $id);
        }
    }

    /**
     * Fonction d'initialisation des variables de fusion pour un projet ou une délibération
     * les bibliothèques Gedooo doivent être inclues par avance
     * génère une exception en cas d'erreur
     *
     * @param type $aData
     * @param phpOdtApi $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param integer $id --> l'id à fusionner
     * @param integer $fusionSeanceId --> id de la séance de la fusion (optionnel, null par défaut)
     * @throws Exception
     *
     * @SuppressWarnings(PHPMD)
     * @since 4.3
     * @access public
     * @version 5.1
     */
    public function setVariablesFusion(&$aData, &$modelOdtInfos, $id, $fusionSeanceId = null)
    {
        App::uses('Component', 'Controller');
        // initialisations
        $collection = new ComponentCollection();
        // liste des champs à lire en base de données
        $fields = ['id', 'typeacte_id', 'parent_id', 'created', 'service_id', 'theme_id',
            'rapporteur_id', 'redacteur_id', 'president_id', 'is_multidelib',
            'titre', 'objet', 'objet_delib', 'etat', 'num_delib', 'num_pref',
            'date_envoi_signature', 'date_acte', 'signee','signature_date','vote_prendre_acte',
            'vote_nb_oui', 'vote_nb_abstention', 'vote_nb_non', 'vote_nb_retrait', 'tdt_ar_date', 'vote_commentaire'];
        if ($modelOdtInfos->hasUserField('texte_projet')) {
            $fields[] = 'texte_projet';
        }
        if ($modelOdtInfos->hasUserFields('texte_deliberation', 'texte_acte')) {
            $fields[] = 'deliberation';
        }
        if ($modelOdtInfos->hasUserField('note_synthese')) {
            $fields[] = 'texte_synthese';
        }
        if ($modelOdtInfos->hasUserField('debat_deliberation')) {
            $fields[] = 'debat';
        }
        //obsolete
        if ($modelOdtInfos->hasUserField('debat_commission')) {
            $fields[] = 'commission';
        }
        if ($modelOdtInfos->hasUserField('tdt_tampon')) {
            $fields[] = 'tdt_data_pdf';
            $fields[] = 'tdt_data_edition';
        }

        // lecture de l'occurence en base de données
        $delib = $this->find('first', [
            'fields' => $fields,
            'contain' => ['Typeacte' => ['fields' => ['id', 'nature_id', 'name']]],
            'conditions' => ['Deliberation.id' => $id],
            'recursive' => -1
        ]);

        if (empty($delib)) {
            throw new Exception('délibération id:' . $id . ' non trouvée en base de données');
        }

        // variables du projet (en dehors de toute section)
        if ($modelOdtInfos->hasUserFieldDeclared('objet_projet')) {
            if (empty($delib['Deliberation']['is_multidelib'])) {
                $aData['objet_projet'] = ['value' => $delib['Deliberation']['objet'], 'type' => 'lines'];
            } else {
                $aData['objet_projet'] = ['value' => $delib['Deliberation']['objet_delib'], 'type' => 'lines'];
            }
        }

        if ($modelOdtInfos->hasUserFieldDeclared('titre_projet')) {
            $aData['titre_projet'] = ['value' => $delib['Deliberation']['titre'], 'type' => 'lines'];
        }

        if ($modelOdtInfos->hasUserFieldDeclared('libelle_projet')) {
            $aData['libelle_projet'] = ['value' => $delib['Deliberation']['objet'], 'type' => 'lines'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('objet_delib')) {
            $aData['objet_delib'] = ['value' => $delib['Deliberation']['objet_delib'], 'type' => 'lines'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('libelle_delib')) {
            $aData['libelle_delib'] = ['value' => $delib['Deliberation']['objet_delib'], 'type' => 'lines'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('identifiant_projet')) {
            $aData['identifiant_projet'] = ['value' => $delib['Deliberation']['id'], 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('service_emetteur')) {
            $aData['service_emetteur'] = [
                'value' => $this->Service->field('name', ['id' => $delib['Deliberation']['service_id']]),
                'type' => 'text'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('service_avec_hierarchie')) {
            $aData['service_avec_hierarchie'] = [
                'value' => $this->Service->doList($delib['Deliberation']['service_id']),
                'type' => 'text'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('etat_projet')) {
            $aData['etat_projet'] = ['value' => $delib['Deliberation']['etat'], 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('etat_projet_alias')) {
            App::uses('ProjetEtat', 'Model');
            $projetEtat = ClassRegistry::init('ProjetEtat');

            $etats = $projetEtat->getLibellesEtat(true);
            $aData['etat_projet_alias'] = ['value' => $etats[$delib['Deliberation']['etat']], 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('classification_deliberation')) {
            $aData['classification_deliberation'] = ['value' => $delib['Deliberation']['num_pref'], 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('nombre_seance')) {
            $aData['nombre_seance'] = ['value' => $this->getNbSeances($delib['Deliberation']['id']), 'type' => 'text'];
        }

        // Information du service émetteur
        if ($modelOdtInfos->hasUserFieldsDeclared('service_emetteur', 'service_avec_hierarchie')) {
            $this->Service->setVariablesFusion($aData, $modelOdtInfos, $delib['Deliberation']['service_id']);
        }
        // Informations sur le thème
        $this->Theme->setVariablesFusion($aData, $modelOdtInfos, $delib['Deliberation']['theme_id']);

        // Informations sur le type d'acte
        $this->Typeacte->setVariablesFusion($aData, $modelOdtInfos, $delib['Deliberation']['typeacte_id']);

        // Informations sur le nature
        $this->Typeacte->Nature->setVariablesFusion($aData, $modelOdtInfos, $delib['Typeacte']['nature_id']);

        // Informations sur le Redacteur
        $this->Redacteur->setVariablesFusion($aData, $modelOdtInfos, $delib['Deliberation']['redacteur_id']);

        // Informations sur le rapporteur
        $this->Rapporteur->setVariablesFusion($aData, $modelOdtInfos, $delib['Deliberation']['rapporteur_id']);

        // Liste des commentaires
        $this->Commentaire->setVariablesFusion($aData, $modelOdtInfos, $delib['Deliberation']['id']);

        // Hitoriques
        $this->Historique->setVariablesFusion($aData, $modelOdtInfos, $id);

        App::uses('CircuitHistorique', 'Model');
        $this->CircuitHistorique = ClassRegistry::init('CircuitHistorique');
        $this->CircuitHistorique->setVariablesFusion($aData, $modelOdtInfos, $id);

        if ($modelOdtInfos->hasUserFieldDeclared('circuit_etape_courante')) {
            $circuit_etape_courante = $this->Traitement->getEtapeCouranteName($id);
            $aData['circuit_etape_courante'] = [
                'value' => !empty($circuit_etape_courante) ? $circuit_etape_courante : '',
                'type' => 'text'
            ];
        }

        // Informations supplémentaires
        $this->Infosup->setVariablesFusion($aData, $modelOdtInfos, 'Deliberation', $delib['Deliberation']['id']);
        // variables de la délibération (en dehors de toute section)
        if ($modelOdtInfos->hasUserFieldDeclared('date_creation')) {
            $aData['date_creation'] = [
                'value' => CakeTime::i18nFormat($delib['Deliberation']['created'], '%d/%m/%Y %H:%M:%S'),
                'type' => 'date'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('numero_acte')) {
            $aData['numero_acte'] = ['value' => $delib['Deliberation']['num_delib'], 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('numero_deliberation')) {
            $aData['numero_deliberation'] = ['value' => $delib['Deliberation']['num_delib'], 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('acte_adopte')) {
            $aData['acte_adopte'] = [
                'value' => in_array($delib['Deliberation']['etat'], [3,5,6,7,8,10,11], true) ? '1' : '0',
                'type' => 'text'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('prendre_acte')) {
            $aData['prendre_acte'] = [
                'value' => $delib['Deliberation']['vote_prendre_acte'] == 1 ? '1' : '0',
                'type' => 'text'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('nombre_pour')) {
            $aData['nombre_pour'] = ['value' => $delib['Deliberation']['vote_nb_oui'], 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('nombre_abstention')) {
            $aData['nombre_abstention'] = ['value' => $delib['Deliberation']['vote_nb_abstention'], 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('nombre_contre')) {
            $aData['nombre_contre'] = ['value' => $delib['Deliberation']['vote_nb_non'], 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('nombre_sans_participation')) {
            $aData['nombre_sans_participation'] = [
                'value' => $delib['Deliberation']['vote_nb_retrait'],
                'type' => 'text'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('nombre_votant')) {
            $aData['nombre_votant'] = [
                'value' => $delib['Deliberation']['vote_nb_oui']
                    + $delib['Deliberation']['vote_nb_abstention']
                    + $delib['Deliberation']['vote_nb_non'],
                'type' => 'text'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('commentaire_vote')) {
            $aData['commentaire_vote'] = ['value' => $delib['Deliberation']['vote_commentaire'], 'type' => 'lines'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('date_reception')) {
            $aData['date_reception'] = [
                'value' => CakeTime::i18nFormat($delib['Deliberation']['tdt_ar_date'], '%d/%m/%Y'),
                'type' => 'date'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('tdt_tampon')) {
            if (!empty($delib['Deliberation']['tdt_data_edition'])) {
                $aData['tdt_tampon'] = ['value' => $delib['Deliberation']['tdt_data_edition'], 'type' => 'file'];
            } elseif (empty($delib['Deliberation']['tdt_data_edition'])
                && !empty($delib['Deliberation']['tdt_data_pdf'])
            ) {
                throw new Exception(
                    'Toutes les documents du projet :'
                    . $id
                    . ' ne sont pas encore converties pour générer le document.'
                    .' \n Veuillez réessayer dans quelques instants ...'
                );
            } else { //Si génération avant envoi au TdT
                $aData['tdt_tampon'] = [
                    'value' => file_get_contents(APP . DS . 'Config' . DS . 'OdtVide.odt'),
                    'type' => 'file'
                ];
            }
        }
        if ($modelOdtInfos->hasUserFieldDeclared('acte_signature')) {
            $aData['acte_signature'] = ['value' => $delib['Deliberation']['signee'] ? '1' : '0', 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('acte_signature_date')) {
            $aData['acte_signature_date'] = [
                'value' => $delib['Deliberation']['signee'] ?
                    CakeTime::i18nFormat($delib['Deliberation']['signature_date'], '%d/%m/%Y') : '',
                'type' => 'date'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('date_envoi_signature')) {
            $aData['date_envoi_signature'] = [
                'value' => !empty($delib['Deliberation']['date_envoi_signature']) ?
                    CakeTime::i18nFormat($delib['Deliberation']['date_envoi_signature'], '%d/%m/%Y') : '',
                'type' => 'date'
            ];
        }
        // variables des multi délibérations
        if ($delib['Deliberation']['is_multidelib']
            && $modelOdtInfos->hasUserFieldsDeclared('libelle_multi_delib', 'id_multi_delib')
        ) {
            $multidelibs = $this->find('all', [
                'recursive' => -1,
                'fields' => ['id', 'objet'],
                'conditions' => ['parent_id' => $delib['Deliberation']['id']]]);
            if (!empty($multidelibs)) {
                foreach ($multidelibs as $multidelib) {
                    if ($modelOdtInfos->hasUserFieldDeclared('libelle_multi_delib')) {
                        $aDeliberations['libelle_multi_delib'] =
                            ['value' => $multidelib['Deliberation']['objet'], 'type' => 'text'];
                    }
                    if ($modelOdtInfos->hasUserFieldDeclared('id_multi_delib')) {
                        $aDeliberations['id_multi_delib'] =
                            ['value' => $multidelib['Deliberation']['id'], 'type' => 'text'];
                    }
                }

                $aData['Deliberations'] = $aDeliberations;
            }
        }

        //Ajout des textes de projet
        $this->setVariablesFusionTextes($modelOdtInfos, $delib, $aData);

        // annexes
        $this->Annexe->setVariablesFusion($aData, $modelOdtInfos, 'Projet', $id);


        // nombre de séances du projet
        $seanceIds = $this->DeliberationSeance->nfield(
            'seance_id',
            ['DeliberationSeance.deliberation_id' => $delib['Deliberation']['id']],
            ['Seance.date']
        );
        $aData['nombre_seance'] = ['value' => count($seanceIds), 'type' => 'text'];

        // position du projet dans la séance de l'édition ou de la séance délibérante
        if ($modelOdtInfos->hasUserFieldDeclared('position_projet')) {
            if (empty($fusionSeanceId)) {
                $positionSeanceId = empty($seanceIds) ? null : $seanceIds[count($seanceIds) - 1];
            } else {
                $positionSeanceId = $fusionSeanceId;
            }
            $position = empty($positionSeanceId)
                ? 0 : $this->getPosition($delib['Deliberation']['id'], $positionSeanceId);
            $aData['position_projet'] = ['value' => $position, 'type' => 'text'];
        }

        if (!empty($fusionSeanceId) || !empty($seanceIds)) {
            // Ajout des informations de séance directement dans la section projet
            $this->Seance->setVariablesFusionSeanceDeliberante(
                $aData,
                $modelOdtInfos,
                !empty($fusionSeanceId) ? $fusionSeanceId : $seanceIds[count($seanceIds) - 1],
                'seance',
                false
            );
        }

        // itération sur les séances
        if (!empty($seanceIds) && $modelOdtInfos->hasSection('ListeSeances')) {
            $this->Seance->setVariablesFusionSeances($aData, $modelOdtInfos, $seanceIds, false, 'ListeSeances');
        }

        //avis du projet
        if (!empty($fusionSeanceId) && $modelOdtInfos->hasUserFieldsDeclared(
            'ProjetAvis_type',
            'ProjetAvis_date',
            'ProjetAvis_avis',
            'ProjetAvis_favorable',
            'ProjetAvis_commentaire'
        )) {
            $this->DeliberationSeance->setVariablesFusionAvis($aData, $modelOdtInfos, $id, $fusionSeanceId);
        }

        // avis des séances
        if ($modelOdtInfos->hasUserFieldsDeclared(
            'avis',
            'AvisProjet_seance_type_libelle',
            'AvisProjet_seance_type_action',
            'AvisProjet_seance_date',
            'avis_favorable',
            'commentaire'
        )) {
            $this->DeliberationSeance->setVariablesFusionPourUnProjet(
                $aData,
                $modelOdtInfos,
                $id,
                $fusionSeanceId
            );
        }

        // listes des présents et suppléants, absents, mandatés
        $this->Listepresence->setVariablesFusionPresents($aData, $modelOdtInfos, $delib['Deliberation']['id']);
        $this->Listepresence->setVariablesFusionAbsents($aData, $modelOdtInfos, $delib['Deliberation']['id']);
        $this->Listepresence->setVariablesFusionMandates($aData, $modelOdtInfos, $delib['Deliberation']['id']);

        // votes
        $this->Vote->setVariablesFusion($aData, $modelOdtInfos, $delib['Deliberation']['id']);

        // Type de vote pour la délibération
        // Si la deliberation a été voté
        if ($this->isVoted($delib['Deliberation']['etat']) && $modelOdtInfos->hasUserFieldDeclared('vote_type')) {
            $aData['vote_type'] = ['value' => $this->getVoteMethod($delib['Deliberation']['id']), 'type' => 'text'];
        }

        // président de séance de la délibération
        if (!empty($delib['Deliberation']['president_id'])) {
            $this->President->setVariablesFusion($aData, $modelOdtInfos, $delib['Deliberation']['president_id']);
        }
    }

    private function setVariablesFusionTextes(&$modelOdtInfos, $projet, &$aData)
    {
        //champs->variables modèle
        $fileodts = [
            ['champ' => 'texte_projet', 'variable' => 'texte_projet'],
            ['champ' => 'deliberation', 'variable' => 'texte_deliberation'],
            ['champ' => 'deliberation', 'variable' => 'texte_acte'],
            ['champ' => 'texte_synthese', 'variable' => 'note_synthese'],
            ['champ' => 'debat', 'variable' => 'debat_deliberation'],
            ['champ' => 'commission', 'variable' => 'debat_commission'],
        ];

        foreach ($fileodts as $fileodt) {
            if ($modelOdtInfos->hasUserField($fileodt['variable'])) {
                if (!empty($projet['Deliberation'][$fileodt['champ']])
                    || !empty($projet['Deliberation']['parent_id'])
                ) {
                    //Projet multi alors on doit mettre le texte du projet et texte synthèse du père
                    if (!empty($projet['Deliberation']['parent_id'])
                        && in_array($fileodt['champ'], ['texte_projet', 'texte_synthese'], true)
                    ) {
                        $texteProjet = $this->find('first', [
                            'fields' => [$fileodt['champ']],
                            'conditions' => ['id' => $projet['Deliberation']['parent_id']],
                            'recursive' => -1,
                        ]);
                        if (!empty($texteProjet['Deliberation'][$fileodt['champ']])) {
                            $aData[$fileodt['variable']] = [
                                'value' => $texteProjet['Deliberation'][$fileodt['champ']],
                                'type' => 'file'
                            ];
                        } else {
                            $aData[$fileodt['variable']] = [
                                'value' => file_get_contents(APP . DS . 'Config' . DS . 'OdtVide.odt'),
                                'type' => 'file'
                            ];
                        }
                    } else {
                        $aData[$fileodt['variable']] = [
                            'value' => $projet['Deliberation'][$fileodt['champ']],
                            'type' => 'file'
                        ];
                    }
                } else {
                    $aData[$fileodt['variable']] = [
                        'value' => file_get_contents(APP . DS . 'Config' . DS . 'OdtVide.odt'),
                        'type' => 'file'
                    ];
                }
            }
        }
    }

    /**
     * @param $etat State of the "Deliberation" object
     * @return bool Return TRUE if is voted FALSE otherwise
     */
    public function isVoted($etat)
    {
        return in_array($etat, [3, 4, 5, 6, 7, 8, 9, 10, 11], true) ? true : false;
    }

    /**
     * Determination du type de vote pour une délibération.
     * Si la délibération est renseignée dans la table votes => Détails des votes
     * Si la délibération !est renseignée dans la table votes
     * && tous les champs de type vote_bn_xxx sont a zéro => Total des voix
     * Si vote_prendre_acte est == true => Prendre acte
     * Sinon vote par Résultat.
     * @param $id identifiant de la délibération
     * @return string code du type de vote
     * @version 5.1
     */
    public function getVoteMethod($id)
    {
        $this->virtualFields['total_detail_vote'] =
            'SUM( vote_nb_oui + vote_nb_non + vote_nb_abstention +  vote_nb_retrait) ';
        $deliberation = $this->find(
            'first',
            [
                'conditions' => ['id' => $id],
                'fields' => [
                    'id',
                    'vote_prendre_acte',
                    'vote_nb_oui',
                    'vote_nb_non',
                    'vote_nb_abstention',
                    'vote_nb_retrait',
                    'total_detail_vote'
                ],
                'group' => [
                    'Deliberation.id', 'vote_prendre_acte', 'vote_nb_oui',
                    'vote_nb_non', 'vote_nb_abstention', 'vote_nb_retrait'
                ],
                'recursive' => -1,
            ]
        );
        unset($this->virtualFields['total_detail_vote']);
//        Prendre acte
        if ($deliberation['Deliberation']['vote_prendre_acte'] !== null) {
            return __('PRENDRE_ACTE');
        }
//        Détail des voix
        if ($this->Vote->find(
            'first',
            [
                'fields' => ['Vote.id', 'Vote.delib_id'],
                'conditions' => ['delib_id' => $id]
            ]
        )
        ) {
            return __('DETAIL');
        }
//        Total des voix
        if (!$this->Vote->find('first', [
            'fields' => ['Vote.id', 'Vote.delib_id'],
                'conditions' => ['delib_id' => $id]])
            && $deliberation['Deliberation']['total_detail_vote'] > 0) {
            return __('TOTAL');
        }
//        Résultat
        return __('RESULTAT');
    }

    /**
     * Construit le tableau à envoyer au parapheur
     *
     * @access public
     * @param $acte_id
     * @return array
     */
    public function getDocumentsForDelegation($acte_id)
    {
        $docs = [
            'docPrincipale' => $this->getDocument($acte_id),
            'annexes' => []
        ];
        foreach ($this->Annexe->getAnnexesWithoutFusion($acte_id) as $annexe) {
            $docs['annexes'][] = [
                'content' => $annexe['Annexe']['data'],
                'mimetype' => $annexe['Annexe']['filetype'],
                'filename' => $annexe['Annexe']['filename']
            ];
        }
        return $docs;
    }

    /**
     * @access public
     * @param type $acte_id
     * @param type $format
     * @return type
     */
    public function getDocument($acte_id, $format = 'pdf')
    {
        // fusion du document
        return $this->fusion($acte_id, null, null, $format);
    }

    /**
     * @access public
     * @param type $acte_id
     * @return type
     */
    public function getAnnexesToSend($acte_id)
    {
        $annexes = [];
        $to_send = $this->Annexe->getAnnexesFromDelibId($acte_id, true);
        $docs_type = Configure::read('DOC_TYPE');
        foreach ($to_send as $annexe) {
            if ($docs_type[$annexe['Annexe']['filetype']]['convertir']) {
                if (!empty($annexe['Annexe']['data_pdf'])) {
                    $annexes[] = [
                        'content' => $annexe['Annexe']['data_pdf'],
                        'mimetype' => 'application/pdf',
                        'filename' => AppTools::getNameFile($annexe['Annexe']['filename']) . '.pdf'
                    ];
                }
            } else {
                $annexes[] = [
                    'content' => $annexe['Annexe']['data'],
                    'mimetype' => $annexe['Annexe']['filetype'],
                    'filename' => $annexe['Annexe']['filename']
                ];
            }
        }
        return $annexes;
    }

    /**
     * Fonction d'initialisation des variables de fusion pour un ou plusieurs projets ou délibérations
     *
     * @access public
     * @param type $aData
     * @param object $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param integer $ids --> liste des id des délibérations
     */
    public function setVariablesFusionDeliberations(&$aData, &$modelOdtInfos, $ids)
    {
        // pour tous les projets/délibérations
        foreach ($ids as $id) {
            $this->setVariablesFusion($aData['Projets'][], $modelOdtInfos, $id);
        }
    }

    /**
     * @param int $acte_id
     * @param boolean $signee
     * @return array
     * @throws Exception
     */
    private function acteSignature($acte_id, $signee = false, $dateSignature = null)
    {

        $acte = $this->find('first', [
            'contain' => [
                'Typeacte' => ['fields' => ['compteur_id', 'nature_id', 'teletransmettre']],
            ],
            'conditions' => ['Deliberation.id' => $acte_id],
            'recursive' => -1
        ]);
        $nature = ClassRegistry::init('Nature');
        $acte['acte_nature_code'] = $nature->getNatureCodeByTypeActe($acte['Deliberation']['typeacte_id']);

        if (empty($acte['Deliberation']['num_delib'])) {
            $compteur = ClassRegistry::init('Compteur');
            $acte['Deliberation']['etat'] = 3;
            $acte['Deliberation']['vote_resultat'] = true;
            $acte['Deliberation']['num_delib'] =  $compteur->genereCompteur(
                $acte['Typeacte']['compteur_id']
            );
        }
        // Si la date n'est pas renseignée prendre la date du jour
        if (empty($dateSignature)) {
            $dateSignature = date("Y-m-d H:i:s", strtotime("now"));
        }
        if ($this->isDeliberant($acte_id)) { //Délibération
            $this->Seance->id = $this->getSeanceDeliberanteId($acte_id);
            $this->Seance->recursive = -1;
            $acte['Deliberation']['date_acte'] = $this->Seance->field('date');
            if ($signee) {
                $acte['Deliberation']['signature_date'] = $dateSignature;
            }
            if ($acte['Deliberation']['vote_resultat'] === true) {
                $acte['Deliberation']['etat'] = 3;
            } else {
                $acte['Deliberation']['etat'] = 4;
            }
        } else { //Autres actes
            // signature manuscrite
            if ($signee) {
                $acte['Deliberation']['signature_date'] = $dateSignature;
                $acte['Deliberation']['date_acte'] = $dateSignature;
            }

            if ($acte['Deliberation']['vote_resultat'] === true) {
                $acte['Deliberation']['etat'] = 3;
            } else {
                $acte['Deliberation']['etat'] = -2;
            }
        }
        CakeLog::info(
            __(
                '[Signature][%s]Signature de l\'acte n°%s en date : %s (Délibérant: %s)',
                __METHOD__,
                $acte_id,
                $acte['Deliberation']['date_acte'],
                var_export($this->isDeliberant($acte_id), true)
            ),
            'connector'
        );


        $acte['Deliberation']['date_envoi_signature'] =
            date("Y-m-d H:i:s", strtotime("now"));

        if ($signee) {
            $acte['Deliberation']['signee'] = true;
        }

        $this->save($acte);

        return $acte;
    }

    public function versioningEnable()
    {
        $this->Behaviors->load('Version', $this->actsAsVersionOptionsList['Version']);
    }

    /**
     * Signature manuscrite, génère le document delib_pdf avec le numéro de delib si c'est un arrêté
     * et enregistre dans l'historique de l'acte
     *
     * @access public
     * @param int $acte_id
     * @param int $user_id
     * @param string $dateSignature
     * @return boolean
     *
     * @version 5.1.4
     * @since 4.3.0
     */
    public function signatureManuscrite($acte_id, $user_id, $dateSignature)
    {
        $this->versioningEnable();

        $acte = $this->acteSignature($acte_id, true, $dateSignature);

        $this->begin();
        $success = true;
        // Effacement des traces d'un rejet parapheur
        $acte['Deliberation']['parapheur_etat'] = null;
        $acte['Deliberation']['pastell_id'] = null;
        try {
            // Document delib_pdf
            $acte['Deliberation']['delib_pdf'] = $this->getDocument($acte_id);
        } catch (Exception $e) {
            CakeLog::error($e->getMessage());
            $success = false;
        }

        // On stoque le fichier en base de données
        if ($success && !empty($acte['Deliberation']['delib_pdf']) && $this->save($acte)) {
            $this->commit();
            $success = true;
        } else {
            $this->rollback();
        }

        if ($success) {
            $this->Historique->enregistre($acte_id, $user_id, __("Signature manuscrite"));
            return true;
        } else {
            return false;
        }
    }

    /**
     * Signe l'acte
     * Génère le document delib_pdf
     * Génère un numéro de delib si c'est un arrêté
     * Enregistre dans l'historique de l'acte
     *
     * @access public
     * @param int $acte_id
     * @param int $user_id
     * @return boolean
     */
    public function genereNumeroActe($acte_id, $user_id, $nowait = false)
    {
        $this->versioningEnable();

        $success = true;
        try {
            $this->begin();
            $db = $this->getDataSource();
            if ($nowait) {
                $lock = $this->query(sprintf(
                    'LOCK TABLE %s IN ACCESS EXCLUSIVE MODE NOWAIT',
                    $db->fullTableName($this->Seance->Typeseance->Compteur->Sequence->useTable)
                )) !== false;
            }

            $acte = $this->find('first', [
                'fields' => [],
                'contain' => ['Typeacte.compteur_id', 'Typeacte.nature_id'],
                'conditions' => ['Deliberation.id' => $acte_id],
                'recursive' => -1
            ]);

            if (!empty($acte) && empty($acte['Deliberation']['num_delib'])) {
                $acte['Deliberation']['etat'] = 3;
                $acte['Deliberation']['vote_resultat'] = true;
                $acte['Deliberation']['num_delib'] =
                    $this->Seance->Typeseance->Compteur->genereCompteur($acte['Typeacte']['compteur_id']);
            } else {
                $success = false;
            }

            $success = $success && $this->save($acte);

            if ($success) {
                $this->commit();
                $this->Historique->enregistre(
                    $acte_id,
                    $user_id,
                    __("Acte adopté (génération du numéro d'acte)")
                );
            }

            return $success;
        } catch (Exception $e) {
            $this->rollback();
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Fonction d'envoi au parapheur
     * Génère le document delib_pdf
     * Génère un numéro de delib si c'est un arrêté
     * Enregistre dans l'historique de l'acte
     *
     * @access public
     * @param int $acte_id --> identifiant de l'acte
     * @param int $circuit_id --> circuit identifiant du circuit parapheur
     * @param int $user_id --> identifiant de l'utilisateur (pour historique)
     * @return boolean success
     * @version 5.1.2
     * @since 4.3.0
     */
    public function envoyerAuParapheur($acte_id, $circuit_id, $user_id)
    {
        $this->Behaviors->load('Version', $this->actsAsVersionOptionsList['Version']);

        $acte = $this->acteSignature($acte_id);

        $this->begin();

        $success = true;
        $documentPrincipal = $this->getDocument($acte_id);
        if (!empty($documentPrincipal)) { // On stoque le fichier en base de données
            App::uses('Signature', 'Lib');
            try {
                $this->Signature = new Signature();
                if (Configure::read('PARAPHEUR') == 'PASTELL') {
                    $nature = ClassRegistry::init('Nature');
                    $acte['acte_nature_code'] = $nature->getNatureCodeByTypeActe($acte['Deliberation']['typeacte_id']);
                    // Fix defaut pastell typologiepiece
                    if (empty($acte['Deliberation']['typologiepiece_code'])) {
                        $acte['Deliberation']['typologiepiece_code'] =
                            $nature->getNatureDefaultCodeByTypeActe($acte['Deliberation']['typeacte_id']);
                    }
                    // Fix defaut pastell classification
                    if (empty($acte['Deliberation']['num_pref'])) {
                        $acte['Deliberation']['num_pref'] = '1.1';
                    }
                }
                $reponse = $this->Signature->send(
                    $acte,
                    $circuit_id,
                    $documentPrincipal,
                    $this->getAnnexesToSend($acte_id)
                );
                $success &= !empty($reponse);
                //Historique
                $success &= $this->Historique->enregistre($acte_id, $user_id, __("Envoi du projet au parapheur"));
                $acte['Deliberation']['parapheur_id'] = $reponse;
                if (Configure::read('PARAPHEUR') == 'PASTELL') {
                    $acte['Deliberation']['pastell_id'] = $reponse;
                }
                $acte['Deliberation']['parapheur_cible'] = Configure::read('PARAPHEUR');
                $acte['Deliberation']['parapheur_etat'] = 1;
            } catch (Exception $e) {
                CakeLog::error($e->getMessage(), 'connector');
                $success = false;
            }
        } else {
            $success = false;
        }
        if ($success && $this->save($acte)) {
            $this->commit();
            return true;
        } else {
            $this->rollback();
            return false;
        }
    }

    /**
     * Ordonne la fusion et retourne le résultat sous forme de flux
     *
     * @access public
     * @param int|string $id --> identifiant de la séance
     * @param string $modeltype --> type de fusion
     * @param int|string $modelTemplateId
     * @param string $format --> format du fichier de sortie
     * @return string --> flux du fichier généré
     */
    public function fusion($id, $modeltype = null, $modelTemplateId = null, $format = 'pdf')
    {
        $this->Behaviors->load('OdtFusion', [
            'id' => $id,
            'fileNameSuffixe' => $id,
            'modelTemplateId' => $modelTemplateId,
            'modelOptions' => ['modelTypeName' => $modeltype]
        ]);
        $this->odtFusion();
        $content = $this->getOdtFusionResult($format);
        $this->deleteOdtFusionResult();
        $this->Behaviors->unload('OdtFusion');
        return $content;
    }

    /**
     * Ordonne la fusion et retourne le résultat sous forme de flux
     *
     * @access public
     * @param int|string $id --> identifiant de la séance
     * @param string $modeltype --> type de fusion
     * @param int|string $modelTemplateId
     * @param string $outputdir --> fichier vers lequel faire la fusion
     * @param string $format --> format du fichier de sortie
     * @param type $seance_id
     * @return array [filename => content]
     */
    public function fusionToFile(
        $id,
        $modeltype,
        $modelTemplateId = null,
        $outputdir = TMP,
        $format = 'pdf',
        $seance_id = null
    ) {
        $this->Behaviors->load('OdtFusion', [
            'id' => $id,
            'fileNameSuffixe' => $id,
            'modelTemplateId' => $modelTemplateId,
            'modelOptions' => ['modelTypeName' => $modeltype, 'seance_id' => $seance_id]
        ]);
        $this->odtFusion();
        $content = $this->getOdtFusionResult($format);
        $this->deleteOdtFusionResult();
        $filename = $this->fusionName();
        $file = new File($outputdir . DS . $filename . '.' . $format, true);
        $file->write($content);
        $this->Behaviors->unload('OdtFusion');
        return $file->path;
    }

    /**
     * Supression des votes d'un projet pour une séance
     *
     * @access public
     * @param int $projet_id --> variable Gedooo de type maintPart du document à fusionner
     * @param int $seance_id --> objet PhpOdtApi du fichier odt du modèle d'édition
     */
    public function resetVote($projet_id, $seance_id)
    {
        $this->Vote->deleteAll(['delib_id' => $projet_id], false);
        $this->Listepresence->deleteAll(['delib_id' => $projet_id], false);

        $this->id = $projet_id;
        $this->save([
            'num_delib' => '',
            'etat' => 2,
            'vote_prendre_acte' => null,
            'vote_nb_oui' => null,
            'vote_nb_non' => null,
            'vote_nb_abstention' => null,
            'vote_nb_retrait' => null,
            'vote_commentaire' => null,

        ], false);
    }

    /**
     * Duplique un projet en rinitialisant certain champs, pour que la délibération
     * soit comme nouvellement créé
     *
     * @param int $id --> id du projet à copier
     * @return int $id --> id du nouveau projet créé
     * @version 5.1
     * @since 4.3
     * @access public
     */
    public function duplicate($id, $user_id, $service_id)
    {
        //FIX duplication des champs valides
        if (!empty($id)) {
            $delib = $this->find('first', [
                'conditions'=> [
                    'Deliberation.id' => $id
                ],
                'recursive' => -1
            ]);
            $this->create();
            $new_delib = [];
            $new_delib['Deliberation']['redacteur_id'] = $user_id;
            $new_delib['Deliberation']['service_id'] = $service_id;
            $new_delib['Deliberation']['typeacte_id'] = $delib['Deliberation']['typeacte_id'];

            $this->Theme->recursive = -1;
            $theme = $this->Theme->findById($delib['Deliberation']['theme_id'], 'actif');
            if (!empty($theme['Theme'])) {
                $new_delib['Deliberation']['theme_id'] = $delib['Deliberation']['theme_id'];
            } else {
                $new_delib['Deliberation']['theme_id'] = 0;
            }

            // For multidélib
            $new_delib['Deliberation']['objet'] = $delib['Deliberation']['objet'];
            $new_delib['Deliberation']['objet_delib'] = $delib['Deliberation']['objet_delib'];
            $new_delib['Deliberation']['titre'] = $delib['Deliberation']['titre'];

            foreach (['texte_projet', 'deliberation', 'texte_synthese'] as $key => $value) {
                $new_delib['Deliberation'][$value] = $delib['Deliberation'][$value];
                $new_delib['Deliberation'][$value . '_name'] = $delib['Deliberation'][$value . '_name'];
                $new_delib['Deliberation'][$value . '_type'] = $delib['Deliberation'][$value . '_type'];
                $new_delib['Deliberation'][$value . '_size'] = $delib['Deliberation'][$value . '_size'];
            }
            foreach (['texte_projet', 'texte_acte', 'texte_synthese'] as $key => $value) {
                $new_delib['Deliberation'][$value . '_active'] = $delib['Deliberation'][$value . '_active'];
            }

            if ($this->save($new_delib) != false) {
                //FIX AfterSave
                //Création des informations supplémenataires initiales
                $new_delib['Deliberation']['id'] = $this->getLastInsertId();
                $new_delib['Infosup'] = $this->Infosup->Infosupdef->valeursInitiales('Deliberation');
                $this->Infosup->saveInfosup($new_delib, 'Deliberation', true);

                return $this->id;
            }
        }

        return false;
    }

    /**
     * Enregistre l'envoi d'un acte au TdT
     *
     * @param type $id --> id du projet à copier
     * @return type $id --> id du nouveau projet créé
     * @version 4.4
     * @access public
     */
    public function saveSendTdt($id, $tdt_id, $user_id = null)
    {
        $this->Behaviors->load('Version', $this->actsAsVersionOptionsList['Version']);

        $this->id = $id;
        $this->save(['etat' => 5, 'tdt_id' => $tdt_id]);
        $user_id = AuthComponent::user('id');
        $this->setHistorique(
            __('Acte envoyé au tiers de télétransmission'),
            $id,
            null,
            (!empty($user_id) ? $user_id : 0)
        );
    }

    /**
     * @param array $data
     * @return type
     * @version 4.3
     * @access public
     */
    public function delegationCakeflow($targetId, $data)
    {
        $visa = $data['Visa'];
        $etape = $data['Etape'];
        $composition = $data['Etape']['Composition'][0];

        if (!empty($composition['type_composition']) && $composition['type_composition'] == 'PARAPHEUR') {
            $libelleSousType = $this->Circuit->Etape->Composition->libelleSousType($composition['soustype']);
            return $this->delegToParapheur($targetId, $libelleSousType); //ENVOI PARAPHEUR
        } elseif (!empty($composition['type_composition']) && $composition['type_composition'] == 'PASTELL') {
            $libelleSousType = $this->Circuit->Etape->Composition->libelleSousType($composition['soustype']);
            return $this->delegToPastell($targetId, $libelleSousType); //ENVOI PARAPHEUR
        } elseif (!empty($composition['type_composition']) && $composition['type_composition'] == 'CONNECTOR') {
            return $this->delegToConnectorAdd($targetId, $composition['soustype']);
        }
    }

    /**
     * envoi le dossier au parapheur pour finir son circuit si celui-ci comporte une étape de délégation
     * @param integer $targetId numéro de la délibération concernée
     * @param String $libelleSousType libelle du sous-type
     * @return boolean code de retour, true: éxecution déroulée avec succès, false: si erreur
     */
    private function delegToParapheur($id, $libelleSousType)
    {
        $projet = $this->find('first', [
            'contain' => ['Typeacte.nature_id'],
            'conditions' => ['Deliberation.id' => $id],
            'recursive' => 1
        ]);

        $this->id = $id;

        $aDelegToParapheurDocuments = $this->getDocumentsForDelegation($id);

        App::uses('Signature', 'Lib');
        $Signature = new Signature();
        $ret = $Signature->send(
            $projet,
            $libelleSousType,
            $aDelegToParapheurDocuments['docPrincipale'],
            $aDelegToParapheurDocuments['annexes']
        );
        unset($aDelegToParapheurDocuments);
        if ($ret !== false) {
            $this->saveField('parapheur_etat', 1);
            $this->saveField('parapheur_id', $ret);
            $this->saveField('parapheur_cible', Configure::read('PARAPHEUR'));
            return true;
        } else {
            $this->log(__('Echec de l\'envoi en délégation du dossier n°%s', $id), 'parapheur');
            return false;
        }
    }

    /**
     * envoi le dossier au parapheur pour finir son circuit si celui-ci comporte une étape de délégation
     * @param integer $targetId numéro de la délibération concernée
     * @param String $libelleSousType libelle du sous-type
     * @return boolean code de retour, true: éxecution déroulée avec succès, false: si erreur
     */
    private function delegToPastell($id, $libelleSousType)
    {
        $projet = $this->find('first', [
            'contain' => ['Typeacte.nature_id'],
            'conditions' => ['Deliberation.id' => $id],
            'recursive' => 1
        ]);

        $this->id = $id;

        $aDelegToParapheurDocuments = $this->getDocumentsForDelegation($id);
        App::uses('Signature', 'Lib');
        $Signature = new Signature();
        $pastellId = $Signature->sendVisa(
            $projet,
            $libelleSousType,
            $aDelegToParapheurDocuments['docPrincipale'],
            $aDelegToParapheurDocuments['annexes']
        );
        unset($aDelegToParapheurDocuments);

        if ($pastellId !== false) {
            $this->recursive= -1;
            $this->save([
                'parapheur_etat' => 1,
                'parapheur_cible' => Configure::read('PARAPHEUR'),
                'pastell_id' => $pastellId
            ]);

            return true;
        } else {
            $this->log(__('Echec de l\'envoi en délégation du dossier n°%s', $id), 'parapheur');
            return false;
        }
    }

    /**
     * @param array $data
     * @return type
     * @version 4.4.0
     * @access public
     */
    private function delegToConnectorAdd($targetId, $connector_id)
    {
        $projet = $this->find('first', [
            'fields' => ['objet'],
            'conditions' => ['id' => $targetId],
            'recursive' => -1
        ]);

        $delegDocumentsToWorkflow = $this->getDocumentsForDelegation($targetId);
        $json = json_encode([
            'objet' => $projet['Deliberation']['objet'],
            'docPrincipale' => [
                'type' => 'binaries',
                'typemime' => 'application/pdf',
                'content' => $delegDocumentsToWorkflow['docPrincipale']
            ],
            'annexes' => $delegDocumentsToWorkflow['annexes']
        ]);
        unset($delegDocumentsToWorkflow);

        App::uses('ConnectorManager.Connector', 'Model');
        $connectorJob = ClassRegistry::init('ConnectorManager.ConnectorJob');
        $connectorJob->create();
        $ret = $connectorJob->save(['ConnectorJob' => [
            'connector_id' => $connector_id,
            'model' => $this->alias,
            'foreign_key' => $targetId,
            'args' => $json
        ]]);

        $cmd = 'nohup nice -n 10 -- ' . APP . 'Console' . DS
            . 'cake Cron majCakeflowDelegation'
            . ' -t ' . Configure::read('Config.tenantName')
            . ' >/dev/null 2>&1  & echo $!';
        $pid = shell_exec($cmd);

        if ($ret !== false) {
            return true;
        } else {
            $this->log("Echec de l'envoi en délégation du dossier " . $targetId, 'parapheur');

            return false;
        }
    }

    /**
     * @param array $data
     * @return type
     * @version 4.4
     * @access public
     */
    public function delegationlinkCakeflow($type_composition, $soustype)
    {
        $delegation = '';
        switch ($type_composition) {
            case 'PARAPHEUR':
                $composition = ClassRegistry::init('Cakeflow.Composition');
                $delegation = __('[iparapheur]')
                    . ' '
                    . " ("
                    . Configure::read('IPARAPHEUR_TYPE')
                    . " / " . $composition->libelleSousType($soustype)
                    . ")";
                break;
            case 'PASTELL':
                $composition = ClassRegistry::init('Cakeflow.Composition');
                $delegation = __('[pastell]') . ' ' . " (iparapheur / "
                    . $composition->libelleSousType($soustype) . ")";
                break;
            case 'CONNECTOR':
                App::uses('Connector', 'Model');
                $connector = ClassRegistry::init('Connector');
                $connector->recursive = -1;
                $connector_name = $connector->findById($soustype, 'name');
                $delegation = '[' . $connector_name['Connector']['name'] . ']';
                break;
        }

        return __('Connecteur %s', $delegation);
    }

    /**
     * Calcule la date avant retard selon la séance délibérante moins le nombre de jours avant retard
     * @param $cptRetard nombre de jours avant retard
     * @param $targetId identifiant du projet
     * @return string date avant retard ou null si aucune séance délibérante rattachée
     */
    public function computeDateRetard($cptRetard, $targetId)
    {
        $seance_id = $this->getSeanceDeliberanteId($targetId);
        if (!empty($seance_id)) {
            $seance = $this->Seance->find('first', [
                'conditions' => ['Seance.id' => $seance_id],
                'fields' => ['Seance.date'],
                'recursive' => -1
            ]);
            if (!empty($seance['Seance']['date'])) {
                if (empty($cptRetard)) {
                    return $seance['Seance']['date'];
                } else {
                    return date(
                        'Y-m-d H:i:s',
                        strtotime($seance['Seance']['date'] . " - $cptRetard days")
                    );
                }
            }
        }
        //Si pas de séance délibérante : ne rien renvoyer
        //Pourrait renvoyer date d'aujourd'hui + compteur retard
        return null;
    }

    /**
     * @param $message
     * @param $texte
     * @param $texteactif
     * @param $messagetexte
     * @return string
     */
    private function elaboreMessage($message, $texte, $texteactif, $messagetexte)
    {
        if (empty($texte) && $texteactif) {
            if (!empty($message)) {
                $message = $message . "\n";
            }
            $message = $message . $messagetexte;
        }
        return $message;
    }


    /**
     * Vérifie la présence des textes dans le projet
     * @param int $id
     * @return string $message
     * @version 5.0.1
     * @access public
     */
    public function checkPresenceTextes($id)
    {
        $projet = $this->find('first', [
            'fields' => [
                'texte_projet',
                'texte_synthese',
                'deliberation',
            ],
            'contain' => ['Typeacte' => [
                'fields' => [
                    'Typeacte.gabarit_projet_active',
                    'Typeacte.gabarit_synthese_active',
                    'Typeacte.gabarit_acte_active']
            ]
            ],
            'conditions' => ['Deliberation.id' => $id],
            'recursive' => -1
        ]);
        $message = "";

        $aTester = [
            [
                "texte" => $projet['Deliberation']['texte_projet'],
                "texteactif" => $projet['Typeacte']['gabarit_projet_active'],
                "messagetexte" => "Le texte projet est vide"
            ],
            [
                "texte" => $projet['Deliberation']['texte_synthese'],
                "texteactif" => $projet['Typeacte']['gabarit_synthese_active'],
                "messagetexte" => "La note de synthèse est vide"
            ],
            [
                "texte" => $projet['Deliberation']['deliberation'],
                "texteactif" => $projet['Typeacte']['gabarit_acte_active'],
                "messagetexte" => "Le texte acte est vide"
            ]
        ];

        foreach ($aTester as $test) {
            $message = $this->elaboreMessage($message, $test["texte"], $test["texteactif"], $test["messagetexte"]);
        }

        return $message;
    }

    /**
     * Cancel a signature
     * Put the field 'etat' to 10
     * Put the field 'signee' to false
     * Put the field signature to null
     * Put the field parapheur_etat to 0
     * @return bool True if the signature is canceled, FALSE otherwise
     * @throws Exception
     */
    public function cancelSignature()
    {
        $this->Behaviors->load('Version', $this->actsAsVersionOptionsList['Version']);

        $data = [
            'etat' => 10,
            'signee' => false,
            'signature' => null,
            'date_envoi_signature' => null,
            'date_acte' => null,
            'parapheur_etat' => 0,
            'pastell_id' => null,
            'parapheur_bordereau' => null,
            'delib_pdf' => null,
        ];
        if ($this->save($data)) {
            $this->Historique->enregistre(
                $this->field('id'),
                AuthComponent::user('id'),
                __("Signature annulée.")
            );
            return true;
        }
        return false;
    }

    /**
     * Return TRUE if cancelable FALSE otherwise
     * @return bool
     */
    public function isCancelableForTdt()
    {
        return in_array($this->field('tdt_status'), [-1, -2, -3], true);
    }

    /**
     * Cancel TDT
     * Put the field etat to 3
     * Put the field signee to false if specified
     * Put the field tdt_data_pdf to null
     * Put the field tdt_bordereau_pdf to null
     * @param $deliberationNumber
     * @return array|bool Return array of errors if fail, TRUE otherwise
     * @throws Exception
     */
    public function resetSendTDT(array $data)
    {
        $this->Behaviors->load('Version', $this->actsAsVersionOptionsList['Version']);

        // Validation rules for slow v3.0
        if (isset($data["Deliberation"]['num_delib']) && $this->isCancelableForTdt()) {
            $this->validator()->add('num_delib', [
                'required' => [
                    'rule' => 'notBlank',
                    'required' => 'update',
                    'message' => "Vous devez spécifier un numéro d'acte."
                ],
                'size' => [
                    'rule' => ['maxLength', 15],
                    'message' => 'Le numéro de délibération ne doit pas excéder 15 caractères.'
                ],
                'structure' => [
                    'rule' => ['custom', '/^[A-Z0-9_]+$/'],
                    'message' => 'Le nouveau numéro d\'acte doit contenir uniquement des majuscules, '
                        .'numéros et le caractère underscore.'
                ],
                'unique' => [
                    'rule' => 'isUnique',
                    'required' => true,
                ],
                'change' => [
                    'rule' => ['isNew'],
                    'message' => __("Le numéro d'acte doit être modifié.")
                ],
            ])
                ->add(
                    'tdt_status',
                    [
                        'custom' => [
                            'rule' => ['isCancelableForTdt'],
                            'message' => 'L\'envoi pour ce projet ne peut pas être annulé.'
                                . ' \n Assurez-vous d\'avoir annulé l\'envoi initial depuis le TDT.',
                            'on' => 'update',
                        ]
                    ]
                );
            $acte = $this->find('first', [
                'conditions' => ['Deliberation.id' => $this->field('id')],
                'contain' => ['Typeacte.compteur_id', 'Typeacte.nature_id'],
                'recursive' => -1
            ]);

            $etat = 3;
            if ($acte['Deliberation']['vote_resultat'] === false) {
                $etat = 4;
            }

            $newData = [
                'etat' => $etat,
                'num_delib' => $data["Deliberation"]['num_delib'],
                'tdt_id' => null,
                'tdt_status' => 0, //was canceled
                'tdt_ar' => null,
                'tdt_ar_date' => null,
                'tdt_data_bordereau_pdf' => null,
                'tdt_data_edition' => null,
                'tdt_data_pdf' => null,
                "tdt_dateAR" => null,
                'tdt_message' => null,
                'pastell_id' => null,
                'publier_etat' => 0,
                'publier_date' => null,
            ];

            $this->Annexe->recursive = -1;
            $annexes = $this->Annexe->findAllByForeignKey($this->field('id'));

            if (!empty($annexes)) {
                foreach ($annexes as $key => $id) {
                    $annexes[$key]['Annexe']['tdt_data_pdf'] = null;
                    $annexes[$key]['Annexe']['tdt_data_edition'] = null;
                    $this->Annexe->save($annexes[$key]['Annexe']);
                }
            }

            if ($this->save($newData)
                // Suppression des courriers ministerriel
                && $this->TdtMessage->deleteAll(['delib_id' => $this->field('id')])) {
                $this->validator()->remove('num_delib');
                $this->Historique->enregistre(
                    $this->field('id'),
                    AuthComponent::user('id'),
                    __("Annulation de l'envoi au TdT.")
                );
                //si enlever la signature est checked
                ($data["Deliberation"]['signee'] == 1)
                    ? $this->DeliberationVersion->clear() && $this->cancelSignature() : null;
                return true;
            }
        }
        return $this->validationErrors;
    }

    /**
     * Validation rules
     * @param $check
     * @return bool
     */
    public function isNew($check)
    {
        $value = array_values($check);
        $key = array_Keys($check);
        if ($value[0] === $this->field($key[0])) {
            return false;
        }
        return true;
    }

    /**
     * Return all ids matching with the TDT
     * @return array|boolean list of etat
     */
    public function isReadyForTDT($id = null)
    {
        $isReadyForTDT = [3, 4];

        if (isset($id)) {
            return in_array($this->field('etat'), $isReadyForTDT, true);
        }
        return $isReadyForTDT;
    }

    /**
     * Change the project state to 11 (send manually)
     * @param $id
     * @return bool Return TRUE if success FALSE otherwise.
     */
    public function sendManually()
    {
        $this->Behaviors->load('Version', $this->actsAsVersionOptionsList['Version']);

        $sendManually = [
            'etat' => 11,
            'tdt_ar_date' => date('Y-m-d H:i:s', time()),
            'tdt_ar' => 'Envoyé manuellement'];

        if ($this->save($sendManually)) {
            $this->Historique->enregistre(
                $this->field('id'),
                AuthComponent::user('id'),
                __("Déclaration de dépôt manuel sur le TDT.")
            );
            return true;
        }
        return false;
    }

    /**
     * Inform if the déliberation was sent mannualy
     * @param $id Id of the 'délibération'
     * @return bool Return TRUE if the 'Deliberation was sent manually FALSE otherwise.
     */
    public function isSendManually($id)
    {
        // Acccording to the documentation
        if ($this->field('etat') == 11) {
            return true;
        }
        return false;
    }

    public function isLocked($id, $action = 'edit')
    {
        return TokensMethodsComponent::locked(
            ['controller' => 'projets', 'action' => $action, 'entity_id' => $id]
        );
    }

    public function abandon($data)
    {
        $this->Behaviors->load('Version', $this->actsAsVersionOptionsList['Version']);

        if ($data['etat'] != 5) {//Si acte non transmis
            $data['etat'] = -3;

            //Commentaire refus
            $this->Commentaire->create();
            $com = [];
            $com['Commentaire']['delib_id'] = $data['id'];
            $com['Commentaire']['agent_id'] = AuthComponent::user('id');
            $com['Commentaire']['texte'] = __('Acte abandonné : ') . $data['Commentaire']['texte'];
            $com['Commentaire']['commentaire_auto'] = 0;
            if ($this->save($data) && $this->Commentaire->save($com)) {
                $this->Historique->enregistre(
                    $this->field('id'),
                    AuthComponent::user('id'),
                    __("Acte abandonné.")
                );
                $document['delib_pdf'] = $this->getDocument($data['id']);
                $this->save($document);
                return $data['id'];
            }
        }
        return false;
    }

    /**
     * @param $valuesOrder
     * @return bool
     * @version 5.1
     */
    public function logicalValuesForOrder($valuesOrder)
    {
        $values = array_values($valuesOrder);
        //si on s'implifie toutes les dates on les même tri
        // toutes les deliberations (hors date)n'ont pas de tri autre autorisé
        //FixME : Factoriser ! ! !
        $logicalValues = [
            'Deliberation.id asc' => [],
            'Deliberation.id desc' => [],
            'Deliberation.num_delib asc' => [],
            'Deliberation.num_delib desc' => [],
            'Deliberation.objet asc' => [],
            'Deliberation.objet desc' => [],
            'DeliberationSeance_Filter.position asc' => [],
            'DeliberationSeance_Filter.position desc' => [],
            'Theme.order asc' => ['DeliberationSeance_Filter.position asc' => [],
                'DeliberationSeance_Filter.position desc' => [],
                'Deliberation.objet asc' => [],
                'Deliberation.objet desc' => [],
                'Seance_Filter.date asc' => [],
                'Seance_Filter.date desc' => [],
                'Deliberation.date_acte asc' => [],
                'Deliberation.date_acte desc' => [],
                'Deliberation.tdt_ar_date asc' => [],
                'Deliberation.tdt_ar_date desc' => []
            ],
            'Theme.order desc' => [
                'DeliberationSeance_Filter.position asc' => [],
                'DeliberationSeance_Filter.position desc' => [],
                'Deliberation.objet asc' => [],
                'Deliberation.objet desc' => [],
                'Seance_Filter.date asc' => [],
                'Seance_Filter.date desc' => [],
                'Deliberation.date_acte asc' => [],
                'Deliberation.date_acte desc' => [],
                'Deliberation.tdt_ar_date asc' => [],
                'Deliberation.tdt_ar_date desc' => [],
            ],
            'Seance_Filter.date asc' => [
                'DeliberationSeance_Filter.position asc' => [],
                'DeliberationSeance_Filter.position desc' => [],
                'Deliberation.objet asc' => [],
                'Deliberation.objet desc' => [],
                'Theme.order asc' => [],
                'Theme.order desc' => [],
            ],
            'Seance_Filter.date desc' => [
                'DeliberationSeance_Filter.position asc' => [],
                'DeliberationSeance_Filter.position desc' => [],
                'Deliberation.objet asc' => [],
                'Deliberation.objet desc' => [],
                'Theme.order asc' => [],
                'Theme.order desc' => [],
            ],
            'Deliberation.date_acte asc' => [
                'DeliberationSeance_Filter.position asc' => [],
                'DeliberationSeance_Filter.position desc' => [],
                'Deliberation.objet asc' => [],
                'Deliberation.objet desc' => [],
                'Theme.order asc' => [],
                'Theme.order desc' => [],
            ],
            'Deliberation.date_acte desc' => [
                'DeliberationSeance_Filter.position asc' => [],
                'DeliberationSeance_Filter.position desc' => [],
                'Deliberation.objet asc' => [],
                'Deliberation.objet desc' => [],
                'Theme.order asc' => [],
                'Theme.order desc' => [],
            ],
            'Deliberation.tdt_ar_date asc' => [
                'DeliberationSeance_Filter.position asc' => [],
                'DeliberationSeance_Filter.position desc' => [],
                'Deliberation.objet asc' => [],
                'Deliberation.objet desc' => [],
                'Theme.order asc' => [],
                'Theme.order desc' => [],
            ],
            'Deliberation.tdt_ar_date desc' => [
                'DeliberationSeance_Filter.position asc' => [],
                'DeliberationSeance_Filter.position desc' => [],
                'Deliberation.objet asc' => [],
                'Deliberation.objet desc' => [],
                'Theme.order asc' => [],
                'Theme.order desc' => [],
            ],
        ];

        //C'est moche mais ça devrait faire le taff ! ! ! !
        switch (count($values)) {
            case 1:
                return true;
            case 2:
                return (isset($values[0], $logicalValues) && isset($values[1], $logicalValues[$values[0]]));
            case 3:
                return (isset($values[0], $logicalValues)
                    && isset($values[1], $logicalValues[$values[0]])
                    && isset($values[2], $logicalValues[$values[0]]));
        }
    }

    /**
     * [isParamTdtEmptyByActe description]
     * @param  [type]  $natureId        [description]
     * @param  [type]  $typologiePieces [description]
     * @return boolean                  [description]
     *
     * @version 5.1.1
     */
    public function isParamTdtEmptyByActe($acte)
    {
        $empty = false;
        if (empty($acte['Deliberation']['num_pref'])) {
            $empty = true;
        }
        if (!$empty && empty($acte['Deliberation']['typologiepiece_code'])) {
            $empty = true;
        }

        if (!$empty && !empty($acte['Annexe'])) {
            foreach ($acte['Annexe'] as $annexe) {
                if (empty($annexe['typologiepiece_code'])) {
                    $empty = true;
                    break;
                }
            }
        }

        return $empty;
    }

    /**
     * [fileOutputFormatValues description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function fileOutputFormatValues($id)
    {
        $projet = $this->find(
            'first',
            [
                'fields' => ['Deliberation.objet_delib'],
                'conditions' => ["Deliberation.id" => $id],
                'recursive' => -1
            ]
        );

        return [
            'projetName' => !empty($projet['Deliberation']['objet_delib'])
                ? $projet['Deliberation']['objet_delib'] : ''
        ];
    }
}
