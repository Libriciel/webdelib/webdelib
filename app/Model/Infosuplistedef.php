<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class Infosuplistedef extends AppModel
{
    public $displayField = 'nom';
    public $belongsTo = 'Infosupdef';
    public $hasMany = ['Infosup'];
    public $validate = ['nom' => [['rule' => 'notBlank', 'message' => 'Entrer un nom pour l\'élément']]];

    /**
     * Intervertit l'ordre de l'élément $id avec le suivant ou le précédent suivant $following*
     *
     * @access public
     * @param integer $id
     * @param boolean $following
     * @return type
     */
    public function invert($id = null, $following = true)
    {
        // Initialisations
        $gap = $following ? 1 : -1;

        // lecture de l'élément à déplacer
        $recFrom = $this->find(
            'first',
            [
                'recursive' => -1,
                'fields' => ['id', 'ordre', 'infosupdef_id'],
                'conditions' => ['id' => $id]
            ]
        );

        // lecture de l'élément a intervertir
        $recTo = $this->find(
            'first',
            [
                'recursive' => -1,
                'fields' => ['id', 'ordre'],
                'conditions' => [
                    'infosupdef_id' => $recFrom['Infosuplistedef']['infosupdef_id'],
                    'ordre' => $recFrom['Infosuplistedef']['ordre'] + $gap
                ]
            ]
        );

        // Si pas d'élément à intervertir alors on sort sans rien faire
        if (empty($recTo)) {
            return;
        }
        // Mise à jour du champ ordre pour les deux enregistrements
        $recFrom['Infosuplistedef']['ordre'] += $gap;
        $this->save($recFrom, false);
        $recTo['Infosuplistedef']['ordre'] -= $gap;
        $this->save($recTo, false);

        return;
    }

    /**
     * @version 4.3
     * @access public
     * @return boolean
     */
    public function beforeSave($options = [])
    {
        // calcul du n° d'ordre en cas d'ajout
        if (!array_key_exists('id', $this->data['Infosuplistedef'])
            || empty($this->data['Infosuplistedef']['id'])
        ) {
            $this->data['Infosuplistedef']['ordre'] = $this->find(
                'count',
                [
                    'recurtsive' => -1,
                    'conditions' => ['infosupdef_id' => $this->data['Infosuplistedef']['infosupdef_id']]
                ]
            ) + 1;
        }
        return true;
    }

    /**
     * Réordonne les numéros d'ordre après une suppression pour l'infosupdef $infosupdefId
     *
     * @access public
     * @param type $infosupdefId
     */
    public function reOrdonne($infosupdefId)
    {
        $recs = $this->find(
            'all',
            [
                'recursive' => -1,
                'fields' => ['id', 'ordre'],
                'conditions' => ['infosupdef_id' => $infosupdefId],
                'order' => ['ordre']
            ]
        );

        foreach ($recs as $n => $rec) {
            if (($n + 1) != $rec['Infosuplistedef']['ordre']) {
                $rec['Infosuplistedef']['ordre'] = ($n + 1);
                $this->save($rec, false);
            }
        }
    }

    /**
     * Suppression de tous les éléments de l'infosupdef $infosupdefId
     *
     * @access public
     * @param type $infosupdefId
     */
    public function delList($infosupdefId)
    {
        $recs = $this->find(
            'all',
            ['conditions' => ['infosupdef_id' => $infosupdefId], 'fields' => ['id'], 'recursive' => -1]
        );
        foreach ($recs as $rec) {
            $this->delete($rec['Infosuplistedef']['id']);
        }
    }

    /**
     * Détermine si une occurence peut être supprimée
     *
     * @access public
     * @param integer $id --> id de l'occurence à supprimer
     * @return boolean --> true si l'instance peut être supprimée et false dans le cas contraire
     */
    public function isDeletable($id)
    {
        // lecture de l'occurence en base de données
        $data = $this->find(
            'first',
            ['recursive' => -1, 'fields' => ['id', 'infosupdef_id'], 'conditions' => ['id' => $id]]
        );
        // si utilisée alors on ne peut pas la supprimer
        return !$this->Infosup->hasAny(
            [
                'infosupdef_id' => $data['Infosuplistedef']['infosupdef_id'],
                'text' => $data['Infosuplistedef']['id']
            ]
        );
    }

    /**
     * Retourne le libellé correspondant au booléen $actif
     *
     * @access public
     * @param type $actif
     * @return type
     */
    public function libelleActif($actif)
    {
        return $actif ? 'Oui' : 'Non';
    }
}
