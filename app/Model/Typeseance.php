<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Seance', 'Model');

/**
 * Type de Séance
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       webdelib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 * @version     v4.3
 * @package     app.Model
 */
class Typeseance extends AppModel
{
    public $name = 'Typeseance';
    public $displayField = 'libelle';
    public $validate = [
        'libelle' => [
            [
                'rule' => 'notBlank',
                'message' => 'Entrer le libellé.'
            ],
            [
                'rule' => 'isUnique',
                'message' => 'Entrez un autre libellé, celui-ci est déjà utilisé.'
            ],
            [
                'rule' => '/^[a-zA-Z0-9\xC0-\xD6\xD8-\xF6\xF8-\xFF-_ \']{5,}$/iu',
                'message' => 'Seulement les lettres, les entiers, les espaces et les caractères spéciaux (-_) '
                    .'sont autorisés dans le libellé. Minimum de 5 caractères.',
            ],
        ],
        'retard' => [
          [
              'rule' => '/^\d+$/',
              'message' => 'Veuillez saisir un nombre entier.'
          ]
        ],
        'action' => [
            [
                'rule' => 'notBlank',
                'message' => 'Sélectionner un type d\'action.'
            ]
        ],
        'compteur_id' => [
            [
                'rule' => 'notBlank',
                'message' => 'Sélectionner un compteur.'
            ]
        ],
        'modele_projet_id' => [
            [
                'rule' => 'notBlank',
                'message' => 'Sélectionner le modèle de projet.'
            ]
        ],
        'modele_deliberation_id' => [
            [
                'rule' => 'notBlank',
                'message' => 'Sélectionner le modèle de délibération.'
            ]
        ],
        'modele_convocation_id' => [
            [
                'rule' => 'notBlank',
                'message' => 'Sélectionner le modèle de la convocation.'
            ]
        ],
        'modele_ordredujour_id' => [
            [
                'rule' => 'notBlank',
                'message' => 'Sélectionner le modèle de l\'ordre du jour.'
            ]
        ],
        'modele_pvsommaire_id' => [
            [
                'rule' => 'notBlank',
                'message' => 'Sélectionner le modèle du PV sommaire.'
            ]
        ],
        'modele_pvdetaille_id' => [
            [
                'rule' => 'notBlank',
                'message' => 'Selectionner le modèle du PV complet.'
            ]
        ],
        'Typeacte' => [
            [
              'rule' => [
                  'multiple',
                  [
                      'min' => 1,
                  ],
                ],
                'message' => 'Sélectionner un ou plusieurs types d\'actes.'
            ]
        ],
        'parent_id' => [
            [
              'rule' => ['canSaveParentId'],
              'message' => 'Vous ne pouvez pas selectionner une appartenance pour une action en séance de type "Voter".'
            ]
        ],
    ];
    public $hasMany = [
        'TypeseancesTypeacte' => [
            'className' => 'TypeseancesTypeacte',
            'foreignKey' => 'typeseance_id',
            'fields' => '',
            'conditions' => '',
            'order' => ''
        ],
        'Deliberation' => [
            'className' => 'DeliberationSeance',
            'foreignKey' => 'typeseance_id',
        ],
        'TypeseanceTypeseance' => [
            'className' => 'Typeseance',
            'foreignKey' => 'parent_id',
            'order' => 'id ASC',
            'dependent' => false
        ],
        ];
    public $belongsTo = [
        'Compteur' => [
            'className' => 'Compteur',
            'foreignKey' => 'compteur_id'],
        'Modele_projet' => [
            'className' => 'ModelOdtValidator.Modeltemplate',
            'foreignKey' => 'modele_projet_id'],
        'Modele_deliberation' => [
            'className' => 'ModelOdtValidator.Modeltemplate',
            'foreignKey' => 'modele_deliberation_id'],
        'Modele_convocation' => [
            'className' => 'ModelOdtValidator.Modeltemplate',
            'foreignKey' => 'modele_convocation_id'],
        'Modele_ordredujour' => [
            'className' => 'ModelOdtValidator.Modeltemplate',
            'foreignKey' => 'modele_ordredujour_id'],
        'Modele_pvsommaire' => [
            'className' => 'ModelOdtValidator.Modeltemplate',
            'foreignKey' => 'modele_pvsommaire_id'],
        'Modele_pvdetaille' => [
            'className' => 'ModelOdtValidator.Modeltemplate',
            'foreignKey' => 'modele_pvdetaille_id'],
        'Modele_journal_seance' => [
            'className' => 'ModelOdtValidator.Modeltemplate',
            'foreignKey' => 'modele_journal_seance_id']
    ];
    public $hasAndBelongsToMany = [
        'Typeacteur' => [
            'classname' => 'Typeacteur',
            'joinTable' => 'typeseances_typeacteurs',
            'foreignKey' => 'typeseance_id',
            'associationForeignKey' => 'typeacteur_id',
            'conditions' => '',
            'order' => '',
            'limit' => '',
            'unique' => true,
            'finderQuery' => '',
            'deleteQuery' => ''],
        'Acteur' => [
            'classname' => 'Acteur',
            'joinTable' => 'typeseances_acteurs',
            'foreignKey' => 'typeseance_id',
            'associationForeignKey' => 'acteur_id',
            'conditions' => '',
            'order' => '',
            'limit' => '',
            'unique' => true,
            'finderQuery' => '',
            'deleteQuery' => ''],
        'Typeacte' => [
            'classname' => 'Typeacte',
            'joinTable' => 'typeseances_typeactes',
            'foreignKey' => 'typeseance_id',
            'associationForeignKey' => 'typeacte_id',
            'conditions' => '',
            'order' => '',
            'limit' => '',
            'unique' => true,
            'finderQuery' => '',
            'deleteQuery' => '']
    ];

    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->Behaviors->load('AuthManager.AclManager', [
            'type' => 'controlled',
            'loadBehavior' => 'DynamicDbConfig'
        ]);
    }

    /**
     * @version 4.3
     * @access public
     * @param type $query
     * @return type
     */
    public function beforeFind($query = [])
    {
        if (!empty($query['allow'])) {
            $query['conditions'] = (is_array($query['conditions'])) ? $query['conditions'] : [];

            //Gestion des droits sur les types de seances
            $conditions = parent::allowAcl($query['allow'], $this->alias, 'Typeseance.id');
            if ($conditions !== false) {
                $query['conditions'] = array_merge($query['conditions'], $conditions);
            }
        }
        return $query;
    }

    /**
     * Retourne le libellé correspondant au champ action 0 : voter, 1 donner un avis , 2 sans action
     *
     * @version 4.3
     * @access public
     * @param type $action
     * @param type $majuscule
     * @return type
     */
    public function libelleAction($action = null, $majuscule = false)
    {
        switch ($action) {
            case 0:
                return $majuscule ? 'Voter' : 'voter';
            case 1:
                return ($majuscule ? 'D' : 'd') . 'onner un avis';
            case 2:
                return ($majuscule ? 'S' : 's') . 'ans action';
        }
    }

    /**
     * Retourne un tableau d'acteurs correspondant a la liste des convocations
     * pour le type de séance $typeseance_id ordonnée par position et nom
     * selon le paramètre $elu on retourne les acteurs suivants :
     *      - null : tous les acteurs élus et non élus
     *      - true : les acteurs élus
     *      - false : les acteurs non élus
     *
     * @version 4.3
     * @access public
     * @param type $typeseance_id
     * @param type $elu
     * @param type $fields
     * @return type
     */
    public function acteursConvoquesParTypeSeanceId($typeseance_id, $elu = null, $fields = [])
    {
        $typeseance = $this->find('first', [
            'contain' => ['Typeacteur.id', 'Acteur.id'],
            'fields' => ['id'],
            'conditions' => ['Typeseance.id' => $typeseance_id]]);
        if (empty($typeseance)) {
            return null;
        }

        /* Par type d'acteur */
        $inTypeacteur = [];
        foreach ($typeseance['Typeacteur'] as $typeacteur) {
            $inTypeacteur[] = $typeacteur['id'];
        }

        /* Par acteur */
        $inId = [];
        foreach ($typeseance['Acteur'] as $acteur) {
            $inId[] = $acteur['id'];
        }

        $condition['Acteur.actif'] = 1;
        if ($elu !== null) {
            $condition['Typeacteur.elu'] = $elu;
        }
        if (!empty($inTypeacteur) && !empty($inId)) {
            $condition['OR']['Acteur.id'] = $inId;
            $condition['OR']['Acteur.typeacteur_id'] = $inTypeacteur;
        } elseif (!empty($inTypeacteur)) {
            $condition['Acteur.typeacteur_id'] = $inTypeacteur;
        } elseif (!empty($inId)) {
            $condition['Acteur.id'] = $inId;
        } else {
            return null;
        }

        return $this->Acteur->find('all', [
                'recursive' => 0,
                'fields' => $fields,
                'order' => ['Acteur.position ASC'],
                'conditions' => $condition]);
    }

    /**
     * Retourne d'id du modèle d 'édition du type de séance $typeseance_id
     * en sonction de l'état du projet de délibération
     *
     * @access public
     * @param type $typeseance_id
     * @param type $etat
     * @return type
     */
    public function modeleProjetDelibParTypeSeanceId($typeseance_id, $etat)
    {
        $typeseance = $this->find('first', [
            'conditions' => ['Typeseance.id' => $typeseance_id],
            'fields' => ['modele_projet_id', 'modele_deliberation_id'],
            'recursive' => -1]);
        if ($etat >= 3) {
            return $typeseance['Typeseance']['modele_deliberation_id'];
        } else {
            return $typeseance['Typeseance']['modele_projet_id'];
        }
    }

    /**
     * @version 4.3
     * @access public
     * @param type $type_id
     * @return type
     */
    public function getLibelle($type_id)
    {
        $this->recursive = -1;
        $libelle = $this->read('libelle', $type_id);
        return $libelle['Typeseance']['libelle'];
    }

    /**
     * @access public
     * @param type $seance_id
     * @return type
     *
     * @version 5.1
     */
    public function isDeliberante($id)
    {
        $this->recursive = -1;
        $typeseance = $this->findById($id, ['action']);
        return ($typeseance['Typeseance']['action'] == 0);
    }

    /**
     * Teste la possibilité de supprimer un type de séance
     * (le type est il affécté à une séance ?)
     *
     * @version 4.3
     * @access public
     * @param integer $id -6> identifiant du type séance à éliminer
     * @return boolean -->true si aucune séance n'est associée à ce type de séance, false sinon
     */
    public function isDeletable($id)
    {
        $seance = ClassRegistry::init('Seance');
        $nbSeancesEnCours = $seance->find('count', [
            'conditions' => ['type_id' => $id],
            'recursive' => -1
        ]);
        return empty($nbSeancesEnCours);
    }

    /**
     *
     * @return type
     *
     * @version 5.1.0
     */
    public function parentNode()
    {
        return null;
    }

    /**
     * @version 4.3
     * @access public
     * @return type
     *
     * @version 5.1.0
     */
    public function parentNodeAlias()
    {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        $data = $this->data;
        if (empty($this->data)) {
            $data = $this->read();
        }

        return ['Typeseance' => ['alias' => $data['Typeseance']['libelle']]];
    }
    /**
     * [canSaveParentId description]
     * @return [type] [description]
     *
     * @version 5.1.0
     */
    public function canSaveParentId()
    {
        return !empty($this->data['Typeseance']['parent_id'])
        && isset($this->data['Typeseance']['action'])
        && ($this->data['Typeseance']['action'] === '0') ? false : true;
    }
}
