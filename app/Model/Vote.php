<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [Vote description]
 */
class Vote extends AppModel
{
    public $name = 'Vote';
    public $belongsTo = [
        'Acteur' => ['className' => 'Acteur',
            'foreignKey' => 'acteur_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => ''
        ],
        'Deliberation' => ['className' => 'Deliberation',
            'foreignKey' => 'delib_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => ''
        ],
    ];

    /**
     * Fonction d'initialisation des variables de fusion pour les votes d'un projet
     * les bibliothèques Gedooo doivent être inclues par avance
     * génère une exception en cas d'erreur
     *
     * @version 5.0
     * @since 4.3
     * @access public
     * @param type $aData
     * @param object_by_ref $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param integer $deliberationId --> id de la délibération
     * @throws Exception
     *
     * @SuppressWarnings(PHPMD)
     */
    public function setVariablesFusion(&$aData, &$modelOdtInfos, $deliberationId)
    {
        // initialisations
        $fusionVariables = [
            'nom', 'prenom', 'salutation', 'titre', 'adresse1', 'adresse2', 'cp',
            'ville', 'email', 'telfixe', 'telmobile', 'note'
        ];
        $voteIterations = [
            ['iterationName' => 'ActeursContre', 'fusionVariableSuffixe' => 'contre', 'voteResultat' => 2],
            ['iterationName' => 'ActeursPour', 'fusionVariableSuffixe' => 'pour', 'voteResultat' => 3],
            ['iterationName' => 'ActeursAbstention', 'fusionVariableSuffixe' => 'abstention', 'voteResultat' => 4],
            [
                'iterationName' => 'ActeursSansParticipation',
                'fusionVariableSuffixe' => 'sans_participation',
                'voteResultat' => 5
            ]
        ];

        // pour chaque itération de vote
        foreach ($voteIterations as &$voteIteration) {
            // initialisations
            $conditions = ['Vote.delib_id' => $deliberationId, 'Vote.resultat' => $voteIteration['voteResultat']];

            // nombre de votes
            $nbVotes = $this->find('count', ['recursive' => -1, 'conditions' => $conditions]);
            if ($modelOdtInfos->hasUserFieldDeclared('nombre_acteur_' . $voteIteration['fusionVariableSuffixe'])) {
                $aData['nombre_acteur_' . $voteIteration['fusionVariableSuffixe']] = [
                    'value' => $nbVotes,
                    'type' => 'text'
                ];
            }
            // liste des variables utilisées dans le template
            $acteurFields = $aliasActeurFields = [];
            foreach ($fusionVariables as $fusionVariable) {
                if ($modelOdtInfos->hasUserFieldDeclared(
                    $fusionVariable . '_acteur_' . $voteIteration['fusionVariableSuffixe']
                )
                ) {
                    $aliasActeurFields[] = 'Acteur.' . $fusionVariable;
                    $acteurFields[] = $fusionVariable;
                }
            }
            if (empty($aliasActeurFields)) {
                continue;
            } else {
                $aliasActeurFields[] = 'Acteur.id';
            }
            // lecture des données en base
            $acteurs = $this->find('all', [
                'fields' => ['Vote.id'],
                'contain' => $aliasActeurFields,
                'conditions' => ['Vote.delib_id' => $deliberationId, 'Vote.resultat' => $voteIteration['voteResultat']],
                'order' => 'Acteur.position ASC']);

            $aVotesActeurs = [];
            if (empty($nbVotes)) {
                $aVoteActeur = [];
                foreach ($acteurFields as $fieldname) {
                    $aVoteActeur[$fieldname . '_acteur_' . $voteIteration['fusionVariableSuffixe']] = [
                        'value' => '',
                        'type' => 'text'
                    ];
                }
                $aVotesActeurs[] = $aVoteActeur;
            } else {
                foreach ($acteurs as &$acteur) {
                    // traitement du suppléant
                    $listepresence = $this->Deliberation->Listepresence->find('first', [
                        'fields' => ['suppleant_id'],
                        'conditions' => ['acteur_id' => $acteur['Acteur']['id'], 'delib_id' => $deliberationId],
                        'recursive' => -1]);

                    if (!empty($listepresence['Listepresence']['suppleant_id'])) {
                        $suppleant = $this->Acteur->find('first', [
                            'fields' => $aliasActeurFields,
                            'conditions' => ['id' => $listepresence['Listepresence']['suppleant_id']],
                            'recursive' => -1
                        ]);
                        if (empty($suppleant)) {
                            throw new Exception(
                                'suppléant non trouvé id:' . $listepresence['Listepresence']['suppleant_id']
                            );
                        }
                        $acteur = &$suppleant;
                    }

                    $aVoteActeur = [];
                    foreach ($acteurFields as $fieldname) {
                        $aVoteActeur[$fieldname . '_acteur_' . $voteIteration['fusionVariableSuffixe']] = [
                            'value' => $acteur['Acteur'][$fieldname],
                            'type' => 'text'
                        ];
                    }
                    $aVotesActeurs[] = $aVoteActeur;
                }
            }

            $aData[$voteIteration['iterationName']] = $aVotesActeurs;
        }
    }
}
