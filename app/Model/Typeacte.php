<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class Typeacte extends AppModel
{
    public $name = 'Typeacte';
    //public $displayField = 'name';

    public $belongsTo = [
        'Compteur' => [
            'className' => 'Compteur',
            'foreignKey' => 'compteur_id'],
        'Nature' => [
            'className' => 'Nature',
            'foreignKey' => 'nature_id'],
        'Modelprojet' => [
            'className' => 'ModelOdtValidator.Modeltemplate',
            'conditions' => ['Modelprojet.modeltype_id' => [MODELE_TYPE_TOUTES, MODELE_TYPE_PROJET]],
            'foreignKey' => 'modele_projet_id'],
        'Modeldeliberation' => [
            'className' => 'ModelOdtValidator.Modeltemplate',
            'conditions' => ['Modeldeliberation.modeltype_id' =>
                [
                    MODELE_TYPE_TOUTES, MODELE_TYPE_PROJET, MODELE_TYPE_DELIBERATION
                ]
            ],
            'foreignKey' => 'modele_final_id'],
        'Modelbordereau' => [
            'className' => 'ModelOdtValidator.Modeltemplate',
            'conditions' => ['Modelbordereau.modeltype_id' => [MODELE_TYPE_BORDEREAU_PROJET]],
            'foreignKey' => 'modele_bordereau_projet_id'],
    ];
    public $hasMany = ['Deliberation', 'TypeseancesTypeacte'];
    public $validate = [
        'name' => [
            [
                'rule' => 'notBlank',
                'message' => 'Veuillez saisir le libellé de l\'acte'
            ],
            [
                'rule' => 'isUnique',
                'message' => 'Entrez un autre libellé, celui-ci est déjà utilisé.'
            ]
        ],
        'compteur_id' => [
            [
                'rule' => 'notBlank',
                'message' => 'Sélectionner un compteur.'
            ]
        ],
        'modele_projet_id' => [
            [
                'rule' => 'notBlank',
                'message' => 'Sélectionner le modèle de projet.'
            ]
        ],
        'modele_deliberation_id' => [
            [
                'rule' => 'notBlank',
                'message' => 'Sélectionner le modèle de délibération.'
            ]
        ],
        'nature' => [
            [
                'rule' => 'notBlank',
                'message' => 'Sélectionner au moins une nature.'
            ]
        ],
        'gabarit_projet_upload' => [
            'gabaritProjetUploadRule-1' => [
                'rule' => 'uploadFileName',
                'required' => false,
                'allowEmpty' => true,
                'message' => 'Seulement les lettres, les entiers et les caractères spéciaux -_.& '
                    .'sont autorisés dans le nom du fichier. Minimum de 5 caractères', 'growl'
            ],
            'gabaritProjetUploadRule-2' => [
                'rule' => ['checkFormat', 'application/vnd.oasis.opendocument.text', false],
                'message' => 'Le gabarit doit être au format ODT.'
            ]
        ],
        'gabarit_synthese_upload' => [
            'gabaritSyntheseUploadRule-1' => [
                'rule' => 'uploadFileName',
                'required' => false,
                'allowEmpty' => true,
                'message' => 'Seulement les lettres, les entiers et les caractères spéciaux -_.& '
                    .'sont autorisés dans le nom du fichier. Minimum de 5 caractères', 'growl'
            ],
            'gabaritSyntheseUploadRule-2' => [
                'rule' => ['checkFormat', 'application/vnd.oasis.opendocument.text', false],
                'message' => 'Le gabarit doit être au format ODT.'
            ]
        ],
        'gabarit_acte_upload' => [
            'gabaritActeUploadRule-1' => [
                'rule' => 'uploadFileName',
                'required' => false,
                'allowEmpty' => true,
                'message' => 'Seulement les lettres, les entiers et les caractères spéciaux -_.& '
                    .'sont autorisés dans le nom du fichier. Minimum de 5 caractères', 'growl'
            ],
            'gabaritActeUploadRule-2' => [
                'rule' => ['checkFormat', 'application/vnd.oasis.opendocument.text', false],
                'message' => 'Le gabarit doit être au format ODT.'
            ]
        ],
        'teletransmettre' => [
            'rule' => ['boolean'],
            'message' => 'Valeur incorrecte pour l\'attribut "Télétransmettre".'
        ]
    ];

    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->Behaviors->load('AuthManager.AclManager', [
            'type' => 'controlled',
            'loadBehavior' => 'DynamicDbConfig'
        ]);
    }

    /**
     * @param array $query
     * @return array
     */
    public function beforeFind($query = [])
    {
        if (!empty($query['allow'])) {
            $query['conditions'] = (is_array($query['conditions'])) ? $query['conditions'] : [];

            //Gestion des droits sur les types d'actes
            $conditions = parent::allowAcl($query['allow'], $this->alias, 'Typeacte.id');
            if ($conditions !== false) {
                $query['conditions'] = array_merge($query['conditions'], $conditions);
            }
        }

        return $query;
    }

    /**
     * @version 4.3
     * @access public
     * @param type $type_id
     * @return type
     */
    public function getLibelle($type_id)
    {
        $name = $this->find('first', [
            'conditions' => ['Typeacte.id' => $type_id],
            'recursive' => -1,
            'fields' => ['Typeacte.name']]);
        return $name['Typeacte']['name'];
    }

    /**
     * @version 4.3
     * @access public
     * @param type $type_id
     * @param type $field
     * @return type
     */
    public function getModelId($type_id, $field)
    {
        $typeActe = $this->find('first', [
            'conditions' => ['Typeacte.id' => $type_id],
            'recursive' => -1,
            'fields' => [$field]]);

        return !empty($typeActe['Typeacte'][$field]) ? $typeActe['Typeacte'][$field] : '';
    }

    /**
     * @access public
     * @return type
     */
    public function getTypeActeIdDeliberant()
    {
        $typeactes = $this->find('all', [
            'conditions' => [
                'Typeacte.deliberant' => true
            ],
            'fields' => ['Typeacte.id'],
            'recursive' => -1]);
        return Set::extract('/Typeacte/id', $typeactes);
    }

    /**
     * Test la possibilité de supprimer un type d'acte (le type est il affécté à un acte ?)
     *
     * @access public
     * @param integer $id --> identifiant du type d'acte à éliminer
     * @return boolean --> true si aucun acte n'est associée à ce type d'acte, false sinon
     */
    public function isDeletable($id)
    {
        $nbSeancesEnCours = $this->Deliberation->find('first', ['conditions' => ['typeacte_id' => $id],
            'recursive' => -1
        ]);
        return empty($nbSeancesEnCours);
    }

    /**
     * Nature code par rapport au type d'acte pour l'envoi vers le Tdt
     *
     * @version 5.1
     * @access public
     * @param array $data
     * @return type
     */
    public function getIsDeliberantById($id)
    {
        $typeacte = $this->find('first', [
            'fields' => ['Typeacte.id','Typeacte.deliberant'],
            'conditions' => ['Typeacte.id' => $id],
            'recursive' => -1,
        ]);

        return $typeacte['Typeacte']['deliberant'];
    }

    /**
     *
     * @return type
     */
    public function parentNode()
    {
        return null;
    }

    /**
     * @version 4.3
     * @access public
     * @return type
     */
    public function parentNodeAlias()
    {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        $data = $this->data;
        if (empty($this->data)) {
            $data = $this->read();
        }

        return ['Typeacte' => ['alias' => $data['Typeacte']['name']]];
    }

    /**
     * Initialisation des variables de fusion pour le type d'acte
     *
     * @param array  $aData Données à fusionner
     * @param object $modelOdtInfos objet PhpOdtApi du fichier odt du modèle d'édition
     * @param integer $id identifiant du type d'acte
     *
     * @version 5.1
     */
    public function setVariablesFusion(&$aData, &$modelOdtInfos, $id)
    {
        if ($modelOdtInfos->hasUserFieldDeclared('type_acte_id')) {
            if ($modelOdtInfos->hasUserFieldDeclared('type_acte_id')) {
                $aData['type_acte_id'] = ['value' => $id, 'type' => 'text'];
            }
        }

        if ($modelOdtInfos->hasUserFieldDeclared('type_acte')) {
            $typeacte = $this->find('first', ['fields' => ['name'],
                'conditions' => ['id' => $id],
                'recursive' => -1
            ]);

            $aData['type_acte'] = ['value' => $typeacte['Typeacte']['name'], 'type' => 'text'];
        }
    }
}
