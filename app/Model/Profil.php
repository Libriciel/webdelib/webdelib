<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr).
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * @see        https://adullact.net/projects/webdelib web-delib Project
 *
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [Profil description].
 *
 * @SuppressWarnings(PHPMD)
 */
class Profil extends AppModel
{
    public $name = 'Profil';

    public $validate = [
        'name' => [
            [
                'rule' => 'notBlank',
                'message' => 'Entrer le libellé.',
            ],
        ],
        'role_id' => [
            [
                'rule' => ['notBlank'],
                'message' => 'Le type est obligatoire',
            ],
        ],
    ];
    public $belongsTo = [
        'ProfilParent' => [
            'className' => 'Profil',
            'foreignKey' => 'parent_id',
        ],
        'Role' => [
            'className' => 'Role',
            'foreignKey' => 'role_id',
        ],
    ];
    public $hasMany = [
        'ProfilEnfant' => [
            'className' => 'Profil',
            'foreignKey' => 'parent_id',
        ],
        'User' => [
            'className' => 'User',
            'foreignKey' => 'profil_id',
        ],
    ];
    public $hasAndBelongsToMany = ['Infosupdef'];

    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->Behaviors->load('AuthManager.AclManager', ['type' => 'requester', 'loadBehavior' => 'DynamicDbConfig']);
    }

    /**
     * Envoi une notification par mail à sur les profils administrateur.
     *
     * @version 4.3
     *
     * @param string $type --> notification à envoyer
     *
     * @return bool --> succès de l'envoi
     */
    public function notifierAdmin($type, $message)
    {
        App::uses('CakeEmail', 'Network/Email');
        $profils = $this->find('all', [
            'contain' => [
                'User' => [
                  'fields' => ['nom', 'prenom', 'email', 'accept_notif', 'mail_'.$type],
                  'conditions' => ['active' => true],
                ],
            ],
            //'conditions' => array('role_id' => 2),
            'recursive' => -1,
        ]);
        $config_mail = 'default';
        $Email = new CakeEmail($config_mail);
        switch ($type) {
            case 'error_send_tdt_auto':
                $template = 'error_send_tdt_auto';
                $subject = __("[web-delib] Erreur lors de l'envoi automatique des actes à télétransmettre.");
                break;
            case 'majtdtcourrier':
                $template = 'error_majtdtcourrier';
                $subject = __('[web-delib] Un acte télétransmis est en erreur ou a été annulé.');
                break;
        }
        foreach ($profils as $profil) {
            foreach ($profil['User'] as $user) {
                // utilisateur existe et accepte les mails ?
                if (empty($user) || empty($user['accept_notif']) || empty($user['email']) || empty($user['mail_'.$type])
                ) {
                    continue;
                }

                $Email->viewVars([
                    'nom' => $user['nom'],
                    'prenom' => $user['prenom'],
                    'message' => preg_replace('/^\R$/', "<br />", h($message))
                ]);

                $Email->template($template, 'default')->to($user['email'])->subject($subject)->send();
            }
        }
    }

    /**
     * @version 4.3
     *
     * @return type
     */
    public function parentNode()
    {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        $data = $this->data;
        if (empty($this->data)) {
            $data = $this->read();
        }
        if (empty($data['Profil']['profil_id'])) {
            return null;
        }

        return ['Profil' => ['id' => $data['Profil']['profil_id']]];
    }

    /**
     * @version 4.3
     *
     * @return type
     */
    public function parentNodeAlias()
    {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        $data = $this->data;
        if (empty($this->data)) {
            $data = $this->read();
        }

        return ['Profil' => ['alias' => $data['Profil']['name']]];
    }
}
