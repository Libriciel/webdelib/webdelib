<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

use phpgedooo_client\GDO_PartType;
use phpgedooo_client\GDO_FusionType;
use phpgedooo_client\GDO_ContentType;

App::uses('Builder', 'FusionConv.Utility');
App::uses('CakeTime', 'Utility');
App::uses('Folder', 'Utility');

/**
  * OdtFusion behavior class.
  *
  * Centralise les fonctions de fusion des modèles odt avec les données des modèles
  *
  * Callbacks :
  *  - getModelTemplateId($this->id, $this->modelOptions) : le modèle doit
  *      posséder cette méthode qui retourne l'id du modeltemplate à utiliser
  *  - beforeFusion($this->id, $this->modelOptions) : le modèle doit posséder
  *      cette méthode pour l'initialisation des variables gedooo avant de faire
  *      la fusion
  *
  * Variables du modèle appellant initialisées dynamiquement :
  *  - odtFusionResult : le résultat de la fusion est stocké dans la variable
  *      odtFusionResult du modèle appelent
  *  - modelTemplateOdtInfos : instance de la librairie ModelOdtValidator.Lib.phpOdtApi
  *      de manipulation des odt
  * @version 5.1.0
  * @version 4.3.0
  * @package app.Model.Behavior
 */
class OdtFusionBehavior extends ModelBehavior
{
    /**
     * Id de l'occurence en base de données à fusionner
     * @var [id]
     */
    protected $id = null;
    /**
     * Variables du modelTemplate utilisé pour la fusion
     * @var [int]
     */
    protected $modelTemplateId = null;
    /**
     * [protected description]
     * @var [int]
     */
    protected $modelTemplateName = '';
    /**
     * [protected description]
     * @var [int]
     */
    protected $modelTemplateFileOutputFormat = '';
    /**
     * [protected description]
     * @var [array]
     */
    protected $modelTemplateFileOutputFormatValues = [];
    /**
     * [protected description]
     * @var [string]
     */
    protected $modelTemplateContent = '';
    /**
     * variable pour la détermination du nom du fichier de fusion
     * @var [string]
     */
    protected $fileNameSuffixe = '';
    /**
     * Options gérées par la classe appelante (Model) qui seront passées aux fonctions de callback
     * @var [array]
     */
    protected $modelOptions = [];

    /**
     * Initialisation du comportement : détection et chargement du template
     * Génère une exception en cas d'erreur
     *
     * @param Model $model
     * @param array $options liste des options formatée comme suit :
     *  'id' => id de l'occurence du modèle sujet à la fusion
     *  'fileNameSuffixe' : suffixe du nom de la fusion (défaut : $id)
     *  'modelTemplateId' : id du template à utiliser
     *  'modelOptions' : options gérées par la classe appelante
     * @throws Exception
     * @return void
     * @version 5.1.0
     * @since 4.3.0
     */
    public function setup(Model $model, $options = [])
    {
        // initialisations des options
        $this->setupOptions($options);

        // chargement du modèle template
        if (empty($this->modelTemplateId)) {
            $this->modelTemplateId = $model->getModelTemplateId($this->id, $this->modelOptions);
        }
        if (empty($this->modelTemplateId)) {
            throw new Exception(
                'identifiant du modèle d\'édition non trouvé pour id:'
                . $this->id . ' du model de données ' . $model->alias
            );
        }
        $myModeltemplate = ClassRegistry::init('ModelOdtValidator.Modeltemplate');
        $modelTemplate = $myModeltemplate->find('first', [
            'recursive' => -1,
            'fields' => ['name', 'content','file_output_format'],
            'conditions' => ['id' => $this->modelTemplateId]]);
        if (empty($modelTemplate)) {
            throw new Exception('modèle d\'édition non trouvé en base de données id:' . $this->id);
        }

        $this->modelTemplateName = $modelTemplate['Modeltemplate']['name'];
        $this->modelTemplateFileOutputFormat = $modelTemplate['Modeltemplate']['file_output_format'];
        $this->modelTemplateContent = $modelTemplate['Modeltemplate']['content'];

        // résultat de la fusion
        $model->odtFusionResult = null;

        // instance de manipulation du fichier odt du modèle template
        App::uses('phpOdtApi', 'ModelOdtValidator.Lib');
        $model->modelTemplateOdtInfos = new phpOdtApi();
        $model->modelTemplateOdtInfos->loadFromOdtBin($this->modelTemplateContent);
    }

    /**
     * Retour la Fusion dans le format demandé
     * @param string $mimeType odt|pdf
     * @return string
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function getOdtFusionResult(Model &$model, $mimeType = 'pdf')
    {
        App::uses('ConversionComponent', 'Controller/Component');
        App::uses('Component', 'Controller');
        // initialisations
        $collection = new ComponentCollection();
        $this->Conversion = new ConversionComponent($collection);
        try {
            //Actualisation du sommaire
            $content = $this->Conversion->convertirFlux(
                $model->odtFusionResult->binary,
                'odt',
                'odt',
                true
            );
            $content = $this->Conversion->convertirFlux(
                $content,
                'odt',
                'odt',
                true
            );
            if ($mimeType==='pdf') {
                $content = $this->Conversion->convertirFlux(
                    $content,
                    'odt',
                    'pdf',
                    false
                );
            }
        } catch (ErrorException $e) {
            $this->log('Erreur lors de la conversion : ' . $e->getCode(), 'error');
        }

        return $content;
    }

    /**
     * Suppression en mémoire du retour de la fusion
     *
     * @param Model $model
     * @version 4.3.0
     */
    public function deleteOdtFusionResult(Model &$model)
    {
        unset($model->odtFusionResult->binary);
    }

    /**
     * initialisation des variables du behavior
     * @param array $options liste des options formatée comme suit :
     *  'id' => id de l'occurence du modèle sujet à la fusion
     *  'fileNameSuffixe' : suffixe du nom de la fusion (défaut : $id)
     *  'modelTemplateId' : id du template à utiliser
     *  'modelOptions' : options gérées par la classe appelante
     * @return void
     * @version 5.1.0
     * @since 4.3.0
     */
    private function setupOptions($options)
    {
        // initialisations
        $defaultOptions = [
            'id' => $this->id,
            'fileNameSuffixe' => $this->fileNameSuffixe,
            'modelTemplateId' => $this->modelTemplateId,
            'modelOptions' => $this->modelOptions
        ];

        if (!empty($options['modelOptions']) && !empty($this->modelOptions)) {
            $options['modelOptions'] = array_merge($this->modelOptions, $options['modelOptions']);
        }
        $options = array_merge($defaultOptions, $options);

        // affectation des variables de la classe
        $this->id = $options['id'];
        $this->fileNameSuffixe = empty($options['fileNameSuffixe']) ? $options['id'] : $options['fileNameSuffixe'];
        $this->modelTemplateId = $options['modelTemplateId'];
        $this->modelOptions = $options['modelOptions'];
    }

    /**
     * Retourne un nom pour la fusion qui est constitué du nom (liellé)
     * du modèle odt échapé, suivi de '_'.$suffix.
     * Génère une exception en cas d'erreur
     * @param Model $model modele du comportement
     * @param array $options tableau des parmètres optionnels :
     *    'id' : identifiant de l'occurence en base de données (défaut : $this->id)
     *    'fileNameSuffixe' : suffixe du nom de la fusion (défaut : $id)
     *  'modelOptions' : options gérées par la classe appelante
     * @return string
     * @throws Exception en cas d'erreur
     * @version 5.1.0
     * @since 4.3.0
     */
    public function fusionName(Model &$model, $options = [])
    {
        // initialisations
        $this->setupOptions($options);
        if (empty($this->modelTemplateId)) {
            throw new Exception('détermination du nom de la fusion -> modèle d\'édition indéterminé');
        }

        if (empty($this->modelTemplateFileOutputFormat)) {
            // contitution du nom
            $fusionName = str_replace(
                [' ', 'é', 'è', 'ê', 'ë', 'à'],
                ['_', 'e', 'e', 'e', 'e', 'a'],
                $this->modelTemplateName
            );
            return preg_replace(
                '/[^a-zA-Z0-9-_\.]/',
                '',
                $fusionName
            ) . (empty($this->fileNameSuffixe) ? '' : '_') . $this->fileNameSuffixe;
        }
        if (isset($options['modelTemplateFileOutputFormatValues'])) {
            $this->modelTemplateFileOutputFormatValues =
                array_merge(
                    $this->modelTemplateFileOutputFormatValues,
                    $options['modelTemplateFileOutputFormatValues']
                );
        }
        $template = ClassRegistry::init('Template');
        $modelTemplateName = $template->fileOutputFormat(
            $this->modelTemplateFileOutputFormat,
            $this->modelTemplateFileOutputFormatValues
        );
        // constitution du nom
        $fusionName = str_replace([' ', 'é', 'è', 'ê', 'ë', 'à'], ['_', 'e', 'e', 'e', 'e', 'a'], $modelTemplateName);

        if (empty($fusionName)) {
            throw new Exception('Détermination du nom de fichier de la fusion impossible');
        }

        return preg_replace('/[^a-zA-Z0-9-_\.]/', '', $fusionName);
    }

    /**
     * Fonction de fusion du modèle odt et des données.
     * Le résultat de la fusion est un odt dont le contenu est stocké dans
     * la variable du model odtFusionResult
     * @param Model $model modele du comportement
     * @param array $options tableau des parmètres optionnels :
     *      'id' : identifiant de l'occurence en base de données (fusionNamedéfaut : $this->id)
     *      'modelOptions' : options gérées par la classe appelante
     * @return void
     * @throws Exception en cas d'erreur
     * @version 5.1.0
     * @since 4.3.0
     */
    public function odtFusion(Model &$model, $options = [])
    {
        // initialisations
        $this->setupOptions($options);
        if (empty($this->modelTemplateId)) {
            throw new Exception('détermination du nom de la fusion -> modèle d\'édition indéterminé');
        }

        // initialisation des datas
        $aData = [];

        // initialisation des variables communes
        $this->setVariablesCommunesFusion($model, $aData);

        // initialisation des variables du model de données
        $model->beforeFusion($aData, $model->modelTemplateOdtInfos, $this->id, $this->modelOptions);

        // initialisation des variables communes
        $this->setVariablesFusionName($model, $aData);

        $MainPart = new GDO_PartType();
        $correspondances = [];
        $types = [];
        $data = [];
        $this->format($MainPart, $aData, $data, $types, $correspondances);

        $folder = new Folder(TMP . 'files' . DS . $model->getTenantName() . DS . 'FusionConv', true, 0777);

        file_put_contents($folder->path . DS . 'Donnees.txt', var_export($MainPart, true));

        $Fusion = new GDO_FusionType(
            Configure::read('FusionConv.Gedooo.wsdl'),
            new GDO_ContentType(
                "",
                $this->modelTemplateName,
                "application/vnd.oasis.opendocument.text",
                "binary",
                $this->modelTemplateContent
            ),
            'application/vnd.oasis.opendocument.text',
            Builder::main($MainPart, $data, $types, $correspondances)
        );
        file_put_contents($folder->path . DS . 'Serialize.txt', serialize($MainPart));
        file_put_contents($folder->path . DS . 'Donnees.txt', var_export($MainPart, true));
        $Fusion->process();

        $model->odtFusionResult = $Fusion->getContent();

        if (is_array($model->odtFusionResult)) {
            throw new Exception($model->odtFusionResult['Message']);
        }

        file_put_contents($folder->path . DS . 'Resultat.odt', $model->odtFusionResult->binary);

        // libération explicite de la mémoire
        unset($aData);
    }

    /**
     * fonction de fusion des variables communes : collectivité et dates
     * génère une exception en cas d'erreur
     * @param GDO_PartType $oMainPart variable Gedooo de type maintPart du document à fusionner
     * @param Model $model modele du comportement
     * @version 5.1.0
     * @since 4.3.0
     */
    private function setVariablesCommunesFusion(Model &$model, &$aData)
    {
        // variables des dates du jour
        if ($model->modelTemplateOdtInfos->hasUserFieldDeclared('date_jour_courant')) {
            //myDate = new DateComponent;
            $aData['date_jour_courant'] = [
                'value' => CakeTime::i18nFormat(date('Y-m-d H:i:s'), '%A %d %B %Y')
                    . ' à '
                    . CakeTime::i18nFormat(date('Y-m-d H:i:s'), '%k:%M'),
                'type' => 'text'
            ];
        }
        if ($model->modelTemplateOdtInfos->hasUserFieldDeclared('date_du_jour')) {
            $aData['date_du_jour'] = ['value' => date("d/m/Y", strtotime("now")), 'type' => 'date'];
        }

        // variables de la collectivité
        $myCollectivite = ClassRegistry::init('Collectivite');
        $myCollectivite->setVariablesFusion($aData, $model->modelTemplateOdtInfos, 1);
    }

    /**
     *
     * @param type $MainPart
     * @param type $aData
     * @param type $data
     * @param type $types
     * @param type $correspondances
     * @version 5.1.0
     * @since 4.3.0
     */
    private function format(&$MainPart, &$aData, &$data, &$types, &$correspondances)
    {
        if (!empty($aData)) {
            foreach ($aData as $key => $value) {
                if (is_array($value) && !empty($value[0])) {
                    $this->formatIteration($MainPart, $key, $value);
                } elseif (isset($value['value']) && isset($value['type'])) {
                    $data[$key] = $value['value'];
                    $types[$key] = $value['type'];
                    $correspondances[$key] = $key;
                }
            }
        }
    }

    /**
     *
     * @param GDO_PartType $MainPart
     * @param String $iteration
     * @param array $aData
     * @version 5.1.0
     * @since 4.3.0
     */
    private function formatIteration(&$MainPart, $iteration, &$aData)
    {
        foreach ($aData as $key => $value) {
            $dataIteration = [];
            foreach ($value as $keyIteration => $valueIteration) {
                if (isset($valueIteration[0])) {
                    $this->formatIterationChild(
                        $dataIteration,
                        $keyIteration,
                        $valueIteration,
                        $typesIteration,
                        $correspondancesIteration
                    );
                } elseif (isset($valueIteration['type'])) {
                    $dataIteration[$keyIteration] = $valueIteration['value'];
                    $typesIteration[$iteration . '.' . $keyIteration] = $valueIteration['type'];
                    $correspondancesIteration[$iteration . '.' . $keyIteration] = $keyIteration;
                }
            }

            $aDataIteration[$iteration][] = $dataIteration;
        }

        Builder::iteration($MainPart, $iteration, $aDataIteration, $typesIteration, $correspondancesIteration);
    }

    /**
     *
     * @param type $dataIteration
     * @param type $iteration
     * @param type $aData
     * @param type $typesIteration
     * @param type $correspondancesIteration
     * @version 5.1.0
     * @since 4.3.0
     */
    private function formatIterationChild(
        &$dataIteration,
        $iteration,
        $aData,
        &$typesIteration,
        &$correspondancesIteration
    ) {
        foreach ($aData as $key => $value) {
            $dataIterationchild = [];
            foreach ($value as $keyIteration => $valueIteration) {
                if (isset($valueIteration[0])) {
                    $this->formatIterationChild(
                        $dataIterationchild,
                        $keyIteration,
                        $valueIteration,
                        $typesIteration,
                        $correspondancesIteration
                    );
                } elseif (isset($valueIteration['type'])) {
                    $dataIterationchild[$keyIteration] = $valueIteration['value'];
                    $typesIteration[$iteration . '.' . $keyIteration] = $valueIteration['type'];
                    $correspondancesIteration[$iteration . '.' . $keyIteration] = $keyIteration;
                }
            }
            $dataIteration[$iteration][] = $dataIterationchild;
        }
    }

    /**
     * [setVariablesFusionName description]
     * @param Model  $model [description]
     * @param [type] $aData [description]
     *
     * @version 5.2.0
     */
    private function setVariablesFusionName(Model &$model, &$aData)
    {
        $template = ClassRegistry::init('Template');

        // initialisations
        $this->modelTemplateFileOutputFormatValues = [
            'id' => $this->id,
            'templateName' => $this->modelTemplateName
        ];

        $model = ClassRegistry::init($model->name);
        if ($this->id !== null && method_exists($model, 'fileOutputFormatValues')) {
            $this->modelTemplateFileOutputFormatValues =
                array_merge($this->modelTemplateFileOutputFormatValues, $model->fileOutputFormatValues($this->id));
        }
    }
}
