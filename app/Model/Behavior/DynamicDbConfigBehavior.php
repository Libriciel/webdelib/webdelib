<?php
App::uses('ModelBehavior', 'Model');

class DynamicDbConfigBehavior extends ModelBehavior
{

    public function setup(Model $model, $config = array())
    {
        if (Configure::read('Config.database') !== null) {
            $model->useDbConfig = Configure::read('Config.database');
            $model->schemaName = ConnectionManager::getDataSource(Configure::read('Config.database'))->config['schema'];
        }
    }
}
