<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('CakeEventManager', 'Event');
App::uses('CakeEvent', 'Event');

///**
// * This behavior provides a way to version dynamic data by keeping versions
// * in a separate table linked to the original record from another one. Versioned
// * fields can be configured to override those in the main table when fetched or
// * put aside into another property for the same entity.
// *
// * If you want to retrieve all versions for each of the fetched records,
// * you can use the custom `versions` finders that is exposed to the table.
// */
/**
 * [VersionBehavior description]
 *
 * @SuppressWarnings(PHPMD)
 */
class VersionBehavior extends ModelBehavior
{
//
    /**
     * Table instance
     *
     * @var Model
     */
    protected $savedData = [];

    /**
     * Default config
     *
     * These are merged with user-provided configuration when the behavior is used.
     *
     * @var array
     */
    protected $default = [
        'implementedFinders' => ['versions' => 'findVersions'],
        'versionTable' => 'version',
        'versionField' => 'version_id',
        'additionalVersionFields' => ['created'],
        'fields' => null,
        'byteaFields' => null,
        'foreignKey' => 'foreign_key',
        'referenceName' => null,
        'onlyDirty' => false,
        'belongsTo' => false,
    ];

    /**
     * Constructor hook method.
     *
     * Implement this method to avoid having to overwrite
     * the constructor and call parent.
     *
     * @param array $config The configuration settings provided to this behavior.
     * @return void
     */
    public function setup(Model $model, $config = [])
    {
        $this->settings[$model->alias] = $config;
        $this->model = $model;

        if ($this->settings[$this->model->alias]['referenceName'] == null) {
            $this->settings[$this->model->alias]['referenceName'] = $this->referenceName();
        }
        $this->setupFieldAssociations($this->settings[$this->model->alias]['versionTable']);
    }

    /**
     * Creates the associations between the bound table and every field passed to
     * this method.
     *
     * Additionally it creates a `version` HasMany association that will be
     * used for fetching all versions for each record in the bound table
     *
     * @param string $table the table name to use for storing each field version
     * @return void
     */
    public function setupFieldAssociations($table)
    {
        $options = [
            'tableName' => $table
        ];
        $this->versionAssociation(null, $options);
    }

    /**
     * Returns association object for all versions or single field version.
     *
     * @param string|null $field Field name for per-field association.
     * @param array $options Association options.
     * @return array assocaitoon \Cake\ORM\Association => cake3
     */
    public function versionAssociation($field = null, $options = [])
    {
        $name = $this->associationName($this->model, $field);
        if (!in_array($name, $this->model->getAssociated('hasMany'), true)) {
            $options += [
                'className' => $this->settings[$this->model->alias]['referenceName'],
                'foreignKey' => $this->settings[$this->model->alias]['foreignKey'],
                'strategy' => 'subquery',
                'dependent' => true
            ];

            $this->model->bindModel(
                [
                    'hasMany' =>
                        [
                            $name => $options
                        ]
                ],
                false
            );
        }
        return $this->model->hasMany[$name];
    }

    /**
     * Retourne l'id de la version pour une donnée
     * @param Model $model
     * @param null $annexe
     * @return array
     *
     * @version 5.1.4
     * @since 5.1.0
     */
    public function getRevisionByModified(Model $model, $data = null)
    {
        $nombreRevision = $this->model->find(
            'count',
            [
              'conditions' => ['foreign_key' => $data['id']]
            ]
        );
        if (empty($nombreRevision)) {
            return null;
        }
        $db = $this->model->getDataSource();
        $version_id = $this->model->query(sprintf(
            "
            SELECT id AS ".$model->alias."
            FROM %s
            WHERE data_annexe -> 'Annexe' ->> 'modified' = '".$data['modified']."'
            AND data_annexe -> 'Annexe' ->> 'id' = '".$data['id']."'",
            $db->fullTableName($model->useTable."_versions")
        ));

        return Hash::extract($version_id, '{n}.{n}.annexe.0');
    }

    /**
     * Modifies the entity before it is saved so that versioned fields are persisted
     * in the database too.
     *
     * @param Model $model
     * @param array $options the options passed to the save method
     * @return boolean
     */
    public function beforeSave(Model $model, $options = [])
    {
        if ($this->settings[$model->alias]['referenceName'] == null) {
            $this->settings[$model->alias]['referenceName'] = $this->referenceName();
        }

        $this->setupFieldAssociations($this->settings[$model->alias]['versionTable']);

        $versionField = $this->settings[$model->alias]['versionField']; // name of version field

        if (!empty($versionField) && in_array($versionField, $model->getColumnTypes(), true)) {
            $model->set($this->settings[$model->alias]['versionField'], $this->getVersionId());
        }
        return true;
    }

    public function prepareRevision(Model $model, $created)
    {
        $entity = $model->find(
            'first',
            [
                'conditions' => [
                    $model->alias . '.' . 'id' => $model->id],
                'contain' => $this->settings[$model->alias]['contain'],
                'recursive' => -1]
        );

        $jsonData = $this->jsonEncode($model->alias, $entity, $this->settings[$model->alias]['byteaFields']);

        $data = [
                'version_id' => $created ? $this->getVersionId($model) : $this->getVersionId($model) + 1,
                'model' => $this->settings[$model->alias]['referenceName'],
                'user_id' => AuthComponent::user('id'),
                'data_' . strtolower($model->alias) => $jsonData,
            ] + $this->extractForeignKey($model)
            + $this->extraFields($model);
        $model->data[$this->associationName($model)] = $data;
        $this->savedData[$model->alias] = $model->data;
    }

    public function afterSave(Model $model, $created, $options = [])
    {
        $this->prepareRevision($model, $created);

        $this->updateBelongsTo($model);

        if (isset($this->savedData[$model->alias]) /*&&  !$created*/) {
            //On récuprère les associations à versionner => // : TODO fonction En fait
            foreach (array_keys($this->savedData[$model->alias]) as $association) {
                if (preg_match("/.*Version/", $association)) {
                    $model->$association->create();
                    $data = [
                        $association => $this->savedData[$model->alias][$association]
                    ];
                    $model->$association->save($data);
                    $versionId = $this->lastVersion($model, $model->id, ['id']);
                    $model->data[$association] += $versionId[$this->settings[$model->alias]['versionTable']];
                }
            }
        }
        return parent::afterSave($model, $created, $options); // TODO: Change the autogenerated stub
    }

    /**
     * Update the json field of the last Revision.
     * @param Model $model
     */
    public function updateLastRevision(Model $model)
    {
        $lastVersion =  $this->lastVersion($model);

        $entity = $model->find(
            'first',
            [
                'conditions' => [ 'Deliberation.id' => $model->id ],
                'contain' => $this->settings['Deliberation']['contain'],
                'recursive' => -1]
        );

        $jsonData = $this->jsonEncode($model->alias, $entity, $this->settings[$model->alias]['byteaFields']);

        $association = $this->associationName($model);
        $model->$association->save([
                'id'=> $lastVersion[$this->settings[$model->alias]['versionTable']]['id'],
                'data_' . strtolower($model->alias) => $jsonData,
            ]);
    }

    /**
     * Update belongsTo json data
     * Use it when adding a new entity
     * @param Model $model
     * @param null $foreignKey
     */
    public function updateBelongsTo(Model $model, $foreignKey = null)
    {
        if (!empty($this->settings[$model->alias]['belongsTo'])
            && $this->settings[$model->alias]['belongsTo']=='Deliberation'
        ) {
            $foreignKey = isset($foreignKey) ? $foreignKey : $model->field('foreign_key')   ;

            $belongsTo = strtolower(Inflector::pluralize($this->settings[$model->alias]['belongsTo']))."_versions";

            $versionModel = ClassRegistry::init($belongsTo);
            $lastVersion = $versionModel->find(
                'first',
                [
                    'fields' => ['id'],
                    'conditions' => [
                        'foreign_key' => $foreignKey,
                    ],
                    'order'=>['id'=>'desc']
                ]
            );

            $modelbelongsTo = ClassRegistry::init($this->settings[$model->alias]['belongsTo']);


            $entitybelongsTo = $modelbelongsTo->find(
                'first',
                [
                    'conditions' => [ 'Deliberation.id' => $foreignKey ],
                    'contain' => $this->settings['Deliberation']['contain'],
                    'recursive' => -1]
            );

            $jsonData = $this->jsonEncode(
                $modelbelongsTo->alias,
                $entitybelongsTo,
                $this->settings['Deliberation']['byteaFields']
            );

            if (isset($lastVersion[$belongsTo])) { //is ! multi delib
                $versionModel->save([
                    'id'=> $lastVersion[$belongsTo]['id'],
                    'data_' . strtolower($this->settings[$model->alias]['belongsTo']) => $jsonData,
                ]);
            }
        }
    }

    /**
     * return the last version id present in the version table 'model__version'
     *
     * @param \Cake\Datasource\EntityInterface $entity Entity.
     * @return int
     */
    public function getVersionId(Model $model)
    {
        $table = ClassRegistry::init($this->settings[$model->alias]['versionTable']);
        $default = 1;
        $preexistent= false;
        if ($model->id) {
            $preexistent = $table->find(
                'first',
                [
                    'fields' => ['version_id'],
                    'order' => ['id' => 'desc'],
                    'limit' => 1,
                    'conditions' => [
                            'model' => $this->settings[$model->alias]['referenceName'],
                        ] + $this->extractForeignKey($model)
                ]
            );
        }

        return (!$preexistent) ?
            $default :
            $preexistent[$this->settings[$model->alias]['versionTable']]['version_id'];
    }

    /**
     * Get the last version historized of the model and put it in _version property
     */
    public function lastVersion(Model $model, $id = null, $fields = [])
    {
        $versionModel = ClassRegistry::init($this->settings[$model->alias]['versionTable']);

        isset($id) ?: $id = $model->id ;
        $lastVersion = $versionModel->find(
            'first',
            [
                'fields'=> $fields,
                'conditions' => [
                    'foreign_key' => $id,
                ],
                'order' => ['id'=>'desc']
            ]
        );

        ((isset($fields['data_version']) || empty($fields))
            && isset($lastVersion[$this->settings[$model->alias]['versionTable']]))
                ? $lastVersion[$this->settings[$model->alias]['versionTable']]['data_version'] =
            json_decode(
                $lastVersion[$this->settings[$model->alias]['versionTable']]['data_' . strtolower($model->alias)],
                true
            )
                : null;
        return $lastVersion;
    }


    /**
     * Get the last version historized of the model and put it in _version property
     */
    public function version(Model $model, $id)
    {
        $this->model = $model;
        $versionModel = ClassRegistry::init($this->settings[$this->model->alias]['versionTable']);
        $version = $versionModel->find(
            'first',
            [
                'conditions' => [
                    'id' => $id
                ],
            ]
        );

        $version[$this->settings[$this->model->alias]['versionTable']]['data_version'] =
            json_decode(
                $version[
                    $this->settings[$this->model->alias]['versionTable']]['data_' . strtolower($this->model->alias)
                ],
                true
            );
        return $version;
    }

    /**
     *
     */
    public function versions(Model $model, $id)
    {
        $this->model = $model;
        $versionModel = ClassRegistry::init($this->settings[$this->model->alias]['versionTable']);

        $versionModel->bindModel(
            [
                'belongsTo' =>
                    [
                        'User' => [
                            'className' => 'User',
                            'conditions' => '',
                            'order' => '',
                            'dependent' => false,
                            'foreignKey' => 'user_id']
                    ]
            ],
            false
        );

        $versions = $versionModel->find(
            'all',
            [
                'contain' => ['User.username', 'User.nom', 'User.prenom',],
                'conditions' => [
                    'foreign_key' => $id,
                ],
                'order' => ['version_id desc']
            ]
        );

        return $versions;
    }


    protected function unsetVersionFields(array &$data)
    {
        foreach ($data as $field => $content) {
            if (!in_array($field, $this->settings[$this->model->alias]['byteaFields'], true)) {
                unset($data[$field]);
            }
        }
    }

    /**
     * Returns an array of fields to be versioned.
     *
     * @return array
     */
    protected function fields()
    {
        $fields = array_keys($this->model->schema());
        return $fields;
    }

    protected function extraFields($model)
    {
        $extraField = [];
        foreach ($this->settings[$model->alias]['byteaFields'] as $field => $name) {
            if (!in_array($name, array_keys($model->getAssociated()), true) && !is_array($name)) {
                $extraField[$name] = $model->field($name);
            }
        }
        return $extraField;
    }

    /**
     * Returns an array with foreignKey value.
     *
     * @param \Cake\Datasource\EntityInterface $entity Entity.
     * @return array
     */
    protected function extractForeignKey(Model $model)
    {
        $foreignKey = (array)$this->settings[$this->model->alias]['foreignKey'];
        if ($model->id) {
            $primaryKey = $model->field('id');
            $pkValue = $model->find('list', ["fields" => $model->primaryKey, 'conditions' => ['id' => $primaryKey]]);
        } else {
            $modelTmp = $model->find('first', ["fields" => 'MAX('.$model->alias.'.id) as id']);
            $primaryKey =  isset($modelTmp[0]['id']) ? $modelTmp[0]['id'] + 1 : 1;
            $pkValue = ['foreign_key'=>$primaryKey];
        }
        return array_combine($foreignKey, $pkValue);
    }

    /**
     * Returns default version association name.
     *
     * @param string $field Field name.
     * @return string
     */
    protected function associationName(Model $model, $field = null)
    {
        $alias = Inflector::singularize($model->alias);
        if ($field) {
            $field = Inflector::camelize($field);
        }

        return $alias . $field . 'Version';
    }

    private function jsonEncode($alias, array $data, array $specificData)
    {

        //Unset specific fields like bytea fiedls
        foreach ($specificData as $field => $content) {
            if (is_array($content)) {
                $num = 0;
                if (!empty($content)) {
                    foreach ($content as $item => $value) {
                        isset($data[$field][$num++][$value])
                            ? $data[$field][$num++][$value] = "" : $data[$field][$value] = "";
                    }
                } else {
                    unset($data[$field]);
                }
            } else {
                unset($data[$alias][$content]);
            }
        }
        return json_encode($data);
    }

    /**
     * Returns reference name for identifying this model's records in version table.
     *
     * @return string
     */
    protected function referenceName()
    {
        return Inflector::camelize($this->model->table);
    }
}
