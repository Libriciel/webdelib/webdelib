<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [DeliberationSeance description]
 *
 * @version 5.1.0
 * @since 4.3.0
 *
 * @SuppressWarnings(PHPMD)
 */
class DeliberationSeance extends AppModel
{
    /**
     * [public description]
     * @var [type]
     */
    public $name = 'DeliberationSeance';
    /**
     * [public description]
     * @var [type]
     */
    public $useTable = 'deliberations_seances';
    /**
     * [public description]
     * @var [type]
     */
    public $belongsTo = ['Deliberation', 'Seance'];
    /**
     * [public description]
     * @var [type]
     */
    public $validate = [
        'seance_id' => [
             [
                'rule' => ['canSaveSeanceDeliberante'],
                'message' => 'Vous ne pouvez affecter le projet que sur une seule séance délibérante'
              ],
        ],
    ];

    /**
     * @version 4.3
     * @access public
     * @param type $query
     * @return type
     */
    public function beforeFind($query = [])
    {
        //Gestion des droits sur les types de seances
        if (!empty($query['allow'])) {
            $query['conditions'] = (is_array($query['conditions'])) ? $query['conditions'] : [];
            $query['joins'] = $query['joins'] + [
              $this->join('Seance', ['type' => 'INNER']),
              $this->Seance->join('Typeseance', ['type' => 'INNER'])
            ];

            //Gestion des droits sur les types d'actes
            $conditions = parent::allowAcl($query['allow'], 'Typeseance', 'Typeseance.id');
            if ($conditions !== false) {
                $query['conditions'] = array_merge($query['conditions'], $conditions);
            }
        }
        return $query;
    }
    /**
     * Suppression d'une seance par rapport à une délibération
     *
     * @access public
     *
     * @param int $delib_id --> id de délibération
     * @param int $seance_id --> id de séance
     *
     * @version 5.1.0
     * @since 4.1.3
     */
    public function deleteDeliberationSeance($delib_id, $seance_id)
    {
        //On récupère l'id de DeliberationSeance à supprimer
        $DeliberationSeance = $this->find('first', [
            'fields' => ['DeliberationSeance.id'],
            'joins' => [$this->join('Deliberation', ['type' => 'INNER'])],
            'conditions' => [
                'DeliberationSeance.seance_id' => $seance_id,
                'deliberation_id' => $delib_id,
                'Deliberation.etat !=' => -1],
            'recursive' => -1,
        ]);

        $this->delete($DeliberationSeance['DeliberationSeance']['id']);

        $multiDelibs = $this->Deliberation->getMultidelibs($delib_id);
        foreach ($multiDelibs as $multiDelib_id) {
            $this->deleteMultiDelib($multiDelib_id, $seance_id);
        }

        $this->reOrdonne($seance_id);
    }

    /**
     * Suppression d'une seance par rapport à une délibération
     *
     * @access protected
     *
     * @param int|string $delib_id
     * @param int|string $seance_id
     *
     * @version 5.1.0
     * @since 4.1.3
     */
    protected function deleteMultiDelib($delib_id, $seance_id)
    {
        //On récupère l'id de DeliberationSeance à supprimer
        $DeliberationSeance = $this->find('first', [
            'conditions' => [
                'seance_id' => $seance_id,
                'deliberation_id' => $delib_id,
                'Deliberation.etat !=' => -1
            ],
            'fields' => ['DeliberationSeance.id']]);
        if (!empty($DeliberationSeance)) {
            $this->delete($DeliberationSeance['DeliberationSeance']['id']);
        }

        $this->reOrdonne($seance_id);
    }

    /**
     * Ajout d'une seance par rapport à une délibération
     *
     * @access public
     *
     * @param int $delib_id --> id le l'acte
     * @param int $seance_id --> id de la séance
     *
     * @version 5.1.0
     * @since 4.1.3
     */
    public function addDeliberationSeance($delib_id, $seance_id)
    {

        //Vérifier qu'il n'existe pas déjà avant de l'ajouter à une séance
        // Si on définit une séance pour une délibération, on la place en dernière position de la séance
        if (!$this->existsByDeliberationSeance($delib_id, $seance_id)) {
            $DeliberationSeance['position'] = intval($this->getLastPosition($seance_id));
            $DeliberationSeance['deliberation_id'] = $delib_id;
            $DeliberationSeance['seance_id'] = $seance_id;

            $this->create($DeliberationSeance);
            $this->save();
        }

        $multiDelibs = $this->Deliberation->getMultidelibs($delib_id);
        foreach ($multiDelibs as $multiDelib_id) {
            $this->addMultiDelib($seance_id, $delib_id, $multiDelib_id);
        }

        $this->reOrdonne($seance_id);
    }

    /**
     * Ajout d'une seance par rapport à une délibération
     *
     * @access protected
     *
     * @param int $seance_id --> id de la séance
     * @param type $parentId
     * @param int $delib_id --> id le l'acte
     *
     * @version 5.1.0
     * @since 4.1.3
     */
    protected function addMultiDelib($seanceId, $parentId, $delibId)
    {
        $position = intval($this->getLastPositionMultidelibByParent($seanceId, $parentId));
        $position++;
        $this->decaleMultiDelib($seanceId, $position);

        $DeliberationSeance['position'] = $position;
        $DeliberationSeance['deliberation_id'] = $delibId;
        $DeliberationSeance['seance_id'] = $seanceId;
        $this->create($DeliberationSeance);
        $this->save();
    }

    /**
     * Retourne la position la plus haute d'une séance
     *
     * @access protected
     *
     * @param int $seance_id --> id de la séance
     * @return type
     *
     * @version 5.1.0
     * @since 4.1.3
     */
    protected function getLastPosition($seance_id)
    {
        $deliberations = $this->find('first', [
            'fields' => ['MAX (DeliberationSeance.position) as position'],
            'conditions' => ['Seance.id' => $seance_id,
                'Deliberation.etat !=' => -1]]);

        return($deliberations[0]['position'] + 1);
    }

    /**
     * Retourne la position la plus haute d'une délibération par raport à une séance et sa délibération parent
     *
     * @access protected
     *
     * @param int $seance_id --> id de la séance
     * @param type $parent_id
     * @return int --> Retourne la position du dernier enregistrement
     *
     * @version 5.1.0
     * @since 4.1.3
     */
    protected function getLastPositionMultidelibByParent($seance_id, $parent_id)
    {
        $deliberations = $this->find('first', [
            'fields' => ['MAX (DeliberationSeance.position) as position'],
            'recursive' => -1,
            'joins' => [
                ['table' => 'deliberations',
                    'alias' => 'Deliberation_parent',
                    'type' => 'inner',
                    'conditions' => [
                        'Deliberation_parent.parent_id =  DeliberationSeance.deliberation_id'
                    ]
                ],
                ['table' => 'deliberations',
                    'alias' => 'Deliberation_fils',
                    'type' => 'inner',
                    'conditions' => [
                        'Deliberation_fils.id = DeliberationSeance.deliberation_id'
                    ]
                ],
            ],
            'conditions' => ['seance_id' => $seance_id,
                'DeliberationSeance.deliberation_id' => $parent_id]
        ]);

        return($deliberations[0]['position']);
    }

    /**
     * Re-ordonne la séance passé en paramètre
     *
     * @access public
     *
     * @param int $seance_id --> id de la séance
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function reOrdonne($seance_id)
    {
        //La position par default
        $position = 1;
        //Fix l'état est-il indipensable
        //On recherche toute les délibérations de la séance par ordre de classement
        $deliberations = $this->find('all', [
            'fields' => [
              'DeliberationSeance.id',
              'DeliberationSeance.deliberation_id',
              'DeliberationSeance.position'
            ],
            'joins' => [
              $this->join('Deliberation', ['type' => 'INNER', 'conditions' => ['Deliberation.etat !=' => -1]]),
            ],
            'conditions' => [
              'DeliberationSeance.seance_id' => $seance_id,
            ],
            'order' => ['DeliberationSeance.position' => 'ASC'],
            'recursive' => -1
          ]);

        // Reclasser l'odre pour toutes les délibérations de la séance passé en paramètre
        foreach ($deliberations as $delib) {
            if ($position != $delib['DeliberationSeance']['position']) {
                $this->save(
                    [
                        'id' => $delib['DeliberationSeance']['id'],
                        'deliberation_id' => $delib['DeliberationSeance']['deliberation_id'],
                        'seance_id' => $seance_id,
                        'position' => $position],
                    [
                        'validate' => false,
                        'callbacks' => false
                    ]
                );
            }
            $position++;
        }
    }

    /**
     * Re-ordonne les multidélibérations suite au modification de la mère
     *
     * @access protected
     *
     * @param int $seanceId --> id de la séance
     * @param type $pointeur
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    protected function decaleMultiDelib($seanceId, $pointeur)
    {

        //La position par default
        $position = 1;

        //On recherche toute les délibérations de la séance par ordre de classement
        $seances = $this->find('all', [
            'fields' => [
                'DeliberationSeance.id',
                'DeliberationSeance.deliberation_id',
                'DeliberationSeance.position'
              ],
              'conditions' => [
                'Seance.id' => $seanceId,
                'Deliberation.etat !=' => -1],
            'order' => ['DeliberationSeance.position' =>  'ASC'],
            'recursive' => 1
        ]);

        // Reclasser l'odre pour toutes les délibérations de la séance passé en paramètre
        foreach ($seances as $delib) {
            if ($position != $delib['DeliberationSeance']['position'] or $position != $pointeur) {
                $this->save(['id' => $delib['DeliberationSeance']['id'],
                    'deliberation_id' => $delib['DeliberationSeance']['deliberation_id'],
                    'seance_id' => $seanceId,
                    'position' => $position], ['validate' => false,
                    'callbacks' => false]);
            }
            $position++;
        }
    }

    /**
     * Pour toutes les délibérations d'une séance délibérante donnée, mettre en tête (position/ordre) ces délibs
     * dans toutes les autres séances dans lesquels ils sont référencés et repousse l'ordre des autres en fin de liste
     *
     * @access public
     *
     * @param integer $seance_id --> séance délibérante contenant les projets à reporter
     * @return boolean --> true si succès des sauvegarde des nouvelles valeurs position
     * des DeliberationSeance, false sinon
     *
     * @version 5.1.0
     * @since 4.1.3
     */
    public function reportePositionsSeanceDeliberante($seance_id)
    {
        $success = true;

        // Récupération des délibérations à reporter (attachée à cette séance)
        $delibsToReport = $this->find('all', [
            'recursive' => -1,
            'fields' => ['deliberation_id'],
            'order' => ['position' => 'ASC'],
            'conditions' => ['seance_id' => $seance_id]
        ]);

        $delibIdsToReport = Hash::extract($delibsToReport, '{n}.DeliberationSeance.deliberation_id');

        // Récupération des séances dans lesquelles apparaissent les projets à reporter (non délibérantes)
        $seancesWhichContainDelibs = $this->find('all', [
            'recursive' => -1,
            'fields' => ['DISTINCT seance_id'],
            'conditions' => [
                'deliberation_id' => $delibIdsToReport,
                'seance_id <>' => $seance_id]]);

        $seanceIdsWhichContainDelibs =
            Hash::extract($seancesWhichContainDelibs, '{n}.DeliberationSeance.seance_id');

        // Filtrer que les séances non traitées
        $seancesToReorder = $this->Seance->find('all', [
            'recursive' => -1,
            'fields' => ['id'],
            'conditions' => [
                'id' => $seanceIdsWhichContainDelibs,
                'traitee' => 0]]);

        $seanceIdsToReorder = Hash::extract($seancesToReorder, '{n}.Seance.id');

        // Pour chaque séances concernées par le report
        foreach ($seanceIdsToReorder as $seanceId) {
            // Récupération des delibs qui font parti de la séance délibérante pour cette séance
            $delibsToReportThisSeance = $this->find('all', [
                'recursive' => -1,
                'fields' => ['id', 'deliberation_id'],
                'order' => ['position' => 'ASC'],
                'conditions' => [
                    'seance_id' => $seanceId,
                    'deliberation_id' => $delibIdsToReport]]);

            // Récupération des delibs qui ne font pas parti de la séance délibérante pour cette séance
            $delibsToPushThisSeance = $this->find('all', [
                'recursive' => -1,
                'fields' => ['id', 'deliberation_id'],
                'order' => ['position' => 'ASC'],
                'conditions' => [
                    'seance_id' => $seanceId,
                    'NOT' => ['deliberation_id' => $delibIdsToReport]]]);

            $position = 1;

            // Avance la position des délibs à reporter
            foreach ($delibIdsToReport as $delibId) {
                foreach ($delibsToReportThisSeance as $delibToReport) {
                    if ($delibId == $delibToReport['DeliberationSeance']['deliberation_id']) {
                        $delibToReport['DeliberationSeance']['position'] = $position;
                        $success = $success && $this->save($delibToReport);
                        $position++;
                        break;
                    }
                }
                if ($position > count($delibsToReportThisSeance)) {
                    break;
                }
            }

            //Repousse la position des autres délibs
            foreach ($delibsToPushThisSeance as $delib) {
                $delib['DeliberationSeance']['position'] = $position;
                $success = $success && $this->save($delib);
                $position++;
            }
        }

        return $success;
    }

    /**
     * Vérifie si un couple seance et délibération existe
     *
     * @access public
     *
     * @param int $delib_id --> id le l'acte
     * @param int $seance_id --> id de la séance
     * @return boolean
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function existsByDeliberationSeance($delib_id, $seance_id)
    {
        $DeliberationSeance = $this->findAllBySeance_idAndDeliberation_id($seance_id, $delib_id);

        if (empty($DeliberationSeance)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Fonction d'initialisation des variables de fusion pour les avis de
     * toutes les séances pour un projet en cours de génération
     *
     * @access public
     *
     * @param type $aData
     * @param object_by_ref $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param type $deliberationId
     * @param type $seanceId
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function setVariablesFusionPourUnProjet(&$aData, &$modelOdtInfos, $deliberation_id)
    {


        // lecture en base de données
        $DeliberationSeances = $this->find('all', [
            'fields' => [
                'avis', 'reset','commentaire','Seance.date', 'Seance.type_id',
                'Typeseance.libelle', 'Typeseance.action'
            ],
            'joins' => [
                    $this->join('Seance', ['type' => 'INNER']),
                    $this->Seance->join('Typeseance', ['type' => 'INNER']),
                ],
            'conditions' => [
                'DeliberationSeance.deliberation_id' => $deliberation_id,
                'OR' => [
                    'Typeseance.action' => ['1','2']
                ]],
            'order' => ['date' => 'DESC'],
            'recursive' => -1]);
        $AvisProjet = [];
        if (empty($DeliberationSeances)) {
            if ($modelOdtInfos->hasUserFieldDeclared('avis')) {
                $AvisProjet['avis'] = ['value' => '', 'type' => 'text'];
            }
            if ($modelOdtInfos->hasUserFieldDeclared('AvisProjet_seance_type_action')) {
                $AvisProjet['AvisProjet_seance_type_action'] = ['value' => '', 'type' => 'text'];
            }
            if ($modelOdtInfos->hasUserFieldDeclared('AvisProjet_seance_type_libelle')) {
                $AvisProjet['AvisProjet_seance_type_libelle'] = ['value' => '', 'type' => 'text'];
            }
            if ($modelOdtInfos->hasUserFieldDeclared('AvisProjet_seance_date')) {
                $AvisProjet['AvisProjet_seance_date'] = ['value' => '', 'type' => 'text'];
            }
            if ($modelOdtInfos->hasUserFieldDeclared('avis_favorable')) {
                $AvisProjet['avis_favorable'] = ['value' => '', 'type' => 'text'];
            }
            if ($modelOdtInfos->hasUserFieldDeclared('commentaire')) {
                $AvisProjet['commentaire'] = ['value' => '', 'type' => 'text'];
            }

            $aData['AvisProjet'][] = $AvisProjet;
            return;
        }
        // fusion des variables
        foreach ($DeliberationSeances as $DeliberationSeance) {
            $seance_type_libelle = $this->Seance->Typeseance->field(
                'libelle',
                ['id' => $DeliberationSeance['Seance']['type_id']]
            );
            $seance_date = CakeTime::i18nFormat($DeliberationSeance['Seance']['date'], '%d/%m/%Y');
            if ($modelOdtInfos->hasUserFieldDeclared('avis')) {
                if ($DeliberationSeance['DeliberationSeance']['reset'] === true) {
                    $avisTexte = '';
                } elseif ($DeliberationSeance['DeliberationSeance']['avis'] === true) {
                    $avisTexte = 'A reçu un avis favorable';
                } elseif ($DeliberationSeance['DeliberationSeance']['avis'] === false) {
                    $avisTexte = 'A reçu un avis défavorable';
                } else {
                    $avisTexte = '';
                }

                $avis = empty($avisTexte) ? '' : __(
                    '%s en %s du %s',
                    $avisTexte,
                    $seance_type_libelle,
                    $seance_date
                );
                $AvisProjet['avis'] = ['value' => $avis, 'type' => 'text'];
            }
            if ($modelOdtInfos->hasUserFieldDeclared('AvisProjet_seance_type_action')) {
                $AvisProjet['AvisProjet_seance_type_action'] = [
                    'value' => $DeliberationSeance['Typeseance']['action'], 'type' => 'text'
                ];
            }
            if ($modelOdtInfos->hasUserFieldDeclared('AvisProjet_seance_type_libelle')) {
                $AvisProjet['AvisProjet_seance_type_libelle'] = ['value' => $seance_type_libelle, 'type' => 'text'];
            }
            if ($modelOdtInfos->hasUserFieldDeclared('AvisProjet_seance_date')) {
                $AvisProjet['AvisProjet_seance_date'] = ['value' => $seance_date, 'type' => 'date'];
            }
            if ($modelOdtInfos->hasUserFieldDeclared('avis_favorable')) {
                $AvisProjet['avis_favorable'] = [
                    'value' => $DeliberationSeance['DeliberationSeance']['avis'] == true ? '1' : '0',
                    'type' => 'text'
                ];
            }
            if ($modelOdtInfos->hasUserFieldDeclared('commentaire')) {
                $AvisProjet['commentaire'] = [
                    'value' => $DeliberationSeance['DeliberationSeance']['commentaire'], 'type' => 'lines'
                ];
            }

            $aData['AvisProjet'][] = $AvisProjet;
        }
    }

    /**
     *   Fonction d'initialisation des variables de fusion pour un seul avis de la séance en cours de génération
     *
     * @access public
     *
     * @param array $aData
     * @param object_by_ref $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param type $acteId
     * @param type $seanceId
     * @return type
     *
     * @version 5.1.0
     * @since 4.1.3
     */
    public function setVariablesFusionAvis(&$aData, &$modelOdtInfos, $acte_id, $seance_id)
    {
        // lecture en base de données
        $DeliberationSeance = $this->find('first', [
            'fields' => ['id', 'avis','reset', 'commentaire'],
            'conditions' => [
                'DeliberationSeance.deliberation_id' => $acte_id,
                'DeliberationSeance.seance_id' => $seance_id,
            ],
            'recursive' => -1]);

        $aProjetAvis = [];
        if (empty($DeliberationSeance)) {
            if ($modelOdtInfos->hasUserFieldDeclared('ProjetAvis_avis')) {
                $aProjetAvis['ProjetAvis_avis'] = ['value' => '', 'type' => 'text'];
            }
            if ($modelOdtInfos->hasUserFieldDeclared('ProjetAvis_favorable')) {
                $aProjetAvis['ProjetAvis_favorable'] = ['value' => '', 'type' => 'text'];
            }
            if ($modelOdtInfos->hasUserFieldDeclared('ProjetAvis_commentaire')) {
                $aProjetAvis['ProjetAvis_commentaire'] = ['value' => '', 'type' => 'text'];
            }

            $aData['ProjetAvis'][] = $aProjetAvis;
            return;
        }

        // fusion des variables
        if ($DeliberationSeance['DeliberationSeance']['avis'] === true) {
            $avisTexte = 'A reçu un avis favorable';
        } elseif ($DeliberationSeance['DeliberationSeance']['reset'] === true) {
            $avisTexte = '';
        } elseif ($DeliberationSeance['DeliberationSeance']['avis'] === false) {
            $avisTexte = 'A reçu un avis défavorable';
        } else {
            $avisTexte = '';
        }

        if ($modelOdtInfos->hasUserFieldDeclared('ProjetAvis_avis')) {
            $aProjetAvis['ProjetAvis_avis'] = ['value' => $avisTexte, 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('ProjetAvis_favorable')) {
            $aProjetAvis['ProjetAvis_favorable'] = [
                'value' => $DeliberationSeance['DeliberationSeance']['avis'] == true ? '1' : '0', 'type' => 'text']
            ;
        }
        if ($modelOdtInfos->hasUserFieldDeclared('ProjetAvis_commentaire')) {
            $aProjetAvis['ProjetAvis_commentaire'] = [
                'value' => $DeliberationSeance['DeliberationSeance']['commentaire'], 'type' => 'lines'
            ];
        }
        $aData['ProjetAvis'][] = $aProjetAvis;
    }

    /**
     * Ordonne la séance passé en paramètre par un tableau d'Ids
     *
     * @access public
     *
     * @param int $seance_id --> id de la séance
     * @param array $sortby --> id de la séance
     *
     * @version 5.1.0
     * @since 4.3.0
     */
    public function ordonneSeanceByValue($seance_id, $sortby)
    {
        // Critere de tri
        switch ($sortby) {
            case 'theme_id':
                $contain = $sortby_champ = 'Theme.order';
                break;
            case 'service_id':
                $contain = $sortby_champ = 'Service.order';
                break;
            case 'rapporteur_id':
                $contain = $sortby_champ = 'Rapporteur.nom';
                break;
            default:
                $sortby_champ = $sortby;
                break;
        }

        $deliberations = $this->Deliberation->find('list', [
            'conditions' => ['Deliberation.id' => $this->Seance->getDeliberationsId($seance_id)],
            'fields' => ['Deliberation.id'],
            'contain' => (!empty($contain) ? [$contain] : null),
            'order' => ["$sortby_champ  ASC"],
            'recursive' => -1
        ]);

        $i = 1;
        foreach ($deliberations as $delib_id) {
            $this->recursive = -1;
            $DeliberationSeance =
                $this->findBySeanceIdAndDeliberationId($seance_id, $delib_id, 'DeliberationSeance.id');

            $this->id = $DeliberationSeance['DeliberationSeance']['id'];
            $this->saveField('position', $i++);
        }
    }

    /**
     * [saveDeliberationSeance description]
     * @param  [type] $data [description]
     * @return [type]       [description]
     *
     * @version 5.1.0
     */
    public function saveDeliberationSeance($data)
    {
        $sucess = true;
        //Vérification des types de séances
        if (!empty($data['DeliberationSeance'])) {
            $nbrSeanceDeliberante = $this->nbrSeanceDeliberante($data);
            if ($nbrSeanceDeliberante > 1) {
                $this->validationErrors[$this->alias] = Set::extract('/seance_id/message', $this->validate);
                return false;
            }
        }

        if (!empty($data['Deliberation']['id']) && isset($data['DeliberationSeance'])) {
            if (!empty($data['DeliberationSeance'])) {
                foreach ($data['DeliberationSeance'] as $key => $deliberationSeance) {
                    $data['DeliberationSeance'][$key] = [
                        'deliberation_id' => $data['Deliberation']['id'],
                        'seance_id' => $deliberationSeance,
                    ];
                }
                //debug($data['DeliberationTypeseance']);
                ///$debug = $this->saveMany($data['DeliberationTypeseance'], ['validate'=>'only','atomic'=>false]);
                // var_dump($this->validationErrors);
                $selectedSeanceIds = Set::extract('/seance_id', $data['DeliberationSeance']);
            }

            //Supression des séances
            $nbr_seances_not_read = $this->find(
                'count',
                [
                  'conditions' => ['deliberation_id' => $data['Deliberation']['id']],
                  'recursive' => -1,
                  'allow' => ['seance_id'=> 'read <>'],
                ]
            );
            if (empty($data['DeliberationSeance']) && $nbr_seances_not_read === 0) {
                $this->deleteAll(['deliberation_id' => $data['Deliberation']['id']]);
            } elseif (empty($data['DeliberationSeance']) && $nbr_seances_not_read > 0) {
                $seances_a_retirer = $this->find(
                    'list',
                    [
                      'fields' => ['seance_id'],
                      'conditions' => [
                        'deliberation_id' => $data['Deliberation']['id']],
                      'recursive' => -1,
                      'allow' => ['seance_id'=> 'read'],
                    ]
                );
                foreach ($seances_a_retirer as $seance_id) {
                    $this->deleteDeliberationSeance($data['Deliberation']['id'], $seance_id);
                }
            }

            if (!empty($data['DeliberationSeance'])) {
                //On récupére les séances dont l'utilisateur peut avoir accès
                //pour ne pas supprimer les séances pour lesquelles il n'a pas d'accès
                $deliberationSeance_reads = $this->find('all', [
                    'fields' => ['deliberation_id', 'id', 'position', 'seance_id'],
                    'conditions' => ['deliberation_id' => $data['Deliberation']['id']],
                    'joins' => [
                        $this->join('Seance', ['type' => 'INNER']),
                        $this->Seance->join('Typeseance', ['type' => 'INNER'])
                    ],
                    'order' => ['position'=>'ASC'],
                    'recursive' => -1,
                    'allow' => ['Typeseance.id'=> 'read']
                  ]);

                $seances_enregistrees  = [];
                if (!empty($deliberationSeance_reads)) {
                    foreach ($deliberationSeance_reads as $deliberationSeance_read) {
                        $seances_enregistrees[] = $deliberationSeance_read['DeliberationSeance']['seance_id'];
                    }
                    $seances_a_retirer = array_diff($seances_enregistrees, $selectedSeanceIds);
                    foreach ($seances_a_retirer as $seance_id) {
                        $this->deleteDeliberationSeance($data['Deliberation']['id'], $seance_id);
                    }
                }

                if (!empty($seances_enregistrees) && is_array($seances_enregistrees)) {
                    $seances_a_ajouter = array_diff($selectedSeanceIds, $seances_enregistrees);
                } else {
                    $seances_a_ajouter = $selectedSeanceIds;
                }

                foreach ($seances_a_ajouter as $seance_id) {
                    $this->addDeliberationSeance($data['Deliberation']['id'], $seance_id);
                }
            }
        }

        return $sucess;
    }

    /**
     * [saveDeliberationSeanceDelibRattachees description]
     * @param  [type] $projetParentId [description]
     * @param  [type] $projetId       [description]
     * @return [type]                 [description]
     *
     * @version 5.1.0
     */
    public function saveDeliberationSeanceDelibRattachees($projetParentId, $projetId)
    {
        $sucess = true;

        $this->recursive = -1;
        $parentSeances = $this->findAllByDeliberationId($projetParentId, ['seance_id']);
        $seances = $this->findAllByDeliberationId($projetId, ['seance_id']);

        if (empty($parentSeances)) {
            $this->deleteAll(['deliberation_id' => $projetId]);
            return $sucess;
        }

        $parentSeanceIds = Hash::extract($parentSeances, '{n}.DeliberationSeance.seance_id');
        $seanceIds = Hash::extract($seances, '{n}.DeliberationSeance.seance_id');

        $seancesARetirer = array_diff($seanceIds, $parentSeanceIds);
        foreach ($seancesARetirer as $seanceId) {
            $this->deleteMultiDelib($seanceId, $projetParentId, $projetId);
        }

        $seancesAAjouter = array_diff($parentSeanceIds, $seanceIds);
        foreach ($seancesAAjouter as $seanceId) {
            $this->addMultiDelib($seanceId, $projetParentId, $projetId);
        }

        return $sucess;
    }


    /**
     * [nbrSeanceDeliberante description]
     *
     * @access private
     *
     * @param  [type] $data [description]
     * @return [type]       [description]
     *
     * @version 5.1.0
     */
    private function nbrSeanceDeliberante($data)
    {
        $count = 0;

        foreach ($data['DeliberationSeance'] as $seance_id) {
            if ($this->Seance->isDeliberante($seance_id)) {
                $count++;
            }
        }
        return $count;
    }

    /**
     * Vérification séance délibérante
     *
     * @access public
     *
     * @param array $seances
     * @return boolean
     *
     * @version 5.1.0
     */
    public function canSaveSeanceDeliberante()
    {
        if (!empty($this->data['DeliberationSeance']['seance_id'])
            && !empty($this->data['DeliberationSeance']['deliberation_id'])
        ) {
            // Hors "nouvelle" séance la délibération appartient-elle à une séance délibérante ?
            $query = [
                'fields' => ['id'],
                'joins' => [
                    $this->join('Seance', [
                        'type' => 'INNER'
                    ]),
                    $this->Seance->join('Typeseance', [
                        'type' => 'INNER',
                        'conditions' => ['action' => 0]
                    ]),
                ],
                'conditions' => [
                    'seance_id <>' => $this->data['DeliberationSeance']['seance_id'],
                    'deliberation_id' => $this->data['DeliberationSeance']['deliberation_id']
                ],
                'recursive' => -1
            ];
            $nbrDeliberante = $this->find('count', $query);

            $query = [
                'contain' => false,
                'joins' => [
                    $this->Seance->join('Typeseance', ['type' => 'INNER'])
                ],
                'conditions' => [
                    'Seance.id' => $this->data['DeliberationSeance']['seance_id'],
                    'Typeseance.action' => 0
                ]
            ];
            $nbrDeliberante += $this->Deliberation->Seance->find('count', $query);

            return !($nbrDeliberante > 1);
        }

        return true;
    }
}
