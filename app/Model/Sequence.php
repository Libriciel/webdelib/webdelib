<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class Sequence extends AppModel
{
    public $name = 'Sequence';
    public $displayField = "nom";
    public $validate = [
        'nom' => [
            [
                'rule' => 'notBlank',
                'message' => 'Entrer un nom pour la séquence'
            ],
            [
                'rule' => 'isUnique',
                'message' => 'Entrer un autre nom, celui-ci est déjà utilisé.'
            ]
        ],
        'num_sequence' => [
            'rule' => 'numeric',
            'allowEmpty' => true,
            'message' => 'Le numéro de séquence doit être un nombre.'
        ]
    ];
    public $hasMany = 'Compteur';
    public $cacheQueries = false;

    /**
     * Retourne une séance par iD
     * @param type $id
     * @return type
     * @throws NotFoundException
     * @since 5.1.2
     */
    public function findById($id)
    {
        $sequence = $this->find('first', [
          'conditions' => ['id' => $id],
          'recursive' => -1
        ]);
        if (empty($sequence)) {
            throw new NotFoundException(__('ID:"%s" de sequence invalide', $id));
        }
        return $sequence;
    }

    /**
     *
     * @param type $date
     * @param type $def_reinit
     * @param type $sequenceId
     * @return int
     *
     * @since 5.1.2
     */
    public function getNextValue($date, $def_reinit, $sequenceId)
    {
        $this->recursive = -1;
        $sequence = $this->findById($sequenceId);
        $reinit = $this->checkIfReinit(
            $sequence['Sequence']['debut_validite'],
            $sequence['Sequence']['fin_validite'],
            $def_reinit,
            $date
        );
        if ($reinit) {
            $this->reInit($sequence['Sequence']['id'], $def_reinit, $date);
            return 1;
        } else {
            $this->increment($sequence['Sequence']['id'], $sequence['Sequence']['num_sequence']);
            return $sequence['Sequence']['num_sequence'] +1;
        }
    }

    /**
     * Repasse à 1 la valeur séance donnée et modifie la val_reinit
     * @param type $id
     * @param type $val_reinit
     * @throws RuntimeException
     * @since 5.1.2
     */

    public function reInit($id, $defReinit, $date)
    {
        $data = $this->calculeNouvellesDates($defReinit, $date);
        $data['id']=$id;
        $data["num_sequence"]=1;
        $saved = $this->save($data);
        if ($saved === false) {
            throw new RuntimeException(
                __(
                    'Impossible d\'enregistrer la séquence d\'id %d (%s)',
                    $id,
                    var_export($this->Sequence->validationErrors, true)
                )
            );
        }
    }

    /**
     *
     * @param type $id
     * @param type $currentValue
     * @throws RuntimeException
     * @since 5.1.2
     */
    public function increment($id, $currentValue)
    {
        $data = ['id'=>$id,"num_sequence"=>($currentValue+1)];
        $saved = $this->save($data);
        if ($saved === false) {
            throw new RuntimeException(
                __(
                    'Impossible d\'enregistrer la séquence d\'id %d (%s)',
                    $id,
                    var_export($this->Sequence->validationErrors, true)
                )
            );
        }
    }

    /**
     *
     * @param type $def_reinit
     * @param type $val_reinit
     * @param type $date
     * @return type
     * @throws NotFoundException
     * @since 5.1.2
     */

    public function checkIfReinit($dateDebut, $dateFin, $def_reinit, $date)
    {
        if (empty($def_reinit)) {
            return false;
        }

        $date = date("Y-m-d", $date);

        if ($date < $dateDebut) {
            throw new NotFoundException("Dernière réinitialisation : ".$dateDebut.", date demandée ".$date);
        }

        if (($date >= $dateDebut) && ($date <= $dateFin)) {
            return false;
        }

        if ($date > $dateFin) {
            return true;
        }
        throw new NotFoundException("cas de reinitialisation non gere");
    }

    /**
     * Calcule les nouvelles dates de fin et de début
     * @param type $defReinit
     * @param type $date
     * @return array  [ ["debut_validite","fin_validite"]]
     */
    public function calculeNouvellesDates($defReinit, $date)
    {
        if ($defReinit === "#AAAA#" || $defReinit === "#AA#") {
            $dateDebut = date("Y-01-01", $date);
            $dateFin = date("Y-12-31", $date);
        }
        if ($defReinit === "#M#" || $defReinit === "#MM#") {
            $dateDebut = date("Y-m-01", $date);
            $dateFin = date("Y-m-t", $date);
        }
        if ($defReinit === "#J#" || $defReinit === "#JJ#") {
            $dateDebut = date("Y-m-d", $date);
            $dateFin = date("Y-m-d", $date);
        }
        return ["debut_validite"=>$dateDebut,"fin_validite"=>$dateFin];
    }
}
