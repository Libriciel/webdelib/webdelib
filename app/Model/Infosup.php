<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('File', 'Utility');
App::uses('Folder', 'Utility');
App::uses('AppTools', 'Lib');
App::uses('SabreDavService', 'Lib/Service');
App::uses('FidoComponent', 'ModelOdtValidator.Controller/Component');

/**
 * [Infosup description]
 *
 * @SuppressWarnings(PHPMD)
 */
class Infosup extends AppModel
{
    public $belongsTo = [
        'Deliberation' => [
            'className' => 'Deliberation',
            'foreignKey' => 'foreign_key',
            'dependent' => false
        ],
        'Seance' => [
            'className' => 'Seance',
            'foreignKey' => 'foreign_key',
            'dependent' => false
        ],
        'Infosupdef' => [
            'className' => 'Infosupdef',
            'foreignKey' => 'infosupdef_id',
            'dependent' => false
        ]
    ];
    public $validate = [
        'file_name' => [
            'fileNameRule-1' => [
                'rule' => 'uploadFileName',
                'required' => false,
                'allowEmpty' => true,
                'message' => 'Seulement les lettres, les entiers et les caractères spéciaux -_.& '
                    .'sont autorisés dans le nom du fichier. Minimum de 5 caractères',
                'growl'
            ],
            'fileNameRule-2' => [
                'rule' => ['maxLength', 255],
                'message' => 'Nom de fichier trop long (255 caractères maximum)',
                'growl'
            ]
        ],
    ];

    /**
     * Transforme la structure [0]['id']['deliberation_id']...
     * en ['code_infosup']=>valeur, ...
     *
     * @param type $infosups
     * @param type $retIdEleListe --> la paramètre $retIdEleListe permet de
     * retourner soit l'id de l'élément de la liste, soit sa valeur
     * @return type
     * @since   4.3
     * @access public
     * @version 5.1
     */
    public function compacte($infosups = [], $retIdEleListe = true)
    {
        $ret = [];

        foreach ($infosups as $infosup) {
            $infosupdef = $this->Infosupdef->find(
                'first',
                [
                    'conditions' => ["Infosupdef.id" => $infosup['infosupdef_id']],
                    'fields' => ['code', 'type', 'api_type', 'api', 'correlation'],
                    'recursive' => -1
                ]
            );

            if (!empty($infosupdef['Infosupdef']['type'])) {
                if ($infosupdef['Infosupdef']['type'] == 'text') {
                    $ret[$infosupdef['Infosupdef']['code']] = $infosup['text'];
                } elseif ($infosupdef['Infosupdef']['type'] == 'geojson') {
                    $correlations = json_decode($infosupdef['Infosupdef']['correlation'], true);
                    $ret[$infosupdef['Infosupdef']['code']]['geojson'] = $infosup['geojson'];
                    $ret[$infosupdef['Infosupdef']['code']]['label'] = empty($infosup['geojson']) === false
                        ? json_decode($infosup['geojson'])->{'properties'}->{$correlations['label']}
                        : null;
                } elseif ($infosupdef['Infosupdef']['type'] == 'richText') {
                    $ret[$infosupdef['Infosupdef']['code']] = stripslashes(html_entity_decode($infosup['content']));
                } elseif ($infosupdef['Infosupdef']['type'] == 'date') {
                    if (empty($infosup['date']) || $infosup['date'] == '0000-00-00') {
                        $ret[$infosupdef['Infosupdef']['code']] = '';
                    } else {
                        //CakeTime::format($this->data['Seance']['date'], '%d/%m/%Y %H:%M'))
                        $ret[$infosupdef['Infosupdef']['code']] = CakeTime::format($infosup['date'], '%d/%m/%Y');
                    }
                } elseif ($infosupdef['Infosupdef']['type'] == 'file') {
                    $ret[$infosupdef['Infosupdef']['code']]['id'] = $infosup['id'];
                    $ret[$infosupdef['Infosupdef']['code']]['name'] = $infosup['file_name'];
                } elseif ($infosupdef['Infosupdef']['type'] == 'odtFile') {
                    $ret[$infosupdef['Infosupdef']['code']]['id'] = $infosup['id'];
                    if (!empty($infosup['file_name'])) {
                        $ret[$infosupdef['Infosupdef']['code']]['name'] = $infosup['file_name'];
                    }
                    if (!empty($infosup['file_infosup'])) {
                        $ret[$infosupdef['Infosupdef']['code']]['file_infosup'] = $infosup['file_infosup'];
                    }
                    if (!empty($infosup['file_infosup_url'])) {
                        $ret[$infosupdef['Infosupdef']['code']]['file_infosup_url'] = $infosup['file_infosup_url'];
                    }
                } elseif ($infosupdef['Infosupdef']['type'] == 'boolean') {
                    $boolean = null;
                    if ($infosup['text'] == '0') {
                        $boolean = false;
                    } elseif ($infosup['text'] == '1') {
                        $boolean = true;
                    } else {
                        $boolean = null;
                    }
                    $ret[$infosupdef['Infosupdef']['code']] = $boolean;
                } elseif ($infosupdef['Infosupdef']['type'] == 'list') {
                    if ($retIdEleListe || empty($infosup['text'])) {
                        $ret[$infosupdef['Infosupdef']['code']] = $infosup['text'];
                    } else {
                        $ele = $this->Infosupdef->Infosuplistedef->find('first', [
                            'conditions' => ['id' => $infosup['text']],
                            'fields' => ['nom'],
                            'recursive' => -1]);
                        if (array_key_exists('Infosuplistedef', $ele)) {
                            $ret[$infosupdef['Infosupdef']['code']] = $ele['Infosuplistedef']['nom'];
                        }
                    }
                } elseif ($infosupdef['Infosupdef']['type'] == 'listmulti') {
                    $ret[$infosupdef['Infosupdef']['code']] = [];
                    if ($retIdEleListe || empty($infosup['text'])) {
                        foreach (explode(
                            ',',
                            str_replace("'", '', $infosup['text'])
                        ) as $elt) {
                            if (!empty($elt)) {
                                $ret[$infosupdef['Infosupdef']['code']][] = $elt;
                            }
                        }
                    } else {
                        $ids = explode(',', str_replace("'", '', $infosup['text']));
                        $elts = $this->Infosupdef->Infosuplistedef->find('all', [
                            'fields' => ['nom'],
                            'conditions' => [
                                'infosupdef_id' => $infosup['infosupdef_id'],
                                'id' => $ids],
                            'order' => ['ordre' => 'ASC'],
                            'recursive' => -1]);
                        foreach ($elts as $elt) {
                            $ret[$infosupdef['Infosupdef']['code']][] = $elt['Infosuplistedef']['nom'];
                        }
                    }
                }
            }
        }
        return $ret;
    }

    /**
     * [beforeValidate description]
     * @param  array  $options [description]
     * @return [type]          [description]
     *
     * @version 5.1.0
     * @since 4.0.0
     */

    /**
     * @access public
     * @param type $options
     * @return boolean
     */
    public function afterSave($created, $options = [])
    {
        if (!empty($this->data['Infosup']['model'])
            && !empty($this->data['Infosup']['foreign_key'])
            && !empty($this->data['Infosup']['content'])
        ) {
            $cmd = 'nohup nice -n 10 -- ' . APP . 'Console' . DS . 'cake Maintenance convertDataToFusion'
                . ' ' . $this->data['Infosup']['model']
                . ' ' . $this->data['Infosup']['foreign_key']
                . ' -t ' . Configure::read('Config.tenantName')
                . ' >/dev/null 2>&1  & echo $!';
            shell_exec($cmd);
        }
    }

    /**
     * Sauvegarde les info sup. recues sous la forme ['code_infosup']=>valeur, ...
     *
     * @param $infosups
     * @param $foreignKey
     * @param $model
     * @return boolean
     * @version 5.1
     * @access public
     */
    public function saveInfosup($data, $model, $create = false)
    {
        $success = true;
        $saveDatas = [];
        $infosupsErrors = [];
        if (isset($data['Infosup'])) {
            foreach ($data['Infosup'] as $code => $value) {
                $saveData = [];
                // lecture de la définition de l'info sup
                $infosupdef = $this->Infosupdef->find(
                    'first',
                    [
                        'fields' => ['id', 'type', 'nom', 'typemime', 'correlation'],
                        'conditions' => [
                            'code' => $code,
                            'model' => $model
                        ],
                        'recursive' => -1,
                    ]
                );

                //Récupération de l'identifiant de l'infosup
                $Infosup = ClassRegistry::init('Infosup');
                $infosup = $Infosup->find(
                    'first',
                    [
                        'fields' => ['id'],
                        'conditions' => [
                            'model' => $model,
                            'foreign_key' => $data[$model]['id'],
                            'infosupdef_id' => $infosupdef['Infosupdef']['id']
                        ],
                        'recursive' => -1,
                    ]
                );
                if (!empty($infosup)) {
                    $saveData['id'] = $infosup['Infosup']['id'];
                }
                $saveData['foreign_key'] = $data[$model]['id'];
                $saveData['infosupdef_id'] = $infosupdef['Infosupdef']['id'];
                $saveData['model'] = $model;

                // affectation de la valeur en fonction du type
                switch ($infosupdef['Infosupdef']['type']) {
                    case 'text':
                    case 'list':
                        // Ajout de la regle de validation pour les textes trop long
                        if ($infosupdef['Infosupdef']['type'] === 'text') {
                            $this->validator()->add('text', 'required', [
                                'rule' => ['between', 0, 1000],
                                'message' => '\"' . $infosupdef['Infosupdef']['nom']
                                    . '\" trop long (1000 caractères maximum)', 'growl',
                            ]);
                        }
                        $saveData['text'] = $value;
                        break;
                    case 'geojson':
                        if (!empty($value['label']) && empty($value['geojson'])) {
                            $infosupsErrors[$code] = __(' Adresse inconnue dans la base d\'adresse');
                        }
                        $saveData['geojson'] = !empty($value['geojson']) ? $value['geojson'] : null;
                        break;
                    case 'boolean':
                        $saveData['text'] = $value;
                        break;
                    case 'listmulti':
                        $saveData['text'] = !empty($value) ? implode(',', $value) : '';
                        break;
                    case 'richText':
                        $saveData['content'] = $value;
                        $saveData['edition_data'] = '';
                        break;
                    case 'date':
                        $saveData['date'] = '';
                        $date = explode('/', $value);
                        if (count($date) == 3) {
                            $saveData['date'] = $date[2] . '-' . $date[1] . '-' . $date[0];
                        }
                        break;
                    case 'file':
                    case 'odtFile':
                        $saveData += $this->saveInfosupFile(
                            $model,
                            $code,
                            $data['Infosup'][$code],
                            $infosupsValidationErrors
                        );
                        break;
                    default:
                        break;
                }

                $saveDatas[] = $saveData;
            }

            if (!empty($infosupsValidationErrors)) {
                $this->validationErrors[$this->alias] = $infosupsValidationErrors;
                return false;
            }

            if (!empty($saveDatas) && empty($infosupsErrors)) {
                $this->saveMany($saveDatas);
            }
        }

        return $success;
    }

    /**
     * [saveInfosupFile description]
     * @param  [type] $code           [description]
     * @param  [type] $data           [description]
     * @param  [type] $infosupsErrors [description]
     * @return [type]                 [description]
     */
    private function saveInfosupFile($model, $code, $data, &$infosupsValidationErrors)
    {
        $saveData = [];

        //Si import d'un nouveau fichier
        if (is_array($data) && !empty($data['tmp_name'])) {
            $typemimeFileValid = $this->Infosupdef->getFileTypemimeByModelAndCode($model, $code);
            $extensionFileValid = AppTools::getTypemimesFileByTypemime($typemimeFileValid);

            $this->validator()->add('content', 'format', [
                'rule' => ['checkFormat', [$typemimeFileValid], false],
                'message' => __(
                    'Le format du fichier est invalide. (Autorisé : fichier %s)',
                    $extensionFileValid
                ), 'growl',
            ]);

            $file = new File($data['tmp_name'], false);
            $saveData['file_name'] = $data['name'];
            $saveData['file_size'] = $file->size();
            $saveData['file_type'] = $file->mime();
            $saveData['content'] = $file->read();
            //Pour convertir les modifications
            $saveData['edition_data'] = '';
            $file->close();

            $this->set($saveData);
            if (!$this->validates()) {
                $infosupsValidationErrors[$code] = array_shift(array_shift($this->validationErrors));
            }
            $this->clear();
            $this->validator()->remove('content');
        } elseif (!empty($data['file_infosup'])) {
            $sabreDavService = new SabreDavService();
            $file = $sabreDavService->getFileDav($data['file_infosup']);
            $saveData['content'] = $file->read();
            $saveData['file_size'] = $file->size();
            $file->close();
        } else {
            if (!empty($data['error']) && $data['error'] === UPLOAD_ERR_NO_FILE) {
                $saveData['file_name'] = '';
                $saveData['file_size'] = '';
                $saveData['file_type'] = '';
                $saveData['content'] = '';
                $saveData['edition_data'] = '';
            }
        }

        return $saveData;
    }

    /**
     * [saveInfosupDelibRattachees description]
     * @param  [type] $projetParentId [description]
     * @param  [type] $projetId       [description]
     * @return [type]                 [description]
     *
     * @version 5.1.0
     */
    public function saveInfosupDelibRattachees($projetParentId, $projetId)
    {
        $this->recursive = -1;
        $parent_infosups = $this->findAllByForeignKeyAndModel($projetParentId, 'Deliberation');

        foreach ($parent_infosups as $parent_infosup) {
            // Prepare data
            $infosup = $this->findByForeignKeyAndInfosupdefId($projetId, $parent_infosup['Infosup']['infosupdef_id']);

            if (empty($infosup)) {
                $this->create();
                unset($parent_infosup['Infosup']['id']);
                $parent_infosup['Infosup']['created'] = date("Y-m-d H:i:s");
                $parent_infosup['Infosup']['modified'] = date("Y-m-d H:i:s");
            } else {
                $parent_infosup['Infosup']['id'] = $infosup['Infosup']['id'];
                $parent_infosup['Infosup']['modified'] = date("Y-m-d H:i:s");
            }

            $parent_infosup['Infosup']['foreign_key'] = $projetId;

            $this->save($parent_infosup, false);
            $this->clear();
        }
    }

    /**
     * Retourne la liste des deliberation_id sous la forme 'delib_id1, delib_id2, ...'
     * correspondant à $recherches qui est sous la forme array('infosupdef_id'=>'valeur')
     * @param type $recherches
     * @return type
     * @version 5.0
     * @since 4.0
     * @access public
     */
    public function selectInfosup($recherches)
    {
        // initialisations
        $conditions = [];
        $db = $this->getDataSource();
        // construction des différentes clauses
        foreach ($recherches as $infosupdefId => $recherche) {
            if (empty($recherche) && $recherche != '0') {
                continue;
            }
            $infosupType = $this->Infosupdef->field('type', ['id' => $infosupdefId]);
            $alias = 'Infosup';
            switch ($infosupType) {
                case 'richText':
                    $subQueryConditions = [$alias . '.content ILIKE' => trim($recherche)];
                    break;
                case 'text':
                    $subQueryConditions = [$alias . '.text ILIKE' => trim($recherche)];
                    break;
                case 'date':
                    if (empty($recherche['dateDebut']) || empty($recherche['dateFin'])) {
                        continue 2;
                    }
                    $dateDebut = explode('/', trim($recherche['dateDebut']));
                    $dateFin = explode('/', trim($recherche['dateFin']));
                    $subQueryConditions = [$alias . '.date BETWEEN ? AND ?' => [
                        $dateDebut[2] . '-' . $dateDebut[1] . '-' . $dateDebut[0],
                        $dateFin[2] . '-' . $dateFin[1] . '-' . $dateFin[0]]
                    ];
                    break;
                case 'boolean':
                    $subQueryConditions = [$alias . '.text' => trim($recherche)];
                    break;
                case 'listmulti':
                case 'list':
                    $conditions_list = [];
                    foreach ($recherche as $idInfoSup) {
                        $conditions_list['OR'][] = [$alias . '.text LIKE' => '%' . $idInfoSup . '%'];
                    }
                    $subQueryConditions = ['AND' => $conditions_list];
                    break;
                default:
                    break;
            }

            $subQuery = [
                'fields' => [
                    'DISTINCT Infosup.foreign_key'
                ],
                'contain' => false,
                'conditions' => array_merge(
                    [
                        'Infosup.infosupdef_id' => $infosupdefId,
                        'Infosup.model' => 'Deliberation'],
                    $subQueryConditions
                )
            ];

            $subQuery = $this->sql($subQuery);
            $subQuery = ' "Deliberation"."id" IN (' . $subQuery . ') ';
            $subQueryExpression = $db->expression($subQuery);
            $conditions[] = $subQueryExpression;
        }

        return $conditions;
    }

    /**
     * Retourne les informations supplémentaire sous forme de tableau suivant leur type
     *
     * @param $id
     * @param string $model
     * @return array
     * @since 4.3
     * @access public
     * @version 5.1
     */
    public function export($id, $model = 'Deliberation', $joindre_sae = false)
    {
        $return = [];
        $joins = false;
        if ($joindre_sae===true) {
            $joins = [
                [
                    'table' => 'infosupdefs',
                    'alias' => 'Infosupdef_Filter',
                    'type' => 'INNER',
                    'conditions' => [
                        'Infosup.infosupdef_id = Infosupdef_Filter.id',
                        'Infosupdef_Filter.joindre_sae' => true
                    ]
                ]
            ];
        }

        $infosups = $this->find(
            'all',
            [
                'fields' => ['Infosup.id', 'Infosupdef.type', 'Infosupdef.code',
                    'Infosup.text', 'Infosup.date', 'Infosup.content',
                    'Infosup.file_name', 'Infosup.file_type', 'Infosup.file_size', 'Infosup.geojson'
                ],
                'joins' => $joins,
                'conditions' => [
                    'Infosup.foreign_key' => $id,
                    'Infosup.model' => $model
                ],
                'recursive' => 0
            ]
        );
        foreach ($infosups as $infosup) {
            if ($infosup['Infosupdef']['type'] == 'text') {
                $return[$infosup['Infosupdef']['code']] = [
                    'type' => 'string',
                    'content' => $infosup['Infosup']['text']];
            } elseif ($infosup['Infosupdef']['type'] == 'richText') {
                $return[$infosup['Infosupdef']['code']] = [
                    'type' => 'string',
                    'content' => $infosup['Infosup']['content']];
            } elseif ($infosup['Infosupdef']['type'] == 'date') {
                App::uses('CakeTime', 'Utility');
                $return[$infosup['Infosupdef']['code']] = [
                    'type' => 'string',
                    'content' => CakeTime::i18nFormat($infosup['Infosup']['date'], '%A %d %B %Y à %k:%M')];
            } elseif ($infosup['Infosupdef']['type'] == 'odtFile') {
                $return[$infosup['Infosupdef']['code']] = [
                    'type' => 'odtFile',
                    'id' => $infosup['Infosup']['id'],
                    'file_name' => $infosup['Infosup']['file_name'],
                    'file_type' => $infosup['Infosup']['file_type'],
                    'content' => $infosup['Infosup']['content']];
            } elseif ($infosup['Infosupdef']['type'] == 'file') {
                $return[$infosup['Infosupdef']['code']] = [
                    'type' => 'file',
                    'id' => $infosup['Infosup']['id'],
                    'file_name' => $infosup['Infosup']['file_name'],
                    'file_type' => $infosup['Infosup']['file_type'],
                    'content' => $infosup['Infosup']['content']];
            } elseif ($infosup['Infosupdef']['type'] == 'boolean') {
                $return[$infosup['Infosupdef']['code']] = [
                    'type' => 'string',
                    'content' => $infosup['Infosup']['text']];
            } elseif ($infosup['Infosupdef']['type'] == 'list') {
                $ele = $this->Infosupdef->Infosuplistedef->find('first', ['fields' => ['nom'],
                    'conditions' => ['id' => $infosup['Infosup']['text']],
                    'recursive' => -1]);
                if (!empty($ele['Infosuplistedef']['nom'])) {
                    $return[$infosup['Infosupdef']['code']] = [
                        'type' => 'string',
                        'content' => $ele['Infosuplistedef']['nom']];
                }
            } elseif ($infosup['Infosupdef']['type'] == 'listmulti') {
                $elts = $this->Infosupdef->Infosuplistedef->find('all', [
                    'conditions' => [
                        'id' => explode(',', str_replace("'", '', $infosup['Infosup']['text']))
                    ],
                    'fields' => ['nom'],
                    'recursive' => -1
                ]);
                $content = [];
                foreach ($elts as $elt) {
                    $content[] = $elt['Infosuplistedef']['nom'];
                }
                $return[$infosup['Infosupdef']['code']] = [
                    'type' => 'string',
                    'content' => implode(', ', $content)
                ];
            } elseif ($infosup['Infosupdef']['type'] == 'geojson') {
                $return[$infosup['Infosupdef']['code']] = ['type' => 'geojson',
                    'id' => $infosup['Infosup']['id'],
                    'file_name' => 'geojson',
                    'file_type' => 'application/json',
                    'content' => $infosup['Infosup']['geojson']];
            }
        }
        return $return;
    }

    /**
     * Fonction d'initialisation des variables de fusion pour l'allias utilisé
     * pour la liaison (Rapporteur, President, ...).
     * Les bibliothèques Gedooo doivent être inclues par avance.
     * Génère une exception en cas d'erreur
     *
     * @access public
     * @param type $aData
     * @param object_by_ref $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param string $modelName --> nom du modele lié
     * @param integer $id --> id du modèle lié
     * @return type
     * @throws Exception en cas d'erreur de fichier non converti
     * @version 5.1
     * @since 4.3
     */
    public function setVariablesFusion(&$aData, &$modelOdtInfos, $modelName, $id)
    {

        // lecture de la définition des infosup
        $allInfoSupDefs = $this->Infosupdef->find('all', [
            'recursive' => -1,
            'fields' => ['id', 'code', 'type','correlation'],
            'conditions' => ['model' => $modelName]]);

        // infosups utilisées dans le modèle d'édition
        $infoSupDefs = $infoSupDefIds = $geojsonInfosupNames = [];
        foreach ($allInfoSupDefs as $infoSupDef) {
            if ($infoSupDef['Infosupdef']['type'] !== 'geojson') {
                if ($modelOdtInfos->hasUserFieldDeclared($infoSupDef['Infosupdef']['code'])) {
                    $infoSupDefIds[] = $infoSupDef['Infosupdef']['id'];
                    $infoSupDefs[$infoSupDef['Infosupdef']['id']] = $infoSupDef['Infosupdef'];
                }
                if ($modelOdtInfos->hasUserFieldDeclared($infoSupDef['Infosupdef']['code'] . '_' . 'libelle')) {
                    $infoSupDefIds[] = $infoSupDef['Infosupdef']['id'];
                    $infoSupDefs[$infoSupDef['Infosupdef']['id']] = $infoSupDef['Infosupdef'];
                }
            } else {
                $correlations = json_decode($infoSupDef['Infosupdef']['correlation'], true);
                $found = false;
                foreach (array_keys($correlations) as $suffix) {
                    $key = $infoSupDef['Infosupdef']['code'] . '_' . $suffix;
                    if ($modelOdtInfos->hasUserFieldDeclared($key)) {
                        $geojsonInfosupNames[] = $key;
                        $found = true;
                    }
                }
                if ($found === true) {
                    $infoSupDefIds[] = $infoSupDef['Infosupdef']['id'];
                    $infoSupDefs[$infoSupDef['Infosupdef']['id']] = $infoSupDef['Infosupdef'];
                }
            }
        }
        $geojsonInfosupNames = array_unique($geojsonInfosupNames);

        if (empty($infoSupDefIds)) {
            return;
        }
        // lecture des valeurs des infosups
        $infosups = $this->find('all', [
            'recursive' => -1,
            'fields' => ['infosupdef_id', 'text', 'date', 'content', 'edition_data','geojson'],
            'joins' => [$this->join('Infosupdef', ['type' => 'LEFT'])],
            'conditions' => [
                'Infosupdef.model' => $modelName,
                'Infosupdef.joindre_fusion' => true,
                'foreign_key' => $id,
                'Infosupdef.id' => $infoSupDefIds]]);

        $file_odt_vide = file_get_contents(APP . DS . 'Config' . DS . 'OdtVide.odt');
        // fusion des variables

        foreach ($infosups as $infosup) {
            $code_name = $infoSupDefs[$infosup['Infosup']['infosupdef_id']]['code'];

            switch ($infoSupDefs[$infosup['Infosup']['infosupdef_id']]['type']) {
                case 'boolean':
                    $aData[$code_name] = ['value' => $infosup['Infosup']['text'] ? '1' : '0', 'type' => 'text'];
                    break;
                case 'text':
                    $aData[$code_name] = ['value' => $infosup['Infosup']['text'], 'type' => 'text'];
                    break;
                case 'date':
                    if (empty($infosup['Infosup']['date'])) {
                        $aData[$code_name] = ['value' => '', 'type' => 'date'];
                        break;
                    }
                    $aData[$code_name] = [
                        'value' => CakeTime::i18nFormat($infosup['Infosup']['date'], '%d/%m/%Y'),
                        'type' => 'date'
                    ];
                    break;
                case 'list':
                    if (empty($infosup['Infosup']['text'])) {
                        $aData[$code_name] = ['value' => '', 'type' => 'text'];
                        break;
                    }
                    $listValue = $this->Infosupdef->Infosuplistedef->field(
                        'nom',
                        ['id' => $infosup['Infosup']['text']]
                    );
                    $aData[$code_name] = ['value' => $listValue, 'type' => 'text'];
                    break;
                case 'listmulti':
                    if (empty($infosup['Infosup']['text'])) {
                        $aData[$code_name] = ['value' => '', 'type' => 'text'];
                        break;
                    }
                    $listValues = $this->Infosupdef->Infosuplistedef->nfield(
                        'nom',
                        [
                            'id' => explode(
                                ',',
                                str_replace('\'', '', $infosup['Infosup']['text'])
                            )
                        ],
                        ['ordre']
                    );
                    $aData[$code_name] = ['value' => implode(', ', $listValues), 'type' => 'text'];
                    break;
                case 'file':
                case 'richText':
                    if (empty($infosup['Infosup']['content'])) {
                        $aData[$code_name] = ['value' => $file_odt_vide, 'type' => 'file'];
                        break;
                    }
                    if (empty($infosup['Infosup']['edition_data'])) {
                        throw new Exception(
                            __(
                                'Toutes les informations supplémentaires du projet : %s '
                                .'ne sont pas encore converties pour générer le document.'
                                . ' \n Veuillez réessayer dans quelques instants ...',
                                $id
                            )
                        );
                    }
                    $aData[$code_name] = ['value' => $infosup['Infosup']['edition_data'], 'type' => 'file'];
                    break;
                case 'odtFile':
                    if (empty($infosup['Infosup']['content'])) {
                        $aData[$code_name] = ['value' => $file_odt_vide, 'type' => 'file'];
                        break;
                    }
                    $aData[$code_name] = [
                        'value' => $infosup['Infosup']['content'],
                        'type' => 'file'
                    ];
                    break;
                case 'geojson':
                    $infosGeojson = json_decode($infosup['Infosup']['geojson']);
                    $correlations = json_decode(
                        $infoSupDefs[$infosup['Infosup']['infosupdef_id']]['correlation'],
                        true
                    );
                    if (empty($infosGeojson) === false) {
                        foreach ($correlations as $var => $alias) {
                            if (in_array($code_name . '_' . $var, $geojsonInfosupNames) === true) {
                                $aData[$code_name . '_' . $var] = [
                                    'value' => $infosGeojson->properties->{$alias},
                                    'type' => 'text'
                                ];
                            }
                        }
                    } else {
                        foreach (array_keys($correlations) as $var) {
                            if (in_array($code_name . '_' . $var, $geojsonInfosupNames) === true) {
                                $aData[$code_name . '_' . $var] = ['value' => '', 'type' => 'text'];
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Retourne le type de résultat attendu lors de la requête à l'api
     * pour les infos sup de type géoloc
     */
    public function getApiTypeFromInfosup(int $deliberation_id)
    {
        return $this->find('first', [
            'fields' => ['Infosupdef.api_type'],
            'contain' => ['Infosupdef'],
            'conditions' => ['Infosup.foreign_key' => $deliberation_id],
            'recursive' => -1
        ]);
    }

    /**
     * Retourne le geojson
     */
    public function getGeoJsonFromInfosup(int $deliberation_id)
    {
        return $this->find('first', [
            'fields' => ['Infosup.geojson'],
            'contain' => ['Infosupdef'],
            'conditions' => ['Infosup.foreign_key' => $deliberation_id, 'Infosupdef.type' => 'geojson'],
            'recursive' => -1
        ]);
    }
}
