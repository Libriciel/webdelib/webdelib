<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [CircuitHistorique description]
 *
 * @SuppressWarnings(PHPMD)
 */
class CircuitHistorique extends AppModel
{
    public $useTable = false;

    /**
     * Fonction d'initialisation des variables de fusion pour l'historique des circuits d'un projet
     *
     * @version 5.1
     * @access public
     * @param array $aData
     * @param type $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param type $deliberation_id --> id du projet/délibération
     * @return type
     */
    public function setVariablesFusion(&$aData, &$oModelOdtInfos, $projetId)
    {
        if (!$oModelOdtInfos->hasUserFieldDeclared('CircuitHistorique')
                && !$oModelOdtInfos->hasAutoStyle('CircuitHistorique')
                && !$oModelOdtInfos->hasHumanStyle('CircuitHistorique')) {
            return;
        }

        App::uses('Traitement', 'Cakeflow.Model');
        App::uses('Visa', 'Cakeflow.Model');
        App::uses('User', 'Model');
        App::uses('Commentaire', 'Model');
        App::uses('Historique', 'Model');
        $this->Traitement = ClassRegistry::init('Cakeflow.Traitement');
        $this->Visa = ClassRegistry::init('Cakeflow.Visa');
        $this->User = ClassRegistry::init('User');
        $this->Commentaire = ClassRegistry::init('Commentaire');
        $this->Historique = ClassRegistry::init('Historique');

        // lecture du traitement
        $this->Traitement->recursive = -1;
        $traitement = $this->Traitement->findByTargetId(
            $projetId,
            ['id', 'numero_traitement','treated']
        );

        $aHistoriques = [];
        if (!empty($traitement)) {
            $aHistoriques = $this->getVariablesFusionCircuitHistorique(
                $projetId,
                $traitement
            );
        }

        $aData['CircuitHistorique'] = empty($aHistoriques) ? self::getVariablesFusionEmpty() : $aHistoriques;
    }

    private function getCircuitDestinatiares($visa_id, $position)
    {
        $users = $this->Visa->whoIs($visa_id, $position);

        $destinataires = '';
        foreach ($users as $user_id) {
            if ($destinataires != '') {
                $destinataires .= ', ';
            }
            $user = $this->User->find('first', [
                    'fields' => ['nom', 'prenom', 'username'],
                    'conditions' => ['User.id' => $user_id],
                    'recursive' => -1]);
            $destinataires .=
                $user['User']['prenom'] . ' ' . $user['User']['nom'] . ' (' . $user['User']['username'] . ')';
        }
        return $destinataires;
    }

    private function getCircuitHistoriqueLibelle($deliberation_id, $visa)
    {
//            'JP' => __d('cakeflow','Retour à une étape précédente'),
//            'ST' => __d('cakeflow','Traitement terminé'),
//            'IN' => __d('cakeflow','Inséré dans le circuit de traitement'),

        switch ($visa['Visa']['action']) {
            case 'VF':
                return __(
                    'Le projet a été envoyé en validation finale à %s',
                    self::getCircuitDestinatiares($visa['Visa']['id'], 'next')
                );
            case 'IL':
                return __(
                    'Le projet a été envoyé en aller-retour à %s',
                    self::getCircuitDestinatiares($visa['Visa']['id'], 'next')
                );
            case 'IP':
                return __(
                    'Le projet a été envoyé (sans retour) à %s',
                    self::getCircuitDestinatiares($visa['Visa']['id'], 'next')
                );
            case 'JS':
                return __(
                    'Saut d\'étape du projet vers %s',
                    self::getCircuitDestinatiares($visa['Visa']['id'], 'next')
                );
            case 'JP':
                return __(
                    'Retour à l\'étape précédente du projet vers %s',
                    self::getCircuitDestinatiares($visa['Visa']['id'], 'next')
                );
            case 'KO':
                return __('Projet Refusé');
            case 'OK':
                return __('Projet accepté');
            case 'RI':
                return __('Pas de validation');
            default:
                return $this->Visa->libelleActionHistorique($visa['Visa']['action']);
        }
    }

    /**
     *
     * @return type
     */
    private function getVariablesFusionCommentaireEmpty()
    {
        return [
            'CircuitHistorique_commentaires_nom' => [
                'value' => '',
                'type' => 'text'],
            'CircuitHistorique_commentaires_prenom' => [
                'value' => '',
                'type' => 'text'],
            'CircuitHistorique_commentaires_date' => [
                    'value' => '',
                    'type' => 'text'],
            'CircuitHistorique_commentaires_texte' => [
                'value' => '',
                'type' => 'text']
        ];
    }

    /**
     *
     * @return type
     */
    private function getVariablesFusionHistoriqueEmpty()
    {
        return [
            'CircuitHistorique_historiques_date' => [
                'value' => '',
                'type' => 'text'],
            'CircuitHistorique_historiques_texte' => [
                'value' => '',
                'type' => 'text']
        ];
    }

    /**
     *
     * @return type
     */
    private function getVariablesFusionValideurEmpty()
    {
        return [
            'CircuitHistorique_valideur_nom' => [
                'value' => '',
                'type' => 'text'],
            'CircuitHistorique_valideur_prenom' => [
                'value' => '',
                'type' => 'text'],
            'CircuitHistorique_valideur_date' => [
                'value' => '',
                'type' => 'date'],
            'CircuitHistorique_valideur_action' => [
                'value' => '',
                'type' => 'text'],
        ];
    }

    /**
     *
     * @return type
     */
    private function getVariablesFusionEmpty()
    {
        return [
            'CircuitHistorique_libelle' => [
                'value' => '',
                'type' => 'text'],
            'CircuitHistorique_valideurs' => self::getVariablesFusionValideurEmpty(),
            'CircuitHistorique_historiques' => self::getVariablesFusionHistoriqueEmpty(),
            'CircuitHistorique_commentaires' => self::getVariablesFusionCommentaireEmpty(),
        ];
    }

    /**
     *
     * @param type $deliberationId
     * @param type $visa
     * @param type $dateLastTraitement
     * @return array
     */
    private function getVariablesFusionCommentaire($deliberationId, $visa, $dateLastTraitement, $dateFinalTraitement)
    {
        //Si saut d'étape
        if ($this->isTraitementJump($visa['Visa']['traitement_id'], $visa['Visa']['numero_traitement'])) {
            return [self::getVariablesFusionCommentaireEmpty()];
        }
        //Récupération des commentaires dans un traitement
        $conditions = [
          'delib_id' => $deliberationId,
          'Commentaire.created >' => $dateLastTraitement,
          'Commentaire.created <=' => $dateFinalTraitement
        ];

        //Récupération des commentaires des rédacteurs
        if (empty($dateLastTraitement) && !empty($visa['Visa']['action']) && $visa['Visa']['action']=='IN') {
            //Récupération des commentaires de rédaction
            $conditions = [
              'delib_id' => $deliberationId,
              'Commentaire.created <=' => $visa['Visa']['date']
            ];
        }
        //Récupération des commentaires dans le traitement en cours
        if ($this->isTraitementSleep($visa['Visa']['traitement_id'], $visa['Visa']['numero_traitement'])) {
            $conditions = [
              'Commentaire.created >' => $dateLastTraitement,
              'delib_id' => $deliberationId,
            ];
        }

        $commentaires = $this->Commentaire->find('all', [
            'fields' => ['texte','Commentaire.created'],
            'contain' => ['User' => ['fields' => ['nom', 'prenom']]],
            'conditions' => $conditions,
            'order' => ['Commentaire.created'=> 'ASC'],
            'recursive' => -1,
        ]);

        if (empty($commentaires)) {
            return [self::getVariablesFusionCommentaireEmpty()];
        }

        $aCommentaires = [];
        foreach ($commentaires as $commentaire) {
            $aCommentaires[] = [
                'CircuitHistorique_commentaires_nom' => [
                    'value' => $commentaire['User']['nom'],
                    'type' => 'text'],
                'CircuitHistorique_commentaires_prenom' => [
                    'value' => $commentaire['User']['prenom'],
                    'type' => 'text'],
                'CircuitHistorique_commentaires_date' => [
                    'value' => CakeTime::i18nFormat(
                        $commentaire['Commentaire']['created'],
                        '%d/%m/%Y %H:%M:%S'
                    ),
                    'type' => 'date'],
                'CircuitHistorique_commentaires_texte' => [
                    'value' => str_replace(PHP_EOL, " ", $commentaire['Commentaire']['texte']),
                    'type' => 'lines']
            ];
        }

        return $aCommentaires;
    }

    /**
     *
     * @param type $deliberationId
     * @param type $dateFinalTraitement
     * @param type $dateVisaPrecedent
     * @return array
     */
    private function getVariablesFusionHistoriques($deliberationId, $visa, $dateLastTraitement, $dateFinalTraitement)
    {
        //Si saut d'étape
        if ($this->isTraitementJump($visa['Visa']['traitement_id'], $visa['Visa']['numero_traitement'])) {
            return [
                self::getVariablesFusionCommentaireEmpty()
            ];
        }

        $conditions = [
            'delib_id' => $deliberationId,
            'commentaire ILIKE' => '%] Modification%',
        ];
        if (!empty($dateLastTraitement)) {
            $conditions[] = [
                'Historique.created >=' => CakeTime::i18nFormat(
                    CakeTime::fromString($dateLastTraitement),
                    '%Y-%m-%d %H:%M:%S'
                )
            ];
        } else {
            $conditions[] = [
                'Historique.created <=' => CakeTime::i18nFormat(
                    CakeTime::fromString($dateLastTraitement),
                    '%Y-%m-%d %H:%M:%S'
                )
            ];
            $dateFinalTraitement = null;
        }
        if (!empty($dateFinalTraitement)) {
            $conditions[] = [
                'Historique.created <=' => CakeTime::i18nFormat(
                    CakeTime::fromString($dateFinalTraitement),
                    '%Y-%m-%d %H:%M:%S'
                )
            ];
        }

        $historiques = $this->Historique->find('all', [
            'fields' => ['id', 'Historique.created', 'commentaire'],
            'conditions' => $conditions,
            'order' => ['Historique.created ASC'],
            'recursive' => -1,
        ]);

        if (empty($historiques)) {
            return [self::getVariablesFusionHistoriqueEmpty()];
        }

        $aHistoriques = [];
        foreach ($historiques as $historique) {
            $aHistoriques[] = [
                'CircuitHistorique_historiques_date' => [
                    'value' => CakeTime::i18nFormat($historique['Historique']['created'], '%d/%m/%Y %H:%M:%S'),
                    'type' => 'date'],
                'CircuitHistorique_historiques_texte' => [
                    'value' => $historique['Historique']['commentaire'],
                    'type' => 'text']
            ];
        }

        return $aHistoriques;
    }

    /**
     *
     * @param type $deliberation_id
     * @param type $visa
     * @return array
     */
    private function getVariablesFusionValideurs($deliberation_id, $traitementId, $numeroTraitement)
    {

        // lecture des visas des étapes passées
        $visas = $this->Visa->find('all', [
            'fields' => ['id',  'trigger_id',  'type_validation', 'date', 'action'],
            'conditions' => [
                'traitement_id' => $traitementId,
                'numero_traitement' => $numeroTraitement,
            ],
            'order' => ['date'=>'DESC'],
            'recursive' => -1,
        ]);

        if (empty($visas)) {
            return [self::getVariablesFusionValideurEmpty()];
        }

        $valideurs = [];
        foreach ($visas as $visa) {
            $firstname = '';
            $lastname = '';
            if ($visa['Visa']['trigger_id']!==-1) {
                $this->User->recursive = -1;
                $user = $this->User->findById($visa['Visa']['trigger_id'], ['nom', 'prenom']);
                $firstname = $user['User']['nom'];
                $lastname = $user['User']['prenom'];
            }

            $valideurs[] = [
              'CircuitHistorique_valideur_nom' => [
                  'value' => $firstname,
                  'type' => 'text'],
              'CircuitHistorique_valideur_prenom' => [
                  'value' => $lastname,
                  'type' => 'text'],
              'CircuitHistorique_valideur_date' => [
                  'value' => CakeTime::i18nFormat($visa['Visa']['date'], '%d/%m/%Y %H:%M:%S'),
                  'type' => 'date'],
              'CircuitHistorique_valideur_action' => [
                  'value' => self::getCircuitHistoriqueLibelle($deliberation_id, $visa),
                  'type' => 'text'],
                    ];
            $this->User->clear();
        }

        return $valideurs;
    }

    /**
     * [getVariablesFusionCircuitHistorique description]
     * @param  [type] $deliberationId [description]
     * @param  [type] $traitementId   [description]
     * @return [type]                 [description]
     */
    private function getVariablesFusionCircuitHistorique($deliberationId, $traitement)
    {
        $db = $this->Visa->getDataSource();
        $subQuery = [
            'fields' => [
                'min(Visa.id)'
            ],
            'contain' => false,
            'conditions' => [
                'traitement_id' => $traitement['Traitement']['id']
            ],
            'group' => ['numero_traitement'],
            'order' => ['numero_traitement' => 'ASC']
        ];

        $this->Visa->Behaviors->attach('Database.DatabaseTable');
        $subQuery = $this->Visa->sql($subQuery);

        $subQuery = ' "Visa"."id" IN (' . $subQuery . ') ';
        $conditions[] = $db->expression($subQuery);
        // lecture des visas des étapes passées par numéro de traitement
        $visas = $this->Visa->find('all', [
          'fields' => [
              'id','action','date','etape_id', 'etape_nom', 'etape_type',
              'trigger_id', 'type_validation', 'traitement_id','Visa.numero_traitement'
          ],
          'conditions' => $conditions,
          'contain' => [
            'Traitement' => ['fields' => ['numero_traitement']]
          ],
          'order' => ['Visa.numero_traitement' => 'ASC'],
          'recursive' => -1,
        ]);

        $aHistoriques = [];
        // constitution du tableau pour l'affichage
        foreach ($visas as $visa) {
            $dateLastTraitement = $this->getDateLastTraitement(
                $visa['Visa']['traitement_id'],
                $visa['Visa']['numero_traitement']
            );
            $dateFinalTraitement = $this->getDateFinalTraitement(
                $visa['Visa']['traitement_id'],
                $visa['Visa']['numero_traitement']
            );

            $aHistoriques[] = [
              'CircuitHistorique_libelle' => [
                  'value' => $visa['Visa']['etape_nom'],
                  'type' => 'text'],
              'CircuitHistorique_valideurs' => self::getVariablesFusionValideurs(
                  $deliberationId,
                  $visa['Visa']['traitement_id'],
                  $visa['Visa']['numero_traitement']
              ),
              'CircuitHistorique_historiques' => self::getVariablesFusionHistoriques(
                  $deliberationId,
                  $visa,
                  $dateLastTraitement,
                  $dateFinalTraitement
              ),
              'CircuitHistorique_commentaires' => self::getVariablesFusionCommentaire(
                  $deliberationId,
                  $visa,
                  $dateLastTraitement,
                  $dateFinalTraitement
              ),
            ];
            //CakeLog::error(var_export($visa['Traitement']['numero_traitement'], true));
            // Ne pas prendre en compte les étapes de circuit non validé
            if (!$traitement['Traitement']['treated']
                && $visa['Visa']['numero_traitement'] === $visa['Traitement']['numero_traitement']
            ) {
                if ($this->isTraitementSleep($visa['Visa']['traitement_id'], $visa['Visa']['numero_traitement'])) {
                    break;
                }
            }

            $this->User->clear();
        }

        return $aHistoriques;
    }

    /**
     * Récupération de la derniere date de traitemenet de l'étape précedente
     * [getDateLastVisa description]
     * @param  [type] $visaTriggerId        [description]
     * @param  [type] $visaNumeroTraitement [description]
     * @return [type]                       [description]
     */
    private function getDateLastTraitement($visaTraitementId, $visaNumeroTraitement)
    {
        $visa = $this->Visa->find('first', [
          'fields' => ['id', 'date'],
          'conditions' => [
              'traitement_id' => $visaTraitementId,
              'numero_traitement <' => $visaNumeroTraitement,
              'date <>' => null
          ],
          'order' => ['date' => 'DESC'],
          'group' => ['numero_traitement', 'id'],
          'recursive' => -1,
        ]);

        return !empty($visa['Visa']['date']) ? $visa['Visa']['date'] : null;
    }

    /**
     * Récupération de la derniere date de traitemenet de l'étape en cours
     * [getDateFinalTraitement description]
     * @param  [type] $visaTriggerId        [description]
     * @param  [type] $visaNumeroTraitement [description]
     * @return [type]                       [description]
     */
    private function getDateFinalTraitement($visaTraitementId, $visaNumeroTraitement)
    {
        $visa = $this->Visa->find('first', [
          'fields' => ['id', 'date'],
          'conditions' => [
              'traitement_id' => $visaTraitementId,
              'numero_traitement' => $visaNumeroTraitement,
              'date <>' => null
          ],
          'order' => ['date' => 'DESC'],
          'group' => ['numero_traitement', 'id'],
          'recursive' => -1,
        ]);

        return !empty($visa['Visa']['date']) ? $visa['Visa']['date'] : null;
    }

    /**
     * [isTraitementSleep description]
     * @param  [type]  $visaTraitementId        [description]
     * @param  [type]  $visaNumeroTraitement [description]
     * @return boolean                       [description]
     */
    private function isTraitementSleep($visaTraitementId, $visaNumeroTraitement)
    {
        $visa = $this->Visa->find('count', [
          'conditions' => [
              'traitement_id' => $visaTraitementId,
              'numero_traitement >=' => $visaNumeroTraitement,
              'action !=' => 'RI'
          ],
          'recursive' => -1,
        ]);

        return empty($visa) ? true : false;
    }

    /**
     * [isTraitementJump description]
     * @param  [type]  $visaTraitementId        [description]
     * @param  [type]  $visaNumeroTraitement [description]
     * @return boolean                       [description]
     */
    private function isTraitementJump($visaTraitementId, $visaNumeroTraitement)
    {
        $isTraitementSleep = $this->isTraitementSleep($visaTraitementId, $visaNumeroTraitement);
        if ($isTraitementSleep) {
            return false;
        }
        $visaTotal = $this->Visa->find('count', [
          'conditions' => [
              'traitement_id' => $visaTraitementId,
              'numero_traitement' => $visaNumeroTraitement,
          ],
          'recursive' => -1,
        ]);

        $visaVide = $this->Visa->find('count', [
          'conditions' => [
              'traitement_id' => $visaTraitementId,
              'numero_traitement' => $visaNumeroTraitement,
              'date' => null,
              'action' => 'RI'
          ],
          'recursive' => -1,
        ]);

        return empty($visaVide) || $visaTotal!==$visaVide ? false : true;
    }
}
