<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class Typologiepiece extends AppModel
{
    /**
     * [public description]
     * @var [type]
     */
    public $hasAndBelongsToMany = [
       'Nature' => [
           'classname' => 'Nature',
           'joinTable' => 'natures_typologiepieces',
           'foreignKey' => 'typologiepiece_id',
           'associationForeignKey' => 'nature_id',
           'conditions' => '',
           'order' => '',
           'limit' => '',
           'unique' => true,
           'finderQuery' => '',
           'deleteQuery' => '']
     ];
    /**
     * [public description]
     * @var [type]
     */
    public $actsAs = ['Tree'];

    /**
     * [getTypologieNamebyCode description]
     * @param  [type] $code [description]
     * @return [type]       [description]
     *
     * @version 5.1.2
     * @since 5.1.1
     */
    public function getTypologieNamebyCode($code)
    {
        if (!empty($code)) {
            $this->recursive = -1;
            $typologiePiece = $this->findByCode($code, ['name', 'code']);
            if (!empty($typologiePiece)) {
                //Mise en place du libelle
                return __(
                    '%s (%s)',
                    $typologiePiece['Typologiepiece']['name'],
                    $typologiePiece['Typologiepiece']['code']
                );
            }
        }

        return '';
    }

    public function getTypologiePieceByTypeActe($type, $typeacteId, $codeClassification = null)
    {
        $typologiePieceIds = $this->getTypologiePieceIdByTypeActe($type, $typeacteId, $codeClassification);
        $typologiePieces = $this->find('all', [
          'fields' => ['Typologiepiece.id', 'Typologiepiece.name', 'Typologiepiece.code'],
          'conditions' => ['Typologiepiece.id' => $typologiePieceIds],
          'order' => ['Typologiepiece.name' => 'ASC'],
          'recursive' => -1
        ]);

        return $typologiePieces;
    }

    /**
     * [getTypologiePieceByTypeActe description]
     * @param  [type] $typeacteId [description]
     * @param  [type] $type       [description]
     * @return [type]             [description]
     */
    public function getTypologiePieceIdByTypeActe($type, $typeacteId, $codeClassification = null)
    {
        $conditions = [
          'Typologiepiece.code LIKE' => '99_%',
          'Typeacte.id' => $typeacteId
        ];
        //CakeLog::error(var_export($codeClassification, true));
        if ($type === 'Annexe' /*&& !empty($codeClassification)*/) {
            $conditions = [
              /*'OR' =>
              [
                ['Typologiepiece.code LIKE' => '99_%'],
                ['Typologiepiece.code LIKE' => substr($codeClassification, 0, 1).'0_%'],
                ['Typologiepiece.code LIKE' => str_replace('_', '', $codeClassification).'_%'],
              ],*/
              'Typeacte.id' => $typeacteId
            ];
        }
        $typologiepieces = $this->find('all', [
            'fields' => ['Typologiepiece.id', 'Typologiepiece.code'],
            'joins' => [
                [
                  'table' => 'natures_typologiepieces',
                  'alias' => 'NaturesTypologiepiece',
                  'type'=> 'INNER',
                  'conditions' => ['Typologiepiece.id = NaturesTypologiepiece.typologiepiece_id']
                ],
                [
                  'table' => 'natures',
                  'alias' => 'Nature',
                  'type'=> 'INNER',
                  'conditions' => ['NaturesTypologiepiece.nature_id = Nature.id']
                ],
                $this->Nature->join('Typeacte', ['type' => 'INNER'])
            ],
            'conditions' => $conditions,
            'recursive' => -1
          ]);
        // if ($type === 'Annexe') {
        //     return Hash::extract($typologiepieces, '{n}.Typologiepiece.id');
        // }

        return  Hash::extract($typologiepieces, '{n}.Typologiepiece.id');
    }

    /**
     * [setTypologiePieceDefault description]
     * @deprecated
     * @param [type] $typologiepieces [description]
     */
    private function setTypologiePieceDefault($typologiepieces)
    {
        $typologiepieceSign = $this->findByCode('99_SE', ['id']);
        //Delete code 99_SE
        $typologiepieces = array_diff($typologiepieces, [$typologiepieceSign['Typologiepiece']['id']]);

        $typologiepieceAutre = $this->findByCode('99_AU', ['id']);
        if (key_exists($typologiepieceAutre['Typologiepiece']['id'], $typologiepieces)) {
            return $typologiepieces;
        }

        return array_merge($typologiepieces, [$typologiepieceAutre['Typologiepiece']['id']]);
    }

    /**
     * [saveTypesPJNatureActe description]
     * @param  [type] $natureId        [description]
     * @param  [type] $typologiePieces [description]
     * @return [type]                  [description]
     */
    public function saveTypesPJNatureActe($natureId, $typologiePieces)
    {
        foreach ($typologiePieces as $typologiePiece) {
            $this->create();
            $this->saveAll(['Typologiepiece'=>[
              'name'=> $typologiePiece['name'],
              'code'=>$typologiePiece['code']
            ],
            'Nature'=>[
              'id'=>$natureId]
            ], ['deep' => true]);
        }
    }
}
