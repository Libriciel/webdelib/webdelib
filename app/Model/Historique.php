<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class Historique extends AppModel
{
    public $name = 'Historique';
    public $belongsTo = [
        'Deliberation' => [
            'className' => 'Deliberation',
            'conditions' => '',
            'order' => '',
            //'type' => 'right',
            'dependent' => false,
            'foreignKey' => 'delib_id'],
        'User' => [
            'className' => 'User',
            'conditions' => '',
            'order' => '',
            'dependent' => false,
            'foreignKey' => 'user_id']
    ];

    /**
     * @version 4.3
     * @access public
     * @param type $results
     * @param type $primary
     * @return string
     */
    public function afterFind($results, $primary = false)
    {
        if (array_key_exists(0, $results)) {
            foreach ($results as $key_historique => $historique) {
                if (array_key_exists('Historique', $historique)
                    && array_key_exists('User', $historique)
                    && ($historique['Historique']['user_id'] <= 0)
                ) {
                    $results[$key_historique]['User']['prenom'] = __('Historique auto');
                    $results[$key_historique]['User']['nom'] = '';
                }
            }
        }

        return $results;
    }

    /**
     * @access public
     * @param type $delib_id
     * @param type $user_id
     * @param type $commentaire
     * @return type
     */
    public function enregistre($delib_id, $user_id, $commentaire)
    {
        $this->create();
        $histo = [];
        $deliberation = $this->Deliberation->find('first', [
            'conditions' => ['Deliberation.id' => $delib_id],
            'fields' => ['id', 'circuit_id'],
            'order'=>['id'=>'desc'],
            'recursive' => -1]);


        $deliberation_version = $this->Deliberation->lastVersion($delib_id);

        $user = $this->User->find('first', [
            'conditions' => ['User.id' => $user_id],
            'recursive' => -1,
            'fields' => ['User.nom', 'User.prenom']]);
        $histo['Historique']['user_id'] = $user_id;
        $histo['Historique']['circuit_id'] = $deliberation['Deliberation']['circuit_id'];
        $histo['Historique']['delib_id'] = $delib_id;
        $histo['Historique']['commentaire'] =
            "[" . $user['User']['prenom'] . ' ' . $user['User']['nom'] . "] $commentaire";
        $histo['Historique']['revision_id'] = isset($deliberation_version['deliberations_versions']['id'])
            ? $deliberation_version['deliberations_versions']['id'] : 0 ;
        return $this->save($histo);
    }

    /**
     * Fonction d'initialisation des variables de fusion pour l'historique d'un projet/délibération
     * les bibliothèques Gedooo doivent être inclues par avance
     * génère une exception en cas d'erreur
     *
     * @version 4.3
     * @access public
     * @param array $aData
     * @param type $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param type $deliberationId --> id du projet/délibération
     * @return type
     */
    public function setVariablesFusion(&$aData, &$oModelOdtInfos, $deliberationId)
    {
        if (!$oModelOdtInfos->hasUserFieldDeclared('log')) {
            return;
        }

        $aHistoriques = [];
        //lecture de l'historique
        $historiques = $this->find('all', ['recursive' => -1,
            'fields' => ['commentaire', 'created'],
            'conditions' => ['delib_id' => $deliberationId]]);

        if (empty($historiques)) {
            $aHistoriques = ['log' => [
                    'value' => '',
                    'type' => 'text']];
        }

        foreach ($historiques as $historique) {
            $aHistoriques[] = ['log' => [
                    'value' => __(
                        '%s : %s',
                        $historique['Historique']['created'],
                        $historique['Historique']['commentaire']
                    ),
                    'type' => 'text']];
        }

        $aData['Historique'] = $aHistoriques;
    }
}
