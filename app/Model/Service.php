<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class Service extends AppModel
{
    public $validate = [
        'name' => [
            [
                'rule' => 'notBlank',
                'message' => 'Entrer le libellé.'
            ]
        ]
    ];
    public $hasAndBelongsToMany = [
        'User' => [
            'classname' => 'User',
            'joinTable' => 'services_users',
            'foreignKey' => 'service_id',
            'associationForeignKey' => 'user_id',
            'conditions' => '',
            'order' => '',
            'limit' => '',
            'unique' => true,
            'finderQuery' => '',
            'deleteQuery' => ''],
        'Acteur' => [
            'classname' => 'Acteur',
            'joinTable' => 'acteurs_services',
            'foreignKey' => 'service_id',
            'associationForeignKey' => 'acteur_id',
            'conditions' => '',
            'order' => '',
            'limit' => '',
            'unique' => true,
            'finderQuery' => '',
            'deleteQuery' => ''],
        'ServiceUser' => [
            'classname' => 'ServiceUser',
            'joinTable' => 'services_users',
            'foreignKey' => 'service_id',
            'associationForeignKey' => 'user_id',
            'conditions' => '',
            'order' => '',
            'limit' => '',
            'unique' => true,
            'finderQuery' => '',
            'deleteQuery' => ''],
    ];
    public $actsAs = [
        'Tree',
    ];

    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->Behaviors->load('AuthManager.AclManager', [
            'type' => 'controlled',
            'loadBehavior' => 'DynamicDbConfig'
        ]);
    }

    /**
     * Fonction récursive de doList
     *
     * @version 4.3
     * @access private
     * @param type $id
     * @return string
     */
    public function doList($id)
    {
        $service = $this->find('first', [
            'conditions' => ['Service.id' => $id],
            'fields' => ['name', 'parent_id'],
            'recursive' => -1]);
        if (empty($service)) {
            return __("Impossible de récupérer le service");
        }
        if (!Configure::read('AFFICHE_HIERARCHIE_SERVICE')) {
            return $service['Service']['name'];
        }
        if (empty($service['Service']['parent_id'])) {
            return $service['Service']['name'];
        } else {
            return $this->doList($service['Service']['parent_id']) . '/' . $service['Service']['name'];
        }
    }

    /**
     * @param array $query
     * @return array
     */
    public function beforeFind($query = [])
    {
        if (!empty($query['allow'])) {
            $query['conditions'] = (is_array($query['conditions'])) ? $query['conditions'] : [];

            //Gestion des droits sur les types d'actes
            $conditions = parent::allowAcl($query['allow'], $this->alias, 'Service.id');
            if ($conditions !== false) {
                $query['conditions'] = array_merge($query['conditions'], $conditions);
            }
        }

        return $query;
    }

    /**
     * Retourne la liste de services disponibles par rapport à un service donné. Fonction privée recursive
     *
     * @access private
     * @param int $id
     * @return array --> Liste Id de tous les services disponibles
     */
    public function doListId($id)
    {
        $aArray = [(int) $id];
        $services = $this->find('all', [
            'fields' => ['id', 'parent_id'],
            'conditions' => ['Service.parent_id' => $id],
            'recursive' => -1]);

        if (!empty($services)) {
            foreach ($services as $service) {
                $service_ids = $this->doListId($service['Service']['id']);
                $aArray = array_merge($aArray, $service_ids);
            }
        }

        return $aArray;
    }

    /**
     * Fonction d'initialisation des variables de fusion pour le service émetteur d'un projet
     * les bibliothèques Gedooo doivent être inclues par avance
     * génère une exception en cas d'erreur
     *
     * @version 4.3
     * @access public
     * @param type $aData
     * @param object_by_ref $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param integer $id --> id du modèle lié
     */
    public function setVariablesFusion(&$aData, &$modelOdtInfos, $id)
    {
        if ($modelOdtInfos->hasUserFieldDeclared('service_emetteur')) {
            $aData['service_emetteur'] = ['value' => $this->field('name', ['id' => $id]), 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('service_avec_hierarchie')) {
            $aData['service_avec_hierarchie'] = ['value' => $this->doList($id), 'type' => 'text'];
        }
    }

    /**
     * @version 4.3
     * @access public
     * @return type
     */
    public function parentNode()
    {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        $data = $this->data;
        if (empty($this->data)) {
            $data = $this->read();
        }
        if (empty($data['Service']['parent_id'])) {
            return null;
        }

        return ['Service' => ['id' => $data['Service']['parent_id']]];
    }

    /**
     * @version 4.3
     * @access public
     * @return type
     */
    public function parentNodeAlias()
    {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        $data = $this->data;
        if (empty($this->data)) {
            $data = $this->read();
        }

        return ['Service' => ['alias' => $data['Service']['name']]];
    }
}
