<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * Liaison entre acteurs et séances
 * @version 5.1.0
 * @since 4.3.0
 */
class Acteurseance extends AppModel
{
    /**
     * [public description]
     * @var [type]
     */
    public $name = 'Acteurseance';
    /*
     * [public description]
     * @var [type]
     */
    public $useTable = 'acteurs_seances';
    /**
     * [public description]
     * @var [type]
     */
    public $belongsTo = ['Acteur', 'Seance'];

    public function deleteSendMail($model, $seance_id)
    {
        $this->deleteAll(['Acteurseance.model' => $model, 'Acteurseance.seance_id' => $seance_id], false);
    }

    public function isSendMailForMeeting($model, $seance_id)
    {
        $mail = $this->find('first', [
          'fields' => 'id',
          'conditions'=> ['Acteurseance.model' => $model, 'Acteurseance.seance_id' => $seance_id]
        ]);

        return !empty($mail) ? true : false;
    }

    public function isSendMailActorPresent($seanceId, $modelTemplateId)
    {
        // chargement  du behavior de fusion du document
        $this->Seance->Behaviors->load('OdtFusion', [
        'id' => $seanceId,
        'modelTemplateId' => $modelTemplateId
        ]);
        // le modèle template possede-t-il des variables de fusion des acteurs
        return $this->Seance->modelTemplateOdtInfos->hasUserFieldsDeclared(
            'salutation_acteur',
            'prenom_acteur',
            'nom_acteur',
            'titre_acteur',
            'position_acteur',
            'email_acteur',
            'telmobile_acteur',
            'telfixe_acteur',
            'adresse1_acteur',
            'adresse2_acteur',
            'cp_acteur',
            'ville_acteur',
            'note_acteur'
        );
    }
}
