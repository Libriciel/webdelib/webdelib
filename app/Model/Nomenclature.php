<?php

/**
 * Nomenclature
 *
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @since       web-delib v4.3
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 * @version     v4.3
 * @package     app.Model
 */
class Nomenclature extends AppModel
{
    public $actsAs = ['Tree'];

    /**
     * @version 4.3
     * @access public
     * @return array
     */
    public function generateTreeListWithOptionGroup()
    {
        $result = [];
        $options = $this->generateTreeList(null, '{n}.Nomenclature.name', '{n}.Nomenclature.libelle', '&nbsp;');
        foreach ($options as $key => $option) {
            if ($option[0] == '&') {
                $titles[str_replace('.', '_', $key)] = $option;
            } else {
                if (!empty($titles)) {
                    $result[str_replace('.', '_', $title)] = $titles;
                }

                $titles = [];
                $title = $option;
            }
        }
        if (!empty($title) && !empty($titles)) {
            $result[str_replace('.', '_', $title)] = $titles;
        }

        return $result;
    }
}
