<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppTools', 'Lib');
App::uses('CakeTime', 'Utility');

/**
 * [Seance description]
 *
 *
 */
class Seance extends AppModel
{
    /**
     * Jours de la semaine
     *
     * @access public
     * @var array
     */
    public $days = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];

    /**
     * Mois de l'année
     *
     * @access public
     * @var array
     */
    public $months = [
        '', 'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet',
        'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'
    ];

    /**
     * Règles de validation
     *
     * @access public
     * @var array
     */
    public $validate = [
        'type_id' => [
            [
                'rule' => 'notBlank',
                'required' => true,
                'message' => 'Entrer le type de séance'
              ],
        ],
        'avis' => [
            [
                'rule' => 'notBlank',
                'message' => 'Sélectionner un avis'
            ]
        ],
        'date' => [
            [
                'rule' => ['datetime', 'ymd'],
                'required' => true,
                'allowEmpty' => false,
                'message' => 'Entrer une date valide'
            ]
        ],
        'commission' => [
            [
                'rule' => 'notBlank',
                'allowEmpty' => false,
                'message' => 'Entrer le texte de débat'
            ]
        ],
        'SeanceChildren' => [
            'SeanceChildren-1' =>
            [
                'rule' => 'canSaveSeanceChildren',
                'message' => 'Vous ne pouvez pas selectionner une ou des séances associées '
                    .'pour une séance non délibérante'
            ],
            'SeanceChildren-2' =>
            [
                'rule' => 'canSaveSeanceChildrenDeliberante',
                'message' => 'Vous ne pouvez pas selectionner une ou des séances associées '
                    .'pour ce type de séance délibérante'
            ],
        ],
        'pv_sommaire' => [
            'rule' => ['checkFormat', 'application/pdf', false],
            'message' => "Format du document invalide. Autorisé : fichier PDF",
            'required' => false,
            'allowEmpty' => true,
        ],
        'pv_complet' => [
            'rule' => ['checkFormat', 'application/pdf', false],
            'message' => "Format du document invalide. Autorisé : fichier PDF",
            'required' => false,
            'allowEmpty' => true,
        ],
    ];

    /**
     * Liaisons de modèles belongsTo
     *
     * @access public
     * @var array
     */
    public $belongsTo = [
        'Typeseance' => [
            'className' => 'Typeseance',
            'dependent' => false,
            'foreignKey' => 'type_id'],
        'Secretaire' => ['className' => 'Acteur',
            'dependent' => false,
            'foreignKey' => 'secretaire_id'],
        'President' => ['className' => 'Acteur',
            'dependent' => false,
            'foreignKey' => 'president_id'],
        'PresidentSeance' => ['className' => 'Acteur',
            'dependent' => false,
            'foreignKey' => 'president_id'],
        'SeanceParent' => [
            'className' => 'Seance',
            'foreignKey' => 'parent_id',
        ],
    ];

    /**
     * Liaison de modèles hasMany
     *
     * @access public
     * @var array
     */
    public $hasMany = [
        'Infosup' => ['dependent' => true,
            'foreignKey' => 'foreign_key',
            'conditions' => ['Infosup.model' => 'Seance']],
        'DeliberationSeance' => [
            'className' => 'DeliberationSeance',
            'foreignKey' => 'seance_id',
            'order' => 'DeliberationSeance.position ASC',
            'counterCache'=>true,
        ],
        'ListePresenceGlobale' => [
            'className' => 'ListePresenceGlobale',
            'foreignKey' => 'seance_id'
        ],
        'SeanceChildren' => [
            'className' => 'Seance',
            'foreignKey' => 'parent_id',
        ],
    ];

    /**
     * Liaisons de modèle hasAndBelongsToMany
     *
     * @access public
     * @var array
     */
    public $hasAndBelongsToMany = [
        'Deliberation' => [
            'className' => 'Deliberation',
            'joinTable' => 'deliberations_seances',
            'foreignKey' => 'seance_id',
            'associationForeignKey' => 'deliberation_id',
            'unique' => true,
            'conditions' => ['Deliberation.etat >=' => 0],
            'fields' => '',
            'order' => 'DeliberationsSeance.position ASC',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => '']];

    public $actsAs = ['Tree'];

    /**
     * @version 4.3
     * @access public
     * @param array $options
     * @return boolean
     */
    public function beforeSave($options = [])
    {
        if (!empty($this->data['Seance']['texte_doc']) && !empty($this->data['Seance']['texte_doc']['tmp_name'])) {
            $analyse = $this->analyzeFile($this->data['Seance']['texte_doc']['tmp_name']);
            $this->data['Seance']['debat_global_type'] = $analyse['mimetype'];
            $this->data['Seance']['debat_global_name'] = $this->data['Seance']['texte_doc']['name'];
            $this->data['Seance']['debat_global_size'] = $this->data['Seance']['texte_doc']['size'];
            $this->data['Seance']['debat_global'] = file_get_contents($this->data['Seance']['texte_doc']['tmp_name']);
        }

        return true;
    }
    /**
     * @version 4.3
     * @access public
     * @param array $query
     * @return array
     */
    public function beforeFind($query = [])
    {
        $query['conditions'] = (is_array($query['conditions'])) ? $query['conditions'] : [];
        //Gestion des droits sur les types d'actes
        if (!empty($query['allow']) && in_array('typeseance_id', $query['allow'], true)) {
            $db = $this->getDataSource();
            $Aro = ClassRegistry::init('Aro');

            $Aro->Behaviors->attach('Database.DatabaseTable');
            $Aro->Permission->Behaviors->attach('Database.DatabaseTable');
            $Aro->Permission->Aco->Behaviors->attach('Database.DatabaseTable');
            $Aro->Permission->Aco->bindModel(
                ['belongsTo' => [
                        'Typeacte' => [
                            'className' => 'Typeacte',
                            'foreignKey' => false,
                            'conditions' => [
                                'Aco.model' => 'Typeacte',
                                'Typeacte.id = Aco.foreign_key'
                            ],
                        ]
                    ]
                ]
            );

            $subQuery = [
                'fields' => [
                    'Typeseance.id'
                ],
                'contain' => false,
                'joins' => [
                    $Aro->join('Permission', ['type' => 'INNER']),
                    $Aro->Permission->join('Aco', ['type' => 'INNER']),
                    $Aro->Permission->Aco->join('Typeacte', ['type' => 'INNER']),
                    $Aro->Permission->Aco->Typeacte->join('TypeseancesTypeacte', ['type' => 'INNER']),
                    $Aro->Permission->Aco->Typeacte->TypeseancesTypeacte->join('Typeseance', ['type' => 'INNER']),
                ],
                'conditions' => [
                    'Aro.foreign_key' => AuthComponent::user('id'),
                    'Permission._read' => 1,
                    'Aro.model' => 'User'
                ]
            ];

            $subQuery = $Aro->sql($subQuery);
            $subQuery = ' "Seance"."type_id" IN (' . $subQuery . ') ';
            $subQueryExpression = $db->expression($subQuery);
            $conditions[] = $subQueryExpression;
            $query['conditions'] = array_merge($query['conditions'], $conditions);

            //Gestion des droits des types de séances
            $conditions = parent::allowAcl($query['allow'], 'Typeseance', 'typeseance_id');
            if ($conditions !== false) {
                $query['conditions'] = array_merge($query['conditions'], $conditions);
            }
        }

        return $query;
    }

    /**
     * @since 4.3
     * @version 5.1
     * @access public
     * @param array $data
     * @return array
     */
    public function saveDebatGen($data)
    {
        $this->validate = [];
        $this->validator()->add('texte_doc', [
            'texteDocRule-1' => [
                'rule' => ['uploadError', true],
                'message' => __('Une erreur est survenue lors de l\'upload du document')
            ],
            'texteDocRule-2' => [
                'rule' => 'uploadFileName',
                'message' => 'Seulement les lettres, les entiers et les caractères spéciaux -_.& '
                    .'sont autorisés dans le nom du fichier. Minimum de 5 caractères',
                'growl'
            ],
            'texteDocRule-3' => [
                'rule' => ['checkFormat', 'application/vnd.oasis.opendocument.text', false],
                'message' => __('Format du document invalide. Autorisé : fichier ODT')
            ]
        ]);

        return parent::save($data);
    }

    /**
     * Retourne la liste des séances futures avec le nom du type de séance
     *
     * @version 4.3
     * @access public
     * @param array $conditionSup
     * @param array $afficherTtesLesSeances
     * @param array $natures
     * @return string
     */
    public function generateList($conditionSup = null, $afficherTtesLesSeances = false, $typeacte_id = [])
    {
        $generateList = [];
        App::import('model', 'TypeseancesTypeacte');
        $TypeseancesTypeacte = ClassRegistry::init('TypeseancesTypeacte');
        $typeseances = $TypeseancesTypeacte->getTypeseanceByTypeActe($typeacte_id);

        $conditions = [];
        $conditions['Seance.type_id'] = $typeseances;
        $conditions['Seance.traitee'] = '0';

        if (!empty($conditionSup)) {
            $conditions = Set::pushDiff($conditions, $conditionSup);
        }
        $seances = $this->find('all', [
            'conditions' => $conditions,
            'order' => ['date'=>'ASC'],
            'fields' => ['Seance.id', 'Seance.date'],
            'contain' => ['Typeseance.action', 'Typeseance.retard', 'Typeseance.libelle']]);

        foreach ($seances as $seance) {
            $deliberante = "----";
            if ($seance['Typeseance']['action'] == 0) {
                $deliberante = "(*)";
            }
            if ($afficherTtesLesSeances) {
                $dateTimeStamp = strtotime($seance['Seance']['date']);
                $dateFr = $this->days[date('w', $dateTimeStamp)] . ' ' . date('d', $dateTimeStamp) . ' ' .
                    $this->months[date('n', $dateTimeStamp)] . ' ' . date('Y', $dateTimeStamp) . ' - ' .
                    date('H', $dateTimeStamp) . ':' . date('i', $dateTimeStamp);
                $generateList[$seance['Seance']['id']] =
                    "$deliberante " . $seance['Typeseance']['libelle'] . " du " . $dateFr;
            } else {
                $retard = $seance['Typeseance']['retard'];

                if ($seance['Seance']['date'] >= date(
                    "Y-m-d",
                    mktime(
                        date("H"),
                        date("i"),
                        date("s"),
                        date("m"),
                        date("d") + $retard,
                        date("Y")
                    )
                )
                ) {
                    $dateTimeStamp = strtotime($seance['Seance']['date']);
                    $dateFr = $this->days[
                            date('w', $dateTimeStamp)
                        ] . ' ' . date('d', $dateTimeStamp) . ' ' .
                        $this->months[
                            date('n', $dateTimeStamp)
                        ] . ' ' . date('Y', $dateTimeStamp) . ' - ' .
                        date('H', $dateTimeStamp) . ':' . date('i', $dateTimeStamp);
                    $generateList[$seance['Seance']['id']] =
                        "$deliberante " . $seance['Typeseance']['libelle'] . " du " . $dateFr;
                }
            }
        }
        return $generateList;
    }

    /**
     * @version 5.1
     * @since 4.3
     * @access public
     * @return string
     */
    public function generateAllList(array $conditions = [])
    {
        $generateList = [];
        $seances = $this->find(
            'all',
            ['order' => 'date DESC',
            'fields' => ['Seance.id', 'Seance.type_id', 'Seance.date'],
            'contain' => ['Typeseance.libelle', 'Typeseance.action'],
            'conditions' => $conditions
            ]
        );

        foreach ($seances as $seance) {
            $deliberante = "";
            if ($seance['Typeseance']['action'] == 0) {
                $deliberante = "(*)";
            }
            $dateTimeStamp = strtotime($seance['Seance']['date']);
            $dateFr = $this->days[date('w', $dateTimeStamp)] . ' ' . date('d', $dateTimeStamp) . ' ' .
                $this->months[date('n', $dateTimeStamp)] . ' ' . date('Y', $dateTimeStamp) . ' - ' .
                date('H', $dateTimeStamp) . ':' . date('i', $dateTimeStamp);
            $generateList[$seance['Seance']['id']] =
                "$deliberante " . $seance['Typeseance']['libelle'] . " du " . $dateFr;
        }
        return $generateList;
    }

    /**
     * @version 4.3
     * @access public
     * @param type $seance_id
     * @param type $nature_id
     * @return boolean
     */
    public function naturecanSave($seance_id, $nature_id)
    {
        if (empty($seance_id)) {
            return true;
        }
        $seance = $this->find('first', ['conditions' => ['Seance.id' => $seance_id],
            'recursive' => -1,
            'fields' => ['Seance.type_id']]);
        App::import('Model', 'TypeseancesTypeacte');
        $this->TypeseancesTypeacte = ClassRegistry::init('TypeseancesTypeacte');
        $natures = $this->TypeseancesTypeacte->getNaturesParTypeseance($seance['Seance']['type_id']);
        return in_array($nature_id, $natures, true);
    }

    /**
     * @version 4.3
     * @access public
     * @param type $seance_id
     * @return type
     */
    public function getDeliberationsId($seance_id)
    {
        return  $this->DeliberationSeance->find('list', [
            'fields' => [
              'DeliberationSeance.position',
              'DeliberationSeance.deliberation_id'
            ],
            'joins' => [
              $this->DeliberationSeance->join('Deliberation', ['type' => 'LEFT'])
            ],
            'conditions' => [
              'DeliberationSeance.seance_id' => $seance_id,
              'Deliberation.etat >=' => 0
            ],
            'order' => ['DeliberationSeance.position' => 'ASC'],
            'recursive' => -1,
        ]);
    }

    /**
     * @version 4.3
     * @access public
     * @param type $seance_id
     * @return type
     */
    public function getLastPosition($seance_id)
    {
        $deliberations = $this->DeliberationSeance->find('count', ['conditions' => ['Seance.id' => $seance_id,
                'Deliberation.etat !=' => -1]]);
        return ($deliberations + 1);
    }



    /**
     * @version 4.3
     * @access public
     * @param int $seance_id
     * @return string
     */
    public function getDate($seance_id)
    {
        if (empty($seance_id)) {
            return '';
        } else {
            $objCourant = $this->find('first', ['conditions' => ['Seance.id' => $seance_id],
                'fields' => ['Seance.date'],
                'recursive' => -1]);
            return $objCourant['Seance']['date'];
        }
    }

    /**
     * @version 4.3
     * @access public
     * @param type $seance_id
     * @return string
     */
    public function getType($seance_id)
    {
        if (empty($seance_id)) {
            return '';
        } else {
            $objCourant = $this->find('first', ['conditions' => ['Seance.id' => $seance_id],
                'fields' => ['Seance.type_id'],
                'recursive' => -1]);
            return $objCourant['Seance']['type_id'];
        }
    }

    /**
     * @param array $seances
     * @return int
     */
    public function getSeanceDeliberante($seances)
    {
        foreach ($seances as $seanceId) {
            $seance = $this->find('first', [
                'conditions' => ['Seance.id' => $seanceId],
                'fields' => ['Seance.id'],
                'contain' => ['Typeseance.action'],
                'recursive' => -1
            ]);
            if ($seance['Typeseance']['action'] === 0) {
                return $seance['Seance']['id'];
            }
        }
        return null;
    }

    /**
     * @access public
     * @param type $seance_id
     * @return type
     *
     * @version 5.1.2
     */
    public function isDeliberante($seance_id)
    {
        $seance = $this->find('first', [
            'fields' => ['Seance.id', 'Seance.type_id'],
            'contain' => ['Typeseance.action'],
            'conditions' => ['Seance.id' => $seance_id],
            'recursive'=> -1
          ]);
        return ($seance['Typeseance']['action'] == 0);
    }

    /**
     * @version 4.3
     * @access public
     * @return type
     */
    public function getSeancesDeliberantes()
    {
        $tab_seances = [];
        $seances = $this->find('all', [
            'conditions' => [
                'Typeseance.action' => 0,
                'Seance.traitee' => 0],
            'fields' => ['Seance.id'],
            'contain' => ['Typeseance.action']]);
        foreach ($seances as $seance) {
            $tab_seances[] = $seance['Seance']['id'];
        }
        return !empty($tab_seances) ? $tab_seances : [];
    }

    /**
     * Fonction d'initialisation des variables de fusion pour plusieurs séances
     * les bibliothèques Gedooo doivent être inclues par avance
     * génère une exception en cas d'erreur
     *
     * @version 4.3
     * @access public
     * @param type $aData
     * @param object_by_ref $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param integer $ids --> liste des id des séances
     * @param boolean $addProjetIterations
     */
    public function setVariablesFusionSeances(
        &$aData,
        &$modelOdtInfos,
        $ids,
        $addProjetIterations = true,
        $variable = 'Seances'
    ) {
        // pour toutes les séances
        $aData['nombre_seance'] = ['value' => count($ids), 'type' => 'text'];
        foreach ($ids as $id) {
            $this->setVariablesFusion(
                $aData[$variable][],
                $modelOdtInfos,
                $id,
                'seances',
                $addProjetIterations
            );
            $this->clear();
        }
    }

    public function setVariablesFusionSeanceDeliberante(&$aData, &$modelOdtInfos, $id, $suffixe = 'seance')
    {
        // lecture de la séance en base
        $seance = $this->find('first', [
            'recursive' => -1,
            'fields' => ['id', 'date', 'date_convocation', 'commentaire', 'type_id', 'president_id', 'secretaire_id'],
            'conditions' => ['id' => $id]]);
        if (empty($seance)) {
            throw new Exception(__('séance  id: %s non trouvée en base de données', $id));
        }
        $dateSeanceTimeStamp = strtotime($seance['Seance']['date']);

        // fusion des variables
        if ($modelOdtInfos->hasUserFieldDeclared('date_' . $suffixe . '_lettres')) {
            $aData['date_' . $suffixe . '_lettres'] = [
                'value' => AppTools::dateLettres($dateSeanceTimeStamp),
                'type' => 'text'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('date_' . $suffixe)) {
            $aData['date_' . $suffixe] = [
                'value' => CakeTime::i18nFormat($seance['Seance']['date'], '%d/%m/%Y'),
                'type' => 'date'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('heure_' . $suffixe)) {
            $aData['heure_' . $suffixe] = [
                'value' => CakeTime::i18nFormat($seance['Seance']['date'], '%H:%M'),
                'type' => 'text'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('hh_' . $suffixe)) {
            $aData['hh_' . $suffixe] = [
                'value' => CakeTime::i18nFormat($seance['Seance']['date'], '%H'),
                'type' => 'text'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('mm_' . $suffixe)) {
            $aData['mm_' . $suffixe] = [
                'value' => CakeTime::i18nFormat($seance['Seance']['date'], '%M'),
                'type' => 'text'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('identifiant_' . $suffixe)) {
            $aData['identifiant_' . $suffixe] = ['value' => $seance['Seance']['id'], 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('type_' . $suffixe)) {
            $aData['type_' . $suffixe] = [
                'value' => $this->Typeseance->field('libelle', ['id' => $seance['Seance']['type_id']]),
                'type' => 'text'
            ];
        }

        if (!empty($seance['Seance']['president_id'])) {
            $this->PresidentSeance->setVariablesFusion(
                $aData,
                $modelOdtInfos,
                $seance['Seance']['president_id']
            );
        }

        if (!empty($seance['Seance']['secretaire_id'])) {
            $this->Secretaire->setVariablesFusion($aData, $modelOdtInfos, $seance['Seance']['secretaire_id']);
        }
    }

    /**
     * Fonction d'initialisation des variables de fusion pour une séance
     * les bibliothèques Gedooo doivent être inclues par avance
     * génère une exception en cas d'erreur
     *
     * @since 4.3
     * @version 5.1
     * @access public
     * @param type $aData
     * @param object_by_ref $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param integer $id --> id de l'occurence en base
     * @param string $suffixe --> suffixe des variables de fusion de la séance
     * @param boolean $addProjetIterations --> ajoute les itérations sur les projets
     * @throws Exception
     *
     * @SuppressWarnings(PHPMD)
     */
    public function setVariablesFusion(&$aData, &$modelOdtInfos, $id, $suffixe = 'seance', $addProjetIterations = true)
    {

        // lecture de la séance en base
        $seance = $this->find('first', [
            'recursive' => -1,
            'fields' => ['id', 'date', 'date_convocation', 'commentaire', 'type_id', 'president_id', 'secretaire_id'],
            'conditions' => ['id' => $id]]);
        if (empty($seance)) {
            throw new Exception(__('séance  id: %s non trouvée en base de données', $id));
        }
        $dateSeanceTimeStamp = strtotime($seance['Seance']['date']);

        // fusion des variables
        if ($modelOdtInfos->hasUserFieldDeclared('date_' . $suffixe . '_lettres')) {
            $aData['date_' . $suffixe . '_lettres'] = [
                'value' => AppTools::dateLettres($dateSeanceTimeStamp),
                'type' => 'text'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('date_' . $suffixe)) {
            $aData['date_' . $suffixe] = [
                'value' => CakeTime::i18nFormat($seance['Seance']['date'], '%d/%m/%Y'),
                'type' => 'date'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('heure_' . $suffixe)) {
            $aData['heure_' . $suffixe] = [
                'value' => CakeTime::i18nFormat($seance['Seance']['date'], '%H:%M'),
                'type' => 'text'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('hh_' . $suffixe)) {
            $aData['hh_' . $suffixe] = [
                'value' => CakeTime::i18nFormat($seance['Seance']['date'], '%H'),
                'type' => 'text'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('mm_' . $suffixe)) {
            $aData['mm_' . $suffixe] = [
                'value' => CakeTime::i18nFormat($seance['Seance']['date'], '%M'),
                'type' => 'text'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('date_convocation_' . $suffixe)) {
            $aData['date_convocation_' . $suffixe] = [
                'value' => (empty($seance['Seance']['date_convocation'])
                    ? '' : CakeTime::i18nFormat($seance['Seance']['date_convocation'], '%d/%m/%Y')),
                'type' => 'text'
            ];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('identifiant_' . $suffixe)) {
            $aData['identifiant_' . $suffixe] = ['value' => $seance['Seance']['id'], 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('commentaire_' . $suffixe)) {
            $aData['commentaire_' . $suffixe] = ['value' => $seance['Seance']['commentaire'], 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('type_' . $suffixe)) {
            $aData['type_' . $suffixe] = [
                'value' => $this->Typeseance->field('libelle', ['id' => $seance['Seance']['type_id']]),
                'type' => 'text'
            ];
        }

        // président de séance/projet
        if (!empty($seance['Seance']['president_id'])) {
            $this->PresidentSeance->setVariablesFusion($aData, $modelOdtInfos, $seance['Seance']['president_id']);
        }

        // secrétaire de séance
        if (!empty($seance['Seance']['secretaire_id'])) {
            $this->Secretaire->setVariablesFusion($aData, $modelOdtInfos, $seance['Seance']['secretaire_id']);
        }
        // Informations supplémentaires
        $this->Infosup->setVariablesFusion($aData, $modelOdtInfos, 'Seance', $id);

        $this->id = $id;

        // acteurs convoqués
        if ($modelOdtInfos->hasUserFieldsDeclared(
            'salutation_acteur_convoque',
            'prenom_acteur_convoque',
            'nom_acteur_convoque',
            'titre_acteur_convoque',
            'position_acteur_convoque',
            'email_acteur_convoque',
            'telmobile_acteur_convoque',
            'telfixe_acteur_convoque',
            'adresse1_acteur_convoque',
            'adresse2_acteur_convoque',
            'cp_acteur_convoque',
            'ville_acteur_convoque',
            'note_acteur_convoque'
        )) {
            $convoques = $this->Typeseance->acteursConvoquesParTypeSeanceId($seance['Seance']['type_id'], null, ['id']);

            $aSeance['nombre_acteur_' . $suffixe] = ['value' => count($convoques), 'type' => 'text']; //, "text"));
            if (!empty($convoques)) {
                $aConvoques = [];
                foreach ($convoques as $convoque) {
                    $aConvoques[] = $this->Secretaire->setVariablesFusion(
                        $aData['Convoques'],
                        $modelOdtInfos,
                        $convoque['Acteur']['id'],
                        'acteur_convoque'
                    );
                }
            }
        }

        if ($modelOdtInfos->hasUserFieldDeclared('debat_' . $suffixe)) {
            $this->recursive = -1;
            $debat = $this->findById($id, 'debat_global');
            if (!empty($debat['Seance']['debat_global'])) {
                $aData['debat_seance'] = ['value' => $debat['Seance']['debat_global'], 'type' => 'file'];
            }
        }

        // listes des présents et suppléants, absents, mandatés pour la séance
        $this->ListePresenceGlobale->setVariablesFusionPresents($aData, $modelOdtInfos, $id);
        $this->ListePresenceGlobale->setVariablesFusionAbsents($aData, $modelOdtInfos, $id);
        $this->ListePresenceGlobale->setVariablesFusionMandates($aData, $modelOdtInfos, $id);

        // projets/délibérations de la séance
        if ($addProjetIterations) {
            $aProjetIds = $this->getDeliberationsId($id);
            if (!empty($aProjetIds)) {
                foreach ($aProjetIds as $iProjetId) {
                    $this->Deliberation->setVariablesFusion($aData['Projets'][], $modelOdtInfos, $iProjetId, $id);
                }
            }
        }
    }

    /**
     * Fonction de callback du behavior OdtFusion : initialise les variables de fusion Gedooo
     *
     * @version 4.3
     * @access public
     * @param type $aData
     * @param object_by_ref $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param integer $id --> id de l'occurence en base de données
     * @param array $modelOptions --> options gérées par la classe appelante
     */
    public function beforeFusion(&$aData, &$modelOdtInfos, $id, $modelOptions)
    {
        switch ($modelOptions['modelTypeName']) {
            case 'convocation':
            case 'ordredujour':
            case 'pvsommaire':
            case 'pvdetaille':
            case 'journal_seance':
                if (!empty($modelOptions['acteurId'])) {
                    $this->Secretaire->setVariablesFusion(
                        $aData,
                        $modelOdtInfos,
                        $modelOptions['acteurId'],
                        $suffixe = 'acteur'
                    );
                }
                $this->setVariablesFusion($aData, $modelOdtInfos, $id, 'seance', true);
                break;
            case 'multiseances':
                $this->setVariablesFusionSeances($aData, $modelOdtInfos, $modelOptions['seanceIds']);
                break;
        }
    }

    /**
     * Fonction de callback du behavior OdtFusion : retourne l'id du model odt à utiliser pour la fusion
     *
     * @version 4.3
     * @access public
     * @param integer $id --> id de l'occurence en base de données
     * @param array $modelOptions --> options gérées par la classe appelante
     * @return integer --> id du modele odt à utiliser
     * @throws Exception
     */
    public function getModelTemplateId($id, $modelOptions)
    {
        // initialisation
        $allowedModelTypes = [
            'projet', 'deliberation', 'convocation', 'ordredujour',
            'pvsommaire', 'pvdetaille', 'journal_seance'
        ];
        if (!in_array($modelOptions['modelTypeName'], $allowedModelTypes, true)) {
            throw new Exception(
                __('le type de modèle d\'édition %s n\'est par autorisé', $modelOptions['modelTypeName'])
            );
        }
        // lecture de la séance en base de données
        $typeSeanceId = $this->field('type_id', ['id' => $id]);
        if (empty($typeSeanceId)) {
            throw new Exception(__('détermination du type de séance de la séance id: %s non trouvée', $id));
        }
        // lecture du modele_id liée au type de séance et au type du model d'édition
        $modelTemplateId = $this->Typeseance->field(
            'modele_' . $modelOptions['modelTypeName'] . '_id',
            ['id' => $typeSeanceId]
        );
        if (empty($modelTemplateId)) {
            throw new Exception(
                __(
                    'détermination du modèle d\'édition %s pour le type de séance id: %s non trouvé',
                    $modelOptions['modelTypeName'],
                    $typeSeanceId
                )
            );
        }
        return $modelTemplateId;
    }

    /**
     * Ordonne la fusion et retourne le résultat sous forme de flux
     *
     * @access public
     * @param int|string $id --> identifiant de la séance
     * @param string $modeltype --> type de fusion
     * @param int|string $modelTemplateId
     * @param string $format -> format du fichier de sortie
     * @return string --> flux du fichier généré
     */
    public function fusion($id, $modeltype, $modelTemplateId = null, $format = 'pdf')
    {
        $this->Behaviors->load('OdtFusion', [
            'id' => $id,
            'fileNameSuffixe' => $id,
            'modelTemplateId' => $modelTemplateId,
            'modelOptions' => ['modelTypeName' => $modeltype]
        ]);
        $this->odtFusion();
        $content = $this->getOdtFusionResult($format);
        $this->deleteOdtFusionResult();
        $this->Behaviors->unload('OdtFusion');
        return $content;
    }

    /**
     * Ordonne la fusion et retourne le résultat sous forme de flux
     *
     * @access public
     * @param int|string $id --> identifiant de la séance
     * @param string $modeltype --> type de fusion
     * @param int|string $modelTemplateId
     * @param string $outputdir --> fichier vers lequel faire la fusion
     * @param string $format --> format du fichier de sortie
     * @return array --> [filename => content]
     */
    public function fusionToFile($id, $modeltype, $modelTemplateId = null, $outputdir = TMP, $format = 'pdf')
    {
        $this->Behaviors->load('OdtFusion', [
            'id' => $id,
            'fileNameSuffixe' => $id,
            'modelTemplateId' => $modelTemplateId,
            'modelOptions' => ['modelTypeName' => $modeltype]
        ]);
        $this->odtFusion();
        $filename = $this->fusionName();
        $content = $this->getOdtFusionResult($format);
        $this->deleteOdtFusionResult();
        $this->Behaviors->unload('OdtFusion');
        $file = new File($outputdir . DS . $filename . '.' . $format, true);
        $file->write($content);
        return $file->path;
    }

    /**
     * [public description]
     * @var [type]
     *
     * @version 5.1.0
     */
    public function canSaveSeanceChildren()
    {
        if (!empty($this->data['Seance']['type_id']) && !empty($this->data['Seance']['SeanceChildren'])) {
            $this->Typeseance->recursive = -1;
            $typeseance = $this->Typeseance->findById($this->data['Seance']['type_id'], 'action');

            return $typeseance['Typeseance']['action'] == 0 ? true : false;
        }

        return true;
    }

    /**
     * [public description]
     * @var [type]
     *
     * @version 5.1.0
     */
    public function canSaveSeanceChildrenDeliberante()
    {
        if (!empty($this->data['Seance']['type_id']) && !empty($this->data['Seance']['SeanceChildren'])) {
            $this->Typeseance->recursive = -1;
            $typeseance = $this->Typeseance->findAllByParentId($this->data['Seance']['type_id'], ['id']);
            $typeseanceIds = Set::extract('/Typeseance/id', $typeseance);
            foreach ($this->data['Seance']['SeanceChildren'] as $seance_id) {
                $seance = $this->find('first', [
                'fields' => ['type_id'],
                'conditions' => ['Seance.id' => $seance_id],
                'recursive' => -1
                ]);
                if (!in_array((int)$seance['Seance']['type_id'], $typeseanceIds, true)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * [fileOutputFormatValues description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function fileOutputFormatValues($id)
    {
        $seance =  $this->find(
            'first',
            [
              'fields' => ['date', 'type_id'],
              'conditions' => ['id' => $id],
              'recursive' => -1
            ]
        );

        return [
          'seanceName' => $this->Typeseance->field('libelle', ['id' => $seance['Seance']['type_id']]),
          'seanceDate' => $seance['Seance']['date']
        ];
    }
}
