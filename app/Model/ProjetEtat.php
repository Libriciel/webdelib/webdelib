<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 *
 * === Deliberation.etat ===
 * Deliberation.etat = -3 : Abandonné
 * Deliberation.etat = -2 : Refusé
 * Deliberation.etat = -1 : Versionné
 * Deliberation.etat = 0 : En cours de rédaction
 * Deliberation.etat = 1 : En cours d'élaboration et de validation
 * Deliberation.etat = 2 : Validé
 * Deliberation.etat = 3 : Adopté (Voté pour)
 * Deliberation.etat = 4 : Rejeté (Voté contre)
 * Deliberation.etat = 5 : Envoyé au TdT
 * Deliberation.etat = 6 : Numéroté (non enregistré en BdD)
 * Deliberation.etat = 7 : Signé (non enregistré en BdD)
 * Deliberation.etat = 8 : Non télétransmis (non enregistré en BdD)
 * Deliberation.etat = 9 : En retard dans un circuit
 * Deliberation.etat = 10 : Renvoyer en signature // WIP
 * Deliberation.etat = 11 : Renvoyer au TdT// WIP
 *
 * @version   5.1.4
 * @since     1.0
 * @package   app.Controller
 *
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace, PSR1.Files.SideEffects
 */
class ProjetEtat extends AppModel
{
    public $useTable = false;

    /**
     * Retourne le libellé correspondant à l'état $etat des projets et délibérations
     * si $codesSpeciaux = true, retourne les libellés avec les codes spéciaux des accents
     * si $codesSpeciaux = false, retourne les libellés sans les accents (listes)
     *
     * @param  $etat
     * @param bool $nature_code
     * @return string
     * @version 4.3
     * @access public
     */
    public function libelleEtat($etat, $nature_code = null)
    {
        $labels = $this->getLibellesEtat();

        if (!empty($nature_code)
            && in_array($nature_code, ['AR', 'AI', 'CC', 'AU'], true)
            && in_array($etat, [2, 3, 4], true)
        ) {
            return $labels[2];
        }

        return $labels[$etat];
    }

    /**
     * Etats des délibérations
     *
     *
     * @param bool $alias Returns Alias if TRUE , labels if not
     * @return array
     * @version 5.1.0
     * @since 4.3.0
     * @access private
     */
    public function getLibellesEtat($alias = false)
    {
        $labels = [];
        if ($alias) {
            $labels = [
                -3 => __('ACTE_ABANDONNE'), // Acte Abandonné
                -2 => __('ACTE_NOUVELLE_VERSION'), // Acte Refusé
                -1 => __('ACTE_REFUSE'), // Acte Refusé
                0 => __('ACTE_EN_REDACTION'), // Acte en Cours de Rédaction
                1 => __('ACTE_EN_ELABORATION_VALIDATION'), // Acte en cour d'Elaboration et de Validation
                2 => __('ACTE_VALIDE'), // Acte validé
                6 => __('ACTE_NUMEROTE'), //Acte Numéroté
                3 => __('ACTE_VOTE_ADOPTE'), //Acte Voté et Adopté
                4 => __('ACTE_VOTE_REJETE'), //Acte Voté et Rejeté
                7 => __('ACTE_SIGNE'), //Acte Signé
                5 => __('ACTE_TELETRANSMIS'), // Acte télétransmis
                8 => __('ACTE_NON_TELETRANSMIS'), //Acte non télétransmis
                9 => __('ACTE_RETARD_CIRCUIT'), // Acte en Retard dans un Circuit
                10 => __('ACTE_RENVOYER_SIGNATURE'), // Acte à Revoyer en Signature
                11 => __('ACTE_TRANSMIS_MANUELLEMENT'), // Acte Transmis manuellemment
                100 => __('CODE_ERRONE') // Code Erroné
            ];
        } else {
            $labels = [
                -3 => __('Abandonné'),
                -2 => __('Nouvelle version'),
                -1 => __('Refusé'),
                0 => __('En cours de rédaction'),
                1 => __('En cours d\'élaboration et de validation'),
                2 => __('Validé'),
                6 => __('Numéroté'),
                3 => __('Voté et adopté'),
                4 => __('Voté et rejeté'),
                7 => __('Signé'),
                5 => __('Transmis au contrôle de légalité'),
                8 => __('Non télétransmis'),
                9 => __('En retard dans un circuit'),
                10 => __('À renvoyer en signature'),
                11 => __('Transmis manuellement au contrôle de légalité'),
                100 => __('Code Erroné')
            ];
        }
        return $labels;
    }
}
