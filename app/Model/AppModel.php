<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [AppModel description]
 * @version 5.1.2
 * @since 1.0.0
 *
 * @SuppressWarnings(PHPMD)
 */
class AppModel extends Model
{
    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);

        $this->Behaviors->load('Containable');
        $this->Behaviors->load('Database.DatabaseTable');
        $this->Behaviors->load('DynamicDbConfig');
    }
    /**
     * Validation du format de fichier par FIDO
     * @since 4.3
     * @version 5.1
     * @access public
     * @param string|array $data --> flux d'un fichier ou tableau de type HTTP Post
     * @param null|string|array $mimetype --> extension(s) autorisée(s), si null autorise
     * toutes celles du fichier formats.inc
     * @param boolean $required --> autoriser qu'il n'y ai pas de fichier
     * @return boolean --> fichier autorisé ou non
     */
    public function checkFormat($data, $mimetype = null, $required = false)
    {
        App::uses('Folder', 'Utility');
        App::uses('File', 'Utility');
        App::uses('AppTools', 'Lib');
        $folder_temp = new Folder(AppTools::newTmpDir(TMP . DS . 'files' . DS . 'upload'));
        if (is_array($data)) {
            $data = array_shift($data);
            if (isset($data['error'])) {
                if (!$required && !empty($data['error']) && $data['error'] == 4) {
                    return true;
                }

                if ($required && !empty($data['error']) && $data['error'] != 0) {
                    return false;
                }
                if (empty($data['size']) || !empty($data['error'])) {
                    $this->validate['content']['message'] = 'Erreur dans le document ou lors de l\'envoi.';
                    return false;
                }

                $file = new File($data['tmp_name'], false);
                //Création du répertoire de test
                $file_test = new File(
                    $folder_temp->pwd() . DS . 'file_test.'
                    . AppTools::getExtensionFile($data['name'])
                );
                $file_test->write($file->read());
            } else {
                $extension = '';
                if (!empty($mimetype)) {
                    if (is_array($mimetype)) {
                        $extension = '.' . AppTools::getTypemimesFileByTypemime(current($mimetype));
                    } else {
                        $extension = '.' . AppTools::getExtensionFile($mimetype);
                    }
                }
                $file_test = new File(
                    $folder_temp->pwd() . DS . sprintf('_content%s', $extension)
                );
                $file_test->write($data);
            }
        } else {
            if (empty($data)) {
                $folder_temp->delete();

                return !$required;
            }
            $file_test = new File(
                $folder_temp->pwd() . DS . '_content' . (!empty($mimetype) ? '.'
                    . AppTools::getExtensionFile($mimetype) : '')
            );
            $file_test->write($data);
        }
        $allowed = AppTools::fileMime($file_test->pwd());
        $file_test->close();

        $folder_temp->delete();
        if (!empty($allowed)) {
            if (empty($mimetype)) {               // Pas de mimetype fourni, on utilise le format.inc
                $typemimes = Set::extract('{n}.typemime', AppTools::getTypemimesFiles());
                if (!empty($allowed['mimetype']) && in_array($allowed['mimetype'], $typemimes, true)) {
                    return true;
                }
            } elseif (!empty($allowed['mimetype'])) { //mimetype fourni, on n'utilise pas le format.inc
                //mimetype sous forme de tableau
                if (is_array($mimetype) && in_array($allowed['mimetype'], $mimetype, true)) {
                    return true;
                }
                //mimetype sous forme string
                if (is_string($mimetype) && $allowed['mimetype'] === $mimetype) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @version 4.3
     * @access public
     * @param type $data
     * @return type
     */
    public function analyzeFile($data)
    {
        App::uses('FidoComponent', 'ModelOdtValidator.Controller/Component');
        $this->Fido = new FidoComponent();

        return $this->Fido->analyzeFile($data);
    }

    /**
     * @access public
     * @param type $model
     * @param type $id
     * @param type $field
     * @return type
     */
    public function changeBoolean($model, $id, $field)
    {
        $mod = new $model();
        $data = $mod->find('first', ['conditions' => ["$model.id" => $id],
            'recursive' => -1,
            'fields' => ["$field"]]);
        $mod->id = $id;
        return ($mod->saveField($field, !$data[$model][$field]));
    }

    /**
     *
     * @param type $field
     * @param type $value
     * @param type $id
     * @return boolean
     */
//    function isUnique($field, $value, $id)
//    {
//        $fields[$this->name . '.' . $field] = $value;
//        if (empty($id)) {         // add
//            $fields[$this->name . '.id'] = "!= NULL";
//        } else {        // edit
//            $fields[$this->name . '.id'] = "!= $id";
//        }
//
//        $this->recursive = -1;
//        if ($this->hasAny($fields)) {
//            $this->invalidate('unique_' . $field);
//            return false;
//        } else {
//            return true;
//        }
//    }

    /**
     * @access public
     * @param type $params
     * @return boolean
     */
    public function isUploadedFile($params)
    {
        $val = array_shift($params);
        if ((isset($val['error']) && $val['error'] == 0) ||
                (!empty($val['tmp_name']) && $val['tmp_name'] != 'none')
        ) {
            return true;
        }
        return false;
    }

    /**
     * Equivalent de la fonction cake field() mais retourne plusieurs valeurs sous forme de tableau
     *
     * @access public
     * @param type $fieldName
     * @param type $conditions
     * @param type $order
     * @return type
     */
    public function nfield($fieldName, $conditions = [], $order = [])
    {
        // initialisations
        $ret = [];
        $fields = [];
        $contain = [];

        // champ a lire
        $fields[] = $fieldName . ' DISTINCT';

        // ajout des champs des modeles liées pour la condition
        foreach ($conditions as $condField => &$cond) {
            if (strpos($condField, ' ') !== false) {
                $condField = substr($condField, 0, strpos($condField, ' '));
            }
            if (strpos($condField, '.') !== false) {
                $tabCondField = explode('.', $condField);
                $condModel = $tabCondField[0];
                if ($condModel != $this->alias) {
                    $contain[] = $condField;
                }
            }
        }
        // ajout des champs des modeles liées pour l'ordre
        foreach ($order as $orderField) {
            if (strpos($orderField, ' ') !== false) {
                $orderField = substr($orderField, 0, strpos($orderField, ' '));
            }
            if (strpos($orderField, '.') !== false) {
                $tabOrderField = explode('.', $orderField);
                $orderModel = $tabOrderField[0];
                if ($orderModel != $this->alias) {
                    $contain[] = $orderField;
                } else {
                    $fields[] = $orderField;
                }
            } else {
                $fields[] = $orderField;
            }
        }

        // lecture en base
        $this->Behaviors->load('Containable');
        $occurs = $this->find('all', [
            'fields' => $fields,
            'contain' => $contain,
            'conditions' => $conditions,
            'order' => $order]);

        // constitution de la liste
        foreach ($occurs as $occur) {
            $ret[] = $occur[$this->alias][$fieldName];
        }
        return $ret;
    }

    public function paginateCount($conditions = null, $recursive = 0, $extra = [])
    {
        $parameters = compact('conditions');
        if ($recursive != $this->recursive) {
            $parameters['recursive'] = $recursive;
        }

        if (!empty($extra['countField'])) {
            $parameters['fields'] = $extra['countField'];
            unset($extra['countField']);
        }

        return $this->find('count', array_merge($parameters, $extra));
    }

    /**
     * [beforeValidate description]
     * @param  array  $options [description]
     * @return [type]          [description]
     * @version 5.1.0
     */
    public function beforeValidate($options = [])
    {
        foreach (array_keys($this->hasAndBelongsToMany) as $model) {
            if (isset($this->data[$model][$model])) {
                $this->data[$this->name][$model] = $this->data[$model][$model];
            }
        }
        return true;
    }

    /**
     * [afterValidate description]
     * @param  array  $options [description]
     * @return [type]          [description]
     * @version 5.1.0
     */
    public function afterValidate($options = [])
    {
        foreach (array_keys($this->hasAndBelongsToMany) as $model) {
            unset($this->data[$this->name][$model]);
            if (isset($this->validationErrors[$model])) {
                $this->$model->validationErrors[$model] = $this->validationErrors[$model];
            }
        }
        return true;
    }

    /**
     * @param string $model
     * @param string $crud
     * @return array
     */
    public function allowAclConditions($model, $crud = 'read')
    {
        $db = $this->getDataSource();
        $Aro = ClassRegistry::init('Aro');

        $Aro->Behaviors->attach('Database.DatabaseTable');
        $Aro->Permission->Behaviors->attach('Database.DatabaseTable');
        $Aro->Permission->Aco->Behaviors->attach('Database.DatabaseTable');

        $permissions = [];
        if (is_array($crud)) {
            foreach ($crud as $key => $permission) {
                array_push($permissions, ['Permission._' . $permission => 1]);
            }
        } else {
            $permissions = ['Permission._' . $crud => 1];
        }

        $subQuery = [
            'fields' => [
                'Aco.foreign_key'
            ],
            'contain' => false,
            'joins' => [
                $Aro->join('Permission', ['type' => 'INNER']),
                $Aro->Permission->join('Aco', ['type' => 'INNER'])
            ],
            'conditions' => [
                'Aro.foreign_key' => AuthComponent::user('id'),
                'Aco.model' => $model
            ] + $permissions
        ];

        $subQuery = $Aro->sql($subQuery);
        $subQuery = ' "' . $model . '"."id" IN (' . $subQuery . ') ';
        $conditions[] = $db->expression($subQuery);

        return $conditions;
    }

    /**
     * [allowAcl description]
     * @param  [array|string] find 'allow'
     * @param  [string] $model Nom du model
     * @param  [string] $acl  Champ de le condition
     * @return [array|boolean] Tableau des conditions de recherche du find
     *
     * @version 5.1.0
     */
    public function allowAcl($allow, $model, $acl)
    {
        //FIX avec behavior
        if (!empty($allow) && (in_array($acl, $allow, true) xor array_key_exists($acl, $allow) xor $allow === $acl)) {
            if (!array_key_exists($acl, $allow)) {
                return $this->allowAclConditions($model);
            } elseif (array_key_exists($acl, $allow)) {
                return $this->allowAclConditions($model, $allow[$acl]);
            }
        }

        return false;
    }

    /**
     * Validation du nom de fichier lors d'un upload de fichier
     * @param string|array $check flux d'un fichier ou tableau de type HTTP Post
     * @return boolean nom de fichier autorisé ou non
     */
    public function uploadFileName($check)
    {
        if (is_array($check)) {
            $regex = '/^[a-zA-Z0-9-_.&]{0,}$/ui';
            $check = array_shift($check);
            if (isset($check['name'])) {
                return preg_match($regex, $check['name']);
            }

            return preg_match($regex, $check);
        }

        return false;
    }

    public function getTenantName()
    {
        return $this->useDbConfig;
    }
}
