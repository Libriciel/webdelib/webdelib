<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppTools', 'Lib');

class ListePresenceGlobale extends AppModel
{
    public $name = 'ListePresenceGlobale';
    public $cacheQueries = false;
    public $belongsTo = [
        'Seance' =>
        ['className' => 'Seance',
            'foreignKey' => 'seance_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => ''
        ],
        'Acteur' =>
        ['className' => 'Acteur',
            'foreignKey' => 'acteur_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => ''
        ],
        'Suppleant' =>
        ['className' => 'Acteur',
            'foreignKey' => 'suppleant_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => ''
        ],
        'Mandataire' =>
        ['className' => 'Acteur',
            'foreignKey' => 'mandataire',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => ''
        ]
    ];

    /**
     * Fonction d'initialisation des variables de fusion pour la liste des presents pour une séance
     *
     * @param array $aData
     * @param PhpOdtApi $modelOdtInfos Objet PhpOdtApi du fichier odt du modèle d'édition
     * @param integer $seanceId Id de la séance
     */
    public function setVariablesFusionPresents(&$aData, &$modelOdtInfos, $seanceId)
    {
        // initialisations
        $fusionVariables = [
            'nom', 'prenom', 'salutation', 'titre', 'adresse1', 'adresse2',
            'cp', 'ville', 'email', 'telfixe', 'telmobile', 'note'
        ];
        $conditions = [
            'ListePresenceGlobale.seance_id' => $seanceId,
            'ListePresenceGlobale.present' => true
        ];

        // nombre d'acteurs présents
        $nbActeurs = $this->find('count', ['recursive' => -1, 'conditions' => $conditions]);
        if ($modelOdtInfos->hasUserFieldDeclared('SeanceActeursPresents_nombre')) {
            $aData['SeanceActeursPresents_nombre'] = ['value' => $nbActeurs, 'type' => 'text'];
        }

        // liste des variables utilisées dans le template
        $acteurFields = $aliasActeurFields = [];
        foreach ($fusionVariables as $fusionVariable) {
            if ($modelOdtInfos->hasUserFieldDeclared(
                sprintf('SeanceActeursPresents_acteur_%s', $fusionVariable)
            )
            ) {
                $aliasActeurFields[] = 'Acteur.' . $fusionVariable;
                $acteurFields[] = $fusionVariable;
            }
        }

        if (empty($aliasActeurFields)) {
            return;
        }

        // lecture des données en base de données
        $acteurs = $this->find('all', [
            'fields' => ['ListePresenceGlobale.suppleant_id'],
            'contain' => $aliasActeurFields,
            'conditions' => $conditions,
            'order' => 'Acteur.position ASC']);

        $aActeursPresents = [];
        // itérations sur les acteurs présents
        if (empty($nbActeurs)) {
            $aActeurPresent = [];
            foreach ($acteurFields as $fieldname) {
                $aActeurPresent[sprintf('SeanceActeursPresents_acteur_%s', $fieldname)] =
                    ['value' => '', 'type' => 'text'];
            }
            $aActeursPresents[] = $aActeurPresent;
        } else {
            foreach ($acteurs as &$acteur) {
                // traitement du suppléant
                if (!empty($acteur['ListePresenceGlobale']['suppleant_id'])) {
                    $suppleant = $this->Acteur->find('first', [
                        'recursive' => -1,
                        'fields' => $aliasActeurFields,
                        'conditions' => ['id' => $acteur['ListePresenceGlobale']['suppleant_id']]]);
                    if (empty($suppleant)) {
                        throw new Exception(
                            'suppléant non trouvé id:' . $acteur['ListePresenceGlobale']['suppleant_id']
                        );
                    }
                    $acteur = &$suppleant;
                }

                $aActeurPresent = [];
                foreach ($acteurFields as $fieldname) {
                    $aActeurPresent[sprintf('SeanceActeursPresents_acteur_%s', $fieldname)] = [
                        'value' => $acteur['Acteur'][$fieldname], 'type' => 'text'
                    ];
                }
                $aActeursPresents[] = $aActeurPresent;
            }
        }
        $aData['SeanceActeursPresents'] = $aActeursPresents;
    }

    /**
     * Fonction d'initialisation des variables de fusion pour la liste des absents pour une séance
     *
     * @param array $aData
     * @param PhpOdtApi $modelOdtInfos Objet PhpOdtApi du fichier odt du modèle d'édition
     * @param integer $seanceId Id de la séance
     */
    public function setVariablesFusionAbsents(&$aData, &$modelOdtInfos, $seanceId)
    {
        // initialisations
        $fusionVariables = [
            'nom', 'prenom', 'salutation', 'titre', 'adresse1', 'adresse2',
            'cp', 'ville', 'email', 'telfixe', 'telmobile', 'note'
        ];
        $conditions = [
            'ListePresenceGlobale.seance_id' => $seanceId,
            'ListePresenceGlobale.present' => false,
            'ListePresenceGlobale.mandataire' => null
        ];

        // nombre d'acteurs absents
        $nbActeurs = $this->find('count', ['recursive' => -1, 'conditions' => $conditions]);
        if ($modelOdtInfos->hasUserFieldDeclared('SeanceActeursAbsents_nombre')) {
            $aData['SeanceActeursAbsents_nombre'] = ['value' => $nbActeurs, 'type' => 'text'];
        }

        // liste des variables utilisées dans le template
        $acteurFields = $aliasActeurFields = [];
        foreach ($fusionVariables as $fusionVariable) {
            if ($modelOdtInfos->hasUserFieldDeclared(
                sprintf('SeanceActeursAbsents_acteur_%s', $fusionVariable)
            )) {
                $aliasActeurFields[] = 'Acteur.' . $fusionVariable;
                $acteurFields[] = $fusionVariable;
            }
        }
        if (empty($aliasActeurFields)) {
            return false;
        }

        // lecture des données en base de données
        $acteurs = $this->find('all', [
            'fields' => ['ListePresenceGlobale.id'],
            'contain' => $aliasActeurFields,
            'conditions' => $conditions,
            'order' => ['Acteur.position' => 'ASC']]);

        $aActeursAbsents = [];
        if (empty($nbActeurs)) {
            $aActeurAbsent = [];
            foreach ($acteurFields as $fieldname) {
                $aActeurAbsent[sprintf('SeanceActeursAbsents_acteur_%s', $fieldname)] = [
                    'value' => '',
                    'type' => 'text'
                ];
            }
            $aActeursAbsents[] = $aActeurAbsent;
        } else {
            foreach ($acteurs as $acteur) {
                $aActeurAbsent = [];
                foreach ($acteurFields as $fieldname) {
                    $aActeurAbsent[sprintf('SeanceActeursAbsents_acteur_%s', $fieldname)] = [
                        'value' => $acteur['Acteur'][$fieldname],
                        'type' => 'text'
                    ];
                }

                $aActeursAbsents[] = $aActeurAbsent;
            }
        }

        $aData['SeanceActeursAbsents'] = $aActeursAbsents;
    }

    /**
     * Fonction d'initialisation des variables de fusion pour la liste des mandatés pour une séance
     *
     * @param array $aData
     * @param PhpOdtApi $modelOdtInfos Objet PhpOdtApi du fichier odt du modèle d'édition
     * @param integer $seanceId Id de la séance
     */
    public function setVariablesFusionMandates(&$aData, &$modelOdtInfos, $seanceId)
    {
        // initialisations
        $fusionVariables = [
            'nom', 'prenom', 'salutation', 'titre', 'adresse1', 'adresse2',
            'cp', 'ville', 'email', 'telfixe', 'telmobile', 'note'
        ];
        $conditions = [
            'ListePresenceGlobale.seance_id' => $seanceId,
            'ListePresenceGlobale.present' => false, 'ListePresenceGlobale.mandataire <>' => null
        ];

        // nombre d'acteurs mandatés
        $nbActeurs = $this->find('count', ['recursive' => -1, 'conditions' => $conditions]);
        if ($modelOdtInfos->hasUserFieldDeclared('SeanceActeursMandataires_nombre')) {
            $aData['SeanceActeursMandataires_nombre'] = ['value' => $nbActeurs, 'type' => 'text'];
        }

        // liste des variables utilisées dans le template
        $acteurFields = $aliasActeurFields = $mandateFields = $aliasMandateFields = [];
        foreach ($fusionVariables as $fusionVariable) {
            if ($modelOdtInfos->hasUserFieldDeclared(
                sprintf('SeanceActeursMandataires_acteur_%s', $fusionVariable)
            )) {
                $aliasActeurFields[] = 'Acteur.' . $fusionVariable;
                $acteurFields[] = $fusionVariable;
            }
            if ($modelOdtInfos->hasUserFieldDeclared(
                sprintf('SeanceActeursMandataires_acteur_mandate_%s', $fusionVariable)
            )) {
                $aliasMandateFields[] = 'Mandataire.' . $fusionVariable;
                $mandateFields[] = $fusionVariable;
            }
        }
        if (empty($aliasActeurFields) && empty($aliasMandateFields)) {
            return false;
        }

        // lecture des données en base de données
        $acteurs = $this->find('all', [
            'fields' => ['ListePresenceGlobale.id'],
            'contain' => array_merge($aliasActeurFields, $aliasMandateFields),
            'conditions' => $conditions,
            'order' => 'Acteur.position ASC']);

        $aActeursMandates = [];
        if (empty($nbActeurs)) {
            $aActeurMandate = [];
            foreach ($acteurFields as $fieldname) {
                $aActeurMandate[sprintf('SeanceActeursMandataires_acteur_%s', $fieldname)] = [
                    'value' => '',
                    'type' => 'text'
                ];
            }
            foreach ($mandateFields as $fieldname) {
                $aActeurMandate[sprintf('SeanceActeursMandataires_acteur_mandate_%s', $fieldname)] = [
                    'value' => '',
                    'type' => 'text'
                ];
            }

            $aActeursMandates[] = $aActeurMandate;
        } else {
            foreach ($acteurs as &$acteur) {
                $aActeurMandate = [];
                foreach ($acteurFields as $fieldname) {
                    $aActeurMandate[sprintf('SeanceActeursMandataires_acteur_%s', $fieldname)] = [
                        'value' => $acteur['Acteur'][$fieldname], 'type' => 'text'
                    ];
                }
                foreach ($mandateFields as $fieldname) {
                    $aActeurMandate[sprintf('SeanceActeursMandataires_acteur_mandate_%s', $fieldname)] = [
                        'value' => $acteur['Mandataire'][$fieldname], 'type' => 'text'
                    ];
                }

                $aActeursMandates[] = $aActeurMandate;
            }
        }

        $aData['SeanceActeursMandataires']  = $aActeursMandates;
    }
}
