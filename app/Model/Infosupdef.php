<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppTools', 'Lib');

/**
 * [Infosupdef description]
 *
 * @SuppressWarnings(PHPMD)
 */
class Infosupdef extends AppModel
{
    public $name = 'Infosupdef';
    public $displayField = "nom";
    public $hasMany = [
        'Infosup',
        'Infosuplistedef'
    ];
    public $hasAndBelongsToMany = ['Profil'];
    public $validate = [
        'nom' => [
            [
                'rule' => 'notBlank',
                'message' => 'Entrer un nom pour l\'information supplémentaire'
            ],
            [
                'rule' => 'isUnique',
                'message' => 'Entrer un autre nom, celui-ci est déjà utilisé.'
            ]
        ],
        'type' => [
            [
                'rule' => 'notBlank',
                'message' => 'Selectionner un type'
            ]
        ],
        'code' => [
            [
                'rule' => 'notBlank',
                'message' => 'Entrez un code pour l\'information supplémentaire'
            ],
            [
                'rule' => 'isUnique',
                'message' => 'Entrez un autre code, celui-ci est déjà utilisé.'
            ],
            [
                'rule' => 'nonConformeCode',
                'message' => 'Le code est non conforme, essayer celui ci : %s'
            ],
            [
                'rule' => 'conformeBaliseXml',
                'message' => 'Le code n\'est pas conforme (%s)'
            ],
            [
                'rule' => 'notInModels',
                'message' => 'Le code est déjà utilisé dans les variables de modèles'
            ]
        ],
//        'api' => [
//            [
//                'rule' => 'notBlank',
//                'message' => 'Indiquez une adresse d\'API pour l\'information supplémentaire'
//            ]
//        ]
    ];
    public $types = [
        'boolean' => 'Booléen',
        'date' => 'Date',
        'file' => 'Fichier',
        'odtFile' => 'Fichier ODT',
        'geojson' => 'Géolocalisation',
        'list' => 'Liste',
        'listmulti' => 'Liste à choix multiple',
        'text' => 'Texte',
        'richText' => 'Texte enrichi',
    ];
    public $listSelectBoolean = [
        '1' => 'Oui',
        '0' => 'Non'
    ];
    public $listEditBoolean = [
        '0' => 'Décoché',
        '1' => 'Coché'
    ];

    public $resultTypes = ['Voie', 'POI'];

    /**
     * [afterValidate description]
     * @param array $options [description]
     * @return true [type] [description]
     */
    public function afterValidate($options = [])
    {
        if (isset($this->data['Infosupdef']['code'])
            && $this->data['Infosupdef']['code'] !== Inflector::variable($this->data['Infosupdef']['code'])
        ) {
            foreach ($this->validationErrors as $field => $messages) {
                foreach ($messages as $key => $message) {
                    if ($field === 'code') {
                        $this->validationErrors[$field][$key] = __(
                            $message,
                            Inflector::variable($this->data['Infosupdef']['code'])
                        );
                    }
                }
            }
        }
        return true;
    }

    /**
     * @access public
     * @return boolean
     */
    public function nonConformeCode()
    {
        return $this->data['Infosupdef']['code'] === Inflector::variable($this->data['Infosupdef']['code']);
    }


    /**
     * @param array $check
     * @return boolean|string
     */
    public function conformeBaliseXml($check)
    {
        $return = true;
        $valeur = array_values($check);
        $valeur = $valeur[0];
        if (preg_match("/^xml+/i", $valeur)) {
            return 'Le code n\'est pas conforme, il ne peut pas commencer par le mot "xml"';
        }
        if (preg_match("/^\d+[a-zA-Z0-9]+$/i", $valeur)) {
            return 'Le code n\'est pas conforme, il ne peut pas commencer par un chiffre.';
        }

        return $return;
    }

    /**
     * @version 5.1
     * @return boolean
     */

    public function notInModels($check)
    {
        $modelVariable = ClassRegistry::init('ModelOdtValidator.Modelvariable');

        return $modelVariable->checkPresence($check['code']);
    }
    /**
     * Retourne la liste code/libellé pour les types d'information
     * @return string
     */
    public function generateListType()
    {
        return $this->types;
    }

    /**
     * Retourne le libellé correspondant au type $type
     * @param string $type
     * @return string
     */
    public function libelleType($type)
    {
        return $this->types[$type];
    }

    /**
     * Retourne le libellé correspondant au booléen $recherche
     * @param string $recherche
     * @return string
     */
    public function libelleRecherche($recherche)
    {
        return $recherche ? 'Oui' : 'Non';
    }

    /**
     * Retourne le libellé correspondant au booléen $actif
     * @param string $actif
     * @return string
     */
    public function libelleActif($actif)
    {
        return $actif ? 'Oui' : 'Non';
    }

    /**
     * Détermine si une occurrence peut être supprimée
     * @param integer $id id de l'occurrence à supprimer
     * @return boolean true si l'instance peut être supprimée et false dans le cas contraire
     */
    public function isDeletable($id)
    {
        // si utilisée alors on ne peut pas la supprimer
        return !$this->Infosup->hasAny(['infosupdef_id' => $id]);
    }

    /**
     * Intervertit l'ordre de l'élément $id avec le suivant ou le précédent suivant $following
     * @param int $id
     * @param boolean $following
     * @return void
     */
    public function invert($id = null, $following = true)
    {
        // Initialisations
        $gap = $following ? 1 : -1;

        // lecture de l'élément à déplacer
        $recFrom = $this->find('first', [
            'fields' => ['id', 'model', 'ordre'],
            'conditions' => ['id' => $id],
            'recursive' => -1,
          ]);

        // lecture de l'élément a intervertir
        $recTo = $this->find('first', [

            'fields' => ['id', 'model', 'ordre'],
            'conditions' => [
                'model' => $recFrom['Infosupdef']['model'],
                'actif' => true,
                'ordre' => ($recFrom['Infosupdef']['ordre'] + $gap)],
              'recursive' => -1,
              ]);

        // Si pas d'élément à intervertir alors on sort sans rien faire
        if (empty($recTo)) {
            return;
        }

        // Mise à jour du champ ordre pour les deux enregistrements
        $recFrom['Infosupdef']['ordre'] += $gap;
        $this->save($recFrom, false);
        $recTo['Infosupdef']['ordre'] -= $gap;
        $this->save($recTo, false);
    }

    /**
     * [beforeSave description]
     * @param  array  $options [description]
     * @return boolean [description]
     */
    public function beforeSave($options = [])
    {
        // Reloading order numbers when adding or active
        if ((empty($this->id) && empty($this->data['Infosupdef']['id']))
            || !empty($this->data['Infosupdef']['actif'])
        ) {
            $this->data['Infosupdef']['ordre'] = $this->find('count', [
              'conditions' => [
                'model' => $this->data['Infosupdef']['model'],
                'actif' => true
              ],
              'recursive' => -1
              ]) + 1;
        }
        // No search possible for additional information like file and file odt
        if (isset($this->data['Infosupdef']['type'])
            && ($this->data['Infosupdef']['type'] === 'file'
            || $this->data['Infosupdef']['type'] === 'odtFile')
        ) {
            $this->data['Infosupdef']['recherche'] = 0;
            $this->data['Infosupdef']['val_initiale'] = '';
            if ($this->data['Infosupdef']['type'] === 'file') {
                $DOC_TYPE = Configure::read('DOC_TYPE');
                $typemimes = AppTools::getTypemimesFilesForFusion();
                $typemimes = Set::extract($typemimes, '/typemime');
                if (!in_array($this->data['Infosupdef']['typemime'], $typemimes, true)) {
                    $this->data['Infosupdef']['joindre_fusion'] = false;
                }
            }
        }

        return true;
    }

    /**
     * [afterSave description]
     * @param  boolean $created [description]
     * @param  array   $options [description]
     */
    public function afterSave($created, $options = [])
    {
        // Reloading sequence numbers in case of activation / deactivation
        if (!$created && isset($this->data['Infosupdef']['actif'])) {
            $this->reorder($this->data['Infosupdef']['model']);
        }
    }

    /**
     * Réordonne les numéros d'ordre après une suppression
     *
     * @access public
     */
    public function afterDelete()
    {
        $models = ['Deliberation', 'Seance'];
        foreach ($models as $model) {
            $recs = $this->find('all', [
                'fields' => ['id', 'ordre'],
                'conditions' => [
                  'model' => $model,
                  'actif' => true
                ],
                'order' => ['ordre'],
                'recursive' => -1
              ]);

            foreach ($recs as $n => $rec) {
                if (($n + 1) != $rec['Infosupdef']['ordre']) {
                    $rec['Infosupdef']['ordre'] = ($n + 1);
                    $this->save($rec, false);
                }
            }
        }
    }

    /**
     * Retourne un tableau ['code']['val_init'] des valeurs initiales des infosup

     * @param string $model
     * @return array
     */
    public function valeursInitiales($model, $id = null)
    {
        $ret = [];
        $conditions = ['model' => $model];

        if (!empty($id)) {
            $conditions['id'] = $id;
        }

        $recs = $this->find('all', [
            'recursive' => -1,
            'conditions' => $conditions,
            'fields' => ['code', 'val_initiale'],
            'order' => ['ordre']]);

        foreach ($recs as $rec) {
            if (!empty($rec['Infosupdef']['val_initiale'])) {
                $ret[$rec['Infosupdef']['code']] = $rec['Infosupdef']['val_initiale'];
            }
        }

        return $ret;
    }

    /**
     * Retourne les éléments des infosup de type 'list'
     * @param string $model
     * @return array
     */
    public function generateListes($model)
    {
        $ret = [];

        $recs = $this->find('all', [
            'recursive' => -1,
            'fields' => ['id', 'code'],
            'conditions' => [
                'type' => ['list', 'listmulti'],
                'model' => $model,
                'actif' => true
            ],
            'order' => 'ordre'
        ]);

        foreach ($recs as $rec) {
            $ret[$rec['Infosupdef']['code']] = $this->Infosuplistedef->find('list', [
                'fields' => ['id', 'nom'],
                'conditions' => [
                    'actif' => true,
                    'infosupdef_id' => $rec['Infosupdef']['id']
                ],
                'order' => ['ordre']]);
        }

        return $ret;
    }

    /**
     * Retourne les éléments des infosup de type 'list'
     * @param string $model
     * @return boolean
     */
    public function isInfosupdefExistForModel($model)
    {
        return !empty($this->find('first', ['conditions'=> [
          'model' => $model,
          'actif' => true
        ]]));
    }

    /**
     * reorder description
     * @param string $model [description]
     * @throws Exception
     */
    private function reorder($model)
    {
        $infosupdefs = $this->find(
            'all',
            [
              'fields' => ['id', 'ordre', 'modified', 'actif'],
              'conditions' => ['model' => $model],
              'order' => ['ordre' => 'ASC', 'modified' => 'DESC'],
              'recursive' => -1,
            ]
        );

        $position = 1;
        foreach ($infosupdefs as $infosupdef) {
            $this->save(['Infosupdef' => [
            'id'=> $infosupdef['Infosupdef']['id'],
            'modified' => false,
            'ordre' => $infosupdef['Infosupdef']['actif'] ? $position : 0]], [
            'fieldList' => ['ordre'],
            'validate' => false,
            'callbacks' => false]);

            if ($infosupdef['Infosupdef']['actif']) {
                $position++;
            }

            $this->clear();
        }
    }

    /**
     * Retourne le nom d'une information supplémentaire via le code
     * @param string $model
     * @param string $code
     * @return string
     * @throws Exception
     */
    public function getLabel($model, $code)
    {
        if (empty($model) || empty($code)) {
            throw new CakeException("Error Processing Request Label");
        }
        $this->recursive = -1;
        $infosupdef = $this->findByModelAndCode($model, $code);

        return $infosupdef['Infosupdef']['nom'];
    }

    /**
     * Retourne l'extension d'une information supplémentaire via le code
     * @param string $model
     * @param string $code
     *
     * @return string
     *
     * @version 7.0.1
     */
    public function getFileTypemimeByModelAndCode($model, $code)
    {
        $this->recursive = -1;
        $infosupdef = $this->findByModelAndCode($model, $code);

        return $infosupdef['Infosupdef']['type'] === 'odtFile'
            ? 'application/vnd.oasis.opendocument.text' : $infosupdef['Infosupdef']['typemime'];
    }

    /**
     * Retourne la liste des types de résulats retournés par le geojson
     *
     * @access public
     */
    public function generateResultType()
    {
        return $this->resultTypes;
    }
}
