<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppTools', 'Lib');

/**
 * [Listepresence description]
 *
 * @version     v5.0
 * @since       v4.3
 * @SuppressWarnings(PHPMD)
 *
 */
class Listepresence extends AppModel
{
    public $name = 'Listepresence';
    public $cacheQueries = false;
    public $belongsTo = [
        'Deliberation' =>
        ['className' => 'Deliberation',
            'foreignKey' => 'delib_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => ''
        ],
        'Acteur' =>
        ['className' => 'Acteur',
            'foreignKey' => 'acteur_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => ''
        ],
        'Suppleant' =>
        ['className' => 'Acteur',
            'foreignKey' => 'suppleant_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => ''
        ],
        'Mandataire' =>
        ['className' => 'Acteur',
            'foreignKey' => 'mandataire',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => ''
        ]
    ];

    /**
     * Fonction d'initialisation des variables de fusion pour la liste des présents pour le vote d'un projet
     * les bibliothèques Gedooo doivent être inclues par avance
     * génère une exception en cas d'erreur
     *
     * @version 5.0
     * @since 4.3
     * @access public
     * @param type $aData
     * @param object_by_ref $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param integer $deliberationId --> id de la délibération
     * @throws Exception
     */
    public function setVariablesFusionPresents(&$aData, &$modelOdtInfos, $deliberationId)
    {
        // initialisations
        $fusionVariables = [
            'nom', 'prenom', 'salutation', 'titre', 'adresse1', 'adresse2',
            'cp', 'ville', 'email', 'telfixe', 'telmobile', 'note'
        ];
        $conditions = [
            'Listepresence.delib_id' => $deliberationId,
            'Listepresence.present' => true
        ];

        // nombre d'acteurs présents
        $nbActeurs = $this->find('count', ['recursive' => -1, 'conditions' => $conditions]);
        if ($modelOdtInfos->hasUserFieldDeclared('nombre_acteur_present')) {
            $aData['nombre_acteur_present'] = ['value' => $nbActeurs, 'type' => 'text'];
        }

        // liste des variables utilisées dans le template
        $acteurFields = $aliasActeurFields = [];
        foreach ($fusionVariables as $fusionVariable) {
            if ($modelOdtInfos->hasUserFieldDeclared($fusionVariable . '_acteur_present')) {
                $aliasActeurFields[] = 'Acteur.' . $fusionVariable;
                $acteurFields[] = $fusionVariable;
            }
        }

        if (empty($aliasActeurFields)) {
            return;
        }

        // lecture des données en base de données
        $acteurs = $this->find('all', [
            'fields' => ['Listepresence.suppleant_id'],
            'contain' => $aliasActeurFields,
            'conditions' => $conditions,
            'order' => 'Acteur.position ASC']);

        $aActeursPresents = [];
        // itérations sur les acteurs présents
        if (empty($nbActeurs)) {
            $aActeurPresent = [];
            foreach ($acteurFields as $fieldname) {
                $aActeurPresent[$fieldname . '_acteur_present'] = ['value' => '', 'type' => 'text'];
            }
            $aActeursPresents[] = $aActeurPresent;
        } else {
            foreach ($acteurs as &$acteur) {
                // traitement du suppléant
                if (!empty($acteur['Listepresence']['suppleant_id'])) {
                    $suppleant = $this->Acteur->find('first', [
                        'recursive' => -1,
                        'fields' => $aliasActeurFields,
                        'conditions' => ['id' => $acteur['Listepresence']['suppleant_id']]]);
                    if (empty($suppleant)) {
                        throw new Exception(
                            'suppléant non trouvé id:' . $acteur['Listepresence']['suppleant_id']
                        );
                    }
                    $acteur = &$suppleant;
                }

                $aActeurPresent = [];
                foreach ($acteurFields as $fieldname) {
                    $aActeurPresent[$fieldname . '_acteur_present'] = [
                        'value' => $acteur['Acteur'][$fieldname], 'type' => 'text'
                    ];
                }
                $aActeursPresents[] = $aActeurPresent;
            }
        }

        $aData['ActeursPresents'] = $aActeursPresents;
    }

    /**
     * Fonction d'initialisation des variables de fusion pour la liste des absents pour le vote d'un projet
     * les bibliothèques Gedooo doivent être inclues par avance
     * génère une exception en cas d'erreur
     *
     * @version 5.0
     * @since 4.3
     * @access public
     * @param type $aData
     * @param object_by_ref $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param integer $deliberationId --> id de la délibération
     */
    public function setVariablesFusionAbsents(&$aData, &$modelOdtInfos, $deliberationId)
    {
        // initialisations
        $fusionVariables = [
            'nom', 'prenom', 'salutation', 'titre', 'adresse1', 'adresse2',
            'cp', 'ville', 'email', 'telfixe', 'telmobile', 'note'
        ];
        $conditions = [
            'Listepresence.delib_id' => $deliberationId,
            'Listepresence.present' => false,
            'Listepresence.mandataire' => null
        ];

        // nombre d'acteurs absents
        $nbActeurs = $this->find('count', ['recursive' => -1, 'conditions' => $conditions]);
        if ($modelOdtInfos->hasUserFieldDeclared('nombre_acteur_absent')) {
            $aData['nombre_acteur_absent'] = ['value' => $nbActeurs, 'type' => 'text'];
        }
        // liste des variables utilisées dans le template
        $acteurFields = $aliasActeurFields = [];
        foreach ($fusionVariables as $fusionVariable) {
            if ($modelOdtInfos->hasUserFieldDeclared($fusionVariable . '_acteur_absent')) {
                $aliasActeurFields[] = 'Acteur.' . $fusionVariable;
                $acteurFields[] = $fusionVariable;
            }
        }
        if (empty($aliasActeurFields)) {
            return;
        }

        // lecture des données en base de données

        $acteurs = $this->find('all', [
            'fields' => ['Listepresence.id'],
            'contain' => $aliasActeurFields,
            'conditions' => $conditions,
            'order' => 'Acteur.position ASC']);

        $aActeursAbsents = [];
        if (empty($nbActeurs)) {
            $aActeurAbsent = [];
            foreach ($acteurFields as $fieldname) {
                $aActeurAbsent[$fieldname . '_acteur_absent'] = ['value' => '', 'type' => 'text'];
            }
            $aActeursAbsents[] = $aActeurAbsent;
        } else {
            foreach ($acteurs as &$acteur) {
                $aActeurAbsent = [];
                foreach ($acteurFields as $fieldname) {
                    $aActeurAbsent[$fieldname . '_acteur_absent'] = [
                        'value' => $acteur['Acteur'][$fieldname], 'type' => 'text'
                    ];
                }

                $aActeursAbsents[] = $aActeurAbsent;
            }
        }

        $aData['ActeursAbsents'] = $aActeursAbsents;
    }

    /**
     * Fonction d'initialisation des variables de fusion pour la liste des mandatés pour le vote d'un projet
     * les bibliothèques Gedooo doivent être inclues par avance
     * génère une exception en cas d'erreur
     *
     * @version 5.0
     * @since 4.3
     * @access public
     * @param type $aData
     * @param object_by_ref $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param integer $deliberationId --> id de la délibération
     */
    public function setVariablesFusionMandates(&$aData, &$modelOdtInfos, $deliberationId)
    {
        // initialisations
        $fusionVariables = [
            'nom', 'prenom', 'salutation', 'titre', 'adresse1', 'adresse2',
            'cp', 'ville', 'email', 'telfixe', 'telmobile', 'note'
        ];
        $conditions = [
            'Listepresence.delib_id' => $deliberationId,
            'Listepresence.present' => false, 'Listepresence.mandataire <>' => null
        ];

        // nombre d'acteurs mandatés
        $nbActeurs = $this->find('count', ['recursive' => -1, 'conditions' => $conditions]);
        if ($modelOdtInfos->hasUserFieldDeclared('nombre_acteur_mandataire')) {
            $aData['nombre_acteur_mandataire'] = ['value' => $nbActeurs, 'type' => 'text'];
        }
        // liste des variables utilisées dans le template
        $acteurFields = $aliasActeurFields = $mandateFields = $aliasMandateFields = [];
        foreach ($fusionVariables as $fusionVariable) {
            if ($modelOdtInfos->hasUserFieldDeclared($fusionVariable . '_acteur_mandataire')) {
                $aliasActeurFields[] = 'Acteur.' . $fusionVariable;
                $acteurFields[] = $fusionVariable;
            }
            if ($modelOdtInfos->hasUserFieldDeclared($fusionVariable . '_acteur_mandate')) {
                $aliasMandateFields[] = 'Mandataire.' . $fusionVariable;
                $mandateFields[] = $fusionVariable;
            }
        }
        if (empty($aliasActeurFields) && empty($aliasMandateFields)) {
            return;
        }

        // lecture des données en base de données
        $acteurs = $this->find('all', [
            'fields' => ['Listepresence.id'],
            'contain' => array_merge($aliasActeurFields, $aliasMandateFields),
            'conditions' => $conditions,
            'order' => 'Acteur.position ASC']);

        $aActeursMandates = [];
        if (empty($nbActeurs)) {
            $aActeurMandate = [];
            foreach ($acteurFields as $fieldname) {
                $aActeurMandate[$fieldname . '_acteur_mandataire'] = ['value' => '', 'type' => 'text'];
            }
            foreach ($mandateFields as $fieldname) {
                $aActeurMandate[$fieldname . '_acteur_mandate'] = ['value' => '', 'type' => 'text'];
            }

            $aActeursMandates[] = $aActeurMandate;
        } else {
            foreach ($acteurs as &$acteur) {
                $aActeurMandate = [];
                foreach ($acteurFields as $fieldname) {
                    $aActeurMandate[$fieldname . '_acteur_mandataire'] = [
                        'value' => $acteur['Acteur'][$fieldname], 'type' => 'text'
                    ];
                }
                foreach ($mandateFields as $fieldname) {
                    $aActeurMandate[$fieldname . '_acteur_mandate'] = [
                        'value' => $acteur['Mandataire'][$fieldname], 'type' => 'text'
                    ];
                }

                $aActeursMandates[] = $aActeurMandate;
            }
        }

        $aData['ActeursMandates']  = $aActeursMandates;
    }
}
