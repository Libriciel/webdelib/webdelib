<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('CakeTime', 'Utility');

/**
 * Acteur Model
 *
 * @package app.Model
 *
 */
class Acteur extends AppModel
{
    /** @var string Should contain a model name  */
    public $name = 'Acteur';

    /** @var string Should contain a display Field name  */
    public $displayField = "nom";

    /** @var Array Contains a validate list */
    public $validate = [
        'nom' => [
            [
                'rule' => 'notBlank',
                'message' => 'Entrer un nom pour l\'acteur'
            ]],
        'prenom' => [
            [
                'rule' => 'notBlank',
                'message' => 'Entrer un prénom pour l\'acteur'
            ]],
        'email' => [
            [
                'rule' => 'email',
                'allowEmpty' => true,
                'message' => 'Adresse email non valide.'
            ]],
        'service' => [
            [
                'rule' => 'notBlank',
                'message' => 'Sélectionnez un ou plusieurs services'
            ]],
        'typeacteur_id' => [
            [
                'rule' => 'notBlank',
                'message' => 'Selectionner un type d\'acteur'
            ]]];

    /** @var Array Contains a belongsTo list */
    public $belongsTo = [
        'Suppleant' => [
            'className' => 'Acteur',
            'foreignKey' => 'suppleant_id'
        ],
        'Typeacteur' => [
            'className' => 'Typeacteur',
            'foreignKey' => 'typeacteur_id'
        ]];

    /** @var Array Contains a hasAndBelongsToMany list */
    public $hasAndBelongsToMany = [
        'Service' => [
            'classname' => 'Service',
            'joinTable' => 'acteurs_services',
            'foreignKey' => 'acteur_id',
            'associationForeignKey' => 'service_id',
            'conditions' => '',
            'order' => '',
            'limit' => '',
            'unique' => true,
            'finderQuery' => '',
            'deleteQuery' => ''
        ]];

    /**
     * Retourne la liste des acteurs élus [id]=>[prenom et nom] pour
     * utilisation html->selectTag
     *
     * @version 5.1
     * @since 4.3
     * @access public
     * @param type $order_by
     * @return string
     */
    public function generateListElus($order_by = null, $inactif = false)
    {
        $rapporteurDesactiveLibelle = '-------------------Rapporteurs désactivés-------------------';
        $conditions = $inactif ? ['Typeacteur.elu' => true] : ['Typeacteur.elu' => true,'Acteur.actif' => true];

        $acteurs = $this->find('all', [
            'fields' => ['id', 'nom', 'prenom','actif','modified'],
            'conditions' => $conditions,
            'order' => (empty($order_by) ? 'Acteur.actif DESC , Acteur.position ASC' : $order_by),
            'joins' => [$this->join('Typeacteur', ['type' => 'INNER'])],
            'group' => 'Acteur.id',
            'recursive' => -1
            ]);

        $generateListElus = [];
        if ($inactif) {
            foreach ($acteurs as $acteur) {
                $acteur['Acteur']['actif'] ?
                    $generateListElus[$acteur['Acteur']['id']] = $acteur['Acteur']['nom']
                        . ' ' . $acteur['Acteur']['prenom'] :
                    $generateListElus[$rapporteurDesactiveLibelle][$acteur['Acteur']['id']] =
                      $acteur['Acteur']['nom'] . ' ' . $acteur['Acteur']['prenom'] . ' ' . "(désactivé depuis le ".
                      CakeTime::format($acteur['Acteur']['modified'], '%e %B, %Y').")";
            }
            if (isset($generateListElus[$rapporteurDesactiveLibelle])) {
                sort($generateListElus[$rapporteurDesactiveLibelle]);
            }
        } else {
            foreach ($acteurs as $acteur) {
                $generateListElus[$acteur['Acteur']['id']] = $acteur['Acteur']['nom'] . ' '
                    . $acteur['Acteur']['prenom'];
            }
        }
        return $generateListElus;
    }

    /**
     * Retourne la liste des acteurs [id]=>[prenom et nom] pour utilisation html->selectTag
     *
     * @version 4.3
     * @access public
     * @param type $order_by
     * @return string
     */
    public function generateList($order_by = null)
    {
        $generateList = [];
        if ($order_by == null) {
            $acteurs = $this->find('all', [
                'fields' => ['id', 'nom', 'prenom'],
                'conditions' => ['Acteur.actif' => 1],
                'order' => 'Acteur.position ASC']);
        } else {
            $acteurs = $this->find('all', [
                'fields' => ['id', 'nom', 'prenom'],
                'conditions' => ['Acteur.actif' => 1],
                'order' => "$order_by ASC"]);
        }

        foreach ($acteurs as $acteur) {
            $generateList[$acteur['Acteur']['id']] = $acteur['Acteur']['prenom'] . ' ' . $acteur['Acteur']['nom'];
        }

        return $generateList;
    }

    /**
     * Retourne l'id du premier acteur élu associé à la délégation $serviceId
     *
     * @param int $delegationId
     * @return int
     */
    public function selectActeurEluIdParDelegationId($delegationId): ?int
    {
        $users = $this->find('all', [
                    'fields' => ['id','ActeurService.service_id'],
                    'joins' => [
                        $this->join('ActeurService', [
                                'type' => 'INNER',
                                'conditions' => ['ActeurService.service_id' => $delegationId]
                        ]),
                        $this->join('Typeacteur', ['type' => 'INNER', 'conditions' => ['Typeacteur.elu' => true]])
                    ],
                    'conditions' => ['Acteur.actif' => true],
                    'order' => ['Acteur.position' => 'ASC'],
                    'recursive' => -1
                ]);

        foreach ($users as $user) {
            foreach ($user['ActeurService'] as $service_id) {
                if ($service_id === $delegationId) {
                    return $user['Acteur']['id'];
                }
            }
        }

        return null;
    }

    /**
     * Retourne le numéro de position max pour tous les acteurs élus
     * pour rester compatible avec le plus grand nombre de bd, on ne passe pas de requête
     * mais on fait le calcul en php
     *
     * @version 4.3
     * @access public
     * @return type
     */
    public function getPostionMaxParActeursElus()
    {
        $acteur = $this->find('all', [
            'fields' => ['Acteur.position'],
            'conditions' => ['Typeacteur.elu' => 1, 'Acteur.actif' => 1],
            'order' => 'Acteur.position DESC']);
        return empty($acteur) ? 0 : $acteur[0]['Acteur']['position'];
    }

    /**
     * Retourne le libellé correspondant au champ position : = 999 : en dernier, <999 : position
     *
     * @param int $ordre
     * @param boolean $majuscule
     * @return string
     */
    public function libelleOrdre($ordre = null, $majuscule = false)
    {
        return ($ordre > 998 || $ordre === null) ? '' : $ordre;
    }

    /**
     * Fonction d'initialisation des variables de fusion pour l'allias utilisé pour la liaison
     * (Rapporteur, President, ...)
     * les bibliothèques Gedooo doivent être inclues par avance
     * génère une exception en cas d'erreur
     *
     *
     * @access public
     * @param type $aData
     * @param type $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param type $id --> id du modèle lié
     * @param string $suffixe --> suffixe des variables de fusion
     * @return boolean
     *
     * @version 5.1
     * @since 4.3
     */
    public function setVariablesFusion(&$aData, &$modelOdtInfos, $id, $suffixe = '')
    {
        // initialisations
        if (empty($suffixe)) {
            $suffixe = trim(Inflector::underscore($this->alias));
        }

        $fields = [];
        $variables = [
            'salutation',
            'prenom',
            'nom',
            'titre',
            'position',
            'email',
            'telmobile',
            'telfixe',
            'adresse1',
            'adresse2',
            'cp',
            'ville',
            'note'
            ];

        // liste des variables présentes dans le modèle d'édition
        foreach ($variables as $variable) {
            if ($modelOdtInfos->hasUserFieldDeclared($variable . '_' . $suffixe)) {
                $fields[] = $variable;
            }
        }

        if (empty($fields)) {
            return;
        }

        if (empty($id)) {
            $this->getVariablesFusionEmpty($aData, $suffixe, $fields);
            return;
        }

        // lecture en base de données
        $acteur = $this->find('first', [
            'fields' => $fields,
            'conditions' => ['id' => $id],
            'recursive' => -1,
            ]);

        if (empty($acteur)) {
            throw new Exception('acteur ' . $suffixe . ' id:' . $id . ' non trouvé en base de données');
        }

        foreach ($acteur[$this->alias] as $field => $val) {
            $aData[$field . '_' . $suffixe] = ['value' => $val, 'type' => 'text'];
        }
    }

    /**
     * [getVariablesFusionEmpty description]
     * @param  [type] $aData  [description]
     * @param $suffixe
     * @param  [type] $fields [description]
     * @return [type]         [description]
     *
     * @version 5.1
     */
    private function getVariablesFusionEmpty(&$aData, $suffixe, $fields)
    {
        foreach ($fields as $field) {
            $aData[$field . '_' . $suffixe] = ['value' => '', 'type' => 'text'];
        }
    }

    /**
     * Envoi une notification par mail à un utilisateur sur l'état d'un dossier
     *
     * @version 4.3
     * @access public
     * @param integer $delib_id --> identifiant du dossier
     * @param integer $user_id -->  identifiant de l'utilisateur à notifier
     * @param string $type --> notification à envoyer
     * @return boolean --> succès de l'envoi
     *
     * @SuppressWarnings(PHPMD)
     */
    public function notifier($delib_id, $user_id, $type)
    {
        App::uses('Deliberation', 'Model');
        App::uses('Seance', 'Model');
        App::uses('CakeEmail', 'Network/Email');
        App::uses('Historique', 'Model');

        $acteur = $this->find('first', [
            'field' => ['nom', 'prenom', 'email', 'notif_' . $type],
            'conditions' => ['id' => $user_id],
            'recursive' => -1,
            ]);

        // utilisateur existe et accepte les mails ?
        if (empty($acteur)
            || empty($type)
            || empty($acteur['Acteur']['email'])
            || empty($acteur['Acteur']["notif_$type"])
        ) {
            return false;
        }

        $config_mail = 'default';
        $Email = new CakeEmail($config_mail);

        $this->Deliberation = ClassRegistry::init('Deliberation');
        $delib = $this->Deliberation->find('first', [
            'fields' => ['Deliberation.id', 'objet', 'titre', 'circuit_id'],
            'conditions' => ['Deliberation.id' => $delib_id],
            'contain' => ['Commentaire' => ['fields' => ['texte'],
                    'conditions' => ['commentaire_auto' => false],
                    'order' => 'Commentaire.created DESC',
                    'limit' => 1
                    ],
                'Theme' => ['fields' => ['libelle'],
                    ],
                ],
            'recursive' => -1,
            ]);


        $seance_id = $this->Deliberation->getSeanceDeliberanteId($delib_id);
        $type_seance_libelle = '';
        if (!empty($seance_id)) {
            $seance = $this->Deliberation->Seance->find('first', [
                'fields' => ['Seance.id', 'date'],
                'conditions' => ['Seance.id' => $seance_id],
                'contain' => [
                  'Typeseance' => ['fields' => ['libelle']
                  ],
                ],
                'recursive' => -1,
                ]);
            $type_seance_libelle = __(' %s :', $seance['Typeseance']['libelle']);
            $delib['Deliberation']['SeanceDeliberante']['texte'] =
            $seance['Typeseance']['libelle'] .
            ' du ' .
            CakeTime::i18nFormat(strtotime($seance['Seance']['date']), '%A %e %B %Y à %k h %M');
        }

        $subject = '';
        switch ($type) {
            case 'insertion':
                $template = 'acteur_projet_insertion';
                $subject = __(
                    '[Projet#%s]%s Un projet dont vous êtes le rapporteur vient '
                    .'d\'être inséré dans un circuit de validation',
                    $delib_id,
                    $type_seance_libelle
                );
                $Email->attachments([
                    'Projet.pdf' => [
                        'data' => $this->Deliberation->fusion($delib['Deliberation']['id']),
                        'mimetype' => 'application/pdf'
                    ]
                ]);
                $typeMessageHistorique = __('d\'insertion');
                break;
            case 'projet_valide':
                $template = 'projet_valide_rapporteur';
                $subject =  __(
                    '[Projet#%s]%s Un projet dont vous êtes le rapporteur vient d\'être validé',
                    $delib_id,
                    $type_seance_libelle
                );
                $Email->attachments(['Projet.pdf' => [
                    'data' => $this->Deliberation->fusion($delib['Deliberation']['id']),
                    'mimetype' => 'application/pdf'
                ]]);
                $typeMessageHistorique = __('de validation');
                break;
        }

        $aVariables = ['civilite' => $acteur['Acteur']['salutation'],
            'nom' => $acteur['Acteur']['nom'],
            'prenom' => $acteur['Acteur']['prenom'],
            'projet_identifiant' => $delib['Deliberation']['id'],
            'projet_objet' => $delib['Deliberation']['objet'],
            'projet_theme' => $delib['Theme']['libelle'],
            'seance_deliberante' => !empty($delib['Deliberation']['SeanceDeliberante']['texte'])
                ? $delib['Deliberation']['SeanceDeliberante']['texte'] : '',
            'projet_titre' => $delib['Deliberation']['titre'],
        ];

        $historique = ClassRegistry::init('Historique');
        $historique->enregistre(
            $delib_id,
            AuthComponent::user('id'),
            __(
                'Notification %s du projet a été envoyée à %s %s %s',
                $typeMessageHistorique,
                $acteur['Acteur']['salutation'],
                $acteur['Acteur']['nom'],
                $acteur['Acteur']['prenom']
            )
        );

        try {
            $Email->viewVars($aVariables);
            $Email->template($template, 'default')
                        ->to($acteur['Acteur']['email'])
                        ->subject($subject);

            $fileTemplateHtml = new File(APP . __('Config/mails/html/%s.ctp', $template));
            if ($fileTemplateHtml->exists()) {
                $Email->viewRender('Email')->template($fileTemplateHtml->pwd());
            }
            $fileTemplateTxt = new File(APP . __('Config/mails/text/%s.ctp', $template));
            if ($fileTemplateTxt->exists()) {
                $Email->viewRender('Text')->template($fileTemplateTxt->pwd());
            }

            $retour = $Email->send();
        } catch (Exception $e) {
            $retour = false;
            CakeLog::error(
                __(
                    'Notification de %s du projet (id:%s) n\'a pas été envoyée à %s %s %s',
                    $typeMessageHistorique,
                    $delib_id,
                    $acteur['Acteur']['salutation'],
                    $acteur['Acteur']['nom'],
                    $acteur['Acteur']['prenom']
                )
            );
        }
        return $retour;
    }
}
