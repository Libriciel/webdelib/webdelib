<?php
/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Sae', 'Lib');
App::uses('Seance', 'Model');
App::uses('Deliberation', 'Model');

/**
 * [VersementSae description]
 */
class SaeVersement extends AppModel
{
    public $useTable = false;

    public array $apiErrors = [
        'generate-sip-error',
        'erreur-envoie-sae',
        'validation-sae-erreur',
        'verif-sae-erreur',
        'fatal-error',
    ];
    public array $apiAccepts = [
        'accepter-sae',
        'termine',
    ];
    public const REFUSER = -2;
    public const ERREUR = -1;
    public const ATTENTE = 0;
    public const VERSER = 1;
    public const ACCEPTER = 2;
    public const VERSER_MANUELLEMENT = 3;

    public function updateSeance($seanceId, $idD)
    {
        return $this->update('Seance', $seanceId, $idD);
    }

    public function updateActe($acteId, $idD)
    {
        return $this->update('Deliberation', $acteId, $idD);
    }

    public function update($modelName, $id, $idD)
    {
        $modelInstance = ClassRegistry::init($modelName);
        $rapport = '';

        try {
            $modelInstance->id = $id;
            $modelInstance->recursive = -1;
            $modelRead = $modelInstance->read(['sae_etat']);
            $sae = new Sae();
            $infos = $sae->getDetailsPastell($idD);
            if (in_array($infos['last_action']['action'], $this->apiErrors, true)) {
                $modelInstance->save(
                    [
                        'sae_etat' => self::ERREUR,
                        'sae_commentaire' => $infos['data']['sae_atr_comment'] ?? __(
                            'Erreur: %s',
                            $infos['last_action']['action']
                        )
                    ],
                    false
                );
                if ($modelRead[$modelName]['sae_etat']!==self::ERREUR) {
                    $this->{'notifierSae'.$modelName}($id, 'sae_error');
                }
                $rapport = __(
                    '%s id %s (%s) : Versement en erreur (%s)',
                    $modelName === 'Deliberation' ? 'Acte' : 'Séance',
                    $id,
                    $idD,
                    $infos['last_action']['action']
                )
                . "\n";
            } elseif ($infos['last_action']['action'] === 'rejet-sae') {
                $modelInstance->save(
                    [
                        'sae_etat' => self::REFUSER,
                        'sae_atr' => $sae->getATR($idD),
                        'sae_commentaire' => $infos['data']['sae_atr_comment'] ?? null
                    ],
                    false
                );
                $this->{'notifierSae'.$modelName}($id, 'sae_refus');
                $rapport = __(
                    '%s id %s (%s) : Versement refusé',
                    $modelName === 'Deliberation' ? 'Acte' : 'Séance',
                    $id,
                    $idD,
                    $infos['last_action']['action']
                )
                . "\n";
            } elseif (in_array($infos['last_action']['action'], $this->apiAccepts, true)
            ) {
                $modelInstance->save(
                    [
                        'sae_etat' => self::ACCEPTER,
                        'sae_atr' => $sae->getATR($idD),
                        'sae_commentaire' => $infos['data']['sae_atr_comment'] ?? null
                    ],
                    false
                );
                $rapport = __(
                    '%s id %s (%s) : Versement Accepté',
                    $modelName === 'Deliberation' ? 'Acte' : 'Séance',
                    $id,
                    $idD,
                    $infos['last_action']['action']
                )
                . "\n";
            } else {
                $rapport = __(
                    '%s id %s (%s) : Versement en attente de réception',
                    $modelName === 'Deliberation' ? 'Acte' : 'Séance',
                    $id,
                    $idD,
                    $infos['last_action']['action']
                )
                    . "\n";
            }
        } catch (Exception $e) {
            CakeLog::error(
                __(
                    '[Sae][%s]Update etat %s n°%s (%s)',
                    __METHOD__,
                    $modelName,
                    $id,
                    $e->getCode()
                )
                . "\n"
                . $e->getMessage(),
                'connector'
            );
            $rapport .= $e->getMessage();
        }

        return $rapport;
    }

    /**
     * Envoi une notification par mail à un utilisateur sur l'état d'un dossier de seance au SAE
     *
     * @param integer $id -> identifiant du dossier
     * @param string $type -> notification à envoyer
     */
    public function notifierSaeSeance(int $id, string $type)
    {
        App::uses('Seance', 'Model');
        App::uses('User', 'Model');
        App::uses('CakeEmail', 'Network/Email');

        $this->User = ClassRegistry::init('User');
        $users =  $this->User->find('all', [
            'fields' => ['id', 'nom', 'prenom', 'email', 'accept_notif', 'mail_error_sae'],
            'conditions' => ['mail_error_sae' => true],
            'recursive' => -1,
        ]);

        foreach ($users as $user) {
            // utilisateur existe et accepte les mails ?
            if (!$this->userAcceptNotification($user)) {
                $message = __(
                    'Utilisateur id %s inexistant ou n\'acceptant pas les mails: mail_error_sae (%s)',
                    $user['User']['id'],
                    $user['User']['email']
                ) . "\n";
                $this->log($message, 'error');
                return false;
            }

            $config_mail = 'default';
            $email = new CakeEmail($config_mail);
            $this->Seance = ClassRegistry::init('Seance');
            $seance = $this->Seance->find('first', [
                'fields' => ['Seance.id', 'date', 'sae_commentaire'],
                'conditions' => ['Seance.id' => $id],
                'contain' => [
                    'Typeseance' => [
                        'fields' => ['libelle']
                    ],
                ],
                'recursive' => -1,
            ]);
            $type_seance_libelle = __(
                ' %s du %s',
                $seance['Typeseance']['libelle'],
                CakeTime::i18nFormat(strtotime($seance['Seance']['date']), '%A %e %B %Y à %k h %M')
            );

            $template = $subject = $sae_error = $saeCommentaire = '';
            switch ($type) {
                case 'sae_error':
                    $template = 'seance_sae_error';
                    $subject = __('[web-delib] Une seance transmise au SAE est en erreur.');
                    $sae_error = !empty($seance['Seance']['sae_commentaire'])
                        ? $seance['Seance']['sae_commentaire'] : null;
                    break;
                case 'sae_refus':
                    $template = 'seance_sae_refus';
                    $subject = __(
                        '[Seance#%s] Une seance que j\'ai versé au SAE a été refusée (%s)',
                        $id,
                        $type_seance_libelle
                    );
                    $saeCommentaire = !empty($seance['Seance']['sae_commentaire'])
                        ? $seance['Seance']['sae_commentaire'] : null;
                    break;
                default:
                    break;
            }
            $aVariables = [
                'nom' => $user['User']['nom'],
                'prenom' => $user['User']['prenom'],
                'seance' => $type_seance_libelle,
                'sae_commentaire' => $saeCommentaire,
                'sae_error' => $sae_error
            ];

            try {
                $this->log(__(
                    'Utilisateur id:%s mail envoyé: %s (%s)',
                    $user['User']['id'],
                    $type,
                    $user['User']['email']
                ) . "\n", 'error');
                $email->viewVars($aVariables);
                $email->template($template, 'default')
                    ->to($user['User']['email'])
                    ->subject($subject);

                $fileTemplateHtml = new File(APP . __('Config/mails/html/%s.ctp', $template));
                if ($fileTemplateHtml->exists()) {
                    $email->viewRender('Email')->template($fileTemplateHtml->pwd());
                }
                $fileTemplateTxt = new File(APP . __('Config/mails/text/%s.ctp', $template));
                if ($fileTemplateTxt->exists()) {
                    $email->viewRender('Text')->template($fileTemplateTxt->pwd());
                }
                $email->send();
            } catch (Exception $e) {
                CakeLog::error(
                    __(
                        'Notification %s de la séance (id:%s) n\'a pas été envoyée à %s %s',
                        $type,
                        $id,
                        $user['User']['nom'],
                        $user['User']['prenom']
                    )
                );
                continue;
            }
        }
    }

    /**
     * Envoi une notification par mail à un utilisateur sur l'état d'un dossier de seance au SAE
     *
     * @param integer $id -> identifiant de l'acte
     * @param string $type -> notification à envoyer
     */
    public function notifierSaeDeliberation(int $id, string $type)
    {
        App::uses('Deliberation', 'Model');
        App::uses('CakeEmail', 'Network/Email');

        App::uses('User', 'Model');
        $this->User = ClassRegistry::init('User');
        $users =  $this->User->find('all', [
            'fields' => ['id', 'nom', 'prenom', 'email', 'accept_notif', 'mail_error_sae'],
            'conditions' => ['mail_error_sae' => true],
            'recursive' => -1,
        ]);

        foreach ($users as $user) {
            // utilisateur existe et accepte les mails ?
            if (!$this->userAcceptNotification($user)) {
                $message = __(
                    'Utilisateur id %s inexistant ou n\'acceptant pas les mails: mail_error_sae (%s)',
                    $user['User']['id'],
                    $user['User']['email']
                ) . "\n";
                $this->log($message, 'error');
                return false;
            }

            $config_mail = 'default';
            $email = new CakeEmail($config_mail);
            $this->Deliberation = ClassRegistry::init('Deliberation');
            $deliberation= $this->Deliberation->find('first', [
                'fields' => ['Deliberation.id','Deliberation.num_delib', 'date_acte', 'sae_commentaire'],
                'conditions' => ['Deliberation.id' => $id],
                'recursive' => -1,
            ]);
            $acte_libelle = __(
                ' %s du %s',
                $deliberation['Deliberation']['num_delib'],
                CakeTime::i18nFormat(strtotime($deliberation['Deliberation']['date_acte']), '%A %e %B %Y')
            );

            $template = $subject = $sae_error = $saeCommentaire = '';
            switch ($type) {
                case 'sae_error':
                    $template = 'acte_sae_error';
                    $subject = __('[web-delib] Une seance transmise au SAE est en erreur.');
                    $sae_error = !empty($deliberation['Deliberation']['sae_commentaire'])
                        ? $deliberation['Deliberation']['sae_commentaire'] : null;
                    break;
                case 'sae_refus':
                    $template = 'acte_sae_refus';
                    $subject = __(
                        '[Acte#%s] Un acte que j\'ai versé au SAE a été refusé',
                        $id,
                    );
                    $saeCommentaire = !empty($deliberation['Deliberation']['sae_commentaire'])
                        ? $deliberation['Deliberation']['sae_commentaire'] : null;
                    break;
                default:
                    break;
            }
            $aVariables = [
                'nom' => $user['User']['nom'],
                'prenom' => $user['User']['prenom'],
                'acte' => $acte_libelle,
                'sae_commentaire' => $saeCommentaire,
                'sae_error' => $sae_error
            ];

            try {
                $this->log(__(
                    'Utilisateur id:%s  mail envoyé: %s (%s)',
                    $user['User']['id'],
                    $type,
                    $user['User']['email']
                ) . "\n", 'error');
                $email->viewVars($aVariables);
                $email->template($template, 'default')
                    ->to($user['User']['email'])
                    ->subject($subject);

                $fileTemplateHtml = new File(APP . __('Config/mails/html/%s.ctp', $template));
                if ($fileTemplateHtml->exists()) {
                    $email->viewRender('Email')->template($fileTemplateHtml->pwd());
                }
                $fileTemplateTxt = new File(APP . __('Config/mails/text/%s.ctp', $template));
                if ($fileTemplateTxt->exists()) {
                    $email->viewRender('Text')->template($fileTemplateTxt->pwd());
                }
                $email->send();
            } catch (Exception $e) {
                CakeLog::error(
                    __(
                        'Notification de l\'acte (id:%s) n\'a pas été envoyée à %s %s',
                        $id,
                        $user['User']['nom'],
                        $user['User']['prenom']
                    )
                );
                continue;
            }
        }
    }

    public function isSeanceOk($seanceId)
    {
        App::uses('Deliberation', 'Model');
        $modelDeliberation = ClassRegistry::init('Deliberation');
        $nbrDeliberationsNonPublie = $modelDeliberation->find('count', [
         'joins' => [
             $modelDeliberation->join('DeliberationSeance', ['type' => 'INNER'])
         ],
         'conditions' => [
             'DeliberationSeance.seance_id' => $seanceId,
             'Deliberation.publier_etat' => 0
         ]
        ]);

        return $nbrDeliberationsNonPublie === 0;
    }

    /**
     * @param $user
     * @return bool
     */
    private function userAcceptNotification($user): bool
    {
        return !(empty($user['User']['accept_notif'])
            || empty($user['User']['email'])
            || empty($user['User']["mail_error_sae"]));
    }
}
