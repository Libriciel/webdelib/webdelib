<?php

/**
 * webdelib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib webdelib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('File', 'Utility');
App::uses('ImageUtilitaire', 'Lib');

/**
 * Collectivite AppModel
 *
 * @version 5.2.0
 * @since 1.0.0
 */
class Collectivite extends AppModel
{
    public $name = 'Collectivite';
    public $cacheSources = 'false';
    public $validate = [
        'nom' => [
            [
                'rule' => 'notBlank',
                'message' => 'Entrer le nom de la collectivité'
            ]
        ],
        'siret' => [
            [
                'rule' => 'notBlank',
                'message' => 'Entrer le siret de la collectivité'
            ],
            [
                'rule' => 'numeric',
                'message' => 'Le numero de SIRET est uniquement composé de chiffre.'
            ],
            [
                'rule' => ['minLength', '14'],
                'message' => 'Le numero de SIRET doit avoir au moins 14 caractères.'
            ],
            [
                'rule' => ['maxLength', '14'],
                'message' => 'Le numero de SIRET ne doit pas dépasser 14 caractères.'
            ],
            [
            'rule' => ['luhn', true],
            'message' => 'Le numero de SIRET indiqué n\'est pas valide.'
            ]
        ],
        'adresse' => [
            [
                'rule' => 'notBlank',
                'message' => 'Entrer l\'adresse.'
            ]
        ],
        'CP' => [
            [
                'rule' => 'notBlank',
                'message' => 'Entrer le code postal.'
            ]
        ],
        'ville' => [
            [
                'rule' => 'notBlank',
                'message' => 'Entrer la ville.'
            ]
        ],
        'telephone' => [
            [
                'rule' => 'notBlank',
                'message' => 'Entrer le numéro de téléphone.'
            ]
        ],
        'force' => [
            [
                'rule' => 'notBlank',
                'message' => 'Veuillez indiquer la force des mots de passe des utilisateurs.'
            ]
        ],
        'logo' => [
            [
                'rule' => ['checkFormat', ['image/jpeg', 'image/png'], false],
                'message' => 'Le format de l\'image est invalide. (Autorisé : fichier "jpeg" ou "png")'
            ]
        ],
        'code_ape' =>
            [
                'rule' => 'notBlank',
                'message' => 'Entrer le code APE de la collectivité'
            ],
            [
                'rule' => ['maxLength', '5'],
                'message' => 'Le code APE doit contenir 5 caractères',
            ],
        'email' =>
            [
                [
                    'rule' => 'notBlank',
                    'message' => 'Entrer le mail de la collectivité'
                ],
                [
                    'rule' => 'email',
                    'message' => 'Le format de l\'email n\'est pas correct',
                ],
            ],
        'responsable_nom' =>
            [
                'rule' => 'notBlank',
                'message' => 'Renseigner le nom du responsable',
            ],
        'responsable_prenom' =>
            [
                'rule' => 'notBlank',
                'message' => 'Renseigner le prénom du responsable',
            ],
        'responsable_fonction' =>
            [
                'rule' => 'notBlank',
                'message' => 'Renseigner la fonction du responsable',
            ],
        'responsable_email' =>
            [
                [
                    'rule' => 'notBlank',
                    'message' => 'Renseigner l\'email du responsable',
                ],
                [
                    'rule' => 'email',
                    'message' => 'Le format de l\'email n\'est pas correct',
                ]
            ],
        'dpo_nom' =>
            [
                'rule' => 'notBlank',
                'message' => 'Renseigner le nom du DPO',
            ],
        'dpo_prenom' =>
            [
                'rule' => 'notBlank',
                'message' => 'Renseigner le prénom du DPO',
            ],
        'dpo_email' =>
            [
                [
                    'rule' => 'notBlank',
                    'message' => 'Renseigner l\'email du DPO',
                ],
                [
                    'rule' => 'email',
                    'message' => 'Le format de l\'email n\'est pas correct',
                ],
            ],
        'saas' => [
            'rule' => 'notBlank',
            'message' => 'Renseigner le type d\'hébergement de l\'application',
        ],
        'templateProject' => [
            [
                'rule' => ['checkJson'],
                'message' => "Format du Json invalide."
            ]
        ],
        'config_login' => [
            [
                'rule' => ['checkJson'],
                'message' => "Format du Json invalide."
            ]
        ]
    ];

    /**
     * Fonction d'initialisation des variables de fusion pour la collectivité
     * les bibliothèques Gedooo doivent être inclues par avance
     * génère une exception en cas d'erreur
     *
     * @version 4.3
     * @access public
     * @param object_by_ref $aData --> variable Gedooo de type maintPart du document à fusionner
     * @param object_by_ref $modelOdtInfos --> objet PhpOdtApi du fichier odt du modèle d'édition
     * @param int $id --> id des données à fusionner
     * @throws Exception
     */
    public function setVariablesFusion(&$aData, &$modelOdtInfos, $id)
    {
        // lecture de la collectivité en  base de données
        $collectivite = $this->find('first', [
            'recursive' => -1,
            'fields' => ['id', 'nom', 'adresse', 'CP', 'ville', 'telephone'],
            'conditions' => ['id' => $id]]);
        if (empty($collectivite)) {
            throw new Exception('collectivité id:' . $id . ' non trouvé en base de données');
        }
        // initialisation des variables
        if ($modelOdtInfos->hasUserFieldDeclared('nom_collectivite')) {
            $aData['nom_collectivite'] = ['value' => $collectivite['Collectivite']['nom'], 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('adresse_collectivite')) {
            $aData['adresse_collectivite'] = ['value' => $collectivite['Collectivite']['adresse'], 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('cp_collectivite')) {
            $aData['cp_collectivite'] = ['value' => $collectivite['Collectivite']['CP'], 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('ville_collectivite')) {
            $aData['ville_collectivite'] = ['value' => $collectivite['Collectivite']['ville'], 'type' => 'text'];
        }
        if ($modelOdtInfos->hasUserFieldDeclared('telephone_collectivite')) {
            $aData['telephone_collectivite'] = [
                'value' => $collectivite['Collectivite']['telephone'],
                'type' => 'text'
            ];
        }
    }

    /**
     * @version 4.3
     * @access public
     * @param type $data
     * @return type
     */
    public function checkJson($data)
    {
        return is_string($data['templateProject'])
        && is_object(json_decode($data['templateProject']))
        && (json_last_error() === JSON_ERROR_NONE) ? true : false;
    }

    /**
     * Recupere le tableau jSON des valeurs definies par defaut pour les 9 cases
     *
     * @version 4.3
     * @access public
     * @return type
     */
    public function getJson9Cases()
    {
        $file = new File(APP . 'Config/9cases.json');
        return $file->read();
    }

    /**
     * @access public
     * @param type $options
     * @return boolean
     * @version 5.2.0
     * @since 5.1.0
     */
    public function beforeSave($options = [])
    {
        if (!empty($this->data['Collectivite']['logo']) && !empty($this->data['Collectivite']['logo']['tmp_name'])
        ) {
            $this->data['Collectivite']['logo'] = file_get_contents($this->data['Collectivite']['logo']['tmp_name']);
        } else {
            unset($this->data['Collectivite']['logo']);
        }

        return true;
    }

    /**
     * @access public
     * @param type $options
     * @return boolean
     * @version 5.1.0
     */
    public function afterSave($created, $options = [])
    {
        if (!empty($this->data['Collectivite']['logo'])) {
            $file = new File(APP . WEBROOT_DIR . DS . 'files' . DS . 'image' . DS . 'logo');
            $file->write($this->data['Collectivite']['logo'], 'w', true);
            $file->close();
        }
    }
}
