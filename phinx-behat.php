<?php

return
    [
        'paths' => [
            'bootstrap' => '%%PHINX_CONFIG_DIR%%/db/bootstrap.php',
            'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
            'seeds' => '%%PHINX_CONFIG_DIR%%/db/seeds/behat'
        ],
        'environments' => [
            'default_migration_table' => 'phinxlog',
            'default_environment' => 'webdelib_behat',
            'webdelib_behat' => [
                'adapter' => 'pgsql',
                'host' => env('POSTGRES_HOST'),
                'name' => env('POSTGRES_DB'),
                'user' => env('POSTGRES_USER'),
                'pass' => env('POSTGRES_PASSWORD'),
                'port' => env('POSTGRES_PORT'),
                'schema' => 'webdelib_behat',
                'charset' => 'utf8',
            ]
        ],
        'version_order' => 'creation'
    ];
