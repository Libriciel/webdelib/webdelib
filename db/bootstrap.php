<?php

if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

/**
 * The full path to the directory which holds "app", WITHOUT a trailing DS.
 *
 */
if (!defined('PHINX_ROOT')) {
    define('PHINX_ROOT', dirname(__FILE__, 2));
}

/**
* Path to the application's directory.
*/
if (!defined('PHINX_APP')) {
    define('PHINX_APP', PHINX_ROOT . DS . 'app' . DS);
}



