<?php

namespace dbSeeder;

trait SeederTrait
{
    public function updateSequence(string $table): void
    {
        $this->query("ALTER TABLE $table ALTER id RESTART WITH {$this->getTableIdMax($table)};");
    }

    private function getTableIdMax($tableName)
    {
        $tableName = $this->getAquoteTableName($tableName);
        $max = $this->fetchRow("SELECT max(id) from $tableName");
        return $max['max'] + 1;
    }

    private function shellRun($command, $args = null, $plugin = null)
    {
        $cakeConsolePath = PHINX_APP . 'Console/cake.php';
        $command = "php $cakeConsolePath -working app $command";
        exec($command, $output, $return_var);
        // Afficher la sortie et les messages d'erreur
        echo "\n". implode("\n", $output);
        if ($return_var === 0) {
            echo "Seed command executed successfully.\n";
        } else {
            echo "Failed to execute seed command.\n";
        }
    }

    private function updateFile($tableName, $columnName, $id, $filePath)
    {
        try {
            // Lire le contenu binaire du fichier PDF
            $fileContent = file_get_contents($filePath);

            if ($fileContent === false) {
                throw new Exception("Impossible de lire le fichier ($filePath).");
            }
            // Échapper correctement les données binaires pour PostgreSQL
            $escapedContent = pg_escape_bytea($fileContent);
            if (getenv('APP_ENV') === 'test') {
                $escapedContent = '';
            }
            $tableName = $this->getAquoteTableName($tableName);

            $this->execute("UPDATE $tableName SET $columnName = '{$escapedContent}' WHERE id = '$id'");
        } catch (Exception $e) {
            echo "Erreur lors du traitement du fichier PDF : " . $e->getMessage();
        }
    }

    private function getAquoteTableName($tableName)
    {
        if ($this->getAdapter()===null) {
            throw new Exception(sprintf('Adapter "%s" is not exist', $this->getName()));
        }
        return $this->getAdapter()->quoteColumnName($tableName);
    }
}
