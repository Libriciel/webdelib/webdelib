<?php

namespace dbSeeder;

use Phinx\Migration\AbstractMigration;

trait MigrationTrait
{
    private int $existData;

    private function removeIndexes($table, $indexes)
    {
        if ($this->getAdapter()===null) {
            throw new Exception(sprintf('Adapter "%s" is not exist', $this->getName()));
        }
        foreach ($indexes as $index) {
            if ($this->getAdapter()->hasIndex($index)) {
                $table->removeIndexByName($index)->save();
            }
        }
    }

    private function addIndexes($table, $indexes)
    {
        foreach ($indexes as $index) {
            if ($table->hasIndex($index)) {
                $table->addIndex($index)->save();
            }
        }
    }

    private function renameIndex($columnName, $newColumnName)
    {
        if ($this->getAdapter()===null) {
            throw new Exception(sprintf('Adapter "%s" is not exist', $this->getName()));
        }
        $sql = sprintf(
            'ALTER INDEX IF EXISTS %s RENAME TO %s',
            $this->getAquoteTableName($columnName),
            $this->getAquoteTableName($newColumnName)
        );

        $this->execute($sql);
    }

    private function changeCakeTimestamp($tableName)
    {
        $this->table($tableName)
        ->changeColumn('created', 'timestamp', [
            'limit' => null,
            'null' => false,
            'timezone' => false,
        ])
        ->changeColumn('modified', 'timestamp', [
            'limit' => null,
            'null' => false,
            'timezone' => false,
        ])
        ->save();
    }

    private function changeCakeString($tableName, $columnName, $nullable = true)
    {
        $this->table($tableName)
        ->changeColumn(
            $columnName,
            'string',
            [
                'limit' => '255', 'null' => $nullable, 'default' => null
            ]
        )->save();
    }

    private function notExistsData()
    {

        if (isset($this->existData)) {
            return $this->existData;
        }

        $oldErrorLevel = error_reporting(E_ALL & ~E_USER_DEPRECATED);

        $this->existData = true;
        if (!$this->table('versions')->exists()) {
            return $this->existData;
        }
        $builder = $this->getQueryBuilder();
        $builder->select('*')->from('versions')->execute();
        if ($builder->rowCountAndClose() > 0) {
            $this->existData = false;
        }
        error_reporting($oldErrorLevel);

        return $this->existData;
    }

    private function getAquoteTableName($tableName)
    {
        if ($this->getAdapter()===null) {
            throw new Exception(sprintf('Adapter "%s" is not exist', $this->getName()));
        }
        return $this->getAdapter()->quoteColumnName($tableName);
    }
}
