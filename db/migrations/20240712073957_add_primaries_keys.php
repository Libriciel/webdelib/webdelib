<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddPrimariesKeys extends AbstractMigration
{
    use dbSeeder\MigrationTrait;

    public function up(): void
    {
        if ($this->notExistsData()) {
            return;
        }

        $this->execute(sprintf(
            'ALTER TABLE %s ADD PRIMARY KEY ("id");',
            $this->getAquoteTableName('deliberations_versions')
        ));
        $this->execute(sprintf(
            'ALTER TABLE %s ADD PRIMARY KEY ("id");',
            $this->getAquoteTableName('tokensmethods')
        ));
        $this->execute(sprintf(
            'ALTER TABLE %s ADD PRIMARY KEY ("id");',
            $this->getAquoteTableName('infosupdefs_profils')
        ));
    }

    public function down(): void
    {
    }
}
