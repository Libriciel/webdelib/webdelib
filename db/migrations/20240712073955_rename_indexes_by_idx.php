<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class RenameIndexesByIdx extends AbstractMigration
{
    use dbSeeder\MigrationTrait;

    public function up(): void
    {
        if ($this->notExistsData()) {
            return;
        }
        $this->renameIndex('idx_acos_alias', 'acos_alias_idx');
        $this->renameIndex('idx_acos_lft_rght', 'acos_lft_rght_idx');

        $this->renameIndex('idx_aros_alias', 'aros_alias_idx');
        $this->renameIndex('idx_aros_lft_rght', 'aros_lft_rght_idx');

        $this->renameIndex('idx_aros_acos_aros_id_acos_id', 'aros_id_acos_id_idx');
        $this->renameIndex('idx_aros_acos_aco_id', 'aros_acos_aco_id');

        $this->renameIndex('acteurs_id', 'acteurs_id_idx');
        $this->renameIndex('typeacteur', 'acteurs_typeacteur_id_idx');
        $this->renameIndex('suppleant_id', 'acteurs_suppleant_id_idx');
        $this->renameIndex('acteurs_actif', 'acteurs_actif_idx');

        $this->renameIndex('acteursservices_acteur_id', 'acteurs_services_acteur_id_idx');
        $this->renameIndex('acteursservices_service_id', 'acteurs_services_service_id_idx');

        $this->renameIndex('model_foreign_key', 'annexes_model_foreign_key_idx');
        $this->renameIndex('annexes_joindre', 'annexes_foreign_key_joindre_fusion_idx');

        $this->renameIndex('user_id', 'circuits_users_user_id_idx');

        $this->renameIndex('nature_id', 'deliberations_typeacte_id_idx');
        $this->renameIndex('theme_id', 'deliberations_theme_id_idx');
        $this->renameIndex('service_id', 'deliberations_service_id_idx');
        $this->renameIndex('redacteur_id', 'deliberations_redacteur_id_idx');
        $this->renameIndex('rapporteur_id', 'deliberations_rapporteur_id_idx');
        $this->renameIndex('parent', 'deliberations_parent_id_idx');
        $this->renameIndex('etat', 'deliberations_etat_idx');

        $this->renameIndex('deliberations_id', 'deliberations_seances_deliberation_id_idx');
        $this->renameIndex('seances_id', 'deliberations_seances_seance_id_idx');

        $this->renameIndex('user', 'historiques_user_id_idx');

        $this->renameIndex('model', 'infosupdefs_model_idx');

        $this->renameIndex('INFOSUPDEF_ID_ORDRE', 'infosuplistedefs_infosupdef_id_ordre_idx');

        $this->renameIndex('foreign_key', 'infosups_foreign_key_idx');
        $this->renameIndex('infosupdef_id', 'infosups_infosupdef_id_idx');

        $this->renameIndex('modelsections_parent_id', 'modelsections_parent_id_idx');

        $this->renameIndex('modelvalidations_modelsection_id', 'modelvalidations_modelsection_id_idx');
        $this->renameIndex('modelvalidations_modeltype_id', 'modelvalidations_modeltype_id_idx');
        $this->renameIndex('modelvalidations_modelvariable_id', 'modelvalidations_modelvariable_id_idx');

        $this->renameIndex('type_id', 'seances_type_id_idx');
        $this->renameIndex('seances_traitee', 'seances_traitee_idx');
        $this->renameIndex('idx_seances_lft_rght', 'seances_lft_rght_idx');

        $this->renameIndex('idx_services_users', 'services_users_user_id_idx');

        $this->renameIndex('tdtmsg_', 'tdt_messages_delib_id_idx');

        $this->renameIndex('tokensmethods_controller_action_params', 'tokensmethods_controller_action_params_idx');
        $this->renameIndex('tokensmethods_user_id', 'tokensmethods_user_id_idx');
        $this->renameIndex('tokensmethods_controller', 'tokensmethods_controller_idx');
        $this->renameIndex('tokensmethods_action', 'tokensmethods_action_idx');
        $this->renameIndex('tokensmethods_php_sid', 'tokensmethods_php_sid_idx');

        $this->renameIndex('idx_typeseances_lft_rght', 'typeseances_lft_rght_idx');
        $this->renameIndex('typeseancenature_nature', 'typeseances_typeactes_typeacte_id_idx');
        $this->renameIndex(
            'typeseance_id',
            'typeseances_typeacteurs_typeseance_id_typeacteur_id_idx'
        );

        $this->renameIndex('username', 'users_username_idx');

        $this->renameIndex('acteur_id', 'votes_acteur_id_idx');
        $this->renameIndex('deliberation_id', 'votes_delib_id_idx');

        $this->renameIndex('created_user_id', 'wkf_circuits_created_user_id_idx');
        $this->renameIndex('modified_user_id', 'wkf_circuits_modified_user_id_idx');

        $this->renameIndex('etape_id', 'wkf_compositions_etape_id_idx');
        $this->renameIndex('trigger', 'wkf_compositions_trigger_id_idx');

        $this->renameIndex('circuit_id', 'wkf_etapes_circuit_id_idx');
        $this->renameIndex('nom', 'wkf_etapes_nom_idx');

        $this->renameIndex('circuits', 'wkf_traitements_circuit_id_idx');
        $this->renameIndex('target', 'wkf_traitements_target_id_idx');
        $this->renameIndex('traitements_treated', 'wkf_traitements_treated_orig_idx');

        $this->renameIndex('wkf_visas_traitements', 'wkf_visas_traitement_id_idx');
        $this->renameIndex('wkf_visas_trigger_id', 'wkf_visas_trigger_id_idx');
    }

    public function down(): void
    {

    }
}
