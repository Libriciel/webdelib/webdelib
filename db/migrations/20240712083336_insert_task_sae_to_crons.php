<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class InsertTaskSaeToCrons extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up(): void
    {
        $table = $this->table('crons');
        $task = [
            'id'    => 12,
            'nom'  => 'SAE : Récupèration des statuts de versement (erreurs, validations et refus)',
            'description' => 'Récupère les statuts des versements SAE (erreurs, validations et refus)',
            'plugin' => null,
            'model' => 'CronJob',
            'action' => 'updateSaeStatus',
            'has_params' => null,
            'params' => null,
            'next_execution_time' => null,
            'execution_duration' => 'PT5M',
            'last_execution_start_time' => null,
            'last_execution_end_time' => null,
            'last_execution_report' => 'Cette tâche n\'a encore jamais été exécutée.',
            'last_execution_status' => null,
            'active' => true,
            'created' => date("Y-m-d H:i:s"),
            'created_user_id' => '1',
            'modified' => date("Y-m-d H:i:s"),
            'modified_user_id' => '1',
            'lock' => false
        ];
        $table->insert($task)->saveData();
    }

    public function down(): void
    {

    }
}
