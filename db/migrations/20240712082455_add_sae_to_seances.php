<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddSaeToSeances extends AbstractMigration
{
    public function up(): void
    {
        $this->table('seances')
            ->addColumn('sae_numero_versement', 'integer', [
                'null' => false,
                'default' => 0,
            ])
            ->addColumn('sae_pastell_id', 'string', ['null' => true, 'limit' => 10])
            ->addColumn('sae_etat', 'integer', [
                'null' => false,
                'default' => 0,
            ])
            ->addColumn('sae_date', 'timestamp')
            ->addColumn('sae_commentaire', 'string', ['limit' => 1500])
            ->addColumn('sae_atr', 'binary')
            ->update();

        $this->table('typeactes')
            ->addColumn('verser_sae', 'boolean', [
                'default' => false,
                'null' => false,
            ])
            ->changeColumn('teletransmettre', 'boolean', [
                'default' => false,
                'null' => false,
            ])
            ->changeColumn('deliberant', 'boolean', [
                'default' => false,
                'null' => false,
            ])
            ->changeColumn('publier', 'boolean', [
                'default' => false,
                'null' => false,
            ])
            ->update();

        $this->table('deliberations')->addColumn('sae_date', 'timestamp')
            ->update();

        $this->table('infosupdefs')->addColumn('joindre_sae', 'boolean', ['default' => false])
            ->update();

        $this->table('users')->addColumn('mail_error_sae', 'boolean', ['null' => true, 'default' => false])
            ->update();
    }

    public function down(): void
    {

    }
}
