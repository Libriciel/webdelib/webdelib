<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AlterTypePublierEtatToDeliberations extends AbstractMigration
{
    use dbSeeder\MigrationTrait;

    public function up(): void
    {
        $this->execute(sprintf('
            ALTER TABLE %s ALTER COLUMN publier_etat DROP DEFAULT;
        ', $this->getAquoteTableName('deliberations')));

        $this->execute(sprintf('
            ALTER TABLE %s
            ALTER COLUMN "publier_etat" TYPE integer USING publier_etat::integer,
            ALTER COLUMN "publier_etat" SET NOT NULL,
            ALTER COLUMN "publier_etat" SET DEFAULT 0;
        ', $this->getAquoteTableName('deliberations')));
    }

    public function down(): void
    {

    }

}
