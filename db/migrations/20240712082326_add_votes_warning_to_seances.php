<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddVotesWarningToSeances extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('seances');
        $table->addColumn('idelibre_votes_warning', 'json')
            ->update();
    }
}
