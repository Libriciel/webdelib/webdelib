<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class DropForeignKeyToDeliberationsVersions extends AbstractMigration
{
    public function up(): void
    {
        $table = $this->table('deliberations_versions');
        $exists = $table->hasForeignKey('deliberations_versions_users_id_fk');
        if($exists) {
            $table->dropForeignKey('deliberations_versions_users_id_fk')
                ->save();
        }
    }

    public function down(): void
    {
    }
}
