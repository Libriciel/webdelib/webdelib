<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AlterPositionToActeurs extends AbstractMigration
{
    use dbSeeder\MigrationTrait;

    public function up(): void
    {
        if ($this->notExistsData()) {
            return;
        }

        $this->table('acteurs')
            ->changeColumn(
                'position',
                'integer',
                [
                    'null' => true, 'default' => null
                ]
            )->save();
    }
    public function down(): void
    {

    }
}
