<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class MigrateSerialToIndentity extends AbstractMigration
{
    use dbSeeder\MigrationTrait;

    public function up(): void
    {
        if ($this->notExistsData()) {
            return;
        }

        $sql = "SELECT 'ALTER TABLE '||table_schema||'.\"'||TABLE_NAME||'\" ALTER '||COLUMN_NAME||' DROP DEFAULT;
                '||replace('DROP SEQUENCE '''||substring(column_default, 9, length(column_default)-19), '''', '')||';
                ALTER TABLE  '||table_schema||'.\"'||TABLE_NAME||'\" ALTER '||COLUMN_NAME||' ADD GENERATED "
            . "BY DEFAULT AS IDENTITY;
                SELECT SETVAL(pg_get_serial_sequence('''||table_schema||'.'||TABLE_NAME||''', '''||COLUMN_NAME||'''),
                (SELECT COALESCE(MAX('||COLUMN_NAME||'), 0) + 1 FROM '||table_schema||'.'||TABLE_NAME||'), false);'
                FROM information_schema.columns
                WHERE column_default LIKE 'nextval%'";

        $this->execute($sql);
    }

    public function down(): void
    {

    }
}
