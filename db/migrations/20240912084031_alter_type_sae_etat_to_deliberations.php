<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AlterTypeSaeEtatToDeliberations extends AbstractMigration
{
    use dbSeeder\MigrationTrait;

    public function up(): void
    {
        $this->execute(sprintf(
            '
            UPDATE %s SET "sae_etat" = FALSE WHERE sae_etat IS NULL',
            $this->getAquoteTableName('deliberations')
        ));

        $this->execute(sprintf('
            ALTER TABLE %s
            ALTER COLUMN "sae_etat" TYPE integer USING CASE WHEN sae_etat THEN 1 ELSE 0 END,
            ALTER COLUMN "sae_etat" SET NOT NULL,
            ALTER COLUMN "sae_etat" SET DEFAULT 0;
        ', $this->getAquoteTableName('deliberations')));

        $this->table('deliberations')
            ->addColumn('sae_commentaire', 'string', ['limit' => 1500])
            ->addColumn('sae_atr', 'binary')
            ->update();
    }

    public function down(): void
    {
    }
}
