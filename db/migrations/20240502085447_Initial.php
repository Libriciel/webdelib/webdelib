<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class Initial extends AbstractMigration
{
    use dbSeeder\MigrationTrait;

    public function up()
    {
        if (!$this->notExistsData()) {
            return;
        }

        $this->table('acos', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'limit' => 11,
                'identity' => true,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])
            ->addColumn('parent_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('model', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('foreign_key', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('alias', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('lft', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('rght', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->create();

        $this->table('acteurs', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])
            ->addColumn('typeacteur_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('nom', 'string', [
                'default' => '',
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('prenom', 'string', [
                'default' => '',
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('salutation', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('titre', 'string', [
                'default' => null,
                'limit' => 250,
                'null' => true,
            ])
            ->addColumn('position', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('adresse1', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('adresse2', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('cp', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('ville', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('telfixe', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->addColumn('telmobile', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->addColumn('suppleant_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('note', 'string', [
                'default' => null,
                'limit' => 1500,
                'null' => false,
            ])
            ->addColumn('actif', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('notif_insertion', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('notif_projet_valide', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'typeacteur_id',
                ],
                ['name' => 'acteurs_typeacteur_id_idx']
            )
            ->addIndex(
                [
                    'suppleant_id',
                ],
                ['name' => 'acteurs_suppleant_id_idx']
            )
            ->addIndex(
                [
                    'actif',
                ],
                ['name' => 'acteurs_actif_idx']
            )
            ->create();

        $this->table('acteurs_seances', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])
            ->addColumn('acteur_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('seance_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('mail_id', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->addColumn('date_envoi', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('date_reception', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('model', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->create();

        $this->table('acteurs_services', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('acteur_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('service_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'acteur_id',
                ],
                ['name' => 'acteurs_services_acteur_id_idx']
            )
            ->addIndex(
                [
                    'service_id',
                ],
                ['name' => 'acteurs_services_service_id_idx']
            )
            ->create();

        $this->table('annexes', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('model', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('foreign_key', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('joindre_ctrl_legalite', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('joindre_fusion', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('titre', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => true,
            ])
            ->addColumn('filename', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('filetype', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('size', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('data', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('data_pdf', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('edition_data', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('edition_data_typemime', 'string', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('position', 'smallinteger', [
                'default' => null,
                'limit' => 5,
                'null' => true,
            ])
            ->addColumn('tdt_data_pdf', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('tdt_data_edition', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('typologiepiece_code', 'string', [
                'default' => null,
                'limit' => 5,
                'null' => true,
            ])
            ->addIndex(
                [
                    'model',
                    'foreign_key',
                ],
                ['name' => 'annexes_model_foreign_key_idx']
            )
            ->addIndex(
                [
                    'foreign_key',
                    'joindre_fusion',
                ],
                ['name' => 'annexes_foreign_key_joindre_fusion_idx']
            )
            ->create();

        $this->table('annexes_versions', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('version_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('data', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('edition_data', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('data_pdf', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('data_annexe', 'json', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('foreign_key', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('model', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('tdt_data_edition', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('tdt_data_pdf', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('aros', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('parent_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('model', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('foreign_key', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('alias', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('lft', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('rght', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->create();

        $this->table('aros_acos', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('aro_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('aco_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('_create', 'string', [
                'default' => '0',
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('_read', 'string', [
                'default' => '0',
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('_update', 'string', [
                'default' => '0',
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('_delete', 'string', [
                'default' => '0',
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('_manager', 'string', [
                'default' => '0',
                'limit' => 2,
                'null' => false,
            ])
            ->create();

        $this->table('circuits_users')
            ->addColumn('circuit_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'user_id',
                ],
                ['name' => 'circuits_users_user_id_idx']
            )
            ->create();

        $this->table('collectivites', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('id_entity', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('nom', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => false,
            ])
            ->addColumn('adresse', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('CP', 'string', [
                'default' => null,
                'limit' => 5,
                'null' => false,
            ])
            ->addColumn('ville', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('telephone', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('logo', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('templateProject', 'json', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('background', 'string', [
                'default' => '#42a1e8',
                'limit' => 25,
                'null' => true,
            ])
            ->addColumn('config_login', 'json', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('siret', 'string', [
                'default' => null,
                'limit' => 14,
                'null' => true,
            ])
            ->addColumn('code_ape', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('responsable_nom', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('responsable_prenom', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('responsable_fonction', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('responsable_email', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('dpo_nom', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('dpo_prenom', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('dpo_email', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('saas', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('force', 'integer', [
                'default' => '5',
                'limit' => 10,
                'null' => true,
            ])
            ->create();

        $this->table('commentaires', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('delib_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('agent_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('texte', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('pris_en_compte', 'smallinteger', [
                'default' => '0',
                'limit' => 5,
                'null' => false,
            ])
            ->addColumn('commentaire_auto', 'boolean', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->create();

        $this->table('compteurs', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('nom', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('commentaire', 'string', [
                'default' => null,
                'limit' => 1500,
                'null' => false,
            ])
            ->addColumn('def_compteur', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('sequence_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('def_reinit', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->create();

        $this->table('crons', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('nom', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('description', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('plugin', 'string', [
                'default' => '',
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('action', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('has_params', 'boolean', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('params', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('next_execution_time', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('execution_duration', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('last_execution_start_time', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('last_execution_end_time', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('last_execution_report', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('last_execution_status', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('active', 'boolean', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created_user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified_user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('model', 'string', [
                'default' => 'CronJob',
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('lock', 'boolean', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('run_all', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('deliberations', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('typeacte_id', 'integer', [
                'default' => '1',
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('circuit_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('theme_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('service_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('redacteur_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('rapporteur_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('anterieure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('is_multidelib', 'boolean', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('parent_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('objet', 'string', [
                'default' => null,
                'limit' => 1000,
                'null' => false,
            ])
            ->addColumn('objet_delib', 'string', [
                'default' => null,
                'limit' => 1000,
                'null' => true,
            ])
            ->addColumn('titre', 'string', [
                'default' => null,
                'limit' => 1000,
                'null' => true,
            ])
            ->addColumn('num_delib', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true,
            ])
            ->addColumn('num_pref', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('pastell_id', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('tdt_id', 'string', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('tdt_dateAR', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('texte_projet', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('texte_projet_name', 'string', [
                'default' => null,
                'limit' => 75,
                'null' => true,
            ])
            ->addColumn('texte_projet_type', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('texte_projet_size', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('texte_synthese', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('texte_synthese_name', 'string', [
                'default' => null,
                'limit' => 75,
                'null' => true,
            ])
            ->addColumn('texte_synthese_type', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('texte_synthese_size', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('deliberation', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('deliberation_name', 'string', [
                'default' => null,
                'limit' => 75,
                'null' => true,
            ])
            ->addColumn('deliberation_type', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('deliberation_size', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('date_limite', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('date_envoi', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('etat', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('parapheur_etat', 'smallinteger', [
                'default' => null,
                'limit' => 5,
                'null' => true,
            ])
            ->addColumn('parapheur_commentaire', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('sae_etat', 'boolean', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('reporte', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('montant', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('debat', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('debat_name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('debat_size', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('debat_type', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('avis', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('vote_nb_oui', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('vote_nb_non', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('vote_nb_abstention', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('vote_nb_retrait', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('vote_commentaire', 'string', [
                'default' => null,
                'limit' => 1500,
                'null' => true,
            ])
            ->addColumn('delib_pdf', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('signature', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('signee', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('commission', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('commission_size', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('commission_type', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('commission_name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('date_acte', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('date_envoi_signature', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('parapheur_id', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('tdt_data_pdf', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('tdt_data_bordereau_pdf', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('president_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('parapheur_cible', 'string', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('parapheur_bordereau', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('tdt_ar', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('tdt_ar_date', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('vote_prendre_acte', 'boolean', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('signature_type', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('numero_depot', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('texte_projet_active', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('texte_synthese_active', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('texte_acte_active', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('tdt_data_edition', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('vote_resultat', 'boolean', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('tdt_message', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('tdt_status', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('tdt_document_papier', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('typologiepiece_code', 'string', [
                'default' => null,
                'limit' => 5,
                'null' => true,
            ])
            ->addColumn('publier_etat', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('publier_date', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'typeacte_id',
                ],
                ['name' => 'deliberations_typeacte_id_idx']
            )
            ->addIndex(
                [
                    'theme_id',
                ],
                ['name' => 'deliberations_theme_id_idx']
            )
            ->addIndex(
                [
                    'service_id',
                ],
                ['name' => 'deliberations_service_id_idx']
            )
            ->addIndex(
                [
                    'redacteur_id',
                ],
                ['name' => 'deliberations_redacteur_id_idx']
            )
            ->addIndex(
                [
                    'rapporteur_id',
                ],
                ['name' => 'deliberations_rapporteur_id_idx']
            )
            ->addIndex(
                [
                    'parent_id',
                ],
                ['name' => 'deliberations_parent_id_idx']
            )
            ->addIndex(
                [
                    'etat',
                ],
                ['name' => 'deliberations_etat_idx']
            )
            ->create();

        $this->table('deliberations_seances', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('deliberation_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('seance_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('position', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('avis', 'boolean', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('commentaire', 'string', [
                'default' => null,
                'limit' => 1500,
                'null' => true,
            ])
            ->addColumn('reset', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'deliberation_id',
                ],
                ['name' => 'deliberations_seances_deliberation_id_idx']
            )
            ->addIndex(
                [
                    'seance_id',
                ],
                ['name' => 'deliberations_seances_seance_id_idx']
            )
            ->create();

        $this->table('deliberations_typeseances', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('deliberation_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('typeseance_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->create();

        $this->table('deliberations_users', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('deliberation_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->create();

        $this->table('deliberations_versions', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('version_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('model', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('foreign_key', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('tdt_data_edition', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('tdt_ar', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('parapheur_bordereau', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('commission', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('tdt_data_bordereau', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('delib_pdf', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('debat', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('deliberation', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('texte_synthese', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('texte_projet', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('signature', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('tdt_data_pdf', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('data_deliberation', 'json', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->create();

        $this->table('historiques', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('delib_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('circuit_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('commentaire', 'text', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('revision_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addIndex(
                [
                    'user_id',
                ],
                ['name' => 'historiques_user_id_idx']
            )
            ->create();

        $this->table('infosupdefs', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('model', 'string', [
                'default' => 'Deliberation',
                'limit' => 25,
                'null' => false,
            ])
            ->addColumn('nom', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('commentaire', 'string', [
                'default' => null,
                'limit' => 1500,
                'null' => false,
            ])
            ->addColumn('ordre', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('code', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('type', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('val_initiale', 'string', [
                'default' => null,
                'limit' => 1000,
                'null' => true,
            ])
            ->addColumn('recherche', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('actif', 'boolean', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('typemime', 'string', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('joindre_fusion', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('reactcomponent', 'boolean', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('edit_post_sign', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('api', 'string', [
                'default' => null,
                'limit' => 150,
                'null' => true,
            ])
            ->addColumn('api_type', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('api_limit', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('api_query', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('correlation', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addIndex(
                [
                    'model',
                ],
                ['name' => 'infosupdefs_model_idx']
            )
            ->create();

        $this->table('infosupdefs_profils', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('profil_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('infosupdef_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->create();

        $this->table('infosuplistedefs', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('infosupdef_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('ordre', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('nom', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('actif', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addIndex(
                [
                    'infosupdef_id',
                    'ordre',
                ],
                ['name' => 'infosuplistedefs_infosupdef_id_ordre_idx']
            )
            ->create();

        $this->table('infosups', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('model', 'string', [
                'default' => 'Deliberation',
                'limit' => 25,
                'null' => false,
            ])
            ->addColumn('foreign_key', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('infosupdef_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('text', 'string', [
                'default' => null,
                'limit' => 1000,
                'null' => true,
            ])
            ->addColumn('date', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('file_name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('file_size', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('file_type', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('content', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('edition_data', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('edition_data_typemime', 'string', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('geojson', 'json', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'foreign_key',
                ],
                ['name' => 'infosups_foreign_key_idx']
            )
            ->addIndex(
                [
                    'infosupdef_id',
                ],
                ['name' => 'infosups_infosupdef_id_idx']
            )
            ->create();

        $this->table('ldapm_groups', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('parent_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('lft', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('rght', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 500,
                'null' => true,
            ])
            ->addColumn('dn', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('ldapm_models_groups', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('ldapm_groups_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('model', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('foreign_key', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->create();

        $this->table('listepresences', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('delib_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('acteur_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('present', 'boolean', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('mandataire', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('suppleant_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->create();

        $this->table('modelsections', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'limit' => 11,
                'identity' => true,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('description', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('parent_id', 'integer', [
                'default' => '1',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->create();

        $this->table('modeltemplates', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('modeltype_id', 'integer', [
                'default' => '1',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('filename', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('filesize', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('content', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('file_output_format', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('force', 'boolean', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('modeltypes', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('description', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->create();

        $this->table('modelvalidations', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('modelvariable_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('modelsection_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('modeltype_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('min', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('max', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('actif', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('modelvariables', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('description', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->create();

        $this->table('natures', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('code', 'string', [
                'default' => null,
                'limit' => 3,
                'null' => false,
            ])
            ->addColumn('default', 'string', [
                'default' => '99_AU',
                'limit' => 5,
                'null' => true,
            ])
            ->create();

        $this->table('natures_typologiepieces', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('nature_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('typologiepiece_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->create();

        $this->table('nomenclatures', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('parent_id', 'integer', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('libelle', 'string', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('lft', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('rght', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->create();

        $this->table('profils', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'limit' => 11,
                'identity' => true,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])
            ->addColumn('parent_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('name', 'string', [
                'default' => '',
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('actif', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('role_id', 'integer', [
                'default' => '1',
                'limit' => 10,
                'null' => false,
            ])
            ->create();

        $this->table('roles', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('name', 'text', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('seances', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('type_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('date_convocation', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('date', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('traitee', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('commentaire', 'string', [
                'default' => null,
                'limit' => 1500,
                'null' => true,
            ])
            ->addColumn('secretaire_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('president_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('debat_global', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('debat_global_name', 'string', [
                'default' => null,
                'limit' => 75,
                'null' => true,
            ])
            ->addColumn('debat_global_size', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('debat_global_type', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('pv_figes', 'smallinteger', [
                'default' => null,
                'limit' => 5,
                'null' => true,
            ])
            ->addColumn('pv_sommaire', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('pv_complet', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('numero_depot', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('idelibre_id', 'string', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('parent_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('lft', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('rght', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addIndex(
                [
                    'type_id',
                ],
                ['name' => 'seances_type_id_idx']
            )
            ->addIndex(
                [
                    'traitee',
                ],
                ['name' => 'seances_traitee_idx']
            )
            ->addIndex(
                [
                    'lft',
                    'rght',
                ],
                ['name' => 'seances_lft_rght_idx']
            )
            ->create();

        $this->table('sequences', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('nom', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('commentaire', 'string', [
                'default' => null,
                'limit' => 1500,
                'null' => false,
            ])
            ->addColumn('num_sequence', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('debut_validite', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('fin_validite', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('services', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])
            ->addColumn('parent_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('order', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('circuit_defaut_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('actif', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('lft', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('rght', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->create();

        $this->table('services_users', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('user_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('service_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'user_id',
                ],
                ['name' => 'services_users_user_id_idx']
            )
            ->create();

        $this->table('tdt_messages', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('delib_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('tdt_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('tdt_type', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('tdt_etat', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('date_message', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('tdt_data', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('parent_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addIndex(
                [
                    'delib_id',
                ],
                ['name' => 'tdt_messages_delib_id_idx']
            )
            ->create();

        $this->table('themes', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('parent_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('order', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('libelle', 'string', [
                'default' => null,
                'limit' => 500,
                'null' => true,
            ])
            ->addColumn('actif', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('lft', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('rght', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->create();

        $this->table('tokensmethods', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('controller', 'string', [
                'default' => null,
                'limit' => 250,
                'null' => false,
            ])
            ->addColumn('action', 'string', [
                'default' => null,
                'limit' => 250,
                'null' => false,
            ])
            ->addColumn('php_sid', 'string', [
                'default' => null,
                'limit' => 32,
                'null' => true,
            ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('params', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addIndex(
                [
                    'controller',
                    'action',
                    'params',
                ],
                [
                    'name' => 'tokensmethods_controller_action_params_idx',
                    'unique' => true
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ],
                ['name' => 'tokensmethods_user_id_idx']
            )
            ->addIndex(
                [
                    'controller',
                ],
                ['name' => 'tokensmethods_controller_idx']
            )
            ->addIndex(
                [
                    'action',
                ],
                ['name' => 'tokensmethods_action_idx']
            )
            ->addIndex(
                [
                    'php_sid',
                ],
                ['name' => 'tokensmethods_php_sid_idx']
            )
            ->create();

        $this->table('typeactes', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 300,
                'null' => false,
            ])
            ->addColumn('modele_projet_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('modele_final_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('nature_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('compteur_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('created', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('gabarit_projet', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('gabarit_synthese', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('gabarit_acte', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('teletransmettre', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('gabarit_acte_name', 'string', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('gabarit_projet_name', 'string', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('gabarit_synthese_name', 'string', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('gabarit_projet_active', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('gabarit_synthese_active', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('gabarit_acte_active', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modele_bordereau_projet_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('deliberant', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('publier', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('typeacteurs', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('nom', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('commentaire', 'string', [
                'default' => null,
                'limit' => 1500,
                'null' => false,
            ])
            ->addColumn('elu', 'boolean', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('actif', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('typeseances', ['id' => false, ''])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])
            ->addColumn('libelle', 'string', [
                'default' => null,
                'limit' => 300,
                'null' => false,
            ])
            ->addColumn('retard', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('action', 'smallinteger', [
                'default' => null,
                'limit' => 5,
                'null' => false,
            ])
            ->addColumn('compteur_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('modele_projet_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('modele_deliberation_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('modele_convocation_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('modele_ordredujour_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('modele_pvsommaire_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('modele_pvdetaille_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('color', 'string', [
                'default' => null,
                'limit' => 7,
                'null' => true,
            ])
            ->addColumn('modele_journal_seance_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('parent_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('lft', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('rght', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addIndex(
                [
                    'lft',
                    'rght',
                ],
                ['name' => 'typeseances_lft_rght_idx']
            )
            ->create();

        $this->table('typeseances_acteurs', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])
            ->addColumn('typeseance_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('acteur_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->create();

        $this->table('typeseances_typeactes', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('typeseance_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('typeacte_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'typeacte_id',
                ],
                ['name' => 'typeseances_typeactes_typeacte_id_idx']
            )
            ->create();

        $this->table('typeseances_typeacteurs', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])
            ->addColumn('typeseance_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('typeacteur_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'typeseance_id',
                    'typeacteur_id',
                ],
                ['name' => 'typeseances_typeacteurs_typeseance_id_typeacteur_id_idx']
            )
            ->create();

        $this->table('typologiepieces', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('code', 'string', [
                'default' => null,
                'limit' => 5,
                'null' => false,
            ])
            ->addColumn('lft', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('rght', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->create();

        $this->table('users', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('profil_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('statut', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('username', 'string', [
                'default' => '',
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('note', 'string', [
                'default' => null,
                'limit' => 1500,
                'null' => true,
            ])
            ->addColumn('circuit_defaut_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('password', 'string', [
                'default' => '',
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('nom', 'string', [
                'default' => '',
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('prenom', 'string', [
                'default' => '',
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => '',
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('telfixe', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->addColumn('telmobile', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->addColumn('date_naissance', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('accept_notif', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('mail_refus', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('mail_traitement', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('mail_insertion', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('position', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('mail_modif_projet_cree', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('mail_modif_projet_valide', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('mail_retard_validation', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('theme', 'string', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('active', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('mail_error_send_tdt_auto', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('mail_projet_valide', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('mail_projet_valide_valideur', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('mail_majtdtcourrier', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('service_defaut_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('preference_first_login_active', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'username',
                ],
                [
                    'unique' => true,
                    'name' => 'users_username_idx'
                ]
            )
            ->create();

        $this->table('versions', ['id' => false, 'primary_key' => ['id', 'subject', 'version']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])
            ->addColumn('subject', 'string', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('version', 'string', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->create();

        $this->table('votes')
            ->addColumn('acteur_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('delib_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('resultat', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addIndex(
                [
                    'acteur_id',
                ],
                ['name' => 'votes_acteur_id_idx']
            )
            ->addIndex(
                [
                    'delib_id',
                ],
                ['name' => 'votes_delib_id_idx']
            )
            ->create();

        $this->table('wkf_circuits', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('nom', 'string', [
                'default' => null,
                'limit' => 250,
                'null' => false,
            ])
            ->addColumn('description', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('actif', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('defaut', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created_user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('modified_user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addIndex(
                [
                    'created_user_id',
                ],
                ['name' => 'wkf_circuits_created_user_id_idx']
            )
            ->addIndex(
                [
                    'modified_user_id',
                ],
                ['name' => 'wkf_circuits_modified_user_id_idx']
            )
            ->create();

        $this->table('wkf_compositions', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('etape_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('type_validation', 'string', [
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('trigger_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('created_user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('modified_user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('soustype', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('type_composition', 'string', [
                'default' => 'USER',
                'limit' => 20,
                'null' => true,
            ])
            ->addIndex(
                [
                    'etape_id',
                ],
                ['name' => 'wkf_compositions_etape_id_idx']
            )
            ->addIndex(
                [
                    'trigger_id',
                ],
                ['name' => 'wkf_compositions_trigger_id_idx']
            )
            ->create();

        $this->table('wkf_etapes', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('circuit_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('nom', 'string', [
                'default' => null,
                'limit' => 250,
                'null' => false,
            ])
            ->addColumn('description', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('type', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('ordre', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('created_user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('modified_user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('soustype', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('cpt_retard', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addIndex(
                [
                    'circuit_id',
                ],
                ['name' => 'wkf_etapes_circuit_id_idx']
            )
            ->addIndex(
                [
                    'nom',
                ],
                ['name' => 'wkf_etapes_nom_idx']
            )
            ->create();

        $this->table('wkf_signatures', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('type_signature', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('signature', 'text', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('visa_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->create();

        $this->table('wkf_traitements', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])
            ->addColumn('circuit_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('target_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('numero_traitement', 'integer', [
                'default' => '1',
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('treated_orig', 'smallinteger', [
                'default' => '0',
                'limit' => 5,
                'null' => false,
            ])
            ->addColumn('created_user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('modified_user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'limit' => null,
                'null' => false,
                'timezone' => false,
            ])
            ->addColumn('treated', 'boolean', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'circuit_id',
                ],
                ['name' => 'wkf_traitements_circuit_id_idx']
            )
            ->addIndex(
                [
                    'target_id',
                ],
                ['name' => 'wkf_traitements_target_id_idx']
            )
            ->addIndex(
                [
                    'treated_orig',
                ],
                ['name' => 'wkf_traitements_treated_orig_idx']
            )
            ->create();

        $this->table('wkf_visas', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', [
                'identity' => true,
                'limit' => 11,
                'generated' => \Phinx\Db\Adapter\PostgresAdapter::GENERATED_BY_DEFAULT,
            ])

            ->addColumn('traitement_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('trigger_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('signature_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('etape_nom', 'string', [
                'default' => null,
                'limit' => 250,
                'null' => true,
            ])
            ->addColumn('etape_type', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('action', 'string', [
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('commentaire', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('date', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('numero_traitement', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('type_validation', 'string', [
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('etape_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('date_retard', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'traitement_id',
                ],
                ['name' => 'wkf_visas_traitement_id_idx']
            )
            ->addIndex(
                [
                    'trigger_id',
                ],
                ['name' => 'wkf_visas_trigger_id_idx']
            )
            ->create();

        $this->table('modelsections')
            ->addForeignKey(
                'parent_id',
                'modelsections',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('modelvalidations')
            ->addForeignKey(
                'modelsection_id',
                'modelsections',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'modeltype_id',
                'modeltypes',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'modelvariable_id',
                'modelvariables',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('tokensmethods')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('wkf_signatures')
            ->addForeignKey(
                'visa_id',
                'wkf_visas',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $table = $this->table('natures');
        $rows = [
            [
                'id'    => 1,
                'name'  => 'Délibérations',
                'code' => 'DE',
                'default' => '99_DE'
            ],
            [
                'id'    => 2,
                'name'  => 'Arrêtés Réglementaires',
                'code' => 'AR',
                'default' => '99_AR'
            ],
            [
                'id'    => 3,
                'name'  => 'Arrêtés Individuels',
                'code' => 'AI',
                'default' => '99_AI'
            ],
            [
                'id'    => 4,
                'name'  => 'Contrats et conventions',
                'code' => 'CC',
                'default' => '99_DC'
            ],
            [
                'id'    => 5,
                'name'  => 'Autres',
                'code' => 'AU',
                'default' => '99_AU'
            ]
        ];
        $table->insert($rows)->saveData();

        $table = $this->table('roles');
        $rows = [
            [
                'id'    => 1,
                'name'  => 'Utilisateur',
            ],
            [
                'id'    => 2,
                'name'  => 'Administrateur',
            ],
            [
                'id'    => 3,
                'name'  => 'Administrateur fonctionnel',
            ],
        ];
        $table->insert($rows)->saveData();

        $table = $this->table('crons');
        $rows = [
            [
                'id'    => 1,
                'nom'  => 'Circuits de traitement : Mise à jour des traitements distants (Parapheur)',
                'description' => 'Mise à jour de l\'état des projets envoyés au parapheur pour validation (délégation dans le circuit de traitement)',
                'plugin' => null,
                'model' => 'CronJob',
                'action' => 'majParapheurDelegation',
                'has_params' => null,
                'params' => null,
                'next_execution_time' => null,
                'execution_duration' => 'PT1H',
                'last_execution_start_time' => null,
                'last_execution_end_time' => null,
                'last_execution_report' => 'Cette tâche n\'a encore jamais été exécutée.',
                'last_execution_status' => null,
                'active' => true,
                'created' => date("Y-m-d H:i:s"),
                'created_user_id' => '1',
                'modified' => date("Y-m-d H:i:s"),
                'modified_user_id' => '1',
                'lock' => false
            ],
            [
                'id'    => 2,
                'nom'  => 'Circuits de traitement : Déclenchement des alertes de retard',
                'description' => 'Relance les utilisateurs qui sont en retard dans la validation d\'un projet dans leur bannette',
                'plugin' => null,
                'model' => 'CronJob',
                'action' => 'alerteCakeflowRetard',
                'has_params' => null,
                'params' => null,
                'next_execution_time' => null,
                'execution_duration' => 'P1D',
                'last_execution_start_time' => null,
                'last_execution_end_time' => null,
                'last_execution_report' => 'Cette tâche n\'a encore jamais été exécutée.',
                'last_execution_status' => null,
                'active' => true,
                'created' => date("Y-m-d H:i:s"),
                'created_user_id' => '1',
                'modified' => date("Y-m-d H:i:s"),
                'modified_user_id' => '1',
                'lock' => false
            ],
            [
                'id'    => 3,
                'nom'  => 'Signature : Mise à jour de l\'état des dossiers envoyés au Parapheur',
                'description' => 'Met à jour l\'état des projets envoyés au Parapheur pour signature et rapatrie les informations de ceux en fin de circuit',
                'plugin' => null,
                'model' => 'CronJob',
                'action' => 'majParapheurSignature',
                'has_params' => null,
                'params' => null,
                'next_execution_time' => null,
                'execution_duration' => 'PT30M',
                'last_execution_start_time' => null,
                'last_execution_end_time' => null,
                'last_execution_report' => 'Cette tâche n\'a encore jamais été exécutée.',
                'last_execution_status' => null,
                'active' => true,
                'created' => date("Y-m-d H:i:s"),
                'created_user_id' => '1',
                'modified' => date("Y-m-d H:i:s"),
                'modified_user_id' => '1',
                'lock' => false
            ],
            [
                'id'    => 4,
                'nom'  => 'Mail sécurisé : Mise à jour des mails sécurisés',
                'description' => 'Envoi/Réception des mails sécurisés',
                'plugin' => null,
                'model' => 'CronJob',
                'action' => 'majTdtMailSec',
                'has_params' => null,
                'params' => null,
                'next_execution_time' => null,
                'execution_duration' => 'PT2H',
                'last_execution_start_time' => null,
                'last_execution_end_time' => null,
                'last_execution_report' => 'Cette tâche n\'a encore jamais été exécutée.',
                'last_execution_status' => null,
                'active' => true,
                'created' => date("Y-m-d H:i:s"),
                'created_user_id' => '1',
                'modified' => date("Y-m-d H:i:s"),
                'modified_user_id' => '1',
                'lock' => false
            ],
            [
                'id'    => 5,
                'nom'  => 'TdT : Récupération des documents retours de télétransmissions',
                'description' => 'Récupération des documents de télétransmission (accusé de réception, bordereau, documents tamponnés ...)',
                'plugin' => null,
                'model' => 'CronJob',
                'action' => 'updateTdtFiles',
                'has_params' => null,
                'params' => null,
                'next_execution_time' => null,
                'execution_duration' => 'PT5M',
                'last_execution_start_time' => null,
                'last_execution_end_time' => null,
                'last_execution_report' => 'Cette tâche n\'a encore jamais été exécutée.',
                'last_execution_status' => null,
                'active' => true,
                'created' => date("Y-m-d H:i:s"),
                'created_user_id' => '1',
                'modified' => date("Y-m-d H:i:s"),
                'modified_user_id' => '1',
                'lock' => false
            ],
            [
                'id'    => 6,
                'nom'  => 'TdT : Récupération des documents retours de télétransmissions',
                'description' => 'Récupération des courriers préfectoraux',
                'plugin' => null,
                'model' => 'CronJob',
                'action' => 'updateTdtMinisterialMails',
                'has_params' => null,
                'params' => null,
                'next_execution_time' => null,
                'execution_duration' => 'P1D',
                'last_execution_start_time' => null,
                'last_execution_end_time' => null,
                'last_execution_report' => 'Cette tâche n\'a encore jamais été exécutée.',
                'last_execution_status' => null,
                'active' => true,
                'created' => date("Y-m-d H:i:s"),
                'created_user_id' => '1',
                'modified' => date("Y-m-d H:i:s"),
                'modified_user_id' => '1',
                'lock' => false
            ],
            [
                'id'    => 7,
                'nom'  => 'CONVERSION : conversion des fichiers',
                'description' => 'Conversion des fichiers dans différents formats pour la fusion ou autre télétransmission',
                'plugin' => null,
                'model' => 'CronJob',
                'action' => 'conversionFiles',
                'has_params' => null,
                'params' => null,
                'next_execution_time' => null,
                'execution_duration' => 'P1D',
                'last_execution_start_time' => null,
                'last_execution_end_time' => null,
                'last_execution_report' => 'Cette tâche n\'a encore jamais été exécutée.',
                'last_execution_status' => null,
                'active' => true,
                'created' => date("Y-m-d H:i:s"),
                'created_user_id' => '1',
                'modified' => date("Y-m-d H:i:s"),
                'modified_user_id' => '1',
                'lock' => false
            ],
            [
                'id'    => 8,
                'nom'  => 'LDAP : Synchronisation des utilisateurs et groupes',
                'description' => 'Synchronisation les utilisateurs et groupes',
                'plugin' => null,
                'model' => 'CronJob',
                'action' => 'syncLdap',
                'has_params' => null,
                'params' => null,
                'next_execution_time' => null,
                'execution_duration' => 'P1D',
                'last_execution_start_time' => null,
                'last_execution_end_time' => null,
                'last_execution_report' => 'Cette tâche n\'a encore jamais été exécutée.',
                'last_execution_status' => null,
                'active' => true,
                'created' => date("Y-m-d H:i:s"),
                'created_user_id' => '1',
                'modified' => date("Y-m-d H:i:s"),
                'modified_user_id' => '1',
                'lock' => false
            ],
            [
                'id'    => 9,
                'nom'  => 'Purge des Fichiers Temporaires (WEBDAV)',
                'description' => 'Purge des Fichiers Temporaires (WEBDAV)',
                'plugin' => null,
                'model' => 'CronJob',
                'action' => 'purgeWebDav',
                'has_params' => null,
                'params' => null,
                'next_execution_time' => null,
                'execution_duration' => 'P1D',
                'last_execution_start_time' => null,
                'last_execution_end_time' => null,
                'last_execution_report' => 'Cette tâche n\'a encore jamais été exécutée.',
                'last_execution_status' => null,
                'active' => true,
                'created' => date("Y-m-d H:i:s"),
                'created_user_id' => '1',
                'modified' => date("Y-m-d H:i:s"),
                'modified_user_id' => '1',
                'lock' => false
            ],
            [
                'id'    => 10,
                'nom'  => 'TdT : Envoi des actes en automatique',
                'description' => 'Envoi des actes signés en automatique',
                'plugin' => null,
                'model' => 'CronJob',
                'action' => 'sendTdtAuto',
                'has_params' => null,
                'params' => null,
                'next_execution_time' => null,
                'execution_duration' => 'PT5M',
                'last_execution_start_time' => null,
                'last_execution_end_time' => null,
                'last_execution_report' => 'Cette tâche n\'a encore jamais été exécutée.',
                'last_execution_status' => null,
                'active' => false,
                'created' => date("Y-m-d H:i:s"),
                'created_user_id' => '1',
                'modified' => date("Y-m-d H:i:s"),
                'modified_user_id' => '1',
                'lock' => false
            ],
            [
                'id'    => 11,
                'nom'  => 'TdT : Récupération des statuts de télétransmissions (erreurs et annulations)',
                'description' => 'Récupère les statuts des actes télétransmis (erreurs et annulations)',
                'plugin' => null,
                'model' => 'CronJob',
                'action' => 'updateTdtStatus',
                'has_params' => null,
                'params' => null,
                'next_execution_time' => null,
                'execution_duration' => 'PT2H',
                'last_execution_start_time' => null,
                'last_execution_end_time' => null,
                'last_execution_report' => 'Cette tâche n\'a encore jamais été exécutée.',
                'last_execution_status' => null,
                'active' => true,
                'created' => date("Y-m-d H:i:s"),
                'created_user_id' => '1',
                'modified' => date("Y-m-d H:i:s"),
                'modified_user_id' => '1',
                'lock' => false
            ],
        ];
        $table->insert($rows)->saveData();
    }

    public function down()
    {
        $this->table('modelsections')
            ->dropForeignKey(
                'parent_id'
            )->save();

        $this->table('modelvalidations')
            ->dropForeignKey(
                'modelsection_id'
            )
            ->dropForeignKey(
                'modeltype_id'
            )
            ->dropForeignKey(
                'modelvariable_id'
            )->save();

        $this->table('tokensmethods')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('wkf_signatures')
            ->dropForeignKey(
                'visa_id'
            )->save();

        $this->table('acos')->drop()->save();
        $this->table('acteurs')->drop()->save();
        $this->table('acteurs_seances')->drop()->save();
        $this->table('acteurs_services')->drop()->save();
        $this->table('annexes')->drop()->save();
        $this->table('annexes_versions')->drop()->save();
        $this->table('aros')->drop()->save();
        $this->table('aros_acos')->drop()->save();
        $this->table('circuits_users')->drop()->save();
        $this->table('collectivites')->drop()->save();
        $this->table('commentaires')->drop()->save();
        $this->table('compteurs')->drop()->save();
        $this->table('crons')->drop()->save();
        $this->table('deliberations')->drop()->save();
        $this->table('deliberations_seances')->drop()->save();
        $this->table('deliberations_typeseances')->drop()->save();
        $this->table('deliberations_users')->drop()->save();
        $this->table('deliberations_versions')->drop()->save();
        $this->table('historiques')->drop()->save();
        $this->table('infosupdefs')->drop()->save();
        $this->table('infosupdefs_profils')->drop()->save();
        $this->table('infosuplistedefs')->drop()->save();
        $this->table('infosups')->drop()->save();
        $this->table('ldapm_groups')->drop()->save();
        $this->table('ldapm_models_groups')->drop()->save();
        $this->table('listepresences')->drop()->save();
        $this->table('modelsections')->drop()->save();
        $this->table('modeltemplates')->drop()->save();
        $this->table('modeltypes')->drop()->save();
        $this->table('modelvalidations')->drop()->save();
        $this->table('modelvariables')->drop()->save();
        $this->table('natures')->drop()->save();
        $this->table('natures_typologiepieces')->drop()->save();
        $this->table('nomenclatures')->drop()->save();
        $this->table('profils')->drop()->save();
        $this->table('roles')->drop()->save();
        $this->table('seances')->drop()->save();
        $this->table('sequences')->drop()->save();
        $this->table('services')->drop()->save();
        $this->table('services_users')->drop()->save();
        $this->table('tdt_messages')->drop()->save();
        $this->table('themes')->drop()->save();
        $this->table('tokensmethods')->drop()->save();
        $this->table('typeactes')->drop()->save();
        $this->table('typeacteurs')->drop()->save();
        $this->table('typeseances')->drop()->save();
        $this->table('typeseances_acteurs')->drop()->save();
        $this->table('typeseances_typeactes')->drop()->save();
        $this->table('typeseances_typeacteurs')->drop()->save();
        $this->table('typologiepieces')->drop()->save();
        $this->table('users')->drop()->save();
        $this->table('versions')->drop()->save();
        $this->table('votes')->drop()->save();
        $this->table('wkf_circuits')->drop()->save();
        $this->table('wkf_compositions')->drop()->save();
        $this->table('wkf_etapes')->drop()->save();
        $this->table('wkf_signatures')->drop()->save();
        $this->table('wkf_traitements')->drop()->save();
        $this->table('wkf_visas')->drop()->save();
    }
}
