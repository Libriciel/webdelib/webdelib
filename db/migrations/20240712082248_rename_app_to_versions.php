<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class RenameAppToVersions extends AbstractMigration
{
    public function up(): void
    {
        $this->execute("UPDATE versions SET subject = 'webdelib';");
    }

    public function down(): void
    {
    }
}
