<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AlterTimestamp extends AbstractMigration
{
    use dbSeeder\MigrationTrait;

    public function up(): void
    {
        if ($this->notExistsData()) {
            return;
        }

        $this->changeCakeTimestamp('annexes');
        $this->changeCakeTimestamp('annexes_versions');
        $this->changeCakeTimestamp('tokensmethods');
        $this->changeCakeTimestamp('wkf_circuits');
        $this->changeCakeTimestamp('wkf_compositions');
        $this->changeCakeTimestamp('wkf_etapes');
        $this->changeCakeTimestamp('wkf_traitements');
        $this->changeCakeTimestamp('infosups');
    }

    public function down(): void
    {
    }

}
