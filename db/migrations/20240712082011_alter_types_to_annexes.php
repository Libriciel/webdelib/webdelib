<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AlterTypesToAnnexes extends AbstractMigration
{
    use dbSeeder\MigrationTrait;

    public function up(): void
    {
        if ($this->notExistsData()) {
            return;
        }

        $this->table('annexes')
            ->changeColumn(
                'edition_data_typemime',
                'string',
                [
                    'limit' => '255', 'null' => true, 'default' => null
                ]
            )
            ->changeColumn(
                'titre',
                'string',
                [
                    'limit' => '200', 'null' => true, 'default' => null
                ]
            )->save();
    }

    public function down(): void
    {

    }
}
