<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class DropIndexes extends AbstractMigration
{
    use dbSeeder\MigrationTrait;

    public function up(): void
    {
        if ($this->notExistsData()) {
            return;
        }

        $table = $this->table('acteurs');
        $table->removeIndexByName('index')
            ->save();

        $table = $this->table('deliberations_seances');
        $table->removeIndexByName('deliberations_seances_seance')
            ->save();

        $table = $this->table('infosups');
        $table->removeIndexByName('text')
            ->save();

        $table = $this->table('themes');
        $table->removeIndexByName('themes_left')
            ->save();

        $table = $this->table('typeacteurs');
        $table->removeIndexByName('elu')
            ->save();
    }

    public function down(): void
    {

    }
}
