<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class RenamePrimaryKeyToTypeseances extends AbstractMigration
{
    use dbSeeder\MigrationTrait;

    public function up(): void
    {
        if ($this->notExistsData()) {
            return;
        }

        $table = $this->table('typeseances');
        $exists = $table->hasPrimaryKey('typeseances_id_key');
        if ($exists) {
            $this->execute('ALTER TABLE typeseances RENAME CONSTRAINT typeseances_id_key TO typeseances_pkey;');
        }
    }

    public function down(): void
    {
    }
}
