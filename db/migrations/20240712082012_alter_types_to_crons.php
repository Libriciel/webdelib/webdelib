<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AlterTypesToCrons extends AbstractMigration
{
    use dbSeeder\MigrationTrait;

        public function up(): void
    {
        if ($this->notExistsData()) {
            return;
        }

        $this->table('crons')
            ->changeColumn(
                'model',
                'string',
                [
                    'limit' => '255', 'null' => false, 'default' => 'CronJob'
                ]
            )->save();
    }

    public function down(): void
    {

    }
}
