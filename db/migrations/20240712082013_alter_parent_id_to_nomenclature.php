<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AlterParentIdToNomenclature extends AbstractMigration
{
    use dbSeeder\MigrationTrait;

    public function up(): void
    {
        if ($this->notExistsData()) {
            return;
        }

        $this->execute(sprintf(
            '
            ALTER TABLE %s
            ALTER COLUMN "parent_id" TYPE integer USING parent_id::integer,
            ALTER COLUMN "parent_id" DROP NOT NULL,
            ALTER COLUMN "parent_id" DROP DEFAULT;',
            $this->getAquoteTableName('nomenclatures')
        ));
    }

    public function down(): void
    {
    }
}
