<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddSignatureDateToDeliberations extends AbstractMigration
{
    use dbSeeder\MigrationTrait;

    public function up(): void
    {
        $this->table('deliberations')
            ->addColumn('signature_date', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->update();

        $this->execute(sprintf(
            'UPDATE %s SET signature_date =  acteSignee.date_envoi_signature'
            . ' FROM (SELECT date_envoi_signature FROM %s WHERE signee=true) AS acteSignee',
            $this->getAquoteTableName('deliberations'),
            $this->getAquoteTableName('deliberations')
        ));
    }

    public function down(): void
    {

    }
}
