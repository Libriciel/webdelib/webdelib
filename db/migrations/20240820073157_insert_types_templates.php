<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class InsertTypesTemplates extends AbstractMigration
{
    public function up(): void
    {
        $this->execute('TRUNCATE TABLE modeltypes CASCADE');
        $table = $this->table('modeltypes');
        $rows = [
            [
                'id'    => 1,
                'name'  => 'Toutes éditions',
                'description'  => 'Toutes éditions',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 2,
                'name'  => 'Projet',
                'description'  => 'Projet',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 3,
                'name'  => 'Délibération',
                'description'  => 'Délibération',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 4,
                'name'  => 'Convocation',
                'description'  => 'Convocation',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 5,
                'name'  => 'Ordre du jour',
                'description'  => 'Ordre du jour',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 6,
                'name'  => 'PV sommaire',
                'description'  => 'PV sommaire',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 7,
                'name'  => 'PV détaillé',
                'description'  => 'PV détaillé',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 8,
                'name'  => 'Recherche',
                'description'  => 'Recherche',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 9,
                'name'  => 'Multi-séance',
                'description'  => 'Multi-séance',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 10,
                'name'  => 'Bordereau de projet',
                'description'  => 'Bordereau de projet',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 11,
                'name'  => 'Journal de séance',
                'description'  => 'Journal de séance',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
        ];
        $table->insert($rows)->saveData();
    }

    public function down(): void
    {
    }
}
