<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class InsertModelSections extends AbstractMigration
{
    public function up(): void
    {
        $this->execute('TRUNCATE TABLE modelsections CASCADE');
        $this->execute("DROP INDEX IF EXISTS modelsections_parent_id;");
        $table = $this->table('modelsections');
        $rows = [
            [
                'id'    => 1,
                'name'  => '#Document',
                'description'  => 'En dehors de toutes sections',
                'parent_id'  => null,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 2,
                'name'  => 'Projets',
                'description'  => 'Itération sur les projets',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 3,
                'name'  => 'Seances',
                'description'  => 'Itération sur les seances',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 4,
                'name'  => 'AvisProjet',
                'description'  => 'Itération sur les Avis',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 5,
                'name'  => 'Annexes',
                'description'  => 'Itération sur les annexes',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 6,
                'name'  => 'ProjetAvis',
                'description'  => 'Itération sur les avis',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 7,
                'name'  => 'ListeSeances',
                'description'  => 'Itération sur les seances',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 8,
                'name'  => 'CircuitHistorique',
                'description'  => 'Itération sur les historiques du circuit',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 9,
                'name'  => 'CircuitHistorique_valideurs',
                'description'  => 'Itération sur les actions de l\'historiques du circuit',
                'parent_id'  => 8,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 10,
                'name'  => 'CircuitHistorique_historiques',
                'description'  => 'Itération sur les actions de l\'historiques du circuit',
                'parent_id'  => 8,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id'    => 11,
                'name'  => 'CircuitHistorique_commentaires',
                'description'  => 'Itération sur les commentaires d\'historique du circuit',
                'parent_id'  => 8,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
        ];
        $table->insert($rows)->saveData();
    }
    public function down(): void
    {
    }

}
