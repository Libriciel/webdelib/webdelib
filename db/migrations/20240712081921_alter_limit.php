<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AlterLimit extends AbstractMigration
{
    use dbSeeder\MigrationTrait;

    public function up(): void
    {
        if ($this->notExistsData()) {
            return;
        }

        $this->changeCakeString('deliberations', 'tdt_id');
        $this->changeCakeString('deliberations', 'parapheur_cible');
        $this->changeCakeString('annexes', 'edition_data_typemime');
        $this->changeCakeString('seances', 'idelibre_id');
        $this->changeCakeString('infosups', 'edition_data_typemime');
        $this->changeCakeString('infosupdefs', 'typemime');
        $this->changeCakeString('typeactes', 'gabarit_acte_name');
        $this->changeCakeString('typeactes', 'gabarit_projet_name');
        $this->changeCakeString('typeactes', 'gabarit_synthese_name');
        $this->changeCakeString('typologiepieces', 'name', false);
        $this->changeCakeString('nomenclatures', 'name', false);
        $this->changeCakeString('nomenclatures', 'libelle', false);
        $this->changeCakeString('users', 'theme');
        $this->changeCakeString('versions', 'subject', false);
        $this->changeCakeString('versions', 'version', false);
    }

    public function down(): void
    {
    }

}
