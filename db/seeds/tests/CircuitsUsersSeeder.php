<?php

use Phinx\Seed\AbstractSeed;

class CircuitsUsersSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;

    public function getDependencies(): array
    {
        return [
            'UsersSeeder',
            'WkfCircuitsSeeder',
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('circuits_users');
        $table->truncate();

        $data = [
            // délégation pour le groupe Majorité seulement
            [
                'id' => 1,
                'circuit_id' => 1,
                'user_id' => 1,
            ],
            [
                'id' => 2,
                'circuit_id' => 2,
                'user_id' => 1,
            ],
        ];

        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());
    }
}
