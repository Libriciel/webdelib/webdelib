<?php




use Phinx\Seed\AbstractSeed;

class HistoriquesSeeder extends AbstractSeed
{
    public function getDependencies(): array
    {
        return [
            'DeliberationsSeeder',
            'UsersSeeder',
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('historiques');
        $table->truncate();

        $data = [];
        $this->addCreatedProject($data);

        $table->insert($data)
            ->saveData();
    }

    private function addCreatedProject(&$data)
    {
        $rows = $this->fetchAll('SELECT id,redacteur_id FROM deliberations');
        foreach ($rows as $row) {
            $user = $this->fetchRow("SELECT id, nom,prenom FROM users WHERE id={$row['redacteur_id']}");
            $data[] = [
                'delib_id' => $row['id'],
                'user_id' => $row['redacteur_id'],
                'commentaire' => sprintf('[%s %s] Le projet a été créé', $user['nom'], $user['prenom']),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ];
        }
    }
}
