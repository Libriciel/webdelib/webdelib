<?php




use Phinx\Seed\AbstractSeed;

class DeliberationsSeancesSeeder extends AbstractSeed
{
    public function getDependencies(): array
    {
        return [
            'TypeActesSeeder',
            'ThemesSeeder',
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('deliberations_seances');
        $table->truncate();

        $data = [
            [
                'deliberation_id' => 1,
                'seance_id' => 1,
                'position' => 1,
            ],
            [
                'deliberation_id' => 2,
                'seance_id' => 1,
                'position' => 2,
            ],
            [
                'deliberation_id' => 3,
                'seance_id' => 1,
                'position' => 3,
            ],
            [
                'deliberation_id' => 4,
                'seance_id' => 2,
                'position' => 1,
            ],
            [
                'deliberation_id' => 5,
                'seance_id' => 2,
                'position' => 2,
            ],
            [
                'deliberation_id' => 6,
                'seance_id' => 2,
                'position' => 3,
            ],
            [
                'deliberation_id' => 2,
                'seance_id' => 3,
                'position' => 1,
            ],
            [
                'deliberation_id' => 3,
                'seance_id' => 3,
                'position' => 2,
            ],
            [
                'deliberation_id' => 7,
                'seance_id' => 4,
                'position' => 1,
            ],
            [
                'deliberation_id' => 8,
                'seance_id' => 4,
                'position' => 2,
            ],
            [
                'deliberation_id' => 9,
                'seance_id' => 4,
                'position' => 3,
            ],
            [
                'deliberation_id' => 11,
                'seance_id' => 5,
                'position' => 1,
            ],
            [
                'deliberation_id' => 12,
                'seance_id' => 5,
                'position' => 2,
            ],
            [
                'deliberation_id' => 13,
                'seance_id' => 5,
                'position' => 3,
            ],
        ];


        $table->insert($data)
            ->saveData();
    }
}
