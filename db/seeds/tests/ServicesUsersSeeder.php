<?php




use Phinx\Seed\AbstractSeed;

class ServicesUsersSeeder extends AbstractSeed
{
    public function getDependencies(): array
    {
        return [
            'ServicesSeeder',
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('services_users');
        $table->truncate();

        $data = [
            [
                'user_id' => 1,
                'service_id' => 1,
            ],
            [
                'user_id' => 2,
                'service_id' => 3,
            ],
            [
                'user_id' => 3,
                'service_id' => 2,
            ],
            [
                'user_id' => 4,
                'service_id' => 4,
            ],
        ];

        $table->insert($data)
            ->saveData();
    }
}
