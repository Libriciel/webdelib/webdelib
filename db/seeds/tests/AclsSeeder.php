<?php

use Phinx\Seed\AbstractSeed;

class AclsSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;

    public function getDependencies(): array
    {
        return [
            'UsersSeeder',
            'ServicesSeeder',
            'TypeActesSeeder',
            'WkfCircuitsSeeder',
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $this->shellRun('maintenance resetAcl --tenant '.$this->environment.' -q');
        $table = $this->table('acos');
        $lft = $this->getMax();

        $data = [];
        $this->addAcosByModel($data, $lft, 'Typeacte', 'typeactes', 'name');
        $this->addAcosByModel($data, $lft, 'Typeseance', 'typeseances', 'libelle');
        $this->addAcosByModel($data, $lft, 'Service', 'services', 'name');
        $this->addAcosByModel($data, $lft, 'Circuit', 'wkf_circuits', 'nom');

        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());

        $table = $this->table('aros');
        $data = [
            [
                'id' => 1,
                'parent_id' => null,
                'model' => 'Profil',
                'foreign_key' => 1,
                'alias' => 'Administrateur et administratrice',
                'lft' => 1,
                'rght' => 4
            ],
            [
                'id' => 2,
                'parent_id' => null,
                'model' => 'Profil',
                'foreign_key' => 3,
                'alias' => 'Administrateur fonctionnel et administratrice fonctionnelle',
                'lft' => 6,
                'rght' => 7
            ],
            [
                'id' => 3,
                'parent_id' => null,
                'model' => 'Profil',
                'foreign_key' => 2,
                'alias' => 'Utilisateur et utilisatrice',
                'lft' => 10,
                'rght' => 15
            ],
            [
                'id' => 4,
                'parent_id' => 1,
                'model' => 'User',
                'foreign_key' => 1,
                'alias' => 'admin',
                'lft' => 2,
                'rght' => 3
            ],
            [
                'id' => 5,
                'parent_id' => 2,
                'model' => 'User',
                'foreign_key' => 3,
                'alias' => 'admin_f1',
                'lft' => 5,
                'rght' => 8
            ],
            [
                'id' => 6,
               'parent_id' => 3,
               'model' => 'User',
               'foreign_key' => 2,
               'alias' => 'user_redacteur1',
               'lft' => 11,
               'rght' => 12
            ],
            [
                'id' => 7,
                'parent_id' => 3,
                'model' => 'User',
                'foreign_key' => 4,
                'alias' => 'user_dg1',
                'lft' => 13,
                'rght' => 14
            ],
        ];
        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());

        $this->createProfilAdmin();
        $this->createProfilAdminFonctionnel();
        $this->createProfilUser();

        $this->propagationAclAdmins();
        $this->propagationAclAdminsFonctionnel();
        $this->propagationAclUsers();
    }

    private function addAcosByModel(&$data, &$lft, $model, $table, $name)
    {
        $rows = $this->fetchAll('SELECT id,' . $name . ' FROM ' . $table);
        foreach ($rows as $row) {
            $data[] = [
                'parent_id' => null,
                'model' => $model,
                'foreign_key' => $row['id'],
                'alias' => $row[$name],
                'lft' => $lft++,
                'rght' => $lft++
            ];
        }
    }

    private function getMax()
    {
        $max = $this->fetchRow('SELECT max(rght) from acos');
        return $max['max'];
    }

    private function createProfilAdmin()
    {
        $this->addArosAcosForUserId('controllers', 1);
    }
    private function createProfilAdminFonctionnel()
    {
        $this->addArosAcosForUserId('controllers', 3);
    }
    private function createProfilUser()
    {
        $this->addArosAcosForUserId('controllers', 2);
    }
    private function addArosAcosForUserId($model, $aroId, $acls = [])
    {
        $table = $this->table('aros_acos');

        if ($model === 'controllers') {
            $rows = $this->fetchAll('SELECT id,alias FROM acos WHERE model is null ORDER BY id ASC');
            $data = [];
            foreach ($rows as $row) {
                    $data[] = [
                        'aro_id' => $aroId,
                        'aco_id' => $row['id'],
                        '_create' => 1,
                        '_read' => 1,
                        '_update' => 1,
                        '_delete' => 1
                    ];
            }
            $table->insert($data)
            ->saveData();

            return;
        }

        $rows = $this->fetchAll(
            sprintf(
                'SELECT id,alias FROM acos WHERE model=\'%s\'',
                $model,
            )
        );
        $data = [];
        foreach ($rows as $row) {
            if (!empty($acls)) {
                if (in_array($row['alias'], array_keys($acls), true)) {
                    $data[] = [
                        'aro_id' => $aroId,
                        'aco_id' => $row['id'],
                        '_create' => $acls[$row['alias']]['create'],
                        '_read' => $acls[$row['alias']]['read'],
                        '_update' => $acls[$row['alias']]['update'],
                        '_delete' => $acls[$row['alias']]['delete']
                    ];
                }
            } else {
                $data[] = [
                    'aro_id' => $aroId,
                    'aco_id' => $row['id'],
                    '_create' => 1,
                    '_read' => 1,
                    '_update' => 1,
                    '_delete' => 1,
                ];
            }
        }
        $table->insert($data)
            ->saveData();
    }

    private function propagationAclAdmins()
    {
        $this->propagationUserAclControlersByProfilId(1);
        $aros = $this->fetchAll("SELECT id FROM aros WHERE model = 'User' AND parent_id = 1;");
        foreach ($aros as $aro) {
            $this->addArosAcosForUserId('Typeacte', $aro['id']);
            $this->addArosAcosForUserId('Typeseance', $aro['id']);
            $this->addArosAcosForUserId('Service', $aro['id']);
            $this->addArosAcosForUserId('Circuit', $aro['id']);
        }
    }

    private function propagationAclAdminsFonctionnel()
    {
        $this->propagationUserAclControlersByProfilId(2);
        $aros = $this->fetchAll("SELECT id FROM aros WHERE model = 'User' AND parent_id = 2;");
        foreach ($aros as $aro) {
            $this->addArosAcosForUserId('Typeacte', $aro['id']);
            $this->addArosAcosForUserId('Typeseance', $aro['id']);
            $this->addArosAcosForUserId('Service', $aro['id'], [
                'Urbanisme' => [
                    'create' => 1,
                    'read' => 1,
                    'update' => 1,
                    'delete' => 1
                ],
                'Voirie' => [
                    'create' => 1,
                    'read' => 1,
                    'update' => 1,
                    'delete' => 1
                ],
                ]);
            $this->addArosAcosForUserId('Circuit', $aro['id'], [
                'Circuit Finances' => [
                    'create' => 1,
                    'read' => 1,
                    'update' => 1,
                    'delete' => 1
                ]
            ]);
        }
    }

    private function propagationAclUsers()
    {
        $this->propagationUserAclControlersByProfilId(3);
        $aros = $this->fetchAll("SELECT id FROM aros WHERE model = 'User' AND parent_id = 3;");

        foreach ($aros as $aro) {
            $this->addArosAcosForUserId('Typeacte', $aro['id']);
            $this->addArosAcosForUserId('Typeseance', $aro['id'], [
                'Conseil Municipal' => [
                    'create' => 1,
                    'read' => 1,
                    'update' => 1,
                    'delete' => 0
                ],
                'Commission' => [
                    'create' => 1,
                    'read' => 1,
                    'update' => 1,
                    'delete' => 0
                ],
            ]);
        }
    }

    private function propagationUserAclControlersByProfilId($profilId)
    {
        // Search all users
        $aros = $this->fetchAll(
            "SELECT id,foreign_key FROM aros WHERE model='User' AND parent_id={$profilId}"
        );
        $table = $this->table('aros_acos');
        foreach ($aros as $aro) {
            $arosAcos = $this->fetchAll(
                "SELECT id,aco_id,_create,_read,_update,_delete,_manager FROM aros_acos WHERE aro_id={$profilId}"
            );
            $data = [];
            foreach ($arosAcos as $aroAco) {
                $data[] = [
                    'aro_id' => $aro['id'],
                    'aco_id' => $aroAco['aco_id'],
                    '_create' => $aroAco['_create'],
                    '_read' => $aroAco['_read'],
                    '_update' => $aroAco['_update'],
                    '_delete' => $aroAco['_delete'],
                    '_manager' => $aroAco['_manager'],
                ];
            }
            $table->insert($data)
                ->saveData();
        }
    }
}
