<?php

use Phinx\Seed\AbstractSeed;

class InfosupdefsProfilsSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;

    public function getDependencies(): array
    {
        return [
            'InfosupdefsSeeder',
            'ProfilsSeeder',
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('infosupdefs_profils');
        $table->truncate();

        $data = [
            [
                'id' => 1,
                'profil_id' => 1,
                'infosupdef_id' => 1,
            ],
            [
                'id' => 2,
                'profil_id' => 1,
                'infosupdef_id' => 2,
            ],
        ];

        $table->insert($data)
            ->saveData();

        $this->updateSequence('infosupdefs_profils');
    }
}
