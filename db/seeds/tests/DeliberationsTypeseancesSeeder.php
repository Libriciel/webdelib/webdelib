<?php




use Phinx\Seed\AbstractSeed;

class DeliberationsTypeseancesSeeder extends AbstractSeed
{
    public function getDependencies(): array
    {
        return [
            'TypeseancesSeeder',
            'DeliberationSeeder',
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('deliberations_typeseances');
        $table->truncate();

        $data = [
            [
                'deliberation_id' => 1,
                'typeseance_id' => 1,
            ],
            [
                'deliberation_id' => 2,
                'typeseance_id' => 1,
            ],
            [
                'deliberation_id' => 3,
                'typeseance_id' => 1,
            ],
            [
                'deliberation_id' => 4,
                'typeseance_id' => 1,
            ],
            [
                'deliberation_id' => 5,
                'typeseance_id' => 1,
            ],
            [
                'deliberation_id' => 6,
                'typeseance_id' => 1,
            ],
            [
                'deliberation_id' => 2,
                'typeseance_id' => 2,
            ],
            [
                'deliberation_id' => 3,
                'typeseance_id' => 2,
            ],
            [
                'deliberation_id' => 7,
                'typeseance_id' => 1,
            ],
            [
                'deliberation_id' => 8,
                'typeseance_id' => 1,
            ],
            [
                'deliberation_id' => 9,
                'typeseance_id' => 1,
            ],
            [
                'deliberation_id' => 11,
                'typeseance_id' => 1,
            ],
            [
                'deliberation_id' => 12,
                'typeseance_id' => 1,
            ],
            [
                'deliberation_id' => 13,
                'typeseance_id' => 1,
            ],
        ];


        $table->insert($data)
            ->saveData();
    }
}
