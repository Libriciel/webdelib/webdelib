<?php



use Phinx\Seed\AbstractSeed;

class InfosupsSeeder extends AbstractSeed
{
    public function getDependencies(): array
    {
        return [
            'InfosupdefsSeeder',
            'InfosuplistedefsSeeder',
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('infosups');
        $table->truncate();

        $data = [
            [
                'id' => 1,
                'model' => 'Deliberation',
                'foreign_key' => 1,
                'infosupdef_id' => 1,
                'text' => '',
                'date' => null,
                'file_name' => '',
                'file_size' => 0,
                'file_type' => '',
                'content' => '',
                'edition_data' => '',
                'edition_data_typemime' => '',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'geojson' => null
            ],
        ];

        $table->insert($data)
            ->saveData();
    }
}
