<?php



use Phinx\Seed\AbstractSeed;

class NaturesSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('natures');
        $table->truncate();

        $data = [
            [
                'id' => 1,
                'name' => 'Délibérations',
                'code' => 'DE',
                'default' => '99_DE',
            ],
            [
                'id' => 2,
                'name' => 'Arrêtés Réglementaires',
                'code' => 'AR',
                'default' => '99_AR',
            ],
            [
                'id' => 3,
                'name' => 'Arrêtés Individuels',
                'code' => 'AI',
                'default' => '99_AI',
            ],
            [
                'id' => 4,
                'name' => 'Contrats et conventions',
                'code' => 'CC',
                'default' => '99_DC',
            ],
            [
                'id' => 5,
                'name' => 'Autres',
                'code' => 'AU',
                'default' => '99_AU',
            ],
        ];

        $table->insert($data)
            ->saveData();
    }
}
