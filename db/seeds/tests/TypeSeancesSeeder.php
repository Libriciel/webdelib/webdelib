<?php




use Phinx\Seed\AbstractSeed;

class TypeSeancesSeeder extends AbstractSeed
{
    public function getDependencies(): array
    {
        return [
            'TypeActeursSeeder',
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('typeseances');
        $table->truncate();

        $data = [
            [
                'id' => 1,
                'libelle' => 'Conseil Municipal',
                'retard' => 5,
                'action' => 0,
                'compteur_id' => 1,
                'modele_projet_id' => 1,
                'modele_deliberation_id' => 3,
                'modele_convocation_id' => 4,
                'modele_ordredujour_id' => 5,
                'modele_pvsommaire_id' => 6,
                'modele_pvdetaille_id' => 7,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'color' => '#d62424',
                'modele_journal_seance_id' => 1,
                'parent_id' => null,
                'lft' => 1,
                'rght' => 4,
            ],
            [
                'id' => 2,
                'libelle' => 'Commission',
                'retard' => 5,
                'action' => 1,
                'compteur_id' => 1,
                'modele_projet_id' => 1,
                'modele_deliberation_id' => 1,
                'modele_convocation_id' => 1,
                'modele_ordredujour_id' => 1,
                'modele_pvsommaire_id' => 1,
                'modele_pvdetaille_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'color' => '#000000',
                'modele_journal_seance_id' => 1,
                'parent_id' => 1,
                'lft' => 2,
                'rght' => 3,
            ],
        ];

        $table->insert($data)
            ->saveData();
    }
}
