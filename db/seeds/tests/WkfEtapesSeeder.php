<?php



use Phinx\Seed\AbstractSeed;

class WkfEtapesSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('wkf_etapes');
        $table->truncate();

        $data = [
            [
                'id' => 1,
                'circuit_id' => 1,
                'nom' => 'Etape 1 - Utilisateur',
                'description' => '',
                'type' => 1,
                'ordre' => 1,
                'created_user_id' => 1,
                'modified_user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'soustype' => 1,
                'cpt_retard' => 30,
            ],
            [
                'id' => 2,
                'circuit_id' => 1,
                'nom' => 'Etape 2 - PARAPHEUR',
                'description' => '',
                'type' => 1,
                'ordre' => 2,
                'created_user_id' => 1,
                'modified_user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'soustype' => 1,
                'cpt_retard' => 30,
            ],
            [
                'id' => 3,
                'circuit_id' => 1,
                'nom' => 'Etape 3 - Utilisateur',
                'description' => '',
                'type' => 1,
                'ordre' => 3,
                'created_user_id' => 1,
                'modified_user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'soustype' => 1,
                'cpt_retard' => 30,
            ],
            [
                'id' => 4,
                'circuit_id' => 2,
                'nom' => 'Etape 1 - Utilisateur',
                'description' => '',
                'type' => 1,
                'ordre' => 1,
                'created_user_id' => 1,
                'modified_user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'soustype' => 1,
                'cpt_retard' => null,
            ],
            [
                'id' => 5,
                'circuit_id' => 2,
                'nom' => 'Etape 2 - Utilisateur',
                'description' => '',
                'type' => 1,
                'ordre' => 2,
                'created_user_id' => 1,
                'modified_user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'soustype' => 1,
                'cpt_retard' => null,
            ],
            [
                'id' => 6,
                'circuit_id' => 2,
                'nom' => 'Etape 3 - Utilisateur',
                'description' => '',
                'type' => 1,
                'ordre' => 3,
                'created_user_id' => 1,
                'modified_user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'soustype' => 1,
                'cpt_retard' => null,
            ]
        ];


        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());
    }
}
