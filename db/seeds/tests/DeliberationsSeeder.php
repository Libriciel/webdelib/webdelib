<?php

use Phinx\Seed\AbstractSeed;

class DeliberationsSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;

    public function getDependencies(): array
    {
        return [
            'TypeActesSeeder',
            'ThemesSeeder',
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('deliberations');
        $table->truncate();

        $data = [
            [
                'id' => 1,
                'typeacte_id' => 1,
                'circuit_id' => 1,
                'theme_id' => 1,
                'service_id' => 1,
                'redacteur_id' => 1,
                'rapporteur_id' => 1,
                'objet' => 'Délibération 1',
                'objet_delib' => 'Délibération 1',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 2,
                'typeacte_id' => 1,
                'circuit_id' => 1,
                'theme_id' => 1,
                'service_id' => 1,
                'redacteur_id' => 1,
                'rapporteur_id' => 1,
                'objet' => 'Délibération 2',
                'objet_delib' => 'Délibération 2',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 3,
                'typeacte_id' => 1,
                'circuit_id' => 1,
                'theme_id' => 1,
                'service_id' => 1,
                'redacteur_id' => 1,
                'rapporteur_id' => 1,
                'objet' => 'Délibération 3',
                'objet_delib' => 'Délibération 3',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 4,
                'typeacte_id' => 1,
                'circuit_id' => 1,
                'theme_id' => 1,
                'service_id' => 1,
                'redacteur_id' => 1,
                'rapporteur_id' => 1,
                'objet' => 'Délibération test idelibre position 1',
                'objet_delib' => 'Délibération test idelibre position 1',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 5,
                'typeacte_id' => 1,
                'circuit_id' => 1,
                'theme_id' => 1,
                'service_id' => 1,
                'redacteur_id' => 1,
                'rapporteur_id' => 1,
                'objet' => 'Délibération test idelibre position 2',
                'objet_delib' => 'Délibération test idelibre position 2',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 6,
                'typeacte_id' => 1,
                'circuit_id' => 1,
                'theme_id' => 1,
                'service_id' => 1,
                'redacteur_id' => 1,
                'rapporteur_id' => 1,
                'objet' => 'Délibération test idelibre position 3',
                'objet_delib' => 'Délibération test idelibre position 3',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 7,
                'typeacte_id' => 1,
                'circuit_id' => 1,
                'theme_id' => 1,
                'service_id' => 1,
                'redacteur_id' => 1,
                'rapporteur_id' => 1,
                'etat' => 3,
                'objet' => 'Délibération séance passée position 1',
                'objet_delib' => 'Délibération séance passée position 1',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 8,
                'typeacte_id' => 1,
                'circuit_id' => 1,
                'theme_id' => 1,
                'service_id' => 1,
                'redacteur_id' => 1,
                'rapporteur_id' => 1,
                'num_pref' => '1.1',
                'num_delib' => 'DEL02',
                'date_acte' => date('Y-m-d'),
                'etat' => 3,
                'objet' => 'Délibération séance passée position 2',
                'objet_delib' => 'Délibération séancee passé position 2',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 9,
                'typeacte_id' => 1,
                'circuit_id' => 1,
                'theme_id' => 1,
                'service_id' => 1,
                'redacteur_id' => 1,
                'rapporteur_id' => 1,
                'etat' => 3,
                'objet' => 'Délibération séance passée position 3',
                'objet_delib' => 'Délibération séance passée position 3',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 10,
                'typeacte_id' => 2,
                'circuit_id' => 1,
                'theme_id' => 1,
                'service_id' => 1,
                'redacteur_id' => 1,
                'pastell_id' => 'PASTELLID',
                'num_pref' => '1.1',
                'num_delib' => 'ARRETE10',
                'publier_etat' => 1,
                'publier_date' => date('Y-m-d', strtotime("-1 day")),
                'etat' => 5,
                'tdt_ar_date' => date('Y-m-d', strtotime("-1 day")),
                'tdt_ar' => $this->arXml(),
                'typologiepiece_code' => '99_DE',
                'signee' => true,
                'tdt_dateAR' => date('l j F Y', strtotime("-1 day")),
                'tdt_status' => 1,
                'date_acte' => date('Y-m-d', strtotime("-1 day")),
                'objet' => 'Arrêté réglementaire',
                'objet_delib' => 'Arrêté réglementaire',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 11,
                'typeacte_id' => 1,
                'circuit_id' => 1,
                'theme_id' => 1,
                'service_id' => 1,
                'redacteur_id' => 1,
                'num_pref' => '1.1',
                'num_delib' => 'DEL11',
                'signee' => true,
                'publier_etat' => 2,
                'publier_date' => date('Y-m-d', strtotime("-1 day")),
                'etat' => 5,
                'tdt_ar_date' => date('Y-m-d', strtotime("-1 day")),
                'tdt_dateAR' => date('l j F Y', strtotime("-1 day")),
                'tdt_status' => 1,
                'date_acte' => date('Y-m-d', strtotime("-1 day")),
                'objet' => 'Délibération 11 séance 5 position 1',
                'objet_delib' => 'Délibération 11 séance 5 position 1',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 12,
                'typeacte_id' => 1,
                'circuit_id' => 1,
                'theme_id' => 1,
                'service_id' => 1,
                'redacteur_id' => 1,
                'num_pref' => '1.1',
                'num_delib' => 'DEL12',
                'signee' => true,
//                'delib_pdf' => file_get_contents(PHINX_APP . 'Test/Data/ActFixture.pdf'),
                'publier_etat' => 1,
                'publier_date' => date('Y-m-d', strtotime("-1 day")),
                'etat' => 5,
                'tdt_ar_date' => date('Y-m-d', strtotime("-1 day")),
                'tdt_dateAR' => date('l j F Y', strtotime("-1 day")),
                'tdt_status' => 1,
                'date_acte' => date('Y-m-d', strtotime("-1 day")),
                'objet' => 'Délibération 12 séance 5 position 2',
                'objet_delib' => 'Délibération 12 séance 5 position 2',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 13,
                'typeacte_id' => 1,
                'circuit_id' => 1,
                'theme_id' => 1,
                'service_id' => 1,
                'redacteur_id' => 1,
                'num_pref' => '1.1',
                'num_delib' => 'DEL13',
                'signee' => true,
//                'delib_pdf' => file_get_contents(PHINX_APP . 'Test/Data/ActFixture.pdf'),
                'publier_etat' => 1,
                'publier_date' => date('Y-m-d', strtotime("-1 day")),
                'etat' => 5,
                'tdt_ar_date' => date('Y-m-d', strtotime("-1 day")),
                'tdt_dateAR' => date('l j F Y', strtotime("-1 day")),
                'tdt_status' => 1,
                'typologiepiece_code' => '99_DE',
                'date_acte' => date('Y-m-d', strtotime("-1 day")),
                'objet' => 'Délibération 13 séance 5 position 3',
                'objet_delib' => 'Délibération 13 séance 5 position 3',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 14,
                'typeacte_id' => 2,
                'circuit_id' => 1,
                'theme_id' => 1,
                'service_id' => 1,
                'redacteur_id' => 1,
                'num_pref' => '1.1',
                'num_delib' => 'AR14',
                'etat' => 2,
                'objet' => 'Arrêté réglementaire AR14 à signer',
                'objet_delib' => 'Arrêté réglementaire AR14 à signer',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 15,
                'typeacte_id' => 2,
                'circuit_id' => 1,
                'theme_id' => 1,
                'service_id' => 1,
                'redacteur_id' => 1,
                'num_pref' => '1.1',
                'num_delib' => 'AR15',
                'etat' => 2,
                'parapheur_etat' => 1,
                'objet' => 'Arrêté réglementaire AR15 en cours parapheur direct',
                'objet_delib' => 'Arrêté réglementaire AR15 en cours parapheur direct',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 16,
                'typeacte_id' => 2,
                'circuit_id' => 1,
                'theme_id' => 1,
                'service_id' => 1,
                'redacteur_id' => 2,
                'num_pref' => '1.1',
                'num_delib' => 'ARRETE16',
                'signee' => true,
//                'delib_pdf' => file_get_contents(PHINX_APP . 'Test/Data/ActFixture.pdf'),
                'publier_etat' => 2,
                'publier_date' => date('Y-m-d', strtotime("-1 day")),
                'tdt_ar_date' => date('Y-m-d', strtotime("-1 day")),
                'tdt_dateAR' => date('l j F Y', strtotime("-1 day")),
                'tdt_status' => 1,
                'tdt_ar' => $this->arXml(),
                'date_acte' => date('Y-m-d', strtotime("-1 day")),
                'typologiepiece_code' => '99_DE',
                'etat' => 11,
                'parapheur_etat' => 2,
                'objet' => 'Arrêté réglementaire AR16 en attente d\'archivage',
                'objet_delib' => 'Arrêté réglementaire AR16 en attente d\'archivage',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
        ];

        $table->insert($data)
            ->saveData();

        $this->updateSequence('deliberations');

        $this->updateActs([10,11,12]);
        $this->updateTextes($data);
    }


    private function updateTextes(&$data)
    {
        foreach (['texte_projet', 'texte_synthese', 'deliberation'] as $texte) {
            foreach ($data as $row) {
                    $this->updateFile(
                        'deliberations',
                        $texte,
                        $row['id'],
                        PHINX_APP . 'Test/Data/OdtVide.odt'
                    );

                $fileData = [
                    'id' => $row['id'],
                    "{$texte}_name" => "gabarit_{$texte}_name.odt",
                    "{$texte}_type" => 'application/vnd.oasis.opendocument.text'
                ];
                $sql = "UPDATE deliberations SET {$texte}_name = :{$texte}_name, {$texte}_type = :{$texte}_type "
                    ."WHERE id = :id";
                $this->execute($sql, $fileData);
            }
        }
    }

    private function updateActs($acts)
    {
        foreach ($acts as $id) {
            $this->updateFile(
                'deliberations',
                'delib_pdf',
                $id,
                PHINX_APP . 'Test/Data/ActFixture.pdf'
            );
        }
    }


    private function updateTexte($id)
    {
        $pdfFilePath = PHINX_APP . 'Test/Data/ActFixture.pdf';

        try {
            // Lire le contenu binaire du fichier PDF
            $pdfContent = file_get_contents($pdfFilePath);

            if ($pdfContent === false) {
                throw new Exception("Impossible de lire le fichier PDF.");
            }
            // Échapper correctement les données binaires pour PostgreSQL
            $escapedContent = pg_escape_bytea($pdfContent);

            $this->execute("UPDATE deliberations SET delib_pdf = '{$escapedContent}' WHERE id = '$id'");
        } catch (Exception $e) {
            echo "Erreur lors du traitement du fichier PDF : " . $e->getMessage();
        }
    }



    // phpcs:disabled
    private function arXml()
    {
        return '<?xml version="1.0" encoding="windows-1252"?>
                <actes:ARActe xmlns:actes="http://www.interieur.gouv.fr/ACTES#v1.1-20040216" xmlns:insee="http://xml.insee.fr/schema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.interieur.gouv.fr/ACTES#v1.1-20040216 actesv1_1.xsd" actes:DateReception="2013-06-23" actes:IDActe="069-123456789-20130601-20130623C-AI">
                  <actes:Acte actes:Date="2013-06-01" actes:NumeroInterne="20130623C" actes:CodeNatureActe="3" xsi:schemaLocation="http://www.interieur.gouv.fr/ACTES#v1.1-20040216 actesv1_1.xsd">
                    <actes:CodeMatiere1 actes:CodeMatiere="3"/>
                    <actes:CodeMatiere2 actes:CodeMatiere="1"/>
                    <actes:Objet>TESTC</actes:Objet>
                    <actes:ClassificationDateVersion>2004-04-01</actes:ClassificationDateVersion>
                    <actes:Document>
                      <actes:NomFichier>069-123456789-20130601-20130623C-AI-1-1_1.pdf</actes:NomFichier>
                    </actes:Document>
                    <actes:Annexes actes:Nombre="0"/>
                  </actes:Acte>
                  <actes:ClassificationDateVersionEnCours>2008-12-31</actes:ClassificationDateVersionEnCours>
                  <actes:ActeRecu actes:Date="2013-06-01" actes:NumeroInterne="20130623C" actes:CodeNatureActe="3">
                    <actes:CodeMatiere1 actes:CodeMatiere="3"/>
                    <actes:CodeMatiere2 actes:CodeMatiere="1"/>
                    <actes:Objet>TESTC</actes:Objet>
                    <actes:ClassificationDateVersion>2004-04-01</actes:ClassificationDateVersion>
                    <actes:Document>
                      <actes:NomFichier>069-123456789-20130601-20130623C-AI-1-1_1.pdf</actes:NomFichier>
                    </actes:Document>
                    <actes:Annexes actes:Nombre="0"/>
                  </actes:ActeRecu>
                  <actes:ClassificationDateVersionEnCours/>
                  <actes:ActeRecu actes:Date="2013-06-01" actes:NumeroInterne="20130623C" actes:CodeNatureActe="3">
                    <actes:CodeMatiere1 actes:CodeMatiere="3"/>
                    <actes:CodeMatiere2 actes:CodeMatiere="1"/>
                    <actes:Objet>TESTC</actes:Objet>
                    <actes:ClassificationDateVersion>2004-04-01</actes:ClassificationDateVersion>
                    <actes:Document>
                      <actes:NomFichier>069-123456789-20130601-20130623C-AI-1-1_1.pdf</actes:NomFichier>
                    </actes:Document>
                    <actes:Annexes actes:Nombre="0"/>
                  </actes:ActeRecu>
                  <actes:ClassificationDateVersionEnCours/>
                </actes:ARActe>';
    }
}
