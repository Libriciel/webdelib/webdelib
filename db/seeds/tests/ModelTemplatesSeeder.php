<?php

use Phinx\Seed\AbstractSeed;

class ModelTemplatesSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('modeltemplates');
        $table->truncate();

        $data = [
            [
                'id' => 1,
                'modeltype_id' => 2,
                'name' => 'Projet',
                'filename' => 'Projet.odt',
                'content' => '',
                'file_output_format' => '',
                'force' => false,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 2,
                'modeltype_id' => 2,
                'name' => 'Arrêté',
                'filename' => 'Arrêté.odt',
                'content' => '',
                'file_output_format' => '',
                'force' => false,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 3,
                'modeltype_id' => 3,
                'name' => 'Délibération',
                'filename' => 'Délibération.odt',
                'content' => '',
                'file_output_format' => '',
                'force' => false,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 4,
                'modeltype_id' => 4,
                'name' => 'Convocation CM',
                'filename' => 'Convocation_CM.odt',
                'content' => '',
                'file_output_format' => '',
                'force' => false,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 5,
                'modeltype_id' => 5,
                'name' => 'ODJ',
                'filename' => 'ODJ.odt',
                'content' => '',
                'file_output_format' => '',
                'force' => false,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 6,
                'modeltype_id' => 6,
                'name' => 'Compte rendu sommaire',
                'filename' => 'Compte_rendu_sommaire.odt',
                'content' => '',
                'file_output_format' => '',
                'force' => false,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 7,
                'modeltype_id' => 7,
                'name' => 'Compte rendu détaillé',
                'filename' => 'Compte_rendu_détaillé.odt',
                'content' => '',
                'file_output_format' => '',
                'force' => false,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 8,
                'modeltype_id' => 8,
                'name' => 'Recherche',
                'filename' => 'Recherche.odt',
                'content' => '',
                'file_output_format' => '',
                'force' => false,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 9,
                'modeltype_id' => 9,
                'name' => 'Multi-séance',
                'filename' => 'Multi-séance.odt',
                'content' => '',
                'file_output_format' => '',
                'force' => false,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 10,
                'modeltype_id' => 10,
                'name' => 'Bordereau',
                'filename' => 'Bordereau.odt',
                'content' => '',
                'file_output_format' => '',
                'force' => false,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 11,
                'modeltype_id' => 11,
                'name' => 'Journal de séance',
                'filename' => 'modele_journal_de_seance.odt',
                'content' => '',
                'file_output_format' => '',
                'force' => false,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
        ];

        $table->insert($data)
            ->saveData();

        $this->updateModels($data);
    }

    private function updateModels($models)
    {
        foreach ($models as $model) {
            $this->updateFile(
                'modeltemplates',
                'content',
                $model['id'],
                PHINX_APP . 'Test/Data/VariableTest.odt'
            );

            $fileData = [
                'id' => $model['id'],
                "filesize" => filesize(PHINX_APP . 'Test/Data/VariableTest.odt')
            ];
            $sql = "UPDATE modeltemplates SET filesize = :filesize WHERE id = :id";
            $this->execute($sql, $fileData);
        }
    }
}
