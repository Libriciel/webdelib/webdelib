<?php




use Phinx\Seed\AbstractSeed;

class TypeActeursSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('typeacteurs');
        $table->truncate();

        $data = [
            [
                'id' => 1,
                'nom' => 'Groupe majorité',
                'commentaire' => 'Groupe majorité',
                'elu' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'actif' => true,
            ],
            [
                'id' => 2,
                'nom' => 'Groupe Opposition',
                'commentaire' => 'Groupe Opposition',
                'elu' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'actif' => true,
            ],
            [
                'id' => 3,
                'nom' => 'Groupe sans étiquette',
                'commentaire' => 'Groupe sans étiquette',
                'elu' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'actif' => true,
            ],
            [
                'id' => 4,
                'nom' => 'Journaliste',
                'commentaire' => '',
                'elu' => false,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'actif' => true,
            ],
            [
                'id' => 5,
                'nom' => 'Suppléants',
                'commentaire' => '',
                'elu' => false,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'actif' => true,
            ],
            [
                'id' => 6,
                'nom' => 'Invités',
                'commentaire' => '',
                'elu' => false,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'actif' => false,
            ],
        ];

        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());
    }
}
