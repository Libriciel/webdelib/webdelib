<?php



use Phinx\Seed\AbstractSeed;

class WkfCircuitsSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('wkf_circuits');
        $table->truncate();

        $data = [
            [
                'id' => '1',
                'nom' => 'Circuit iparapheur',
                'description' => 'Visa du Maire dans iparapheur puis retour WD',
                'actif' => true,
                'defaut' => true,
                'created_user_id' => 1,
                'modified_user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'nom' => 'Circuit Finances',
                'description' => '',
                'actif' => true,
                'defaut' => false,
                'created_user_id' => 1,
                'modified_user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'nom' => 'Circuit DSI',
                'description' => '',
                'actif' => true,
                'defaut' => false,
                'created_user_id' => 1,
                'modified_user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ]
        ];


        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());
    }
}
