<?php




use Phinx\Seed\AbstractSeed;

class TypeSeancesTypesActesSeeder extends AbstractSeed
{
    public function getDependencies(): array
    {
        return [
            'TypeActesSeeder',
            'TypeSeancesSeeder',
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('typeseances_typeactes');
        $table->truncate();

        $data = [
            [
                'typeseance_id' => 1,
                'typeacte_id' => 1,
            ],
            [
                'typeseance_id' => 2,
                'typeacte_id' => 1,
            ],
        ];

        $table->insert($data)
            ->saveData();
    }
}
