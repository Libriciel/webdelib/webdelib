<?php



use Phinx\Seed\AbstractSeed;

class WkfCompositionsSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('wkf_compositions');
        $table->truncate();

        $data = [
            [
                'id' => 1,
                'etape_id' => 1,
                'type_validation' => 'V',
                'trigger_id' => 1,
                'created_user_id' => 1,
                'modified_user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'soustype' => 1,
                'type_composition' => 'USER',
            ],
            [
                'id' => 2,
                'etape_id' => 2,
                'type_validation' => 'V',
                'trigger_id' => 1,
                'created_user_id' => 1,
                'modified_user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'soustype' => 1,
                'type_composition' => 'PASTELL',
            ],
            [
                'id' => 3,
                'etape_id' => 3,
                'type_validation' => 'V',
                'trigger_id' => 1,
                'created_user_id' => 1,
                'modified_user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'soustype' => 1,
                'type_composition' => 'USER',
            ],
            [
            'id' => 4,
            'etape_id' => 4,
            'type_validation' => 'V',
            'trigger_id' => 1,
            'created_user_id' => 1,
            'modified_user_id' => 1,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
            'soustype' => 1,
            'type_composition' => 'USER',
            ],
            [
                'id' => 5,
                'etape_id' => 5,
                'type_validation' => 'V',
                'trigger_id' => 2,
                'created_user_id' => 1,
                'modified_user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'soustype' => 1,
                'type_composition' => 'USER',
            ],
            [
                'id' => 6,
                'etape_id' => 6,
                'type_validation' => 'V',
                'trigger_id' => 1,
                'created_user_id' => 1,
                'modified_user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'soustype' => 1,
                'type_composition' => 'USER',
            ]
        ];


        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());
    }
}
