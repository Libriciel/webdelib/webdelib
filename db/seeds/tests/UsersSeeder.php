<?php

use Phinx\Seed\AbstractSeed;

class UsersSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $this->execute('TRUNCATE TABLE users CASCADE');
        $table = $this->table('users');

        $data = [
            [
                'id' => 1,
                'profil_id' => 1,
                'statut' => 1,
                'username' => 'admin',
                'circuit_defaut_id' => 1,
                'password' => md5(env('APP_ADMIN_PASSWORD')),
                'nom' => 'admin',
                'prenom' => 'admin',
                'email' => 'admin@webdelib.invalid',
                'telfixe' => '',
                'telmobile' => '',
                'date_naissance' => null,
                'accept_notif' => false,
                'mail_refus' => false,
                'mail_traitement' => false,
                'mail_insertion' => false,
                'position' => null,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'mail_modif_projet_cree' => false,
                'mail_modif_projet_valide' => false,
                'mail_retard_validation' => false,
                'theme' => null,
                'active' => true,
                'mail_error_send_tdt_auto' => false,
                'mail_projet_valide' => false,
                'mail_projet_valide_valideur' => false,
                'mail_majtdtcourrier' => false,
                'service_defaut_id' => null,
                'preference_first_login_active' => true,
                'mail_error_sae' => false,
            ],
            [
                'id' => 2,
                'profil_id' => 2,
                'statut' => 1,
                'username' => 'user_redacteur1',
                'circuit_defaut_id' => 1,
                'password' => md5(env('APP_ADMIN_PASSWORD')),
                'nom' => 'Redac1Nom',
                'prenom' => 'Redac1Prenom',
                'email' => 'user_redacteur1@webdelib.invalid',
                'telfixe' => '',
                'telmobile' => '',
                'date_naissance' => null,
                'accept_notif' => false,
                'mail_refus' => false,
                'mail_traitement' => false,
                'mail_insertion' => false,
                'position' => null,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'mail_modif_projet_cree' => false,
                'mail_modif_projet_valide' => false,
                'mail_retard_validation' => false,
                'theme' => null,
                'active' => true,
                'mail_error_send_tdt_auto' => false,
                'mail_projet_valide' => false,
                'mail_projet_valide_valideur' => false,
                'mail_majtdtcourrier' => false,
                'service_defaut_id' => null,
                'preference_first_login_active' => true,
                'mail_error_sae' => false,
            ],
            [
                'id' => 3,
                'profil_id' => 3,
                'statut' => 1,
                'username' => 'admin_f1',
                'circuit_defaut_id' => 1,
                'password' => md5(env('APP_ADMIN_PASSWORD')),
                'nom' => 'AdminF1Nom',
                'prenom' => 'AdminF1Prenom',
                'email' => 'admin_f1@webdelib.invalid',
                'telfixe' => '',
                'telmobile' => '',
                'date_naissance' => null,
                'accept_notif' => false,
                'mail_refus' => false,
                'mail_traitement' => false,
                'mail_insertion' => false,
                'position' => null,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'mail_modif_projet_cree' => false,
                'mail_modif_projet_valide' => false,
                'mail_retard_validation' => false,
                'theme' => null,
                'active' => true,
                'mail_error_send_tdt_auto' => false,
                'mail_projet_valide' => false,
                'mail_projet_valide_valideur' => false,
                'mail_majtdtcourrier' => false,
                'service_defaut_id' => null,
                'preference_first_login_active' => true,
                'mail_error_sae' => false,
            ],
            [
                'id' => 4,
                'profil_id' => 2,
                'statut' => 1,
                'username' => 'user_dg1',
                'circuit_defaut_id' => 0,
                'password' => md5(env('APP_ADMIN_PASSWORD')),
                'nom' => 'UserDG1Nom',
                'prenom' => 'UserDG1Prenom',
                'email' => 'user_dg1@webdelib.invalid',
                'telfixe' => '',
                'telmobile' => '',
                'date_naissance' => null,
                'accept_notif' => false,
                'mail_refus' => false,
                'mail_traitement' => false,
                'mail_insertion' => false,
                'position' => null,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'mail_modif_projet_cree' => false,
                'mail_modif_projet_valide' => false,
                'mail_retard_validation' => false,
                'theme' => null,
                'active' => true,
                'mail_error_send_tdt_auto' => false,
                'mail_projet_valide' => false,
                'mail_projet_valide_valideur' => false,
                'mail_majtdtcourrier' => false,
                'service_defaut_id' => null,
                'preference_first_login_active' => true,
                'mail_error_sae' => false,
            ],
        ];

        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());
    }
}
