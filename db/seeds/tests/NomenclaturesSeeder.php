<?php

use Phinx\Seed\AbstractSeed;

class NomenclaturesSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('nomenclatures');
        $table->truncate();

        $data = [
            [
                'id' => 1,
                'parent_id' => null,
                'name' => '1',
                'libelle' => '1 Commande Publique',
                'lft' => 1,
                'rght' => 4,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 2,
                'parent_id' => 1,
                'name' => '1.1',
                'libelle' => '1.1 Marchés publics',
                'lft' => 2,
                'rght' => 3,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
        ];

        $table->insert($data)
            ->saveData();
    }
}
