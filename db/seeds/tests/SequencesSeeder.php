<?php




use Phinx\Seed\AbstractSeed;

class SequencesSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $this->execute('TRUNCATE TABLE sequences CASCADE');
        $table = $this->table('sequences');

        $data = [
            [
                'id' => 1,
                'nom' => 'Séquence 1',
                'commentaire' => 'Séquence avec date de validité sur l\'année en cours',
                'num_sequence' => 100,
                'debut_validite' => date('Y-01-01'),
                'fin_validite' => date('Y-12-31'),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 2,
                'nom' => 'Séquence 2',
                'commentaire' => 'Séquence avec date de validité sur le mois en cours',
                'num_sequence' => 100,
                'debut_validite' => date('Y-m-01'),
                'fin_validite' => date('Y-m-d', strtotime("last day of this month")),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 3,
                'nom' => 'Séquence 3',
                'commentaire' => 'Séquence avec date de validité sur le jour en cours',
                'num_sequence' => 100,
                'debut_validite' => date('Y-m-d'),
                'fin_validite' => date('Y-m-d'),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 4,
                'nom' => 'Séquence 4',
                'commentaire' => 'Séquence avec date de validité sur l\'année passé',
                'num_sequence' => 100,
                'debut_validite' => date('Y-01-01', strtotime("last year")),
                'fin_validite' => date('Y-12-31', strtotime("last year")),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 5,
                'nom' => 'Séquence 5',
                'commentaire' => 'Séquence avec date de validité sur le mois passé',
                'num_sequence' => 100,
                'debut_validite' => date('Y-m-01', strtotime("-1 month")),
                'fin_validite' => date('Y-m-d', strtotime("last day of last month")),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 6,
                'nom' => 'Séquence 6',
                'commentaire' => 'Séquence avec date de validité sur le jour passé',
                'num_sequence' => 100,
                'debut_validite' => date('Y-m-d', strtotime("-1 day")),
                'fin_validite' => date('Y-m-d', strtotime("-1 day")),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 7,
                'nom' => 'Séquence 7',
                'commentaire' => 'Séquence déjà réinitialisée avec date de validité sur l\'année en cours',
                'num_sequence' => 0,
                'debut_validite' => date('Y-01-01'),
                'fin_validite' => date('Y-12-31'),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 8,
                'nom' => 'Séquence 8',
                'commentaire' => 'Séquence déjà réinitialisée avec date de validité sur le mois en cours',
                'num_sequence' => 0,
                'debut_validite' => date('Y-m-01'),
                'fin_validite' => date('Y-m-d', strtotime("last day of this month")),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 9,
                'nom' => 'Séquence 9',
                'commentaire' => 'Séquence déjà réinitialisée avec date de validité sur le jour en cours',
                'num_sequence' => 0,
                'debut_validite' => date('Y-m-d'),
                'fin_validite' => date('Y-m-d'),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 10,
                'nom' => 'Séquence 10',
                'commentaire' => 'Séquence sans réinitialisation',
                'num_sequence' => 1000,
                'debut_validite' => null,
                'fin_validite' => null,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ]
        ];

        $table->insert($data)
            ->saveData();
    }
}
