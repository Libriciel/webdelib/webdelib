<?php




use Phinx\Seed\AbstractSeed;

class TypeActesSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('typeactes');
        $table->truncate();

        $data = [
            [
                'id' => 1,
                'name' => 'Délibération',
                'modele_projet_id' => 1,
                'modele_final_id' => 1,
                'nature_id' => 1,
                'compteur_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'gabarit_projet' => file_get_contents(PHINX_APP . 'Config/OdtVide.odt'),
                'gabarit_synthese' => file_get_contents(PHINX_APP . 'Config/OdtVide.odt'),
                'gabarit_acte' => file_get_contents(PHINX_APP . 'Config/OdtVide.odt'),
                'teletransmettre' => true,
                'gabarit_acte_name' => 'gabarit_acte_name.odt',
                'gabarit_projet_name' => 'gabarit_projet_name.odt',
                'gabarit_synthese_name' => 'gabarit_synthese_name.odt',
                'gabarit_projet_active' => 1,
                'gabarit_synthese_active' => 1,
                'gabarit_acte_active' => 1,
                'modele_bordereau_projet_id' => 1,
                'deliberant' => true,
                'publier' => true,
                'verser_sae' => true,
            ],
            [
                'id' => 2,
                'name' => 'Arrêté réglementaire',
                'modele_projet_id' => 1,
                'modele_final_id' => 1,
                'nature_id' => 1,
                'compteur_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'gabarit_projet' => file_get_contents(PHINX_APP . 'Config/OdtVide.odt'),
                'gabarit_synthese' => file_get_contents(PHINX_APP . 'Config/OdtVide.odt'),
                'gabarit_acte' => file_get_contents(PHINX_APP . 'Config/OdtVide.odt'),
                'teletransmettre' => true,
                'gabarit_acte_name' => 'gabarit_acte_name.odt',
                'gabarit_projet_name' => 'gabarit_projet_name.odt',
                'gabarit_synthese_name' => 'gabarit_synthese_name.odt',
                'gabarit_projet_active' => 1,
                'gabarit_synthese_active' => 1,
                'gabarit_acte_active' => 1,
                'modele_bordereau_projet_id' => 1,
                'deliberant' => false,
                'publier' => true,
                'verser_sae' => true,
            ],
        ];

        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());
    }
}
