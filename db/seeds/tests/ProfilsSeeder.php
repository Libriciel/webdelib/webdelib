<?php




use Phinx\Seed\AbstractSeed;

class ProfilsSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('profils');
        $table->truncate();

        $data = [
            [
                'id' => 1,
                'role_id' => 2,
                'name' => 'Administrateur et administratrice',
                'actif' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 2,
                'role_id' => 1,
                'name' => 'Utilisateur et utilisatrice',
                'actif' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 3,
                'role_id' => 3,
                'name' => 'Administrateur fonctionnel et administratrice fonctionnelle',
                'actif' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
        ];

        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());
    }
}
