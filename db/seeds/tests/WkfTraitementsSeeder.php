<?php

use Phinx\Seed\AbstractSeed;

class WkfTraitementsSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('wkf_traitements');
        $table->truncate();

        $data = [
            [
                'id' => 1,
                'circuit_id' => 1,
                'target_id' => 1,
                'numero_traitement' => 1,
                'treated_orig' => 0,
                'created_user_id' => 1,
                'modified_user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'treated' => false,
            ],
        ];


        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());
    }
}
