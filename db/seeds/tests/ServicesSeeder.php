<?php

use Phinx\Seed\AbstractSeed;

class ServicesSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('services');
        $table->truncate();

        $data = [
            [
                'id' => 1,
                'parent_id' => null,
                'order' => 'A',
                'name' => 'Informatique',
                'circuit_defaut_id' => 1,
                'actif' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'lft' => 1,
                'rght' => 2,
            ],
            [
                'id' => 2,
                'parent_id' => null,
                'order' => 'BB',
                'name' => 'Urbanisme',
                'circuit_defaut_id' => 1,
                'actif' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'lft' => 5,
                'rght' => 8,
            ],
            [
                'id' => 3,
                'parent_id' => 2,
                'order' => 'B',
                'name' => 'Voirie',
                'circuit_defaut_id' => 1,
                'actif' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'lft' => 6,
                'rght' => 7,
            ],
            [
                'id' => 4,
                'parent_id' => null,
                'order' => 'AA',
                'name' => 'Direction Générale',
                'circuit_defaut_id' => 0,
                'actif' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'lft' => 3,
                'rght' => 4,
            ],
            [
                'id' => 5,
                'parent_id' => null,
                'order' => 'AA',
                'name' => 'Pôle technique',
                'circuit_defaut_id' => 1,
                'actif' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'lft' => 9,
                'rght' => 10,
            ],
        ];

        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());
    }
}
