<?php



use Phinx\Seed\AbstractSeed;

class DeliberationsUsersSeeder extends AbstractSeed
{
    public function getDependencies(): array
    {
        return [
            'UsersSeeder',
            'DeliberationsSeeder',
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('deliberations_users');
        $table->truncate();

        $data = [
            [
                'deliberation_id' => 1,
                'user_id' => 1,
            ],
            [
                'deliberation_id' => 1,
                'user_id' => 2,
            ],
            [
                'deliberation_id' => 2,
                'user_id' => 2,
            ],
            [
                'deliberation_id' => 2,
                'user_id' => 3,
            ],
            [
                'deliberation_id' => 3,
                'user_id' => 3,
            ],
            [
                'deliberation_id' => 4,
                'user_id' => 1,
            ],
            [
                'deliberation_id' => 5,
                'user_id' => 1,
            ],
            [
                'deliberation_id' => 6,
                'user_id' => 1,
            ],
            [
                'deliberation_id' => 7,
                'user_id' => 1,
            ],
            [
                'deliberation_id' => 8,
                'user_id' => 1,
            ],
            [
                'deliberation_id' => 9,
                'user_id' => 1,
            ],
            [
                'deliberation_id' => 10,
                'user_id' => 1,
            ],
            [
                'deliberation_id' => 11,
                'user_id' => 1,
            ],
            [
                'deliberation_id' => 12,
                'user_id' => 1,
            ],
            [
                'deliberation_id' => 14,
                'user_id' => 1,
            ],
            [
                'deliberation_id' => 15,
                'user_id' => 1,
            ],
            [
                'deliberation_id' => 16,
                'user_id' => 1,
            ],
        ];

        $table->insert($data)
            ->saveData();
    }
}
