<?php




use Phinx\Seed\AbstractSeed;

class CompteursSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $this->execute('TRUNCATE TABLE compteurs CASCADE');
        $table = $this->table('compteurs');

        $data = [
            [
                'id' => 1,
                'nom' => 'Compteur 1',
                'commentaire' => 'Compteur avec réinitialisation à l\'année à ne pas réinitialiser',
                'def_compteur' => 'C1_#AAAA#_#000#',
                'sequence_id' => 1,
                'def_reinit' => '#AAAA#',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 2,
                'nom' => 'Compteur 2',
                'commentaire' => 'Compteur avec réinitialisation au mois à ne pas réinitialiser',
                'def_compteur' => 'C2_#AAAA#_#MM#_#000#',
                'sequence_id' => 2,
                'def_reinit' => '#MM#',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 3,
                'nom' => 'Compteur 3',
                'commentaire' => 'Compteur avec réinitialisation au jour à ne pas réinitialiser',
                'def_compteur' => 'C3_#AAAA#_#MM#_#JJ#_#000#',
                'sequence_id' => 3,
                'def_reinit' => '#JJ#',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 4,
                'nom' => 'Compteur 4',
                'commentaire' => 'Compteur avec réinitialisation à l\'année à réinitialiser',
                'def_compteur' => 'C4_#AAAA#_#000#',
                'sequence_id' => 4,
                'def_reinit' => '#AAAA#',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 5,
                'nom' => 'Compteur 5',
                'commentaire' => 'Compteur avec réinitialisation au mois à réinitialiser',
                'def_compteur' => 'C5_#AAAA#_#MM#_#000#',
                'sequence_id' => 5,
                'def_reinit' => '#MM#',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 6,
                'nom' => 'Compteur 6',
                'commentaire' => 'Compteur avec réinitialisation au jour à réinitialiser',
                'def_compteur' => 'C6_#AAAA#_#MM#_#JJ#_#000#',
                'sequence_id' => 6,
                'def_reinit' => '#JJ#',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 7,
                'nom' => 'Compteur 7',
                'commentaire' => 'Compteur avec réinitialisation à l\'année  à ne pas réinitialiser'
                    . ' car sur une autre séquence déjà réinitialisée',
                'def_compteur' => 'C7_#AAAA#_#000#',
                'sequence_id' => 7,
                'def_reinit' => '#AAAA#',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 8,
                'nom' => 'Compteur 8',
                'commentaire' => 'Compteur avec réinitialisation au mois à ne pas réinitialiser'
                    . ' car sur une autre séquence déjà réinitialisée',
                'def_compteur' => 'C8_#AAAA#_#MM#_#000#',
                'sequence_id' => 8,
                'def_reinit' => '#MM#',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 9,
                'nom' => 'Compteur 9',
                'commentaire' => 'Compteur avec réinitialisation au jour à ne pas réinitialiser'
                    . ' car sur une autre séquence déjà réinitialisée',
                'def_compteur' => 'C9_#AAAA#_#MM#_#JJ#_#000#',
                'sequence_id' => 9,
                'def_reinit' => '#JJ#',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 10,
                'nom' => 'Compteur 10',
                'commentaire' => 'Compteur sans réinitialisation à l\'année à ne pas réinitialiser',
                'def_compteur' => 'C10_#AAAA#_#0000#',
                'sequence_id' => 10,
                'def_reinit' => '',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 11,
                'nom' => 'Compteur 11',
                'commentaire' => 'Compteur sans réinitialisation au mois à ne pas réinitialiser',
                'def_compteur' => 'C11_#AAAA#_#MM#_#0000#',
                'sequence_id' => 10,
                'def_reinit' => '',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 12,
                'nom' => 'Compteur 12',
                'commentaire' => 'Compteur sans réinitialisation au jour à ne pas réinitialiser',
                'def_compteur' => 'C12_#AAAA#_#MM#_#JJ#_#0000#',
                'sequence_id' => 10,
                'def_reinit' => '',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
        ];

        $table->insert($data)
            ->saveData();
    }
}
