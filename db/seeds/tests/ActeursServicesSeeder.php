<?php

use Phinx\Seed\AbstractSeed;

class ActeursServicesSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;

    public function getDependencies(): array
    {
        return [
            'ActeursSeeder',
            'ServicesSeeder',
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('acteurs_services');
        $table->truncate();

        $data = [
            // délégation pour le groupe Majorité seulement
            [
                'id' => 1,
                'acteur_id' => 1,
                'service_id' => 1,
            ],
            [
                'id' => 2,
                'acteur_id' => 2,
                'service_id' => 1,
            ],
            [
                'id' => 3,
                'acteur_id' => 3,
                'service_id' => 1,
            ],
            [
                'id' => 4,
                'acteur_id' => 4,
                'service_id' => 1,
            ],
            [
                'id' => 5,
                'acteur_id' => 5,
                'service_id' => 1,
            ],
            [
                'id' => 6,
                'acteur_id' => 6,
                'service_id' => 1,
            ],
            [
                'id' => 7,
                'acteur_id' => 7,
                'service_id' => 1,
            ],
            [
                'id' => 8,
                'acteur_id' => 8,
                'service_id' => 1,
            ],
            [
                'id' => 9,
                'acteur_id' => 9,
                'service_id' => 1,
            ],
            [
                'id' => 10,
                'acteur_id' => 10,
                'service_id' => 1,
            ],
            [
                'id' => 11,
                'acteur_id' => 11,
                'service_id' => 1,
            ],
            [
                'id' => 12,
                'acteur_id' => 12,
                'service_id' => 1,
            ],
            [
                'id' => 13,
                'acteur_id' => 13,
                'service_id' => 1,
            ],
            [
                'id' => 14,
                'acteur_id' => 14,
                'service_id' => 1,
            ],
            [
                'id' => 15,
                'acteur_id' => 15,
                'service_id' => 1,
            ],
        ];

        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());
    }
}
