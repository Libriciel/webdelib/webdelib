<?php

use Phinx\Seed\AbstractSeed;

class ActeursSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;

    public function getDependencies(): array
    {
        return [
            'TypeActeursSeeder',
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('acteurs');
        $table->truncate();

        $data = [
            // Groupe Majorité
            [
                'id' => 1,
                'typeacteur_id' => 1,
                'position' => 1,
                'suppleant_id' => 26,
            ],
            [
                'id' => 2,
                'typeacteur_id' => 1,
                'position' => 2,
                'suppleant_id' => 27,
            ],
            [
                'id' => 3,
                'typeacteur_id' => 1,
                'position' => 3,
                'suppleant_id' => 28,
            ],
            [
                'id' => 4,
                'typeacteur_id' => 1,
                'position' => 4,
                'suppleant_id' => 29,
            ],
            [
                'id' => 5,
                'typeacteur_id' => 1,
                'position' => 5,
                'suppleant_id' => 30,
            ],
            [
                'id' => 6,
                'typeacteur_id' => 1,
                'position' => 6,
                'suppleant_id' => 31,
            ],
            [
                'id' => 7,
                'typeacteur_id' => 1,
                'position' => 7,
                'suppleant_id' => 32,
            ],
            [
                'id' => 8,
                'typeacteur_id' => 1,
                'position' => 8,
                'suppleant_id' => 33,
            ],
            [
                'id' => 9,
                'typeacteur_id' => 1,
                'position' => 9,
                'suppleant_id' => 34,
            ],
            [
                'id' => 10,
                'typeacteur_id' => 1,
                'position' => 10,
                'suppleant_id' => 35,
            ],
            [
                'id' => 11,
                'typeacteur_id' => 1,
                'position' => 11,
            ],
            [
                'id' => 12,
                'typeacteur_id' => 1,
                'position' => 12,
            ],
            [
                'id' => 13,
                'typeacteur_id' => 1,
                'position' => 13,
            ],
            [
                'id' => 14,
                'typeacteur_id' => 1,
                'position' => 14,
            ],
            [
                'id' => 15,
                'typeacteur_id' => 1,
                'position' => 15,
            ],
            // Groupe Opposition
            [
                'id' => 16,
                'typeacteur_id' => 2,
                'position' => 16,
            ],
            [
                'id' => 17,
                'typeacteur_id' => 2,
                'position' => 17,
            ],
            [
                'id' => 18,
                'typeacteur_id' => 2,
                'position' => 18,
            ],
            [
                'id' => 19,
                'typeacteur_id' => 2,
                'position' => 19,
            ],
            [
                'id' => 20,
                'typeacteur_id' => 2,
                'position' => 20,
            ],
            [
                'id' => 21,
                'typeacteur_id' => 2,
                'position' => 21,
            ],
            // Groupe sans étiquette
            [
                'id' => 22,
                'typeacteur_id' => 3,
                'position' => 22,
            ],
            [
                'id' => 23,
                'typeacteur_id' => 3,
                'position' => 23,
            ],
            [
                'id' => 24,
                'typeacteur_id' => 3,
                'position' => 24,
            ],
            [
                'id' => 25,
                'typeacteur_id' => 3,
                'position' => 25,
            ],
            //Suppléants
            [
                'id' => 26,
                'typeacteur_id' => 5,
            ],
            [
                'id' => 27,
                'typeacteur_id' => 5,
            ],
            [
                'id' => 28,
                'typeacteur_id' => 5,
            ],
            [
                'id' => 29,
                'typeacteur_id' => 5,
            ],
            [
                'id' => 30,
                'typeacteur_id' => 5,
            ],
            [
                'id' => 31,
                'typeacteur_id' => 5,
            ],
            [
                'id' => 32,
                'typeacteur_id' => 5,
            ],
            [
                'id' => 33,
                'typeacteur_id' => 5,
            ],
            [
                'id' => 34,
                'typeacteur_id' => 5,
            ],
            [
                'id' => 35,
                'typeacteur_id' => 5,
            ],
        ];

        $this->addInformations($data);

        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());
    }

    private function addInformations(&$data)
    {
        foreach ($data as &$row) {
            $this->acteurInformations($row);
        }
    }

    private function acteurInformations(&$row)
    {
        $faker = Faker\Factory::create('fr_FR');
        $gender = $faker->randomElement(['male', 'female']);
        $row += [
            'nom' => $faker->lastName($gender),
            'position' => !empty($row['position']) ? $row['position'] : null,
            'prenom' => $faker->firstName($gender),
            'salutation' => $faker->title($gender),
            'titre' => $faker->randomElement(['Docteur', 'Professeur', '', '', '']),
            'adresse1' => $faker->address(),
            'adresse2' => $faker->secondaryAddress(),
            'cp' => $faker->postcode(),
            'ville' => $faker->city(),
            'email' => $faker->email(),
            'telfixe' => $faker->phoneNumber(),
            'telmobile' => $faker->mobileNumber(),
            'note' => $faker->realText(500),
            'actif' => true,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
            'notif_insertion' => false,
            'notif_projet_valide' => false,
        ];
    }
}
