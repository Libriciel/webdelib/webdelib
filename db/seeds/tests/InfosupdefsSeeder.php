<?php

use Phinx\Seed\AbstractSeed;

class InfosupdefsSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('infosupdefs');
        $table->truncate();

        $data = [
            [
                'id' => 1,
                'model' => 'Deliberation',
                'nom' => 'Axe 1',
                'commentaire' => '',
                'ordre' => 1,
                'code' => 'axes',
                'type' => 'listmulti',
                'val_initiale' => '',
                'recherche' => false,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'actif' => true,
            ],
            [
                'id' => 2,
                'model' => 'Deliberation',
                'nom' => 'Axe 2',
                'commentaire' => '',
                'ordre' => 2,
                'code' => 'axes2',
                'type' => 'listmulti',
                'val_initiale' => '',
                'recherche' => false,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'actif' => true,
            ],
        ];

        $table->insert($data)
            ->saveData();

        $this->updateSequence('infosupdefs');
    }
}
