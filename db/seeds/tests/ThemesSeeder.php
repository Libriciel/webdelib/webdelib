<?php


use Phinx\Seed\AbstractSeed;

class ThemesSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('themes');
        $table->truncate();

        $data = [
            [
                'id' => 1,
                'parent_id' => null,
                'order' => 'A',
                'libelle' => 'ADMINISTRATION GENERALE',
                'actif' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'lft' => 1,
                'rght' => 2,
            ],
            [
                'id' => 2,
                'parent_id' => null,
                'order' => 'B',
                'libelle' => 'FINANCES',
                'actif' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'lft' => 3,
                'rght' => 4,
            ],
        ];

        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());
    }
}
