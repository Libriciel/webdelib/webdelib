<?php




use Phinx\Seed\AbstractSeed;

class SeancesSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('seances');
        $table->truncate();

        $data = [
            [//Conseil Municipal
                'id' => 1,
                'type_id' => 1,
                'date' => date('Y-m-d 15:00:00'),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [//Conseil Municipal
                'id' => 2,
                'type_id' => 1,
                'date' => date('Y-m-d 20:00:00'),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [//Commission
                'id' => 3,
                'type_id' => 2,
                'date' => date('Y-m-d 10:00:00'),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [//Conseil Municipal passé en cours
                'id' => 4,
                'type_id' => 1,
                'date' => date('Y-m-d 20:00:00', strtotime('-1 day')),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [//Conseil Municipal passé // post-séances
                'id' => 5,
                'type_id' => 1,
                'date' => date('Y-m-d 20:00:00', strtotime('-7 day')),
                'traitee' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'pv_figes' => 1
            ],
        ];

        $table->insert($data)
            ->saveData();
    }
}
