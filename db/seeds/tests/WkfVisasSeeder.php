<?php



use Phinx\Seed\AbstractSeed;

class WkfVisasSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $this->execute('TRUNCATE TABLE wkf_visas CASCADE');
        $table = $this->table('wkf_visas');

        $data = [
            [
                'id' => 1,
                'traitement_id' => 1,
                'trigger_id' => 1,
                'signature_id' => null,
                'etape_nom' => 'Etape 1 - Utilisateur',
                'etape_type' => 1,
                'action' => 'OK',
                'commentaire' => '',
                'date' => date('Y-m-d H:i:s'),
                'numero_traitement' => 1,
                'type_validation' => 'V',
                'etape_id' => 1,
                'date_retard' => null,
            ],
            [
                'id' => 2,
                'traitement_id' => 1,
                'trigger_id' => 0,
                'signature_id' => null,
                'etape_nom' => 'Etape 2 - PARAPHEUR',
                'etape_type' => 1,
                'action' => 'RI',
                'commentaire' => '',
                'date' => null,
                'numero_traitement' => 1,
                'type_validation' => 'V',
                'etape_id' => 2,
                'date_retard' => null,
            ],
            [
                'id' => 3,
                'traitement_id' => 1,
                'trigger_id' => 1,
                'signature_id' => null,
                'etape_nom' => 'Etape 2 - PARAPHEUR',
                'etape_type' => 1,
                'action' => 'RI',
                'commentaire' => '',
                'date' => null,
                'numero_traitement' => 1,
                'type_validation' => 'V',
                'etape_id' => 3,
                'date_retard' => null,
            ],
        ];


        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());
    }
}
