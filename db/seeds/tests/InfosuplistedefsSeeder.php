<?php



use Phinx\Seed\AbstractSeed;

class InfosuplistedefsSeeder extends AbstractSeed
{

    public function getDependencies(): array
    {
        return [
            'InfosupdefsSeeder',
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('infosuplistedefs');
        $table->truncate();

        $data = [
        ];

        $table->insert($data)
            ->saveData();
    }
}
