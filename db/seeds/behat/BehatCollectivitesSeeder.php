<?php

use Phinx\Seed\AbstractSeed;

class BehatCollectivitesSeeder extends AbstractSeed
{
    public function getDependencies(): array
    {
        return [
            'TruncateAllSeeder'
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('collectivites');
        $table->truncate();
        $data = [
            'id' => 1,
            'id_entity' => null,
            'nom' => 'webdelib',
            'adresse' => '836 Rue du Mas de Verchant',
            'CP' => '34000',
            'ville' => 'MONTPELLIER',
            'telephone' => '0467659644',
            'logo' => '',
            'templateProject' => null,
            'background' => '',
            'config_login' => null,
            'siret' => '49101169800033',
            'code_ape' => '',
            'email' => '',
            'responsable_nom' => '',
            'responsable_prenom' => '',
            'responsable_fonction' => '',
            'responsable_email' => '',
            'dpo_nom' => '',
            'dpo_prenom' => '',
            'dpo_email' => '',
            'saas' => false,
            'force' => 1,
        ];

        $table->insert($data)
            ->saveData();
    }
}
