<?php

use Phinx\Seed\AbstractSeed;

class BehatProfilsSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;

    public function getDependencies(): array
    {
        return [
            'TruncateAllSeeder',
        ];
    }
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('profils');
        $table->truncate();

        $data = [
            [
                'id' => 1,
                'role_id' => 2,
                'name' => 'Administrateur',
                'actif' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]
        ];

        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());
    }
}
