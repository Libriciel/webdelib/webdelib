<?php

use Phinx\Seed\AbstractSeed;

class BehatAcosSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('acos');
        $table->truncate();
        //phpcs:disable
        //select '/*' || lpad(id || '', 3, ' ') || '*/ [''parent_id'' => ' || coalesce(parent_id || '', 'null')
        // || ', ''model'' => ' || coalesce(model || '', 'null') || ', ''foreign_key'' => '
        // || coalesce(foreign_key || '', 'null') || ', ''alias'' => ''' || alias ||''', ''lft'' => '
        // || lft || ', ''rght'' => ' || rght || '],' from acos order by id;
        $data = [
            /*  1*/
            ['parent_id' => null, 'model' => null, 'foreign_key' => null, 'alias' => 'controllers', 'lft' => 1, 'rght' => 174],
            /*  2*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Actes', 'lft' => 44, 'rght' => 47],
            /*  3*/
            ['parent_id' => 2, 'model' => null, 'foreign_key' => null, 'alias' => 'editPostSign', 'lft' => 45, 'rght' => 46],
            /*  4*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Acteurs', 'lft' => 162, 'rght' => 163],
            /*  5*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'AutresActes', 'lft' => 30, 'rght' => 43],
            /*  6*/
            ['parent_id' => 5, 'model' => null, 'foreign_key' => null, 'alias' => 'autresActesAValider', 'lft' => 31, 'rght' => 32],
            /*  7*/
            ['parent_id' => 5, 'model' => null, 'foreign_key' => null, 'alias' => 'autresActesValides', 'lft' => 33, 'rght' => 34],
            /*  8*/
            ['parent_id' => 5, 'model' => null, 'foreign_key' => null, 'alias' => 'autresActesAEnvoyer', 'lft' => 35, 'rght' => 36],
            /*  9*/
            ['parent_id' => 5, 'model' => null, 'foreign_key' => null, 'alias' => 'nonTransmis', 'lft' => 39, 'rght' => 40],
            /* 10*/
            ['parent_id' => 5, 'model' => null, 'foreign_key' => null, 'alias' => 'autresActesEnvoyes', 'lft' => 37, 'rght' => 38],
            /* 11*/
            ['parent_id' => 5, 'model' => null, 'foreign_key' => null, 'alias' => 'autresActesAbandon', 'lft' => 41, 'rght' => 42],
            /* 12*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Avis', 'lft' => 112, 'rght' => 117],
            /* 13*/
            ['parent_id' => 12, 'model' => null, 'foreign_key' => null, 'alias' => 'sendSeanceAvisToIdelibre', 'lft' => 113, 'rght' => 114],
            /* 14*/
            ['parent_id' => 12, 'model' => null, 'foreign_key' => null, 'alias' => 'downloadAvisElectroniqueJson', 'lft' => 115, 'rght' => 116],
            /* 15*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Collectivites', 'lft' => 130, 'rght' => 131],
            /* 16*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Compteurs', 'lft' => 142, 'rght' => 143],
            /* 17*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Connecteurs', 'lft' => 168, 'rght' => 169],
            /* 18*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Crons', 'lft' => 170, 'rght' => 171],
            /* 19*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Deliberations', 'lft' => 22, 'rght' => 29],
            /* 20*/
            ['parent_id' => 19, 'model' => null, 'foreign_key' => null, 'alias' => 'tousLesProjetsValidation', 'lft' => 25, 'rght' => 26],
            /* 21*/
            ['parent_id' => 19, 'model' => null, 'foreign_key' => null, 'alias' => 'tousLesProjetsSansSeance', 'lft' => 23, 'rght' => 24],
            /* 22*/
            ['parent_id' => 19, 'model' => null, 'foreign_key' => null, 'alias' => 'tousLesProjetsAFaireVoter', 'lft' => 27, 'rght' => 28],
            /* 23*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'ExportGed', 'lft' => 82, 'rght' => 87],
            /* 24*/
            ['parent_id' => 23, 'model' => null, 'foreign_key' => null, 'alias' => 'sendToGedAutreActe', 'lft' => 85, 'rght' => 86],
            /* 25*/
            ['parent_id' => 23, 'model' => null, 'foreign_key' => null, 'alias' => 'sendToGedDeliberation', 'lft' => 83, 'rght' => 84],
            /* 26*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Historiques', 'lft' => 172, 'rght' => 173],
            /* 27*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Infosupdefs', 'lft' => 166, 'rght' => 167],
            /* 28*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Postseances', 'lft' => 118, 'rght' => 129],
            /* 29*/
            ['parent_id' => 28, 'model' => null, 'foreign_key' => null, 'alias' => 'toSend', 'lft' => 119, 'rght' => 120],
            /* 30*/
            ['parent_id' => 28, 'model' => null, 'foreign_key' => null, 'alias' => 'transmit', 'lft' => 121, 'rght' => 122],
            /* 31*/
            ['parent_id' => 28, 'model' => null, 'foreign_key' => null, 'alias' => 'signature', 'lft' => 125, 'rght' => 126],
            /* 32*/
            ['parent_id' => 28, 'model' => null, 'foreign_key' => null, 'alias' => 'ExportGedPostSeances', 'lft' => 127, 'rght' => 128],
            /* 33*/
            ['parent_id' => 28, 'model' => null, 'foreign_key' => null, 'alias' => 'sendToSae', 'lft' => 123, 'rght' => 124],
            /* 34*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Preferences', 'lft' => 2, 'rght' => 7],
            /* 35*/
            ['parent_id' => 34, 'model' => null, 'foreign_key' => null, 'alias' => 'changeFormatSortie', 'lft' => 5, 'rght' => 6],
            /* 36*/
            ['parent_id' => 34, 'model' => null, 'foreign_key' => null, 'alias' => 'changeUserMdp', 'lft' => 3, 'rght' => 4],
            /* 37*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Profils', 'lft' => 150, 'rght' => 151],
            /* 38*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Projets', 'lft' => 8, 'rght' => 21],
            /* 39*/
            ['parent_id' => 38, 'model' => null, 'foreign_key' => null, 'alias' => 'mesProjetsATraiter', 'lft' => 11, 'rght' => 12],
            /* 40*/
            ['parent_id' => 38, 'model' => null, 'foreign_key' => null, 'alias' => 'mesProjetsValidation', 'lft' => 9, 'rght' => 10],
            /* 41*/
            ['parent_id' => 38, 'model' => null, 'foreign_key' => null, 'alias' => 'mesProjetsValides', 'lft' => 13, 'rght' => 14],
            /* 42*/
            ['parent_id' => 38, 'model' => null, 'foreign_key' => null, 'alias' => 'projetsMonService', 'lft' => 15, 'rght' => 16],
            /* 43*/
            ['parent_id' => 38, 'model' => null, 'foreign_key' => null, 'alias' => 'editerTous', 'lft' => 17, 'rght' => 18],
            /* 44*/
            ['parent_id' => 38, 'model' => null, 'foreign_key' => null, 'alias' => 'supprimerTous', 'lft' => 19, 'rght' => 20],
            /* 45*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Publier', 'lft' => 88, 'rght' => 93],
            /* 46*/
            ['parent_id' => 45, 'model' => null, 'foreign_key' => null, 'alias' => 'sendToPublish', 'lft' => 91, 'rght' => 92],
            /* 47*/
            ['parent_id' => 45, 'model' => null, 'foreign_key' => null, 'alias' => 'sendToPublishDeliberation', 'lft' => 89, 'rght' => 90],
            /* 48*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Seances', 'lft' => 100, 'rght' => 105],
            /* 49*/
            ['parent_id' => 48, 'model' => null, 'foreign_key' => null, 'alias' => 'calendrier', 'lft' => 101, 'rght' => 102],
            /* 50*/
            ['parent_id' => 48, 'model' => null, 'foreign_key' => null, 'alias' => 'ExportGedSeances', 'lft' => 103, 'rght' => 104],
            /* 51*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Search', 'lft' => 62, 'rght' => 65],
            /* 52*/
            ['parent_id' => 51, 'model' => null, 'foreign_key' => null, 'alias' => 'tousLesProjetsRecherche', 'lft' => 63, 'rght' => 64],
            /* 53*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Sequences', 'lft' => 140, 'rght' => 141],
            /* 54*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Services', 'lft' => 152, 'rght' => 153],
            /* 55*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Signatures', 'lft' => 66, 'rght' => 75],
            /* 56*/
            ['parent_id' => 55, 'model' => null, 'foreign_key' => null, 'alias' => 'sendAutreActesToSignature', 'lft' => 71, 'rght' => 72],
            /* 57*/
            ['parent_id' => 55, 'model' => null, 'foreign_key' => null, 'alias' => 'autresActesCancelSignature', 'lft' => 73, 'rght' => 74],
            /* 58*/
            ['parent_id' => 55, 'model' => null, 'foreign_key' => null, 'alias' => 'deliberationCancelSignature', 'lft' => 69, 'rght' => 70],
            /* 59*/
            ['parent_id' => 55, 'model' => null, 'foreign_key' => null, 'alias' => 'sendToParapheur', 'lft' => 67, 'rght' => 68],
            /* 60*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'TableauDeBord', 'lft' => 60, 'rght' => 61],
            /* 61*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Teletransmettre', 'lft' => 76, 'rght' => 81],
            /* 62*/
            ['parent_id' => 61, 'model' => null, 'foreign_key' => null, 'alias' => 'deliberationCancelSendToTDT', 'lft' => 77, 'rght' => 78],
            /* 63*/
            ['parent_id' => 61, 'model' => null, 'foreign_key' => null, 'alias' => 'autresActesCancelSendToTDT', 'lft' => 79, 'rght' => 80],
            /* 64*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Themes', 'lft' => 132, 'rght' => 133],
            /* 65*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Typeactes', 'lft' => 144, 'rght' => 145],
            /* 66*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Typeacteurs', 'lft' => 164, 'rght' => 165],
            /* 67*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Typeseances', 'lft' => 146, 'rght' => 147],
            /* 68*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Users', 'lft' => 148, 'rght' => 149],
            /* 69*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Validations', 'lft' => 48, 'rght' => 59],
            /* 70*/
            ['parent_id' => 69, 'model' => null, 'foreign_key' => null, 'alias' => 'retour', 'lft' => 51, 'rght' => 52],
            /* 71*/
            ['parent_id' => 69, 'model' => null, 'foreign_key' => null, 'alias' => 'validerEnUrgence', 'lft' => 57, 'rght' => 58],
            /* 72*/
            ['parent_id' => 69, 'model' => null, 'foreign_key' => null, 'alias' => 'goNext', 'lft' => 49, 'rght' => 50],
            /* 73*/
            ['parent_id' => 69, 'model' => null, 'foreign_key' => null, 'alias' => 'rebond', 'lft' => 53, 'rght' => 54],
            /* 74*/
            ['parent_id' => 69, 'model' => null, 'foreign_key' => null, 'alias' => 'reattribuerTous', 'lft' => 55, 'rght' => 56],
            /* 75*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'VerserSae', 'lft' => 94, 'rght' => 99],
            /* 76*/
            ['parent_id' => 75, 'model' => null, 'foreign_key' => null, 'alias' => 'sendToSaeAutresActes', 'lft' => 97, 'rght' => 98],
            /* 77*/
            ['parent_id' => 75, 'model' => null, 'foreign_key' => null, 'alias' => 'sendToSaeDeliberation', 'lft' => 95, 'rght' => 96],
            /* 78*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Votes', 'lft' => 106, 'rght' => 113],
            /* 79*/
            ['parent_id' => 78, 'model' => null, 'foreign_key' => null, 'alias' => 'downloadVotesElectroniqueJson', 'lft' => 109, 'rght' => 110],
            /* 80*/
            ['parent_id' => 78, 'model' => null, 'foreign_key' => null, 'alias' => 'sendSeanceVotesToIdelibre', 'lft' => 107, 'rght' => 108],
            /* 81*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'Cakeflow', 'lft' => 154, 'rght' => 161],
            /* 82*/
            ['parent_id' => 81, 'model' => null, 'foreign_key' => null, 'alias' => 'Circuits', 'lft' => 155, 'rght' => 156],
            /* 83*/
            ['parent_id' => 81, 'model' => null, 'foreign_key' => null, 'alias' => 'Compositions', 'lft' => 159, 'rght' => 160],
            /* 84*/
            ['parent_id' => 81, 'model' => null, 'foreign_key' => null, 'alias' => 'Etapes', 'lft' => 157, 'rght' => 158],
            /* 85*/
            ['parent_id' => 1, 'model' => null, 'foreign_key' => null, 'alias' => 'ModelOdtValidator', 'lft' => 134, 'rght' => 139],
            /* 86*/
            ['parent_id' => 85, 'model' => null, 'foreign_key' => null, 'alias' => 'Modeltemplates', 'lft' => 135, 'rght' => 136],
            /* 87*/
            ['parent_id' => 85, 'model' => null, 'foreign_key' => null, 'alias' => 'Modelvalidations', 'lft' => 137, 'rght' => 138],
            /* 88*/
            ['parent_id' => 78, 'model' => null, 'foreign_key' => null, 'alias' => 'importVotesIdelibre', 'lft' => 111, 'rght' => 112],
        ];
        //phpcs:enable
        $table->insert($data)
            ->saveData();

        $data = [];

        $lft = $this->getMax();

        $this->addAcosByModel($data, $lft, 'Typeacte', 'typeactes', 'name');
        $this->addAcosByModel($data, $lft, 'Typeseance', 'typeseances', 'libelle');
        $this->addAcosByModel($data, $lft, 'Service', 'services', 'name');
        $this->addAcosByModel($data, $lft, 'Circuit', 'wkf_circuits', 'nom');

        $table->insert($data)
            ->saveData();
    }

    private function addAcosByModel(&$data, &$lft, $model, $table, $name)
    {
        $rows = $this->fetchAll('SELECT id,' . $name . ' FROM ' . $table);
        foreach ($rows as $row) {
            $data[] = [
                'parent_id' => null,
                'model' => $model,
                'foreign_key' => $row['id'],
                'alias' => $row[$name],
                'lft' => $lft++,
                'rght' => $lft++
            ];
        }
    }

    private function getMax()
    {
        $max = $this->fetchRow('SELECT max(rght) from acos');
        return $max['max'];
    }
}
