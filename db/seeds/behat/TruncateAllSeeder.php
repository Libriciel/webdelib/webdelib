<?php

use Phinx\Seed\AbstractSeed;

class TruncateAllSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $rows = $this->fetchAll('select * from pg_catalog.pg_tables
                where schemaname=\'public\'
                 AND tablename NOT IN (
                   \'modelsections\',\'modeltypes\',\'modelvalidations\',\'modelvariables\',
                   \'acos\', \'natures\', \'crons\', \'phinxlog\', \'roles\', \'versions\'
              )
        ;');
        foreach ($rows as $row) {
            $tableName = $row['tablename'];
            $table = $this->table($tableName);
            if ($table->exists()) {
                $sql = sprintf(
                    'TRUNCATE TABLE %s RESTART IDENTITY CASCADE',
                    $this->getAdapter()->quoteTableName($tableName)
                );
                $this->query($sql);
            }
        }
    }
}
