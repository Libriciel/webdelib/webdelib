<?php

use Phinx\Seed\AbstractSeed;

class BehatArosSeeder extends AbstractSeed
{
    public function getDependencies(): array
    {
        return [
            'TruncateAllSeeder'
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('aros');
        $table->truncate();
        $data = [
            [
                'parent_id' => null,
                'model' => 'Profil',
                'foreign_key' => 1,
                'alias' => 'Administrateur',
                'lft' => 1,
                'rght' => 6
            ],
            [
                'parent_id' => 1,
                'model' => 'User',
                'foreign_key' => 1,
                'alias' => 'administrateur',
                'lft' => 2,
                'rght' => 3
            ],
        ];

        $table->insert($data)
            ->saveData();
    }
}
