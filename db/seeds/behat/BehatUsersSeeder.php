<?php

use Phinx\Seed\AbstractSeed;

class BehatUsersSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;

    public function getDependencies(): array
    {
        return [
            'TruncateAllSeeder'
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $this->execute('TRUNCATE TABLE users CASCADE');
        $table = $this->table('users');

        $data = [
            [
                'id' => 1,
                'profil_id' => 1,
                'statut' => 1,
                'username' => 'admin',
                'circuit_defaut_id' => 1,
                'password' => md5(env('APP_ADMIN_PASSWORD')),
                'nom' => 'admin',
                'prenom' => 'admin',
                'email' => 'admin@webdelib.invalid',
                'telfixe' => '',
                'telmobile' => '',
                'date_naissance' => null,
                'accept_notif' => false,
                'mail_refus' => false,
                'mail_traitement' => false,
                'mail_insertion' => false,
                'position' => null,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'mail_modif_projet_cree' => false,
                'mail_modif_projet_valide' => false,
                'mail_retard_validation' => false,
                'theme' => null,
                'active' => true,
                'mail_error_send_tdt_auto' => false,
                'mail_projet_valide' => false,
                'mail_projet_valide_valideur' => false,
                'mail_majtdtcourrier' => false,
                'service_defaut_id' => null,
                'preference_first_login_active' => true,
                'mail_error_sae' => false,
            ],
            [
                'id' => 2,
                'profil_id' => 1,
                'statut' => 1,
                'username' => 'assemblee1Username',
                'circuit_defaut_id' => 1,
                'password' => md5(env('APP_ADMIN_PASSWORD')),
                'nom' => 'assemblee1Nom',
                'prenom' => 'assemblee1Prenom',
                'email' => 'assemblee1Username@webdelib.invalid',
                'telfixe' => '',
                'telmobile' => '',
                'date_naissance' => null,
                'accept_notif' => false,
                'mail_refus' => false,
                'mail_traitement' => false,
                'mail_insertion' => false,
                'position' => null,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'mail_modif_projet_cree' => false,
                'mail_modif_projet_valide' => false,
                'mail_retard_validation' => false,
                'theme' => null,
                'active' => true,
                'mail_error_send_tdt_auto' => false,
                'mail_projet_valide' => false,
                'mail_projet_valide_valideur' => false,
                'mail_majtdtcourrier' => false,
                'service_defaut_id' => null,
                'preference_first_login_active' => true,
                'mail_error_sae' => false,
            ],
            [
                'id' => 3,
                'profil_id' => 1,
                'statut' => 1,
                'username' => 'adminFonctionel1Username',
                'circuit_defaut_id' => 1,
                'password' => md5(env('APP_ADMIN_PASSWORD')),
                'nom' => 'adminFonctionel1Nom',
                'prenom' => 'adminFonctionel1Prenom',
                'email' => 'adminFonctionel1Username@webdelib.invalid',
                'telfixe' => '',
                'telmobile' => '',
                'date_naissance' => null,
                'accept_notif' => false,
                'mail_refus' => false,
                'mail_traitement' => false,
                'mail_insertion' => false,
                'position' => null,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'mail_modif_projet_cree' => false,
                'mail_modif_projet_valide' => false,
                'mail_retard_validation' => false,
                'theme' => null,
                'active' => true,
                'mail_error_send_tdt_auto' => false,
                'mail_projet_valide' => false,
                'mail_projet_valide_valideur' => false,
                'mail_majtdtcourrier' => false,
                'service_defaut_id' => null,
                'preference_first_login_active' => true,
                'mail_error_sae' => false,
            ],
            [
                'id' => 4,
                'profil_id' => 1,
                'statut' => 1,
                'username' => 'redacteur1Username',
                'circuit_defaut_id' => 1,
                'password' => md5(env('APP_ADMIN_PASSWORD')),
                'nom' => 'redacteur1Nom',
                'prenom' => 'redacteur1Prenom',
                'email' => 'redacteur1Username@webdelib.invalid',
                'telfixe' => '',
                'telmobile' => '',
                'date_naissance' => null,
                'accept_notif' => false,
                'mail_refus' => false,
                'mail_traitement' => false,
                'mail_insertion' => false,
                'position' => null,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'mail_modif_projet_cree' => false,
                'mail_modif_projet_valide' => false,
                'mail_retard_validation' => false,
                'theme' => null,
                'active' => true,
                'mail_error_send_tdt_auto' => false,
                'mail_projet_valide' => false,
                'mail_projet_valide_valideur' => false,
                'mail_majtdtcourrier' => false,
                'service_defaut_id' => null,
                'preference_first_login_active' => true,
                'mail_error_sae' => false,
            ],
            [
                'id' => 5,
                'profil_id' => 1,
                'statut' => 1,
                'username' => 'validateur1Username',
                'circuit_defaut_id' => 1,
                'password' => md5(env('APP_ADMIN_PASSWORD')),
                'nom' => 'validateur1Nom',
                'prenom' => 'validateur1Prenom',
                'email' => 'validateur1Username@webdelib.invalid',
                'telfixe' => '',
                'telmobile' => '',
                'date_naissance' => null,
                'accept_notif' => false,
                'mail_refus' => false,
                'mail_traitement' => false,
                'mail_insertion' => false,
                'position' => null,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'mail_modif_projet_cree' => false,
                'mail_modif_projet_valide' => false,
                'mail_retard_validation' => false,
                'theme' => null,
                'active' => true,
                'mail_error_send_tdt_auto' => false,
                'mail_projet_valide' => false,
                'mail_projet_valide_valideur' => false,
                'mail_majtdtcourrier' => false,
                'service_defaut_id' => null,
                'preference_first_login_active' => true,
                'mail_error_sae' => false,
            ]
        ];

        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());
    }
}
