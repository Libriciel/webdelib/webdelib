<?php

use Phinx\Seed\AbstractSeed;

class BehatServicesSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;

    public function getDependencies(): array
    {
        return [
            'TruncateAllSeeder',
        ];
    }
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('services');
        $table->truncate();

        $data = [
            [
                'id'    => 1,
                'parent_id'    => null,
                'order'    => 'A',
                'name'    => 'Informatique',
                'circuit_defaut_id'    => 1,
                'actif'    => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'lft'    => 1,
                'rght'    => 2,
            ],
            [
                'id'    => 2,
                'parent_id'    => null,
                'order'    => 'AA',
                'name'    => 'Urbanisme',
                'circuit_defaut_id'    => 1,
                'actif'    => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'lft'    => 3,
                'rght'    => 6,
            ],
            [
                'id'    => 3,
                'parent_id'    => 2,
                'order'    => 'A',
                'name'    => 'Voirie',
                'circuit_defaut_id'    => 1,
                'actif'    => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'lft'    => 4,
                'rght'    => 5,
            ],
        ];

        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());
    }
}
