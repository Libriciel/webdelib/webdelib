<?php

use Phinx\Seed\AbstractSeed;

class BehatArosAcosSeeder extends AbstractSeed
{
    public function getDependencies(): array
    {
        return [
            'AcosSeeder',
            'ArosSeeder',
            'TruncateAllSeeder'
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('aros_acos');
        $table->truncate();
        //phpcs:disable
        // To generate
        // SELECT '[''aro_id'' => ' || aro_id || ', ''aco_id'' => ' || aco_id || ', ''_create'' => '
        // || _create || ', ''_read'' => ' || _read || ', ''_update'' => '
        // || _update || ', ''_delete'' => ' || _delete || ', ''_manager'' => ' || _manager ||'],'
        // FROM aros_acos WHERE aro_id = 1 ORDER BY aro_id, aco_id;
        //phpcs:enable
        $data = [
            ['aro_id' => 1, 'aco_id' => 1, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 2, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 3, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 4, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 5, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 6, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 7, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 8, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 9, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 10, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 11, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 12, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 13, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 14, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 15, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 16, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 17, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 18, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 19, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 20, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 21, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 22, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 23, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 24, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 25, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 26, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 27, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 28, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 29, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 30, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 31, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 32, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 33, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 34, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 35, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 36, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 37, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 38, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 39, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 40, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 41, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 42, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 43, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 44, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 45, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 46, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 47, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 48, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 49, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 50, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 51, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 52, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 53, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 54, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 55, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 56, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 57, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 58, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 59, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 60, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 61, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 62, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 63, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 64, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 65, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 66, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 67, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 68, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 69, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 70, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 71, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 72, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 73, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 74, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 75, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 76, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 77, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 78, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 79, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 80, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 81, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 82, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 83, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 84, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 85, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 86, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
            ['aro_id' => 1, 'aco_id' => 87, '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1],
        ];

        $this->addUserAdministrateur($data, 2);
        $this->addUserUtilisateur($data, 3);

        $table->insert($data)
            ->saveData();
    }

    private function addUserAdministrateur(&$data, $aroId)
    {
        foreach ($data as $row) {
            $row['aro_id'] = $aroId;
            $data[] = $row;
        }

        $this->addArosAcosForUserId($data, 'Typeacte', $aroId);
        $this->addArosAcosForUserId($data, 'Typeseance', $aroId);
        $this->addArosAcosForUserId($data, 'Service', $aroId);
        $this->addArosAcosForUserId($data, 'Circuit', $aroId);
    }

    private function addUserUtilisateur(&$data, $aroId)
    {
        foreach ($data as $row) {
            $row['aro_id'] = $aroId;
            $data[] = $row;
        }

        $this->addArosAcosForUserId($data, 'Typeacte', $aroId);
        $this->addArosAcosForUserId($data, 'Typeseance', $aroId);
        $this->addArosAcosForUserId($data, 'Service', $aroId);
        $this->addArosAcosForUserId($data, 'Circuit', $aroId);
    }

    private function addArosAcosForUserId(&$data, $model, $aroId)
    {
        $rows = $this->fetchAll('SELECT id FROM acos WHERE model=\'' . $model .'\'');
        foreach ($rows as $row) {
            $data[] = [
                'aro_id' => $aroId,
                'aco_id' => $row['id'],
                '_create' => 1,
                '_read' => 1,
                '_update' => 1,
                '_delete' => 1
            ];
        }
    }
}
