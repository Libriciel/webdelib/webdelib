<?php

use Phinx\Seed\AbstractSeed;

class BehatServicesUsersSeeder extends AbstractSeed
{
    use dbSeeder\SeederTrait;

    public function getDependencies(): array
    {
        return [
            'TruncateAllSeeder',
            'ServicesSeeder',
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('services_users');
        $table->truncate();

        $data = [
            [
                'user_id'    => 1,
                'service_id'    => 1,
            ],
            [
                'user_id'    => 2,
                'service_id'    => 3,
            ],
        ];

        $table->insert($data)
            ->saveData();

        $this->updateSequence($table->getName());
    }
}
