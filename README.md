# webdelib
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
[![PHP](https://img.shields.io/badge/PHP-%3E%3D7.4-%237A86B8)](https://www.php.net)
[![Ubuntu 20.04 package](https://repology.org/badge/version-for-repo/ubuntu_20_04/php.svg)](https://repology.org/project/php/versions)

## Canonical source

The canonical source of webdelib is [hosted on adullact.net](https://gitlab.adullact.net/libriciel/webdelib/).

## informations

Download webdelib:
https://gitlab.adullact.net/Libriciel/webdelib/webdelib

Documentation:
http://www.libriciel.fr/index.php/libriciels/webdelib

If you need help with your webdelib installation and for any technical questions please contact us at contact.client@libriciel.coop

For all other questions, contact us at contact@libriciel.coop

## Open source software to collaborate on code

To see how webdelib looks please see the [features page on our website](https://client.libriciel.fr/otrs/public.pl?Action=PublicFAQExplorer;CategoryID=7).

- Manage project with fine grained access controls
- Used by more than 200 organizations, webdelib is the most popular solution to manage acte process
- Completely free and open source (GNU AFFERO license)
- Powered by [CakePHP](https://github.com/cakephp/cakephp)


## Editions

There are one editions of webdelib:

- webdelib is available freely under the GNU AFFERO license.

## Website

On [about.libriciel.fr](https://www.libriciel.fr/) you can find more information.

# File fqdn_tenants.php
```
<?php

$config = [
    'fqdn_config' => [
        'localhost' => 'webdelib_db1', // Localhost par défaut
        'webdelib.localhost' => 'webdelib_db1',
        'webdelib2.localhost' => 'webdelib_db2'
    ]
];
```
In the exemple the first tenant name is webdelib_db1 and the second is webdelib_db2.

# All in one

How to install this?

1. [Install Docker Compose](https://docs.docker.com/compose/install/)
1. Clone this repository
1. Copy .env.dist to .env
1. Change your `POSTGRES_PASSWORD` in `.env`
1. Add folder `/data` fqdn and tenants in `/data/fqdn_tenants.php`
1. Add folder `/data/sessions`
1. Change your `APP_DATA` in `.env`
1. Copy app/Config/webdelib.inc.default to `/data/{tenant_name}/config/webdelib.inc`
1. Copy app/Config/pastell.inc.default to `/data/{tenant_name}/config/pastell.inc`
1. Copy app/Config/formats.inc.default to `/data/{tenant_name}/config/formats.inc`
1. Run all containers with `make start`
1. Run installation data `make composer c='app-install'`
1. Verify installation with your fqdn
