<?php

namespace contextTrait;

/**
 * Trait ajoutant la possiblité de cliquer avec des sélecteurs Xpath et CSS.
 * 
 * @see {@url http://masnun.com/2012/11/28/behat-and-mink-finding-and-clicking-with-xpath-and-jquery-like-css-selector.html}
 */
trait ExtraClickSelectorsMinkContextTrait {

    /**
     * Click on the element with the provided xpath query
     *
     * @When /^(?:|I )click on the element with xpath "(?P<selector>[^"]*)"$/
     */
    public function iClickOnTheElementWithXPath($selector) {
        $session = $this->getSession(); // get the mink session
        $xpath = $session->getSelectorsHandler()->selectorToXpath('xpath', $selector);
        $element = $session->getPage()->find('xpath', $xpath);

        if (null === $element) {
            throw new \InvalidArgumentException(sprintf('Could not evaluate XPath: "%s"', $selector));
        }

        $element->click();
    }

    /**
     * Click on the element with the provided CSS selector
     *
     * @When /^I click on the element with css selector "(?P<selector>[^"]*)"$/
     */
    public function iClickOnTheElementWithCSSSelector($selector) {
        $session = $this->getSession();
        $xpath = $session->getSelectorsHandler()->selectorToXpath('css', $selector);
        $element = $session->getPage()->find('xpath', $xpath);

        if (null === $element) {
            throw new \InvalidArgumentException(sprintf('Could not evaluate CSS Selector: "%s"', $selector));
        }

        $element->click();
    }

}
