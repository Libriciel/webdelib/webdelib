<?php

namespace contextTrait;

/**
 * Trait simplifiant l'utilisation d'éléments Bootstrap (3).
 */
trait BootstrapMinkContextTrait
{
    protected function linkXpath($text)
    {
        $normalized = normalize($text);
        return 'a[normalize-space(text())="' . $normalized . '" or normalize-space(@title)="' . $normalized . '"]';
    }

    /**
     * Méthode protégée, à surcharger dans les contextes des applications car
     * on peut soit avoir à cliquer sur chacun des éléments du menu, soit avoir
     * à cliquer le premier et le dernier niveau et à survoler les niveaux
     * intermédiaires.
     *
     * @param string $path Le chemin désignant l'élément de menu auquel accéder.
     * @param bool $alwaysClick Doit-on toujours cliquer pour accéder aux éléments
     *  intermédiaires.
     * @throws \InvalidArgumentException
     */
    protected function accessBootstrapMenuElement($path, $alwaysClick = false)
    {
        $page = $this->getSession()->getPage();
        $parts = explode('>', $path);
        $max = count($parts) - 1;
        $xpath = '';

        foreach ($parts as $idx => $part) {
            $xpath .= '//ul//'.$this->linkXpath($part);
            $element = $page->find('xpath', $xpath);

            // Attendre que l'élément bloquant disparaisse
            $this->getSession()->wait(5000, "document.querySelector('.alert-info.growl-animated') === null");


            if (null === $element) {
                $message = 'Could not find link: "%s" at position "%s" with xpath "%s"';
                throw new \InvalidArgumentException(sprintf($message, $path, $part, $xpath));
            }

            if (false === $element->isVisible()) {
                $message = 'Element is not visible: "%s" at position "%s" with xpath "%s"';
                throw new \InvalidArgumentException(sprintf($message, $path, $part, $xpath));
            }

            if (in_array($idx, [0, $max], true)) {
                $element->click();
            } elseif (true === $alwaysClick) {
                $element->click();
            } else {
                $element->mouseOver();
            }

            if ($idx !== $max) {
                $xpath .= '/parent::li';
            }
        }
    }
    
    /**
     * @When /^(?:|je )suis dans l'onglet "(?P<name>[^"]*)"$/
     *
     * @param string $name
     */
    public function iAmInTheTab($name)
    {
        $page = $this->getSession()->getPage();
        $xpath = '//ul[contains(@class,"nav nav-tabs")]//li[@class="active"]//*[normalize-space(text())="'.normalize($name).'"]';
        $this->assertSession()->elementExists('xpath', $xpath);
    }
    
    /**
     * @When /^(?:|je )clique sur l'onglet "(?P<name>[^"]*)"$/
     *
     * @param string $name
     */
    public function iClickOnTheTab($name)
    {
        $page = $this->getSession()->getPage();
        $xpath = '//ul[contains(@class,"nav nav-tabs")]//li//a[normalize-space(text())="'.normalize($name).'"]';
        $element = $page->find('xpath', $xpath);
        $element->click();
    }
}
