<?php

namespace contextTrait;

/**
 * Trait permettant de vérifier le titre de la page ainsi que la présence de
 * titres (headings) visibles dans la page.
 *
 * Il surcharge également la vérification de texte pour ajouter des termes au
 * vocabulaire.
 */
trait HtmlTitleAndTextMinkContextTrait
{
//    /**
//     * Checks, that page contains specified visible text
//     * Example: Then I should see "Who is the Batman?"
//     * Example: And I should see the text "Who is the Batman?"
//     *
//     * @Given /^(?:|I )see the text "(?P<text>(?:[^"]|\\")*)"$/
//     * @Then /^(?:|I )see "(?P<text>(?:[^"]|\\")*)"$/
//     * @Then /^(?:|I )should see "(?P<text>(?:[^"]|\\")*)"$/
//     * @Then /^(?:|I )should see the text "(?P<text>(?:[^"]|\\")*)"$/
//     */
//    public function assertPageContainsText($text)
//    {
//        parent::assertPageContainsText($text);
//    }
    
//    /**
//     * Checks, that page doesn't contain specified text or that it is invisible
//     * Example: Then I should not see "Batman is Bruce Wayne"
//     * Example: And I should not see "Batman is Bruce Wayne"
//     *
//     * @Given /^(?:|I )(do not|don't) see the text "(?P<text>(?:[^"]|\\")*)"$/
//     * @Then /^(?:|I )should not see "(?P<text>(?:[^"]|\\")*)"$/
//     * @Then /^(?:|I )should not see the text "(?P<text>(?:[^"]|\\")*)"$/
//     */
//    public function assertPageNotContainsText($text)
//    {
//        parent::assertPageNotContainsText($text);
//    }

    /**
     * @param string $text
     * @param bool $contains
     * @return string
     */
    protected function getAnyHeadingXpath($text, $contains = false)
    {
        if(true === $contains) {
            $prefix = '//text()[contains(normalize-space(.),"'.normalize($text).'")]';
        } else {
            $prefix = '//text()[normalize-space(.) = "'.normalize($text).'"]';
        }

        $xpaths = [
            $prefix . '/ancestor::*[self::h]',
            $prefix . '/ancestor::*[self::h1]',
            $prefix . '/ancestor::*[self::h2]',
            $prefix . '/ancestor::*[self::h3]',
            $prefix . '/ancestor::*[self::h4]',
            $prefix . '/ancestor::*[self::h5]',
            $prefix . '/ancestor::*[self::h6]'
        ];

        return implode("|", $xpaths);
    }

    /**
     * Checks, that specified title is visible on page
     * Example: Then I should see the title "Who is the Batman?"
     * Example: And I should see a title "Who is the Batman?"
     *
     * @Then /^(?:|I )(should ){0,1}see (a|the) title "(?P<title>(?:[^"]|\\")*)"$/
     */
    public function assertPageHasHeading($title)
    {
        $xpath = $this->getAnyHeadingXpath($this->fixStepArgument($title), false);
        $node = $this->assertSession()->elementExists('xpath', $xpath);
        if (null !== $node && false === $node->isVisible()) {
            $message = 'Title "%s" matching xpath "%s" was found but is not visible';
            throw new \InvalidArgumentException(sprintf($message, $title, $xpath));
        }
    }

    /**
     * Checks, that specified title is not visible on page
     * Example: Then I should not see the title "Batman is Bruce Wayne"
     * Example: And I should not see a title "Batman is Bruce Wayne"
     *
     * @Then /^(?:|I )(should not|do not|don't) see (a|the) title "(?P<title>(?:[^"]|\\")*)"$/
     */
    public function assertPageNotHasHeading($title)
    {
        $xpath = $this->getAnyHeadingXpath($this->fixStepArgument($title), false);
        $page = $this->getSession()->getPage();
        $node = $page->find('xpath', $xpath);
        if (null !== $node && true === $node->isVisible()) {
            $message = 'Visible title "%s" matching xpath "%s" was found';
            throw new \InvalidArgumentException(sprintf($message, $title, $xpath));
        } else {
            $this->assertSession()->elementNotExists('xpath', $xpath);
        }
    }

    /**
     * Checks, that a title containing the specified text is visible on page
     * Example: Then I should see the title containing "Who is the Batman?"
     * Example: And I should see a title containing "Who is the Batman?"
     *
     * @Then /^(?:|I )(should ){0,1}see (a|the) title containing "(?P<title>(?:[^"]|\\")*)"$/
     */
    public function assertPageHasHeadingContaining($title)
    {
        $xpath = $this->getAnyHeadingXpath($this->fixStepArgument($title), true);
        $node = $this->assertSession()->elementExists('xpath', $xpath);
        if (null !== $node && false === $node->isVisible()) {
            $message = 'Title containing "%s" matching xpath "%s" was found but is not visible';
            throw new \InvalidArgumentException(sprintf($message, $title, $xpath));
        }
    }

    /**
     * Checks, that a title containing the specified text is not visible on page
     * Example: Then I should not see the title containing "Batman is Bruce Wayne"
     * Example: And I should not see a title containing "Batman is Bruce Wayne"
     *
     * @Then /^(?:|I )(should not|do not|don't) see (a|the) title containing "(?P<title>(?:[^"]|\\")*)"$/
     */
    public function assertPageNotHasHeadingContaining($title)
    {
        $xpath = $this->getAnyHeadingXpath($this->fixStepArgument($title), true);
        $page = $this->getSession()->getPage();
        $node = $page->find('xpath', $xpath);
        if (null !== $node && true === $node->isVisible()) {
            $message = 'Visible title containing "%s" matching xpath "%s" was found';
            throw new \InvalidArgumentException(sprintf($message, $title, $xpath));
        } else {
            $this->assertSession()->elementNotExists('xpath', $xpath);
        }
    }
    
    /**
     * Checks, that the page has the specified title
     * Example: Then I should see that the page title is "Who is the Batman?"
     *
     * @Then /^(?:|I )(should ){0,1}see that the page title is "(?P<title>(?:[^"]|\\")*)"$/
     */
    public function assertPageHasTitle($title)
    {
        $page = $this->getSession()->getPage();
        $node = $this->assertSession()->elementExists('xpath', '//title');

        $expected = normalize_space($title);
        $actual = normalize_space($node->getText());
        if($expected !== $actual) {
            $message = 'Expecting page title "%s" while "%s" was found';
            throw new \InvalidArgumentException(sprintf($message, $expected, $actual));
        }
    }
}