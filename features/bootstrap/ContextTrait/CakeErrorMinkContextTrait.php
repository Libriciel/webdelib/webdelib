<?php

namespace contextTrait;

/**
 * Trait permettant de s'assurer qu'aucun message de classe cake-error n'existe
 * dans la page. Il est donc nécessaire que la valeur de debug soit >= 1.
 * La vérification est faite:
 *  - avant et après chaque scénario
 *  - avant et après chaque étape
 * ATTENTION: la vérification ne se fait pas pour le contexte.
 * @see {@url http://behat.org/en/latest/user_guide/context/hooks.html}
 */
trait CakeErrorMinkContextTrait
{
    /**
     * On s'assure qu'aucun message de classe cake-error n'existe dans la page.
     */
    protected function assertCakeErrorMessagesNotExists()
    {
        $page = $this->getSession()->getPage();
        $xpath = '//pre[contains(@class, "cake-error")]';
        $this->assertSession()->elementNotExists('xpath', $xpath);
    }

    /**
     * @BeforeScenario
     */
    public function beforeScenarioAssertCakeMessagesNotExists($scope)
    {
        $this->assertCakeErrorMessagesNotExists();
    }

    /**
     * @AfterScenario
     */
    public function afterScenarioAssertCakeMessagesNotExists($scope)
    {
        $this->assertCakeErrorMessagesNotExists();
    }

    /**
     * @BeforeStep
     */
    public function beforeStepAssertCakeMessagesNotExists($scope)
    {
        $this->assertCakeErrorMessagesNotExists();
    }

    /**
     * @AfterStep
     */
    public function afterStepAssertCakeMessagesNotExists($scope)
    {
        $this->assertCakeErrorMessagesNotExists();
    }
}