<?php

namespace contextTrait;

/**
 * Trait simplifiant l'utilisation d'éléments Bootstrap (3).
 */
trait OverloadedMinkContextTrait
{
    /**
     * Surcharge de ma méthode MinkContext::attachFileToField afin de vérifier
     * la présence du fichier à attacher.
     * @param string $field
     * @param string $path
     * @throws \InvalidArgumentException
     *@see Behat\\MinkExtension\\Context\\MinkContext::attachFileToField
     * @When /^(?:|I )attach the file verify "(?P<path>[^"]*)" to "(?P<field>(?:[^"]|\\")*)"$/
     * @When j'attache le fichier vérifié :path à :field
     */
//    #[When('/^(?:|I )attach the file "(?P<path>[^"]*)" to "(?P<field>(?:[^"]|\\")*)"$/')]
    public function attachFileVerifyToField($path, $field)
    {
        if ($this->getMinkParameter('files_path')) {
            $fullPath = rtrim(realpath($this->getMinkParameter('files_path')), DIRECTORY_SEPARATOR)
                . DIRECTORY_SEPARATOR . $path;
            if (is_file($fullPath)) {
                $path = $fullPath;
            }
        }

        if (is_file($path) === false) {
            $message = 'Le fichier à attacher %s n\'existe pas ou n\'est pas lisible ' . $fullPath ;
            throw new \RuntimeException(sprintf($message, $path));
        }

        parent::attachFileToField($field, $path);
    }
    
    /**
     * Surcharge de la méthode fixStepArgument pour
     *  - remplacer \\" par " (comme la méthode originale)
     *  - remplacer \\n par \n
     *
     * @param string $argument
     * @return string
     */
    public function fixStepArgument($argument)
    {
        $replacements = [
            '\\"' => '"',
            '\\\\n' => "\n"
        ];
        return str_replace(array_keys($replacements), array_values($replacements), $argument);
    }
}
