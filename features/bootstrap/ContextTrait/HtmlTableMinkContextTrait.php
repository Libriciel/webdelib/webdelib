<?php

namespace contextTrait;

/**
 * Trait permettant de travailler facilement avec des tableaux.
 */
trait HtmlTableMinkContextTrait
{
    /**
     * 
     * @param string $path
     * @return string
     */
    protected function getTableRowXpath($path)
    {
        $cells = explode('|', $path);
        $count = count($cells);
        $xpath = '';
        

        if (1 <= $count) {
             $xpath = '//td[contains(normalize-space(.),"'.normalize($cells[0]).'")]';
            if (2 <= $count) {
                for ($i = 1; $i < $count; $i++) {
                    $xpath .= '/following-sibling::td[contains(normalize-space(.),"'.normalize($cells[$i]).'")]';
                }
            }
        }

        return $xpath.'/parent::tr';
    }

    /**
     * 
     * @Then /^(?:|I )see the table row "(?P<path>[^"]*)"$/
     * @Then /^(?:|je )(devrais voir|vois) la ligne "(?P<path>[^"]*)"$/
     * 
     * @param string $button
     * @param string $path
     */
    public function iSeeTheTableRow($path)
    {
        $xpath = $this->getTableRowXpath($path);
        return $this->assertSession()->elementExists('xpath', $xpath);
    }

    /**
     * 
     * @Then /^(?:|I )don't see the table row "(?P<path>[^"]*)"$/
     * @Then /^(?:|je )ne (devrais pas voir|vois pas) la ligne "(?P<path>[^"]*)"$/
     * 
     * @param string $button
     * @param string $path
     */
    public function iDontSeeTheTableRow($path)
    {
        $xpath = $this->getTableRowXpath($path);
        $this->assertSession()->elementNotExists('xpath', $xpath);
    }
    

    /**
     * 
     * @param string $text
     * @param string $path
     * @return string
     */
    protected function getTableRowLinkXpath($text, $path)
    {
        $xpath = $this->getTableRowXpath($path);
        $xpaths = [
            $xpath.'//text()[normalize-space(.) = "'.normalize($text).'"]/parent::a',
            $xpath.'//a[normalize-space(@title) = "'.normalize($text).'"]',
            $xpath.'//button[normalize-space(@title) = "'.normalize($text).'"]'
        ];
        return implode("|", $xpaths);
    }

    /**
     * 
     * @Then /^(?:|I )see (the link ){0,1}"(?P<text>[^"]*)" in the table row "(?P<path>[^"]*)"$/
     * @Then /^(?:|je )vois (le lien ){0,1}"(?P<text>[^"]*)" dans la ligne "(?P<path>[^"]*)"$/
     * 
     * @param string $text
     * @param string $path
     */
    public function iSeeTheTableRowLink($text, $path)
    {
        $xpath = $this->getTableRowLinkXpath($text, $path);
        $this->assertSession()->elementExists('xpath', $xpath);
    }

    /**
     * 
     * @Then /^(?:|I )don't see (the link ){0,1}"(?P<text>[^"]*)" in the table row "(?P<path>[^"]*)"$/
     * @Then /^(?:|je )ne vois pas (le lien ){0,1}"(?P<text>[^"]*)" dans la ligne "(?P<path>[^"]*)"$/
     * 
     * @param string $text
     * @param string $path
     */
    public function iDontSeeTheTableRowLink($text, $path)
    {
        $xpath = $this->getTableRowLinkXpath($text, $path);
        $this->assertSession()->elementNotExists('xpath', $xpath);
    }

    /**
     * 
     * @Then /^(?:|I )click on (the link ){0,1}"(?P<text>[^"]*)" in the table row "(?P<path>[^"]*)"$/
     * @Then /^(?:|je )(clique sur (le lien ){0,1}|suis )"(?P<text>[^"]*)" dans la ligne "(?P<path>[^"]*)"$/
     * 
     * @param string $text
     * @param string $path
     */
    public function iClickOnTheTableRowLink($text, $path)
    {
        $xpath = $this->getTableRowLinkXpath($text, $path);

        $page = $this->getSession()->getPage();
        $link = $page->find('xpath', $xpath);
        $link->click();
    }
    
    /**
     * Checks checkbox with specified id|name|label|value
     * Example: When I check "Pearl Necklace" in the table row "Foo"
     * Example: And I check "Pearl Necklace" in the table row "Foo"
     *
     * @When /^(?:|I )check "(?P<option>(?:[^"]|\\")*)" in the table row "(?P<path>(?:[^"]|\\")*)"$/
     */
    public function checkOptionInTableRow($option, $path)
    {
        $xpath = $this->getTableRowXpath($path);
        $page = $this->getSession()->getPage();
        $row = $page->find('xpath', $xpath);

        $option = $this->fixStepArgument($option);
        $field = $row->findField($option);
        if ($field === null) {
            $field = $row->find('xpath', '//input[contains(normalize-space(@title), "'.normalize($option).'")]');
        }

        if (null === $field) {
            throw new ElementNotFoundException($this->getDriver(), 'form field', 'id|name|label|value|title', $option);
        }

        $field->check();
    }

    /**
     * Unchecks checkbox with specified id|name|label|value
     * Example: When I uncheck "Broadway Plays" in the table row "Foo"
     * Example: And I uncheck "Broadway Plays" in the table row "Foo"
     *
     * @When /^(?:|I )uncheck "(?P<option>(?:[^"]|\\")*)" in the table row "(?P<path>(?:[^"]|\\")*)"$/
     */
    public function uncheckOptionInTableRow($option, $path)
    {
        $xpath = $this->getTableRowXpath($path);
        $page = $this->getSession()->getPage();
        $row = $page->find('xpath', $xpath);

        $option = $this->fixStepArgument($option);
        $field = $row->findField($option);
        if ($field === null) {
            $field = $row->find('xpath', '//input[contains(normalize-space(@title), "'.normalize($option).'")]');
        }

        if (null === $field) {
            throw new ElementNotFoundException($this->getDriver(), 'form field', 'id|name|label|value|title', $option);
        }

        $field->uncheck();
    }
    
    protected function getFormPrefix($text)
    {
        $page = $this->getSession()->getPage();
        $parts = explode(">", $text);
        $max = count($parts) - 1;
        if ($max > 0) {
            $xpath = '';
            foreach($parts as $idx => $part) {
                $part = trim($part);

                if ($idx < $max) {
                    $xpath .= '//legend[contains(normalize-space(text()),"' . normalize($part) . '")]/parent::fieldset';
                }
            }
        } else {
            $xpath = '//body';
        }
        
        return $xpath;
    }
    
    protected function getFormSuffix($text)
    {
        $page = $this->getSession()->getPage();
        $parts = explode(">", $text);
        return trim($parts[count($parts)-1]);
    }

    /**
     * @Then /^(?:|I )check the "(?P<text>[^"]*)" radio button$/
     * @Then /^(?:|je )sélectionne le bouton radio "(?P<text>[^"]*)"$/
     * 
     * @param string $text
     * @throws \Exception
     */
    public function iCheckTheRadioButton($text)
    {
        $page = $this->getSession()->getPage();
        $radioButton = $page->find('named', ['radio', $text]);
        
        $prefix = $this->getFormPrefix($text);
        $suffix = $this->getFormSuffix($text);
        $parent = $page->find('xpath', $prefix);

        $radioButton = $parent->find('named', ['radio', $suffix]);

        if (null !== $radioButton) {
            $select = $radioButton->getAttribute('name');
            $option = $radioButton->getAttribute('value');
            $parent->selectFieldOption($select, $option);
            $radioButton->click();
        } else {
            throw new \Exception("Radio button with label {$text} not found");
        }
    }
    
    /**
     * @Then /^the radio button "(?P<radio>(?:[^"]|\\")*)" should be checked$/
     * @Then /^the radio button "(?P<radio>(?:[^"]|\\")*)" is checked$/
     * @Then /^the radio button "(?P<radio>(?:[^"]|\\")*)" (?:is|should be) checked$/
     * @Then /^le bouton radio "(?P<radio>(?:[^"]|\\")*)" (est|devrait être) sélectionné$/
     */
    public function assertRadioButtonChecked($radio)
    {
        $this->assertSession()->checkboxChecked($this->fixStepArgument($radio));
    }
    
    /**
     * @Then /^the radio button "(?P<radio>(?:[^"]|\\")*)" should not be checked$/
     * @Then /^the radio button "(?P<radio>(?:[^"]|\\")*)" is not checked$/
     * @Then /^the radio button "(?P<radio>(?:[^"]|\\")*)" (?:is|should be) not checked$/
     * @Then /^le bouton radio "(?P<radio>(?:[^"]|\\")*)" (n'est pas|ne devrait pas être) sélectionné$/
     */
    public function assertRadioButtonNotChecked($radio)
    {
        $this->assertSession()->checkboxNotChecked($this->fixStepArgument($radio));
    }

    /**
     * @link https://github.com/knpuniversity/sunshinephp-behat/tree/master/_tuts
     *
     * @Then /^(?:|I )(should ){0,1}see (?P<count>\d+) row(s){0,1} in the table$/
     */
    public function iShouldSeeRowsInTheTable($count)
    {
        $table = $this->getSession()->getPage()->find('css', 'table');
        if (null === $table) {
            throw new \InvalidArgumentException('Cannot find a table!');
        }

        $rows = $table->findAll('css', 'tbody tr');

        if ($count != count($rows)) {
            $msgstr = 'Table contains %d line(s) instead of the exected %d';
            throw new \InvalidArgumentException(sprintf($msgstr, count($rows), $count));
        }
    }

    /**
     * @Then /^(?:|I )(should ){0,1}see at position (?P<position>\d+) the table row "(?P<path>[^"]*)"$/
     * @Then /^(?:|je )(devrais voir|vois) en position (?P<position>\d+) la ligne "(?P<path>[^"]*)"$/
     */
    public function iShouldSeeAtPositionTheTableRow($position, $path)
    {
        $xpathRow = $this->getTableRowXpath($path);
        $xpathLine = "{$xpathRow}/parent::tbody/tr[{$position}]";
        $nodeRow = $this->assertSession()->elementExists('xpath', $xpathRow);
        $nodeLine = $this->assertSession()->elementExists('xpath', $xpathLine);

        if ($nodeRow != $nodeLine) {
            $msgstr = 'Table row "%s" (xpath "%s") not found at position %d';
            throw new \InvalidArgumentException(sprintf($msgstr, $path, $xpathRow, $position));
        }
    }
}
