<?php

namespace contextTrait;
use Behat\Mink\Exception\ElementNotFoundException;

trait ElementMoveMinkContextTrait
{

    /**
     * @param $xpath
     * @return string
     * @see \DMore\ChromeDriver\ChromeDriver::getXpathExpression
     */
    protected function driverGetXpathExpression($xpath)
    {
        $xpath = addslashes($xpath);
        $xpath = str_replace("\n", '\\n', $xpath);
        return "var xpath_result = document.evaluate(\"{$xpath}\", document, null, XPathResult.ORDERED_NODE_ITERATOR_TYPE);";
    }
    
    /**
     * @param $xpath
     * @return array
     * @see \DMore\ChromeDriver\ChromeDriver::getCoordinatesForXpath
     */
    protected function driverGetCoordinatesForXpath($xpath)
    {
        $expression = $this->driverGetXpathExpression($xpath);
        $expression .= <<<JS
    var element = xpath_result.iterateNext();
    rect = element.getBoundingClientRect();
    [rect.left, rect.top, rect.width, rect.height]
JS;

        list($left, $top, $width, $height) = $this->getSession()->getDriver()->evaluateScript($expression);
        return [ceil($left), ceil($top), floor($width), floor($height)];
    }

    /**
     * 
     * @param string $xpath
     * @param integer $x
     * @param integer $y
     * @throws ElementNotFoundException
     * @throws RuntimeException
     */
    protected function moveXpathElementBy($xpath, $x, $y)
    {
        $page = $this->getSession()->getPage();
        $element = $page->find('xpath', $xpath);

        if (null === $element) {
            throw new ElementNotFoundException($this->getSession(), 'draggable form element', 'xpath', $xpath);
        }

        list($left, $top) = $this->driverGetCoordinatesForXpath($xpath);
        $target = [
            'id' => 'drop-target-'.hash('md5', $xpath.$x.$y.microtime(true)),
            'left' => $left + $x,
            'top' => $top + $y
        ];
        
        $driver = $this->getSession()->getDriver();

        $script = <<<JS
return (function(window){
    try {
        var target = document.createElement('div'), style;
        if (target) {
            target.setAttribute('id', '{$target['id']}');
            style = 'width: 100px; height: 100px; position: absolute;top:{$target['top']}px;left: {$target['left']}px;background: red;';
            target.setAttribute('style', style);
            document.body.appendChild(target);
            return true;
        }
        return false;
    } catch(error) {
        return false;
    }
})(window);
JS;
        $result = $driver->evaluateScript($script);
        if ($result === false) {
            $message = sprintf('JavaScript returned false: %s', $script);
            throw new RuntimeException($message);
        }

        $driver->dragTo($xpath, "//div[@id='{$target['id']}']");
        
        $script = <<<JS
return (function(window){
    try {
        var target = document.getElementById('{$target['id']}');
        if (target) {
            document.body.removeChild(target);
            return true;
        }
        return false;
    } catch(error) {
        return false;
    }
})(window);
JS;
        $result = $driver->evaluateScript($script);
        if ($result === false) {
            $message = sprintf('JavaScript returned false: %s', $script);
            throw new RuntimeException($message);
        }
    }
    
    /**
     * 
     * @When /^(?:|I )move the form element "(?P<label>[^"]*)" by (?P<x>[\+\-]{0,1}[0-9]+) *, *(?P<y>[\+\-]{0,1}[0-9]+)$/
     * 
     * @param string $label
     * @param integer $x
     * @param integer $y
     */
    public function iMoveTheFormElementBy($label, $x, $y)
    {
        $xpath = sprintf("//div[@id='form-container']//div[contains(@class,'draggable')]//span[@class=\"labeler\"][text()=\"%s\"]/ancestor::div[contains(@class,\"ui-draggable-handle\")]", $label);
        $this->moveXpathElementBy($xpath, $x, $y);
    }
    
    /**
     * 
     * @When /^(?:|I )move the category title "(?P<title>[^"]*)" by (?P<x>[\+\-]{0,1}[0-9]+) *, *(?P<y>[\+\-]{0,1}[0-9]+)$/
     * 
     * @param string $title
     * @param integer $x
     * @param integer $y
     */
    public function iMoveTheCategoryTitleBy($title, $x, $y)
    {
        $xpath = sprintf("//div[@id='form-container']//div[contains(@class,'draggable')]//h1[text()=\"%s\"]/ancestor::div[contains(@class,\"ui-draggable-handle\")]", $title);
        $this->moveXpathElementBy($xpath, $x, $y);
    }
    
    /**
     * 
     * @When /^(?:|I )move the information field "(?P<text>[^"]*)" by (?P<x>[\+\-]{0,1}[0-9]+) *, *(?P<y>[\+\-]{0,1}[0-9]+)$/
     * 
     * @param string $text
     * @param integer $x
     * @param integer $y
     */
    public function iMoveTheInformationFieldBy($text, $x, $y)
    {
        $xpath = sprintf("//div[@id='form-container']//div[contains(@class,'draggable')]//div[text()=\"%s\"]/ancestor::div[contains(@class,\"ui-draggable-handle\")]", $text);
        $this->moveXpathElementBy($xpath, $x, $y);
    }
    
    /**
     * 
     * @When /^(?:|I )move the label "(?P<label>[^"]*)" by (?P<x>[\+\-]{0,1}[0-9]+) *, *(?P<y>[\+\-]{0,1}[0-9]+)$/
     * 
     * @param string $label
     * @param integer $x
     * @param integer $y
     */
    public function iMoveTheLabelBy($label, $x, $y)
    {
        $xpath = sprintf("//div[@id='form-container']//div[contains(@class,'draggable')]//h5[text()=\"%s\"]/ancestor::div[contains(@class,\"ui-draggable-handle\")]", $label);
        $this->moveXpathElementBy($xpath, $x, $y);
    }
}
