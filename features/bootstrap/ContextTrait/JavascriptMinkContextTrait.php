<?php

namespace contextTrait;

/**
 * Trait nécessitant javascript et permettant de répondre à des boîtes de
 * confirmation et d'attendre un certain temps.
 */
trait JavascriptMinkContextTrait
{   
    /**
     * 
     * @param string $message
     * @param bool $accept
     */
    protected function setNextConfirmationAcceptance($message, $accept) {
        $escaped = json_encode($message);
        $accept = json_encode((bool)$accept);

        $script = <<<JS
        (function(window){
            var confirm_bak = window.confirm,
                escaped = $escaped;
            window.confirm = function(message) {
                if(message.localeCompare(escaped) === 0) {
                    window.confirm = confirm_bak;
                    return $accept;
                }
            };
        })(window);
JS;
        $this->getSession()->getDriver()->executeScript($script);
    }

    /**
     * 
     * @Then /^(?:|j')accepterai la question "(?P<message>[^"]*)"$/
     * 
     * @param string $message
     */
    public function iWillAcceptNextConfirmation($message) {
        $this->setNextConfirmationAcceptance($message, true);
    }

    /**
     * 
     * @Then /^(?:|je )n'accepterai pas la question "(?P<message>[^"]*)"$/
     * 
     * @param string $message
     */
    public function iWillNotAcceptNextConfirmation($message) {
        $this->setNextConfirmationAcceptance($message, false);
    }

    /**
     * 
     * @Then /^(?:|I )wait (for ){0,1}(?P<time>[0-9]+) (?P<unit>(milli){0,1}seconds{0,1})$/
     * @Then /^(?:|j')attends (pendant ){0,1}(?P<time>[0-9]+) (?P<unit>(milli){0,1}secondes{0,1})$/
     * 
     * @param int $time
     * @param string $unit
     */
    public function iWaitFor($time, $unit)
    {
        if(0 === preg_match('/^milli/', $unit)) {
            $time *= 1000;
        }
        $this->getSession()->wait($time);
    }

    /*protected $nextMicrosecondsWait = null;
    
    //@AfterStep
    public function afterStepIWaitFor($scope)
    {
        if(null !== $this->nextMicrosecondsWait) {
            usleep($this->nextMicrosecondsWait);
            $this->nextMicrosecondsWait = null;
        }
    }

    public function iWaitFor($amount, $unit)
    {
        $matches = [];
        if(preg_match('/^(milli|micro){0,1}/', $unit, $matches)) {
            if('milli' === $matches[0]) {
                $amount *= 1000;
            } elseif('' === $matches[0]) {
                $amount *= 1000000;
            }
        }
//        $this->nextMicrosecondsWait = $amount;
        usleep($amount);
    }*/
}
