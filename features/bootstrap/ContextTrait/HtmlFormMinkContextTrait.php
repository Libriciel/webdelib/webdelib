<?php
namespace contextTrait;

use Behat\Mink\Exception\ElementNotFoundException;
use Behat\Mink\Exception\ExpectationException;

/**
 * Trait ajoutant des fonctionnalités concernant les champs de formulaire.
 */
trait HtmlFormMinkContextTrait
{
    /**
     * @see {@url https://github.com/Aplyca/BehatContexts}
     * 
     * @When /^the option "(?P<option>(?:[^"]|\\")*)" should be selected from "(?P<select>(?:[^"]|\\")*)"$/
     *
     * @param string $select
     * @param string $option
     * @throws ElementNotFoundException
     * @throws ExpectationException
     */
    public function assertSelectedOption($select, $option)
    {
        $page = $this->getSession()->getPage();
        $select = $this->fixStepArgument($select);
        $option = normalize($this->fixStepArgument($option));

        $field = $page->findField($select);
        if (null === $field) {
            throw new ElementNotFoundException($this->getSession(), 'select field', 'id|name|label|value', $select);
        }
        
        $found = $field->find('named', array('option', $option));
        if (null === $found) {
            throw new ElementNotFoundException($this->getSession(), 'select option field', 'id|name|label|value', $option);
        }
        
        if ($found->isSelected() !== true) {
            $message = 'Select option field with value|text "'.$option.'" is not selected in the select "'.$select.'"';
            throw new ExpectationException($message, $this->getSession());
        }
    }

    /**
     * @When /^the "(?P<field>(?:[^"]|\\")*)" static field should contain "(?P<value>(?:[^"]|\\")*)"$/
     */
    public function theStaticFieldShouldContain($field, $value)
    {
        $page = $this->getSession()->getPage();
        $labelXpath = '//*[contains(@class, "input")][contains(@class, "staticctrl")]//*[contains(@class, "control-label")][contains(normalize-space(.),"'.normalize($field).'")]';
        $label = $this->assertSession()->elementExists('xpath', $labelXpath);
        $valueXpath = $labelXpath.'/parent::*[contains(@class, "input")][contains(@class, "staticctrl")]//*[contains(@class, "form-control-static")][contains(normalize-space(.),"'.normalize($value).'")]';
        $this->assertSession()->elementExists('xpath', $valueXpath);
    }
}
