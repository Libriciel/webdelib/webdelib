<?php

namespace contextTrait;

/**
 * Trait permettant d'ajouter la prise de photos d'écran à un MinkContext après
 * chacune des étapes.
 * Les images seront dans des sous-répertoires situés dans out/screenshots.
 */
trait AfterStepScreenshotMinkContextTrait
{
    protected $beforeScenarioScope = null;
    /**
     * @BeforeScenario
     *
     * @param Behat\Behat\Hook\Scope\BeforeScenarioScope $scope
     *
     */
    public function setUpTestEnvironment($scope)
    {
        $this->beforeScenarioScope = $scope->getScenario();
    }
    
    protected function getCurrentWindowSize()
    {
        $js = 'return {width: jQuery(window).width(), height: jQuery(window).height()}';
        return $this->getSession()->evaluateScript($js);        
    }
    
    protected function getMaxWindowSize()
    {
        $js = 'return {width: jQuery("html").width(), height: jQuery("html").height()}';
        return $this->getSession()->evaluateScript($js);        
    }
    
    /**
     * @AfterStep
     *
     * @param Behat\Behat\Hook\Scope\AfterStepScope $scope
     */
    public function afterStep($scope)
    {
        //$current = $this->getCurrentWindowSize();
        //$max = $this->getMaxWindowSize();

        //$this->getSession()->resizeWindow($max['width'], $max['height']);
        
        if (get_class($this->beforeScenarioScope) === 'Behat\\Gherkin\\Node\\ExampleNode') {
            $scenario = sprintf(
                "%s - ligne %04d",
                $this->beforeScenarioScope->getOutlineTitle(),
                $this->beforeScenarioScope->getLine()
            );
        } else {
            $scenario = $this->beforeScenarioScope->getTitle();
        }

        $feature = preg_replace('/[\s\.]/', '_', $scope->getFeature()->getTitle());
        $scenario = preg_replace('/[\s\.]/', '_', $scenario);
        $filepath = ROOT."/out/screenshots/{$feature}/{$scenario}";
        if (false === file_exists($filepath)) {
            mkdir($filepath, 0777, true);
        }

        $line = $scope->getStep()->getLine();
        $filename = sprintf("line_%04d.png", $line);
        $this->saveScreenshot($filename, $filepath);
        
        //$this->getSession()->resizeWindow($current['width'], $current['height']);
    }
}
