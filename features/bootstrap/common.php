<?php
if(false === defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

if(false === defined('ROOT')) {
    define('ROOT', dirname(__FILE__).DS.'..'.DS.'..');
}

function normalize($text)
{
    return str_replace('"', "&quot;", normalize_space($text));
}

function normalize_space($text)
{
    return preg_replace('/\s+/', ' ', trim($text));
}

// @see CakePHP 2.9.8
if (!function_exists('debug')) {
	function debug($var, $showHtml = null, $showFrom = true) {
		$file = '';
		$line = '';
		$lineInfo = '';
		if ($showFrom) {
			$trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 1);
			$file = str_replace(array(ROOT), '', $trace[0]['file']);
			$line = $trace[0]['line'];
		}
		$html = <<<HTML
<div class="cake-debug-output">
%s
<pre class="cake-debug">
%s
</pre>
</div>
HTML;
		$text = <<<TEXT
%s
########## DEBUG ##########
%s
###########################

TEXT;
		$template = $html;
		if (PHP_SAPI === 'cli' || $showHtml === false) {
			$template = $text;
			if ($showFrom) {
				$lineInfo = sprintf('%s (line %s)', $file, $line);
			}
		}
		if ($showHtml === null && $template !== $text) {
			$showHtml = true;
		}
		$var = var_export($var, true);
		if ($showHtml) {
			$template = $html;
			$var = htmlspecialchars($var, ENT_QUOTES);
//			if ($showFrom) {
//				$lineInfo = sprintf('<span><strong>%s</strong> (line <strong>%s</strong>)</span>', $file, $line);
//			}
		}
		printf($template, $lineInfo, $var);
	}
}
