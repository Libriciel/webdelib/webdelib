<?php
use Behat\Behat\Context\ClosuredContextInterface,
    Behat\Behat\Context\TranslatedContextInterface,
    Behat\Behat\Context\BehatContext,
    Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;

use Behat\MinkExtension\Context\MinkContext;

use Behat\Behat\Context\TranslatableContext;
use Behat\Mink\Exception\DriverException;
use contextTrait\OverloadedMinkContextTrait;
use contextTrait\AfterStepScreenshotMinkContextTrait;
use contextTrait\BootstrapMinkContextTrait;
use contextTrait\CakeErrorMinkContextTrait;
use contextTrait\ExtraClickSelectorsMinkContextTrait;
use contextTrait\HtmlFormMinkContextTrait;
use contextTrait\HtmlTableMinkContextTrait;
use contextTrait\HtmlTitleAndTextMinkContextTrait;
use contextTrait\JavascriptMinkContextTrait;

require_once 'common.php';

/**
 * Features context.
 */
class FeatureContext extends MinkContext
{
    use AfterStepScreenshotMinkContextTrait;
    use BootstrapMinkContextTrait;
//    use CakeErrorMinkContextTrait;
    use ExtraClickSelectorsMinkContextTrait;
    use HtmlFormMinkContextTrait;
    use HtmlTableMinkContextTrait;
    use HtmlTitleAndTextMinkContextTrait;
    use JavascriptMinkContextTrait;
    use OverloadedMinkContextTrait;

    private array $passwords = [];

    protected $pages = [
        'la page de connexion' => '/'
    ];

    /**
     * Permet de récupérer les traductions se trouvant dans le dossier i18n.
     *
     * @return array
     */
    public static function getTranslationResources()
    {
        return array_merge(
            self::getMinkTranslationResources(),
            glob(__DIR__ . './../i18n/*.xliff')
        );
    }

    public function setUsers()
    {
        $this->passwords = [
            'admin' => getenv('APP_ADMIN_PASSWORD'),
            'adrien' => getenv('APP_ADMIN_PASSWORD').'adrien',
            'amelie' => getenv('APP_ADMIN_PASSWORD').'amelie',
            'arnaud' => getenv('APP_ADMIN_PASSWORD').'arnaud',
            'axel' => getenv('APP_ADMIN_PASSWORD').'axel',
            'camille' => getenv('APP_ADMIN_PASSWORD').'camille',
            'celine' => getenv('APP_ADMIN_PASSWORD').'celine',
            'david' => getenv('APP_ADMIN_PASSWORD').'david',
            'emma' => getenv('APP_ADMIN_PASSWORD').'emma',
            'eric' => getenv('APP_ADMIN_PASSWORD').'eric',
            'franck' => getenv('APP_ADMIN_PASSWORD').'franck',
            'ines' => getenv('APP_ADMIN_PASSWORD').'ines',
            'julien' => getenv('APP_ADMIN_PASSWORD').'julien',
            'lukas' => getenv('APP_ADMIN_PASSWORD').'lukas',
            'mathias' => getenv('APP_ADMIN_PASSWORD').'mathias',
            'mickael' => getenv('APP_ADMIN_PASSWORD').'mickael',
            'nathalie' => getenv('APP_ADMIN_PASSWORD').'nathalie',
            'patricia' => getenv('APP_ADMIN_PASSWORD').'patricia',
            'pierre' => getenv('APP_ADMIN_PASSWORD').'pierre',
            'remi' => getenv('APP_ADMIN_PASSWORD').'remi',
            'romain' => getenv('APP_ADMIN_PASSWORD').'romain',
            'sebastien' => getenv('APP_ADMIN_PASSWORD').'sebastien'
        ];
    }
    /**
     *
     * @param string $page
     */
    public function visit($page)
    {
        if (true === isset($this->pages[$page])) {
            $page = $this->pages[$page];
        }
        $this->visitPath($page);
    }

    /**
     *
     * @When /^(?:|I )try to connect with "(?P<username>[^"]*)" "(?P<password>[^"]*)"$/
     * @When /^(?:|j')essaie de me connecter avec "(?P<username>[^"]*)" "(?P<password>[^"]*)"$/
     *
     * @param string $username
     * @param string $password
     */
    public function iTryToConnectWith($username, $password)
    {
//        $this->getSession()->getDriver()->setTimeouts([
//            'script' => 10000, 'implicit' => 30000, 'page' => 3000]);
        $this->visit('/');
        $this->iMaximizeTheWindow();
        $this->assertPageContainsText('Veuillez saisir vos identifiants de connexion');
        $this->fillField('data[User][username]', $username);
        $this->fillField('data[User][password]', $password);
        $this->iClickOnTheElementWithCssSelectorUsingJavaScript('button.btn.btn-primary[type="submit"]');
    }

    /**
     *
     * @When /^(?:|I )am connected with "(?P<username>[^"]*)" "(?P<password>[^"]*)"$/
     * @When /^(?:|je )suis connecté avec "(?P<username>[^"]*)" "(?P<password>[^"]*)"$/
     *
     * @param string $username
     * @param string $password
     */
    public function iAmConnectedWith($username, $password)
    {
        $this->iTryToConnectWith($username, $password);
        $this->assertPageHasHeading('Accueil');
    }

    /**
     *
     * @param string $username
     * @return string
     */
    protected function password($username)
    {
        if (true === isset($this->passwords[$username])) {
            return $this->passwords[$username];
        }

        return null;
    }

    /**
     *
     * @When /^(?:|I )try to connect with the user "(?P<username>[^"]*)"$/
     * @When /^(?:|j')essaie de me connecter avec l'utilisateur "(?P<username>[^"]*)"$/
     *
     * @param string $username
     */
    public function iTryToConnectWithUser($username)
    {
        $this->iTryToConnectWith($username, $this->password($username));
    }

    /**
     *
     * @When /^(?:|I )am connected with the user "(?P<username>[^"]*)"$/
     * @When /^(?:|je )suis connecté avec l'utilisateur "(?P<username>[^"]*)"$/
     *
     * @param string $username
     */
    public function iAmConnectedWithUser($username)
    {
        $this->iAmConnectedWith($username, $this->password($username));
    }

    /**
     *
     * @info Seuls les premiers et derniers niveaux sont cliquables.
     *
     * @When /^(?:|I )go into the menu "(?P<path>[^"]*)"$/
     * @When /^(?:|je )vais dans le menu "(?P<path>[^"]*)"$/
     */
    public function iGoIntoTheMenu($path)
    {
        //$this->checkNotificationUser();
        $this->accessBootstrapMenuElement($path, true);
    }

    /**
     * @When /^(?:|I )should get the error "(?P<error>(?:[^"]|\\")*)" for the field "(?P<field>(?:[^"]|\\")*)"$/
     *
     */
    public function assertFieldHasError($field, $error)
    {
        $selectorsHandler = $this->getSession()->getSelectorsHandler();
        $page = $this->getSession()->getPage();

        // Recherche du champ
        $selector = array('field', $this->fixStepArgument($field));
        $xpath = $selectorsHandler->selectorToXpath('named_exact', $selector);

        $node = $page->find('xpath', $xpath);
        if (null === $node) {
            $xpath = $selectorsHandler->selectorToXpath('named_partial', $selector);
        }
        $node = $this->assertSession()->elementExists('xpath', $xpath);

        // Recherche de messages d'erreur pour ce champ
        $xpath = '('.$xpath.')/ancestor::div[contains(@class, "form-group")]/following-sibling::div[contains(@class, "error-message")]';
        $node = $this->assertSession()->elementExists('xpath', $xpath);
        if (null !== $node && false === $node->isVisible()) {
            $message = 'Error message for field "%s" was found but is not visible; xpath used: %s';
            throw new \InvalidArgumentException(sprintf($message, $field, $xpath));
        }

        // Recherche du message d'erreur demandé pour ce champ
        $xpath .= '[contains(normalize-space(text()), "'.normalize($error).'")]';
        $node = $this->assertSession()->elementExists('xpath', $xpath);
        if (null !== $node && false === $node->isVisible()) {
            $message = 'Error message "%s" for field "%s" was found but is not visible; xpath used: %s';
            throw new \InvalidArgumentException(sprintf($message, $error, $field, $xpath));
        }
    }

    /**
     * @Then je devrais voir le titre :arg1
     */
    public function jeDevraisVoirLeTitre($arg1)
    {
        $this->assertPageHasHeading($arg1);
    }

    /**
     * @Then je devrais voir la ligne de texte :arg1
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function jeDevraisVoirLaLigneDeTexte($arg1)
    {
        $this->assertSession()
            ->elementExists(
                'xpath',
                '//li[contains(@class, "jstree-node")]//a[contains(text(), "'.$arg1.'")]'
            );
    }

    /**
     * @Then je devrais voir le message :arg1
     */
    public function jeDevraisVoirLeMessage($arg1)
    {
        $this->assertPageContainsText($arg1);

        // Attendre que l'élément bloquant disparaisse
        $this->getSession()->wait(1000, "document.querySelector('.alert-info.growl-animated') === null");
    }


    /**
     * @BeforeScenario
     */
    public function setUp()
    {
        $this->setUsers();
    }

    /**
     * @Given I maximize the window
     */
    public function iMaximizeTheWindow()
    {
        try {
            $this->getSession()->maximizeWindow();
        } catch (DriverException $e) {
            // Handle the exception if maximizing the window fails
            echo "Failed to maximize the window: " . $e->getMessage();
        }
    }

    /**
     * @When I click on the element with CSS selector :cssSelector using JavaScript
     */
    public function iClickOnTheElementWithCssSelectorUsingJavaScript($cssSelector)
    {
        $session = $this->getSession();
        $page = $session->getPage();
        $element = $page->find('css', $cssSelector);

        if (null === $element) {
            throw new NotFoundException($this->getSession(), 'element', 'css', $cssSelector);
        }

        // Click the element using JavaScript
        $session->executeScript("document.querySelector('$cssSelector').click();");
    }

    /**
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    private function checkNotificationUser(): void
    {
        $searchModelNotification = $this->assertSession()
            ->elementExists(
                'xpath',
                '//div[contains(@id, "notification-user")]'
            );
        if (null !== $searchModelNotification && false !== $searchModelNotification->isVisible()) {
            echo $searchModelNotification->getHtml();
            $searchModelNotification->pressButton('Enregister');
        }
    }

    /**
     * @When /^je remplis "([^"]*)" avec "([^"]*)" si la valeur n'est pas vide$/
     */
    public function jeRemplisAvec($champ, $valeur)
    {
        if (!empty($valeur)) {
            $this->fillField($champ, $valeur);
        }
    }

    /**
     * @Given /^je suis sur la première page$/
     */
    public function jeSuisSurLaPremierePage()
    {
        if ($this->isFirstPageLinkPresent()) {
            $currentPage = $this->getCurrentPageNumber();
            if ($currentPage !== 1) {
                $firstPageLink = $this->getSession()->getPage()->find('css', '.pagination li:first-child a');
                if ($firstPageLink!==null) {
                    $firstPageLink->click();
                    $this->getSession()->wait(1000, "document.readyState === 'complete'");
                }
            }
        }
    }

    /**
     * @When je cherche et je clique sur :action dans la ligne :path
     */
    public function jeChercheEtJeCliqueSurDansLaLigne($action, $path)
    {
        $xpath = $this->getTableRowXpathV2($path);
        $element = $this->getSession()->getPage()->find('xpath', $xpath);

        while (!$element && $this->isNextPageLinkPresent()) {
            $this->clickNextPageLink();
            // Attendre que la page se charge complètement
            $this->getSession()->wait(1000, "document.readyState === 'complete'");
            // Vérifiez à nouveau la présence de l'élément
            $element = $this->getSession()->getPage()->find('xpath', $xpath);
        }

        if (null === $element) {
            throw new \Exception("La ligne de tableau avec les valeurs '$path' n'a pas été trouvée.");
        }

        // Chercher le lien ou bouton spécifique dans la ligne trouvée et cliquer dessus
        $link = $element->find('xpath', sprintf('.//a[contains(@title, "%s")]', $action));
        if (null === $link) {
            throw new \Exception("Le lien ou bouton '$action' dans la ligne de tableau avec les valeurs'.
            ''$path' n'a pas été trouvé.");
        }

        $link->click();
    }

    /**
     * @Then je cherche et je vois la ligne :path
     */
    public function jeChercheEtJeVoisLaLigne($path)
    {
        $xpath = $this->getTableRowXpath($path);
        $element = $this->getSession()->getPage()->find('xpath', $xpath);

        while (!$element && $this->isNextPageLinkPresent()) {
            $this->clickNextPageLink();
            // Attendre que la page se charge complètement
            $this->getSession()->wait(1000, "document.readyState === 'complete'");
            // Vérifiez à nouveau la présence de l'élément
            $xpath = $this->getTableRowXpath($path); // Régénérer le XPath après changement de page
            $element = $this->getSession()->getPage()->find('xpath', $xpath);
        }

        if (null === $element) {
            throw new \Exception("La ligne de tableau avec les valeurs '$path' n'a pas été trouvée.");
        }
    }

    protected function getTableRowXpathV2($path)
    {
        $cells = explode('|', $path);
        $cells = array_map('trim', $cells); // Trim des espaces blancs autour des valeurs
        $count = count($cells);

        if ($count < 1) {
            throw new \InvalidArgumentException("Les valeurs des cellules ne peuvent pas être vides.");
        }

        $xpath = '//tr[td[contains(normalize-space(.), "' . htmlspecialchars($this->normalize($cells[0])) . '")';

        for ($i = 1; $i < $count; $i++) {
            $xpath .= ' and following-sibling::td[contains(normalize-space(.), "'
                . htmlspecialchars($this->normalize($cells[$i])) . '")]';
        }

        $xpath .= ']]';

        return $xpath;
    }

    protected function normalize($string)
    {
        return trim(preg_replace('/\s+/', ' ', $string));
    }

    private function isFirstPageLinkPresent()
    {
        // Adapté selon votre implémentation réelle de la pagination
        $firstPageLink = $this->getSession()->getPage()->find('css', '.pagination li:first-child a');
        return $firstPageLink !== null;
    }

    private function getCurrentPageNumber()
    {
        // Adapter cette méthode selon votre logique de récupération du numéro de page actuelle
        $currentPageElement = $this->getSession()->getPage()->find('css', '.pagination li.active');
        if ($currentPageElement) {
            return (int)$currentPageElement->getText();
        }
        return 0; // Retourner 0 ou une autre valeur appropriée si la page actuelle n'est pas trouvée
    }

    /**
     * Vérifie si le lien de la page suivante est présent
     */
    private function isNextPageLinkPresent()
    {
        $nextPageLink = $this->getSession()->getPage()->find('css', '.pagination li.active + li a');
        return $nextPageLink !== null;
    }

    /**
     * Clique sur le lien du numéro de page suivant
     */
    private function clickNextPageLink()
    {
        $nextPageLink = $this->getSession()->getPage()->find('css', '.pagination li.active + li a');
        if ($nextPageLink) {
            $nextPageLink->click();
        } else {
            throw new \Exception("Lien de la page suivante non trouvé.");
        }
    }
}
