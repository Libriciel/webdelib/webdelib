# language: fr
@webdelib @recette @paramétrage @javascript
Fonctionnalité: 010 - Paramétrage des utilisateurs

Contexte:
    Etant donné que je suis connecté avec l'utilisateur "admin"
        Et que je vais dans le menu "Administration > Utilisateurs > Utilisateurs"
    Alors je devrais voir un titre contenant "Liste des utilisateurs"

Plan du Scénario:  001 - Modification des utilisateurs
    Lorsque je suis "<Login>"
    Et que je suis "Modifier"
    Alors je devrais voir le titre "Modifier l'utilisateur : <Nom> <Prénom> "
    Et que je sélectionne "<Profil>" depuis "Profil utilisateur"
    Et je suis dans l'onglet "Types d'actes"
    Et que je coche "Type d'acte"
    Et je suis dans l'onglet "Typeseance"
    Et que je coche "Types de seances"
    Et je suis dans l'onglet "Circuit & Services"
    Et que je coche "<Service>"
    Et que je presse "Enregistrer"
    Alors je devrais voir "Le profil a été modifié"
Exemples:
    | Login                      | Nom                 | Prénom                 | Profil                     | Service         |
    | adminFonctionel1Username   | adminFonctionel1Nom | adminFonctionel1Prenom | Administrateur fonctionnel | Service voirie  |
    | assemblee1Username         | assemblee1Nom       | assemblee1Prenom       | Service assemblées         | Service voirie  |
    | redacteur1Username         | redacteur1Nom       | redacteur1Prenom       | Rédacteurs                 | Service voirie  |
    | validateur1Username        | validateur1Nom      | validateur1Prenom      | Validateurs                | Service voirie  |

#Plan du Scénario: 001 - Ajout d'utilisateurs "simples"
#    Lorsque je suis "Ajouter un utilisateur"
#        Alors je devrais voir le titre "Ajouter un utilisateur"
#        Et je suis dans l'onglet "Informations principales"
#    Lorsque je remplis "Login" avec "<Login>"
#        Et que je remplis "Mot de passe" avec "<Mot de passe>"
#        Et que je remplis "Confirmez le mot de passe" avec "<Mot de passe>"
#        Et que je remplis "Nom" avec "<Nom>"
#        Et que je remplis "Prénom" avec "<Prénom>"
#        Et que je sélectionne "<Profil>" depuis "Profil utilisateur"
#        Et que je clique sur l'onglet "Types d'actes"
#        Et que je coche "types_actes"
#        Et que je décoche "Décisions"
#        Et que je clique sur l'onglet "Notifications"
#        Et que je clique sur l'onglet "Circuit & Services"
#        Et que je remplis "Filtrer par nom de service" avec "<Service>"
#        Et que je presse "Rechercher un service"
#        Et que je suis "<Service>"
#        Et que je presse "Enregistrer"
#    Alors je devrais voir "L'utilisateur \"<Login>\" a été modifié"
#        Et je vois la ligne "<Login> | <Nom> | <Prénom> | <Service>"
#Exemples:
#    | Login    | Mot de passe | Nom        | Prénom   | Profil     | Service                              |
#    | adrien   | adrien       | BRICCO     | Adrien   | Rédacteurs | Direction des Sports                 |
#    | arnaud   | arnaud       | AUZOLAT    | Arnaud   | Valideurs  | Direction de l'Education             |
#    | emma     | emma         | DURAND     | Emma     | Service Assemblées | Direction générale des services      |
#    | eric     | eric         | POMMATEAU  | Eric     | Validateur  | Services techniques municipaux       |
#    | franck   | franck       | MEIGNEN    | Franck   | Rédacteurs | Service voirie                       |