# language: fr
@webdelib @recette @paramétrage @javascript
Fonctionnalité: 005 - Paramétrage des compteurs

Contexte:
    Etant donné que je suis connecté avec l'utilisateur "admin"
        Et que je vais dans le menu "Administration > Générale > Compteurs"
    Alors je devrais voir le titre "Liste des compteurs"

Plan du Scénario: 001 - Ajout de compteurs
    Lorsque je suis "Ajouter un compteur"
    Alors je devrais voir le titre "Ajouter un compteur"
    Lorsque je remplis "Nom" avec "<Nom>"
        Et que je remplis "Commentaire" avec "<Commentaire>"
        Et que je remplis "CompteurDefCompteur" avec "<Définition>"
        Et que je remplis "Critère de réinitialisation" avec "<Critère>"
        Et que je sélectionne "<Séquence>" depuis "CompteurSequenceId"
        Et que je presse "Enregistrer"
    Alors je devrais voir "Le compteur \"<Nom>\" a été ajouté"
    Alors je devrais voir la ligne "<Nom> | <Commentaire> | <Séquence> : 0"
Exemples:
    | Nom                    | Commentaire | Définition           | Critère | Séquence      |
    | Délibérations          |             | #AAAA#_#0000#        | #AAAA#  | Délibérations |
    | Arrêtés Réglementaires |             | A#AAAA#_#0000#       | #AAAA#  | Arrêtés       |
    | Décisions              |             | DC#AA##0000#         | #AAAA#  | Décisions     |

Scénario: 002 - Modification du compteur "Délibérations"
    Lorsque je suis "Modifier" dans la ligne "Délibérations | #AAAA#_#0000# | #AAAA# | Délibérations : 0"
    Et que je remplis "Nom" avec "Délibérations"
    Et que je remplis "CompteurDefCompteur" avec "D#AAAA##MM##JJ#_#000#"
    Et que je remplis "Critère de réinitialisation" avec "#AAAA#"
    Et que je sélectionne "Délibérations" depuis "CompteurSequenceId"
    Et que je presse "Enregistrer"
    Alors je devrais voir "Le compteur \"Délibérations\" a été modifié"
    Alors je devrais voir la ligne "Délibérations | | D#AAAA##MM##JJ#_#000# | #AAAA# | Délibérations : 0"