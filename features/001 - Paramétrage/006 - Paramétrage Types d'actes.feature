# language: fr
@webdelib @recette @paramétrage @javascript
Fonctionnalité: 006 - Paramétrage des types d'actes

Contexte:
    Etant donné que je suis connecté avec l'utilisateur "admin"
        Et que je vais dans le menu "Administration > Générale > Types d'actes"
    Alors je devrais voir le titre "Liste des types d'acte"

    Plan du scénario: 001 - Ajout du type d'acte "Délibération"
        Lorsque je suis "Ajouter un type d'acte"
        Alors je devrais voir le titre "Ajouter un type d'acte"
        Lorsque je remplis "Libellé" avec "<Nom>"
        Et que je sélectionne "<Compteur>" depuis "TypeacteCompteurId"
        Et que je sélectionne "<Nature>" depuis "TypeacteNatureId"
        Et que je sélectionne le bouton radio "<Télétransmissible>"
        Et que je sélectionne le bouton radio "<Délibérant>"
        Et que je sélectionne le bouton radio "<Publiable>"
        Et que je sélectionne le bouton radio "<Archivable>"
        Et que je sélectionne "<ModeleProjet>" depuis "TypeacteModeleProjetId"
        Et que je sélectionne "<ModeleFinal>" depuis "TypeacteModeleFinalId"
        Et que je coche "TypeacteGabaritProjetActive"
        Et que j'attache le fichier "<FichierProjet>" à "TypeacteGabaritProjetUpload"
        Et que je coche "TypeacteGabaritSyntheseActive"
        Et que j'attache le fichier "<FichierSynthese>" à "TypeacteGabaritSyntheseUpload"
        Et que je coche "TypeacteGabaritActeActive"
        Et que j'attache le fichier "<FichierActe>" à "TypeacteGabaritActeUpload"
        Et que je presse "Enregistrer"
        Alors je devrais voir "Le type d'acte ' <Nom> ' a été sauvegardé"
        Alors je devrais voir la ligne "<Nom> | <Compteur> | <Nature>"
        Exemples:
            | Nom          | Compteur       | Nature       | ModeleProjet | ModeleFinal  | Télétransmissible        | Délibérant | Publiable | Archivable | FichierProjet           | FichierSynthese         | FichierActe             |
            | Délibération | Délibérations   | Délibérations | Projet       | Délibération | TypeacteTeletransmettre1 | TypeacteDeliberant1        | TypeactePublier1       | TypeacteVerserSae1        | types_actes/gabarit.odt | types_actes/gabarit.odt | types_actes/gabarit.odt |
            | Arrêté télétransmissible | Arrêtés Réglementaires   | Arrêtés Réglementaires | Projet       | Arrêté | TypeacteTeletransmettre1 | TypeacteDeliberant0        | TypeactePublier1       | TypeacteVerserSae1        | types_actes/gabarit.odt | types_actes/gabarit.odt | types_actes/gabarit.odt |
            | Arrêté non transmissible | Arrêtés Réglementaires   | Arrêtés Réglementaires | Projet       | Arrêté | TypeacteTeletransmettre0 | TypeacteDeliberant0        | TypeactePublier1       | TypeacteVerserSae1        | types_actes/gabarit.odt | types_actes/gabarit.odt | types_actes/gabarit.odt |
            | Décision | Délibérations   | Délibérations | Projet       | Délibération | TypeacteTeletransmettre1 | TypeacteDeliberant1        | TypeactePublier1       | TypeacteVerserSae1        | types_actes/gabarit.odt | types_actes/gabarit.odt | types_actes/gabarit.odt |


#    Scénario: 005 - Modification du type d'acte "Délibération"
#        Lorsque je suis "Modifier" dans la ligne "Délibération | Compteur Délibérations"
#        Alors je devrais voir le titre "Délibération"
#        Lorsque je remplis "Libellé" avec "Délibération"
#        Et que je sélectionne "Compteur Délibérations" depuis "TypeacteCompteurId"
#        Et que je sélectionne "Délibérations" depuis "TypeacteNatureId"
#        Et que je coche "Télétransmettre"
#        Et que je sélectionne "Projet" depuis "TypeacteModeleProjetId"
#        Et que je sélectionne "Délibération" depuis "TypeacteModeleFinalId"
#        Et que je coche "TypeacteGabaritProjetActive"
#        Et que j'attache le fichier "types_actes/gabarit.odt" à "TypeacteGabaritProjetUpload"
#        Et que je coche "TypeacteGabaritSyntheseActive"
#        Et que je coche "TypeacteGabaritActeActive"
#        Et que j'attache le fichier "types_actes/gabarit_delib.odt" à "TypeacteGabaritActeUpload"
#        Et que je presse "Enregistrer"
#        Alors je devrais voir "Le type d'acte ' Délibération ' a été modifié"
#        Alors je devrais voir la ligne "Délibération | Compteur Délibérations | Délibérations | Oui"