# language: fr
@webdelib @recette @paramétrage @javascript
Fonctionnalité: 011 - Paramétrage des connecteurs

Contexte:
    Etant donné que je suis connecté avec l'utilisateur "admin"
        Et que je vais dans le menu "Administration > Maintenance > Connecteurs"
    Alors je devrais voir un titre contenant "Gestion des connecteurs"

Scénario: 001 - Parapheur électronique (Signature)
    Lorsque je suis "Gérer le connecteur : Parapheur électronique (Signature)" dans la ligne "Parapheur électronique (Signature)"
        Alors je devrais voir le titre "Parapheur électronique (Signature)"
    Lorsque je sélectionne le bouton radio "Activation du connecteur > Oui"
        Et que je sélectionne "i-Parapheur" depuis "Type"
        Et que je remplis "Server" avec "https://secure-iparapheur-partenaires.libriciel.fr"
        Et que je remplis "Uri" avec "ws-iparapheur"
        Et que je remplis "Login" avec "webdelib@formation"
        Et que je remplis "Mot de passe utilisateur" avec "webdelib2018"
        Et que j'attache le fichier "Certificats/wd@libriciel.coop.p12" à "ConnecteurClientcert"
        Et que je remplis "Mot de passe du certificat" avec "wd@libriciel.coop"
        Et que je remplis "Type technique" avec "ACTES_AD"
        Et que je presse "Enregistrer"
    Alors je devrais voir "La configuration du module \"signature\" a été enregistrée"

Scénario: 002 - Tiers de télétransmission (TDT)
    Lorsque je suis "Gérer le connecteur : Tiers de télétransmission (TDT)" dans la ligne "Tiers de télétransmission (TDT)"
        Alors je devrais voir le titre "Tiers de télétransmission (TDT)"
    Lorsque je sélectionne le bouton radio "Activation du TDT > Oui"
        Et que je sélectionne "S²low" depuis "ConnecteurTdtProtocol"
        Et que je remplis "URL du serveur" avec "https://s2low.test.libriciel.fr"
        Et que j'attache le fichier "Certificats/demou.p12" à "ConnecteurClientcert"
        Et que je remplis "Mot de passe" avec "demou"
        Et que je sélectionne le bouton radio "Proxy > Non"
        Et que je sélectionne le bouton radio "Mail sécurisé > Oui"
        Et que je presse "Enregistrer"
    Alors je devrais voir "La configuration du module \"tdt\" a été enregistrée"

Scénario: 003 - I-delibRE
    Lorsque je suis "Gérer le connecteur : I-delibRE" dans la ligne "I-delibRE"
        Alors je devrais voir le titre "I-delibRE"
    Lorsque je sélectionne le bouton radio "Activation du service I-delibRE > Oui"
        Et que je remplis "URL" avec "https://idelibre.test.libriciel.fr"
        Et que je remplis "Connexion de la collectivité" avec "dasilva"
        Et que je remplis "Login" avec "secretaire"
        Et que je remplis "Mot de passe" avec "secretaire"
        Et que je sélectionne le bouton radio "Proxy > Non"
        Et que je sélectionne le bouton radio "Certificat de connexion > Non"
        Et que je sélectionne le bouton radio "Retirer les annexes > Non"
        Et que je presse "Enregistrer"
    Alors je devrais voir "La configuration du module \"idelibre\" a été enregistrée"

Scénario: 004 - Génération des documents
    Lorsque je suis "Gérer le connecteur : Génération des documents" dans la ligne "Génération des documents"
        Alors je devrais voir le titre "Génération des documents"
    Lorsque je sélectionne le bouton radio "Oui"
        Et que je remplis "WSDL de ODFGEDOOo :" avec "http://192.168.3.11:8080/ODFgedooo/OfficeService?wsdl"
        Et que je remplis "Adresse de CLOUDOOo :" avec "192.168.3.11"
        Et que je remplis "Port de CLOUDOOo :" avec "8011"
        Et que je presse "Enregistrer"
    Alors je devrais voir "La configuration du module \"conversion\" a été enregistrée"

Scénario: 005 - Pastell
    Lorsque je suis "Gérer le connecteur : Pastell" dans la ligne "Pastell"
        Alors je devrais voir le titre "Pastell"
    Lorsque je sélectionne le bouton radio "Oui"
        Et que je remplis "URL du serveur pastell :" avec "https://pastell2.test.libriciel.fr/"
        Et que je remplis "Nom d'utilisateur" avec "webdelib_recette"
        Et que je remplis "Mot de passe" avec "webdelib_recette"
        Et que je clique sur l'élément ayant le xpath "//*[@class='select2-selection__clear']"
        Et que je presse "Enregistrer"
    Alors je devrais voir "La configuration du module \"pastell\" a été enregistrée"
    Lorsque j'attends 5 secondes
        Et que je suis "Gérer le connecteur : Pastell" dans la ligne "Pastell"
        Alors je devrais voir le titre "Pastell"
    Lorsque je clique sur l'élément ayant le xpath "//*[@id='btnRefreshEntities']"
        Et que j'attends 2 secondes
        Et que je vois le texte "Important: Récupération effectuée"
        Et que je sélectionne "WEBDELIB RECETTE 5.1" depuis "CollectiviteIdEntity"
        Et que je remplis "Flux" avec "actes-generique"
        Et que je presse "Enregistrer"
    Alors je devrais voir "La configuration du module \"pastell\" a été enregistrée"

Scénario: 006 - Système d'archivage électronique (SAE)
    Lorsque je suis "Gérer le connecteur : Système d'archivage électronique (SAE)" dans la ligne "Système d'archivage électronique (SAE)"
        Alors je devrais voir le titre "Système d'archivage électronique (SAE)"
    Lorsque je sélectionne le bouton radio "Oui"
        Et que je sélectionne "Pastell" depuis "ConnecteurSaeProtocol"
        Et que je presse "Enregistrer"
    Alors je devrais voir "La configuration du module \"sae\" a été enregistrée"