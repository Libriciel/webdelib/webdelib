# language: fr
@webdelib @recette @javascripts @paramétrage
Fonctionnalité: 002 - Paramétrage des séances

    Contexte:
        Etant donné que je suis connecté avec l'utilisateur "admin"
        Et que je vais dans le menu "Administration > Utilisateurs > Utilisateurs"
        Alors je devrais voir un titre contenant "Liste des utilisateurs"

# OK -> adrien -> Direction des Sports
# amelie -> Service Petite Enfance
# axel -> Service voirie
# camille -> Direction des Sports
# celine -> Direction de l'Environnement
# david -> Service Jeunesse
# franck -> Service voirie
# pierre -> Service Jeunesse
# remi -> Direction des systèmes d'information
# romain -> Direction des Sports

# @see {@url http://www.fatrazie.com/jeux-de-mots/recreamots/284-pangrammes-fils-conducteurs}
# @see {@url https://pangram.me/fr/pangrammes}
Scénario: 001 - Attributions des droits sur tout les séances pour l'uitlisateur admin
    Lorsque je clique sur "Modifier  l'utilisateur" dans la ligne "admin"
        Alors je devrais voir le titre "Modifier l'utilisateur admin Administrateur"
        Et je clique sur l'onglet "Types de seances"
        #master checkBox
        Et je coche "AcoTypeseance"
        Et que je presse "Enregistrer"
    Alors je devrais voir "L'utilisateur \"admin\" a été modifié"

Scénario: 002 - Attibution des droits sur les séances pour l'utilisateur Adrien BRICCO
    Lorsque je clique sur "Gérer les droits" dans la ligne "adrien"
        Alors je devrais voir un titre contenant "Gérer les droits"
        Alors je devrais voir un titre contenant "de BRICCO Adrien"
        Et je coche "Séances : Gestion"
        Et que je presse "Enregistrer"
        Alors je devrais voir "L'utilisateur \"adrien\" a été modifié"

Scénario: 003 - Attribution des droits sur le type de séance Conseil municipal et Commission Finances pour l'utilisateur Adrien
    Lorsque je clique sur "Modifier  l'utilisateur" dans la ligne "adrien"
        Alors je devrais voir le titre "Modifier l'utilisateur Adrien BRICCO"
        Et je clique sur l'onglet "Types de seances"
        Et je coche "Conseil municipal"
        Et je coche "Commission Finances"
        Et que je presse "Enregistrer"
        Alors je devrais voir "L'utilisateur \"adrien\" a été modifié"

Scénario: 004 - Création de la liaison entre une séance et une séance délibérante
    Lorsque je vais dans le menu "Administration  > Générale > Types de séances"
    Et que je clique sur "Modifier" dans la ligne "Commission Finances"
    Alors je devrais voir le titre "Commission Finances"
    Et que je sélectionne "Conseil municipal" depuis "TypeseanceParentId"
    Et que je presse "Enregistrer"
    Alors je devrais voir " Le type de séance \"Commission Finances\" a été modifié"