# language: fr
@webdelib @recette @paramétrage @javascript
Fonctionnalité: 015 - Paramétrage des acteurs

Contexte:
    Etant donné que je suis connecté avec l'utilisateur "admin"
        Et que je vais dans le menu "Administration > Acteurs > Acteurs"
    Alors je devrais voir le titre "Liste des acteurs"

Plan du Scénario: 001 - Ajout des acteurs, élus, première page
    Lorsque je suis "Ajouter un acteur"
        Alors je devrais voir le titre "Ajouter un acteur"
    Lorsque je remplis "Civilité" avec "<Civilité>"
        Et que je remplis "Nom" avec "<Nom>"
        Et que je remplis "Prénom" avec "<Prénom>"
        Et que je remplis "Titre" avec "<Titre>"
        Et que je remplis "Adresse 1" avec "<Adresse 1>"
        Et que je remplis "Adresse 2" avec "<Adresse 2>"
        Et que je remplis "Code postal" avec "<Code postal>"
        Et que je remplis "Ville" avec "<Ville>"
        Et que je remplis "Téléphone fixe" avec "<Téléphone fixe>"
        Et que je remplis "Téléphone mobile" avec "<Téléphone mobile>"
        Et que je remplis "Email" avec "<Email>"
        Et que je sélectionne "<type acteur>" depuis "ActeurTypeacteurId"
        Et que je remplis "Ordre dans le conseil" avec "<Ordre>" si la valeur n'est pas vide
        Et que je sélectionne le bouton radio "ActeurNotifInsertion0"
        Et que je sélectionne le bouton radio "ActeurNotifProjetValide0"
        Et que je presse "Enregistrer"
    Alors je devrais voir "L'acteur \"<Prénom> <Nom>\" a été ajouté"
        Et je vois la ligne "<Ordre> | <Civilité> | <Nom> | <Prénom> | <Titre> | <type acteur> | élu"
Exemples:
    | Civilité | Nom      | Prénom    | Titre       | Adresse 1                    | Adresse 2 | Code postal | Ville       | Téléphone fixe | Téléphone mobile | Email                  | type acteur     | Ordre |
    | Mme      | BLANC    | Louise    | Maire       | 1 avenue Gambetta            |           | 34000       | Montpellier |                |                  | admin@webdelib.invalid | Majorité        | 1     |
    | M.       | BLEU     | Etienne   | 1er adjoint | 145 impasse du Levant        |           | 34000       | Montpellier |                |                  | admin@webdelib.invalid | Majorité        | 2     |
    | Mme      | ROSE     | Charline  | 2e adjointe | 1290 avenue du Pont Trinquat |           | 34070       | Montpellier |                |                  | admin@webdelib.invalid | Majorité        | 3     |
    | M.       | VERT     | Elliot    | 3e adjoint  | 4 boulevard du Jeu de Paume  |           | 34000       | Montpellier |                |                  | admin@webdelib.invalid | Majorité        | 4     |
    | Mme      | VIOLET   | Emma      |             |                              |           |             | Montpellier |                |                  | admin@webdelib.invalid | Majorité        | 5     |
    | M.       | NOIR     | Charles   |             |                              |           |             | Montpellier |                |                  | admin@webdelib.invalid | Majorité        | 6     |
    | Mme      | JAUNE    | Caroline  |             |                              |           |             | Montpellier |                |                  | admin@webdelib.invalid | Majorité        | 7     |
    | M.       | ORANGE   | François  |             |                              |           |             | Montpellier |                |                  | admin@webdelib.invalid | Majorité        | 8     |
    | Mme      | AZUR     | Jennifer  |             |                              |           |             | Montpellier |                |                  | admin@webdelib.invalid | Majorité        | 9     |
    | M.       | PRUNE    | Jean      |             |                              |           |             | Montpellier |                |                  | admin@webdelib.invalid | Opposition      | 10    |
    | Mme      | FUSCHIA  | Léa       |             |                              |           |             | Montpellier |                |                  | admin@webdelib.invalid | Opposition      | 11    |
    | M.       | MARRON   | Edouard   |             |                              |           |             | Montpellier |                |                  | admin@webdelib.invalid | Opposition      | 12    |
    | Mme      | GRIS     | Agathe    |             |                              |           |             | Montpellier |                |                  | admin@webdelib.invalid | Opposition      | 13    |
    | M.       | ROUGE    | Jérémy    |             |                              |           |             | Montpellier |                |                  | admin@webdelib.invalid | Opposition      | 14    |
    | M.       | BRUN     | David     |             |                              |           |             | Montpellier |                |                  | admin@webdelib.invalid | Elus suppléants |       |
    | Mme      | BLOND    | Pascaline |             |                              |           |             | Montpellier |                |                  | admin@webdelib.invalid | Elus suppléants |       |
    | M.       | ROUX     | François  |             |                              |           |             | Montpellier |                |                  | admin@webdelib.invalid | Elus suppléants |       |
    | Mme      | VENITIEN | Anne      |             |                              |           |             | Montpellier |                |                  | admin@webdelib.invalid | Elus suppléants |       |
    | M.       | CHATAIN  | Loïc      |             |                              |           |             | Montpellier |                |                  | admin@webdelib.invalid | Elus suppléants |       |

Scénario: 002 - Ajouter un acteur, invité, première page
    Étant donné que je suis sur la première page
    Lorsque je suis "Ajouter un acteur"
        Alors je devrais voir le titre "Ajouter un acteur"
    Lorsque je remplis "Civilité" avec "M."
        Et que je remplis "Nom" avec "Expert1Nom"
        Et que je remplis "Prénom" avec "Expert1Prenom"
        Et que je remplis "Titre" avec "Expert"
        Et que je remplis "Ville" avec "Montpellier"
        Et que je remplis "Email" avec "Expert1Prenom.Expert1Nom@webdelib.invalid"
        Et que je sélectionne "Invités" depuis "ActeurTypeacteurId"
        Et que je sélectionne le bouton radio "ActeurNotifInsertion0"
        Et que je sélectionne le bouton radio "ActeurNotifProjetValide0"
        Et que je presse "Enregistrer"
    Alors je devrais voir "L'acteur \"Expert1Prenom Expert1Nom\" a été ajouté"
    Et je cherche et je vois la ligne "M. | Expert1Nom | Expert1Prenom | Expert | Invités | non élu"

Scénario: 003 - Ajouter un acteur, invité, seconde page
    Étant donné que je suis sur la première page
    Lorsque je suis "Ajouter un acteur"
        Alors je devrais voir le titre "Ajouter un acteur"
    Lorsque je remplis "Civilité" avec "Mme"
        Et que je remplis "Nom" avec "Journaliste1Nom"
        Et que je remplis "Prénom" avec "Journaliste1Prenom"
        Et que je remplis "Titre" avec "Journaliste"
        Et que je remplis "Ville" avec "Montpellier"
        Et que je remplis "Email" avec "JournalistePrenom.JournalisteNom@webdelib.invalid"
        Et que je sélectionne "Invités" depuis "ActeurTypeacteurId"
        Et que je sélectionne le bouton radio "ActeurNotifInsertion0"
        Et que je sélectionne le bouton radio "ActeurNotifProjetValide0"
        Et que je presse "Enregistrer"
    Alors je devrais voir "L'acteur \"Journaliste1Prenom Journaliste1Nom\" a été ajouté"
        Et je cherche et je vois la ligne "Mme | Journaliste1Nom | Journaliste1Prenom | Journaliste | Invités | non élu"

Plan du Scénario: 004 - Paramétrage des suppléants
    Étant donné que je suis sur la première page
    Lorsque je cherche et je clique sur "Modifier" dans la ligne "<Nom> | <Prénom>"
        Alors je devrais voir le titre "Modifier l' acteur"
    Lorsque je sélectionne "<Suppléant>" depuis "ActeurSuppleantId"
        Et que je presse "Enregistrer"
    Alors je devrais voir "L'acteur \"<Prénom> <Nom>\" a été modifié"
        Et je cherche et je vois la ligne "<Nom> | <Prénom> | <Suppléant>"
Exemples:
    | Nom    | Prénom   | Suppléant       |
    | BLANC  | Louise   | David BRUN      |
    | BLEU   | Etienne  | Pascaline BLOND |
    | ROSE   | Charline | François ROUX   |
    | VERT   | Elliot   | Anne VENITIEN   |
    | VIOLET | Emma     | Loïc CHATAIN    |
