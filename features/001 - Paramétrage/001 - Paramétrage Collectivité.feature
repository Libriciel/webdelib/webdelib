# language: fr
@webdelib @recette @paramétrage @javascript
Fonctionnalité: 001 - Paramétrage de la collectivité

  Contexte:
    Etant donné que je suis connecté avec l'utilisateur "admin"
    Et que je vais dans le menu "Administration > Générale > Collectivité"
    Alors je devrais voir le titre "Informations de votre collectivité"

  Scénario: 001 - Modification de la collectivité
    Lorsque je suis "Modifier la collectivité" dans la ligne "webdelib"
    Et que je remplis "Code APE" avec "96.02A"
    Et que je remplis "Adresse mail" avec "collectivite@webdelib.invalid"
    Et que j'attache le fichier vérifié "logo.jpeg" à "CollectiviteLogo"
    Et que je presse "Enregistrer"
#    Alors je devrais voir "Le logo a bien été ajouté"
    Alors je devrais voir "La collectivité a été modifiée"