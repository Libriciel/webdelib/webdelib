# language: fr
@webdelib @recette @paramétrage @javascript
Fonctionnalité: 002 - Administration des thèmes

Contexte:
    Etant donné que je suis connecté avec l'utilisateur "admin"
        Et que je vais dans le menu "Administration > Générale > Thèmes"
    Alors je devrais voir le titre "Liste des thèmes"

Plan du Scénario: 001 - Ajout de thèmes sans parent
    Lorsque je suis "boutonAdd"
    Alors je devrais voir le titre "Ajouter un thème"
    Lorsque je remplis "Libellé" avec "<Valeur libellé>"
        Et que je remplis "Critère de tri" avec "<Valeur critère>"
        Et que je presse "Enregistrer"
    Alors je devrais voir "Le thème a été sauvegardé"
Exemples:
    | Valeur libellé                 | Valeur critère |
    | ADMINISTRATION GENERALE | 01             |
    | FINANCES                | 02             |
    | ENFANCE - JEUNESSE      | 03             |
    | URBANISME               | 04             |
    | CULTURE ET PATRIMOINE   | 05             |
    | NOUVELLES TECHNOLOGIES  | 06             |
    | POLITIQUE DE LA VILLE   | 07             |
    | ENVIRONNEMENT           | 08             |
    | AFFAIRES ECONOMIQUES    | 09             |
    | AFFAIRES SOCIALES       | 10             |
    | ETAT CIVIL              | 11             |
    | Défaut                  | 12             |

Plan du Scénario: 002 - Ajout de thèmes avec parent
    Lorsque je suis "boutonAdd"
    Alors je devrais voir le titre "Ajouter un thème"
    Lorsque je remplis "Libellé" avec "<Valeur libellé>"
        Et que je remplis "Critère de tri" avec "<Valeur critère>"
        Et que je sélectionne "<Appartient à>" depuis "ThemeParentId"
        Et que je presse "Enregistrer"
    Alors je devrais voir "Le thème a été sauvegardé"
Exemples:
    | Valeur libellé      | Valeur critère | Appartient à            |
    | Ressources Humaines | 011            | ADMINISTRATION GENERALE |
    | Affaires juridiques | 012            | ADMINISTRATION GENERALE |
    | Budget              | 021            | FINANCES                |
    | Subvention          | 022            | FINANCES                |
    | Dotation            | 023            | FINANCES                |
    | Education           | 031            | ENFANCE - JEUNESSE      |
    | Jeunesse et Sports  | 032            | ENFANCE - JEUNESSE      |

Scénario: 003 - Suppression du thème Défaut
    Lorsque je suis "Défaut"
        Et que j'accepterai la question "Voulez-vous vraiment supprimer le thème ?"
        Et que je suis "Supprimer"
    Alors je devrais voir "Le thème a été désactivé"