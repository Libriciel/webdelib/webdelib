# language: fr
@webdelib @recette @paramétrage @javascript
Fonctionnalité: 004 - Paramétrage des séquences

Contexte:
    Etant donné que je suis connecté avec l'utilisateur "admin"
        Et que je vais dans le menu "Administration > Générale > Séquences"
    Alors je devrais voir le titre "Liste des séquences"

Plan du Scénario: 001 - Ajout de séquences
    Lorsque je suis "Ajouter une séquence"
    Alors je devrais voir le titre "Ajouter une séquence"
    Lorsque je remplis "Nom" avec "<Nom>"
        Et que je remplis "Commentaire" avec "<Commentaire>"
        Et que je presse "Enregistrer"
    Alors je devrais voir "La séquence \"<Nom>\" a été ajoutée"
    Alors je devrais voir la ligne "<Nom> | <Commentaire> | 0"
Exemples:
    | Nom           | Commentaire |
    | Délibérations |             |
    | Arrêtés       |             |
    | Décisions     |             |
