# language: fr
@webdelib @recette @paramétrage @javascript
Fonctionnalité: 014 - Paramétrage des types d'acteurs

Contexte:
    Etant donné que je suis connecté avec l'utilisateur "admin"
        Et que je vais dans le menu "Administration > Acteurs > Types"
    Alors je devrais voir le titre "Liste des types d'acteur"

Plan du Scénario: 001 - Ajout les types d'acteurs
    Lorsque je suis "Ajouter un type d'acteur"
        Alors je devrais voir le titre "Ajouter un type d'acteur"
    Lorsque je remplis "Nom" avec "<Nom>"
        Et que je remplis "Commentaire" avec "<Commentaire>"
        Et que je sélectionne le bouton radio "<Type>"
        Et que je presse "Enregistrer"
    Alors je devrais voir "Le type d'acteur \"<Nom>\" a été ajouté"
        Et je vois la ligne "<Nom> | <Commentaire> | <Type>"
Exemples:
    | Nom             | Commentaire         | Type    |
    | Majorité        | Majorité            | élu     |
    | Opposition      | Opposition          | élu     |
    | Elus suppléants | Elus suppléants     | non élu |
    | Invités         | Invités             | non élu |
