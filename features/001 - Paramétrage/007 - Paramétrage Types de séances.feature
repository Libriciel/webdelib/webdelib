# language: fr
@todo-tests-manuels-nécessaires @webdelib @recette @paramétrage @javascript
Fonctionnalité: 007 - Paramétrage des types de séances

Contexte:
    Etant donné que je suis connecté avec l'utilisateur "admin"
        Et que je vais dans le menu "Administration > Générale > Types de séances"
    Alors je devrais voir le titre "Liste des types de séance"

Plan du Scénario: 001 Ajout de types de séances
    Lorsque je suis "Ajouter un type de séance"
    Alors je devrais voir le titre "Ajouter un type de séance"
    Lorsque je remplis "Libellé" avec "<Nom>"
        Et que je remplis "Couleur de fond" avec "<Couleur>"
        Et que je remplis "Nombre de jours avant retard" avec "<Jours>"
        Et que je sélectionne "<Action en séance>" depuis "TypeseanceAction"
        Et que je sélectionne "<Compteur>" depuis "TypeseanceCompteurId"
        Et que je sélectionne "<Types d'actes>" depuis "TypeacteTypeacte"
        Et que je sélectionne "<Projet>" depuis "TypeseanceModeleProjetId"
        Et que je sélectionne "<Document final>" depuis "TypeseanceModeleDeliberationId"
        Et que je sélectionne "<Convocation>" depuis "TypeseanceModeleConvocationId"
        Et que je sélectionne "<Ordre du jour>" depuis "TypeseanceModeleOrdredujourId"
        Et que je sélectionne "<PV sommaire>" depuis "TypeseanceModelePvsommaireId"
        Et que je sélectionne "<PV complet>" depuis "TypeseanceModelePvdetailleId"
        Et que je sélectionne "<Journal>" depuis "TypeseanceModeleJournalSeanceId"
        Et que je presse "Enregistrer"
    Alors je devrais voir "Le type de séance \"<Nom>\" a été sauvegardé"
    Alors je devrais voir la ligne "<Nom> | <Action en séance> | <Compteur>"
Exemples:
    | Nom                    | Couleur | Jours | Action en séance | Compteur      | Types d'actes | Projet | Document final | Convocation    | Ordre du jour | PV sommaire           | PV complet  | Journal           |
    | Conseil municipal      | #3b81df | 7     | Voter            | Délibérations | Délibération  | Projet | Délibération   | Convocation CM | ODJ complet   | Compte rendu sommaire | PV détaillé | Journal de séance |
    | Commission sans action | #e2e96e | 0     | Sans action      | Délibérations | Délibération  | Projet | Projet         | Convocation CM | ODJ simple    | Compte rendu sommaire | PV détaillé | Journal de séance |
    | Commission Finances    | #3b8958 | 0     | Donner un avis   | Délibérations | Délibération  | Projet | Projet         | Convocation CM | ODJ simple    | Compte rendu sommaire | PV détaillé | Journal de séance |
    | Commission urbanisme   | #faef09 | 15    | Donner un avis   | Délibérations | Délibération  | Projet | Délibération   | Convocation CM | ODJ simple    | Compte rendu sommaire | PV détaillé | Journal de séance |
