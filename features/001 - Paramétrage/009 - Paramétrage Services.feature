# language: fr
@webdelib @recette @paramétrage @javascript
Fonctionnalité: 009 - Paramétrage des services

Contexte:
    Etant donné que je suis connecté avec l'utilisateur "admin"
        Et que je vais dans le menu "Administration > Utilisateurs > Services"
    Alors je devrais voir le titre "Liste des services"

Plan du Scénario: 001 - Ajout de services sans parent
    Lorsque je suis "boutonAdd"
        Alors je devrais voir le titre "Ajouter un service"
    Lorsque je remplis "Libellé" avec "<Libellé>"
        Et que je presse "Enregistrer"
    Alors je devrais voir "Le service a été sauvegardé"
        Et je devrais voir "<Libellé>"
Exemples:
    | Libellé                              |
    | Cabinet du Maire                     |
    | Direction de la Communication        |
    | Direction de l'Education             |
    | Direction de l'Environnement         |
    | Direction des ressources             |
    | Direction des Sports                 |
    | Direction des systèmes d'information |
    | Direction générale des services      |
    | Services techniques municipaux       |

Plan du Scénario: 002 - Ajout de services enfants
    Lorsque je suis "boutonAdd"
        Alors je devrais voir le titre "Ajouter un service"
    Lorsque je remplis "Libellé" avec "<Libellé>"
        Et que je sélectionne "<Appartient à>" depuis "ServiceParentId"
        Et que je presse "Enregistrer"
    Alors je devrais voir "Le service a été sauvegardé"
Exemples:
    | Libellé                                       | Appartient à                           |
    | Service Jeunesse                       | Direction de l'Education        |
    | Service Petite Enfance                 | Direction de l'Education        |
    | Service des Finances                   | Direction des ressources        |
    | Services des Ressources Humaines       | Direction des ressources        |
    | Secrétariat des assemblées             | Direction générale des services |
    | Service propreté - gestion des déchets | Services techniques municipaux  |
    | Service voirie                         | Services techniques municipaux  |
