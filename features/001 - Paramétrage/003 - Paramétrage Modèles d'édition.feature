# language: fr
@todo-plus-de-modèles-avec-erreurs @webdelib @recette @paramétrage @javascript
Fonctionnalité: 003 - Paramétrage des modèles d'édition

Contexte:
    Etant donné que je suis connecté avec l'utilisateur "admin"
        Et que je vais dans le menu "Administration > Générale > Modèles d'édition"

Plan du Scénario: 001 - Ajout de modèles d'édition
    Alors je devrais voir le titre "Liste des modèles d'édition"
    Lorsque je suis "Ajouter un modèle"
    Alors je devrais voir le titre "Ajouter un modèle"
    Lorsque je remplis "Libellé" avec "<Valeur libellé>"
        Et que je sélectionne "<Valeur type de modèle>" depuis "ModeltemplateModeltypeId"
        Et que j'attache le fichier "<Valeur fichier>" à "ModeltemplateFileupload"
        Et que je presse "Enregistrer"
    Alors je devrais voir "Modèle validé et enregistré"
Exemples:
    | Valeur libellé            | Valeur type de modèle | Valeur fichier                             |
    | Projet                    | Projet                | modeles/1Projet_delib_annexe.odt           |
    | Arrêté                    | Projet                | modeles/2Arrete.odt                        |
    | Délibération              | Délibération          | modeles/3Delibération.odt                  |
    | Convocation CM            | Convocation           | modeles/4 Convocation CM.odt               |
    | ODJ simple                | Ordre du jour         | modeles/5Odj_simple.odt                    |
    | ODJ complet               | Ordre du jour         | modeles/6Odj_complet.odt                   |
    | Multiséances ODJ          | Multi-séance          | modeles/7Ordre du jour (Multi-séances).odt |
    | Multiséances vote         | Multi-séance          | modeles/8multiseance_tableau_vote.odt      |
    | Compte rendu sommaire     | PV sommaire           | modeles/9 CR.odt                           |
    | PV détaillé               | PV détaillé           | modeles/10 PV.odt                          |
    | Recherche recueil arrêtés | Recherche             | modeles/11Recherche_recueil_arretes.odt    |
    | Bordereau                 | Bordereau de projet   | modeles/12bordereau_projet.odt             |
    | Journal de séance         | Journal de séance     | modeles/13Journal_seance.odt               |
    | Lot actes tamponnés       | Recherche             | modeles/lot_actes_tampon.odt               |
    | modele_variables          | Recherche             | modeles/test_variables.odt                 |
