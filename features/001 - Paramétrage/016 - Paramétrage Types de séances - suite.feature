# language: fr
@webdelib @recette @paramétrage @javascript
Fonctionnalité: 016 - Paramétrage des types de séances - suite

Contexte:
    Etant donné que je suis connecté avec l'utilisateur "admin"
        Et que je vais dans le menu "Administration > Générale > Types de séances"
    Alors je devrais voir le titre "Liste des types de séance"

Scénario: 001 - Ajout des convocations pour les séances de type "Commission urbanisme"
    Lorsque je clique sur "Modifier" dans la ligne "Commission urbanisme"
        Et que je sélectionne "Elus titulaires" depuis "TypeacteurTypeacteur"
        Et que je sélectionne une autre option "Elus suppléants" depuis "TypeacteurTypeacteur"
        Et que je sélectionne une autre option "Invités" depuis "TypeacteurTypeacteur"
        Et que je presse "Enregistrer"
    Alors je devrais voir "Le type de séance \"Commission urbanisme\" a été modifié"

Scénario: 002 - Ajout des convocations pour les séances de type "Conseil municipal"
    Lorsque je clique sur "Modifier" dans la ligne "Conseil municipal"
        Et que je sélectionne "Elus titulaires" depuis "TypeacteurTypeacteur"
        Et que je sélectionne une autre option "Elus suppléants" depuis "TypeacteurTypeacteur"
        Et que je sélectionne une autre option "Invités" depuis "TypeacteurTypeacteur"
        Et que je presse "Enregistrer"
    Alors je devrais voir "Le type de séance \"Conseil municipal\" a été modifié"