# language: fr
@fixme-droits-non-traduits @fixme-droits-réordonnés @webdelib @recette @paramétrage @javascript
Fonctionnalité: 008 - Paramétrage des profils

Contexte:
    Etant donné que je suis connecté avec l'utilisateur "admin"
        Et que je vais dans le menu "Administration > Utilisateurs > Profils"
    Alors je devrais voir le titre "Liste des profils"

Plan du Scénario: 001 - Ajout des profils
    Lorsque je suis "boutonAdd"
        Alors je devrais voir le titre "Ajouter un profil"
    Lorsque je remplis "Libellé" avec "<Nom>"
        Et que je sélectionne "<Type>" depuis "ProfilRoleId"
        Et que je presse "Enregistrer"
    Alors je devrais voir le message "Le profil a été sauvegardé"
        Et je devrais voir la ligne de texte "<Nom>"
Exemples:
    | Nom                        | Type                       |
    | Administrateur fonctionnel | Administrateur fonctionnel |
    | Service assemblées         | Administrateur             |
    | Validateurs                | Utilisateur                |
    | Rédacteurs                 | Utilisateur                |

Scénario: 002 - Modification du profil "Admin fonctionnel"
    Lorsque je suis "Administrateur fonctionnel"
        Et que je suis "Modifier"
        Alors je devrais voir le titre "Modifier le profil : Administrateur fonctionnel"
        Et je suis dans l'onglet "Informations principales"
    Lorsque je clique sur l'onglet "Droits"
        Et que je coche "Profils"
        Et que je presse "Enregistrer"
    Alors je devrais voir le message "Le profil a été modifié"

Scénario: 003 - Modification du profil "Service assemblées"
    Lorsque je suis "Service assemblées"
        Et que je suis "Modifier"
        Alors je devrais voir le titre "Modifier le profil : Service assemblées"
        Et je suis dans l'onglet "Informations principales"
    Lorsque je clique sur l'onglet "Droits"
        Et que je coche "Profils"
        Et que je décoche "Administration > Utilisateurs > Profils"
        Et que je décoche "Administration > Maintenance > Connecteur (Accès)"
        Et que je décoche "Administration > Maintenance > Tâches automatiques"
        Et que je décoche "Administration > Maintenance > Historiques"
        Et que je presse "Enregistrer"
    Alors je devrais voir le message "Le profil a été modifié"

Scénario: 004 - Modification du profil "Validateurs"
    Lorsque je suis "Validateurs"
        Et que je suis "Modifier"
        Alors je devrais voir le titre "Modifier le profil : Validateurs"
        Et je suis dans l'onglet "Informations principales"
    Lorsque je clique sur l'onglet "Droits"
        Et que je coche "Connexion (Accès)"
        Et que je coche "Préférences utilisateur (Accès)"
        Et que je coche "Mes projets > À traiter"
        Et que je coche "Mes projets > Validés"
        Et que je coche "Retourner à"
        Et que je coche "Envoyer à"
        Et que je coche "Recherche (Accès)"
        Et que je presse "Enregistrer"
    Alors je devrais voir le message "Le profil a été modifié"

Scénario: 005 - Modification du profil "Rédacteurs"
    Lorsque je suis "Rédacteurs"
        Et que je suis "Modifier"
        Alors je devrais voir le titre "Modifier le profil : Rédacteurs"
        Et je suis dans l'onglet "Informations principales"
    Lorsque je clique sur l'onglet "Droits"
        Et que je coche "Connexion (Accès)"
        Et que je coche "Préférences utilisateur (Accès)"
        Et que je coche "Projets"
        Et que je décoche "Modifier tous les projets"
        Et que je décoche "Supprimer tous les projets"
        Et que je coche "Mes projets > À traiter"
        Et que je coche "Mes projets > Validés"
        Et que je coche "Retourner à"
        Et que je coche "Envoyer à"
        Et que je coche "Recherche (Accès)"
        Et que je presse "Enregistrer"
    Alors je devrais voir le message "Le profil a été modifié"