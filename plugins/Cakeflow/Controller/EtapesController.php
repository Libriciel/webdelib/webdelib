<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class EtapesController extends CakeflowAppController
{
    public $components = [
        'Cakeflow.VueDetaillee',
        'Paginator',
        'Auth' => [
            'mapActions' => [
                'read' => ['manager_index','manager_view','admin_index','admin_view'],
                'create'=> ['manager_add','admin_add'],
                'update' => ['manager_edit','admin_edit','admin_moveUp','admin_moveDown','manager_moveUp','manager_moveDown'],
                'delete'=>['manager_delete','admin_delete'],
                ]
            ]
        ];

    public $uses = ['Cakeflow.Circuit', 'Cakeflow.Etape', 'Cakeflow.Visa', 'Cakeflow.Composition', 'Cakeflow.Traitement'];

    public function admin_index($circuit_id)
    {
        $this->index($circuit_id);

        $this->render('index');
    }

    public function manager_index($circuit_id)
    {
        $this->index($circuit_id, ['id']);

        $this->render('index');
    }

    /**
     * @param integer $circuit_id
     * @return render|redirect
     */
    private function index($circuit_id, $allow=null)
    {
        $this->Circuit->id=$circuit_id;
        if ($this->Circuit->exists()) {
            $this->set('circuit', $this->Etape->Circuit->field('nom'));
            $this->paginate = [
                'fields' => [
                    'Etape.ordre',
                    'Etape.nom',
                    'Etape.description',
                    'Etape.type',
                    'Etape.soustype',
                    'Etape.cpt_retard'
                ],
                'allow'=>$allow,
                'contain' => ['Composition'=> ['fields' => ['trigger_id', 'type_composition', 'soustype']]],
                'order' => ['Etape.ordre' => 'ASC']
            ];
            $etapes = $this->Paginator->paginate('Etape', ['Etape.circuit_id' => $circuit_id]);
            //Si le circuit est vide, rediriger vers la vue d'ajout d'étape
            if (empty($etapes)) {
                $this->redirect(['action' => 'add', $circuit_id]);
            }

            // Mise en forme pour chaque ligne
            foreach ($etapes as &$etape) {
                $etape['ListeActions']['delete'] = $this->Etape->isDeletable($etape['Etape']['id']);
                $etape['Etape']['libelleType'] = Etape::getTypeLabelByValue($etape['Etape']['type']);
                $etape['Etape']['libelleSousType'] = '';
                foreach ($etape['Composition'] as &$composition) {
                    $composition['libelleTrigger'] = $this->Etape->formatLinkedModel('Trigger', $composition['trigger_id'], $composition['type_composition'], $composition['soustype']);
                }
            }
            $nbrEtapes = $this->Etape->find('count', ['conditions' => ['Etape.circuit_id' => $circuit_id]]);
            $this->set(compact('etapes', 'nbrEtapes'));
        } else {
            $this->Flash->set('Circuit introuvable, id erroné', ['element' => 'growl']);

            return $this->redirect($this->previous);
        }
        $this->set('circuit_id', $circuit_id);
    }

    public function admin_view($id = null)
    {
        $this->view($id);

        $this->render('view');
    }

    public function manager_view($id = null)
    {
        $this->view($id, ['id']);

        $this->render('view');
    }
    /**
     * @param integer $id
     * @return render|redirect
     */
    private function view($id = null, $allow=null)
    {
        $this->request->data = $etape = $this->Etape->find('first', [
            'conditions' => ['id' => $id],
            'recursive' => -1,
            'allow'=>$allow
            ]);
        if (empty($this->data)) {
            $this->Flash->set(__('Invalide id pour l\'', true) . ' ' . __('étape', true) . ' : ' . __('affichage de la vue impossible.', true), ['element' => 'growl','params'=>['type' => 'danger']]);
            return $this->redirect(['action' => 'index']);
        } else {
            $this->pageTitle = Configure::read('appName') . ' : ' . __('Etapes des circuits de traitement', true) . ' : ' . __('vue d&eacute;taill&eacute;e', true);
            // lecture du circuit de l'étape
            $circuit = $this->Etape->Circuit->find('first', [
                'recursive' => -1,
                'fields' => ['id', 'nom'],
                'conditions' => ['id' => $this->data['Etape']['circuit_id']]]);

            // préparation des informations é afficher dans la vue détaillée
            $maVue = new $this->VueDetaillee(
                    __('Vue d&eacute;taill&eacute;e de l\'etape \'', true) . $this->data['Etape']['nom'] . '\' du circuit \'' . $circuit['Circuit']['nom'] . '\'',
                __('Retour &agrave; la liste des étapes', true),
                ['action' => 'index', $circuit['Circuit']['id']]
            );

            $maVue->ajouteSection(__('Création / Modification'));
            $maVue->ajouteLigne(__('Date de cr&eacute;ation'), $this->data['Etape']['created']);
            $maVue->ajouteElement(__('Par'), $this->Etape->formatUser($this->data['Etape']['created_user_id']));
            $maVue->ajouteLigne(__('Date de derni&egrave;re modification'), $this->data['Etape']['modified']);
            $maVue->ajouteElement(__('Par'), $this->Etape->formatUser($this->data['Etape']['modified_user_id']));

            $maVue->ajouteSection(__('Informations principales'));
            $maVue->ajouteLigne(__('Identifiant interne (id)'), $this->data['Etape']['id']);
            $maVue->ajouteLigne(__('Nom'), $this->data['Etape']['nom']);
            $maVue->ajouteLigne(__('Description'), $this->data['Etape']['description']);
            $maVue->ajouteLigne(__('Ordre'), $this->data['Etape']['ordre']);
            $maVue->ajouteLigne(__('Type'), $this->Etape->getTypeLabelByValue($this->data['Etape']['type']));

            // Affichage des compositions
            $compositions = $this->Etape->Composition->find('all', [
                'conditions' => ['Composition.etape_id' => $id],
                'fields' => ['Composition.trigger_id', 'Composition.type_validation']
            ]);
            if (!empty($compositions)) {
                $maVue->ajouteSection(__('Composition de l\'étape', true));
                foreach ($compositions as $composition) {
                    $maVue->ajouteLigne($this->Etape->formatLinkedModel('Trigger', $composition['Composition']['trigger_id']), $this->Etape->Composition->libelleTypeValidation($composition['Composition']['type_validation']));
                }
            }
            $this->set('contenuVue', $maVue->getContenuVue());
        }
        $this->set('circuit', $circuit);
    }

    public function admin_add($circuit_id = null)
    {
        $this->add($circuit_id);

        $this->render('add_edit');
    }

    public function manager_add($circuit_id = null)
    {
        $this->add($circuit_id);

        $this->render('add_edit');
    }
    /**
     * @param integer $circuit_id
     * @return redirect|render
     */
    private function add($circuit_id = null, $allow=null)
    {
        //'allow'=>$allow,
        if (empty($circuit_id) || !$this->Circuit->exists($circuit_id)) {
            $this->Flash->set('Circuit introuvable.', ['element' => 'growl']);
            return $this->redirect(['controller'=>'circuits', 'action' => 'index']);
        }
        $nbrEtapes = $this->Etape->find('count', ['conditions' => ['Etape.circuit_id' => $circuit_id]]);
        $etapeMinCptRetard = $this->Etape->find('first', [
            'fields' => ['MIN(Etape.cpt_retard) as retardmax'],
            'conditions' => ['Etape.circuit_id' => $circuit_id]
        ]);
        $retardMax = $etapeMinCptRetard[0]['retardmax'];
        $this->set('retard_max', $retardMax);
        if (!empty($this->data)) {
            //Vérification cpt_retard valide
            if (!empty($retardMax) && $this->request->data['Etape']['cpt_retard'] > $retardMax) {
                $this->Flash->set('Erreur lors de l\'enregistrement : le compteur de retard a une valeur supèrieure à celui d\'une étape précédente', ['element' => 'growl']);
            } else {
                $this->request->data['Etape']['ordre'] = $nbrEtapes + 1;
                $this->Etape->create();
                $this->setCreatedModifiedUser($this->request->data, 'Etape');
                if ($this->Etape->save($this->request->data)) {
                    $this->Flash->set('Enregistrement effectué.', ['element' => 'growl']);
                    return $this->redirect(['action' => 'index', $circuit_id]);
                } else {
                    $this->Flash->set('Erreur lors de l\'enregistrement.', ['element' => 'growl']);
                }
            }
        } else {
            $this->request->data['Etape']['circuit_id'] = $circuit_id;
        }
        $circuit = $this->Etape->Circuit->find('first', [
                'recursive' => -1,
                'fields' => ['id', 'nom'],
                'conditions' => ['id' => $circuit_id]]);

        $nbr_etape = $this->Etape->find('count', [
          'conditions' => ['Etape.circuit_id' => $circuit_id],
          'recursive' => -1,
        ]);
        if ($nbr_etape==0) {
            $this->set('previous', ['plugin'=>'cakeflow','controller'=>'circuits']);
        }

        $this->set('circuit', $circuit);
        $this->set('types', $this->Etape->getTypesLabel());
    }

    public function admin_edit($id = null)
    {
        $this->edit($id);
    }

    public function manager_edit($id = null)
    {
        $this->edit($id);
    }

    /**
     * @param integer $id identifiant de l'étape à modifier
     * @return redirect | render
     */
    private function edit($id = null, $allow=null)
    {
        if (empty($id) || !$this->Etape->exists($id)) {
            $this->Flash->set('Etape introuvable.', ['element' => 'growl']);
            return $this->redirect(['controller'=>'circuits', 'action' => 'index']);
        }
        $etape = $this->Etape->find('first', [
            'recursive' => -1,
            'conditions' => ['Etape.id'=>$id]
        ]);
        $circuit = $this->Etape->Circuit->find('first', [
                'recursive' => -1,
                'fields' => ['id', 'nom'],
                'conditions' => ['id' => $etape['Etape']['circuit_id']]]);
        $recordRetardMax = $this->Etape->find('first', [
            'conditions' => [
                'Etape.circuit_id' => $etape['Etape']['circuit_id'],
                'Etape.ordre <' => $etape['Etape']['ordre'],
                'Etape.cpt_retard >=' => 0
            ],
            'fields' => ['MIN(Etape.cpt_retard) as retardmax']
        ]);
        $this->set('retard_max', $recordRetardMax[0]['retardmax']);
        $recordRetardInf = $this->Etape->find('first', [
            'conditions' => [
                'Etape.circuit_id' => $etape['Etape']['circuit_id'],
                'Etape.ordre <' => $etape['Etape']['ordre'],
                'Etape.cpt_retard !=' => 0
            ],
            'fields' => ['MAX(Etape.cpt_retard) as retardinf']
        ]);
        $this->set('retard_inf', $recordRetardInf[0]['retardinf']);

        if (!empty($this->data)) {
            //Vérification cpt_retard valide
            if ($recordRetardMax[0]['retardmax'] != null && $this->request->data['Etape']['cpt_retard'] > $recordRetardMax[0]['retardmax']) {
                $this->Flash->set('Erreur lors de l\'enregistrement : le compteur de retard a une valeur supèrieure à celui d\'une étape précédente', ['element' => 'growl']);
            } else {
                $this->Etape->begin();
                try {
                    // Si nouvelle affectation ou diminution de cpt_retard
                    if ($etape['Etape']['cpt_retard'] > $this->request->data['Etape']['cpt_retard']
                        || (empty($etape['Etape']['cpt_retard']) && !empty($this->request->data['Etape']['cpt_retard']))) {
                        $etapeSuivante = $this->Etape->find('first', [
                            'recursive' => -1,
                            'fields' => ['Etape.cpt_retard'],
                            'conditions' => [
                                'Etape.circuit_id' => $this->request->data['Etape']['circuit_id'],
                                'Etape.ordre' => $this->request->data['Etape']['ordre']+1
                            ]
                        ]);
                        if (!empty($etapeSuivante)) {
                            $decalage = $etapeSuivante['Etape']['cpt_retard'] - $this->request->data['Etape']['cpt_retard'];
                            if ($decalage > 0) {
                                $etapesSuivante = $this->Etape->find('all', [
                                    'recursive' => -1,
                                    'conditions' => [
                                        'Etape.circuit_id' => $this->request->data['Etape']['circuit_id'],
                                        'Etape.ordre >' => $this->request->data['Etape']['ordre']
                                    ],
                                    'order' => 'Etape.ordre ASC'
                                ]);
                                foreach ($etapesSuivante as $etapeSuivante) {
                                    if (!empty($etapeSuivante['Etape']['cpt_retard'])) {
                                        $etapeSuivante['Etape']['cpt_retard'] -= $decalage;
                                        if ($etapeSuivante['Etape']['cpt_retard'] < 0) {
                                            $etapeSuivante['Etape']['cpt_retard'] = 0;
                                        }
                                        $this->Etape->id = $etapeSuivante['Etape']['id'];
                                        $this->setCreatedModifiedUser($etapeSuivante, 'Etape');
                                        if (!$this->Etape->save($etapeSuivante)) {
                                            throw new Exception('Erreur lors de la modification du compteur des étapes suivante.');
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $this->Etape->id = $id;
                    $this->setCreatedModifiedUser($this->request->data, 'Etape');
                    if ($this->Etape->save($this->request->data)) {
                        $this->Etape->commit();
                        $this->Flash->set('Enregistrement effectué.', ['element' => 'growl']);
                        return $this->redirect(['action' => 'index', $etape['Etape']['circuit_id']]);
                    } else {
                        throw new Exception('Erreur lors de l\'enregistrement de l\étape.');
                    }
                } catch (Exception $e) {
                    $this->Etape->rollback();
                    $this->Flash->set($e->getMessage(), ['element' => 'growl']);
                }
            }
        } else {
            $this->request->data = $etape;
        }
        $this->set('circuit', $circuit);
        $this->set('types', $this->Etape->getTypesLabel());
        $this->render('add_edit');
    }

    public function admin_delete($id = null)
    {
        $this->delete($id);
    }

    public function manager_delete($id = null)
    {
        $this->delete($id);
    }

    /**
     * Supprimer une étape
     * @param integer $id
     * @return redirect
     */
    private function delete($id = null, $allow=null)
    {  // FIXME: !empty find + service_id
        $etape = $this->Etape->find('first', [
            'conditions' => ['Etape.id' => $id],
            'recursive' => -1,
            'fields' => ['Etape.circuit_id', 'Etape.ordre']]);
        if ($this->Etape->delete($id)) {
            $etapes = $this->Etape->find('all', [
                'conditions' => [
                    'Etape.circuit_id' => $etape['Etape']['circuit_id'],
                    'Etape.ordre >' => $etape['Etape']['ordre']],
                'fields' => ['Etape.id', 'Etape.ordre'],
                'recursive' => -1]);
            foreach ($etapes as $etape) {
                $this->Etape->id = $etape['Etape']['id'];
                $this->Etape->saveField('ordre', $etape['Etape']['ordre'] - 1);
            }
            $this->Flash->set('Suppression effectuée', ['element' => 'growl']);
        } else {
            $this->Flash->set('Erreur lors de la suppression', ['element' => 'growl']);
        }

        return $this->redirect($this->previous);
    }

    public function admin_moveUp($id = null, $allow=null)
    {
        $this->moveUp($id, $allow);
    }

    public function manager_moveUp($id = null, $allow=null)
    {
        $this->moveUp($id, $allow);
    }

    /**
     * Monter l'ordre d'une étape
     * @param integer $id
     * @return redirect
     */
    private function moveUp($id = null, $allow=null)
    {  // FIXME: !empty find + service_id
        if (!$this->Etape->moveUp($id)) {
            $this->Flash->set('Impossible de changer l\'ordre des étapes', ['element' => 'growl']);
        }
        return $this->redirect($this->previous);
    }

    public function admin_moveDown($id = null, $allow=null)
    {
        $this->moveDown($id, $allow);
    }

    public function manager_moveDown($id = null, $allow=null)
    {
        $this->moveDown($id, $allow);
    }

    /**
     * @param integer $id
     * @return redirect
     */
    private function moveDown($id = null, $allow=null)
    {  // FIXME: !empty find + service_id
        if (!$this->Etape->moveDown($id)) {
            $this->Flash->set('Impossible de changer l\'ordre des étapes', ['element' => 'growl']);
        }
        return $this->redirect($this->previous);
    }
}
