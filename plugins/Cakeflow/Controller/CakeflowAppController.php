<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class CakeflowAppController extends AppController
{
    public $components = ['RequestHandler','Flash'];
    public $helpers = ['Html', 'Time'];

    /**
     * Initialisation des champs 'user_created_id' et 'user_modified_id'
     * @param array $data tableau de données
     * @param string $modelClass nom du modèle des données contenues dans $data
     * @param string $action nom de l'action 'add' ou 'edit'
     */
    protected function setCreatedModifiedUser(&$data, $modelClass = '', $action = '')
    {
        // Initialisations
        if (empty($modelClass)) {
            $modelClass = $this->modelClass;
        }
        if (empty($action)) {
            $action = $this->action;
        }

        // lecture en session de l'id de l'utilisateur connecté
        if (Configure::read('Cakeflow.user.idsession')) {
            $userId = $this->Session->read(Configure::read('Cakeflow.user.idsession'));
        } else {
            return;
        }

        if ($action == 'add') {
            $data[$modelClass]['created_user_id'] = $userId;
        }
        $data[$modelClass]['modified_user_id'] = $userId;
    }

    public function beforeFilter()
    {

        //Pas d'autentification pour les requesteds
        //if (isset($this->params['requested'])) $this->Auth->allow($this->action);
        //initialisation des mapActions pour les droits CRUD
        if (isset($this->components['Auth']['mapActions'])) {
            //Mise en place des actions publiques
            if (isset($this->components['Auth']['mapActions']['allow'])) {
                $this->Auth->allow($this->components['Auth']['mapActions']['allow']);
                unset($this->components['Auth']['mapActions']['allow']);
            }

            $this->Auth->mapActions($this->components['Auth']['mapActions']);
        }

        parent::beforeFilter();
    }
}
