<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Etape', 'Cakeflow.Model');
/**
 * Plugin Workflow for CakePHP: Modèle des compositions
 *
 * @package app.Model
 * @version v4.1.0
 */

class Composition extends CakeflowAppModel
{
    public $tablePrefix = 'wkf_';
    public $belongsTo = [
        'Cakeflow.Etape',
        CAKEFLOW_TRIGGER_MODEL => [
            'className' => CAKEFLOW_TRIGGER_MODEL,
            'foreignKey' => 'trigger_id'
        ]
    ];
    public $validate = [
        'etape_id' => [
            [
                'rule' => 'notBlank',
                'required' => true,
                'message' => 'Champ obligatoire'
            ]
        ],
        'trigger_id' => [
            [
                'rule' => 'notBlank',
                'required' => true,
                'message' => 'Champ obligatoire'
            ]
        ],
        'type_validation' => [
            [
                'rule' => 'notBlank',
                'required' => true,
                'message' => 'Champ obligatoire'
            ]
        ],
        'type_composition' => [
            'notBlank' => [
                'rule' => 'notBlank',
                'required' => true,
                'message' => 'Champ obligatoire'
            ],
            'delegation' => [
                'rule' => ['canSaveDelegation'],
                'message' => 'Delegation impossible pour cette étape'
            ],
            'TypeComposition_simple' => [
                'rule' => ['canSaveTypeComposition'],
                'message' => 'Impossible d\'ajouter une composition pour cette étape'
            ],
        ],
    ];

    /**
     * @version 4.1.0
     * @access public
     * @param type $options
     * @return boolean
     */
    public function beforeSave($options = [])
    {
        $this->setCreatedModifiedUser($this->data);
        if (!empty($this->data['Composition']['type_composition']) && in_array($this->data['Composition']['type_composition'], ['CONNECTOR', 'PARAPHEUR', 'PASTELL'], true)) {
            $this->Etape->id = $this->data['Composition']['etape_id'];
            $this->Etape->saveField('soustype', $this->data['Composition']['soustype']);
        }

        return true;
    }

    /**
     * Retourne true si la validation de la composition est de type Signature
     * @param integer $etapeId id de l'étape de la composition
     * @param integer $userId id de l'utilisateur de la composition
     * @return boolean true si signature, false dans le cas contraire
     */
    public function validationParSignature($etapeId, $userId)
    {
        $data = $this->find('first', [
            'recursive' => -1,
            'fields' => ['type_validation'],
            'conditions' => [
                'etape_id' => $etapeId,
                'trigger_id' => $userId]]);
        return ($data['Composition']['type_validation'] == 'S');
    }

    /**
     * Retourne la liste des types de validation
     * @return array code, libelle de la liste des type de composition
     */
    public function listeTypeValidation()
    {
        return [
            'V' => __('Visa', true),
            'S' => __('Signature', true),
            'D' => __('Délégation de validation', true)];
    }

    /**
     * Retourne le libellé du soustype d'étape
     * @param string $code_soustype
     */
    public function libelleSousType($code_soustype)
    {
        $soustypes = $this->listeSousTypesParapheur();
        return $soustypes[$code_soustype];
    }

    /**
     * Retourne la liste des sous types du parapheurs associés au type défini dans le fichier de config
     * @return array soustypes
     */
    public function listeSousTypesParapheur()
    {
        App::uses('Signature', 'Lib');
        $Signature = new Signature();
        return $Signature->listCircuits();
    }

    /**
     * Retourne la liste des sous types du parapheurs associés au type défini dans le fichier de config
     * @return array soustypes
     */
    public function listeVisaSousTypesParapheur()
    {
        App::uses('Signature', 'Lib');
        $Signature = new Signature();
        return $Signature->listVisaCircuits();
    }

    /**
     * Retourne le libellé du type de validation
     * @param string $code_type lettre S ou V
     */
    public function libelleTypeValidation($code_type)
    {
        $typesValid = $this->listeTypeValidation();
        return $typesValid[$code_type];
    }

    /**
     * @version 4.3
     * @access public
     * @return boolean
     */
    public function canSaveDelegation()
    {
        $conditions = ['etape_id' => $this->data['Composition']['etape_id']];
        if (!empty($this->data['Composition']['type_composition']) && in_array($this->data['Composition']['type_composition'], ['CONNECTOR', 'PARAPHEUR', 'PASTELL'], true)) {
            //Le type SIMPLE est obligatoire
            $type = $this->Etape->field('type', ['Etape.id' => $this->data['Composition']['etape_id']]);


            if ($type != Etape::getTypeValue('simple')) {
                $this->validator()->getField('type_composition')->getRule('delegation')->message = __d('Cakeflow', 'Une delegation n\'est possible qu\'avec un type d\'étape simple.');

                return false;
            }
            //Lors de la modification d'une composition de type non USER
            if (!empty($this->id)) {
                $conditions['id !='] = $this->id;
            }
        } else {
            $conditions['trigger_id'] = -1;
            //Lors de la création ou modification d'une composition de type USER
            if (!empty($this->id)) {
                $conditions['id !='] = $this->id;
            }
            $this->validator()->getField('type_composition')->getRule('delegation')->message = __d('Cakeflow', 'Une delegation est présente pour cette étape.');
        }

        return !$this->hasAny($conditions);
    }

    /**
     * Vérifie si l'on peut rajouter un composant à cette étape, à savoir:
     * étape pas de type simple, ou nombre de composants égale à 0.
     */
    public function canSaveTypeComposition()
    {
        return !empty($this->id) ? true : $this->canAdd($this->data['Composition']['etape_id']);
    }

    /**
     * Vérifie si l'on peut rajouter un composant à cette étape, à savoir:
     * étape pas de type simple, ou nombre de composants égale à 0.
     */
    public function canAdd($etape_id)
    {
        $this->recursive = -1;
        $nbrCompositionsEtape = $this->find('count', [
            'conditions' => [
                'etape_id' => $etape_id]
        ]);
        $type = $this->Etape->field('type', ['Etape.id' => $etape_id]);

        return ($type != Etape::getTypeValue('simple')) || ($nbrCompositionsEtape < 1);
    }
}
