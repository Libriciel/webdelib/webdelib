<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * Plugin Workflow for CakePHP
 *
 * @package app.Model
 * @version 5.3.0
 * @since 4.1.0
 */
class Etape extends CakeflowAppModel
{
    public $tablePrefix = 'wkf_';
    public $displayField = 'nom';

    /**
     * @var array Associations
     */
    public $belongsTo = [
        'Circuit' => [
            'className' => 'Cakeflow.Circuit',
            'foreignKey' => 'circuit_id',
            'conditions' => '',
            'order' => ''
        ],
        'CreatedUser' => [
            'className' => CAKEFLOW_USER_MODEL,
            'foreignKey' => 'created_user_id',
            'conditions' => '',
            'order' => ''
        ],
        'ModifiedUser' => [
            'className' => CAKEFLOW_USER_MODEL,
            'foreignKey' => 'modified_user_id',
            'conditions' => '',
            'order' => ''
        ]
    ];
    /**
     * [public description]
     * @var array
     */
    public $hasMany = [
        'Composition' => [
            'className' => 'Cakeflow.Composition',
            'foreignKey' => 'etape_id',
            'conditions' => '',
            'order' => '',
            'dependent' => true
        ]
    ];

    /**
     * Règles de validation
     * @var array
     */
    public $validate = [
        'circuit_id' => [
            [
                'rule' => 'notBlank',
                'required' => true,
                'message' => 'Champ obligatoire'
            ]
        ],
        'nom' => [
            [
                'rule' => ['maxLength', '250'],
                'message' => 'Maximum 250 caractères'
            ],
            [
                'rule' => 'notBlank',
                'required' => true,
                'message' => 'Champ obligatoire'
            ]
        ],
        'type' => [
            [
                'rule' => 'notBlank',
                'required' => true,
                'message' => 'Champ obligatoire'
            ]
        ],
        'ordre' => [
            [
                'rule' => 'notBlank',
                'required' => true,
                'message' => 'Champ obligatoire'
            ]
        ]
    ];

    public function beforeSave($options = [])
    {
        if (empty($this->data[$this->alias]['id'])) {
            $this->data[$this->alias]['created_user_id'] = AuthComponent::user('id');
        }
        $this->data[$this->alias]['modified_user_id'] = AuthComponent::user('id');

        return true;
    }

    /**
     * @param $id
     * @return bool
     */
    public function moveUp($id)
    {
        $item = $this->find('first', [
            'recursive' => -1,
            'conditions' => ['Etape.id' => $id]
        ]);
        if (!empty($item)) {
            if ($item['Etape']['ordre'] > 1) {
                $swap = $this->find(
                    'first',
                    [
                        'conditions' => [
                            'Etape.ordre' => $item['Etape']['ordre'] - 1,
                            'Etape.circuit_id' => $item['Etape']['circuit_id']
                        ],
                        'recursive' => -1
                    ]
                );
                if (!empty($swap)) {
                    $swap['Etape']['ordre'] = $swap['Etape']['ordre'] + 1;
                    $item['Etape']['ordre'] = $item['Etape']['ordre'] - 1;

                    //Le compteur de retard de l'étape après ne doit pas être inférieur
                    if ($swap['Etape']['cpt_retard'] > $item['Etape']['cpt_retard']) {
                        $swap['Etape']['cpt_retard'] = $item['Etape']['cpt_retard'];
                    }

                    $saved = true;
                    $this->begin();

                    $saved = $this->save($swap) && $saved;
                    $saved = $this->save($item) && $saved;

                    if ($saved) {
                        $this->commit();
                    } else {
                        $this->rollback();
                    }
                    return $saved;
                }
            }
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public function moveDown($id)
    {
        $item = $this->find('first', [
            'recursive' => -1,
            'conditions' => ['Etape.id' => $id]
        ]);
        if (!empty($item)) {
            $swap = $this->find(
                'first',
                [
                    'conditions' => [
                        'Etape.ordre' => $item['Etape']['ordre'] + 1,
                        'Etape.circuit_id' => $item['Etape']['circuit_id']
                    ],
                    'recursive' => -1
                ]
            );
            if (!empty($swap)) {
                $swap['Etape']['ordre'] = $swap['Etape']['ordre'] - 1;
                $item['Etape']['ordre'] = $item['Etape']['ordre'] + 1;

                //Le compteur de retard de l'étape avant ne doit pas être supérieur
                if ($swap['Etape']['cpt_retard'] < $item['Etape']['cpt_retard']) {
                    $swap['Etape']['cpt_retard'] = $item['Etape']['cpt_retard'];
                }

                $saved = true;
                $this->begin();

                $saved = $this->save($swap) && $saved;
                $saved = $this->save($item) && $saved;

                if ($saved) {
                    $this->commit();
                } else {
                    $this->rollback();
                }
                return $saved;
            }
        }
        return false;
    }

    /**
     * @param $circuit_id
     * @return array
     */
    public function getNbEtapes($circuit_id)
    {
        return ($this->find('count', ['circuit_id' => "$circuit_id"]));
    }

    /**
     * @param $circuit_id
     * @return bool
     */
    public function _reorder($circuit_id)
    {
        $items = $this->find(
            'all',
            [
                'conditions' => ['Etape.circuit_id' => $circuit_id],
                'order' => ['ordre ASC'],
                'recursive' => -1
            ]
        );
        if (!empty($items)) {
            $i = 1;
            $saved = true;
            foreach ($items as $item) {
                $item['Etape']['ordre'] = $i;
                $this->create($item);
                $saved = $this->save() && $saved;
                $i++;
            }
        }
        return true;
    }

    /**
     * @param integer $id
     * @param bool $cascade
     * @return bool
     */
    public function del($id = null, $cascade = true)
    {
        $this->begin();
        if ($item = $this->findById($id, null, null, -1)) {
            $return = parent::del($id, $cascade);
            $return = $this->_reorder($item['Etape']['circuit_id']) && $return;
            if ($return) {
                $this->commit();
            } else {
                $this->rollback();
            }
            return $return;
        }
        return false;
    }

    /**
     * Détermine si une étape est la dernière d'un circuit
     * @param integer $etapeId id de l'étape que l'on veut tester
     * @return boolean true si dernière étape, false dans le cas contraire
     */
    public function estDerniereEtape($etapeId)
    {
        $etape = $this->find('first', [
            'conditions' => ['Etape.id' => $etapeId],
            'recursive' => -1,
            'fields' => ['Etape.circuit_id', 'Etape.ordre']]);

        $etapeSuivanteOrdre = $etape['Etape']['ordre'] + 1;

        $etapeSuivante = $this->find('first', [
            'conditions' => [
                'Etape.circuit_id' => $etape['Etape']['circuit_id'],
                'Etape.ordre' => $etapeSuivanteOrdre],
            'recursive' => -1,
            'fields' => ['Etape.id']]);

        return empty($etapeSuivante);
    }

    /**
     * Détermine si une étape est la dernière d'un circuit
     * @param integer $circuitId id du circuit
     * @return integer identifiant de la dernière étape
     */
    public function getDerniereEtape($circuitId)
    {
        $etape = $this->find('first', [
            'conditions' => ['circuit_id' => $circuitId],
            'recursive' => -1,
            'order' => ['Etape.ordre DESC']]);

        return $etape['Etape']['id'];
    }
    /**
     * Retourne l'id de l'étape suivante d'une étape
     * @param integer $etapeId id de l'étape
     * @return integer id de l'étape suivante, 0 si pas d'étape suivante
     */
    public function etapeSuivante($etapeId)
    {
        $etape = $this->find('first', [
            'conditions' => [
                'Etape.id' => $etapeId],
            'recursive' => -1]);

        $etapeSuivanteOrdre = $etape['Etape']['ordre'] + 1;

        $etapeSuivante = $this->find('first', [
            'conditions' => [
                'Etape.circuit_id' => $etape['Etape']['circuit_id'],
                'Etape.ordre' => $etapeSuivanteOrdre],
            'recursive' => -1,
            'fields' => ['Etape.id']]);

        return empty($etapeSuivante) ? 0 : $etapeSuivante['Etape']['id'];
    }

    /**
     * Indique si une étape pour un traitement est validée ou pas (ie peut-on passer à l'étape suivante)
     * @param integer $traitementId id de l'étape
     * @param integer $etapeId id de l'étape
     * @param $numeroTraitement
     * @return bool true si l'étape est validée et false dans le cas contraire
     */
    public function etapeValidee($traitementId, $etapeId, $numeroTraitement)
    {
        // Initialisation
        $validee = false;
        // Lecture de l'étape
        $etape = $this->find('first', [
            'conditions' => [
                'Etape.id' => $etapeId],
            'recursive' => -1]);
        // recherche de ou des compositions
        $compositions = $this->Composition->find('all', [
            'conditions' => [
                'Composition.etape_id' => $etape['Etape']['id']],
            'recursive' => -1]);
        // on teste en fonction du type de l'étape la présence d'un visa pour chaque composition
        foreach ($compositions as $composition) {
            $visa = $this->Visa->find('first', [
                'conditions' => [
                    'Visa.traitement_id' => $traitementId,
                    'Visa.composition_id' => $composition['Composition']['id'],
                    'Visa.numero_traitement' => $numeroTraitement],
                'recursive' => -1]);
            $validee = !empty($visa);
            if ($etape['Etape']['type'] == self::getTypeValue('simple')) {
                break;
            } elseif ($etape['Etape']['type'] ==  self::getTypeValue('concurrent')) {
                if ($validee) {
                    break;
                }
            } elseif ($etape['Etape']['type'] == self::getTypeValue('collaboratif')) {
                if (!$validee) {
                    break;
                }
            }
        }
        return $validee;
    }

    /**
     * retourne la liste des étapes précédentes
     * @param integer $etapeId id de l'étape courante
     * @return array tableau associatif id-nom des étapes précédentes
     */
    public function listeEtapesPrecedentes($etapeId)
    {
        $etape = $this->find('first', [
            'conditions' => ['Etape.id' => $etapeId],
            'recursive' => -1]);
        if (empty($etape) || $etape['Etape']['ordre'] == 1) {
            return [];
        } else {
            return $this->find('list', [
                'conditions' => [
                    'Etape.circuit_id' => $etape['Etape']['circuit_id'],
                    'Etape.ordre <' => $etape['Etape']['ordre']],
                'order' => ['Etape.ordre']]);
        }
    }

    /**
     * Détermine si une instance peut être supprimée tout en respectant l'intégrité référentielle
     * Paramètre : id
     */
    public function isDeletable($id)
    {
        // Existence de l'instance en base
        if (!$this->find('count', ['recursive' => -1, 'conditions' => ['id' => $id]])) {
            return false;
        }

        // Existence d'une composition liée
        if ($this->Composition->find('count', ['recursive' => -1, 'conditions' => ['etape_id' => $id]])) {
            return false;
        }

        return true;
    }

    /**
     * Calcule la date avant retard selon la séance délibérante moins le nombre de jours avant retard
     * @param $cptRetard nombre de jours avant retard
     * @param $targetId identifiant du projet
     * @return string date avant retard ou null si aucune séance délibérante rattachée
     */
    public function computeDateRetard($cptRetard, $targetId)
    {
        //Si la méthode existe dans le modèle cible
        if (!empty($cptRetard) && method_exists($this->Circuit->Traitement->{CAKEFLOW_TARGET_MODEL}, 'computeDateRetard')) {
            return $this->Circuit->Traitement->{CAKEFLOW_TARGET_MODEL}->computeDateRetard($cptRetard, $targetId);
        }

        return null;
    }

    public static function getTypesLabel()
    {
        return Hash::combine(Configure::read('Cakeflow.etapesType'), '{s}.value', '{s}.label');
    }

    public static function getTypeLabel($type)
    {
        $types = Configure::read('Cakeflow.etapesType');

        return $types[$type]['label'];
    }

    public static function getTypeValue($type)
    {
        $types = Configure::read('Cakeflow.etapesType');
        return $types[$type]['value'];
    }

    public static function getTypeLabelByValue($value)
    {
        return current(Hash::extract(Configure::read('Cakeflow.etapesType'), '{s}[value='.$value.'].label'));
    }
}
