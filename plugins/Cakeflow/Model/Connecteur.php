<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * Plugin Workflow for CakePHP
 *
 * @package app.Model
 * @version v4.1.0
 */

class Connecteur extends CakeflowAppModel
{
    public $useTable = false;

    public function _pingRest($traitement)
    {
        $circuit_id = $traitement['Traitement']['circuit_id'];
        $orde = $traitement['Traitement']['numero_traitement'] - 1;

        $this->Composition->unbindModel([
            'belongsTo' => [
                CAKEFLOW_TRIGGER_MODEL
            ]
        ]);

        $composition = $this->Composition->find('first', [
            'fields' => ['Connecteur.id', 'Connecteur.nom_u', 'Connecteur.monfichier', 'Etape.id'],
            'conditions' => ['Etape.circuit_id' => $circuit_id, 'Etape.ordre' => $orde],
            'order' => ['Etape.ordre ASC'],
            'contain' => [
                'Etape' => ['fields' => ['id', 'circuit_id', 'ordre']],
                'Connecteur' => ['fields' => ['id', 'nom_u', 'monfichier']],
            ]
        ]);
        $content = json_decode($composition['Connecteur']['monfichier'], true);
        App::uses('RestClient', 'Lib');
        try {
            $restclient = new RestClient($content['Connecteur']['url']['value']);
            if (empty($restclient)) {
                return false;
            }
            $opts = [
                CURLOPT_USERPWD => $content['Connecteur']['login']['value'] . ':' . $content['Connecteur']['password']['value'],
                CURLOPT_HEADER => "Content-Type:application/xml"
            ];
            $request = $content['Connecteur']['get']['value'];
            //Debugger::dump($request);
            $response = $restclient->get($request, $opts);

            if (!empty($response)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            throw $e;
        }
        return false;
    }

    /**
     * Le dispatcher permet de choisir le traitement a utiliser en fonction du
     * serveur mit dans le json de configuration(REST,...)
     *
     * @param $targetId
     * @param $traitement 'Traitement' => array(
     *                      'numero_traitement' => (int) 3,
     *                      'id' => (int) 352,
     *                      'circuit_id' => (int) 17,
     *                      'target_id' => (int) 437
     *                      )
     * @param null $type    type d'envoi au serveur
     * @param null $id      id du document voulue, id en rapport avec le
     * connecteur
     */
    public function dispatcher($targetId, $traitement, $type = null, $id = null)
    {
        $circuit_id = $traitement['Traitement']['circuit_id'];
        $orde = $traitement['Traitement']['numero_traitement'] - 1;

        if ($type == 'GET' && $id === null) {
            try {
                return $this->_pingRest($traitement);
            } catch (Exception $exc) {
                throw $exc;
            }
        }
        $this->Composition->unbindModel([
            'belongsTo' => [
                CAKEFLOW_TRIGGER_MODEL
            ]
        ]);

        $composition = $this->Composition->find('first', [
            'fields' => ['Connecteur.id', 'Connecteur.nom_u', 'Connecteur.monfichier', 'Etape.id'],
            'conditions' => ['Etape.circuit_id' => $circuit_id, 'Etape.ordre' => $orde],
            'order' => ['Etape.ordre ASC'],
            'contain' => [
                'Etape' => ['fields' => ['id', 'circuit_id', 'ordre']],
                'Connecteur' => ['fields' => ['id', 'nom_u', 'monfichier']],
            ]
        ]);
        //Debugger::dump($this->Composition->find('all',array('conditions' => array('Etape.circuit_id' => $circuit_id, 'Etape.ordre' => $orde))));
        $content = json_decode($composition['Connecteur']['monfichier'], true);
        if ($content['TypeConnecteur'] == 'REST') {
            try {
                return $this->_restConnecteur($targetId, $traitement, $type, $id);
            } catch (Exception $e) {
                //return $e->getMessage();
                throw $e;
            }
        }
    }

    /**
     * Fonction temporaire de test, pour la connexion par web service REST dans u premier temp avec mafate
     * @param integer $targetId id
     * @param array $traitement
     * @Param String $type contient le type de la requette  GET /  POST /  PUT /  DELETE
     */
    public function _restConnecteur($targetId, $traitement, $type = null, $id = null)
    {
        $circuit_id = $traitement['Traitement']['circuit_id'];
        $orde = $traitement['Traitement']['numero_traitement'] - 1;
        $addUrl = '';


        // récupération du nom du connecteur
        //récupération des informations du connecteur
        $this->Composition->unbindModel([
            'belongsTo' => [
                CAKEFLOW_TRIGGER_MODEL
            ]
        ]);
        $composition = $this->Composition->find('first', [
            'fields' => ['Connecteur.id', 'Connecteur.nom_u', 'Connecteur.monfichier', 'Etape.id'],
            'conditions' => ['Etape.circuit_id' => $circuit_id, 'Etape.ordre' => $orde],
            'order' => ['Etape.ordre ASC'],
            'contain' => [
                'Etape' => ['fields' => ['id', 'circuit_id', 'ordre']],
                'Connecteur' => ['fields' => ['id', 'nom_u', 'monfichier']],
            ]
        ]);

        $content = json_decode($composition['Connecteur']['monfichier'], true);

        //Contenue dans le json (comme le type)
        if (!empty($content['addUrl'])) {
            $addUrl = $content['addUrl'];
        }

        App::uses('RestClient', 'Lib');
        try {
            //$InfoProjet = array('id' => 'n est pas null');

            $restclient = new RestClient($content['Connecteur']['url']['value']);
            if (empty($restclient)) {
                $this->Session->setFlash('Erreur lors de la tentative de connexion au serveur REST.', 'growl', ['type' => 'erreur']);
                return false;
            }
            if (!empty($content['Connecteur']['password']['value']) && !empty($content['Connecteur']['login']['value'])) {
                $opts = [
                    CURLOPT_HEADER => "Content-Type:application/xml"
                ];
            } elseif (!empty($content['Connecteur']['password']['value'])) {
                $opts = [
                    CURLOPT_USERPWD => $content['Connecteur']['login']['value'],
                    CURLOPT_HEADER => "Content-Type:application/xml"
                ];
            } else {
                $opts = [
                    CURLOPT_HEADER => "Content-Type:application/xml"
                ];
            }

            if ($type == 'GET') {
                //on récupère la chaine sans les "{}"
                $request = $content['Connecteur']['get']['value'] . '/' . $id . $addUrl;
                //Debugger::dump($request);
                $response = $restclient->get($request, $opts);
                //retourne un tableau sous la forme $array['retour'] = OK, KO , IT
                $json = json_decode($response, true);
                //Debugger::dump($response);
                if (!empty($response)) {
                    return $json[$id];
                } else {
                    return false;
                }
            } elseif ($type == 'POST') {
                $InfoProjet = $this->_getInfoProjet($targetId);
                $request = $content['Connecteur']['post']['value'] . $addUrl;
                $response = $restclient->post($request, $InfoProjet['Deliberation'], $opts);
                //on verifi si la reponse du serveur est une érreur
                if (stripos($response, 'err') != false) {
                    return false;
                }
                //on récupère l'id renvoyé par le serveur
                $json = json_decode($response, true);
                $this->Visa->unbindModel([
                    'belongsTo' => [
                        CAKEFLOW_TRIGGER_MODEL, 'Traitement', 'Etape'
                    ]
                ]);
                //sauvegarde l'id de retour du connecteur afin de faire le lien avec l'id de webdelib ds la table wkf_traitement
                $visa = $this->Visa->find('first', [
                    'conditions' => ['Visa.traitement_id' => $traitement['Traitement']['id'], 'Visa.numero_traitement' => $traitement['Traitement']['numero_traitement']]
                ]);
                if (empty($visa)) {
                    return false;
                }
                $this->Visa->id = $visa['Visa']['id']; // InfoProjet contient le tableau deliberation avec l'id voulue
                $this->Visa->saveField('id_custom_connecteur', $json['id']); //sauvegarde de l'id du connecteur ds la table wkf_traitement
            } elseif ($type == 'PUT') {
                $InfoProjet = $this->_getInfoProjet($targetId);
                $request = $content['Connecteur']['get']['value'] . '/' . $id . $addUrl;
                $response = $restclient->put($request, $InfoProjet['Deliberation'], $opts);
                $json = json_decode($response, true);
                if (!empty($response)) {
                    return $json;
                } else {
                    return false;
                }
            } elseif ($type == 'DELETE') {
                $request = $content['Connecteur']['get']['value'] . '/' . $id . $addUrl;
                $response = $restclient->delete($request);
                $json = json_decode($response, true);
                if (!empty($response)) {
                    return $json;
                } else {
                    return false;
                }
            }
        } catch (Exception $e) {
            //return $e->getMessage();
            //throw new Exception($e->getMessage(),$e->getCode(),$e);

            throw $e;
        }
        return true;
    }

    /**
     * La fonction va metre a jour l'etat des connecteur, dans un premier temp récupérer les visa d'on la date est null
     * (pas encore traité) et qui correspond au numéro de traitement de l'étape,pour chaque etape de connecteurs trouvé
     * on va faire appel a la fonction _restConnecteur qui doit renvoyé 'OK' quand tout c'est bien passé et que le
     * document est traité, 'ER' pour une érreur, 'KO' pour un document refusé, 'IT' pour un document non traité.
     * @return array contient la valeur de retour de la fonction execute(dernier traitement ou non), et si la valeur du traitement(true ou 'KO')
     */
    public function updateConnecteurs()
    {
        //onrécupère tout les id de traitement des circuit encour avec un connecteur
        //SELECT traitement_id,min(numero_traitement) FROM wkf_visas WHERE date IS NULL  AND type_validation = 'C' GROUP BY traitement_id

        $this->Visa->unbindModel([
            'belongsTo' => [
                CAKEFLOW_TRIGGER_MODEL, 'Etape', 'Connecteur'
            ]
        ]);

        $idConnecteur = $this->Visa->find('all', [
            'fields' => [
                'Visa.id',
                'Visa.traitement_id',
                'Visa.trigger_id',
                'Visa.id_custom_connecteur',
                'Visa.type_validation',
                'Visa.numero_traitement',
                'Traitement.numero_traitement',
                'Traitement.id',
                'Traitement.circuit_id',
                'Traitement.target_id'
            ],
            'conditions' => [
                'Visa.date' => null,
                'Visa.type_validation' => 'C',
                'Visa.numero_traitement = Traitement.numero_traitement '
            ],
            'group' => [
                'Visa.id',
                'Visa.traitement_id',
                'Visa.trigger_id',
                'Visa.id_custom_connecteur',
                'Visa.type_validation',
                'Visa.numero_traitement',
                'Traitement.numero_traitement',
                'Traitement.id',
                'Traitement.circuit_id',
                'Traitement.target_id HAVING Visa.action <> \'KO\''
            ]
        ]);
        //'joins' => array($this->join('Typeacteur',array( 'type' => 'INNER' ))),
        //Debugger::dump($this->Visa);
        //Debugger::dump($idConnecteur);
        //pour chaque id de traitement(donc connecteurs) on va éffectuer une requette get avec id pour savoir ou en est le traitement
        $retTab = [];
        try {
            foreach ($idConnecteur as $id => $val) {
                $traitement['Traitement'] = $val['Traitement'];
                $traitement['Traitement']['numero_traitement'] = $val['Visa']['numero_traitement'];
                $targetId = $val['Traitement']['target_id'];
                $id = $val['Visa']['id_custom_connecteur'];
                if (empty($id)) {
                    $id = $targetId;
                }
                $ret = $this->dispatcher($targetId, $traitement, 'GET', $id);
                //$ret = 'OK';
                //Debugger::dump($ret);
                if ($ret != 'IT') {
                    $retTab[$targetId]['valid'] = $this->Visa->Traitement->execute($ret, $val['Visa']['trigger_id'], $targetId);
                    $retTab[$targetId]['trigger_id'] = $val['Visa']['trigger_id'];
                    if ($ret != 'OK') {
                        $retTab[$targetId]['valid'] = $ret;
                    }
                }
            }
        } catch (Exception $e) {
            //$e->getMessage();
            new Exception($e->getMessage(), $e->getCode(), $e);
        }
        //Debugger::dump($retTab);
        return $retTab;
    }
}
