<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * Plugin Workflow for CakePHP
 *
 * @package app.Model
 * @version v4.1.0
 */

class CakeflowAppModel extends AppModel
{
    public $actsAs = ['Containable', 'Database.DatabaseTable'];

    /**
     * Retourne le libellé d'un booléen sous la forme 'Oui' 'Non'
     * @param boolean $bouleen valeur du booléen
     * @param string $libelleTrue libelle a retourner pour la valeur true ('Oui' par défaut)
     * @param string $libelleFalse libelle a retourner pour la valeur false ('Non' par défaut)
     */
    public function boolToString($bouleen, $libelleTrue = 'Oui', $libelleFalse = 'Non')
    {
        return ($bouleen ? __($libelleTrue, true) : __($libelleFalse, true));
    }

    /**     * *******************************************************************
     *       Permet de supprimer toutes les associations d'un modèle donné
     *       INFO: http://bakery.cakephp.org/articles/view/unbindall
     * ** ****************************************************************** */
    public function unbindModelAll($reset = true)
    {
        $unbind = [];
        foreach ($this->belongsTo as $model => $info) {
            $unbind['belongsTo'][] = $model;
        }
        foreach ($this->hasOne as $model => $info) {
            $unbind['hasOne'][] = $model;
        }
        foreach ($this->hasMany as $model => $info) {
            $unbind['hasMany'][] = $model;
        }
        foreach ($this->hasAndBelongsToMany as $model => $info) {
            $unbind['hasAndBelongsToMany'][] = $model;
        }
        parent::unbindModel($unbind, $reset);
    }

    /**
     * Initialisation des champs 'user_created_id' et 'user_modified_id'
     * @param array $data tableau de données
     * @param string $modelClass nom du modèle des données contenues dans $data
     * @param string $action nom de l'action 'add' ou 'edit'
     */
    protected function setCreatedModifiedUser(&$data, $modelClass = '', $action = '')
    {
        // Initialisations
        if (empty($modelClass)) {
            $modelClass = $this->modelClass;
        }
        if (empty($action)) {
            $action = $this->action;
        }

        // lecture en session de l'id de l'utilisateur connecté
        if (Configure::read('Cakeflow.user.idsession')) {
            $userId = CakeSession::read(Configure::read('Cakeflow.user.idsession'));
        } else {
            return;
        }

        if ($action == 'add') {
            $data[$modelClass]['created_user_id'] = $userId;
        }
        $data[$modelClass]['modified_user_id'] = $userId;
    }

    /**
     * Lit et formate l'occurence $modelId du modèle $modelName ('User', 'Trigger' ou 'Target') en fonction des paramètres du fichier de config
     * @param integer $modelName nom du model ('User', 'Trigger' ou 'Target')
     * @param integer $modelId id de l'utilisateur
     * @return string occurence du modele formatée
     */
    public function formatLinkedModel($modelName, $modelId, $type_composition = null, $soustype = null)
    {
        // initialisations
        $model_const = Configure::read('Cakeflow.'.strtolower($modelName).'.model');

        if (strtolower($modelName) == 'trigger' && ($modelId == 0)) {
            return 'Rédacteur du projet';
        } elseif (strtolower($modelName) == 'trigger' && ($modelId == -1)) {
            $target_model = ClassRegistry::init(CAKEFLOW_TARGET_MODEL);
            return $target_model->delegationLinkCakeflow($type_composition, $soustype);
        }
        ${$model_const} = ClassRegistry::init($model_const);
        // lecture des informations en base
        $occ = ${$model_const}->find('first', [
          'recursive' => -1,
          'fields' => explode(',', Configure::read('Cakeflow.'.strtolower($modelName).'.fields')),
          'conditions' => ['id' => $modelId]]);

        // mise en forme de la réponse
        if (empty($occ)) {
            return '';
        } else {
            $occ = $occ[$model_const];
            return vsprintf(Configure::read('Cakeflow.'.strtolower($modelName).'.format'), $occ);
        }
    }

    /**
     * Lit et formate l'occurence $modelId du modèle $modelName ('User', 'Trigger' ou 'Target') en fonction des paramètres du fichier de config
     * @version 5.3.0
     * @param string $modelName nom du model ('User', 'Trigger' ou 'Target')
     * @param array $modelIds tableau d'id utilisateur
     * @return string occurence du modele formatée
     */
    public function formatLinkedModels($modelName, $modelIds)
    {
        // initialisations
        $model_const = Configure::read('Cakeflow.'.strtolower($modelName).'.model');
        ${$model_const} = ClassRegistry::init($model_const);

        $results = [];
        foreach ($modelIds as $modelId) {
            if ((strtolower($modelName) == 'trigger') && ($modelId == 0)) {
                $results[] = 'Rédacteur du projet';
            } elseif ((strtolower($modelName) == 'trigger') && ($modelId == -1)) {
                $results[] = '[Parapheur]';
            }
            $occ = ${$model_const}->find('first', [
                'recursive' => -1,
                'fields' => explode(',', Configure::read('Cakeflow.'.strtolower($modelName).'.fields')),
                'conditions' => ['id' => $modelId]]);

            // mise en forme de la réponse
            if (empty($occ)) {
                $results[] = '';
            } else {
                $occ = $occ[$model_const];
                $results[] = vsprintf(Configure::read('Cakeflow.'.strtolower($modelName).'.format'), $occ);
            }
        }

        return $results;
    }

    /**
     * Retourne la liste des occurences en vu de l'utiliser dans un select
     * @version 5.3.0
     * @param integer $modelName nom du model ('User', 'Trigger' ou 'Target')
     * @return array liste formatée
     */
    public function listLinkedModel($modelName)
    {
        // initialisations
        $ret = [];
        $model_const = Configure::read('Cakeflow.'.strtolower($modelName).'.model');
        //$model_const = constant('CAKEFLOW_' . strtoupper($modelName) . '_MODEL');
        // /$modelConditions = constant('CAKEFLOW_' . strtoupper($modelName) . '_CONDITIONS');

        // initialisation de la condition
        $conditions = [];
        if (strtolower($modelName) == 'trigger' && Configure::check('Cakeflow.trigger.conditions')) {
            $criters = explode(';', Configure::read('Cakeflow.trigger.conditions'));
            foreach ($criters as $criter) {
                $fieldValue = explode(',', $criter);
                $conditions[$fieldValue[0]] = $fieldValue[1];
            }
        }

        // lecture des informations en base
        ${$model_const} = ClassRegistry::init($model_const);
        $occs = ${$model_const}->find('all', [
            'recursive' => -1,
            'fields' => 'id',
            'order' => 'nom',
            'conditions' => $conditions]);

        // formatage des occurences
        $ret[0] = "Rédacteur du projet";
        foreach ($occs as $occ) {
            $ret[$occ[$model_const]['id']] = $this->formatLinkedModel($modelName, $occ[$model_const]['id']);
        }

        return $ret;
    }

    /**
     * Mise en forme pour l'affichage des utilisateurs de création et de modification dans les vue détaillées
     * @version 5.3.0
     * @param integer $userId id de l'utilisateur
     * @return string utilisateur formaté selon le fichier de config
     */
    public function formatUser($userId)
    {
        // vérifie si l'acces à l'utilisateur en session est défini
        return !Configure::read('Cakeflow.user.idsession') ? '' : $this->formatLinkedModel('User', $userId);
    }
}
