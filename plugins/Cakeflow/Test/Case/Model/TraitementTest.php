<?php

/**
 *
 * PHP 5.3
 *
 * @package app.Test.Case.Model
 * @license CeCiLL V2 (http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)
 */
App::uses('Circuit', 'Cakeflow.Model');
App::uses('Traitement', 'Cakeflow.Model');

class TraitementTest extends CakeTestCase {

    public $fixtures = ['plugin.cakeflow.circuit', 'plugin.cakeflow.traitement', 'plugin.cakeflow.visa'];
    public $Circuit;
    public $Traitement;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->Traitement = ClassRegistry::init('Cakeflow.Traitement');
        $this->Circuit = ClassRegistry::init('Cakeflow.Circuit');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown() {
        unset($this->Circuit);
        unset($this->Traitement);
        parent::tearDown();
    }

    /**
     * hasEtapeDelegation test method
     *
     * @return void
     */
    public function test_delegToParapheur() {
        
    }

    /**
     * test la fonction envoye a, soit la fonction de délégation
     * a une autre personne, la requette a la fonction peut contenir 1 chemin
     * unique parmi 3 choix et 1 une régle de validation parmie 3 choix
     * 
     * $action = 'IL'; //allé retour
     * $action = 'IP'; //allé
     * $action = 'VF'; //allé fin
     * $user_id id utilisaeteur connecté fesant la demande 
     * $delib_id numéro de la délibération voulue
     * $users les ou la cible sous la forme
     * $options[$data] = $user['User']['prenom'] . ' ' . $user['User']['nom'];
     * $etape_type 1: Simple 2: OU 3: ET
     */
    public function test_envoye_Ou_Alle() {
        $nom = 'prenom nom';
        $users = ['0' => '12', '1' => '13'];
        $options = [];
        $i = 0;

        $options['insertion'][$i] = ['Etape' => ['etape_id' => null, 'etape_nom' => $nom, 'etape_type' => 2], 'Visa' => [$i => ['trigger_id' => $users, 'type_validation' => 'V']]];

        $this->Traitement->execute('IP', 13, 1, $options);

        $traitement = $this->Traitement->find('first', ['recursive' => -1, 'fields' => ['id', 'numero_traitement', 'treated'], 'conditions' => ['target_id' => 1]]);

        $visa = $this->Traitement->Visa->find('all', ['recursive' => -1, 'fields' => ['trigger_id', 'etape_nom', 'etape_type', 'action', 'numero_traitement'], 'conditions' => ['(numero_traitement = 2 OR numero_traitement = 3)']]);

        $comTraitement = ['Traitement' => ['id' => 1, 'numero_traitement' => 3, 'treated' => false]];
        $comVisa = [0 => ['Visa' => ['trigger_id' => 13, 'etape_nom' => '1', 'etape_type' => 1, 'action' => 'IP', 'numero_traitement' => 2]], 1 => ['Visa' => ['trigger_id' => 12, 'etape_nom' => $nom, 'etape_type' => 2, 'action' => 'RI', 'numero_traitement' => 3]], 2 => ['Visa' => ['trigger_id' => 13, 'etape_nom' => $nom, 'etape_type' => 2, 'action' => 'RI', 'numero_traitement' => 3]]];

        $this->assertEqual($comTraitement, $traitement);
        $this->assertEqual($comVisa, $visa);
        //debug($visa);
    }

    public function test_executeOuAR() {
        $nom = 'prenom nom';
        $users = ['0' => '12', '1' => '13'];
        $options = [];
        $i = 0;
        $options['insertion'][$i] = ['Etape' => ['etape_id' => null, 'etape_nom' => $nom, 'etape_type' => 2], 'Visa' => [$i => ['trigger_id' => $users, 'type_validation' => 'V']]];

        $this->Traitement->execute('IL', 13, 1, $options);

        $traitement = $this->Traitement->find('first', ['recursive' => -1, 'fields' => ['id', 'numero_traitement', 'treated'], 'conditions' => ['target_id' => 1]]);

        $visa = $this->Traitement->Visa->find('all', ['recursive' => -1, 'fields' => ['trigger_id', 'etape_nom', 'etape_type', 'action', 'numero_traitement'], 'conditions' => ['(numero_traitement = 2 OR numero_traitement = 3 OR numero_traitement = 4)']]);

        $comTraitement = ['Traitement' => ['id' => 1, 'numero_traitement' => 3, 'treated' => false]];
        $comVisa = [0 => ['Visa' => ['trigger_id' => 13, 'etape_nom' => '1', 'etape_type' => 1, 'action' => 'IL', 'numero_traitement' => 2]], 1 => ['Visa' => ['trigger_id' => 12, 'etape_nom' => $nom, 'etape_type' => 2, 'action' => 'RI', 'numero_traitement' => 3]], 2 => ['Visa' => ['trigger_id' => 13, 'etape_nom' => $nom, 'etape_type' => 2, 'action' => 'RI', 'numero_traitement' => 3]], 3 => ['Visa' => ['trigger_id' => 13, 'etape_nom' => '1', 'etape_type' => 1, 'action' => 'RI', 'numero_traitement' => 4]]];

        $this->assertEqual($comTraitement, $traitement);
        $this->assertEqual($comVisa, $visa);
        //debug($visa);
    }

    public function test_executeOuAF() {
        $nom = 'prenom nom';
        $users = ['0' => '12', '1' => '13'];
        $options = [];
        $i = 0;

        $options['insertion'][$i] = ['Etape' => ['etape_id' => null, 'etape_nom' => $nom, 'etape_type' => 2], 'Visa' => [$i => ['trigger_id' => $users, 'type_validation' => 'V']]];

        $this->Traitement->execute('VF', 13, 1, $options);

        $traitement = $this->Traitement->find('first', ['recursive' => -1, 'fields' => ['id', 'numero_traitement', 'treated'], 'conditions' => ['target_id' => 1]]);

        $visa = $this->Traitement->Visa->find('all', ['recursive' => -1, 'fields' => ['trigger_id', 'etape_nom', 'etape_type', 'action', 'numero_traitement'], 'conditions' => ['numero_traitement >= 2 ']]);

        $comTraitement = ['Traitement' => ['id' => 1, 'numero_traitement' => 3, 'treated' => false]];
        $comVisa = [0 => ['Visa' => ['trigger_id' => 13, 'etape_nom' => '1', 'etape_type' => 1, 'action' => 'VF', 'numero_traitement' => 2]], 1 => ['Visa' => ['trigger_id' => 12, 'etape_nom' => $nom, 'etape_type' => 2, 'action' => 'RI', 'numero_traitement' => 3]], 2 => ['Visa' => ['trigger_id' => 13, 'etape_nom' => $nom, 'etape_type' => 2, 'action' => 'RI', 'numero_traitement' => 3]]];
        $this->assertEqual($comTraitement, $traitement);
        $this->assertEqual($comVisa, $visa);
        //debug($visa);
    }

    public function test_executeEtA() {
        $nom = 'prenom nom';
        $users = ['0' => '12', '1' => '13'];
        $options = [];
        $i = 0;
        $options['insertion'][$i] = ['Etape' => ['etape_id' => null, 'etape_nom' => $nom, 'etape_type' => 3], 'Visa' => [$i => ['trigger_id' => $users, 'type_validation' => 'V']]];

        $this->Traitement->execute('IP', 13, 1, $options);

        $traitement = $this->Traitement->find('first', ['recursive' => -1, 'fields' => ['id', 'numero_traitement', 'treated'], 'conditions' => ['target_id' => 1]]);

        $visa = $this->Traitement->Visa->find('all', ['recursive' => -1, 'fields' => ['trigger_id', 'etape_nom', 'etape_type', 'action', 'numero_traitement'], 'conditions' => ['(numero_traitement = 2 OR numero_traitement = 3)']]);

        $comTraitement = ['Traitement' => ['id' => 1, 'numero_traitement' => 3, 'treated' => false]];
        $comVisa = [0 => ['Visa' => ['trigger_id' => 13, 'etape_nom' => '1', 'etape_type' => 1, 'action' => 'IP', 'numero_traitement' => 2]], 1 => ['Visa' => ['trigger_id' => 12, 'etape_nom' => $nom, 'etape_type' => 3, 'action' => 'RI', 'numero_traitement' => 3]], 2 => ['Visa' => ['trigger_id' => 13, 'etape_nom' => $nom, 'etape_type' => 3, 'action' => 'RI', 'numero_traitement' => 3]]];

        $this->assertEqual($comVisa, $visa);
        $this->assertEqual($comTraitement, $traitement);
    }

    public function test_executeEtAR() {
        $nom = 'prenom nom';
        $users = ['0' => '12', '1' => '13'];
        $options = [];
        $i = 0;
        $options['insertion'][$i] = ['Etape' => ['etape_id' => null, 'etape_nom' => $nom, 'etape_type' => 3], 'Visa' => [$i => ['trigger_id' => $users, 'type_validation' => 'V']]];

        $this->Traitement->execute('IL', 13, 1, $options);

        $traitement = $this->Traitement->find('first', ['recursive' => -1, 'fields' => ['id', 'numero_traitement', 'treated'], 'conditions' => ['target_id' => 1]]);

        $visa = $this->Traitement->Visa->find('all', ['recursive' => -1, 'fields' => ['trigger_id', 'etape_nom', 'etape_type', 'action', 'numero_traitement'], 'conditions' => ['(numero_traitement = 2 OR numero_traitement = 3 OR numero_traitement = 4)']]);

        $comTraitement = ['Traitement' => ['id' => 1, 'numero_traitement' => 3, 'treated' => false]];
        $comVisa = [0 => ['Visa' => ['trigger_id' => 13, 'etape_nom' => '1', 'etape_type' => 1, 'action' => 'IL', 'numero_traitement' => 2]], 1 => ['Visa' => ['trigger_id' => 12, 'etape_nom' => $nom, 'etape_type' => 3, 'action' => 'RI', 'numero_traitement' => 3]], 2 => ['Visa' => ['trigger_id' => 13, 'etape_nom' => $nom, 'etape_type' => 3, 'action' => 'RI', 'numero_traitement' => 3]], 3 => ['Visa' => ['trigger_id' => 13, 'etape_nom' => '1', 'etape_type' => 1, 'action' => 'RI', 'numero_traitement' => 4]]];

        $this->assertEqual($comTraitement, $traitement);
        $this->assertEqual($comVisa, $visa);
        //debug($visa);
    }

    public function test_executeEtAF() {
        $nom = 'prenom nom';
        $users = ['0' => '12', '1' => '13'];
        $options = [];
        $i = 0;
        $options['insertion'][$i] = ['Etape' => ['etape_id' => null, 'etape_nom' => $nom, 'etape_type' => 3], 'Visa' => [$i => ['trigger_id' => $users, 'type_validation' => 'V']]];

        $this->Traitement->execute('VF', 13, 1, $options);

        $traitement = $this->Traitement->find('first', ['recursive' => -1, 'fields' => ['id', 'numero_traitement', 'treated'], 'conditions' => ['target_id' => 1]]);

        $visa = $this->Traitement->Visa->find('all', ['recursive' => -1, 'fields' => ['trigger_id', 'etape_nom', 'etape_type', 'action', 'numero_traitement'], 'conditions' => ['numero_traitement >= 2 ']]);

        $comTraitement = ['Traitement' => ['id' => 1, 'numero_traitement' => 3, 'treated' => false]];
        $comVisa = [0 => ['Visa' => ['trigger_id' => 13, 'etape_nom' => '1', 'etape_type' => 1, 'action' => 'VF', 'numero_traitement' => 2]], 1 => ['Visa' => ['trigger_id' => 12, 'etape_nom' => $nom, 'etape_type' => 3, 'action' => 'RI', 'numero_traitement' => 3]], 2 => ['Visa' => ['trigger_id' => 13, 'etape_nom' => $nom, 'etape_type' => 3, 'action' => 'RI', 'numero_traitement' => 3]]];
        $this->assertEqual($comTraitement, $traitement);
        $this->assertEqual($comVisa, $visa);
        //debug($visa);
    }

    public function test_executeSA() {
        $nom = 'prenom nom';
        $users = ['0' => '12'];
        $options = [];
        $i = 0;
        $options['insertion'][$i] = ['Etape' => ['etape_id' => null, 'etape_nom' => $nom, 'etape_type' => 1], 'Visa' => [$i => ['trigger_id' => $users, 'type_validation' => 'V']]];

        $this->Traitement->execute('IP', 13, 1, $options);

        $traitement = $this->Traitement->find('first', ['recursive' => -1, 'fields' => ['id', 'numero_traitement', 'treated'], 'conditions' => ['target_id' => 1]]);

        $visa = $this->Traitement->Visa->find('all', ['recursive' => -1, 'fields' => ['trigger_id', 'etape_nom', 'etape_type', 'action', 'numero_traitement'], 'conditions' => ['(numero_traitement = 2 OR numero_traitement = 3)']]);

        $comTraitement = ['Traitement' => ['id' => 1, 'numero_traitement' => 3, 'treated' => false]];
        $comVisa = [0 => ['Visa' => ['trigger_id' => 13, 'etape_nom' => '1', 'etape_type' => 1, 'action' => 'IP', 'numero_traitement' => 2]], 1 => ['Visa' => ['trigger_id' => 12, 'etape_nom' => $nom, 'etape_type' => 1, 'action' => 'RI', 'numero_traitement' => 3]]];

        $this->assertEqual($comVisa, $visa);
        $this->assertEqual($comTraitement, $traitement);
    }

    public function test_executeSAR() {
        $nom = 'prenom nom';
        $users = ['0' => '12'];
        $options = [];
        $i = 0;
        $options['insertion'][$i] = ['Etape' => ['etape_id' => null, 'etape_nom' => $nom, 'etape_type' => 1], 'Visa' => [$i => ['trigger_id' => $users, 'type_validation' => 'V']]];

        $this->Traitement->execute('IL', 13, 1, $options);

        $traitement = $this->Traitement->find('first', ['recursive' => -1, 'fields' => ['id', 'numero_traitement', 'treated'], 'conditions' => ['target_id' => 1]]);

        $visa = $this->Traitement->Visa->find('all', ['recursive' => -1, 'fields' => ['trigger_id', 'etape_nom', 'etape_type', 'action', 'numero_traitement'], 'conditions' => ['(numero_traitement = 2 OR numero_traitement = 3 OR numero_traitement = 4)']]);

        $comTraitement = ['Traitement' => ['id' => 1, 'numero_traitement' => 3, 'treated' => false]];
        $comVisa = [0 => ['Visa' => ['trigger_id' => 13, 'etape_nom' => '1', 'etape_type' => 1, 'action' => 'IL', 'numero_traitement' => 2]], 1 => ['Visa' => ['trigger_id' => 12, 'etape_nom' => $nom, 'etape_type' => 1, 'action' => 'RI', 'numero_traitement' => 3]], 2 => ['Visa' => ['trigger_id' => 13, 'etape_nom' => '1', 'etape_type' => 1, 'action' => 'RI', 'numero_traitement' => 4]]];

        $this->assertEqual($comTraitement, $traitement);
        $this->assertEqual($comVisa, $visa);
        //debug($visa);
    }

    public function test_executeSAF() {
        $nom = 'prenom nom';
        $users = ['0' => '12'];
        $options = [];
        $i = 0;
        $options['insertion'][$i] = ['Etape' => ['etape_id' => null, 'etape_nom' => $nom, 'etape_type' => 1], 'Visa' => [$i => ['trigger_id' => 12, 'type_validation' => 'V']]];

        $this->Traitement->execute('VF', 13, 1, $options);

        $traitement = $this->Traitement->find('first', ['recursive' => -1, 'fields' => ['id', 'numero_traitement', 'treated'], 'conditions' => ['target_id' => 1]]);

        $visa = $this->Traitement->Visa->find('all', ['recursive' => -1, 'fields' => ['trigger_id', 'etape_nom', 'etape_type', 'action', 'numero_traitement'], 'conditions' => ['numero_traitement >= 2 ']]);

        $comTraitement = ['Traitement' => ['id' => 1, 'numero_traitement' => 3, 'treated' => false]];
        $comVisa = [0 => ['Visa' => ['trigger_id' => 13, 'etape_nom' => '1', 'etape_type' => 1, 'action' => 'VF', 'numero_traitement' => 2]], 1 => ['Visa' => ['trigger_id' => 12, 'etape_nom' => $nom, 'etape_type' => 1, 'action' => 'RI', 'numero_traitement' => 3]]];
        $this->assertEqual($comTraitement, $traitement);
        $this->assertEqual($comVisa, $visa);
        //debug($visa);
    }

}
