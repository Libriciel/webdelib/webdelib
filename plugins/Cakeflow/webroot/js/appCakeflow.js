/**
 * Comportement des checkboxes / master checkbox
 * Select All / Deselect All
 * Initial state
 * @version 1.1
 */

define(["jquery"], function ($) {
    'use strict';

    // Main function
    $.fn.appCakeflow = function (options) {
        // Ensure that only one scrollUp exists
        if (!$.data(document.body, 'appCakeflow')) {
            $.data(document.body, 'appCakeflow', true);
            $.fn.appCakeflow.init(options);
        }
    };

    // Init
    $.fn.appCakeflow.init = function (options) {
        $('#EtapeCptRetard').change(changeCptRetard);
        $('#EtapeCptRetard').keyup(changeCptRetard);

            $.fn.select2.defaults.set( "theme", "bootstrap" );
            $.fn.select2.defaults.set('language', 'fr');
            $(".cakeflow_selectone").select2({
                templateSelection: function (object) {
                    // trim sur la sélection (affichage en arbre)
                    return $.trim(object.text);
                }
            })
            onClickActif($('#CircuitActif').attr('checked'));
    }

    function changeCptRetard(){
        var cpt_current = parseInt($('#EtapeCptRetard').val());
        var cpt_max = parseInt($('#EtapeCptRetard').prop('max'));
        if (cpt_max && cpt_current > cpt_max){
            $('#EtapeCptRetard').val(cpt_max);
            $.jGrowl('<p>Nombre maximum : '+cpt_max+' jours.</p><p>Cette valeur ne peut pas dépasser celle de l\'étape précédente</p>');
        }
    }

    function onClickActif(actifChecked) {
        if (!actifChecked) {
            $('#CircuitDefaut').removeAttr('checked');
        }
    }
//    function writeupdate () {
//        var id_array = Sortable.sequence('etapes');
//        new Ajax.Request('/cakeflow/circuits/reorder/'+ id_array.join(','), {onSuccess: function() {return true;} });
//     }


    $.appCakeflow = $.fn.appCakeflow;
});
