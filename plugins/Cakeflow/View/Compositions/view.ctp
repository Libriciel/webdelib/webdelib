<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Utilisateurs'));
$this->Html->addCrumb(__('Circuits'), ['controller' => 'circuits', 'action' => 'index']);
$this->Html->addCrumb(__('Étapes du circuit'), ['controller' => 'etapes', 'action' => 'index', $etape['Etape']['circuit_id']]);
$this->Html->addCrumb(__('Composition de l\'étape'), $contenuVue['lienRetour']['url']);
$this->Html->addCrumb(__('Vue détaillée '));
// Affichage du titre de la vue
$affichage = $this->Bs->row();
$affichage .= $this->Bs->col('lg12');
if (!empty($contenuVue['titreVue'])) {
    $affichage .= $this->Html->tag('h3', $contenuVue['titreVue']);
}
$affichage .= $this->Bs->close();
$affichage .= $this->Bs->close();
echo $affichage;

$affichage = $this->Bs->row();
$affichage .= $this->Bs->col('lg12');
// Affichage des sections principales
foreach ($contenuVue['sections'] as $section) {
    // affichage du titre de la section
    if (!empty($section['titreSection'])) {
        $affichage .= $this->Html->tag('h4', $section['titreSection']);
    }
    $dl = '';
    // Parcours des lignes de la section
    foreach ($section['lignes'] as $iLigne => $ligne) {
        $dl .= $this->element('viewLigne', ['ligne' => $ligne, 'altrow' => ($iLigne & 1)]);
    }
    $dl = $this->Html->tag('div', $dl, ['class' => 'well']);
    $affichage .= $this->Html->tag('dl', $dl);
}
$affichage .= $this->Bs->close();
echo $affichage;
$affichage = $this->Bs->row();
$affichage .= $this->Bs->col('lg12');
$bouton = $this->Html2->btnCancel($previous);
$affichage .= $this->Html->tag('div', $bouton, ['class' => 'actions']);
$affichage .= $this->Bs->close();
$affichage .= $this->Bs->close();
echo $affichage;
