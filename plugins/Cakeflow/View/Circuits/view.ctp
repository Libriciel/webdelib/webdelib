<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Utilisateurs'));
$this->Html->addCrumb(__('Circuits'), ['controller' => 'circuits', 'action' => 'index']);
$this->Html->addCrumb(__('Vue détaillée'));

echo  $this->Bs->row() . $this->Bs->col('lg12');
if (!empty($contenuVue['titreVue'])) {
    echo $this->Html->tag('h3', $contenuVue['titreVue']);
}
echo $this->Bs->close(2);

echo  $this->Bs->row() . $this->Bs->col('lg8');
foreach ($contenuVue['sections'] as $section) {
    // affichage du titre de la section
    if (!empty($section['titreSection'])) {
        echo $this->Html->tag('h4', $section['titreSection']);
    }
    $dl = '';
    // Parcours des lignes de la section
    foreach ($section['lignes'] as $iLigne => $ligne) {
        $dl .= $this->element('viewLigne', [ 'ligne' => $ligne,  'altrow' => $iLigne ]);
    }
    echo $this->Html->tag('dl', $this->Html->tag('div', $dl, ['class' => 'well']));
}
echo  $this->Bs->close();
$circuit = $this->Bs->col('lg4');
$circuit .= $this->element('circuit');

echo $this->Html->tag('h4', __('Etapes du circuit')).
$this->Html->tag('div', $circuit, ['class' => 'circuit', 'id' => 'etapes']).
$this->Bs->close(2);

echo $this->Bs->row() . $this->Bs->col('lg12') .
$this->Html->tag('div', $this->Html2->btnCancel($previous), ['class' => 'actions']) .
$this->Bs->close(2);
