<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Utilisateurs'));
$this->Html->addCrumb(__('Circuits'), ['controller' => 'circuits', 'action' => 'index']);
echo $this->Html->css('Cakeflow.design.css');
if (empty($this->data['Circuit']['id'])) {
    $action = 'add';
    $titre = __('Créer un circuit de traitement');
} else {
    $action = 'edit';
    $titre = __('Modifier un circuit de traitement : ') . $this->data['Circuit']['nom'];
}
$this->Html->addCrumb($titre);
echo $this->Bs->tag('h3', $titre);
echo $this->BsForm->create(null, ['url'=>['action' => $action]]);
echo $this->BsForm->input('Circuit.nom', ['size' => '100', 'label' => __('Nom')]);
echo $this->BsForm->input('Circuit.description', ['label' => __('Description'), 'cols' => 100, 'rows' => 5]);
echo $this->Html->tag("div", null, ["id" => "ckb_actif"]);
echo $this->BsForm->checkbox('Circuit.actif', [
    'label' => __('Actif'),
    'onClick' => "onClickActif(this.checked);"]);
echo $this->Html->tag('/div', null);
if (Configure::read('Cakeflow.manage.default')) {
    echo $this->BsForm->input('Circuit.defaut', ['label' => __('Circuit par défaut')]);
} else {
    echo $this->BsForm->hidden('Circuit.defaut', ['value' => 0]);
}
echo '<br />';
echo $this->BsForm->hidden('id');
echo $this->Html2->btnSaveCancel('', $previous) .
 $this->BsForm->end();
