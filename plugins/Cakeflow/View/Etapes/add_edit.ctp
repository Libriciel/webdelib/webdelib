<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->pageTitle = ($this->action == 'admin_add' or $this->action == 'manager_add') ? __('Ajouter une étape', true) : __('Modifier l\'étape', true) . ' : ' . $this->data['Etape']['nom'];
echo $this->Html->tag('h3', $this->pageTitle);
$this->Html->addCrumb(__('Liste des circuits'), ['controller' => 'circuits', 'action' => 'index']);
$this->Html->addCrumb(__('Étapes du circuit'), ['action' => 'index', $circuit['Circuit']['id']]);
$this->Html->addCrumb(__('Modification de l\'étape'));

if ($this->action == 'admin_edit' or $this->action == 'manager_edit') {
    echo $this->BsForm->create('Etape', ['url'=> [
        'plugin'=>'cakeflow',
        'controller'=> 'etapes',
        'action'=> 'edit', $this->data['Etape']['id'] ]]);

    echo $this->BsForm->input('Etape.ordre', ['type' => 'hidden']);
    echo $this->BsForm->input('retardInf', ['type' => 'hidden']);
    echo $this->BsForm->input('Etape.id', ['type' => 'hidden']);
} else {
    echo $this->BsForm->create('Etape', ['url'=> [
        'plugin'=>'cakeflow',
        'controller'=> 'etapes',
        'action'=> 'add', $circuit['Circuit']['id']
            ]]);
}

echo $this->BsForm->input('Etape.circuit_id', ['type' => 'hidden']);
echo $this->BsForm->input('Etape.nom', ['required'=>true,'label' => __('Nom', true)]);
echo $this->BsForm->input('Etape.description', ['label' => __('Description', true), 'cols' => 100, 'rows' => 5]);
echo $this->BsForm->select('Etape.type', $types, [
    'label' => __('Type', true),
    'class'=> 'cakeflow_selectone',
    'data-placeholder' => __('Aucun service'),
    'required'=> true,
    'title' => __("- Type d'étape - \nSimple: accord requis \nConcurrent: l'accord d'un seul suffit \nCollaboratif: accord de tous requis")]);
echo $this->BsForm->input('Etape.cpt_retard', [
    'type' => 'number',
    'label' => [
        'text' => __('Nombre de jours avant retard', true),
        'title' => __('Nombre de jours avant la date de la séance pour déclencher l\'alerte de retard')],
    'min' => '0',
    'max' => $retard_max,
    'title' => __('Nombre de jours avant la date de la séance pour déclencher l\'alerte de retard'),
    'help' => __('La date de retard est calculée par rapport à la date de la séance délibérante du projet.') .
    (!empty($retard_max) ? __(' (Maximum : %s jours)', $retard_max) : '')
]);

echo $this->Html2->btnSaveCancel('', $previous) .
$this->BsForm->end();
?>

 <script type="text/javascript">
     require(['domReady'], function (domReady) {
         domReady(function () {
             require(['jquery', '/cakeflow/js/appCakeflow.js'], function ($) {
                 $.appCakeflow();
             });
             $("#CompositionSelectUser").on("change", function () {
                 $('#CompositionTriggerId').val($('#CompositionSelectUser').val());
             })
         });
     });
 </script>
