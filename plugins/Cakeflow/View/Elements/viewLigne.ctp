<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

 /*
   Affiche une ligne dans les vues détaillées view contenant de 1 à 3 éléments max
   Paramètres :
   array $ligne : array(iElement=>array('libelle'=>, 'valeur'=>))
   boolean $altrow : (défaut = false)
   string $valeurDefaut : (défaut = '-')
  */
$altrow = isset($altrow) ? $altrow : false;
$valeurDefaut = isset($valeurDefaut) ? $valeurDefaut : '-';

// Nombre d'éléments de la ligne
$nbElements = count($ligne);

// Nombre d'éléments par ligne max = 3
if ($nbElements == 0 || $nbElements > 3) {
    return;
}

// Affichage des éléments de la ligne
foreach ($ligne as $element) {
    if ($nbElements == 2) {
        echo "<div class='demi'>";
    } elseif ($nbElements == 3) {
        echo "<div class='tiers'>";
    }
    if (!empty($element['libelle'])) {
        if ($altrow || !empty($element['class'])) {
            echo '<dt class="' . ($altrow ? 'altrow ' : null) . '' . (!empty($element['class']) ? $element['class'] : null) . '">' . $element['libelle'] . '</dt>';
        } else {
            echo '<dt>' . $element['libelle'] . '</dt>';
        }
    }
    if (!empty($element['valeur'])) {
        echo '<dd>' . (empty($element['valeur']) ? $valeurDefaut : $element['valeur']) . '</dd>';
    }
    if ($nbElements > 1) {
        echo "</div>";
    }
}
