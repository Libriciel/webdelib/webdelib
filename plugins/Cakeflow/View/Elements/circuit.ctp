<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

if (!empty($etapes)) {
    echo $this->Html->css('/cakeflow/css/circuit');

    $circuit = '';
    foreach ($etapes as $etape) {
        $div_etape = $this->Html->div('nom', '[' . $etape['Etape']['ordre'] . '] - ' . $etape['Etape']['nom']);
        $div_etape.= $this->Html->div('type', $etape['Etape']['libelleType']);
        $utilisateurs = '';
        foreach ($etape['Composition'] as $composition) {
            $typeValidation = Configure::read('Cakeflow.manage.parapheur') ? ', ' . $composition['libelleTypeValidation'] : '';
            $utilisateurs .= $this->Html->div('utilisateur', $composition['libelleTrigger'] . $typeValidation);
        }
        $div_etape .= $this->Html->tag('div', $utilisateurs, ['class' => 'utilisateurs']);
        $circuit .= $this->Html->tag('div', $div_etape, ['class' => 'etape', 'id' => 'etape_' . $etape['Etape']['id']]);
    }

    echo $this->Html->tag('div', $circuit, ['class' => 'circuit', 'id' => 'etapes']);
} else {
    echo $this->Html->tag('div', 'Aucune étape configurée', ['class' => 'circuit', 'id' => 'etapes']);
}

echo $this->Bs->scriptBlock(
    "
    require(['domReady'], function (domReady) {
        domReady(function() {
        require(['jquery', '/cakeflow/js/appCakeflow.js'], function ($) {
                $.appCakeflow();
            });
        });
    });"
);
