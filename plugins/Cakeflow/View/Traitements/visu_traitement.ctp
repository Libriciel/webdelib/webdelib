<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

echo $this->Html->css('/cakeflow/css/circuit');

echo "<div id='etapes' class='circuit'>";

foreach ($etapes as $i=>$etape) {
    echo $this->Html->tag('div', null, ['class'=>'etape', 'id'=>"etape_$i"]);
    $classname = $etape['Etape']['courante'] ? 'courante' : '';
    echo $this->Html->div('nom'.$classname, '['.$i.'] - '.$etape['Etape']['nom']);
    echo $this->Html->div('type', $etape['Etape']['libelleType']);
    echo $this->Html->tag('div', null, ['class'=>'utilisateurs']);
    foreach ($etape['Visa'] as $visa) {
        $icon = null;
        $iconTrigger = '';
        switch ($visa['action']) {
                    case 'OK':
                        $icon = $this->Bs->icon('check');
                        break;
                    case 'KO':
                        $icon = $this->Bs->icon('times');
                        break;
                    case 'IL':
                        $icon = $this->Bs->icon('retweet');
                        break;
                    case 'IP':
                        $icon = $this->Bs->icon('share');
                        break;
                    case 'JP':
                        $icon = $this->Bs->icon('reply');
                        break;
                    case 'JS':
                        $icon = $this->Bs->icon('fast-forward');
                        break;
                    case 'ST':
                        $icon = $this->Bs->icon('stop');
                        break;
                    case 'IN':
                        $icon = $this->Bs->icon('folder-open');
                        break;
                    case 'VF':
                        $icon = $this->Bs->icon('flag-checkered');
                        break;
                }
        if ($icon) {
//            $iconTrigger = $this->Html->image('/cakeflow/img/icons/'.$icon, ['border'=>'0', 'style'=>'height:16px;width:16px;']);
            $iconTrigger = $icon;
        }
        $typeValidation = Configure::read('Cakeflow.manage.parapheur') ? ', '.$visa['libelleTypeValidation'] : '';
        $title = ($visa['action'] == 'RI' ? '' : $visa['libelleAction'].' le '.$this->Time->format("d-m-Y à H:i", $visa['date']));
        echo $this->Html->div('utilisateur', $iconTrigger."&nbsp;".$visa['libelleTrigger'].$typeValidation, ['title'=>$title]);
    }
    echo $this->Html->tag('/div', null);
    echo $this->Html->tag('/div', null);
}

echo $this->Bs->scriptBlock(
    "
    require(['domReady'], function (domReady) {
        domReady(function() {
        require(['jquery', '/cakeflow/js/appCakeflow.js'], function ($) {
                $.appCakeflow();
            });
        });
    });"
);
echo "</div>";
