--
-- Création des circuits
--
CREATE TABLE wkf_circuits (
  id serial primary key,
  nom character varying(250) NOT NULL,
  description text,
  actif boolean DEFAULT true NOT NULL,
  defaut boolean DEFAULT false NOT NULL,
  created_user_id integer,
  modified_user_id integer,
  created timestamp without time zone,
  modified timestamp without time zone
);

--
-- Création des compositions
--
CREATE TABLE wkf_compositions (
  id serial primary key,
  etape_id integer NOT NULL,
  type_validation character varying(1) NOT NULL,
  trigger_id integer,
  created_user_id integer,
  modified_user_id integer,
  created timestamp without time zone,
  modified timestamp without time zone
);

--
-- Création des étapes
--
CREATE TABLE wkf_etapes (
  id serial primary key,
  circuit_id integer NOT NULL,
  nom character varying(250) NOT NULL,
  description text,
  type integer NOT NULL,
  ordre integer NOT NULL,
  created_user_id integer NOT NULL,
  modified_user_id integer,
  created timestamp without time zone,
  modified timestamp without time zone
);

--
-- Création des signatures
--
CREATE TABLE wkf_signatures (
  id serial primary key,
  type_signature character varying(100) NOT NULL,
  signature text NOT NULL
);

--
-- Création des traitements
--
CREATE TABLE wkf_traitements (
  id serial primary key,
  circuit_id integer NOT NULL,
  target_id integer NOT NULL,
  numero_traitement integer DEFAULT 1 NOT NULL,
  treated_orig smallint DEFAULT 0 NOT NULL,
  created_user_id integer,
  modified_user_id integer,
  created timestamp without time zone,
  modified timestamp without time zone,
  treated boolean
);

--
-- Création des visas
--
CREATE TABLE wkf_visas (
  id serial primary key,
  traitement_id integer NOT NULL,
  trigger_id integer NOT NULL,
  signature_id integer,
  etape_nom character varying(250),
  etape_type integer NOT NULL,
  action character varying(2) NOT NULL,
  commentaire TEXT,
  date timestamp without time zone,
  numero_traitement integer NOT NULL,
  type_validation character varying(1) NOT NULL
);

--
-- Ajout des indexes
--
CREATE INDEX created_user_id ON wkf_circuits ("created_user_id");
CREATE INDEX modified_user_id ON wkf_circuits ("modified_user_id");
CREATE INDEX etape_id ON wkf_compositions ("etape_id");
CREATE INDEX trigger ON wkf_compositions ("trigger_id");
CREATE INDEX circuit_id ON wkf_etapes ("circuit_id");
CREATE INDEX nom ON wkf_etapes ("nom");
CREATE INDEX circuits ON wkf_traitements ("circuit_id");
CREATE INDEX target ON wkf_traitements ("target_id");
CREATE INDEX traitements_treated ON wkf_traitements ("treated_orig");
CREATE INDEX wkf_visas_traitements ON wkf_visas ("traitement_id");