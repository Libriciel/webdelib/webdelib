DROP TABLE IF EXISTS modelsections, modelvalidations, modelvariables, modeltypes CASCADE;
DROP SEQUENCE IF EXISTS modelsections_id_seq, modelvalidations_id_seq, modelvariables_id_seq, modeltypes_id_seq CASCADE;
DROP INDEX IF EXISTS aro_aco_key,idx_acos_lft_rght,idx_acos_alias,idx_aros_lft_rght,idx_aros_alias,idx_aco_id;