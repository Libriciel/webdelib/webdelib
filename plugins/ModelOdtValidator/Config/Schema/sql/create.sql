CREATE TABLE IF NOT EXISTS modeltypes (
  id serial primary key,
  name varchar(255),
  description varchar(255),
  created timestamp without time zone NOT NULL,
  modified timestamp without time zone NOT NULL
);

CREATE TABLE IF NOT EXISTS modeltemplates (
  id serial primary key,
  modeltype_id integer references modeltypes(id) DEFAULT 1,
  name varchar(255) not null,
  filename varchar(255) not null,
  filesize integer,
  content bytea not null,
  created timestamp without time zone NOT NULL,
  modified timestamp without time zone NOT NULL
);

CREATE TABLE IF NOT EXISTS modelsections (
  id serial primary key,
  name varchar(255),
  description varchar(255),
  parent_id integer references modelsections(id) DEFAULT 1,
  created timestamp without time zone NOT NULL,
  modified timestamp without time zone NOT NULL
);
CREATE TABLE IF NOT EXISTS modelvariables (
  id serial primary key,
  name varchar(255),
  description varchar(255),
  created timestamp without time zone NOT NULL,
  modified timestamp without time zone NOT NULL
);
CREATE TABLE IF NOT EXISTS modelvalidations (
  id serial primary key,
  modelvariable_id integer references modelvariables(id),
  modelsection_id integer references modelsections(id) NOT NULL,
  modeltype_id integer references modeltypes(id) NOT NULL,
  min integer default 0,
  max integer default null,
  actif bool DEFAULT true
);