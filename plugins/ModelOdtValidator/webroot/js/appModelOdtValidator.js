/**
 * appModelOdtValidator
 * @version 5.0
 */

define(["jquery"], function ($) {
    'use strict';

    // Main function
    $.fn.appModelOdtValidator = function (options) {
        // Ensure that only one scrollUp exists
        if (!$.data(document.body, 'appModelOdtValidator')) {
            $.data(document.body, 'appModelOdtValidator', true);
            $.fn.appModelOdtValidator.init(options);
        }
    };

    // Init
    $.fn.appModelOdtValidator.init = function (options) {

        var object = $.fn.appModelOdtValidator.settings = $.extend({}, $.fn.appModelOdtValidator.defaults, options),
                $self;

        $('#ModeltemplateModeltypeId').on('change', function () {
          $.ajax({
              type: 'GET',
              url: '/admin/model_odt_validator/modeltemplates/getNamesFileOutputAjax/'+ $(this).val(),
              success: function (options) {
                if (!_.isEmpty(options)) {
                  var data = _.map(options, function (element, number) {
                    return {
                      id: number,
                      text: element
                    }
                  });
                  $("#ModeltemplateAideformatOptions option").remove();

                  $('#ModeltemplateAideformatOptions').select2('destroy');
                  $('#ModeltemplateAideformatOptions').select2({
                      data: data,
                    });
                }
              },
              error: function (XMLHttpRequest, textStatus, errorThrown) {
                $.growl({
                    title: '<strong>Erreur: </strong>',
                    message: 'Erreur de récupération de la liste des collectivités, veuillez vérfier les informations de connexion',
                  },
                  { type: 'danger' }
              );

                //console.log(XMLHttpRequest);
                console.log(errorThrown);
                //console.log(textStatus);
              }
          });
        });

        $('#ModeltemplateModeltypeId').change();

        $('#ModeltemplateAideformatOptions').change(function(){
            InsertSelectedValueInToInput($(this))
        });
    }

    /**
     *  Insère la valeur de l'élément sélectionné du champ select 'select_element'
     * dans le champ input 'input_name' du formulaire 'form_name'
     * a l'endroit du curseur ou remplace le texte sélectionné
     * @param {type} e
     * @param select_element Objet champ select
     * @param form_name Nom du formulaire contenant le champ input concerné
     * @param input_name Nom du champ input
     * @returns {undefined}
     *
     */
    function InsertSelectedValueInToInput(e) {
        var input_element = document.getElementById(e.attr('data-file-output-format'));
        input_element.focus();

        /* pour Internet Explorer */
        if (typeof document.selection != 'undefined') {
            /* Insertion du code de formatage */
            var range = document.selection.createRange();
            var insText = range.text;
            range.text = e.val();
            /* Ajustement de la position du curseur */
            range.select();
        }
        /* pour navigateurs basés sur Gecko*/
        else if (typeof input_element.selectionStart != 'undefined')
        {
            /* cas particulier du critère de réinitialisation */
            if (e.attr('data-sortie-def')  === 'CompteurDefReinit') {
                input_element.value = e.val();

            /* Insertion du code de formatage */
            } else {
                var start = input_element.selectionStart;
                var end = input_element.selectionEnd;
                input_element.value = input_element.value.substr(0, start) + e.val() + input_element.value.substr(end);
                /* Ajustement de la position du curseur */
                var pos = start + e.val().length;
                input_element.selectionStart = pos;
                input_element.selectionEnd = pos;
            }
        };

        e.selectedIndex = 0;
    };

    // Defaults
    $.fn.appModelOdtValidator.defaults = {
    };

    $.appModelOdtValidator = $.fn.appModelOdtValidator;
});
