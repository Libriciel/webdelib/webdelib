<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('ModelOdtValidatorAppModel', 'ModelOdtValidator.Model');

/**
 * [Modeltype description]
 */
class Modeltype extends ModelOdtValidatorAppModel
{
    public $hasMany = [
        'Modeltemplate' => [
            'className' => 'ModelOdtValidator.Modeltemplate',
            'foreignKey' => 'modeltype_id',
            'order' => 'Modeltemplate.created DESC',
            'dependent' => true
        ],
        'ModelOdtValidator.Modelvalidation'
    ];

    /**
     * @var array Règles de validation des enregistrements
     */
    public $validate = [
        'name' => [
            'isUnique' => [
                'rule' => 'isUnique',
                'allowEmpty' => false,
                'message' => 'Ce nom est déjà utilisé.'
            ],
            'maxLength' => [
                'rule' => ['maxLength', 255],
                'message' => 'Le nom ne doit pas dépasser 255 caractères.'
            ]
        ],
        'description' => [
            'rule' => ['maxLength', 255],
            'message' => 'La description ne doit pas dépasser 255 caractères.'
        ]
    ];

    /**
     * Trouve l'id d'un type grace à son nom
     * @param string $name nom du type
     * @return integer id du type
     */
    public function findIdByName($name)
    {
        $modeltype = $this->find('first', [
            'recursive' => -1,
            'fields' => ['Modeltype.id'],
            'conditions' => ['Modeltype.name' => $name]
        ]);
        if (empty($modeltype['Modeltype']['id'])) {
            return false;
        }
        return $modeltype['Modeltype']['id'];
    }

    /**
     * Trouve le nom d'un type grace à son id
     * @param integer $id id du type
     * @return string nom du type
     */
    public function findNameById($id)
    {
        $modeltype = $this->find('first', [
            'recursive' => -1,
            'fields' => ['Modeltype.name'],
            'conditions' => ['Modeltype.id' => $id]
        ]);
        if (empty($modeltype['Modeltype']['name'])) {
            return false;
        }
        return $modeltype['Modeltype']['name'];
    }
}
