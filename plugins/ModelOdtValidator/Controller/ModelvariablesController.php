<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [ModelvariablesController description]
 */
class ModelvariablesController extends ModelOdtValidatorAppController
{
    public $components = [
            'Auth' => [
            'mapActions' => [
                'allow' => ['admin_index','admin_add','admin_delete','admin_edit','admin_view'],
        ]
    ]];

    public function admin_edit($id = null)
    {
        if (!empty($id)) {
            if (empty($this->request->data)) {
                $this->request->data = $this->Modelvariable->find('first', ['conditions' => ['Modelvariable.id' => $id]]);
            } else {
                $conditions = [
                    'NOT' => ['Modelvariable.id' => $id],
                    'Modelvariable.name' => $this->request->data['Modelvariable']['name']
                ];
                if (!$this->Modelvariable->hasAny($conditions)) {
                    $this->Modelvariable->id = $id;
                    if ($this->Modelvariable->save($this->request->data)) {
                        $this->Flash->set("Variable enregistrée avec succès.", ['element' => 'growl','params'=>['type' => 'success']]);
                        return $this->redirect($this->previous);
                    } else {
                        $this->Flash->set("Erreur lors de l'enregistrement de la variable.", ['element' => 'growl','params'=>['type' => 'error']]);
                    }
                } else {
                    $this->Flash->set("Une variable de ce nom existe déjà.", ['element' => 'growl','params'=>['type' => 'danger']]);
                }
            }
        } else {
            $this->Flash->set("Veuillez spécifier la variable à modifier", 'growl');
            return $this->redirect(['action' => 'index']);
        }
    }

    public function admin_add()
    {
        if (!empty($this->request->data)) {
            if (!$this->Modelvariable->hasAny(['Modelvariable.name' => $this->request->data['Modelvariable']['name']])) {
                $maxid = $this->Modelvariable->find('first', [
                    'recursive' => -1,
                    'fields' => ['id'],
                    'order' => ['id DESC']
                ]);
                $this->request->data['Modelvariable']['id'] = $maxid['Modelvariable']['id'] + 1;
                $this->Modelvariable->create();
                if ($this->Modelvariable->save($this->request->data)) {
                    $this->Flash->set("Variable créée avec succès.", ['element' => 'growl','params'=>['type' => 'success']]);
                    return $this->redirect($this->previous);
                } else {
                    $this->Flash->set("Erreur lors de l'enregistrement de la variable.", ['element' => 'growl','params'=>['type' => 'error']]);
                }
            } else {
                $this->Flash->set("Une variable de ce nom existe déjà.", 'growl');
            }
        }
        $this->render('admin_edit');
    }

    public function admin_index()
    {
        $conditions = [];
        if (!empty($this->request->data['Modelvariable']['nom'])) {
            $conditions['Modelvariable.id'] = $this->request->data['Modelvariable']['nom'];
        }
        $data = $this->Modelvariable->find('all', [
            'recursive' => -1,
            'conditions' => $conditions,
            'order' => 'name ASC'
        ]);
        $this->set('data', $data);
        $this->set('names', $this->Modelvariable->find('list'));
    }

    public function admin_delete($id)
    {
        if ($this->Modelvariable->delete($id, false)) {
            $this->Flash->set("La variable $id à été supprimé", 'growl');
        } else {
            $this->Flash->set("Impossible de supprimer la variable", ['element' => 'growl','params'=>['type' => 'error']]);
        }
        return $this->redirect($this->previous);
    }
}
