<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('phpOdtApi', 'ModelOdtValidator.Lib');
App::uses('File', 'Utility');

/**
 * [ModelvalidationsController description]
 */
class ModelvalidationsController extends ModelOdtValidatorAppController
{
    public $components = [
            'Auth' => [
            'mapActions' => [
                'read' => ['admin_index','admin_add','admin_delete','admin_edit','admin_view', 'admin_generate'],
        ]
    ]];

    public function admin_edit($id = null)
    {
        if (!empty($id)) {
            if (empty($this->request->data)) {
                $this->request->data = $this->Modelvalidation->find('first', [
                    'recursive' => 1,
                    'conditions' => [
                        'Modelvalidation.id' => $id,
                    ]
                ]);
            } else {
                if (empty($this->request->data['Modelvalidation']['modeltype_id'])) {
                    $this->request->data['Modelvalidation']['modeltype_id'] = 1;
                }
                if (empty($this->request->data['Modelvalidation']['modelsection_id'])) {
                    $this->request->data['Modelvalidation']['modelsection_id'] = 1;
                }
                if (empty($this->request->data['Modelvalidation']['modelvariable_id'])) {
                    $this->request->data['Modelvalidation']['modelvariable_id'] = null;
                }
                $exist = $this->Modelvalidation->find('count', [
                    'conditions' => [
                        'Modelvalidation.modeltype_id' => $this->request->data['Modelvalidation']['modeltype_id'],
                        'Modelvalidation.modelsection_id' => $this->request->data['Modelvalidation']['modelsection_id'],
                        'Modelvalidation.modelvariable_id' => $this->request->data['Modelvalidation']['modelvariable_id'],
                    ]
                ]);

                if (empty($exist)) {
                    $this->Modelvalidation->id = $id;
                    if ($this->Modelvalidation->save($this->request->data)) {
                        $this->Flash->set("Règle enregistrée avec succès", ['element' => 'growl','params'=>['type' => 'success']]);
                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->set("Erreur lors de l'enregistrement de la règle", ['element' => 'growl','params'=>['type' => 'error']]);
                    }
                } else {
                    $this->Flash->set("Une règle existe déjà pour cette combinaison type/section/variable", ['element' => 'growl','params'=>['type' => 'danger']]);
                }
            }
            $this->set('variables', $this->Modelvalidation->Modelvariable->find('list'));
            $this->set('sections', $this->Modelvalidation->Modelsection->find('list', [
                'conditions' => ['Modelsection.id !=' => 1]
            ]));
            $this->set('types', $this->Modelvalidation->Modeltype->find('list', [
                'conditions' => ['Modeltype.id !=' => 1]
            ]));
        } else {
            $this->Flash->set("Veuillez spécifier la règle à modifier", ['element' => 'growl','params'=>['type' => 'error']]);
            return $this->redirect(['action' => 'index']);
        }
    }

    public function admin_add()
    {
        if (!empty($this->request->data)) {
            if (empty($this->request->data['Modelvalidation']['modeltype_id'])) {
                $this->request->data['Modelvalidation']['modeltype_id'] = 1;
            }
            if (empty($this->request->data['Modelvalidation']['modelsection_id'])) {
                $this->request->data['Modelvalidation']['modelsection_id'] = 1;
            }
            if (empty($this->request->data['Modelvalidation']['modelvariable_id'])) {
                $this->request->data['Modelvalidation']['modelvariable_id'] = null;
            }

            $exist = $this->Modelvalidation->find('count', [
                'conditions' => [
                    'Modelvalidation.modeltype_id' => $this->request->data['Modelvalidation']['modeltype_id'],
                    'Modelvalidation.modelsection_id' => $this->request->data['Modelvalidation']['modelsection_id'],
                    'Modelvalidation.modelvariable_id' => $this->request->data['Modelvalidation']['modelvariable_id'],
                ]
            ]);
            if (empty($exist)) {
                $this->Modelvalidation->create();
                if ($this->Modelvalidation->save($this->request->data)) {
                    $this->Flash->set("Règle créée avec succès", ['element' => 'growl','params'=>['type' => 'success']]);
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->set("Erreur lors de l'enregistrement de la règle.", ['element' => 'growl','params'=>['type' => 'error']]);
                }
            } else {
                $this->Flash->set("Une règle existe déjà pour cette combinaison type/section/variable.", ['element' => 'growl','params'=>['type' => 'danger']]);
            }
        }
        $this->set('variables', $this->Modelvalidation->Modelvariable->find('list'));
        $this->set('sections', $this->Modelvalidation->Modelsection->find('list', [
            'conditions' => ['Modelsection.id !=' => 1]
        ]));
        $this->set('types', $this->Modelvalidation->Modeltype->find('list', [
            'conditions' => ['Modeltype.id !=' => 1]
        ]));
        $this->render('admin_edit');
    }

    public function admin_index()
    {
        $conditions = [];
        if (!empty($this->request->data['Filtre'])) {
            if (!empty($this->request->data['Filtre']['modeltype_id'])) {
                $conditions['Modelvalidation.modeltype_id'][] = $this->request->data['Filtre']['modeltype_id'];
            }
            if (!empty($this->request->data['Filtre']['modelsection_id'])) {
                $conditions['Modelvalidation.modelsection_id'][] = $this->request->data['Filtre']['modelsection_id'];
            }
            if (!empty($this->request->data['Filtre']['modelvariable_id'])) {
                $conditions['Modelvalidation.modelvariable_id'][] = $this->request->data['Filtre']['modelvariable_id'];
            }
        }
        $datas = $this->Modelvalidation->find('all', [
            'recursive' => 1,
            'conditions' => $conditions,
            'order' => 'modeltype_id ASC'
        ]);

        $this->set('datas', $datas);

        $this->set('variables', $this->Modelvalidation->Modelvariable->find('list'));
        $this->set('sections', $this->Modelvalidation->Modelsection->find('list'));
        $this->set('types', $this->Modelvalidation->Modeltype->find('list'));
    }

    public function admin_delete($id)
    {
        if ($this->Modelvalidation->delete($id, false)) {
            $this->Flash->set("La règle $id à été supprimé", 'growl');
        } else {
            $this->Flash->set("Impossible de supprimer la règle de validation", ['element' => 'growl','params'=>['type' => 'error']]);
        }
        return $this->redirect($this->previous);
    }

    /**
     * Permet la création de plusieurs règles de validation
     * en fonction de variables/sections/types/min/max
     * et affiche le code sql correspondant
     */
    public function admin_generate()
    {
        if (!empty($this->request->data['Modelvalidation']['sql_commands'])) {
            $commands = trim($this->request->data['Modelvalidation']['sql_commands']);
            $querys = explode("\n", $commands);
            try {
                $this->Modelvalidation->begin();
                foreach ($querys as $query) {
                    $this->Modelvalidation->query(trim($query));
                }
                $this->Modelvalidation->commit();
                $this->Flash->set("Les règles de validation ont été ajoutées dans la base de données", 'growl');
            } catch (Exception $e) {
                $this->Modelvalidation->rollback();
                $this->Flash->set($e->getMessage(), ['element' => 'growl','params'=>['type' => 'error']]);
            }
        }
        $this->set('variables', $this->Modelvalidation->Modelvariable->find('list'));
        $this->set('sections', $this->Modelvalidation->Modelsection->find('list', []));
        $this->set('types', $this->Modelvalidation->Modeltype->find('list', [
            'conditions' => ['Modeltype.id !=' => 1]
        ]));
    }
}
