<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [FidoComponent description]
 */
class FidoComponent extends Component
{

    /**
     * @var array formats reconnus par l'application
     */
    public $formats;

    /**
     * @var array résultats de la derniere analyse
     */
    public $lastResults;

    /**
     * Constructeur
     * Chargement de la librairie Fido pour analyse de fichier
     */
    public function __construct()
    {
        $this->formats = Configure::read("DOC_TYPE");
        $this->lastResults = [];
        App::import('Lib', 'ModelOdtValidator.Fido');
    }

    /**
     * Analyse d'un fichier par fido
     * @param $file
     * @return array|bool
     */
    public function analyzeFile($file)
    {
        $this->lastResults = Fido::analyzeFile($file);
        if ($this->lastResults['result'] == 'OK') {
            $this->_getDetails();
            return $this->lastResults;
        } else {
            return false;
        }
    }

    /**
     * @param string $mime
     * @param string $puid
     * @return bool format activé
     */
    private function _checkFormat($mime, $puid)
    {

        //Fido ne trouve pas le typemime du fichier mais il y a bien le puid
        if (empty($mime)) {
            foreach ($this->formats as $key_format => $format) {
                foreach ($format['puid'] as $key_format_puid => $format_puid) {
                    if ($key_format_puid===$puid) {
                        $this->lastResults['mimetype']=$key_format;
                        return $this->formats[$key_format]['puid'][$puid]['actif'];
                    }
                }
            }

            return false;
        }

        if (isset($this->formats[$mime]['puid'][$puid])) {
            return $this->formats[$mime]['puid'][$puid]['actif'];
        } else {
            return false;
        }
    }

    /**
     * @param $file
     * @return bool
     */
    public function checkFile($file)
    {
        $this->lastResults = Fido::analyzeFile($file);
        if ($this->lastResults['result'] == 'OK') {
            $this->_getDetails();
            return $this->_checkFormat($this->lastResults['mimetype'], $this->lastResults['puid']);
        } else {
            return false;
        }
    }

    /**
     * Résultats analyse fichier avec format actif ou non
     * @return array results
     */
    private function _getDetails()
    {
        //Si le type mime est repertorié
        if (array_key_exists($this->lastResults['mimetype'], $this->formats)) {

            //Extrait les infos de la config
            $configDetails = $this->formats[$this->lastResults['mimetype']];

            //Retire la branche avec les puid
            $configDetails = Hash::remove($configDetails, 'puid');
            if (array_key_exists($this->lastResults['puid'], $this->formats[$this->lastResults['mimetype']]['puid'])) {
                $this->lastResults['actif'] = $this->formats[$this->lastResults['mimetype']]['puid'][$this->lastResults['puid']]['actif'];
            } else {
                $this->log($this->lastResults, 'error');
                $this->lastResults['actif'] = false;
            }
            $this->lastResults = Hash::merge($this->lastResults, $configDetails);
        } else { //Type mime non repertorié dans la liste
            $this->lastResults['actif'] = false;
        }

        return $this->lastResults;
    }
}
