<?php

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb('Modèles d\'édition', ['action'=>'index']);

if ($this->action == 'admin_edit') {
    echo $this->Bs->tag('h3', __('Modifier le modèle', $this->data['Modeltemplate']['name']));
    $this->Html->addCrumb(__('Modifier le modèle', $this->data['Modeltemplate']['name']));
} else {
    echo $this->Bs->tag('h3', __('Ajouter un modèle'));
    $this->Html->addCrumb(__('Ajouter un modèle'));
}

echo $this->BsForm->create('Modeltemplate', [
    'url'=>['admin'=>true, 'prefix'=>'admin','plugin'=>'model_odt_validator', 'controller'=>'modeltemplates','action'=> $this->action],
    'type' => 'file']);

echo $this->BsForm->input('Modeltemplate.name', [
  'label' => __('Libellé') . ' <abbr title="obligatoire">*</abbr>',
  'placeholder' => 'Nom du modèle']);
echo $this->BsForm->select('Modeltemplate.modeltype_id', $modeltypes, [
    'label' => __('Type de modèle') . ' <abbr title="obligatoire">*</abbr>',
    'class'=>'selectone'
    ]);
echo $this->BsForm->checkbox('Modeltemplate.force', [
  'autocompete' => false,
  'title' => __('Activation'),
  'label' => __('Forcer le modèle'),
  'help' => __('La vérification des variables n\'est pas prise en compte pour ce modèle.')
]);

echo $this->BsForm->input('Modeltemplate.file_output_format', [
    'label' => __('Nommage du fichier de sortie'),
    'maxlength' => '100']);

$this->BsForm->setLeft(3);
$this->BsForm->setRight(9);
echo $this->BsForm->select('aideformatOptions', $aideformatOptions, [
    'label' => false,
    'class' => 'selectone',
    'data-placeholder' => __('Cliquer ici pour ajouter une définition'),
    'empty' => true,
    'inline' => true,
    'data-file-output-format'=> 'ModeltemplateFileOutputFormat',
    'autocomplete' => 'off',
    'escape' => false]);
$this->BsForm->setDefault();

if ($this->action == 'admin_edit') {
    if (empty($this->validationErrors['Modeltemplate']['fileupload'])) {
        echo $this->Bs->row(['id' => 'originalDocument']);
        echo $this->Bs->col('xs'.$this->BsForm->getLeft()).$this->Bs->div('text-right', '<strong>'. __('Fichier')).'</strong>'.$this->Bs->close();
        echo $this->Bs->col('xs'.$this->BsForm->getRight());
        echo $this->Html->Link($this->Bs->icon('file-alt').' '.$this->data['Modeltemplate']['filename'], ['action' => 'download', $this->data['Modeltemplate']['id']], [
            'title' => __('Télécharger le document'),
            'escapeTitle'=> false]);
        echo $this->Bs->btn($this->Bs->icon('eraser').' '.__('Remplacer le document'), null, [
            'id' => 'replaceDocument',
            'tag'=>'button',
            'type' => 'danger',
            'escapeTitle'=> false,
            ]);
        echo $this->Bs->close(2);
    }
}
?>

<span id="fileUpload">
<?php
echo $this->BsForm->input('fileupload', [
                'type' => 'file',
                'label' => __('Fichier') . ' <abbr title="obligatoire">*</abbr>',
                'data-btnClass' => 'btn-primary',
                'data-placeholder' => __('Pas de fichier'),
                'data-text' => ' ' . __('Choisir un fichier') ,
                'data-icon' => $this->Bs->icon('folder-open'),
                'data-badge' => 'true',
                'class' => 'filestyle',
                'help'=> 'Modèle de fichier (odt)',
            ]);
 ?>
</span>
<?php
if (!empty($validation['errors'])) {
     echo $this->Html->tag('hr');
     echo $this->Html->tag('div', null, ['id' => 'validationErrors']);
     echo $this->Html->tag('h3', 'Erreurs du modèle');
     echo $this->Html->tag('ul', null);
     foreach ($validation['errors'] as $error) {
         echo $this->Html->tag('li', $error);
     }
     echo $this->Html->tag('/ul');
     echo $this->Html->tag('/div', null);
 }

echo $this->BsForm->hidden('Modeltemplate.id');
echo $this->Html->tag('br');
   echo $this->Html2->btnSaveCancel('', $previous);
 echo $this->BsForm->end();

 echo $this->Bs->scriptBlock(
     "
     require(['domReady'], function (domReady) {
         domReady(function() {
         require(['jquery', '/ModelOdtValidator/js/appModelOdtValidator.js'], function ($) {
                 $.appModelOdtValidator();
             });
         });
     });"
 );
 ?>
<?php if ($this->action == 'admin_edit' && empty($this->validationErrors['Modeltemplate']['fileupload'])): ?>
    <script type="application/javascript">
        require(['domReady'], function (domReady) {
    domReady(function () {

            $('#ModeltemplateFileupload').prop('disabled', true);
            $('#replaceDocument').click(function () {
                $('#fileUpload').show();
                $('#ModeltemplateFileupload').prop('disabled', false);
                $('#originalDocument').remove();
                return false;
            });
        });
});
    </script>
<?php endif; ?>
<style>
    label {
        line-height: 28px;
    }

    #validationErrors {
        padding: 10px;
        background-color: whitesmoke;
        border: 2px dotted red;
    }

    <?php if ($this->action == 'admin_edit' && empty($this->validationErrors['Modeltemplate']['fileupload'])): ?>
    #fileUpload {
        display: none;
    }

    <?php endif; ?>
    #replaceDocument {
        cursor: pointer;
        margin-left: 20px;
    }
</style>
