<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Règles de validation des modèles'), ['admin' => true,'prefix'=> 'admin', 'plugin'=>'model_odt_validator', 'controller'=>'modelvalidations', 'action'=>'index']);
$this->Html->addCrumb(__('Sections de modèles'), ['admin' => true,'prefix'=> 'admin', 'plugin'=>'model_odt_validator', 'controller'=>'Modelsections', 'action'=>'index']);

$this->Html->addCrumb('Sections de modèles');
?>
<?php if ($this->action == 'add'): ?>
    <h2>Création d'une nouvelle section de modèle</h2>
<?php else: ?>
    <h2>Modifier une section de modèle</h2>
<?php endif; ?>

<?php
echo $this->BsForm->create('Modelsection');
if ($this->action == 'edit') {
    echo $this->Form->hidden('id');
}
echo $this->BsForm->input('name', ['label' => 'Nom']);
echo $this->BsForm->input('description', ['label' => 'Description', 'type' => 'textarea']);
echo $this->BsForm->input('parent_id', ['label' => 'Section parent', 'options' => $parents, 'empty' => true]);
echo $this->Html2->btnSaveCancel();
echo $this->BsForm->end();
?>
<script>
    $('#ModelsectionParentId').select2({
        width:'resolve',
        placeholder: "Aucune contrainte (Document)",
        allowClear: true
    });
</script>
