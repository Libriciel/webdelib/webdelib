<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Règles de validation des modèles'), ['admin' => true,'prefix'=> 'admin', 'plugin'=>'model_odt_validator', 'controller'=>'modelvalidations', 'action'=>'index']);


 ?>
<?php<?php if ($this->action == 'add'): ?>
    <h2>Création d'une nouvelle variable de modèle</h2>
<?php else: ?>
    <h2>Modifier une variable de modèle</h2>
<?php endif; ?>

<?php
echo $this->BsForm->create('Modelvariable', ['url'=> [
        'controller' => 'Modelvariables',
        'action' => $this->action,
        'plugin'=>'model_odt_validator',
        'prefix'=>'admin']]);
if ($this->action == 'edit') {
    echo $this->BsForm->hidden('id');
}
echo $this->BsForm->input('name', ['label' => 'Nom']);
echo $this->BsForm->input('description', ['label' => 'Description', 'type' => 'textarea']);
?>
    <hr/>
<?php

echo $this->Html2->btnSaveCancel(null, $previous);

echo $this->BsForm->end();
?>
