<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

//include_once('../Lib/phpOdtApi.php');

//function recurseTree($var){
//    $out = '';
//    foreach($var as $v){
//        if(is_array($v)){
//            $out .= '<ul>'.recurseTree($v).'</ul>';
//        }else{
//            $out .= '<li>'.$v.'</li>';
//        }
//    }
//    return $out;
//}
//
//echo '<ul>Fichiers scannés : ';
////Table des matières
//foreach (scandir ( './files/' ) as $file){
//    if ($file[0] != '.')
//        echo '<li><a href="#'.$file.'">'.$file.'</a></li>';
//}
//echo '</ul>';


echo "<h4>Sections</h4>";
echo "<ul>";
foreach ($sections as $section) {
    echo "<li>$section</li>";
}
echo "</ul>";

echo "<h4>Variables</h4>";
echo "<ul>";
foreach ($variables as $var) {
    echo "<li>$var</li>";
}
echo "</ul>";
