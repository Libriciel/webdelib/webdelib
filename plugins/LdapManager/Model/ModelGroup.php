<?php

class ModelGroup extends LdapManagerAppModel
{
    public $tablePrefix = 'ldapm_';
    
    public $useTable = 'models_groups';
    
    public $hasOne = ['Group' => ['className' => 'Group', 'foreignKey' => false, 'conditions' => ['Group.id = ModelGroup.ldapm_groups_id'], 'dependent' => false]];
    
}
