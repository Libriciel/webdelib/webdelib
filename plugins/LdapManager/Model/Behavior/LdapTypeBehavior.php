<?php

class LdapTypeBehavior extends ModelBehavior
{

    public array $mapLdapKeyWord = [];
    
    public $ldapSource = [];

    public function setup(Model $model, $config = []): void
    {
        if (empty($this->mapLdapKeyWord)) {
            $this->ldapSource=$model->getDataSource();

            $model->useTable ='';

            if (!empty($this->ldapSource->config['type'])) {
                switch ($this->ldapSource->config['type']) {
                    case 'OpenLDAP':
                        $this->primaryKey = 'cn';
                        break;
                    case 'ActiveDirectory':
                        $this->primaryKey = 'dn';
                        $this->setMappingsActiveDirectory();
                        break;
                    
                    case 'OracleInternetDirectory':
                        $this->primaryKey = 'dn';
                        break;
                    default:
                        break;
                }
            }
        }
    }
    
    private function setMappingsActiveDirectory(): void
    {

        $this->mapLdapKeyWord = [
            'objectClass'=>'objectCategory',
            'groupOfNames'=>'group',
            'cn'=>'samaccountname',
            'inetOrgPerson'=>'user',
            'uid'=> $this->ldapSource->config['fields']['User']['username']];
    }
    
    public function getKeyWord(Model $model, $key)
    {
        if (!empty($this->mapLdapKeyWord[$key])) {
            return $this->mapLdapKeyWord[$key];
        }

        return $key;
    }
    
    public function getType(Model $model)
    {
        return $this->ldapSource->config['type'];
    }
    
    public function getFilter(Model $model)
    {
        
        return $this->ldapSource->config['filter'];
    }
    
    public function getFieldsUser(Model $model): array
    {
        $user_fields = [];
        foreach ($this->ldapSource->config['fields']['User'] as $field) {
            if (!empty($field)) {
                $user_fields[]=$field;
            }
        }
        
        return $user_fields;
    }
    
    public function getFieldsUserActive(Model $model)
    {
        return $this->ldapSource->config['fields']['User']['active'];
    }
    
    public function getFieldsUserMap(Model $model): array
    {
        
        return array_flip($this->ldapSource->config['fields']['User']);
    }
}
