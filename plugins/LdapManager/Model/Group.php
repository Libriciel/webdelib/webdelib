<?php

class Group extends LdapManagerAppModel
{
    public $tablePrefix = 'ldapm_';
    
    public $actsAs = ['Tree'];
    
    public $hasMany = ['ModelGroup' => ['className' => 'LdapManager.ModelGroup', 'foreignKey' => 'ldapm_groups_id ', 'dependent' => true]];
    
}
