<?php

App::uses('AppModel', 'Model');

class LdapManagerAppModel extends AppModel {
    
    public $actsAs = [
        'Containable'
    ];

    public function __construct($id = false, $table = null, $ds = null)
    {
        if ($this->useDbConfig == 'ldap' && !array_key_exists('ldap', ConnectionManager::enumConnectionObjects())) {
            
            if (!Configure::read('LdapManager.Ldap.use')) {
                trigger_error(__d('ldap_manager', '(LdapManagerAppModel::__construct) Unable to build ConnectionManager because LDAP not use.'), E_USER_ERROR);
            }
            
            $fields = Configure::read('LdapManager.Ldap.fields');
            
            $settings = [
                'datasource' => 'LdapManager.Ldap',
                'type' => Configure::read('LdapManager.Ldap.type'),
                'host' => [
                    Configure::read('LdapManager.Ldap.host'),
                    Configure::read('LdapManager.Ldap.host_fall_over')
                ],
                'port' => Configure::read('LdapManager.Ldap.port'),
                'basedn' => Configure::read('LdapManager.Ldap.basedn'),
                'login' => Configure::read('LdapManager.Ldap.login'),
                'password' => Configure::read('LdapManager.Ldap.password'),
                'database' => '',
                'tls'      => Configure::read('LdapManager.Ldap.tls'),
                'version'  => Configure::read('LdapManager.Ldap.version'),
                'username' => $fields['User']['username'],
                'account_suffix' => Configure::read('LdapManager.Ldap.account_suffix'),
                'fields' => $fields,
                'certificat' => Configure::read('LdapManager.Ldap.certificat'),
                'forceLogin' => Configure::read('LdapManager.Ldap.forceLogin'),
                'filter' => Configure::read('LdapManager.Ldap.filter'),
            ];
            
            if (is_null(ConnectionManager::create('ldap', $settings))) {
                trigger_error(__d('ldap_manager', '(LdapManagerAppModel::__construct) Unable to build ConnectionManager for $settings data.'), E_USER_ERROR);
            }
        }
        
        parent::__construct($id, $table, $ds);
    }
}
