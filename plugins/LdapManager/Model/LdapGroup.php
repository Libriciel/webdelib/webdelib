<?php

class LdapGroup extends LdapManagerAppModel
{
    public $name = 'Group';
    public $useTable = false;
    public $useDbConfig = 'ldap';
    
    public $primaryKey = 'dn';
    
     public $actsAs = ['LdapManager.LdapType'];
}
