<?php

App::uses('Component', 'Controller');

class GroupManagerComponent extends Component {
    
    public $components = [];
    
    /**
    * Object
    *
    * @var Group
    */
    public $_Group;
    
    /**
    * Request object
    *
    * @var CakeRequest
    */
    public $_Instance;
    
    /**
     * Initializes AuthComponent for use in the controller.
     *
     * @param Controller $controller A reference to the instantiating controller object
     * @return void
     */

    function initialize(Controller $controller) {

        $this->_Instance = $controller;
    }
    
    /**
     * Called automatically after controller beforeFilter
     * @param Controller $controller A reference to the controller object
     */
    function startup(Controller $controller) {
         
        $this->_Group = ClassRegistry::init('LdapManager.Group');
        $this->_modelGroup = ClassRegistry::init('LdapManager.ModelGroup');
        
    }
    
    public function __call($name, $arguments) {
        //FIX A CHANGER
        $position=strripos(Inflector::underscore($name),'_');
        $position=($position+1)-substr_count(Inflector::underscore($name), '_');
        $function=substr($name, 0, $position);
        array_unshift($arguments, substr($name, $position));
        if (method_exists($this, $function))
            return call_user_func_array([$this, $function], $arguments);
        else {
            throw new Exception(sprintf('The required method "%s" does not exist for %s', $name, get_class()));
        }
    }
    
    
    /**
    * Manage Permissions
    */
   public function getGroups($model, $id)
   {
       $this->_Instance->set('groups', $this->_Group->find('list', ['recursive'=>-1]));
       
       $modelGroups = $this->_getlistGroups($model, $id);
       
       $this->_Instance->request->data[$model]['ModelGroup']=$modelGroups;
       
       $this->_Instance->set('groups_'.$model, $model);
   }
   
   /**
    * Manage Permissions
    */
   public function setGroups($model, $id, $groups)
   {
        $modelGroups = $this->_getlistGroups($model, $id);
        if (!empty($modelGroups)) {
            foreach (array_diff($modelGroups, empty($groups)?[]:$groups) as $group_id) {
                $this->_modelGroup->deleteAll(['foreign_key' => $id, $this->_modelGroup->tablePrefix.'groups_id'=> $group_id, 'model' => $model], false);
                
            }
        }
        if (!empty($groups)) {
            foreach (array_diff($groups, $modelGroups) as $group_id) {
                $this->_modelGroup->create();
                $this->_modelGroup->save([$this->_modelGroup->tablePrefix . 'groups_id' => $group_id, 'foreign_key' => $id, 'model' => $model]);
            }
        }
    }
    
    /**
    * Liste des groupes pour un modèle
    * 
    */
    private function _getlistGroups($model, $id)
    {
        return $this->_modelGroup->find('list',['fields' => [$this->_modelGroup->tablePrefix.'groups_id'], 'conditions' => ['foreign_key' => $id, 'model' => $model], 'recursive' => -1]);
    }
    
}
