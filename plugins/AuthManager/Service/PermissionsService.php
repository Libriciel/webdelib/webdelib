<?php
/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AclComponent', 'Controller/Component');

class PermissionsService
{
    protected static $Acl = null;

    protected static $liveCache = [];

    protected static $initialized = false;

    protected static function initialize()
    {
        if (!static::$initialized) {
            $collection = new ComponentCollection();
            static::$Acl = new AclComponent($collection);
            $aclAdapter = static::$Acl->adapter();
            $aclAdapter->Permission->Behaviors->load('DynamicDbConfig');
            $aclAdapter->Aco->Behaviors->load('DynamicDbConfig');
            $aclAdapter->Aro->Behaviors->load('DynamicDbConfig');
            static::$Acl->adapter($aclAdapter);

            static::$initialized = true;
        }
    }

    public static function check($path, $permission = '*')
    {
        static::initialize();

        $id = AuthComponent::user('id');

        $cacheKey = "{$id}.{$path}.{$permission}";
        if (!array_key_exists($cacheKey, static::$liveCache)) {
            static::$liveCache[$cacheKey] = static::$Acl->check(
                [ 'model' => 'User', 'foreign_key' => $id],
                /* 'controllers/'.*/
                $path,
                $permission
            );
        }

        return static::$liveCache[$cacheKey];
    }

    public static function clear()
    {
        static::initialize();
        static::$liveCache = [];
    }
}
