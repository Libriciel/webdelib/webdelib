<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

if (!empty(${'acos_'.$model})) {
    $this->BsForm->setLeft(0);
    $this->BsForm->setRight(12);

    $titles = [
        ['title' => __d('auth_manager', 'permissions')
        ]
    ];
    foreach (${'permKeys_'.$model} as $key => $value) {
        $titles[] = ['title' =>__d('auth_manager', $value),
                    'class'=> 'text-center'
        ];
    }
    $tableDroits= $this->Bs->table(
        $titles,
        ['hover', 'striped'],
        [
            'class' => ['tableAco'. $model],
            'data-toggle'=>'table',
        ]
    );
    $configCRUD = Configure::read('AuthManager.configCRUD');

    $noCruds = [];
    $autoselects = [];
    $firstActionGroupNames = [];
    foreach ($configCRUD['CRUD'] as $key => $value) {
        if (!empty($value['noCrud']) && $value['noCrud'] === true) {
            $noCruds[] = $key;
        }
        if (!empty($value['auto-select']) && $value['auto-select'] === true) {
            $autoselects[] = $key;
        }
        if (!empty($value['groupName']) && !in_array($value['groupName'], $firstActionGroupNames, true)) {
            $firstActionGroupNames[$key] = $value['groupName'];
        }
    }

    $fullAllChecked = true;

    if (!empty(${'acos_'.$model})) {
        foreach (${'acos_'.$model} as $key_aco=>$aco) {
            $parent = false;
            if (
                !empty(${'acos_' . $model}[$key_aco + 1]['Aco']['parent_id'])
                && ${'acos_' . $model}[$key_aco + 1]['Aco']['parent_id'] === $aco['Aco']['id']
                && $aco['Aco']['id'] !== 1
            ) {
                $parent = true;
            }

            $aco_select = $this->Bs->icon($parent ? 'arrow-circle-down' : 'arrow-right');
            $action = substr($aco['Action'] , strpos($aco['Action'], '/') +1);
            $niveau = substr_count($aco['Action'], '/');

            if (array_key_exists($action, $firstActionGroupNames)
            ) {
                $tableDroits .= $this->Bs->cell(__d('auth_manager', $firstActionGroupNames[$action]),
                    '', ['style' => 'font-weight: bold;padding-left: 0.5em'])
                    . $this->Bs->cell(null, 'text-center', ['colspan' => count(${'permKeys_' . $model})]);
                $this->Bs->linePosition(0);
            }

            $label = '';
            if ($aco['Aco']['parent_id'] ===1 || empty($aco['Aco']['parent_id'])) {
                $label = $this->Bs->tag('b', __d('auth_manager', $aco['Action']));
            } else {
                $aco_select = $this->Bs->icon(
                    'arrow-right',
                    [],
                    [
                        'style' => 'margin-left: '. $niveau .'em'
                    ]
                );

                $label = __d('auth_manager', $aco['Action']);
            }


            $this->Bs->lineAttributes(
                [
                    'data-model' => $model,
                    'data-aco_id' => $aco['Aco']['id'],
                    'data-parent_id' => $aco['Aco']['parent_id']
                ]
            );

            $allChecked = true;
            foreach (${'permKeys_'.$model} as $key => $value) {
                // debug($aco['Aro']);
                if ($allChecked && (
                    empty($aco['Aro'][0]['Permission'][$value])
                xor (!empty($aco['Aro'][0]['Permission'][$value]) && $aco['Aro'][0]['Permission'][$value] !== '1')
                )) {
                    $allChecked = false ;
                }
            }
            if (!$allChecked) {
                $fullAllChecked = false;
            }

            if(in_array($action, $autoselects, true)) {
                $checkbox = $this->Bs->tag(
                    'label',
                    __d('auth_manager', $aco['Action']),
                    ['class' => 'checkbox-inline', 'style' => 'font-weight: bold;padding-left: 0em']
                );
            } else {
                $checkbox = $this->BsForm->checkbox(
                    'Aco.' . $model . '.' . $aco['Aco']['id'],
                    [
                        'checked' => $allChecked,
                        'class' => 'childCheckboxAuthManager',
                        'autocomplete' => 'off',
                        'data-model' => $model,
                        'data-aco_id' => $aco['Aco']['id'],
                        'title' => __d('auth_manager', 'Donner tous les droits à la ligne'),
                        'style' => 'vertical-align: middle;margin-left: 0.5em',
                        'inline' => true,
                        'label' => $this->Bs->tag('label', $label, [
                            'class' => 'checkbox-inline',
                            'style' => 'font-weight: normal;padding-left: 0.5em',
                        ]),
                    ]
                );
            }

            $checkbox .= $this->BsForm->hidden('Aco.' .$model.'.'. $aco['Aco']['id'].'.alias', ['value' => $aco['Action']]);
            $checkbox .= $this->BsForm->hidden('Aco.' .$model.'.'. $aco['Aco']['id'].'.foreign_key', ['value' => $aco['Aco']['foreign_key']]);
            $aco_select =  $this->Bs->tag('label', $aco_select, ['class' => 'checkbox-inline']);
            $tableDroits .= $this->Bs->cell($aco_select .' '. $checkbox);

            $diff=[];
            $intersect=[];
            $groupAll=false;
            if (in_array($aco['Aco']['alias'], $noCruds, true)) {
                $groupAll=true;
                $diff = array_diff(${'permKeys_' . $model}, ['_update','_create','_delete']);
                $intersect = array_intersect(${'permKeys_' . $model}, ['_update','_create','_delete']);
            }

            foreach (${'permKeys_' . $model} as $key => $value) {
                if(!in_array($action, $autoselects, true)) {
                    $checkbox = $this->BsForm->checkbox(
                        'Aco.' . $model . '.' . $aco['Aco']['id'] . '.permKeys.' . $value,
                        [
                            'checked' => !empty($aco['Aro'][0]['Permission'][$value])
                                && $aco['Aro'][0]['Permission'][$value] === '1',
                            'inline' => 'inline',
                            'style' => (in_array($value, $intersect, true) ? 'display:none' : ''),
                            'autocompete' => false,
                            "data-model" => $model,
                            "data-aco_id" => $aco['Aco']['id'],
                            'class'=> (($value==='_read'&& $groupAll) ? 'AuthManage_groupAllCRUDCheckbox' : ''),
                            'title' => __d('auth_manager', $value),
                            'label' => false]
                    );
                } else {
                    $checkbox = $this->BsForm->checkbox(
                        'Aco.' . $model . '.' . $aco['Aco']['id'] . '.permKeys.' . $value,
                        [
                            'checked' => false,
//                            'checked' => !empty($aco['Aro'][0]['Permission'][$value])
//                                && $aco['Aro'][0]['Permission'][$value] === '1',
                            'inline' => 'inline',
                            'style' => 'display:none',
                            'autocompete' => false,
                            "data-autoselect" => true,
//                            'class'=> (($value==='_read'&& $groupAll) ? 'AuthManage_groupAllCRUDCheckbox' : ''),
                            'title' => __d('auth_manager', $value),
                            'label' => false]
                    );
                }

                $tableDroits .= $this->Bs->cell($checkbox, 'text-center '.($value === '_manager' ? 'danger' : null));
            }
        }
    } else {
        $tableDroits.= $this->Bs->cell(
            $this->Bs->tag('p', $this->Bs->icon('trash')
                   .' '.__('Aucun droit disponible')),
            'text-center',
            ['colspan'=> count(${'permKeys_'.$model})]
        );
    }

    $tableDroits.= $this->Bs->endTable();

    echo $this->Bs->tag(
        'h4',
        $this->BsForm->checkbox(
            'Aco.'.$model,
            [  'checked' => $fullAllChecked,
                'class'=> 'masterCheckboxAuthManager',
                'data-model'=>$model,
                //'inline'=>'inline',
                'title'=>__d('auth_manager', 'Donner tous les droits au tableau'),
                'autocompete' => false,
                'label' => $this->Bs->tag(
                    'span',
                    __d('auth_manager', '%s permissions', __d('auth_manager', ${'acoAlias_'.$model})),
                    ['class' => 'label label-primary']
                )
            ]
        )
    );

    echo $tableDroits;

    echo $this->Bs->scriptBlock(
        "
    require(['domReady'], function (domReady) {
        domReady(function() {
        require(['jquery', '/AuthManager/js/appAuthManager.js'], function ($) {
                $.appAuthManager();
            });
        });
    });"
    );
}
