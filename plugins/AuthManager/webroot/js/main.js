$(document).ready(function () {
    // La master checkbox coche/décoche tout d'un coup
    $(".masterCheckboxAuthManager").change(function () {
        $(".tableAco"+$(this).attr("data-model")+" input[type=checkbox]").not(':disabled').prop('checked', $(this).prop('checked'));
    });
    //Checkbox -> masterCheckbox  data-aco
    $(".childCheckboxAuthManager").change(function () {
        $(".tableAco"+$(this).attr("data-model")+" tr[data-aco_id="+$(this).attr("data-aco_id")+"] input[type=checkbox]").not(':disabled').prop('checked', $(this).prop('checked'));
    });
    
    $(".AuthManage_groupAllCRUDCheckbox").change(function () {
        console.log(".tableAco"+$(this).attr("data-model")+" tr[data-aco_id="+$(this).attr("data-aco_id")+"] :input[type=checkbox][readonly='readonly']");
        $(".tableAco"+$(this).attr("data-model")+" tr[data-aco_id="+$(this).attr("data-aco_id")+"] :input[type=checkbox][readonly='readonly']").prop('checked', $(this).prop('checked'));
    });
});