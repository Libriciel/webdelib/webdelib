<?php
/**
 * AuthManager : Plugin Authenticate User CakePhp
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/scm/viewvc.php/AuthManager/?root=plugins-cakephp AuthManager Project
 * @since       AuthManager v 0.9.0
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('AppModel', 'Model');

/**
 *
 * @package AuthManager.Model
 */

class AuthManagerAppModel extends AppModel {

}
