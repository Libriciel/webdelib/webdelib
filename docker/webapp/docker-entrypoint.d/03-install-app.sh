#!/bin/sh
# vim:sw=4:ts=4:et

set -e

ME=$(basename "$0")

entrypoint_log() {
    if [ -z "${APP_ENTRYPOINT_QUIET_LOGS:-}" ]; then
        echo "$@"
    fi
}

# Vérification de l'existence de l'environnement de test
if [ "${APP_ENV}" = "test" ] || [ "${APP_ENV}" = "behat" ]; then
    entrypoint_log "$ME: info: install not enabled on mode test [PASS]";
    exit 0;
fi;

APP_CONFIG="/data/fqdn_tenants.php"
WORKDIR="/var/www/html"

entrypoint_log "$ME: info: Install webapp (type=${APP_ENV})";

# Vérification de l'existence du fichier de configuration
if [ ! -f "${APP_CONFIG}" ]; then
    entrypoint_log "$ME: error: Le fichier ${APP_CONFIG} n'existe pas [ERROR]";
    exit 1;
fi;

# Changer le répertoire de travail
cd "$WORKDIR" || exit 1

# Vérification de l'existence de l'environnement canary
if [ "${APP_ENV}" = "canary" ]; then
  if ! app/Console/cake install canaryMigration; then
      entrypoint_log "$ME: error: La commande 'app/Console/cake install canaryMigration' a échoué [ERROR]";
      exit 1;
  fi
  entrypoint_log "$ME: L'installation a réussi";
  exit 0
fi;

# Exécuter la commande et vérifier le succès
if ! app/Console/cake install; then
    entrypoint_log "$ME: error: La commande 'app/Console/cake install' a échoué [ERROR]";
    exit 1;
fi

entrypoint_log "$ME: L'installation a réussi";

exit 0
