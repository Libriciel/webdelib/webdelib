#!/bin/sh
# vim:sw=4:ts=4:et

set -e

ME=$(basename "$0")

entrypoint_log() {
    if [ -z "${APP_ENTRYPOINT_QUIET_LOGS:-}" ]; then
        echo "$@"
    fi
}

# Certbot
if [ -z "${ENABLE_LETS_ENCRYPT}" ]; then
    entrypoint_log "$ME: info: Let's encrypt not enabled [PASS]"
    exit 0
fi

# Construire la liste de domaines pour la commande Certbot
CERT_PATH="/etc/letsencrypt/live/"
# shellcheck disable=SC3060
DOMAINS="${SERVER_NAME},$(echo "$DOMAINS_EXTEND" | sed 's/\\//g')"

if [ -f "$CERT_PATH/${SERVER_NAME}/fullchain.pem" ] ; then
    entrypoint_log "Try to renew Let's Encrypt certificate for domain ${SERVER_NAME}"
    if ! certbot  --logs-dir /var/log/apache2/ --work-dir /var/run --http-01-port 80 renew; then
        entrypoint_log "$ME: error: Unable to renew Let's Encrypt certificate [ERROR] ... Continue anyway"
        exit 0
    fi
    cp "$CERT_PATH/${SERVER_NAME}/fullchain.pem" /etc/apache2/ssl
    cp "$CERT_PATH/${SERVER_NAME}/privkey.pem" /etc/apache2/ssl
    exit 0
fi

# Exécuter Certbot pour générer un certificat unique pour tous les domaines
certbot \
    --http-01-port 80 \
    --logs-dir /var/log/apache2/ \
    --work-dir /var/run \
    --standalone --noninteractive --agree-tos \
    --preferred-challenges http \
    --cert-name="${SERVER_NAME}" \
    -m "${LETS_ENCRYPT_EMAIL}" certonly -d "${DOMAINS}"

# Vérification du succès
if [ -f "$CERT_PATH/${SERVER_NAME}/fullchain.pem" ]; then
    # shellcheck disable=SC3054
    entrypoint_log "$ME: Certificat généré avec succès pour les domaines: ${DOMAINS}"
else
    entrypoint_log "$ME: error: Failed to create Let's Encrypt certificate [ERROR] ... Continue anyway"
    exit 0
fi

cp "$CERT_PATH/${SERVER_NAME}/fullchain.pem" /etc/apache2/ssl
cp "$CERT_PATH/${SERVER_NAME}/privkey.pem" /etc/apache2/ssl

entrypoint_log "$ME: info: Enabled Let's Encrypt"

exit 0
