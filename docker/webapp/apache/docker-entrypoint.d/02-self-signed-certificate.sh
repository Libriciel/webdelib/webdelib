#!/bin/sh
# vim:sw=4:ts=4:et

set -e

ME=$(basename "$0")

entrypoint_log() {
    if [ -z "${APP_ENTRYPOINT_QUIET_LOGS:-}" ]; then
        echo "$@"
    fi
}

APACHE_CERTIFICATE_DIRECTORY=/etc/apache2/ssl
KEY_NAME=privkey.pem
CERT_NAME=fullchain.pem
OPENSSL_CONFIG_TEMPLATE=/var/local/openssl-certificate-extension.cnf


PRIVATE_KEY_PATH=${APACHE_CERTIFICATE_DIRECTORY}/${KEY_NAME}
CERTIFICATE_PATH=${APACHE_CERTIFICATE_DIRECTORY}/${CERT_NAME}


if [ -f ${PRIVATE_KEY_PATH} ]
then
    entrypoint_log "$ME: info: Le fichier ${PRIVATE_KEY_PATH} existe déjà [PASS]"
    exit 0
fi;

if [ -f ${CERTIFICATE_PATH} ]
then
    entrypoint_log "$ME: error: Le fichier ${CERTIFICATE_PATH} existe déjà [ERROR]";
    exit 1;
fi;

entrypoint_log "$ME: info: Le certificat du site n'a pas été trouvé, on en génère un"

OPENSSL_CONFIG_PATH=/tmp/openssl.cnf
sed  "s/%SERVER_NAME%/${SERVER_NAME}/" ${OPENSSL_CONFIG_TEMPLATE} > ${OPENSSL_CONFIG_PATH}


CSR_TEMP_PATH=/tmp/$$_csr.pem

openssl req  \
        -new \
        -newkey rsa:4096 \
        -days 825 \
        -nodes  \
        -subj "/C=FR/ST=HERAULT/L=MONTPELLIER/O=LIBRICIEL/OU=CERTIFICAT_AUTO_SIGNE/CN=${SERVER_NAME}/emailAddress=test@localhost" \
        -keyout ${PRIVATE_KEY_PATH} \
        -out ${CSR_TEMP_PATH}

if [ $? -ne 0 ]
then
entrypoint_log "$ME: eintr: Problème lors de la génération du CSR"
exit 4
fi

openssl x509 \
        -req \
        -days 825 \
        -in ${CSR_TEMP_PATH} \
        -signkey ${PRIVATE_KEY_PATH} \
        -out ${CERTIFICATE_PATH} \
        -extfile ${OPENSSL_CONFIG_PATH}

if [ $? -ne 0 ]
then
entrypoint_log "$ME: eio: Problème lors de la signature du certificat"
exit 5
fi

rm ${CSR_TEMP_PATH}
rm ${OPENSSL_CONFIG_PATH}

chmod 400 ${PRIVATE_KEY_PATH}

entrypoint_log "$ME: info: Génération de la clé et du certificat terminé"

exit 0
