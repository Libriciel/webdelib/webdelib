#!/bin/bash
# generate-schema.sh

app/Console/cake schema generate -f -q \
	--exclude ldapm_groups,ldapm_models_groups,modelsections,modeltemplates, \
	modeltypes,modelvalidations,modelvariables,wkf_circuits,wkf_compositions, \
	wkf_etapes,wkf_signatures,wkf_traitements,wkf_visas

app/Console/cake schema generate -f -q \
	-p ModelOdtValidator -m Modelsection,Modeltemplate,Modeltype,Modelvalidation,Modelvariable --path plugins/Config/Schema.php

app/Console/cake schema generate -f -q \
	-p LdapManager -m ldapGroup,modelGroup --path plugins/Config/Schema.php

app/Console/cake schema generate -f -q \
	-p Cakeflow -m Circuit,Composition,Etape,Signature,Traitement,Visa --path plugins/Config/Schema.php