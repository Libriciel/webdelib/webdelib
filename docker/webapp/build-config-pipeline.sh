#!/bin/bash
# build-config-pipeline.sh

set -e

if [ "$APP_ENV" = "behat" ]; then
  exit 0
fi

mkdir -p /data/webdelib_db1/config
cp -ra app/Config/webdelib.inc.default /data/webdelib_db1/config/webdelib.inc
cp -ra app/Config/formats.inc.default /data/webdelib_db1/config/formats.inc
cp -ra app/Config/pastell.inc.default /data/webdelib_db1/config/pastell.inc
sed -i "s/Configure::write *( *'debug' *, *[0-9] *) *;/Configure::write('debug', 1);/" "/data/webdelib_db1/config/webdelib.inc" >> /dev/null 2>&1
sed -i "s/Configure::write *( *'App.fullBaseUrl' *, *[^)]\+ *) *;/Configure::write('App.fullBaseUrl', '$APP_FULL_BASE_URL');/" "/data/webdelib_db1/config/webdelib.inc" >> /dev/null 2>&1
