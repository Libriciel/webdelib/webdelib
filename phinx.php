<?php

if (!class_exists('Configure')) {
    // Définir le chemin vers l'application CakePHP
    define('APP', __DIR__ . '/app/');
    // Définir le chemin vers les datas
    define('DATA', '/data/');
    // Charger manuellement le fichier database.php de CakePHP
    require DATA .'fqdn_tenants.php';
    $fqdn_config = array_unique($config['fqdn_config']);
    unset($config);
} else {
    Configure::load('fqdn_databases');
    $fqdn_config = array_unique(Configure::read('fqdn_config'));
}

$configPhinx =
[
    'paths' => [
        'bootstrap' => '%%PHINX_CONFIG_DIR%%/db/bootstrap.php',
        'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
        'seeds' => '%%PHINX_CONFIG_DIR%%/db/seeds/tests'
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_environment' => env('PHINX_CONFIG_ENVIRONMENT', 'webdelib_db1'),
//        env('POSTGRES_DB') => [
//            'adapter' => 'pgsql',
//            'host' => env('POSTGRES_HOST'),
//            'name' => env('POSTGRES_DB'),
//            'user' => env('POSTGRES_USER'),
//            'pass' => env('POSTGRES_PASSWORD'),
//            'port' => env('POSTGRES_PORT'),
//            'schema' => 'public',
//            'charset' => 'utf8',
//        ]
    ],
    'version_order' => 'creation'
];

foreach ($fqdn_config as $fqdn => $database) {
    $configPhinx['environments'][$database] = [
            'adapter' => 'pgsql',
            'host' => env('POSTGRES_HOST'),
            'name' => env('POSTGRES_DB'),
            'user' => env('POSTGRES_USER'),
            'pass' => env('POSTGRES_PASSWORD'),
            'port' => env('POSTGRES_PORT'),
            'schema' => $database,
            'charset' => 'utf8',
    ];
}

return $configPhinx;
