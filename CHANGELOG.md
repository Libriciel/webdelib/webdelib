# Change Log

Toutes les modifications apportées au projet seront documentées dans ce fichier.
Le format est basé sur le modèle [Keep a Changelog](http://keepachangelog.com/) et adhère aux principes du [Semantic Versioning](http://semver.org/).

## [8.0.0] - 

### Ajouts
- Mise en place d'une architecture multi-tenant avec des schémas séparés de base de donnée #1970
- Mise en place de lspdf2odt pour la conversion des annexes pdf en odt #1940
- Ajout de la gestion du service lspdf2odt au connecteur "Génération des documents" #1947
- Mise en place du certificat https wildcard avec docker #1865
- Mise en place de phinx pour les mises à jour de base de donnée #1861
- Dockerisation de webdelib #1629
- Remplacement du service de fusion gedooo par le fork Flow v1.3.2 #1635
- Mise en place de la génération des documents par défaut en PDF/A-2 balisés au lieu du standard 1.7 #1709
- Ajout d'une complétion automatique des informations de votes (lsvotes) avec les informations récupérées par le connecteur idelibre #1728
- Afficher les erreurs de récupération des votes d'une séance #1727
- Ajout d'un bouton de récupération des votes d'une séance depuis la liste des projets d'une séance #1726
- Pouvoir configurer le connecteur idélibre pour la récupération des votes (lsvotes) avec l'api v2 #1725
- Ajout de la possibilité d'importer les documents de procès-verbaux #1730
- Ajouter les annexes PDF de l'acte dans le flux ls-actes-publication #1486
- Récupération des états de transmission vers le SAE pour le dossier de séance #1690
- Ajouter un bouton de versement pour le dossier de séance #1694
- Ajout de la possibilité d'archiver les dossiers de séance avec pastell #1667
- Archivage des actes par lot #1298
- Ajouter un filtre dans le menu Post-séances > Versement vers le SAE #1653
- Ajouter un nouveau menu pour les versements SAE des autres actes #1656
- Ajout d'un nouveau type de dossier dans le connecteur SAE pour l’archivage des dossiers de séance avec pastell #1658

### Evolutions
- Ajout d'une notification pour les retours d'erreur des versements SAE du dossier de séance #1697
- Prise en compte des nouveaux points d'api pour les courriers Ministériels avec S2low #1965
- Garder le lien demandé avant l'authentification pour les redirections #1959
- Prise en charge de l'erreur "erreur-verif-tdt" pour la récupération des informations TdT avec Pastell #1961
- Mise en place d'une gestion plus fine des droits pour l'administration fonctionnelle des circuits et des services #1960
- Mise à jour de la page de login avec la nouvelle charte et les nouveaux logos Libriciel SCOP #1509
- Récupération des états de transmission vers le SAE pour les actes unitaires #1702
- Intégration de la vérification de l'application dans le menu administration #1810
- Modification des droits utilisateurs pour plus de clarté avec l'ajout de nombreux nouveaux droits #1753
- Limitation des libellés des actes à 499 caractères pour être en correspondance avec le protocole actes #1747
- Nouvelle formulation de notification dans le paramétrage des acteurs #1603
- Mise en place de libreoffice 24.2.6 pour la conversion des documents #1710
- Ajout des informations de publication pour le connecteur GED CMIS (version 4) #1338
- Amélioration de l'erreur en attente de réception lors de la récupération des retours TdT si l'acquittement n'est pas encore disponible dans pastell #1685
- Suppression de la limite du nombre de caractères dans le libellé lors de l'envoi vers pastell #1679
- Les caractères ne sont plus échappé par _ dans le nom du dossier pour l'envoi à pastell #1680
- Récupération de la classification avec la librairie pastell-api-php #1670
- Mise à jour du check applicatif avec la librairie pastell-api-php #1665
- Remplacement des requêtes pastell v1 par la librairie pastell-api-php qui utilise l'api v2 #1664

### Corrections
- Correction empêcher la clore d'une séance si les projets sont en l'état de brouillon #1971
- Correction sur le changement de profils d'un compte utilisateur qui récupérait des droits sur les données au lieu de prendre en compte que les droits sur les contrôleurs #1908
- Correction de la confirmation de lecture du mail sécurisé qui n'était pas récupérée #1494
- Suppression des formats de sortie de la date et du type de séance pour les modèles multi-séances #1800
- Correction pour la récupération des signatures du parapheur Fast #1713
- Suppression des transformations de texte des libellés de projet vers pastell #1641
- Correction de l'orthographe de certains messages #1415
- Correction erreur 500 lors de la suppression d'un utilisateur #1703
- Correction sur la récupération des acquittements depuis le TdT FAST si l'acte contient des annexes télétransmissibles #1688
- Classification incomplète lors de l'envoi des actes vers pastell #1389
- Correction pour la date de l'acte des délibérations qui ne prenaient pas en compte la date de séance, mais la date du jour d'envoi en signature #1661
- Les champs telmobile et telfixe ne sont pas récupérés dans les informations de l'utilisateur depuis le connecteur LDAP #1655

### Limitations connues
- L'utilisation de la validation de circuit avec le connecteur parapheur n'est pas possible avec connecteur pastell activé
- L'utilisation du connecteur parapheur, idélibre et pastell n'autorisent plus de certificat d'authentification
- Limitation d'utilisation de cette version avec pastell v4.0.13 minimum pour la publication des annexes
- Les projets qui ne sont pas signés auront leur libellé automatiquement tronqué après 499 caractères lors de leur modification 
- Modification des modèles de fusion comportant des variables de présence des acteurs à une séance.

## [7.0.3] - 2023-11-10

### Corrections
- Incohérence dans les ACL pour la modification des préférences utilisateurs #1628

## [7.0.2] - 2023-09-15

### Corrections
- Le valideur ne voit pas son projet dans la banette "validés" #1529
- Différence de prise en compte des variables de seance dans la génération de projet dans l'ordre du jour #1595
- Erreurs dans le fichier CSV lors de l'export opendata #1580

## [7.0.1] - 2023-07-20

### Evolutions
- Bloquer les téléchargements suivant une taille par défaut (20 Mo) depuis le connecteur de génération des documents d'un fichier #1571
- Empêcher l'ajout de fichier avec des accents pour les documents modifiables avec LibreOffice #1449
- Gérer les caractères incompatibles ISO-8859-1 pour le connecteur S2low #1563

### Corrections
- Dédoublement du bouton de génération dans le menu "Séance/ A traiter/ Voter" #1583
- Le téléchargement des fichiers d'informations supplémentaires ne fonctionnait plus #1587
- Suppression du message de var_dump des notifications lors de la première connexion #1577

## [7.0.0] - 2023-03-03

### Ajout
- Changement de la license opensource de la CeCILL v2.1 vers GNU Affero v3 #1495
- Migration Ubuntu LTS 20.04 (Fin du support Avril 2025) #1015
- Migration php 7.4 #1455
- Configuration de la politique de confidentialité #1261
- Pouvoir effectuer une signature FAST avec le connecteur parapheur via pastell #1446
- Mise en place des services de génération en services docker #1506
- Pouvoir supprimer les données de projets et séances antérieurs à une date #1566

### Evolutions
- Prise en compte de la compatibilité de version 5 du parapheur pour le connecteur direct #1499
- Ajouter la force du mot de passe utilisateur pour être en conformité avec les préconisations de l'ANSSI (RGPD) #1458
- Suppression de la valeur "Date de naissance" d'un acteur #1466
- Prise en charge des informations supplémentaires de type géolocalisation #1456
- Pouvoir générer des documents finaux pour les commissions avec leur propre modèle #988
- Prise en compte du RGPD dans le paramétrage des notifications utilisateurs #1262

### Corrections
- Ne plus vérifier les modèles de document avec FIDO #1557
- Ne pas laisser la possibilité d'annuler la signature d'un acte déposé manuellement sur le TdT #1548
- Un dépôt manuel sur le TdT n'incrémente pas le compteur des délibérations télétransmises depuis post-séances > editions #1543
- Envoi des convocations et ordre du jour indique statut 'Envoyé, mais non lu' avant même de cliquer sur envoyer #1540
- Suppression de la conversion en hex dans le format json pour l'export des votes électronique #1545
- Tâche auto de récupération des courriers ministériels reste verrouillée #1526
- Les modifications des réglages de notification ne sont pas conservées pour un utilisateur #1347
- Trois points étaient ajoutés aux commentaires parapheur alors que le texte n'était pas tronqué #1523

## [6.0.8] - 2022-10-20

### Corrections
- La récupération des fichiers tamponnés prend désormais en compte la configuration du connecteur #1366
- Correction sur la compréhension du changelog de la version 6.0.7 #1470

## [6.0.7] - 2022-09-07

### Corrections
- Mise en place des variables de configuration de la publication avec des chemins non conformes dans le patch 6.0.6 #1453
- Tâche auto d'envoi des actes au tdt en automatique reste verrouillée dûe à une mauvaise signature de fonction #1438
- Dysfonctionnement de la création ou modification d'une étape de circuit #1444
- Synchronisation de l'annuaire LDAP impossible suite à l'ajout du filtre dans la configuration #1442
- Anonymiser le message dans le mail sécurisé Pastell par défaut dans le corps texte du mail #1448
- Correction du texte du sélecteur pour le nommage de fichier de sortie dans le formulaire de gestion des modèles #1420
- Publication format csv non conforme en fin de document #1452

## [6.0.6] - 2022-07-20

### Ajout
- Prise en charge de la publication des actes vers un flux Pastell #1358
- Mise en place de la date de publication dans l'acte avec l'intégrité de la signature respectée #1360
- Mise en place d'une page de configuration d'un connecteur Pastell vers un flux studio de publication #1361
 
### Evolutions
- Pise en compte de l'annulation d'un acte pour une nouvelle publication #1364
- Ajout du siret dans les informations de collectivité pour la publication #1362
- Pouvoir définir si un type d'acte peut passer en séance #1045
- Ajouter une information sur le type d'acte pour savoir si l'acte doit être publié #1359
- Suppression du numéro d'acte lors de la suppression du vote #1306
- Autoriser ONLYOFFICE pour l'édition des textes #1348
- Mise en place d'un filtre dans la configuration du LDAP pour améliorer la recherche #1330
- Affichage du poids des annexes #1185
- Prise en compte de la date signature iparapheur via le connecteur Pastell #819
- Interdire les espaces dans le champ code des informations supplémentaires #1340
- Prendre en compte lors d'une composition d'étape de circuit la délégation de validation avec le flux Pastell ls-actes-visa #1321

### Corrections
- Lors de la création d'un utilisateur le message de confirmation indique une modification de l'utilisateur #1517
- La signature électronique ne fonctionnait pas pour la nature contrats et conventions via Pastell #1430
- Affichage du Nombre de projets de la séance erroné #1343
- Mise en place par défaut de la connexion LDAP sans certificat #1352
- Connecteur de mail fait planter l'application #1336
- Choix du service émetteur via les préférences #389
- Interdire les espaces dans le champ code des informations supplémentaires #1340
- Après réinitialisation d'un avis, une mention reste apparente dans le modèle #1282

## [6.0.5] - 2022-03-21

### Evolutions
- Désactivation du header 100-continue pour Pastell avec un proxy #1194
- Prise en compte de la non-récupération de l'acte tamponné et du bordereau de télétransmission dans le cas d'un TdT FAST via Pastell #1295
- Afficher le nombre de projets dans une séance #906
- Infobulle case Envoi de documents papiers complémentaires #1305
- Interdire les espaces pour la définition du compteur #436
- Choix du service émetteur via les préférences #389

### Corrections
- Les variables de présence globale ne fonctionnaient plus pour les PVs #1316

## [6.0.4] - 2022-02-04

### Evolutions
- Autoriser Collabora Online pour l'édition des textes #1271
- Modèles de mail modifiables depuis la configuration du serveur #1284
- Prise en charge d'un uid différent de samaccountname pour les serveurs active directory #1272
- Ajout d’une information supplémentaire de type géolocalisation #1270
- Les acteurs notés absent avec mandataire apparaissaient dans les listes d'absence et d'absence avec pouvoir dans la liste des présents en génération #1210

### Corrections
- Mise en place des fichiers de configuration de locale FR au lieu de lien symbolique #1317
- Création de projet en double #1289
- Le tri par numéro d'acte ne fonctionne pas dans une génération via la recherche #1227
- Changement des informations des statuts dans Séances à traiter /Envoi des convocations #1297
- Mettre des guillemets dans le libellé des séances #863
- Les utilisateurs ne voient les projets dont ils ont été corédacteur dans les bannettes : En cours de rédaction, En cours de validation, Validés #1294
- Changement de vocabulaire métier des votes "oui et non" par "pour et contre" #1301
- Dysfonctionnement de l'envoi au parapheur via Pastell #1313
- Prise en compte du certificat CAS si un fichier de certificat existe dans la configuration #1300
- Blocage des boutons de création de projet lors de l'enregistrement pour ne pas créer des projets en doubles #1289

## [6.0.3] - 2021-12-06

### Evolutions
- Prise en compte des mises à jour de patchs automatisées en cascade #1287

### Corrections
- Dysfonctionnement de l'envoi au parapheur via Pastell #1293
- Le corédacteur ne peut pas suivre le circuit de validation si celui si n'a pas d'étape rédacteur dans la définition du circuit de validation #1267
- Afficher les erreurs de retour api idelibre #1278
- Lors de la suppression d'un acteur, il faut que la bulle informative indique le nom de l'acteur #1285
- Manque le dossier cert_parapheur #1250
- Tâche auto d'envoi des actes au tdt en automatiques reste verrouillée #1230
- Connecteur mail : supprimer la partie configuration smtp #1226
- Génération de document via la recherche : Impossible de supprimer le critère renseigné pour le champ 'état' et 'date' également #1225
- Condition "adopté/rejeté" non respectée pour une séance archivée #1218
- Ordre des séances dans Créer une séance #1161

## [6.0.2] - 2021-10-25

### Ajouts
- Mise à jour de patch automatisée #1269

### Evolutions
- Pouvoir activer/désactiver les notifications depuis les paramètres utilisateurs #1203
- Prendre en compte le mail from du connecteur mail comme destinataire principal pour l'envoi de convocation pastell #1234
- Envoi des mails sécurisé via Pastell pouvoir désactiver la vérification du certificat SSL #1235 #1240
- Amélioration de l'affichage quand il y a une erreur de mot de passe #1248
- Mise en place de l'envoi groupé par défaut pour le mail sécurisé Pastell #1233
- Pouvoir supprimer le modèle sélectionné pour la recherche multicritère #1255
- Lors d'une suppression d'un acteur, on indique le nom de l'acteur supprimé et pas son numéro #1249
- Mettre les acteurs en copies cachées lors des envois groupées des convocations #1264
 
### Corrections
- Un élu déclaré comme mandaté apparaît également dans la catégorie non représentée #1155
- Harmonisation des infobulles #1247
- Supprimer colonne "Modifiable après signature" pour Informations supplémentaires de séance #1168
- Corrections du vocabulaire ou de l'orthographe #1243 #1244
- Action "Envoyer à" : Rétablissement de la mention "Rédacteur du projet" à côté du nom du rédacteur du projet #1241
- Prise en compte du datestyle 'ISO, DMY' dans l'application au lieu du système d'application #1229
- Les informations supplémentaires disparaissent lors du refus d'un projet avec des multi-rédacteurs #1183
- Suite à l'annulation d'une signature, un autre acte ne peut être remis dans un circuit #1251
- Le paramétrage du format de sortie de génération implique un blocage lors d'une recherche avec génération multiple et sortie format zip #1228
- Rendre compatible les formats de sortie de génération avec l'envoi IdélibRE #1257
- Prise en compte des commissions lors de l'export GED #1281
- Export des votes électronique non désiré lors d'un retour en arrière #1251

## [6.0.1] - 2020-10-19

### Evolutions
- Mise en place de l'authentification LDAPS via certificat #1180
- Pouvoir désactiver la récupération de la base DN de l'utilisateur pour l'authentification #1173

### Corrections
- Temporisation entre la récupération des Aractes et des documents télétransmis pour le connecteur Pastell #1195
- Optimisation de requête lors de la génération #1193
- Impossible d'exécuter un circuit de validation d'une seule étape #1169
- Augmentation de l'actualisation des paginations (2 passages) #1080
- Impossibilité de faire des recherches multicritères avec une infosup de type liste #1178
- Mise en place d'une clef supérieur à 5 caractères pour sabredav #1179
- Dysfonctionnement du filtre "Rédacteur du projet" dans "Autres actes envoyés au contrôle de légalité" #1079

## [6.0] - 2020-06-16

### Ajouts
- Export json pour vote électronique
- Prise en charge des composants logiciels de la nouvelle plateforme de référence Ubuntu 18.04 LTS (php7.2 et postgresql 10.3)
- Paramétrage des noms de fichier des sorties de génération !628
- Par défaut utilisation de Pastell pour l'envoi des mails sécuriséss !624
- Gestion des notifications par les utilisateurs !623
- Conformité RGPD !621
- Pouvoir remplacer l'objet et le message du mail HTML pour l'envoi de la convocation et de l'ordre du jour !624

### Evolutions
- Pouvoir forcer l'upload d'un modèle !629
- Bordereau de signature pour les autres actes !625
- Pour les transferts de validation vérifier que l'utilisateur ait les droits d'effectuer les actions demandées !620
- Pouvoir récupérer le statut tdt d'un acte depuis une console d’administration !617
- Informations supplémentaires éditables après signature (Date de publication...) !616
- Mise en place de la page de connexion LS v2 !613  

### Corrections
- Validation d'un projet verrouillé impossible pour un profil administrateur !641
- L'étape rédacteur lors de l'ajout d'un deuxième circuit doit être le ou les rédacteurs du projet !620
- Les modifications sql sur la version 5.1.4 n'a pas été pris en compte par la console de patch !620
- Date d'exécution des tâches automatiques ne se met pas à jour !619
- La récupération d'acquittement via la console ne fonctionnait plus !617
- Les annexes tamponnées en .doc sont également récupérées !615

## [5.1.5] - 2020-10-08

### Corrections
- Suppression du plugin browser de sabredav !645

## [5.1.4] - 2019-12-26

### Evolutions
- Déverrouillage automatique des tâches automatiques en cas de blocage !606
- Vérification de la récupération de la classification (check TdT) avec connexion login/password !601
- Ne pas prendre en compte le tedetis_transaction_id manquant pour une confirmation de télétransmission avec Pastell !597

### Corrections
- Envoi en GED impossible des délibérations pour l'export d'un acte avec Pastell !610
- L'authentification pour S2low avec certificat avec login/password ne prenait pas le bon utilisateur !605
- Le critère de tri sur les acteurs ne fonctionnent plus dans me menu "Tous les projets - Autres Actes - Télétransmis" !604
- Pour l'utilisateur, lors de l'ajout d'un nouveau circuit, il ne peut pas être mis directement comme circuit par défaut !602
- Lors du refus d'un projet, celui-ci est bien dupliqué avec ses annexes correspondantes au projet initial !600
- Les critères de tri ne fonctionnent plus pour une recherche avancée" !599
- Les actes en erreur sur le TdT bloque la tache automatique en cas de changement de status !598
- Blocage à l'envoi d'une délibération en signature si celle-ci n'a pas été voté aux détails !596
- La visualisation pdf ne marche pas pour les actes en cours de signature pour le menu autre actes validés !595
- Le droit de signature pour les autres actes bloque le droit de signature pour les délibérations !594
- L'ajout d'une annexe écrase les valeurs des cases à cocher des annexes déjà ajoutées en amont !589
- L'ajout d'annexe ne se fait pas s'il y a déjà une annexe au format odt !588

## [5.1.3] - 2019-10-15

### Evolutions
- Prise en compte de Pastell 3.0 !583

### Corrections
- Récupération des annexes tamponnées avec une source au format odt impossible !585
- Les projets des séances sans action doivent avoir une date de décision pour l'envoi au TdT !582
- Notifications de profil envoyées vers des comptes inactifs !581
- Mise à jour des séquences inopérantes sur le patch 5.1.2 !580

## [5.1.2] - 2019-09-19

### Evolutions
- Les actes en erreur sur le TdT sont vérifiés en cas de changement de status !569
- Nom des modèles pour la finalisation des séances #942
- Problème de gestion des droits sur les modèles d'édition !548
- Bloquer l'envoi d'un projet en circuit de validation quand l'acte est déjà envoyer en signature !545
- La date limite ne doit pas être envoyée au parapheur !543
- Mise en place d'une meilleur gestion des réinitialisations de compteur !490
- Mise en place d'une meilleur gestion du verrou !537
- Ajout du nom de la personne en cours d'édition du projet #921
- Ajout d'un texte d'alerte pour déclarer un dépôt manuel au TdT !541

### Corrections
- Les annexes converties trop nombreuses bloquent la remontée des annexes depuis le TdT !565
- Ne pas actualiser le document odt lors d'une conversion simple !567
- La recherche textuelle et certains critères de la recherche  multi-critères passaient outre les droits sur les projets !566
- Lors du changement de l'ordre des annexes sur un projet celui-ci n'ordonne pas les suivant !564
- Tâches automatique ne s'exécute plus s'il y a eu un verrou avec un intervalle de temps supérieurs à la programmation !563
- "Recherche multicritère champs [numéro d'acte] un seul paramètre pris en compte" !559
- Des doublons apparaissent sur liste des utilisateurs avec le filtre par profil !562
- Un administrateur peut s'ajouter comme "Autre(s) rédacteur(s)" alors qu'il est déjà le rédacteur principal du projet !560
- Gestion des droits de type de séance avec les administrateurs fonctionnels #968
- Un utilisateur appartenant à plusieurs services peut se voir retirer des services suite à la modification d'un l'administrateur fonctionnel qui n'a pas de droit d'accès à tous les service de l'utilisateur !562
- La visualisation des données du rédacteur provoque une erreur de ressource !557
- Les boutons d'accès à une révision d'un projet désactivé ne doivent pas être cliquable !556
- Changement de la programmation des taches automatiques à l'installation !555
- Pas de pagination dans les listes d'étapes et de compositions des circuits de validation #951
- Total des voix non visibles en modification des votes !554
- La classification ne remonte pas depuis Pastell lorsque le connecteur Tdt est sur une entité mère #954
- Perte de l'identité de l'agent qui a télétransmis l'acte au contrôle de légalité dans l'historique du projet !553
- Acteurs présent plusieurs fois dans la liste des présent en génération !552
- Envoi en GED impossible avec des commissions pour l'export d'une séance !550
- La dernière action du circuit dans la vue synthétique ne remonte pas le bon valideur dans une étape collaborative !549
- Empêcher la remise dans un circuit de validation d'une délibération numérotée !547
- Modifier les informations de télétransmission pour les délibérations n'est pas autorisé avec le droit "Post-séances > À Télétransmettre" #932
- Filtrer mes projets validés par séance provoque une erreur !544
- Les projets refusé ne sont pas dupliqué suivant l'utilisation de la base de donnée !542
- Filtrer les projets validés dans le tableau de bord provoque une erreur quand l'on sélectionne un rédacteur !540
- Modification de la configuration connecteur LDAP ne sauvegarde pas le tableau des concordances !538
- Pouvoir sauvegarder les votes avec le droit de modification #920
- Envoi au parapheur via Pastell demande la classification même si celle-ci est renseignée !534
- L'envoi en GED des autres actes ne fonctionne pas en version 3 du connecteur !535

## [5.1.1] - 2019-04-19

### Ajouts
- Ajout d'un commentaire lorsque l'on fait un "Envoyer à" et "Retourner à" dans le circuit de validation #885 ~Circuit
- Prise en compte de l'envoi multicanal (protocle @cte V2) pour la télétransmission et l'export GED v4 #623 ~TDT ~Pastell ~GED
- Prise en compte des types de document (protocle @cte V2) pour la télétransmission et l'export GED v4 #455 ~TDT ~Pastell ~GED
- Console de purge de base de donnée #886 ~Console

### Evolutions
- Commentaire de saut d'étape ou validation en urgence non obligatoire via la configuration #866 ~Circuit
- Envois en GED Annexes tamponnées pour les autres actes" #883 ~GED

### Corrections
- Les informations supplémentaires ne s'initialisent pas à la création du projet et la création de la séance #870 ~Regression ~Projet ~Seance
- Tache automatique de conversion des fichiers bloquée (Conversion des infosups jointes à la fusion) #869 ~Regression ~"Taches automatiques"
- Saisir plus d'une seule valeur par critère dans le formulaire de filtre des circuits #867 ~Circuit
- Les variables nombre_acteur_present et nombre_acteur_absent ne sont plus présentes en génération #858 ~Regression ~Generation
- Abandon de certains actes impossible #855 ~Projet
- Problème de lors de l'utilisation du filtre par services / sous services #857 ~Filtre
- Impossibilité d'effectuer une recherche via "Ordre du jour" si aucune date de séance n'est spécifiée dans les paramètres de la recherche #846 ~Recherche
- Suppression des séances associées lors de la suppression d'une délibérante #852 ~Regression ~Seance
- La modification d'un projet par un administrateur afficher une erreur si celui-ci n'avait pas terminé pour le verrou d'un projet (supplément: Modification du message)  #842 ~Projet
- Le filtre de la liste des utilisateurs affiche des désactivés avec plus avec 2 critères #868 ~Administration

## [5.1.0] - 2019-01-31

### Ajouts
- Effectuer des actions de validations en dehors du circuit pour un administrateur #314 ~Circuit ~Projet
- Commenter les sauts d'étapes de circuit par un administrateur #314 ~Circuit ~Projet
- Intégrer login/mdp dans le connecteur TDT #322 ~TDT
- Lien entre les commissions et la séance délibérante #238 ~Seance
- Ajout de variables de génération pour le type d'acte #654 ~Generation
- Droits sur les types de séance pour les utilisateurs #332 ~Seance
- Gestion des projets abandonnés : autres actes #422 ~Projet
- Nouvelles variables de génération d'actes et annexes tamponnés pour la recherche multicritères #423 ~Recherche ~TDT
- Dupliquer les circuits #334 ~Circuit ~Administration
- Export GED en préparation de séance ~Connecteurs #325 ~GED
- Reprise des commentaires parapheur lors d'un délégation de circuit #363 ~Circuit
- Nouveau type de modèle "Bordereau de projet" #303 ~Modele ~Generation ~Projet
- Nouveau type de modèle "Journal de séances" #340 ~Modele ~Generation ~Seance
- Export des actes en GED depuis le connecteur Pastell #346 ~Connecteurs ~Pastell  

### Evolutions
- Récupération des annexes par Tdt via Pastell #380 ~TDT
- Gestion des présents en seance #333 ~Generation ~Seance
- Ajouter les notifications envoyées par mail aux acteurs à l'historique d'une délibération #370 ~Notification ~Projet
- Prise en compte de 1000 caractères pour les commentaires (Vote, Refus, Annulation ...) #651 ~Core
- Nouvelle limite du nombre de caractères des nom de fichier des annexes à 45 caractères #57 ~Projet ~Parapheur
- Modification de l'affichage de l'erreur d'ajout d'un fichier annexe d'un projet #57 ~UI
- Saisir plus d'une seule valeur par critère dans le formulaire de Filtre + Limite #321 ~Filtre ~UI
- Les auteurs en génération pour les commentaires #386
- Ajout de l'ordre du jour des projets pour la séance en export GED #448 ~GED
- Ajout du type d'acte en export GED #445 ~GED
- Ajout de la classification pour le projet en export GED #451 ~GED
- Affichage différent pour les dossiers non validés pour la vue de l'ordre du jour #313 ~Seance ~UI
- Filtrer l'affichage des circuits pour les utilisateurs #316 ~Administration
- Nouvelle icône pour les connecteurs #405 ~Administration ~UI
- Une coche blanche sur fond vert pour les projets validés par un utilisateur présent dans le circuit replace le cadena sur fond blanc #206 ~UI ~Projet
- Ajout du champ "rapporteur" et l'étape courante dans la vue synthétique #373 ~Projet
- Prise en compte du type de message (info, danger) sur l'écran Autres actes validés #220 ~Projet
- Redimensionnement automatique de l'image de la collectivité #79 ~Administration
- Pourvoir rechercher des rapporteurs inactifs #413 ~Recherche
- Ajout d'un filtre par service pour les utilisateurs #320 ~UI ~Administration ~Filtre
- Ordre de la recherche par date de séance et ordre du jour combinés #331 ~Recherche
- Pourvoir rechercher des rapporteurs inactifs #413 ~Recherche
- Type de vote en génération #329 ~Generation
- Garder l'ordre dans les champs multiple #335 ~Projet
- Icône de projet validé (coche verte) dans toutes les vues quand le projet est validé #206 ~Projet ~UI
- Distinction de la variable salutation_president #323 ~Generation
- Dans la sélection des circuits les items restent visible en recherche textuelle #429 ~UI
- Traduction des messages d'erreur Cakephp en français et affichage du stack trace pour l'admin #71 ~Administration
- Annuler des actions de télétransmissions, signatures, numérotations #327 ~TDT
- Nom des modèles pour la préparation des séances #324 ~Generation
- Permettre l'authentification à S2low via certificat + login #322 ~TDT
- Empêcher de créer une Information supplémentaire dont le code est déjà utilisé par les variables de modèle #39 ~Securite
- Rester en mode édition après la sauvegarde d'un projet en ajout ou édition #317 ~Projet ~UI
- Mise en place de la page d’accueil Libriciel SCOP #460 ~UI
- Gestion des retours d'erreur s2low #172 ~Connecteurs ~TDT


### Corrections
- Numéro de séquence modifiable depuis webdelib.inc #823 ~Correction ~Administration
- Les délibérations d'une séance sans action qui n'ont pas de séance délibérantes doivent prendre le compteur du type de séance sans action #42 ~Correction ~Seance
- Information de séance manquantes lorsqu'il n' y a pas de délibérante #483 ~Notification
- Saut de ligne double dans l'affichage des commentaires de projet #707 ~Projet
- Le rapporteur n'était pas vide lors d'une génération multi-projet quand celui-ci était bien non renseigné #662 ~Generation
- Date de modification des acteurs non pris en compte #404 ~Administration
- Variable de génération nature_acte et nature_projet #273 ~Generation
- Suppression des convocations #354 ~Seance
- Double-clic impossible sur le champ de saisie d'une date #189 ~UI
- Variables manquantes sur le modèle de type recherche #393 ~Modele
- Configuration du connecteur SAE inopérante depuis l'application #404 ~Administration
- Configuration et authentification CAS inopérante #399 ~Connecteurs ~CAS
- Afficher les thèmes selon l'ordre de tri #395 ~Administration
- Ne pas laisser envoyer les convocations lorsqu'aucun type d'acteur ni acteur n'est renseigné dans le type de séance #264 ~Seance
- Afficher un lien vers la visualisation du projet sur l'écran "donner un avis" #228 ~Seance
- Effacer les traces du passage au parapheur lors d'une signature manuelle #277 ~Projet
- L'ordre d'affichage des valeurs sélectionnées dans le select "ordonner" est éronné #420 ~Regression ~UI
- Afficher le message d'erreur à l'ajout de compteur lorsqu'aucune séquence n'est définie #255 ~Administration
- Permettre la suppression des infosup odt depuis l'écran modification d'une séance #407 ~Seance

## [5.0.2] - 2017-10-12

### Corrections
- Les informations supplémentaires de listes déroulantes initiaient la première valeur en sélection #418 ~Regression ~Projet
- Les taches automatiques bloquées depuis longtemps ne récupéraient pas des dates d'exécutions futures #417 ~Regression ~"Taches automatiques"
- La suppression d'informations supplémentaires de type fichier ne fonctionnait pas en édition #416 ~Regression ~Projet ~Seance
- La tache automatique de purge des fichiers temporaire pour le protocole WEBDAV effaçait le jour en cours #415 ~Regression ~Projet
- Les informations supplémentaires de type textarea des projets et seances passés n'étaient pas converties en odt #402 ~Regression ~Generation
- Affichage des projets dans la bannette "Mes projets en cours d'élaboration et de validation" aléatoire #401 ~Regression ~Circuit ~Projet

## [5.0.1] - 2017-07-18

### Ajouts
- Nouveau shell pour trouver les annexes n'ayant pas de position #353 ~Console

### Evolutions
- Vérification de la présence des textes lors de l'insertion dans le circuit #350 ~Circuit
- L'alerte flash lorsque l'acte tanponné et ses annexes ne sont pas disponibles passe du type "attention" à "information" #345 ~TDT
- Commentaire de projet en ordre décroissant #345 ~Projet
- Désactiver les champs supplémentaires #394 ~Evolution ~Projet
- Export des annexes vers idélibre ~Connencteurs ~Evolution
- Permettre l'ajout d'un commentaire via le formulaire d'attribution d'un circuit ~Evolution ~Projet
- Permettre l'ajout d'un commentaire via le formulaire d'attribution d'un circuit ~Evolution ~Projet
- Date de création de projet en génération ~Evolution ~Generation

### Corrections
- Types de séance et dates de séances restent inactives lors de la suppression à l'édition de projet #406 ~Regression ~Projet
- Les projets d'une séance avec avis sans avis était défavorable par défaut en génération #366 ~Regression ~Generation
- Conservation des dates de séances cloturées dans l'édition de projet #352 ~Projet
- Récupération de la date AR des mails sécurisés #361 ~TDT
- Calcul de la position des annexes n'ayant pas de position pour les anciennes versions #353 ~TDT
- Affichage des délibérations à télétransmettres #355 ~Regression ~TDT
- Enregistrement du commentaire avant le traitement de refus ou validation #351 ~Circuit
- Ralentissement en modification d'un type d'acte #349 ~Administration
- Selection impossible d'un mandataire dans une liste de plus de 10 votants #349 ~Regression ~Seance
- Les informations supplémentaires de type liste n'était pas supprimables #348 ~Regression ~Projet
- Les variables "themeN_projet" qui étaient vides #344 ~Regression ~Generation
- Le webdav était non fonctionnel avec ie et word  #343 ~Regression ~Projet
- Le corps du mail de notification d'alerte de retard de validation était érroné #342 ~Regression ~Notification
- Configuration du connecteur LDAP non pris en compte #341 ~Regression ~Connecteur ~LDAP
- Les textes par défaut n'était pas activé lors des mises à jour de versions précédentes #338 ~Regression ~Projet

## [5.0.0] - 2017-04-21

### Ajouts
- Possibilité d' afficher/masquer les textes dans la configuration du type d'acte #75 ~Administration ~Projet
- Nouveau droit de réatribution de circtui pour un projet validé #236 ~Circuit
- Nouveau droit de visualisation du calendrier des séances #239 ~Seance

### Evolutions
- Prise en charge de php en version 7 #5 ~Core
- Prise en charge de postgresql en version 9.5 #20 ~Core
- Mise à jour de la librairie SabreDav en version 3.2 #14 ~Core
- Mise à jour de la librairie FIDO en version 3.2 #2 ~Core
- Uniformisation du système du choix d'ordre ~Evolution ~UI
- Nombre total de voix dans tableau de vote ~Evolution ~Seance

### Corrections
- Tri des rapporteurs dans la page "Séance à traiter/Ordre du jour"
- Recherche multi-critères : liste des rédacteurs limitée pour le droit rechercher tous les projets #63 ~Regression ~Recherche
- Récupération de la classification en finalisation de séance #22 ~TDT ~UI
- Limites des libellés de projet avec iparapheur depuis Pastell #68 ~Connecteurs ~Pastell
- Tri par ordre alphabétique des types d'actes lors de la création d'un projet #?? ~Administration
- Affichage de la classification dans "Tous les projets/Autres actes/Télétransmis" #17 ~Regression ~TDT
- Envois d'acte sur pastell avec parapheur hors pastell #173 ~Regression ~Connecteur ~Pastell
- Modification de la date de naissance d'un acteur #74 ~Regression ~Administration
- Retours d'erreur idélibre #36 ~Regression ~Connecteurs
- Logs applicatifs iparapheur,cmis,idélibre #33 ~Regression
- Affichage de la classification dans "Tous les projets/Autres actes/Télétransmis  #17 ~Regression ~TDT
- Lien de traiter un projet dans le mail de notification "traiter le projet"  #4 ~Regression ~Notification
- Envois en GED des autres actes "XML en double dans le fichier"  #4 ~GED
- Récupération des annexes tamponnées #77 ~TDT
- Affichage des courriers ministériels #153 ~TDT
- Informations supplémentaires infosups de type list et listmulti non vidé lors des sections seances et projets #235 ~Regression ~Generation
- Filtre lors de l'initialisation sans nom de session pour pagination #246 ~Filtre
- Filtre sur la sélection des séances dans "Tous les projets/Délibérations/à faire voter" #249 ~Filtre
- Recupération des dates de réception des mails sécurisés #252 ~TDT
- Variables de commentaire et d'historique du projet #284 ~Regression ~Generation
- Ordre des annexes qui ne fonctionnait pas avec un nombres d'annexes supérieurs à 9 #261 ~Projet
- Envoi des mails lors des refus dans les circuits avec délégations #285 ~Circuit

### Sécurités
- Prendre compte la configuration en https de base #19 ~Core
- Prise en charge de la distribution Linux Ubuntu LTS 16.04 de base #62 ~Core

## v4.4.1
- Correction sur la désactivation d'un compte utilisateur par une administrateur fonctionnel #61 ~Correction ~Administration
- Correction type de séance incorrect dans le commentaire du projet pour les avis en séance #60 ~Correction ~Seance
- Correction pour la prise en compte des textes de projets vides #59 ~Correction ~Projet
- Correction sur l'envoi au tdt avec une classification sur 5 niveaux #58 ~Correction ~TDT
- Correction sur l'enregistrement des logs iparapheur, idelibre, pastell et s2low #54 ~Correction ~Regression ~api
- Correction sur l'envoi des mails dans les profils qui ne fesait pas apparaitre le message #54 ~Correction ~Regression ~Administration ~Ged
- Correction sur la gestion de la signaure PAdES lors de l'envoi en GED  #55 ~Correction ~Regression ~Ged
- Correction des gestions des droits dans l'interface d'administration des utilisateurs #50 ~Correction ~Administration
- Correction de la génération dans l'interface de signature #53 ~Correction ~Regression ~Generation ~UI
- Correction de la gestion des "Envoyé manuellement" dans le statut TDT #43 ~Correction ~Regression ~TDT
- Correction de la clôture d'une séance sans action signé et numéroté les projets #42 ~Correction ~Seance
- Correction du fil d'ariane séance "donner un avis" #37 ~Correction ~UI
- Correction des variables de génération "theme1_projet", "theme2_projet", etc... ne s'affichaient pas #41 ~Correction ~Generation
- Correction de la section seances des types de modèle multi-séance #49 ~Correction ~Regression ~Generation
- Correction de la suppression du dossier temporaire du "Token" de génération après récupération du fichier #29 ~Correction ~Generation
- Correction de la perte de statut de co-rédacteur pour les utilisateurs ayant le droit éditer tous les projets #44 ~Correction ~Projet
- Correction des informations supplémentaires non existante suite à duplication du projet #35 ~Correction ~Projet
- Correction du traitement par lot : accepter, refuser, etc... ne fonctionnaient pas #16 ~Correction ~Projet
- Correction de la modification d'un projet avec le webdav sans gabarit #38 ~Correction ~Projet
- Correction de l'ordre des annexes dans la vue synthétique du projet #24 ~Correction ~Projet
- Correction de la visualiser de tous les projets lors d'un recherche qui a besoin du droit "tousLesProjetsRecherche" au lieu de "editerTous" #27 ~Correction ~Projet
- Correction du droit "éditer tous les projets" n'était pas effectif dans toute l'application pour les roles utilisateur et administrateur fonctionnel #23 ~Correction ~Administration
- Correction du libellé d'un droit (Séances > Tableau de bord par Tout les projets / Tableau de bord) #25 ~Correction ~Administration
- Correction de la configuration des informations supplémentaires ne prennait pas en compte joindre fusion sur les fichiers Pdf lors du premier clique #30 ~Correction ~Administration
- Correction du téléchargement des gabarits #34 ~Correction ~Administration
- Correction du problème de blocage lors de l'envois des actes #31 ~Correction ~Pastell
- Correction du lien pour traiter le projet dans le mail de notifications n'était pas le bon #32 ~Correction ~Notification
- Mise à jour de la version de cakePHP vers la plus récente de la branche 2.8 #48 ~Evolution ~Securite ~Librairie

4.4.0
======
- Evolution : [Administration] Ajout de la notification des raporteurs à l'insertion d'un projet dans un circuit sur les acteurs
- Evolution : [Modèle] Ajout de AvisProjet_seance_type_libelle, AvisProjet_seance_type_action et AvisProjet_seance_date dans la section AvisProjet
- Evolution : [Modèle] Ajout "theme1_projet", "theme2_projet" ... pour contrer la rupture pour les thèmes
- Evolution : [Circuit] Prise en charge de cakeflow version 4.1.0
- Evolution : [Circuit] Mise en place d'une configuration pour la validation finale dans les circuits (Désactivé par défaut)
- Evolution : [UI] Barre de progression lors des phases d'attentes sur les actions longues (Génération, Export CMIS...)
- Evolution : [Recherche] Amélioration de la recherche unifiée sur les projets et Génération multiples de documents en format zip et pdf
- Evolution : [Connecteur] Prise en compte d'Allo (Pour la mise à jour automatique des applications maintenues par ADULLACT-Projet)
- Evolution : [Génération] Conversion des informations supplémentaires de type fichier pour la fusion en Odt image
- Evolution : [Génération] Gestion des services de génération des documents depuis le connecteur et récupération du dernier fichier fusionné
- Evolution : [Projet] Prise en compte de 1000 caractères pour les informations supplémentaires de type texte
- Evolution : [Tableau de bord] Ajout d'un tableau de bord des projets avec export csv des circuits de traitements
- Evolution : [Export GED] Envoi des annexes tamponnées en version 3 de l'export CMIS
- Evolution : [TDT] Récupération des annexes tamponnées
- Evolution : [Tâches automatiques] Ne pas prendre en compte toutes les tâches pour le bouton " Exécuter toutes lestâches" ou la commande "runAll" (Par défaut: désactivé pour les tâches de conversion, d'alertes de retard et de synchronisation LDAP et Purge)
- Correction : [Seance] Ajout de débat généraux en post-seance via l'écran éditer les débats
- Correction : [Administration] le tri des historiques effaçait le filtre en cours
- Correction : [Export GED] Autres Actes envois multiple non fonctionnels
- Correction : [Export GED] Dossier contenant les fichiers d'information supplémentaire dans le zip non conforme
- Correction : [Génération] Alerte annexes non encore converties
- Correction : [Génération] Débats généraux non pris en compte avec la variable debat_seance
- Correction : [Export i-délibRE] Variables de séance non pris en compte pour l'envoi vers i-délibRE
- Correction : [Configuration] Prise en compte des codes postall commençant par zéro
- Correction : [Projet] Vérification des types d'annexes problème avec le double espace
- Correction : [Projet] Edition des multirédacteurs avec le droit éditer tous
- Correction : [Projet] Suppression de projets multiples et ajout d'annexes
- Correction : [Projet] Lock sur le table séquence pour la génération des numéros d'actes pour éviter les numéros en double
- Correction : [LDAP] Suppression des groupes dans le profil
- Correction : [Signature] Les actes refusés n'étaient pas supprimés du parapheur électronique
- Correction : [UI] Affichage des états de projet dans les circuits de validation
- Correction : [TDT] Envois automatiques vers le TDT ne doit prendre que des actes télétransmissibles
- Correction : [TDT] Prise en compte des natures autres pour l'envois vers le TDT
- Correction : [TDT] Le dernier point de la liste de la classification était manquant
- Correction : [TDT] Utilisation de CURLOPT_CAINFO au lieu de CURLOPT_CAPATH
- Correction : [TDT] Changement du type d'information supplémentaire interdit si un projet est créé avec cette même information supplémentaire
- Correction : [UI] Boucle infini lors de la génération des convocations
- Correction : [TDT] La date de décision envoyée vers le TDT n'était pas la date de séance pour les délibérations
- Correction : [Projet] Ne pas pouvoir modifier un vote après signature
- Correction : [Tâches automatiques] Les éxécutions manuelles ne modifient plus les exécutions programmées
- Correction : [Projet] Modification des options d'annexes joindre fusion et contrôle de l'égalité non effectives
- Correction : [Filtre] Les inputs de type text disparaissait après activation du filtre
- Correction : [Filtre] Correction sur les champs en erreurs (séance, type de séance)
- Correction : [Configuration] Liste des services sélection multiple via ctrl
- Correction : [Parapheur] Erreur lors d'envoi lorsque le nom du projet est trop long (Prise en compte de l'encodage utf-8)
- Correction : [Projet] Visualisation du titre dans la vue d'un projet manquant
- Correction : [Projet] La sélection de la classification était problèmatique avec un 0 en fin, exemple: 7.1 et 7.10
- Correction : [Projet] téléchargement du PV sommaire et complet après séance

4.3.1
======
- Evolution : [Webdav] Configuration du port d'écoute dans webdelib.inc
- Evolution : [UI] Ajout du thème "Readable" pour l'interface utilisateur
- Evolution : [TDT] Activation des envois automatiques vers le TDT des actes (Par défaut: désactivé)
- Evolution : [TDT] Visualisation des entêtes du certificat de connexion
- Correction : [Projet] Upload des annexes avec ie et chrome
- Correction : [Projet] Annexe en double sur projet refusé
- Correction : [Projet] Lors de l'ajout d'une annexe (Annulation)
- Correction : [Projet] Suppression des types de séance d'un projet de délibération
- Correction : [Projet] Un projet peut avoir des séances sans vote avec une séance délibérante
- Correction : [Filtre] Prise en compte de la pagination, télétransmis et "Mes projets" -> "Mon service" sur le type de séance et ajout du filtre "Rédacteur du projet" sur "Mes projets" -> "Mon service"
- Correction : [Circuit] Affichage du circuit utilisateur par défaut pour l'insertion dans un circuit
- Correction : [Circuit] Perte de l'état suite a un deuxième passage dans un circuit de validation pour la gestion des "Autres actes..."
- Correction : [Vote] Bouton récupération de la délibération précédente et affichage des suppléants dans la liste des présents
- Correction : [Webdav] Perte de session
- Correction : [TDT] Prise en compte de la date de l'ARacte pour la récupération des documents du TDT
- Correction : [TDT] Affichage des annexes dans "Autres actes..." -> "Télétransmis"
- Correction : [TDT] Affichage des annexes de projet pour l'envoi au TDT dans "Autres actes..."
- Correction : [Export GED] Envoi vers la GED des séances (Dossiers temporaires érronés + Génération des rapports de séance + PV complet et PV sommaire non présent)
- Correction : [Signature] Récupération des signatures PAdES depuis le parapheur électronique
- Correction : [Signature] Date_limite sur envoi vers le parapheur électronique
- Correction : [Génération] Envoi des informations "richText" et "odtFile"
- Correction : [Génération] Texte_deliberation alias de texte_acte pour la génération
- Correction : [Génération] Gestion des erreurs
- Correction : [Génération] Variables heure_seance et heure_seances
- Correction : [UI] Logo de la collectivité lors des mises à jours manquant
- Correction : [UI] Bouton prendre en compte commentaire + suppression du commentaire
- Correction : [UI] Affichage des projets de nature "délibération" pour les sous-menus de "Tous les projets" -> "Délibérations"
- Correction : [UI] "Tous les projets" -> "A valider" : affichage des séances en limite de temps
- Correction : [UI] Limite d'affichage des séances à traiter sur l'accueil
- Correction : [UI] Bouton édition des projets non visible dans "Autres actes..." -> "A valider"
- Correction : [UI] Suppression de la date limite
- Correction : [UI] Bouton ajouter un projet seulement sur "Mes projets" -> "En cours de rédaction"
- Correction : [UI] Affichage du fil d'Ariane dans les séances à voter
- Correction : [UI] Ordre d'affichage des thèmes dans la création et l'édition des projets
- Correction : [UI] Pagination des actes et ajout de la pagination sur les pages de gestions des "Autres actes...", "Post-séances" et "Mes projets" -> "Mon service"
- Correction : [UI] Affichage du nombre de projets dans tous les écrans "Mes projets" et "Tous les projets"
- Correction : [UI] traitement par lot (Support sur des pages supplémentaires)

4.3.0
======
- Evolution : [Circuit] Validation final non disponible par default
- Evolution : [Parapheur] Ajout de la prise en compte de la signature PAdES du parapheur électronique
- Evolution : [Suivi] Ajout d'un historique global
- Evolution : [Suivi] Mise en place d'un tableau de bord des projets de délibération
- Evolution : [Configuration] Augmentation du libelle de type de séance à 300 caractères
- Evolution : [Export GED] Ajout export des GED des autres actes et correction informations supplémentaires de type fichier et odt
- Evolution : [i-delibRE] Prise en compte du mail obligatoire pour les acteurs lors de l'envois  
- Evolution : [Projet] Mise en place pour les autres actes de la génération du numéro de délibération et de la date de signature pour les signatures manuscrites
- Evolution : [Séance] Modification du nom de la pièce jointe pour l'envoi de l'ordre du jour et de la convocation
- Evolution : [Vote] Mise en place de la fonctionnalité "Prendre acte"
- Evolution : [Vote] Prise en compte de l'ordre d'affichage des acteurs dans la gestion des votes
- Evolution : [Recherche] Mise en place de la recherche unifiée sur les projets
- Evolution : [Recherche] Ajout du numéro d'acte et rédacteur
- Evolution : [Recherche] Ajout des champ de recherche sur les projets, sur la date de création du projet et sur la date de réception en préfecture
- Evolution : [UI] Ajout de la possibilité de voir les annexes lors des télétransmissions vers le TDT
- Evolution : [Configuration] Mise en place du bouton d'action activer/désactiver dans la liste des utilisateurs
- Evolution : [Configuration] Mise en place de l’éditeur des 9 cases
- Evolution : [Configuration] Fusion de 2 services ensemble
- Evolution : [Configuration] Mise en place des mails HTML/TEXT pour les alertes et les nouveaux corps de messages
- Evolution : [Post-séances] Ajout Webdav sur les débats
- Evolution : [Console] Nouveau shell de modification en masse : modifications des alertes mails
- Evolution : [Console] Nouveau shell d'importation de donnée : informations supplémentaires de type liste
- Evolution : [Librairie] Mise en place du plugin GEDOOo (Fusion documentaire)
- Evolution : [Librairie] Mise en place de SabreDav pour le gestion du protocole Webdav
- Evolution : [Librairie] Mise en place de Font-Awesome pour les icones
- Evolution : [Librairie] Changement de growl (Averstissement) de jGrowl vers bootstap.growl
- Evolution : [Librairie] Mise à jour de Bootstrap
- Evolution : [Librairie] Mise à jour de Select2
- Correction : [Configuration] Profils en doubles (désactivés) dans l'édition des utilisateurs
- Correction : [Paramétrage] Limite du type d'acte à 300 caractères
- Correction : [Modèle] Ajout de le règle type_seances pour les modèles de multiséance pour le plugin ModelOdtValidator
- Correction : [Vote] L'acteur non élu n'apparaissait pas dans la liste des suppléants possible
- Correction : [Projet] Un projet voté ne peut plus être validé
- Correction : [Projet] Ajout des annexes xlsx (formats.inc.default)
- Correction : [Projet] Libelle de projet supérieur à 499 caractères tronqué pour l'envoi au TDT
- Correction : [Séance] Les séances sans action n'était plus disponible
- Correction : [Séance] Attribution d'une nouvelle séance depuis une séance non délibérante
- Correction : [Post-séances] Délibérations téléchargeable dans le menu signature lorsque celle-ci sont signées
- Correction : [Génération] Ordre des avis de projets par date par ordre croissant
- Correction : [Parapheur] Génération du numéro d'acte lors de l'envoi au parapheur
- Correction : [Export GED] RelName + version export en version 2 et 3 et prise en compte du https
- Correction : [TDT]envoi de la signature vers le TDT
- Correction : [Circuit] la validation depuis les mails d'alerte pour ne pas valider un projet signé
- Correction : [Circuit] Droit de modification d'un utilisateur dans le circuit de validation
- Correction : [Circuit] Ne prendre en compte que les projets dans un circuit de validation pour les alertes de retard


4.2.02
======
- Correction de la génération des rapports pour l'export CMIS
- Mise à jour cakeflow vers 3.1.02
- Correction de la recherche pour ne pas afficher les services émetteurs inactifs
- Correction du retour après la saisie des débats généraux dans les séances passées
- Ajout d'un contrôle de caractère (255 max.) sur les informations supplémentaires de type texte
- Correction de la génération qui ne fonctionnait pas dans le vu de visualisation du projet à traiter
- Ajout d'un bouton voir l'annexe en modification de projet
- Correction du filtre pour l'envoi TDT, il ne prenait pas en compte les types de séance
- Mise en place de l'optimisation Cakeflow des étapes pour un circuit ajouté à un circuit existant
- Prise en compte du proxy pour le connecteur i-délibRE
- Réorganisation des projets dans toutes les séances suite à la suppression d'un projet
- Correction problème de retour après suppression d'un thème
- Correction pour ie9 pour l'ajout de mandataire lors d'un vote
- Correction prise en compte du thème pour les multi-délibérations
- Prise en compte des variables vide de GEDOOo
- Correction de la recherche de projet : il n'a plus génération possible si le projet a été signé
- Correction de l'extension du nom de fichier pour le téléchargement de la signature (pdf au lieu de zip)
- Correction des caractères spéciaux pour les informations supplémentaires de texte enrichi n'étaient pas pris en compte lors de l'export CMIS.
- Correction les projets refusés s'affichaient avec la séance (ordre du jour...)

4.2.01
======
- Correction information supplémentaire de texte enrichi de projet en édition
- Ajout d'une croix de fermeture pour les générations
- Correction lors de la modification d'un acte avec une date de séance en retard (plusieurs dates)
- Correction : Pas d'alertes de retard sur projet supprimé
- Correction Ajout de la variable "debat_global" lors de la fusion
- Correction Le Bordereau de signature n'est pas bon quand le dossier comportait une annexe à télétransmettre
- Correction prise en compte de 5 niveaux sur la classification TDT
- Correction le suppléant n'apparaissait pas dans la listes des votants en génération
- Prise en compte des ods en modification webdav
- Prise en compte de nouveaux formats d'annexe par défaut : Powerpoint, Excel (hors génération)
- La recherche rapide ne fonctionnait pas correctement lors que l'utilisateur n'avait pas le droit "tousLesProjetsRecherche"
- Correction de la génération et de la validation par lot
- Correction problème bouton retour (historique) vers la home
- Correction du retour de l'acte tamponné via pastell
- Correction nomenclature non pris en compte à la création d'un projet pour pastell
- Correction export v1 prise en compte des annexes en pdf
- Correction les variables du rédacteur de projet et "titre_annexe" étaient manquantes en génération
- Modification de l'administration des connecteurs (Ldap, https pastell ...)
- Ajout du connecteur SAE pour pastell
- Blocage des actions de suppression, valider projet et sauter étape pour une étape parapheur
- Maintenance : Récupération des anciens bordereaux et messages du TDT
- Mise à jour cakeflow vers 3.1.01
- Mise à jour ModelOdtValidator vers 1.0.01

4.2
===
- Ajout d'une confirmation lors d'un refus de projet
- Correction affichage du dernier acteur d'un circuit dans les 9 cases "Dernière action de :"
- Migration de bootstrap vers v2.0.4 à v2.3.2
- Changement de la génération des annexes pour fusion vers gedooo
- Utilisation de FIDO (Format Identification for Digital Objects) pour la détection du format des fichiers basé sur le répertoire des archives nationales du Royaume-Uni (PRONOM)
- Prise en charge des annexes au format .doc .docx
- Refonte du système d'upload des modèles d'édition
- Résolution d'image pour la génération des annexes paramétrable via le fichier webdelib.inc (GS_RESOLUTION)
- Le droit "Modifier les projets validés" devient "Modifier tous les projets" et permet de modifier des projets en cours d'élaboration ou validés
- Amélioration de la notification de refus, contient désormais un lien vers la page d'édition du nouveau projet et indique le nouvel id
- Ajout d'un bouton génération dans la page de traitement d'un projet
- Refonte graphique de la page de traitement et vue de projet (boutons bootstrap avec icônes font-awesome, et changement couleurs)
- Migration de Font-Awesome v3.0.2 vers v4.0.3 (plus d'icônes, nouvelles conventions de nommage des classes)
- Migration de jQuery v1.7.1 vers v1.10.2
- Ajout de deux nouvelles notifications utilisateur : "Un projet dont je suis le rédacteur a été modifié" et "Un projet que j'ai visé a été modifié"
- Fonction "retourner à" de cakeflow corrigée
- Champs texte à partir de gabarits
- Typage des modèles d'édition (catégorie)
- Ajout d'un système de validation des documents "modèles d'édition"
- Affectation d'un président par affaire de séance (par défaut: président de séance)
- Gestion différenciée des actes pour la télétransmission au contrôle de légalité. Un attribut est placé au niveau du type d'acte.
- Dans la page de traitement d'une séance, ajout d'un lien cliquable sur le N° d'ID du projet. Ce lien redirige vers le formulaire de modification du projet.
- Ajout de filtres de recherche dans la page Utilisateurs, filtres sur le profil, nom, prénom et login avec un champ autocomplété et 3 champs texte avec wildcard '%' pour recherche large
- Ajout d'un lien pour remonter en haut de la page. Le lien est représenté par une flèche vers le haut en bas à droite de chaque page et n'apparaît que si besoin.
- Refonte de l'affichage "en arbre" des services, thèmes et profils. Il est désormais possible de plier/déplier les noeuds et de filtrer les données.
- Réécriture du connecteur Pastell
- Refonte configuration des connecteurs iparapheur et S2low qui deviennent Signature et TDT avec la possibilité de selectionner Pastell
- Réécriture du connecteur idelibre
- Création d'une page de configuration du connecteur idelibre
- Refonte de la page d'édition de Multi-delibs
- Refonte édition annexes de projet
- Amélioration de la gestion des erreurs de fichier à l'ajout d'annexe
- Possibilité pour l'utilisateur de choisir le thème de son interface
- Amélioration graphique des tableaux, changement de certains boutons/icônes
- Ajout de différentes tâches automatiques (pour Circuits, Génération, Signature et TDT)
- Tri des résultats de recherche par identifiant décroissant

4.1.04
======
- Correction de la notification par mail lors d'un projet à traiter lors de l'insertion dans un circuit  
- Numérotation hiérarchisée par code des matières de la Classification
- Message d'avertissement lors de la sortie en modification d'un projet d'acte
- Modification de la sortie des documents générés, modification des extensions de fichiers non valides
- Mise ne place d'un place d'un script js placeholder pour les navigateurs non supporté
- Prendre en compte que lorsqu'un projet et dans "mes projets à traiter" qu'il ne s'affiche pas dans "mes projets en cours d'élaboration et de validation"
- Suppression de la limite de mémoire pour le composant iparapheur
- Correction les projets des sous-services n'apparaissaient pas dans les recherches (Service Emetteur)
- Correction sur l'ordre d'affichage des séances : type d'acte > date de séance
- Correction sur la recherche rapide pour les projets visés par un utilisateur
- Modification sur l'upload des annexes, les caractères autorisés pour les nom de fichier sont les alphanumériques et -_.&
- Correction du "retourné à" : prise en compte du circuit d'origine et pas du circuit déroulé
- Modification pour enregistrer les délibérations taponnées et les bordereaux en retour du TDT
- Modification de l'export CMIS pour prendre en compte les délibérations taponnées et les bordereaux de télétransmission
- Modification de la liste des modèles multiséances par ordre alphabétique dans le menu "Séances à traiter"
- Correction sur l'écrasement des avis de projet pour les projets multi-séances
- Correction ie 7-8-9 placeholder avec jquery plugin
- Correction ie 7-8-9 bandeau bleu de connexion
- Modification de  pour la fonction attendable et prise en compte dans la page séances>A traiter>Liste des projets
- Suppression de la section AvisSeance

4.1.03
======
 - Correction à l'import des annexes pdf avec la mise en place des images en paragraphe dans l'odt
 - Correction sur l'affichage de la classification
 - Prise en compte de la variable position_projet à la génération d'un projet
 - Mise en place d'un fond noir pour l'étape en cours du circuit de validation dans webdelib
 - Mise en place des informations supplémentaires, idDepot ->numero_depot dans l'export CMIS
 - Fermeture des requêtes curl après connexion externe
 - Suppression de la signature d'email "This email was sent using CakePHP Framework"
 - Non exécution des tâches planifiées des services désactivés (iparapheur, S2low)
 - Tri des actes pour envoi au contrôle de légalité désormais par numéro de délibération (ascendant)
 - Correction du connecteur Asalae + configuration : webdelib.inc.default
 - Mise en place du logo de la collectivité en BDD
 - Le retour à la liste des délibération sur saisie des débats en post-séance ne fonctionnait pas
 - L'avertissement pour le le texte de projet "vide" pour l'insertion dans un circuit passe de erreur à important
 - Correction lors de l'ajout d'un fichier de débat en post-séance
 - Tri des actes désormais par numéro d'id (descendant) au lieu de la date de création
 - Correction sur les imports de document au format docx (reconnu comme des zip)
 - Correction sur l'affichage des titres des annexes non odt ou pdf dans une génération
 - Correction sur l'ajout d'un acte dans une séance depuis un avis (ajout en fin de séance)
 - Correction sur la suppression d'un acte dans une séance (on réordonne la séance)
 - Mise en place d'un htaccess pour ne pas mettre en cache les documents pdf et odt dans le navigateur (dossier app/www_root/files/generee)
 - Outil de test en batch des documents des actes sur les séances en cours et en rédaction pour gedooo
 - Génération des recueils: trié par numéro (ascendant)
 - Mise en place d'une limite  maximum de vote 1000 caractères pour le commentaire d'une séance
 - Mise en place de la barre de progression pour la gestion des profils, des génération d'ordre du jour, pv ...
 - Mise en place d'une attente lors de la génération de projet
 - Correction sur l'affichage du label de texte projet qui disparaissait à la création d'un acte
 - Mise en place d'une limite maximun de 200 caractères pour le titre d'une annexe et 100 pour le nom de fichier de l'annexe
 - Corrections des entrées historiques de cakeflow
 - Correction des valeurs intial des informations supplémentaires à la création d'un projet
 - Correction sur l'affichage des avis défavorables
 - Correction sur l'envoi i-delibRE

4.1.02
======
 - Prise en compte de la nouvelle version de Gedooo r390
 - Prise en compte de la nouvelle version de Cloudooo 1.2.4r573
 - Ajout de la variable  "texte_projet" pour la génération
 - Création d'un formulaire pour l'envoi des ordres du jour
 - Amélioration de l'upload des types d'annexes
 - Amélioration des mails de notifications
 - Amélioration de l'enregistrement des annexes pdf en odt (format odt valide)
 - Correction sur la génération pour la prise en compte des annexes pdf en fin de documents
 - Correction sur la création d'informations supplémentaires
 - Correction sur la création d'étape de circuit, l'affichage d'un circuit
 - Correction sur l'ajout d'annexes
 - Correction sur la modification d'une séance
 - Correction sur l'attribution du numéro final d'un acte
 - Correction sur les droits des menus
 - Correction de la génération multi-séances
 - Correction sur l'envoi de mail à partir d'un profil
 - Correction sur la suppression d'un utilisateur
 - Correction sur la modification d'une date de séance d'un acte
 - Correction sur le filtre par rapport au type de séance
 - Correction sur la liste des votants
 - Correction sur la recherche par libellé
 - Correction sur le report d'un ordre du jour
 - Correction sur la visualisation d'un projet
 - Correction pour la transmission vers s²low
 - Correction sur un projet refusé
 - Correction du paramétrage de "service_avec_hierarchie"
 - Correction du saut d'étape dans un circuit
 - Correction sur une séance de type non délibérante, prise en compte des avis et commentaires
 - Correction sur l'affichage de la classification
 - Correction sur la saisie des débats généraux

4.1.01
======
 - Multi-modèles et traitement par lot pour la synthèse
 - Gestion des configurations des connecteurs dans l'administration
 - Modification d'une info supplémentaire sous condition de profils/droits

4.1
===
 - Multi-modèles et traitement par lot pour la synthèse
 - Gestion des configurations des connecteurs dans l'administration
 - Modification d'une info supplémentaire sous condition de profils/droits
 - Affichage des annexes via URL
 - Références sur les annexes PDF
 - Intégration des annexes PDF dans les documents générés
 - Désactivation d'une information supplémentaire
 - Refonte de l'ergonomie de l'application
 - Ticket 1097 : Correction d'un bogue sur l'ordre de vote des délibérations
 - Ticket 1374 : Ajout d'un champ service_avec_hierarchie pour l'utilisation dans les modèles
 - Ticket 1408 : Ajout d'un champ date_envoi_signature pour l'utilisation dans les modèles
 - Ticket 1560 : Correction d'un bogue lors de l'enregistrement d'un projet avec jour de retard
 - Ticket 1627/2183 : Correction sur l'enregistrement webdav des multi-délibérations
 - Ticket 1644 : Ajout d'un champ etat_projet pour l'utilisation dans les modèles
 - Ticket 1718 : Correction d'un bogue lors du refus d'un projet par un validateur
 - Ticket 1744 : Correction sur l'application du nombre de jour avant retard d'une séance
 - Ticket 1796 : Ajout d'un bouton pour vider les champs de type file
 - Ticket 1783 : Correction d'un bogue sur les annexes de projet en multi-délibération
 - Ticket 1976 : Ajout des libellés texte projet, note synthèse et texte acte après création du projet
 - Ticket 1962/2021 : Correction d'un bug sur l'ordre des informations supplémentaires
 - Ticket 2029 : Modification de l'encodage des templates mails et webdelib.inc.default en UTF8
 - Ticket 2031 : La variable titre_annexe ne reprenait pas toutes les annexes
 - Ticket 2072 : La fonction de recherche passe de LIKE à ILIKE
 - Ticket 2117 : Modification de la longueur du titre d'une annexe en création
 - Ticket 2147 : Correction sur la recherche avec comme critères des informations supplémentaires
 - Ticket 2175/2217 : Correction d'un bug sur gestion des votes
 - Ticket 2182 : Correction sur l'enregistrement des multi-délibérations dans un projet
 - Ticket 2183 : Correction sur l'enregistrement webdav des multi-délibérations
 - Ticket 2187 : Correction sur les visualisations des projets inclus dans des séances non délibérantes
 - Ticket 2199 : Les projets des sous-services n'apparaissaient pas dans les projet de mon service
 - Ticket 2200 : Chronologie des historiques du plus récent au plus ancien
 - Ticket 2201 : Chronologie des commentaires du plus récent au plus ancien
 - Ticket 2244 : Correction sur la récupération de la liste des mandataires
 - Ticket 2301 : Correction sur le bouton de suppression d'un acteur
 - Ticket 2316 : Correction sur l'enregistrement de l'historique par un traitement par lot des projets visés
 - Ticket 2338 : Liste des projets classés par id sur le tableau de bord
 - Ticket 2356 : Correction prise en compte du type d'actes pour la recherche multi-critères
 - Ticket 2357 : Correction problème d'affichage des boutons "favorable" et "défavorable" sur un avis

4.0
===
 - WEBDELIB migre sur cake2.2.x,
 - Utilisation de PostgreSQL comme base de données,
 - Optimisation de la gestion des arrêtés,
 - Révision des types de séances,
 - Envoi des convocations via mail sécurisé,
 - Traitement par lot,
 - Valider les formats envoyés au contrôle de légalité,
 - Limiter les circuits vus par un utilisateur,
 - Effacer une séance complète,
 - Délibération tamponnée,
 - Outil mail de notifications aux utilisateurs,
 - Vote par groupe,
 - Gestion des suppléants,
 - Ajout du droit de suppression d'un projet pour les administrateurs fonctionnels.

3.5.02
=====

3.5.01
=====

3.5
===

3.0
===
 - Upgrade du framework CakePHP 1.2.8
 - Intégration du plugin CakeFlow : moteur de workflow intégrant des fonctions dynamiques d'exécution  
 - Attribution des droits par profil et par utilisateur
 - Affinage des droits  (modification d'un projet, saut d'étape, envoyer à, ...
 - Gestion de différentes natures d'acte
 - Pagination des listes d'utilisateurs et acteurs
 - Tri de ces listes en fonction de différents critères
 - Changement d'un mot de passe par l'utilisateur lui-même
 - Gestion des circuits via le plugin CakeFlow
 - Affichage des thèmes et services avec la forme hiérarchique
 - Ajout des critères de filtre par bannette
 - Enrichissement de l'historique

2.1.02
=====
 - Correction d'un bogue lors de l'enregistrement des PVs (en mode éditeur en ligne).
 - Correction d'encodage des caractères spéciaux (CP1252).
 - La première personne d'un circuit d'élaboration ne recevra plus le mail d'insertion mais le mail a traiter...
 - Lors d'un retour de dossier, la personne a qui est renvoyé le dossier recevra un mail de notification.
 - Rétablissement du menu sinon cela posait un problème de droit.
 - Correction de problème d'encodage lorsque l'on utilise l'éditeur en ligne en mode 'source'
 - Désactivation du bouton précédent du navigateur pour éviter les effets de bords (exemple, je clique sur précédent après avoir validé un projet...)
 - Patch pour les navigateurs IE 6 : le message jgrowl empeche l'ouverture de la page avec ce navigateur.

2.1.01
=====
 - Correction des short_open_tag
 - Correction d'un bogue lors de la saisie des débats de projets en commission
 - Correction d'un bogue dans l'affichage des délibérations télétransmises
 - Correction d'un bogue lors de la ré-attribution d'un projet à un circuit
 - Actualisation du logo
 - Changement de la mise en page des vues synthétiques
 - Information par Infos Bulles
 - Possibilité (en fonction des droits) de sauter une étape du circuit
 - Enregistrement de la vie du projet dans un historique ( exploitable dans la génération des documents)
 - Possibilité d'avoir plusieurs fois le même utilisateur dans un circuit d'élaboration
 - Possibilité de saisir les débats de délib en post-séances
 - Enregistrement des PVs au format PDF
 - Possibilité de choisir directement la position d'un projet
 - Insertion dans le iparapheur électronique avec le libellé du projet.
 - Insertion des pièces jointes dans le iparapheur électronique
 - Récupération de la signature des dossiers envoyés dans le iparapheur électronique
 - Export vers AS@LAE

2.1
====
 - Choix du PV Sommaire lors de l'édition des types de séance (BUG N°330)
 - Amélioration de la cinématique OOo
 - Simplification de l'éditeur en ligne (BUG N°229)
 - Affichage du signe ¿ dans les documents générés (BUG N°297)
 - Informations supplémentaires: Ajout du type "Booléen"
 - Informations supplémentaires: Ajout du type "Fichier ODT"
 - Informations supplémentaires: Ajout du type "Liste déroulante"
 - Possibilité de changer le contenu des mails envoyés par l'application (BUG N°340)
 - Connexion au iparapheur électronique (pour la validation en préparation et pour la signature en post-séance)
 - Ajout d'un bouton "Retourner à ..." qui permet de retourner le projet à n'importe quel membre du circuit avant la personne qui traite le projet
 - Possibilité de recherche sur un circuit
 - Amélioration des tris des projets
 - Possibilité de générer les convocations et ordres du jour au format odt
 - Numérotation hiérarchisée des rapports/délibérations
 - Correction de la saisie des débats de projet (BUG N°316)
 - Gestion des pouvoirs (liste des mandataires) (BUG N°308)
 - Désactivation de la gestion du Quorum (BUG N°146)
 - Possibilité d'ajouter un commentaire à une séance
 - Stockage des délibérations au format pdf après la clôture de la séance.
 - Suite à l'envoi de délibération à s2low, il est possible de suivre l'avancé du cycle de vie de la délibération.
 - Récupération du bordereau d'acquittement du MIAT
 - Correction des fautes d'orthographe et de faute d'accent
 - Compatibilité avec le navigateur Chrome pour Windows
 - Ajout des variables : avisCommission, date_reception, commentaire_seance, total_votant, acteur_sans_participation

2.0.03
=====
 - Conservation des informations supplémentaires, annexes et commentaires suite à un refus de dossier.
 - Ré-ajout des styles : ActeursPour, ActeursContre et ActeursAbstenu

2.0.02
=====
 - Correction faute d'orthographe (mois de septembre mal orthographié)
 - Correction bug lors d'un refus de dossier : le rédacteur ne recevait pas de mail
 - Mise en place des boutons d'aperçu pour la génération des convocations et de l'ordre du jour

2.0.01
=====
 - Ajout des Styles ActeursContre, ActeursPour, ActeursAbstention qui permet de lister les élus qui ont votés contre, pour ou se sont abstenus au vote d'un projet.
 - Correction d'un bogue dans l'affichage des services et des thèmes.
 - Correction d'un bogue lors du changement d'ordre ou de la suppression des informations supplémentaires.

2.0
====
 - Suppression des composants documentaires, la conception des modèles se fait dans un fichier unique.
 - Ajout de variable pour la génération des documents
 - Importation de la partie PHP de GEDOOo dans WEBDELIB
 - Ajout d'un moteur de recherche.
 - Envoi des pièces jointes des délibérations au contrôle de légalité
 - Enregistrement des débats de commission dans un nouveau champ
 - Possibilité de valider un projet en urgence.
 - Ajout de champ dynamique à la création d'un rapport de projet.

1.1.02
=====
 - Uniformisation de l'encodage des fichiers : tous sont encodés en ISO-8859-15

1.1.01
=====
 - Ajout de balises pour la génération des documents.
 - Validation des commentaires.
 - Import de fichiers odt pour les textes de projet, notes de synthèse et textes de délibération.
 - Correction de bug d'affichage
 - Agrandissement du champ commentaire lors des votes.
 - Moteur de recherche simple.

[7.0.0]: https://gitlab.libriciel.fr/webdelib/webdelib/compare/6.0.8...7.0.0
[6.0.8]: https://gitlab.libriciel.fr/webdelib/webdelib/compare/6.0.7...6.0.8
[6.0.7]: https://gitlab.libriciel.fr/webdelib/webdelib/compare/6.0.6...6.0.7
[6.0.6]: https://gitlab.libriciel.fr/webdelib/webdelib/compare/6.0.5...6.0.6
[6.0.5]: https://gitlab.libriciel.fr/webdelib/webdelib/compare/6.0.4...6.0.5
[6.0.4]: https://gitlab.libriciel.fr/webdelib/webdelib/compare/6.0.3...6.0.4
[6.0.3]: https://gitlab.libriciel.fr/webdelib/webdelib/compare/6.0.2...6.0.3
[6.0.2]: https://gitlab.libriciel.fr/webdelib/webdelib/compare/6.0.1...6.0.2
[6.0.1]: https://gitlab.libriciel.fr/webdelib/webdelib/compare/6.0.0...6.0.1
[6.0.0]: https://gitlab.libriciel.fr/webdelib/webdelib/compare/5.1.5...6.0.0
[5.1.5]: https://gitlab.libriciel.fr/webdelib/webdelib/compare/5.1.4...5.1.5
[5.1.4]: https://gitlab.libriciel.fr/webdelib/webdelib/compare/5.1.3...5.1.4
[5.1.3]: https://gitlab.libriciel.fr/webdelib/webdelib/compare/5.1.2...5.1.3
[5.1.2]: https://gitlab.libriciel.fr/web-delib/web-delib/compare/5.1.1...5.1.2
[5.1.1]: https://gitlab.libriciel.fr/web-delib/web-delib/compare/5.1.0...5.1.1
[5.1.0]: https://gitlab.libriciel.fr/web-delib/web-delib/compare/5.0.2...5.1.0
[5.0.2]: https://gitlab.libriciel.fr/web-delib/web-delib/compare/5.0.1...5.0.2
[5.0.1]: https://gitlab.libriciel.fr/web-delib/web-delib/compare/5.0.0...5.0.1
[5.0.0]: https://gitlab.libriciel.fr/web-delib/web-delib/compare/4.4.1...5.0.0
