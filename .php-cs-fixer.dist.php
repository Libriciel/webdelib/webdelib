<?php

declare(strict_types=1);

/*
 * This file is part of PHP CS Fixer.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *     Dariusz Rumiński <dariusz.ruminski@gmail.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

use PhpCsFixer\Config;
use PhpCsFixer\Finder;

$header = <<<'EOF'
webdelib : Application de gestion des actes administratifs
Copyright (c) Libriciel SCOP (https://www.libriciel.fr)

Licensed under The CeCiLL V2 License
For full copyright and license information, please see the LICENSE.txt
Redistributions of files must retain the above copyright notice.

@copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr)
@link        https://adullact.net/projects/webdelib webdelib Project
@license     https://cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
EOF;

$finder = (new Finder())
    ->ignoreDotFiles(false)
    ->ignoreVCSIgnored(true)
    ->notPath(__DIR__ . '/.php-cs-fixer.dist.php')
    ->notPath(__DIR__ . '/app/Config/database.php')
    ->notPath(__DIR__ . '/app/Config/core.php')
    ->notPath(__DIR__ . '/app/Console/cake.php')
    ->notPath(__DIR__ . '/app/Console/Command/AppShell.php')
    ->notPath(__DIR__ . '/app/View/Helper/FormHelper.php')
    ->notPath(__DIR__ . '/app/View/Helper/BsFormHelper.php')
    ->exclude('app/Tests/Fixtures')
    ->in(__DIR__ . '/app/Console')
    ->in(__DIR__ . '/app/Model')
    ->in(__DIR__ . '/app/Controller')
    ->in(__DIR__ . '/app/View')
    ->in(__DIR__ . '/app/Lib')
;

return (new Config())
    ->setRiskyAllowed(true)
    ->setUsingCache(false)
    ->setCacheFile(__DIR__.'/.php_cs.cache')
    ->setFinder($finder)
    ;
