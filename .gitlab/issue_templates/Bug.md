<details>
<summary>Sommaire</summary>

[[_TOC_]]
</details>

## Description

Ex : Les bannettes doivent devenir noires quand le projet est vérouillé
Si possible mettre un imprime écran. (cliquer sur attach a file)

## Url(s) de l'erreur :

- [Edition d'un projet](http://web-delib.local/deliberation/:id)
- [Autres](http://web-delib.local/:path)

## Liste des actions menant au bug

Lister ici les actions permettant de reproduire le bug

## Message d'erreur :

Lister ici le(s) message(s) d'erreur(s) afficher à l'utilisateur ou des logs  

### Tâche à effectuer
(liens vers les sous-tâches ou liste à faire)
- [ ] Ma tâche

### Checklist de développement

**Avant une demande de Merge Request**
- [ ] Mettre à jour le ChangeLog

**Avant une demande de Merge Request**
- [ ] Ajouter le temps réel `/spend`

## Checklist de vérification après déploiement en production

- [ ] Notification de la disponibilité sur le ticket client OTRS

## Ticket
- Ville [Ticket](lien vers le ticket)

/label ~"Origine: Ticket client" ~"Type:Bug" ~"Priorité::Basse" ~"Roadmap::A qualifier"

/assign @splaza