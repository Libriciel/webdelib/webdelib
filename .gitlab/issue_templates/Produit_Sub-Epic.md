<details>
<summary>Sommaire</summary>

[[_TOC_]]
</details>

## Demande de fonctionnalité
<!-- Une sub-Epic est un travail important qui peut être découpé en un certain nombre de petites stories. Si cela ne semble pas correct, choisissez un autre template. -->
### Problème à résoudre
<!-- Une description claire et concise de la nature du problème. Ex. Je suis toujours frustré quand [...] -->

*En tant que* ...   
*Je veux* ...    
*Afin de* visualiser ...

### Contexte supplémentaires
<!-- Ajoutez tout autre contexte ou capture d'écran sur la demande de fonctionnalité ici. -->

### Checklist produit
**Avant T0**
- [ ] Toutes les demandes sont qualifiées par l'équipe Product Owner ~"Roadmap::A qualifier"
- [ ] Toutes les demandes sont mise en attente par l'équipe projet ~"Roadmap::Backlog"
- [ ] Toutes les demandes sont proposé par l'équipe projet ~"Roadmap::A étudier"
**Après T0**
- [ ] Toutes les demandes sont retenuees en T0 ~"Roadmap::Retenu"
- [ ] Toutes les demandes sont mises en bonus en T0 ~"Roadmap::Bonus"
- [ ] Toutes les demandes sont proposées en T0 ~"Roadmap::Bonus"

**Avant mise en design**
- [ ] Toutes les demandes sont étudiées par l'équipe Product Owner ~"Métier::Prêt pour le dev"
- [ ] Toutes les demandes sont validées par l'équipe projet ~"Design::A maquetter"

**Avant mise en développement**
- [ ] validation de la maquette par l'équipe projet ~"Design::Test OK"
- [ ] Ajouter le temps estimé `/estimate`
- [ ] Planifier la demande ~"Dev::A Faire"

**Après développement**
- [ ] Clôturer le temps réel `/spend`
- [ ] Vérifier que toutes les demandes soient mise en place dans le ChangeLog

**Avant mise en recette continue**
- [ ] Vérifier que toutes les rétrospectives ont présentées à l'équipe projet
- [ ] Toutes les demandes sont validées par l'équipe projet ~"Recette::A tester"

## Production

**Requalifier les tickets OTRS**

- [ ] Collectivité [Ticket](lien_vers_le_ticket)

/label ~"Origine: Ticket client" ~"Priorité::Basse" ~"Roadmap::A qualifier"
/assign @splaza