<details>
<summary>Sommaire</summary>

[[_TOC_]]
</details>

## Demande de correction
### Problème à résoudre
<!-- Une description claire et concise de la nature du problème. Ex. Les bannettes doivent devenir noires quand le projet est vérouillé [...] -->

## Erreur
### Plateforme
| Plateforme de recette                  |  Utilisateur   |
|----------------------------------------| ---------------|
| https://webdelib.recette.libriciel.net |                 |

### Url(s)
- https://webactes.recette.libriciel.net/administration/utilisateurs

### Liste des actions
<!-- Lister ici les actions permettant de reproduire le bug -->

*En tant que* ...   
*Je ne veux pas* ...    
*Afin de* visualiser ...

### Copie d'écran :
<!-- Lister ici le(s) message(s) d'erreur(s) afficher à l'utilisateur, si possible mettre un imprime écran ou des logs -->

### Checklist de développement
*Le développeur doit ajouter la progression à chaque étape importante du développement*
**Avant une demande de Merge Request**
- [ ] Clôturer le temps réel `/spend`
- [ ] Vérifier que la correction soit mise en place dans le ChangeLog

**Après une demande de Merge Request**
- [ ] La correction doit avoir le label ~"Dev::Fait"

/label ~"Origine:Recette" ~"Type:Bug" ~"Recette::Dev - A corriger"
/assign @splaza 




