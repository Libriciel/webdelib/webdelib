<details>
<summary>Sommaire</summary>

[[_TOC_]]
</details>

## Demande de fonctionnalité
<!-- Suggérer une idée pour web-delib. Si cela ne semble pas correct, choisissez un autre template. -->
### Problème à résoudre
<!-- Une description claire et concise de la nature du problème. Ex. Je suis toujours frustré quand [...] -->

*En tant que* ...   
*Je veux* ...    
*Afin de* visualiser ...

### Contexte supplémentaires
<!-- Ajoutez tout autre contexte ou capture d'écran sur la demande de fonctionnalité ici. -->

## Produit

### Solution sélectionnée
<!-- Une description claire et détaillé (règles métier) de ce qu'il doit se passer -->

### Remarques
<!-- Remarques ou les solutions ou fonctionnalités alternatives que vous avez envisagées. -->

### Proposition de design
<!-- (Exemple d'action pour aider le design pour les propositions envisagées)
- Bouton d'action depuis ....
 -->

**Avant mise en design**

- [ ] La demande est suffisamment définie par l'équipe Product Owner ~"Métier::Prêt pour le dev"
- [ ] La demande est validée par l'équipe projet ~"Design::A maquetter"

**Avant mise en développement**
- [ ] validation de la maquette par l'équipe projet ~"Design::Test OK"
- [ ] Ajouter le temps estimé `/estimate`
- [ ] Planifier la demande ~"Dev::A Faire"

## Recette

### Paramétrage
<!-- Une description du paramétrage -->

### Critères d'acceptation
<!--  liens vers les critères d'acceptation) -->

## Développement
Le développeur peut indiquer un statut bloqué si celui trouve une incohérence dans la solution proposée.
*Le développeur doit ajouter la progression à chaque étape importante du développement*
**Avant une demande de Merge Request**
- [ ] Clôturer le temps réel `/spend`
- [ ] Vérifier que la demande soit mise en place dans le ChangeLog

**Après une demande de Merge Request**
- [ ] Faire une rétrospective avec l'équipe projet
- [ ] Toutes les demandes sont validées par l'équipe projet ~"Recette::A tester"

## Production

**Requalifier les tickets OTRS**

- [ ] Collectivité [ticket_client]()

/label ~"Agile : User Story"  ~"Métier:Infos à fournir"
/assign @splaza 