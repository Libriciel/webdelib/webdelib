<details>
<summary>Sommaire</summary>

[[_TOC_]]
</details>

# Préparation T0 WD

## Début des développements

1 novembre 2023

## Fin des développements

15 mars 2024

## Sortie prévisionnelle

12 avril 2024

## Forces en présence

* Mickael : 0 semaines
* Sébastien : 24 semaines

**84 jours disponibles (Annuel).**

***Calcul :***   
*12 mois * 20 j * 0,7 ETP = 168 jours j/h   
168 jours j/h * 0,5 ETP = 84 j/h*

## Fonctionnalités prévues

| Ordre | Priorités | Fonctionnalités | Développement (j/h) | Effectué (%) 
|:--:|:---------:| :------------------------------------------ |:-------------------:|:------------: 
| 1 | Haute | []()                         |         0j          |      0%      |
| **Total**  |   ||       **0j**        |    **0j**    |

## Fonctionnalités non prévues

|   Ordre   | Priorités | Fonctionnalités | Développement (j/h) | Effectué (%) 
|:---------:|:---------:| :------------------------------------------ |:-------------------:|:------------: 
|     1     | Haute | []()                         |         0j          |      0%      |
| **Total** |   ||       **0j**        |    **0j**    |

## Fonctionnalités demandées supplémentaires non prévues et non étudiées
-...

## Calendrier prévisionnel

```mermaid
gantt
  title WD
  dateFormat YYYY-MM-DD
  axisFormat %m-%y
  
  section x versions 
  T0 : milestone, m1, 2023-11-01, 0d
  webdelib 7.1: a1, 2023-11-01, 130d
  Recette: crit, r1, after a1, 10d
  Go : milestone, m2, 2024-04-01, 0d
```
---

/label ~"Origine:Dev"
/assign @splaza 




