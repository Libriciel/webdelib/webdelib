'use strict'

const path = require('path')

module.exports = {
    mode: 'development',
    entry: ['./app/webroot/js/webpack/main.js'],
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'app/webroot/js/webpack/dist'),
        publicPath: '/js/webpack/dist/',
    },
    module: {
        rules: [
        {
            test: /\.(?:js|mjs|cjs)$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: [
                        ['@babel/preset-env', { targets: "defaults" }]
                    ]
                }
            }
        },
        {
            test: /\.css$/, // Gérer les fichiers CSS
            use: ['style-loader', 'css-loader'],
        },
        {
            test: /\.(woff|woff2|eot|ttf|otf|svg)$/, // Gérer les polices et icônes
            type: 'asset/resource',
            generator: {
                filename: 'fonts/[name][ext]',
            },
        },
        ],
    },
    resolve: {
        extensions: ['.js'], // Permet de résoudre les fichiers JS sans extension explicite
    },
}