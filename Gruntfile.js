'use strict';

module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        version: grunt.file.read('VERSION.txt').replace(/[\r\t\n]/g, ''),
        requirejs: {
            compile: {
                options: {
                    locale: "fr-fr",
                    optimize: 'none',
                    useStrict: true,
                    findNestedDependencies: true,
                    preserveLicenseComments: true,
                    baseUrl: "app/webroot/js",
                    mainConfigFile: 'app/webroot/js/config.js',
                    out: 'app/webroot/js/require-v<%= version %>.min.js',

                    include: ['components/requirejs/require'],
                    done: function (done, output) {
                        var duplicates = require('rjs-build-analysis').duplicates(output);

                        if (Object.keys(duplicates).length) {
                            grunt.log.subhead('Duplicates found in requirejs build:');
                            grunt.log.warn(duplicates);
                            return done(new Error('r.js built duplicate modules, please check the excludes option.'));
                        }

                        done();
                    },
                    error: function (done, err) {
                        grunt.log.warn(err);
                        done();
                    }
                }
            }
        },
        connect: {
            serverTest: {
                options: {
                    port: 9001,
                    base: 'app/webroot/'
                }
            }
        },
        watch: {
            src: {
                files: ['app/webroot/*.js'],
                tasks: ['default'],
            },
        },
    });

    // Load the plugin that provides the "uglify" task.
    //grunt.loadNpmTasks('grunt-contrib-uglify');
    //grunt.loadNpmTasks('grunt-bower-requirejs');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task(s).
    grunt.registerTask('default', ['requirejs']); //'uglify' 'bowerRequirejs'

};
