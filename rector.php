<?php
use Rector\Config\RectorConfig;

return RectorConfig::configure()
    ->withPaths([
        __DIR__ . '/app',
        __DIR__ . '/plugins/AuthManager',
        __DIR__ . '/plugins/Cakeflow',
        __DIR__ . '/plugins/LdapManager',
        __DIR__ . '/plugins/ModelOdtValidator',
    ])
    ->withFileExtensions([
        'php',
        'ctp',
        'inc',
        'inc.default',
    ])
    // register single rule
    ->withRules([
        \Rector\Php54\Rector\Array_\LongArrayToShortArrayRector::class,
    ]);
    // here we can define, what prepared sets of rules will be applied
//    ->withPreparedSets(
//        deadCode: true,
//        codeQuality: true
//    );
